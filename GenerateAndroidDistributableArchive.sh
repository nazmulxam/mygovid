set -e
export ANDROID_HOME="~/Library/Developer/Xamarin/android-sdk-macosx"
echo $ANDROID_HOME
export PATH=$PATH:$ANDROID_HOME/platform-tools:$ANDROID_HOME/build-tools/28.0.3
echo $PATH
# Restore all the nugets
nuget restore myGovID.sln
# Build the solution
cd Application/Droid
MSBuild "/t:Clean;Build;SignAndroidPackage" "/p:Configuration=${1}" "/p:Platform=AnyCPU" "/p:AndroidUseSharedRuntime=False" "/p:EmbedAssembliesIntoApk=True" "Droid.csproj" "/p:MtouchArch=${2}"
# Zip the android package
DATE=`date '+%Y-%m-%d %H:%M:%S'`
zipalign -f -v 4 "bin/${1}/au.gov.ato.myGovID-${2}.apk" "bin/myGovID.${1}-${2}-${DATE}.apk"
# Sign the android package
# TODO: Change the keystore based on the input configuration. myGovIDDebug for Debug/Mock and myGovIDProduction for Release
apksigner sign --ks myGovIdDebug --ks-key-alias mygovid.debug "bin/myGovID.${1}-${2}-${DATE}.apk"
echo ""
echo ""
echo "Success -- Build is Application/Droid/bin/myGovID.${1}-${2}-${DATE}.apk"
