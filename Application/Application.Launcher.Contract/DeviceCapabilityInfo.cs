﻿namespace myGovID.Application.Launcher.Contract
{
    public enum DeviceCapabilityInfo
    {
        Touch,
        Fingerprint,
        FacialVerification,
        Camera
    }
}