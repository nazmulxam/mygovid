﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace myGovID.Application.Launcher.Contract.Mock
{
    public class MockDeviceInfoWrapper : IDeviceInfo
    {
        private readonly IDeviceInfo _actualDeviceInfo;

        public string PlatformTypeOverride;
        public string RuntimeTypeOverride;
        public string RuntimeVersionOverride;
        public string FormFactorTypeOverride;
        public bool? HasBiometricsOverride;
        public bool? HasCameraOverride;

        public MockDeviceInfoWrapper(IDeviceInfo actualDeviceInfo)
        {
            _actualDeviceInfo = actualDeviceInfo;
        }

        public string PlatformType => PlatformTypeOverride ?? _actualDeviceInfo.PlatformType;

        public string RuntimeType => RuntimeTypeOverride ?? _actualDeviceInfo.RuntimeType;

        public string RuntimeVersion => RuntimeVersionOverride ?? _actualDeviceInfo.RuntimeVersion;

        public string FormFactorType => FormFactorTypeOverride ?? _actualDeviceInfo.FormFactorType;

        public IEnumerable<DeviceCapabilityInfo> Capabilities
        {
            get
            {
                var filtered = new HashSet<DeviceCapabilityInfo>(_actualDeviceInfo.Capabilities);

                if (HasBiometricsOverride.HasValue && !HasBiometricsOverride.Value)
                {
                    filtered.Remove(DeviceCapabilityInfo.FacialVerification);
                    filtered.Remove(DeviceCapabilityInfo.Fingerprint);
                    filtered.Remove(DeviceCapabilityInfo.Touch);
                }

                if (HasCameraOverride.HasValue)
                {
                    if (HasCameraOverride.Value)
                    {
                        filtered.Add(DeviceCapabilityInfo.Camera);
                    }
                    else
                    {
                        filtered.Remove(DeviceCapabilityInfo.Camera);
                    }

                }

                return filtered;
            }
        }
    }
}
