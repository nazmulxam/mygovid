﻿using myGovID.Services.Dependency.Contract;

namespace myGovID.Application.Launcher.Contract.Mock
{
    public class MockLaunchConfiguration: LaunchConfiguration
    {
        protected MockLaunchConfiguration(): base()
        {
        }

        public IDependencyService DependencyService { get; private set; }
        public string TestcaseId { get; private set; }
        public bool UseBiometrics { get; private set; }
        public new sealed class Builder
        {
            private MockLaunchConfiguration _configuration = new MockLaunchConfiguration();
            public Builder SetApplicationInfo(IApplicationInfo applicationInfo)
            {
                _configuration.ApplicationInfo = applicationInfo;
                return this;
            }
            public Builder SetDeviceInfo(IDeviceInfo deviceInfo)
            {
                _configuration.DeviceInfo = deviceInfo;
                return this;
            }
            public Builder SetDependencyService(IDependencyService dependencyService)
            {
                _configuration.DependencyService = dependencyService;
                return this;
            }
            public Builder SetTestcaseId(string testcaseId)
            {
                _configuration.TestcaseId = testcaseId;
                return this;
            }
            public Builder SetUseBiometrics(bool useBiometrics)
            {
                _configuration.UseBiometrics = useBiometrics;
                return this;
            }
            public MockLaunchConfiguration Build()
            {
                return _configuration;
            }
        }
    }
}