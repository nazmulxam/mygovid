﻿namespace myGovID.Application.Launcher.Contract
{
    public class LaunchConfiguration
    {
        protected LaunchConfiguration() { }
        public IApplicationInfo ApplicationInfo { get; protected set; }
        public IDeviceInfo DeviceInfo { get; protected set; }

        public sealed class Builder {
            private LaunchConfiguration _configuration = new LaunchConfiguration();
            public Builder SetApplicationInfo(IApplicationInfo applicationInfo)
            {
                _configuration.ApplicationInfo = applicationInfo;
                return this;
            }
            public Builder SetDeviceInfo(IDeviceInfo deviceInfo)
            {
                _configuration.DeviceInfo = deviceInfo;
                return this;
            }
            public LaunchConfiguration Build()
            {
                return _configuration;
            }
        }
    }
}
