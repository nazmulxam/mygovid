﻿using System.Collections.Generic;

namespace myGovID.Application.Launcher.Contract
{
    public interface IDeviceInfo
    {
        /// <summary>
        /// Gets the type of the platform.
        /// </summary>
        /// <value>The type of the platform.</value>
        string PlatformType { get; }

        /// <summary>
        /// Gets the type of the runtime.
        /// </summary>
        /// <value>The type of the runtime.</value>
        string RuntimeType { get; }

        /// <summary>
        /// Gets the runtime version.
        /// </summary>
        /// <value>The runtime version.</value>
        string RuntimeVersion { get; }

        /// <summary>
        /// Gets the type of the form factor.
        /// </summary>
        /// <value>The type of the form factor.</value>
        string FormFactorType { get; }

        /// <summary>
        /// Gets the capabilities.
        /// </summary>
        /// <value>The capabilities.</value>
        IEnumerable<DeviceCapabilityInfo> Capabilities { get; }
    }
}
