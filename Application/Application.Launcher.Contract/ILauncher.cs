﻿namespace myGovID.Application.Launcher.Contract
{
    public interface ILauncher
    {
        void Configure(LaunchConfiguration configuration);
    }
}
