﻿using System;
using UIKit;

namespace myGovID.iOS.Views
{
    public static class UIToolbarFactory
    {
        /*
         * Return a UIToolbar with a Done button that automatically calls
         * ResignFirstResponder on the container view.
         */
        public static UIToolbar DoneButtonToolbar(Action onDone = null)
        {
            var toolbar = new UIToolbar
            {
                BarStyle = UIBarStyle.Default,
                Translucent = true,
                TintColor = UIColor.Black
            };
            toolbar.SizeToFit();

            var doneButton = new UIBarButtonItem("Done", UIBarButtonItemStyle.Plain, (sender, args) => {
                var controller = UIApplication.SharedApplication.KeyWindow.FindTopViewController();
                controller?.View?.EndEditing(true);
                onDone?.Invoke();
            });

            var spaceButton = new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace);
            toolbar.SetItems(new UIBarButtonItem[] { spaceButton, doneButton }, false);
            return toolbar;
        }
    }
}
