﻿using UIKit;
using CoreGraphics;
using Foundation;
using System;

namespace myGovID.iOS.Views
{
    public class UITextFieldWithBinding: UITextField
    {
        public UITextFieldWithBinding(): base()
        {
            CommonInit();
        }
        public UITextFieldWithBinding(IntPtr p) : base (p)
        {
            CommonInit();
        }
        public UITextFieldWithBinding(CGRect rect): base(rect)
        {
            CommonInit();
        }
        public UITextFieldWithBinding(NSCoder coder): base(coder)
        {
            CommonInit();
        }
        void CommonInit()
        {
            EditingChanged += (s, e) => { };
        }
    }
}
