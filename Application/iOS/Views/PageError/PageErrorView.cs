using CoreGraphics;
using Foundation;
using myGovID.iOS.Views.Common;
using myGovID.Presentation.Contract;
using UIKit;

namespace myGovID.iOS.Views
{
    public partial class PageErrorView : UIView
    {
        public PageErrorView(UIView containerView, PageMessage pageMessage) : base(containerView.Bounds)
        {
            NSBundle.MainBundle.LoadNib("PageErrorView", this, null);
            Icon.Image = Icon.Image.ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
            Icon.TintColor = UIColor.White;
            Update(pageMessage);
            AddSubview(RootView);
            HeightAnchor.ConstraintEqualTo(RootView.HeightAnchor).Active = true;
            WidthAnchor.ConstraintEqualTo(RootView.WidthAnchor).Active = true;
            RootView.BackgroundColor = ColorsHelper.ErrorLabelColor;
            foreach (NSLayoutConstraint Constraint in containerView.Constraints)
            {
                if (Constraint.FirstAttribute == NSLayoutAttribute.Height)
                {
                    containerView.RemoveConstraint(Constraint);
                }
            }
            SetNeedsLayout();
            containerView.AddConstraint(NSLayoutConstraint.Create(containerView, NSLayoutAttribute.Height,
                                                                  NSLayoutRelation.Equal,
                                                                  null,
                                                                  NSLayoutAttribute.NoAttribute,
                                                                  1,
                                                                  RootView.Frame.Height));
        }
        private void Update(PageMessage pageMessage)
        {
            TitleLabel.Hidden = true;
            DescriptionLabel.Hidden = true;
            if (pageMessage == null)
            {
                return;
            }
            if (!string.IsNullOrWhiteSpace(pageMessage.Title))
            {
                TitleLabel.Text = pageMessage.Title;
                TitleLabel.SizeToFit();
                TitleLabel.Hidden = false;
            }
            if (!string.IsNullOrWhiteSpace(pageMessage.Description))
            {
                new ThemedHtmlParser(pageMessage.Description)
                .ApplyToLabel(DescriptionLabel);
                DescriptionLabel.SizeToFit();
                DescriptionLabel.Hidden = false;
            }
            RootView.SizeToFit();
            RootView.SetNeedsLayout();
        }
    }
}