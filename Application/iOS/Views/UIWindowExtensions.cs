﻿using System;
using UIKit;

namespace myGovID.iOS.Views
{
    public static class UIWindowExtensions
    {
        public static UIViewController FindTopViewController(this UIWindow window)
        {
            var top = window.RootViewController;
            while (true)
            {
                if (top.PresentedViewController != null)
                {
                    // Presented controllers are always on top
                    top = top.PresentedViewController;
                }
                else if (top is UINavigationController)
                {
                    // Special case container controller
                    top = ((UINavigationController)top).VisibleViewController;
                }
                else if (top is UITabBarController)
                {
                    // Special case container controller
                    top = ((UITabBarController)top).SelectedViewController;
                }
                else
                {
                    break;
                }
            }
            return top;
        }

        public static void ShowLoading(this UIWindow window)
        {
            DismissLoading(window);
            var indicator = new LoadingIndicator(UIScreen.MainScreen.Bounds);
            indicator.Alpha = 0;
            window.AddSubview(indicator);
            UIView.Animate(
                0.25,
                () =>
                {
                    indicator.Alpha = 1;
                }
            );
        }

        public static void DismissLoading(this UIWindow window)
        {
            var subViews = window.Subviews;
            foreach (UIView indicator in subViews)
            {
                if(indicator is LoadingIndicator)
                {
                    UIView.Animate(
                        0.25,
                        () => { indicator.Alpha = 0; },
                        indicator.RemoveFromSuperview
                    );
                    break;
                }
            }
        }
    }
}
