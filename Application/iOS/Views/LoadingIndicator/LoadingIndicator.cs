using System;
using UIKit;
using CoreGraphics;
using System.Threading.Tasks;
using Foundation;
using System.Linq;
using myGovID.Presentation.Contract;

namespace myGovID.iOS
{
    public class LoadingIndicator : UIView
    {
        private Action _action;
        public UIActivityIndicatorView Indicator;

        public LoadingIndicator(CGRect rect) : base(rect)
        {
            BackgroundColor = new UIColor(0, 0, 0, 0.75f);
            AutoresizingMask = UIViewAutoresizing.All;
            nfloat labelHeight = 22;
            nfloat labelWidth = Frame.Width - 20;

            // derive the center x and y
            nfloat centerX = Frame.Width / 2;
            nfloat centerY = Frame.Height / 2;

            // create the activity spinner, center it horizontall and put it 5 points above center x
            Indicator = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.WhiteLarge);
            Indicator.Frame = new CGRect(
                centerX - (Indicator.Frame.Width / 2),
                centerY - Indicator.Frame.Height / 2,
                Indicator.Frame.Width,
                Indicator.Frame.Height);
            Indicator.AutoresizingMask = UIViewAutoresizing.All;
            AddSubview(Indicator);
            Indicator.StartAnimating();
        }
        public override void TouchesMoved(NSSet touches, UIEvent evt)
        {
            // Dont propagate touch events.
        }

    }
}