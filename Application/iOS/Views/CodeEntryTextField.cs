﻿using System;
using CoreGraphics;
using Foundation;
using ObjCRuntime;
using UIKit;

namespace myGovID.iOS.Views
{
    [Register("CodeEntryTextField")]
    public class CodeEntryTextField : UIView
    {
        private CodeContainerStackView _codeContainerStackView;
        private TransparentTextField _transparentTextField;
        private int _codeLength;

        public UITextField TextField {
            get
            {
                return _transparentTextField;
            }
        }

        public CodeEntryTextField(NSCoder coder) : base(coder)
        {
            Setup();
        }
        public CodeEntryTextField(CGRect frame) : base(frame)
        {
            Setup();
        }
        public CodeEntryTextField(IntPtr handle) : base(handle)
        {
            Setup();
        }

        private void Setup(int codeLength = 6)
        {
            _codeLength = codeLength;

            _codeContainerStackView?.RemoveFromSuperview();
            _codeContainerStackView = new CodeContainerStackView(_codeLength);

            _transparentTextField?.RemoveFromSuperview();
            _transparentTextField = new TransparentTextField(CGRect.Empty);
            _transparentTextField.AddTarget((sender, args) => {
                HandleCodeChanged();
            }, UIControlEvent.EditingChanged);

            AddSubviews(new UIView[] { _codeContainerStackView, _transparentTextField });
        }
        public void SetCodeLength(int codeLength)
        {
            Setup(codeLength);
        }
        public override void LayoutSubviews()
        {
            _codeContainerStackView.Frame = Bounds;
            _transparentTextField.Frame = Bounds;
            base.LayoutSubviews();
        }

        private void HandleCodeChanged()
        {
            var code = _transparentTextField.Text;
            // Truncate the code to the max characters
            if (code.Length > _codeLength)
            {
                code = code.Substring(0, _codeLength);
                _transparentTextField.Text = code;
            }
            // If the last value was entered hide the keyboard
            if (code.Length == _codeLength)
            {
                _transparentTextField.ResignFirstResponder();
            }
            // Display the new code
            _codeContainerStackView.UpdateCodeCharacters(code);
        }
    }

    class CodeContainerStackView : UIStackView
    {
        public CodeContainerStackView(NSCoder coder) : base(coder)
        {
            throw new NotSupportedException();
        }
        public CodeContainerStackView(CGRect frame) : base(frame)
        {
            throw new NotSupportedException();
        }
        public CodeContainerStackView(int length) : base(CGRect.Empty)
        {
            Setup(length);
        }
        private void Setup(int length)
        {
            Axis = UILayoutConstraintAxis.Horizontal;
            Distribution = UIStackViewDistribution.FillEqually;
            Alignment = UIStackViewAlignment.Fill;
            Spacing = 8;

            // Remove any existing views so we can safely call this method multiple times
            foreach (var view in ArrangedSubviews)
            {
                RemoveArrangedSubview(view);
            }
            for (var i = 0; i < length; i++)
            {
                AddArrangedSubview(new CodeCharacterLabel(CGRect.Empty));
            }
        }
        public void UpdateCodeCharacters(string fromText)
        {
            var chars = fromText.ToCharArray();
            for (var i = 0; i < ArrangedSubviews.Length; i++)
            {
                var value = i < chars.Length ? chars[i].ToString() : String.Empty;
                var labelSubview = (UILabel)ArrangedSubviews[i];
                labelSubview.Text = value;
            }
        }
    }

    class CodeCharacterLabel : UILabel
    {
        public CodeCharacterLabel(NSCoder coder) : base(coder)
        {
            Setup();
        }
        public CodeCharacterLabel(CGRect frame) : base(frame)
        {
            Setup();
        }
        private void Setup()
        {
            Layer.BorderWidth = 1;
            Layer.BorderColor = UIColor.Gray.CGColor;
            TextAlignment = UITextAlignment.Center;
            TextColor = UIColor.LightGray;
        }
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            // Calculate font size, check width or height, whichever is smaller, and add some padding
            var max = (nfloat) Math.Min(Bounds.Width, Bounds.Height);
            Font = UIFont.SystemFontOfSize(max - (max > 48 ? 24 : 8));
        }
    }

    class TransparentTextField : UITextField
    {
        public TransparentTextField(NSCoder coder): base(coder)
        {
            Setup();
        }
        public TransparentTextField(CGRect frame) : base(frame)
        {
            Setup();
        }
        private void Setup()
        {
            TextColor = UIColor.Clear;
            BackgroundColor = UIColor.Clear;
            AccessibilityIdentifier = "verificationCode";
            KeyboardType = UIKeyboardType.NumberPad;
            InputAccessoryView = UIToolbarFactory.DoneButtonToolbar();
        }

        public override void DrawPlaceholder(CGRect rect)
        {
            // Don't draw the placeholder
        }

        public override CGRect GetCaretRectForPosition(UITextPosition position)
        {
            // Hides the cursor
            return CGRect.Empty;
        }

        public override UITextSelectionRect[] GetSelectionRects(UITextRange range)
        {
            // Disable selection
            return new UITextSelectionRect[0];
        }

        public override bool CanPerform(Selector action, NSObject withSender)
        {
            // Disables popup actions like copy/paste
            return false;
        }

        public override bool ShouldChangeTextInRange(UITextRange inRange, string replacementText)
        {
            // Disables pasting text
            return false;
        }
        public override UITextPosition GetClosestPositionToPoint(CGPoint point)
        {
            // Anywhere you tap will set the cursor to the end of the field
            return GetPosition(BeginningOfDocument, Text.Length);
        }

        public override void AddGestureRecognizer(UIGestureRecognizer gestureRecognizer)
        {
            // Disable the long press gesture
            if (gestureRecognizer is UILongPressGestureRecognizer) {
                gestureRecognizer.Enabled = false;
            }
            base.AddGestureRecognizer(gestureRecognizer);
        }
    }
}
