﻿using UIKit;
using CoreGraphics;
using Foundation;
using CoreAnimation;
using System;
using System.Threading.Tasks;

namespace myGovID.iOS
{
    /// <summary>
    /// How to use:
    ///  new SuccessCheckMarkView(UIScreen.MainScreen.Bounds)
    ///                                        .SetParent(View)
    ///                                        .Start();
    /// </summary>
    public class SuccessCheckMarkView : UIView
    {
        private float _lineWidth = 4.0f;
        private float _duration = 2;
        private readonly int _size = 100;
        private UIView _parent;
        private CGColor _lineColor = UIColor.White.CGColor;

        public SuccessCheckMarkView(CGRect rect) : base(rect)
        {
        }

        public SuccessCheckMarkView AddTransparentBackground()
        {
            var background = new UIView(Frame)
            {
                BackgroundColor = UIColor.Black,
                Alpha = 0.7f
            };

            AddSubview(background);
            return this;
        }

        public SuccessCheckMarkView SetDuration(float duration)
        {
            _duration = duration;
            return this;
        }

        public SuccessCheckMarkView SetLineColor(UIColor color)
        {
            _lineColor = color.CGColor;
            return this;
        }

        public SuccessCheckMarkView SetParent(UIView parent)
        {
            _parent = parent;
            return this;
        }

        public void Start(Action onCompleteAction)
        {
            _parent?.AddSubview(this);
            CATransaction.Begin();
            CATransaction.CompletionBlock = onCompleteAction;
            CreateCircle();
            CreateCheckmark();
            CATransaction.Commit();
        }

        private void CreateCheckmark()
        {
            var path = new UIBezierPath();
            var val = _size / 5;

            path.MoveTo(new CGPoint(Frame.GetMidX() - val, Frame.GetMidY()));
            path.AddLineTo(new CGPoint(Frame.GetMidX() + 13 - val, Frame.GetMidY() + 15));
            path.AddLineTo(new CGPoint(Frame.GetMidX() + 45 - val, Frame.GetMidY() - 15));

            var shapeLayer = new CAShapeLayer
            {
                FillColor = UIColor.Clear.CGColor,
                StrokeColor = _lineColor,
                LineWidth = _lineWidth,
                Path = path.CGPath
            };
            var animation = new CABasicAnimation
            {
                KeyPath = "strokeEnd",
                From = NSNumber.FromDouble(0),
                Duration = (_duration / 2),
            };
            shapeLayer.AddAnimation(animation, "MyAnimation");
            Layer.AddSublayer(shapeLayer);
        }

        private void CreateCircle()
        {
            var rectShape = new CAShapeLayer
            {
                Bounds = new CGRect(0, 0, _size, _size),
                Position = new CGPoint(Frame.GetMidX(), Frame.GetMidY()),
                CornerRadius = _size / 2,

                Path = UIBezierPath.FromOval(new CGRect(0, 0, _size, _size)).CGPath,
                LineWidth = _lineWidth,
                StrokeColor = _lineColor,
                FillColor = UIColor.Clear.CGColor,
                StrokeStart = 0,
                StrokeEnd = 0
            };

            var easeOut = CAMediaTimingFunction.FromName(CAMediaTimingFunction.EaseOut);

            var start = new CABasicAnimation
            {
                KeyPath = "strokeStart",
                To = NSNumber.FromDouble(0)
            };

            var end = new CABasicAnimation
            {
                KeyPath = "strokeEnd",
                To = NSNumber.FromDouble(1)
            };

            var group = new CAAnimationGroup
            {
                Animations = new CAAnimation[] { end, start },
                Duration = _duration,
                AutoReverses = false,
                RepeatCount = 0,
                TimingFunction = easeOut,
                FillMode = CAFillMode.Both,
                RemovedOnCompletion = false
            };

            rectShape.AddAnimation(group, null);
            Layer.AddSublayer(rectShape);
        }
    }
}