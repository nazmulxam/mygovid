using System;
using System.ComponentModel;
using Foundation;
using myGovID.iOS.Constants;
using UIKit;

namespace myGovID.iOS.Views.Common
{
    /// <summary>
    /// Custom button type. These button types are supported.
    /// </summary>
    public enum CustomButtonType
    {
        Intro, Footer, Link, FooterLink, WhiteFooterLink, Popup, SexButton, AddNameField
    }

    [Register("UIButtonTypeAware"), DesignTimeVisible(true)]
    public class UIButtonTypeAware : UIButton
    {
        private string _internalType = "link";
        private bool _appliedStyle = false;

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            if (_appliedStyle)
            {
                return;
            }
            _appliedStyle = TitleLabel != null;

        }

        public void IsButtonSelected(bool isSelected)
        {
            if (isSelected)
            {
                Layer.BorderWidth = 1;
                SetTitleColor(ColorsHelper.BlueButtonColor, UIControlState.Normal);
                BackgroundColor = ColorsHelper.WhiteColor;
                Layer.BorderColor = ColorsHelper.BlueButtonColor.CGColor;
            }
            else
            {
                Layer.BorderWidth = 0.5f;
                SetTitleColor(ColorsHelper.DescriptionLabelColor, UIControlState.Normal);
                BackgroundColor = ColorsHelper.SexButtonUnselectedBackgroundColor;
                Layer.BorderColor = ColorsHelper.TextFieldTextColor.CGColor;
            }
        }

        [Export("type"), Browsable(true)]
        public String Type
        {
            get { return _internalType; }
            set
            {
                _internalType = value;
                SetupView();
            }
        }

        public void SetupView()
        {
            var buttonType = UIViewEnumParse.SafeParse<CustomButtonType>(_internalType);

            if (buttonType != CustomButtonType.Link
                && buttonType != CustomButtonType.FooterLink
                && buttonType != CustomButtonType.WhiteFooterLink
                && buttonType != CustomButtonType.AddNameField)
            {
                HeightAnchor.ConstraintEqualTo(GetHeight(buttonType)).Active = true;
            }

            BackgroundColor = GetBackgroundColor(buttonType);

            var newFontSize = (int)GetFontSize(buttonType);
            TitleLabel.Font = UIFont.FromName(GetFontStyle(buttonType), newFontSize);
            SetTitleColor(GetTextColor(buttonType), UIControlState.Normal);
            SetTitleColor(GetTextColor(buttonType), UIControlState.Selected);
            SetTitleColor(GetTextColor(buttonType), UIControlState.Highlighted);

            if (buttonType == CustomButtonType.SexButton)
            {
                Layer.BorderWidth = 0.5f;
            }
        }

        private int GetHeight(CustomButtonType buttonType)
        {
            switch (buttonType)
            {
                case CustomButtonType.Intro:
                    return 72;
                case CustomButtonType.Footer:
                    return 50;
                case CustomButtonType.SexButton:
                    return 45;
                default:
                    return 0; // default
            }
        }

        private UIColor GetBackgroundColor(CustomButtonType buttonType)
        {

            switch (buttonType)
            {
                case CustomButtonType.Intro:
                case CustomButtonType.Footer:
                case CustomButtonType.Popup:
                    return ColorsHelper.BlueButtonColor;
                case CustomButtonType.Link:
                case CustomButtonType.FooterLink:
                case CustomButtonType.WhiteFooterLink:
                case CustomButtonType.AddNameField:
                    return UIColor.Clear;
                case CustomButtonType.SexButton:
                    return ColorsHelper.SexButtonUnselectedBackgroundColor;
                default:
                    return UIColor.Clear;
            }
        }

        private UIColor GetTextColor(CustomButtonType buttonType)
        {
            switch (buttonType)
            {
                case CustomButtonType.Intro:
                case CustomButtonType.Footer:
                case CustomButtonType.WhiteFooterLink:
                case CustomButtonType.Popup:
                    return ColorsHelper.WhiteColor;
                case CustomButtonType.Link:
                case CustomButtonType.AddNameField:
                    return ColorsHelper.LinkButtonColor;
                case CustomButtonType.FooterLink:
                    return ColorsHelper.FooterLinkButtonColor;
                case CustomButtonType.SexButton:
                    return ColorsHelper.DescriptionLabelColor;
                default:
                    return UIColor.Clear;
            }
        }
        private FontSize GetFontSize(CustomButtonType buttonType)
        {
            switch (buttonType)
            {
                case CustomButtonType.Intro:
                case CustomButtonType.WhiteFooterLink:
                case CustomButtonType.Footer:
                case CustomButtonType.FooterLink:
                case CustomButtonType.Popup:
                    return FontSize.Medium;

                case CustomButtonType.Link:
                    return FontSize.Medium;
                case CustomButtonType.SexButton:
                    return FontSize.Small;
                case CustomButtonType.AddNameField:
                    return FontSize.VSmall;
                default:
                    return FontSize.Medium;
            }
        }

        private string GetFontStyle(CustomButtonType buttonType)
        {
            switch (buttonType)
            {
                case CustomButtonType.Intro: return FontType.Bold;
                case CustomButtonType.Footer: return FontType.Bold;
                case CustomButtonType.Link: return FontType.UltraLight;
                case CustomButtonType.FooterLink: return FontType.UltraLight;
                case CustomButtonType.Popup: return FontType.Bold;
                case CustomButtonType.WhiteFooterLink: return FontType.UltraLight;
                case CustomButtonType.SexButton: return FontType.UltraLight;
                case CustomButtonType.AddNameField: return FontType.UltraLight;
                default: return FontType.Medium;
            }
        }

        public UIButtonTypeAware(IntPtr p) : base(p)
        {
        }

    }

}
