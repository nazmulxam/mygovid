﻿using System;
using Foundation;
using UIKit;

namespace myGovID.iOS.Views.Common
{
    public class ColorsHelper
    {

        public readonly static UIColor BackgroundColor = new UIColor(red: 0, green: 0.14f, blue: 0.25f, alpha: 1);
        public readonly static UIColor BlueButtonColor = new UIColor(red: 0.05f, green: 0.51f, blue: 0.53f, alpha: 1.0f);
        public readonly static UIColor IdentityDocumentsSeparatorColor = new UIColor(red: 198, green: 198, blue: 198, alpha: 1);
        public readonly static UIColor LinkButtonColor = new UIColor(red: 0.05f, green: 0.51f, blue: 0.53f, alpha: 1);
        public readonly static UIColor FooterLinkButtonColor = UIColor.Black;
        public readonly static UIColor NormalLabelColor = UIColor.Black;
        public readonly static UIColor ErrorLabelColor = new UIColor(red: 0.62f, green: 0.09f, blue: 0.25f, alpha: 1);
        public readonly static UIColor DescriptionLabelColor = UIColor.DarkGray;
        public readonly static UIColor ConsentLabelColor = new UIColor(red: 0.4f, green: 0.4f, blue: 0.4f, alpha: 1);
        public readonly static UIColor SexButtonUnselectedTextColor = new UIColor(red: 0.4f, green: 0.4f, blue: 0.4f, alpha: 1);
        public readonly static UIColor SexButtonUnselectedBackgroundColor = new UIColor(red: 0.96f, green: 0.96f, blue: 0.96f, alpha: 1);
        public readonly static UIColor RadioButtonBorderColor = new UIColor(red:153, green:153,blue:153, alpha:1);
        public readonly static UIColor WhiteColor = UIColor.White;
        public readonly static UIColor TextFieldTextColor = UIColor.LightGray;
        public readonly static UIColor VerificationCodeBorderColor = new UIColor(red: 151, green: 151, blue: 151, alpha: 1);
        public readonly static UIColor MutableTextLinkColor = UIColor.White;
        public readonly static UIColor PopupBackgroundColor = UIColor.Black.ColorWithAlpha(0.6f);
        public readonly static UIColor HeaderColor = UIColor.Clear;
        public readonly static UIColor LoginTextfieldPlaceholderColor = new UIColor(red: 183, green: 183, blue: 183, alpha: 1);
        public readonly static UIColor DashboardItemColorLight = new UIColor(red: 66, green: 199, blue: 218, alpha: 1);
        public readonly static UIColor DashboardItemColorDark = new UIColor(red: 30, green: 81, blue: 120, alpha: 1);
        public readonly static UIColor TopNavHorizontalLine = new UIColor(red: 0.74f, green: 0.73f, blue: 0.76f, alpha: 1);
        public readonly static UIColor PageControlIndicatorEnabled = new UIColor(red: 0.05f, green: 0.51f, blue: 0.53f, alpha: 1);
        public readonly static UIColor PageControlIndicatorDisabled = new UIColor(red: 0.83f, green: 0.83f, blue: 0.83f, alpha: 1);
        public readonly static UIColor TabBarTitleColor = new UIColor(red: 0, green: 0.14f, blue: 0.25f, alpha: 1);
        public readonly static UIColor DocumentsVerifiedMessageViewColor = new UIColor(red: 0.08f, green: 0.54f, blue: 0.01f, alpha: 1);
        public readonly static UIColor DashboardSubScreensBackgroundColor = new UIColor(red: 0.93f, green: 0.93f, blue: 0.93f, alpha: 1);
        public readonly static UIColor InfoPopupBackgroundColor = UIColor.DarkGray;
        public readonly static UIColor CheckMarkAnimBackground = UIColor.Black.ColorWithAlpha(0.2f);
        public readonly static UIColor LivenessTargetColor = new UIColor(red: 28, green: 125, blue: 126, alpha: 1);
        public readonly static UIColor LivenessProgressColor = new UIColor(red: 45, green: 227, blue: 229, alpha: 1);
        public readonly static UIColor LivenessTargetRing1Color = new UIColor(red: 1, green: 1, blue: 1, alpha: 0.3f);
        public readonly static UIColor LivenessTargetRing2Color = new UIColor(red: 1, green: 1, blue: 1, alpha: 0.05f);
        public readonly static UIColor DashboardTabButtonBackgroundColor = new UIColor(red: 0.96f, green: 0.96f, blue: 0.96f, alpha: 1);
        public readonly static UIColor DashboardTabButtonBorderColor = UIColor.LightGray;
        public readonly static UIColor DocumentListItemBorderColor = UIColor.LightGray;
    }
}
