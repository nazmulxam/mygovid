﻿using System;
using System.ComponentModel;
using Foundation;
using myGovID.iOS.Constants;
using UIKit;
namespace myGovID.iOS.Views.Common
{
    [Register("RadioButton"), DesignTimeVisible(true)]
    public class RadioButton : UIButton
    {
        public RadioButton(IntPtr p) : base(p)
        {
            SetImage(UIImage.FromBundle(Images.CheckboxOn), UIControlState.Selected);
            SetImage(UIImage.FromBundle(Images.CheckboxOff), UIControlState.Normal);

            TouchUpInside += (sender, args) =>
            {
                Selected = !Selected;
            };
        }
    }
}
