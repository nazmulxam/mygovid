﻿using System;
namespace myGovID.iOS.Views.Common
{
    public enum FontSize
    {
        XLarge = 34,
        VVLarge = 28,
        VLarge = 24,
        Large = 23,
        Medium = 17,
        Small = 15,
        Smaller = 14,
        VSmall = 12,
        VVSmall = 10
    }
}
