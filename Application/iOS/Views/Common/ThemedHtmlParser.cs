﻿using System;
using Foundation;
using myGovID.iOS.Constants;
using myGovID.iOS.Extensions;
using UIKit;

namespace myGovID.iOS.Views.Common
{
    /// <summary>
    /// Helper class to customise rendering html into an
    /// attributed string using the app's theme.
    /// </summary>
    public class ThemedHtmlParser
    {
        private enum Style
        {
            Body, Bold, Link
        }

        // Sensible defaults
        private string _baseFontType = FontType.UltraLight;
        private FontSize _baseFontSize = FontSize.Medium;
        private UIColor _baseFontColor = UIColor.Black;

        private string _boldFontType = FontType.Bold;

        private string _linkFontType; // Defaults to base font
        private bool _linkUnderline = true;
        private UIColor _linkColor = UIColor.Blue;

        // parsed values
        private UIFont _baseFont;
        private UIFont _boldFont;
        private UIFont _linkFont;
        private NSUnderlineStyle _linkUnderlineStyle;
        private NSMutableAttributedString _result;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:myGovID.iOS.Views.Common.ThemedHtmlParser"/> class.
        /// </summary>
        /// <param name="htmlString">Html string.</param>
        public ThemedHtmlParser(string htmlString)
        {
            NSError error = null;
            var htmlAttributes = new NSAttributedStringDocumentAttributes
            {
                DocumentType = NSDocumentType.HTML
            };

            var attrString = new NSAttributedString(
                NSData.FromString(htmlString),
                htmlAttributes,
                ref error);

            if (error != null)
            {
                throw new ArgumentException($"Error parsing html: {error.Code} - {error.LocalizedDescription}", nameof(htmlString));
            }

            _result = new NSMutableAttributedString(attrString);
        }

        #region Styling methods

        /// <summary>
        /// Sets the font for anchor elements in the html. If this is not set
        /// the parser will use the body font.
        /// </summary>
        /// <returns>The parser for chaining.</returns>
        /// <param name="fontType">Font type. Use values from <see cref="FontType"/>.</param>
        public ThemedHtmlParser LinkFont(string fontType)
        {
            _linkFontType = fontType;
            return this;
        }

        /// <summary>
        /// Whether anchor elements in the html are underlined.
        /// </summary>
        /// <returns>The parser for chaining.</returns>
        /// <param name="underline">If set to <c>true</c> underline.</param>
        public ThemedHtmlParser LinkUnderline(bool underline)
        {
            _linkUnderline = underline;
            return this;
        }

        /// <summary>
        /// The color for anchor elements in the html.
        /// </summary>
        /// <returns>The parser for chaining.</returns>
        /// <param name="color">Color.</param>
        public ThemedHtmlParser LinkColor(UIColor color)
        {
            _linkColor = color;
            return this;
        }

        /// <summary>
        /// The font size to use for the base body text.
        /// </summary>
        /// <returns>The parser for chaining.</returns>
        /// <param name="size">Size.</param>
        public ThemedHtmlParser BodyFontSize(FontSize size)
        {
            _baseFontSize = size;
            return this;
        }


        /// <summary>
        /// The font to use for the base body text.
        /// </summary>
        /// <returns>The parser for chaining.</returns>
        /// <param name="fontType">Font type. Use values from <see cref="FontType"/>.</param>
        public ThemedHtmlParser BodyFont(string fontType)
        {
            _baseFontType = fontType;
            return this;
        }

        /// <summary>
        /// The color to use for the base body text.
        /// </summary>
        /// <returns>The parser for chaining.</returns>
        /// <param name="color">Color.</param>
        public ThemedHtmlParser BodyFontColor(UIColor color)
        {
            _baseFontColor = color;
            return this;
        }

        /// <summary>
        /// The font to use for the bold text.
        /// </summary>
        /// <returns>The parser for chaining.</returns>
        /// <param name="fontType">Font type. Use values from <see cref="FontType"/>.</param>
        public ThemedHtmlParser BoldFont(string fontType)
        {
            _boldFontType = fontType;
            return this;
        }

        #endregion

        #region Terminating methods

        /// <summary>
        /// Generates an attributed string from the html and with styles applied.
        /// </summary>
        /// <returns>The attributed string.</returns>
        public NSAttributedString ToAttributedString()
        {
            ProcessStyles();

            _result.EnumerateAttributedSubstrings(StyleAttributedSubstring);

            return new NSAttributedString(_result);
        }

        /// <summary>
        /// Applies the html text and styles to a UITextView which needs some
        /// more properties set to correctly render the styled anchor tags.
        /// </summary>
        /// <param name="textView">Text view.</param>
        public void ApplyToTextView(UITextView textView)
        {
            textView.AttributedText = ToAttributedString();
            textView.WeakLinkTextAttributes = GetAttributesForStyle(Style.Link);
            textView.TintColor = _linkColor;
        }

        /// <summary>
        /// Applies the html text and styles to a UILabel for which we also
        /// override the line spacing.
        /// </summary>
        /// <param name="label">Label.</param>
        public void ApplyToLabel(UILabel label)
        {
            var paragraphStyle = new NSMutableParagraphStyle
            {
                LineSpacing = 4,
                Alignment = label.TextAlignment
            };

            var attrString = new NSMutableAttributedString(ToAttributedString());
            attrString.AddAttribute(
                UIStringAttributeKey.ParagraphStyle,
                paragraphStyle,
                new NSRange(0, attrString.Length));

            label.AttributedText = attrString;
        }

        #endregion


        /// <summary>
        /// Returns an NSDictionary of string attributes for the given style.
        /// </summary>
        /// <returns>The attributes for the style.</returns>
        /// <param name="style">Style.</param>
        private NSDictionary GetAttributesForStyle(Style style)
        {
            ProcessStyles();

            var attributes = new NSMutableDictionary();
            switch (style)
            {
                case Style.Body:
                    attributes[UIStringAttributeKey.Font] = _baseFont;
                    attributes[UIStringAttributeKey.ForegroundColor] = _baseFontColor;
                    break;
                case Style.Bold:
                    attributes[UIStringAttributeKey.Font] = _boldFont;
                    attributes[UIStringAttributeKey.ForegroundColor] = _baseFontColor;
                    break;
                case Style.Link:
                    attributes[UIStringAttributeKey.Font] = _linkFont;
                    attributes[UIStringAttributeKey.ForegroundColor] = _linkColor;
                    attributes[UIStringAttributeKey.UnderlineColor] = _linkColor;
                    attributes[UIStringAttributeKey.UnderlineStyle] = NSNumber.FromInt32((int)_linkUnderlineStyle);
                    break;
            }

            return new NSDictionary(attributes);
        }

        /// <summary>
        /// Detects a theme style from the given string attributes.
        /// </summary>
        /// <returns>The style from attributes.</returns>
        /// <param name="attributes">Attributes.</param>
        private Style GetStyleFromAttributes(NSDictionary attributes)
        {
            Style style = Style.Body;
            if (attributes.ContainsKey(UIStringAttributeKey.Link))
            {
                style = Style.Link;
            }
            else if (attributes.ContainsKey(UIStringAttributeKey.Font))
            {
                var font = (UIFont)attributes[UIStringAttributeKey.Font];
                var traits = font.FontDescriptor.Traits.SymbolicTrait ?? UIFontDescriptorSymbolicTraits.ClassUnknown;
                var hasBold = (traits & UIFontDescriptorSymbolicTraits.Bold) != 0;
                if (hasBold)
                {
                    style = Style.Bold;
                }
            }

            return style;
        }

        private void StyleAttributedSubstring(NSDictionary attributes, NSRange range)
        {
            var style = GetStyleFromAttributes(attributes);
            var newAttributes = GetAttributesForStyle(style);
            _result.AddAttributes(newAttributes, range);
        }

        private void ProcessStyles()
        {
            _baseFont = UIFont.FromName(_baseFontType, (int)_baseFontSize);
            _boldFont = UIFont.FromName(_boldFontType, (int)_baseFontSize);
            _linkFont = _linkFontType != null
                ? UIFont.FromName(_linkFontType, (int)_baseFontSize)
                : _baseFont;
            _linkUnderlineStyle = _linkUnderline ? NSUnderlineStyle.Single : NSUnderlineStyle.None;
        }
    }
}
