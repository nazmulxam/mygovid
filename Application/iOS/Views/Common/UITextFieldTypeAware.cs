﻿using System;
using System.ComponentModel;
using CoreGraphics;
using Foundation;
using myGovID.iOS.Constants;
using myGovID.Presentation.Contract;
using UIKit;

namespace myGovID.iOS.Views.Common
{
    /// <summary>
    /// Custom TextField type. These TextField types are supported.
    /// </summary>
    public enum TextFieldType
    {
        Normal, DropDown, Login, Info, Hidden, NoBorder
    }

    [Register("UITextFieldTypeAware"), DesignTimeVisible(true)]
    public class UITextFieldTypeAware : UITextFieldWithBinding
    {
        private string _internalType = "normal";
        private bool _appliedStyle = false;
        private string _internalFontSize = "small";
        private string _internalFontStyle = "thin";
        private UIButton _rightViewButton;
        private bool _isTransparentTextField = false;

        [Export("type"), Browsable(true)]
        public String Type
        {
            get { return _internalType; }
            set
            {
                _internalType = value;
                SetupView();
            }
        }

        [Export("fontSize"), Browsable(true)]
        public String TextFontSize
        {
            get { return _internalFontSize; }
            set
            {
                _internalFontSize = value;
                SetupView();
            }
        }

        [Export("fontStyle"), Browsable(true)]
        public String FontStyle
        {
            get { return _internalFontStyle; }
            set
            {
                _internalFontStyle = value;
                SetupView();
            }
        }

        public new UIReturnKeyType ReturnKeyType { get; set; } = UIReturnKeyType.Default;
        public new UITextFieldCondition ShouldReturn { get; set; } = (textfield) =>
        {
            textfield.ResignFirstResponder();
            return true;
        };

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            if (_appliedStyle)
            {
                return;
            }
            SetupView();
        }
        public void SetupView()
        {
            if (_rightViewButton == null)
            {
                _rightViewButton = new UIButton(UIButtonType.Custom);
            }
            if (!_isTransparentTextField)
            {

                var textFieldType = UIViewEnumParse.SafeParse<TextFieldType>(_internalType);
                var newFontSize = UIViewEnumParse.SafeParse<FontSize>(_internalFontSize);
                Font = UIFont.FromName(GetFontStyle(), (int)newFontSize);
                TextColor = GetTextColor();
                if (textFieldType != TextFieldType.Hidden)
                {
                    if (textFieldType == TextFieldType.Login)
                    {
                        SetLoginFieldBorder();

                    }
                    else
                    {
                        if (textFieldType != TextFieldType.NoBorder)
                        {
                            SetSquareBorder();
                        }
                    }
                    ChangePlaceholderColor(ColorsHelper.TextFieldTextColor);
                    // This has height needs to be calculated dynamically, it will be raised as a tech debt.
                    HeightAnchor.ConstraintEqualTo(constant: 45).Active = true;
                    Alpha = 1;
                }
                else
                {
                    HeightAnchor.ConstraintEqualTo(constant: 0).Active = true;
                    Alpha = 0;
                }
            }

            base.ReturnKeyType = this.ReturnKeyType;
            base.ShouldReturn = this.ShouldReturn;
        }


        private FontSize GetFontSize(TextFieldType fieldType)
        {
            switch (fieldType)
            {
                case TextFieldType.Login:
                    return FontSize.VLarge;
                default:
                    return FontSize.Medium;
            }
        }
        private string GetFontStyle()
        {
            return FontType.Medium;
        }
        private UIColor GetTextColor()
        {
            return ColorsHelper.ConsentLabelColor;
        }
        private void SetSquareBorder()
        {
            BorderStyle = UITextBorderStyle.None;
            Layer.CornerRadius = 0;
            Layer.MasksToBounds = true;
            Layer.BorderColor = UIColor.LightGray.CGColor;
            Layer.BorderWidth = 1.0f;
        }
        private void SetLoginFieldBorder()
        {
            Layer.BackgroundColor = ColorsHelper.WhiteColor.CGColor;
        }
        private void ChangePlaceholderColor(UIColor color)
        {
            AttributedPlaceholder = new NSAttributedString(Placeholder ?? "",
                                                           foregroundColor: color);
        }
        private UIEdgeInsets _padding = new UIEdgeInsets(0, 10, 0, 10);
        public override CGRect TextRect(CGRect forBounds)
        {
            return base.TextRect(_padding.InsetRect(forBounds)); ;
        }
        public override CGRect EditingRect(CGRect forBounds)
        {
            return base.EditingRect(_padding.InsetRect(forBounds));
        }
        private void SetRightViewIcon(string type)
        {
            _rightViewButton.ImageEdgeInsets = new UIEdgeInsets(0, -16, 0, 0);
            _rightViewButton.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
            _rightViewButton.AccessibilityIdentifier = "rightButton";
            var textFieldType = UIViewEnumParse.SafeParse<TextFieldType>(_internalFontSize);
            switch (textFieldType)
            {
                case TextFieldType.DropDown:
                    var size = 12.0f;
                    _rightViewButton.Frame = new CGRect(Frame.Size.Width - size, 5.0, size, size);
                    _rightViewButton.UserInteractionEnabled = false;
                    _rightViewButton.SetImage(UIImage.FromBundle(Images.DropDown), UIControlState.Normal);
                    break;
                case TextFieldType.Info:
                    var infoSize = 22.0f;
                    _rightViewButton.Frame = new CGRect(Frame.Size.Width - infoSize, 5.0, infoSize, infoSize);
                    _rightViewButton.UserInteractionEnabled = false;
                    _rightViewButton.SetImage(UIImage.FromBundle(Images.DropDown), UIControlState.Normal);
                    break;

            }
            RightViewMode = UITextFieldViewMode.Always;
            RightView = _rightViewButton;
        }

        public UITextFieldTypeAware(IntPtr p) : base(p)
        {
        }

        public UITextFieldTypeAware(CGRect frame, bool isTransparent) : base(frame)
        {
            _isTransparentTextField = isTransparent;
        }
    }
}
