﻿using System;
using System.ComponentModel;
using Foundation;
using myGovID.iOS.Constants;
using UIKit;

namespace myGovID.iOS.Views.Common
{
    public static class UIViewEnumParse
    {
        public static T SafeParse<T>(string value) where T : struct
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentException(string.Format("Attempted to parse missing value for type {0}", typeof(T).Name), nameof(value));
            }
            T parsedValue;
            if (Enum.TryParse<T>(value, true, out parsedValue))
            {
                return parsedValue;
            }
            else
            {
                throw new ArgumentException(string.Format("Attempted to parse incorrect value of {1} for type {0}", typeof(T).Name, value), nameof(value));
            }
        }
    }
    /// <summary>
    /// Custom Label type. These Label types are supported.
    /// </summary>
    public enum LabelType
    {
        Normal, NormalWhite, Error, ErrorLeft, Description, Consent
    }

    [Register("UILabelTypeAware"), DesignTimeVisible(true)]
    public class UILabelTypeAware : UILabel
    {
        private string _internalFontSize = "Small";
        private string _internalFontStyle = "Thin";
        private string _internalType = "Normal";

        [Export("fontSize"), Browsable(true)]
        public String LabelFontSize
        {
            get { return _internalFontSize; }
            set
            {
                _internalFontSize = value;
                SetupView();
            }
        }

        [Export("fontStyle"), Browsable(true)]
        public String FontStyle
        {
            get { return _internalFontStyle; }
            set
            {
                _internalFontStyle = value;
                SetupView();
            }
        }

        [Export("type"), Browsable(true)]
        public String Type
        {
            get { return _internalType; }
            set
            {
                _internalType = value;
                SetupView();
            }
        }

        public override string Text
        {
            get => base.Text;
            set
            {
                base.Text = value;
                var fontSize = UIViewEnumParse.SafeParse<FontSize>(_internalFontSize);
                this.SetLineSpacing(font: GetFontName(FontStyle), size: (int)fontSize);
            }
        }

        public void SetupView()
        {
            var labelType = UIViewEnumParse.SafeParse<LabelType>(_internalType);
            if (labelType == LabelType.Error)
            {
                Font = UIFont.FromName(GetFontName(fontStyle: "BOLD"), size: (int)FontSize.Small);
                TextColor = ColorsHelper.ErrorLabelColor;
                if (labelType == LabelType.ErrorLeft)
                {
                    TextAlignment = UITextAlignment.Left;
                }
                else
                {
                    TextAlignment = UITextAlignment.Right;
                }
                LineBreakMode = UILineBreakMode.WordWrap;
                Lines = 0;
            }
            else
            {
                var fontSize = UIViewEnumParse.SafeParse<FontSize>(_internalFontSize);
                Font = UIFont.FromName(GetFontName(_internalFontStyle), (int)fontSize);
                TextColor = GetTextColor(labelType);
            }
        }
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            SetupView();
        }

        private UIColor GetTextColor(LabelType labelType)
        {
            switch (labelType)
            {
                case LabelType.Normal:
                    return ColorsHelper.NormalLabelColor;
                case LabelType.NormalWhite:
                    return ColorsHelper.WhiteColor;
                case LabelType.Error:
                case LabelType.ErrorLeft:
                    return ColorsHelper.ErrorLabelColor;
                case LabelType.Description:
                    return ColorsHelper.DescriptionLabelColor;
                case LabelType.Consent:
                    return ColorsHelper.ConsentLabelColor;
                default:
                    return UIColor.Clear;
            }
        }

        private string GetFontName(string fontStyle)
        {
            switch (fontStyle.ToUpper())
            {
                case "BOLD": return FontType.Bold;
                case "MEDIUM": return FontType.Medium;
                default: return FontType.UltraLight;
            }
        }

        public UILabelTypeAware(IntPtr p) : base(p)
        {
        }
    }

    public static class UILabelExtensions
    {
        public static void SetLineSpacing(this UILabel label, string font, float size)
        {
            // Don't try to set line spacing if there is no value
            if (string.IsNullOrEmpty(label.Text))
            {
                label.AttributedText = new NSAttributedString();
                return;
            }
            var labelText = label.Text;
            var textString = new NSMutableAttributedString(str: labelText, attributes: new UIStringAttributes { Font = UIFont.FromName(font, size) });
            var textRange = new NSRange(start: 0, len: textString.Length);
            var paragraphStyle = new NSMutableParagraphStyle();
            paragraphStyle.LineSpacing = 4;
            paragraphStyle.Alignment = label.TextAlignment;
            UIStringAttributes stringAttributes = new UIStringAttributes
            {
                ParagraphStyle = paragraphStyle
            };
            textString.AddAttributes(attrs: stringAttributes, range: textRange);
            label.AttributedText = textString;
        }

        public static void SetCharacterSpacing(this UILabel label)
        {
            var labelText = label.Text;
            if (!string.IsNullOrEmpty(labelText))
            {
                var textString = new NSMutableAttributedString(str: labelText);
                UIStringAttributes stringAttributes = new UIStringAttributes
                {
                    KerningAdjustment = 7,
                };
                textString.AddAttributes(stringAttributes, new NSRange(start: 0, len: textString.Length - 1));
                label.AttributedText = textString;
            }
        }
    }
}