﻿using System;
using System.ComponentModel;
using CoreGraphics;
using Foundation;
using myGovID.Presentation.Contract;
using UIKit;

namespace myGovID.iOS.Views.Common
{
    [Register("UIDateTextField"), DesignTimeVisible(true)]
    public class UIDateTextField : UITextFieldTypeAware
    {
        private UIDatePicker _datePickerView;
        public UIDateTextField(IntPtr p) : base(p)
        {
            _datePickerView = new UIDatePicker
            {
                Mode = UIDatePickerMode.Date
            };

            EditingDidBegin += (sender, e) =>
            {
                LaunchDatePicker();
            };

            _datePickerView.ValueChanged += (pickerSender, args) =>
            {
                Text = ((DateTime)_datePickerView.Date).ToString("dd/MM/yyyy");
                SendActionForControlEvents(UIControlEvent.EditingChanged);
            };
        }

        private void LaunchDatePicker()
        {

            InputView = _datePickerView;
            Text = ((DateTime)_datePickerView.Date).ToString("dd/MM/yyyy");

        }

        public UIDateTextField(CGRect frame, bool isTransparent) : base(frame, isTransparent)
        {
        }
    }
}
