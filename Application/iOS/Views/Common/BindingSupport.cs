﻿using System;
using System.Collections.Generic;
using GalaSoft.MvvmLight.Helpers;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Inputs;
using UIKit;

namespace myGovID.iOS.Views.Common
{
    public class BindingSupport
    {
        /// <summary>
        /// Start a binding chain. Terminate using the GetBindings() method
        /// </summary>
        /// <returns>BindingSupport for chaining</returns>
        public static BindingSupport Bind()
        {
            return new BindingSupport();
        }

        protected ICollection<Binding> _bindings = new List<Binding>();
        protected BindingSupport()
        {
        }

        /// <summary>
        /// One way bind from the IControl Visibility to the UIView's Hidden property
        /// </summary>
        /// <returns>BindingSupport for chaining</returns>
        /// <param name="control">source</param>
        /// <param name="view">target</param>
        public BindingSupport Visibility(IControl control, UIView view)
        {
            view.Hidden = !control.Visible;
            var visibleBinding = new Binding<bool, bool>(control, () => control.Visible)
                .WhenSourceChanges(() => { view.Hidden = !control.Visible; });
            AddBinding(visibleBinding);
            return this;
        }

        /// <summary>
        /// One way bind from the IControl AccessibilityIdentifier to the UIView
        /// </summary>
        /// <returns>BindingSupport for chaining</returns>
        /// <param name="control">source</param>
        /// <param name="view">target</param>
        /// <param name="accesibilityOverride">override accessibility identifier</param>
        /// <param name="suffix">the suffix to apply to the identifier</param>
        public BindingSupport AccessibilityId(IControl control, UIView view, string accesibilityOverride = null, string suffix = "")
        {
            if (accesibilityOverride != null)
            {
                view.AccessibilityIdentifier = $"{accesibilityOverride}{suffix}";
            }
            else
            {
                view.AccessibilityIdentifier = $"{control.AccessibilityIdentifier}{suffix}";
                var accessibilityIdentifierBinding = new Binding<string, string>(control, () => control.AccessibilityIdentifier)
                    .WhenSourceChanges(() => {
                        view.AccessibilityIdentifier = $"{control.AccessibilityIdentifier}{suffix}";
                    });
                AddBinding(accessibilityIdentifierBinding);
            }
            return this;
        }

        /// <summary>
        /// One way bind from the IAction AccessibilityIdentifier to the UIView
        /// </summary>
        /// <returns>BindingSupport for chaining</returns>
        /// <param name="action">source</param>
        /// <param name="view">target</param>
        /// <param name="accesibilityOverride">override accessibility identifier</param>
        /// <param name="suffix">the suffix to apply to the identifier</param>
        public BindingSupport AccessibilityId<T>(IAction<ActionType, T> action, UIView view, string accesibilityOverride = null, string suffix = "")
        {
            if (accesibilityOverride != null)
            {
                view.AccessibilityIdentifier = $"{accesibilityOverride}{suffix}";
            }
            else
            {
                view.AccessibilityIdentifier = $"{action.AccessibilityIdentifier}{suffix}";
                var accessibilityIdentifierBinding = new Binding<string, string>(action, () => action.AccessibilityIdentifier)
                    .WhenSourceChanges(() => {
                        view.AccessibilityIdentifier = $"{action.AccessibilityIdentifier}{suffix}";
                    });
                AddBinding(accessibilityIdentifierBinding);
            }
            return this;
        }

        /// <summary>
        /// One way bind from the IAction AccessibilityIdentifier to the UIBarButtonItem
        /// </summary>
        /// <returns>BindingSupport for chaining</returns>
        /// <param name="action">source</param>
        /// <param name="barButtonItem">target</param>
        /// <param name="accesibilityOverride">override accessibility identifier</param>
        /// <param name="suffix">the suffix to apply to the identifier</param>
        public BindingSupport AccessibilityId<T>(IAction<ActionType, T> action, UIBarButtonItem barButtonItem, string accesibilityOverride = null, string suffix = "")
        {
            if (accesibilityOverride != null)
            {
                barButtonItem.AccessibilityIdentifier = $"{accesibilityOverride}{suffix}";
            }
            else
            {
                barButtonItem.AccessibilityIdentifier = $"{action.AccessibilityIdentifier}{suffix}";
                var accessibilityIdentifierBinding = new Binding<string, string>(action, () => action.AccessibilityIdentifier)
                    .WhenSourceChanges(() => {
                        barButtonItem.AccessibilityIdentifier = $"{action.AccessibilityIdentifier}{suffix}";
                    });
                AddBinding(accessibilityIdentifierBinding);
            }
            return this;
        }

        /// <summary>
        /// One way bind from the IControl Label to the UILabel Text or AttributedText
        /// </summary>
        /// <returns>BindingSupport for chaining</returns>
        /// <param name="control">source</param>
        /// <param name="label">target</param>
        /// <param name="isHTML">whether to parse the label string as HTML</param>
        public BindingSupport Label(IControl control, UILabel label, bool isHTML = false)
        {
            label.Text = control.Label;
            var labelBinding = new Binding<string, string>(
                control, () => control.Label)
                .WhenSourceChanges(() => {
                    if (isHTML)
                    {
                        new ThemedHtmlParser(control.Label)
                            .ApplyToLabel(label);
                    }
                    else
                    {
                        label.Text = control.Label;
                    }
                });
            AddBinding(labelBinding);
            return this;
        }

        /// <summary>
        /// One way bind from the FormInput Placeholder to the UITextField Placeholder
        /// </summary>
        /// <returns>BindingSupport for chaining</returns>
        /// <param name="input">source</param>
        /// <param name="textField">target</param>
        public BindingSupport Placeholder(FormInput<string> input, UITextField textField)
        {
            textField.Placeholder = input.Placeholder;
            var labelBinding = new Binding<string, string>(
                input, () => input.Placeholder)
                .WhenSourceChanges(() => {
                    textField.Placeholder = input.Placeholder;
                });
            AddBinding(labelBinding);
            return this;
        }

        /// <summary>
        /// One way bind from the FormInput Enabled to the UITextField Enabled
        /// </summary>
        /// <returns>BindingSupport for chaining</returns>
        /// <param name="input">source</param>
        /// <param name="textField">target</param>
        public BindingSupport Enabled(FormInput<string> input, UITextField textField)
        {
            textField.Enabled = input.Enabled;
            var enabledBinding = new Binding<bool, bool>(
                input, () => input.Enabled)
                .WhenSourceChanges(() => {
                    textField.Enabled = input.Enabled;
                });
            AddBinding(enabledBinding);
            return this;
        }

        /// <summary>
        /// One way bind from the IControl Label to the UIButton Title for UIControlState.Normal
        /// </summary>
        /// <returns>BindingSupport for chaining</returns>
        /// <param name="control">source</param>
        /// <param name="button">target</param>
        public BindingSupport Label(IControl control, UIButton button)
        {
            button.SetTitle(control.Label, UIControlState.Normal);
            var labelBinding = new Binding<string, string>(
                control, () => control.Label)
                .WhenSourceChanges(() => {
                    button.SetTitle(control.Label, UIControlState.Normal);
                });
            AddBinding(labelBinding);
            return this;
        }

        /// <summary>
        /// Two way bind between the FormInput Value and the UITextField Text
        /// </summary>
        /// <returns>BindingSupport for chaining</returns>
        /// <param name="input">source</param>
        /// <param name="textField">target</param>
        public BindingSupport Value(FormInput<string> input, UITextField textField)
        {
            textField.Text = input.Value;
            var valueBinding = new Binding<string, string>(
                textField, () => textField.Text,
                input, () => input.Value,
                BindingMode.TwoWay)
                .ObserveSourceEvent(nameof(UITextField.EditingChanged));

            AddBinding(valueBinding);

            return this;
        }

        /// <summary>
        /// Two way bind between the FormInput bool Value and the UIControl Selected
        /// via the PrimaryActionTriggered event.
        /// </summary>
        /// <returns>BindingSupport for chaining</returns>
        /// <param name="input">source</param>
        /// <param name="control">target</param>
        public BindingSupport Value(FormInput<bool> input, UIControl control)
        {
            control.Selected = input.Value;
            control.ValueChanged += (s, e) => { }; // To fool the linker.
            var valueBinding = new Binding<bool, bool>(
                control, () => control.Selected,
                input, () => input.Value,
                BindingMode.TwoWay)
                .ObserveSourceEvent(nameof(UIControl.TouchUpInside));
            AddBinding(valueBinding);
            return this;
        }

        /// <summary>
        /// One way bind from the FormInput Text to the UILabel Title and Hidden
        /// </summary>
        /// <returns>BindingSupport for chaining</returns>
        /// <param name="input">source</param>
        /// <param name="errorLabel">target</param>
        public BindingSupport Error<T>(FormInput<T> input, UILabel errorLabel) where T : IComparable
        {
            errorLabel.Hidden = string.IsNullOrWhiteSpace(input.Error);
            errorLabel.Text = input.Error;
            var errorBinding = new Binding<string, string>(
                input, () => input.Error)
                .WhenSourceChanges(() =>
                {
                    errorLabel.Text = input.Error;
                    errorLabel.Hidden = string.IsNullOrWhiteSpace(input.Error);
                });
            AddBinding(errorBinding);
            return this;
        }

        /// <summary>
        /// Bind the Button control OnTapped command to the UIButton TouchUpInside event
        /// </summary>
        /// <returns>BindingSupport for chaining</returns>
        /// <param name="input">source</param>
        /// <param name="button">target</param>
        public BindingSupport OnTapped(ButtonControl input, UIButton button)
        {
            button.SetCommand(input.OnTapped);
            return this;
        }

        /// <summary>
        /// Gets all the bindings stored in this object so far.
        /// </summary>
        /// <returns>The bindings.</returns>
        public IEnumerable<Binding> GetBindings()
        {
            return _bindings;
        }

        protected void AddBinding(Binding binding)
        {
            _bindings.Add(binding);
        }
    }
}
