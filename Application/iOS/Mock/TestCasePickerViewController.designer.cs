// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace myGovID.iOS.Mock
{
    [Register ("TestCasePickerViewController")]
    partial class TestCasePickerViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch MockLivenessSwitch { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField ScenarioTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch SkipToDash { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TestcaseTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch UseBiometricsSwitch { get; set; }

        [Action ("didTapStartButton")]
        partial void DidTapStartButton ();

        void ReleaseDesignerOutlets ()
        {
            if (MockLivenessSwitch != null) {
                MockLivenessSwitch.Dispose ();
                MockLivenessSwitch = null;
            }

            if (ScenarioTextField != null) {
                ScenarioTextField.Dispose ();
                ScenarioTextField = null;
            }

            if (SkipToDash != null) {
                SkipToDash.Dispose ();
                SkipToDash = null;
            }

            if (TestcaseTextField != null) {
                TestcaseTextField.Dispose ();
                TestcaseTextField = null;
            }

            if (UseBiometricsSwitch != null) {
                UseBiometricsSwitch.Dispose ();
                UseBiometricsSwitch = null;
            }
        }
    }
}