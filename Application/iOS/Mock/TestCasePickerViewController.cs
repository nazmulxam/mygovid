﻿using System;
using System.Collections.Generic;
using System.Linq;
using myGovID.Application.Launcher.Contract;
using UIKit;
using ObjCRuntime;
using Foundation;
using myGovID.Services.Http.Mock;

namespace myGovID.iOS.Mock
{
    public partial class TestCasePickerViewController : UIViewController, IUIPickerViewDelegate
    {
        private MockFileManager mockFileManager = new MockFileManager();
        public List<String> Testcases;
        public List<String> Scenarios;
        public TestCasePickerViewController() : base("TestCasePickerViewController", null)
        {
            var testData = new List<KeyValuePair<string, string>>(mockFileManager.GetTestCases());
            testData.Sort((a, b) =>
            {
                return String.Compare(a.Key, b.Key);
            });
            Testcases = new List<String>(from testcase in testData select testcase.Key);
            Scenarios = new List<String>(from testcase in testData select testcase.Value);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            var testPickerViewModel = new TestPickerViewModel(this);

            TestcaseTextField.InputAccessoryView = BuildToolbar();
            TestcaseTextField.InputView = new UIPickerView() { Model = testPickerViewModel };
            TestcaseTextField.Text = Testcases[0];

            ScenarioTextField.InputAccessoryView = BuildToolbar();
            ScenarioTextField.InputView = new UIPickerView() { Model = testPickerViewModel };
            ScenarioTextField.Text = Scenarios[0];

            // TODO: Deal with the liveness mocking
            if (Runtime.Arch == Arch.DEVICE)
            {
                MockLivenessSwitch.On = true;
                MockLivenessSwitch.Enabled = false;
            }
        }

        partial void DidTapStartButton()
        {
            var appDelegate = (MockAppDelegate)UIApplication.SharedApplication.Delegate;
            appDelegate.RegisterDependencies(SkipToDash.On);
            appDelegate.StartTestcase(TestcaseTextField.Text, UseBiometricsSwitch.On);
        }

        private UIToolbar BuildToolbar()
        {
            var toolbar = new UIToolbar
            {
                BarStyle = UIBarStyle.Default,
                Translucent = true,
                TintColor = UIColor.Black
            };
            toolbar.SizeToFit();

            var doneButton = new UIBarButtonItem("Done", UIBarButtonItemStyle.Plain, (sender, args) =>
            {
                View.EndEditing(true);
            });

            var spaceButton = new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace);
            toolbar.SetItems(new UIBarButtonItem[] { spaceButton, doneButton }, false);
            return toolbar;
        }

        private class TestPickerViewModel : UIPickerViewModel
        {
            private TestCasePickerViewController _controller;
            public TestPickerViewModel(TestCasePickerViewController controller)
            {
                _controller = controller;

            }
            [Export("pickerView:numberOfRowsInComponent:")]
            public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
            {
                return _controller.Testcases.Count;
            }

            [Export("pickerView:titleForRow:forComponent:")]
            public override string GetTitle(UIPickerView pickerView, nint row, nint component)
            {
                if (pickerView == _controller.TestcaseTextField.InputView)
                {
                    return _controller.Testcases[(int)row];
                }
                else if (pickerView == _controller.ScenarioTextField.InputView)
                {
                    return _controller.Scenarios[(int)row];
                }
                throw new RuntimeException($"Unhandled pickerView: {pickerView}");
            }

            [Export("numberOfComponentsInPickerView:")]
            public override nint GetComponentCount(UIPickerView pickerView)
            {
                return 1;
            }

            public override void Selected(UIPickerView pickerView, nint row, nint component)
            {
                _controller.TestcaseTextField.Text = _controller.Testcases[(int)row];
                _controller.ScenarioTextField.Text = _controller.Scenarios[(int)row];
            }
        }
    }
}

