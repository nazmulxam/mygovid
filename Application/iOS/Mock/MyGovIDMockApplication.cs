﻿using UIKit;

namespace myGovID.iOS.Mock
{
    public class MyGovIDMockApplication: MyGovIDApplication
    {
        public override void SendEvent(UIEvent uievent)
        {
            MonitorEvents = !(KeyWindow.RootViewController is TestCasePickerViewController);
            base.SendEvent(uievent);
        }
    }
}
