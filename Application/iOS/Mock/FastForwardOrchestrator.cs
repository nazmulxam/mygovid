﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CoreFoundation;
using myGovID.iOS.Steps;
using myGovID.iOS.Steps.CapturePassword;
using myGovID.iOS.Views;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps;
using myGovID.Presentation.Contract.Steps.DocumentIdentitySuccess;
using myGovID.Presentation.Contract.Steps.AccountSetup;
using myGovID.Presentation.Contract.Steps.CaptureEmail;
using myGovID.Presentation.Contract.Steps.CaptureEmailVerification;
using myGovID.Presentation.Contract.Steps.CapturePassword;
using myGovID.Presentation.Contract.Steps.CapturePersonalDetails;
using myGovID.Presentation.Contract.Steps.Dashboard;
using myGovID.Presentation.Contract.Steps.OnBoarding;
using myGovID.Presentation.Contract.Steps.SecureYourAccount;
using myGovID.Presentation.Contract.Steps.TermsAndConditions;
using myGovID.Presentation.Contract.Steps.TermsAndConditionsConfirm;
using myGovID.Presentation.Contract.Steps.VersionUpgrade;
using UIKit;

namespace myGovID.iOS.Mock
{
    public class FastForwardOrchestrator : StepOrchestrator
    {
        public FastForwardOrchestrator(IStepRepository repository) : base(repository)
        {
        }

        private static bool _isInDashboard = false;

        public override (UIViewController ViewController, bool Overlay)? GetViewController(IStep newStep)
        {
            if (!_isInDashboard)
            {
                switch (newStep.ViewModel)
                {
                    case AccountSetupViewModel vm:
                        return ExecuteAction(newStep, ActionType.Next, AccountSetupAction.Register);
                    case OnBoardingViewModel vm:
                        return ExecuteAction(newStep, ActionType.Next);
                    case TermsAndConditionsViewModel vm:
                        return ExecuteAction(newStep, ActionType.Next);
                    case TermsAndConditionsConfirmViewModel vm:
                        return ExecuteAction(newStep, ActionType.Next);
                    case SecureYourAccountViewModel vm:
                        return ExecuteAction(newStep, ActionType.Next);
                    case CaptureEmailViewModel vm:
                        vm.Inputs.FirstOrDefault()?.FromString("test@email.com");
                        return ExecuteAction(newStep, ActionType.Next);
                    case CaptureEmailVerificationViewModel vm:
                        vm.Inputs.FirstOrDefault()?.FromString("111111");
                        return ExecuteAction(newStep, ActionType.Next);
                    case CapturePasswordViewModel vm:
                        foreach (var input in vm.Inputs)
                            input.FromString("Pass123456");
                        return ExecuteAction(newStep, ActionType.Next);
                    //break;
                    case CapturePersonalDetailsViewModel vm:
                        foreach (var input in vm.Inputs)
                        {
                            switch (input.MetaData)
                            {
                                case InputMetaData.GivenName: input.FromString("Shane"); break;
                                case InputMetaData.FamilyName: input.FromString("Foreman"); break;
                                case InputMetaData.DateOfBirth: input.FromString("01/01/1980"); break;
                            }
                        }
                        return ExecuteAction(newStep, ActionType.Next);
                    case EmptyViewModel vm:
                        if (newStep is IDocumentIdentitySuccessStep)
                        {
                            return ExecuteAction(newStep, ActionType.Next);
                        }
                        break;
                    case DashboardViewModel vm:
                        _isInDashboard = true;
                        break;
                }
            }
            if (newStep.ViewModel is VersionUpgradeViewModel versionVM)
            {
                if (!versionVM.ForcedUpdate)
                    return ExecuteAction(newStep, ActionType.Previous);
            }
            return base.GetViewController(newStep);
        }



        protected (UIViewController ViewController, bool Overlay)? ExecuteAction(IStep step, ActionType actionType)
        {
            return ExecuteAction<Enum>(step, actionType);
        }

        protected (UIViewController ViewController, bool Overlay)? ExecuteAction<E>(IStep step, ActionType actionType, E subType = default)
        {
            var targetAction = step.GetActions<E>().FirstOrDefault((action) =>
            {
                if (action.Type != actionType)
                {
                    return false;
                }
                if (action.SubType == null && subType == null)
                {
                    return true;
                }
                if (action.SubType != null && subType != null)
                {
                    return action.SubType.Equals(subType);
                }
                return false;
            });

            // For some reason the credential component didn't work if this
            // happened synchronously on the same thread. Using a background
            // thread seems to do the trick.
            var source = new TaskCompletionSource<IStep>();
            DispatchQueue.GetGlobalQueue(DispatchQueuePriority.Low).DispatchAsync(async () =>
            {
                source.TrySetResult(await targetAction.Run());
            });
            source.Task.Wait();

            return GetViewController(source.Task.Result);
        }
    }
}
