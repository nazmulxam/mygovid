﻿using System.Collections.Generic;
using System.Linq;
using Foundation;
using myGovID.Application.Launcher.Contract;
using myGovID.Application.Launcher.Contract.Mock;
using myGovID.Application.Launcher.Mock;
using myGovID.iOS.Configuration;
using myGovID.iOS.Steps;
using myGovID.Services.Dependency.Contract;
using myGovID.Services.Dependency.iOS;
using UIKit;

namespace myGovID.iOS.Mock
{
    public class MockAppDelegate : AppDelegate
    {
        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            base.FinishedLaunching(application, launchOptions);
            Window.RootViewController = new TestCasePickerViewController();
            return true;
        }

        public void RegisterDependencies(bool skipToDashboard)
        {
            if (skipToDashboard)
            {
                DependencyService.Shared.Register<StepOrchestrator, FastForwardOrchestrator>(Scope.Singleton);
            }
            else
            {
                base.RegisterDependencies();
            }
        }

        public void StartTestcase(string testcaseId, bool useBiometrics)
        {
            var deviceInfo = new MockDeviceInfoWrapper(new DeviceInfo())
            {
                HasBiometricsOverride = useBiometrics
            };

            MockLaunchConfiguration configuration = new MockLaunchConfiguration.Builder()
                .SetDeviceInfo(deviceInfo)
                .SetApplicationInfo(new ApplicationInfo(NSBundle.MainBundle))
                .SetTestcaseId(testcaseId)
                .SetUseBiometrics(useBiometrics)
                .SetDependencyService(DependencyService.Shared).Build();

            Launcher.Configure(configuration);
            base.LaunchInitialStep();
        }

        protected override void SetupLauncher()
        {
            Launcher = new MockLauncher(DependencyService.Shared);
        }

        protected override bool AutoLaunchInitialStep()
        {
            return false;
        }
    }
}
