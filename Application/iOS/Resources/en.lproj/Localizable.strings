
//Tutorial - What is AUSid? screen
"tutorial_page_title" = "How it works";
"tutorial_page1_title" = "What is myGovID?";
"tutorial_page2_title" = "Before you start";
"tutorial_page3_title" = "Login securely";
"tutorial_page1_desc" = "It’s a secure, safe and easy way for you to prove who you are when logging in to government online services.";
"tutorial_page2_desc" = "Verify your identity using your passport, Medicare card and driver licence.";
"tutorial_page3_desc" = "Login to government services securely anywhere, anytime and on any device.";

// Terms and Conditions screen text
"tnc_title" = "Terms of use";
"tnc_accept_btn" = "Accept";
"tnc_decline_btn" = "Decline";
"tnc_decline_description" = "By declining the terms of use, you will not be able to create a myGovID account and access online government services.";
"tnc_alert_decline_btn" = "Ok";
"tnc_alert_back_btn" = "Back";

// Create account
"create_account_page_title" = "Create an account";
// Specify Email
"specify_email_title" = "Email address";
"specify_email_desc" = "Enter your email address to receive a verification code";
// Verify Email
"verify_email_title" = "Verify your email";
"verify_email_desc" = "Enter the 6 digit code sent to your email:";

// Code sent text
"email_code_sent_alert_title" = "Code sent";
"email_code_sent_alert_message" = "Check your email now";
"email_code_sent_ok_btn" = "OK";

// Verification Code screen text
"code_sent_alert_title" = "Code resent";
"code_sent_alert_message" = "We have resent a verification code to your email address.";
"code_sent_dismiss_btn" = "Dismiss";

//Version Update Alert - Mandatory
"version_update_required_alert_title" = "Update required";
"version_update_required_alert_message" = "A newer version is available.\nYou must update to continue using myGovID.";
"version_update_required_alert_update_btn" = "Update";

//Version Update Alert
"version_update_alert_title" = "Update available";
"version_update_alert_message" = "A newer version is available.\nWould you like to update now?";
"version_update_alert_update_btn" = "Update";
"version_update_alert_dismiss_btn" = "Not now";

//Existing Account screen text
"existing_account_title" = "Account already exists";
"existing_account_content_para" = "The email address you’ve provided already exists.\n\nYou can either add this device to your existing account or create a new account.";

//Biometrics screen text
"biometric_message" = "Access requires authentication";
"secure_account_title" = "Secure your account";
"secure_account_description" = "You can enable <b>{0}</b> to login to services using your <b>{1}</b>.";
"secure_account_switch_text" = "Enable %@ ID";
"secure_account_additional_text" = "By enabling <b>{0}</b>, you accept that having anyone else&rsquo;s fingerprint stored on this device will allow them to be authorised to access your myGovID app.";

//Backup Password screen text
"backup_password_title" = "Create a Backup password";
"backup_password_desc" = "Your password must be a minimum of 10 characters and contain an uppercase character, lowercase character, and either a number or a special character.";

//Password screen text
"password_title" = "Create a password";
"password_desc" = "Your password must be 10 characters with uppercase, lowercase and either a number or special character.";
"password_field_info" = "Your password must contain an uppercase character, a lowercase character and either a number or special character.\n\nIt must be a minimum of 10 characters.";

// Account created popup
"account_created_title" = "Account created";
"account_created_message" = "You’ve successfully created an account. Now let’s begin building your identity.";
"account_created_positive_btn" = "Continue";

// Success Auth Request
"success_auth_title" = "Success";
"success_auth_desc" = "Your authentication request was successful. Return to your computer browser now.";
"success_auth_account_btn" = "Ok, got it!";
// "success_auth_logout_btn" = "Logout";

// Declined Auth Request
"declined_auth_title" = "Declined";
"declined_auth_desc" = "Your authentication request was declined.";
"declined_auth_account_btn" = "Ok, got it!";
//"declined_auth_logout_btn" = "Logout";

// Auth request Alert Ref code
"alert_auth_request_title" = "Authentication request";
"alert_auth_request_desc" = "A request has been received to login to an online service. Enter your reference code below.";
"alert_auth_accept_btn" = "Accept";
"alert_auth_decline_btn" = "Decline";

//Document Success screen text
"document_success_title" = "Complete";
"document_success_desc" = "The following details have been added to your account";
"document_success_details_desc_label1" = "Name:";
"document_success_details_desc_label2" = "Date of birth:";

//Document List Not Now alert
"alert_document_not_now_title" = "Not now";
"alert_document_not_now_desc" = "By proceeding you can verify your identity at anytime from the account dashboard";
"alert_document_not_now_proceed_btn" = "Proceed";
"alert_document_not_now_goback_btn" = "Go back";

// Verify Doc screen
"verify_text" = "Verify your identity now instead of having your documents checked in person\n\nBegin the identity verification process by providing your passport, Medicare card or driver’s licence";

// IP1 screen & IP2 screen
"ip1_title" = "Identity documents";
"ip1_desc" = "We will ask you to enter some information about you and your identity documents. The information we collect will be checked with the issuing authority. Upon verification, we will only store your name and date of birth.";
"ip1_doc_select_label" = "Select your primary document:";
"ip2_title" = "Identity documents";
"ip2_desc" = "We will now ask you to enter some additional information about you and a secondary identity document.";
"ip2_doc_select_label" = "Select your secondary document:";

// Documents
"document_page_title" = "Build your identity";

// Aus passport screen
"aus_passport_desc" = "Enter your details";
"aus_passport_desc_ocr" = "Review your details";

// Medicare screen
"medicare_desc" = "Enter your details";
"medicare_name_field_info" = "If your name appears over multiple lines on your card, click 'Add new name field'. Do not enter names of other people listed on the card.";

// Drivers licence screen
"drivers_licence_desc" = "Enter your details";
"drivers_licence_desc_ocr" = "Review your details";

// Settings Screen
"settings_page_title" = "Settings";
"biometric_title" = "Touch ID Login";
"biometric_message" = "Touch ID allows you to use your fingerprint to action authentication requests and login faster";
"notification_title" = "Notifications";
"notification_message" = "Notifications allow you to stay up to date with your account activity";
"settings_footer_1" = "By enabling %@ ID, you accept that by having anyone elses fingerprint stored on this device will allow them to access your myGovID";
"change_password" = "Change password";
"allow_notifications" = "Allow notifications";
"biometric_switch_text" = "Enable %@ ID";
"terms_of_use" = "Terms of use";
"privacy" = "Privacy";

// About Screen
"about_page_title" = "About";
"about_version" = "Version ";
"about_description" = "AUSid can be used to securely login to online government services provided by the Australian Government. Your AUSid app will allow you to accept authentication requests to enable you to login.";
"about_what_is_mygovid" = "What is myGovID?";
"about_how_to_use_mygovid" = "How to use myGovID";
"about_supported_services" = "Supported services";
"about_feedback" = "Feedback";
"about_help" = "Help";
"about_version" = "Version";

// Devices Screen
"device_page_title" = "Devices";
"device_desc1" = "Manage devices";
"device_desc2" = "You can view and manage your devices on this page";

// Account Details Screen
"account_page_title" = "Account";

// Generic Dead End error messages
"error_title" = "System error";
"error_description" = "We are unable to process your request at this time. Try again in 10 minutes.\n\nIf the issue persists, contact us on 13 28 61";
"error_code_default" = "Error code: POI11000 (500)";
"error_code" = "Error code: ";
"error_continue_btn" = "Continue";

// Consent Text
"consent_text" = "I consent to have my identity details verified with the document issuer.";

// Dashboard
"identity_strength_weak" = "Weak";
"identity_strength_intermediate" = "Intermediate";
"identity_strength_strong" = "Strong";
"next_steps_title_weak" = "Build your identity";
"next_steps_title_intermediate" = "Your identity has been verified";
"next_steps_title_strong" = "Your identity has been verified";
"next_steps_description_weak" = "Verify your details so that you can use your myGovID to login to online services. ";
"next_steps_description_intermediate" = "You can now use your myGovID to login to online services. ";
"next_steps_description_strong" = "You can now leave the app to return to what you were doing. ";
"view_tutorial_link" = "View tutorial";

// Message view (for testing only)
"system_message_title" = "Planned system maintenance";
"system_message_description" = "myGovID will be unavailable from 11pm AEST Saturday 14 April until 3am AEST Monday 16 April. We apologise for the inconvenience and appreciate your patience.";
"document_message_title" = "Your %@ has been verified";

//TakeAPhoto Alert View
"take_aphoto_alert_title" = "Match your face";
"take_aphoto_alert_message" = "Strengthen your identity. We need to match your face with your passport photo, so we know it's you.";
"take_aphoto_alert_update_btn" = "Continue";
"take_aphoto_alert_dismiss_btn" = "Not now";

// Success FVS
"success_fvs_title" = "Success";
"success_fvs_desc" = "We have successfully matched your face\n against your passport.";
"success_fvs_ok_btn" = "Ok";

//FVS Intro
"fvs_intro_page_title" = "Match your face";
"fvs_intro_title" = "Before you start";
"fvs_intro_note1" = "Find a plain background";
"fvs_intro_note2" = "Remove your glasses, hat, or scarf";
"fvs_intro_note3" = "Move your hair away from your face";
"fvs_intro_note4" = "Keep your eyes open with a relaxed \nexpression";
"fvs_intro_note5" = "Look into the camera front on";
"fvs_intro_privacy_note" = "For your privacy, we will delete your photo once we have matched it.";

// FVS related text
"fvs_image_preview_consent" = "I consent to have my photo matched with my passport";

//AppUsageTutorial - How to use myGovID screen
"appusage_page_title" = "How to use myGovID";
"appusage_page1_title" = "Access a service";
"appusage_page2_title" = "Enter your email";
"appusage_page3_title" = "Verification";
"appusage_page1_desc" = "Navigate to a service that supports myGovID - like the ATO Business Portal.";
"appusage_page2_desc" = "Enter your email and click login. If you select 'remember me' you don't need to enter your email each time.";
"appusage_page3_desc" = "A four digit reference code will be sent to your phone for verification.";
//logout alert
"logout_alert_title" = "Session expired";
"logout_alert_description" = "Your session has expired due to inactivity";
"logout_alert_btn" = "Ok, got it!";

//account details page
"account_details_mygovid_desc" = "This is your myGovID, you will use this to login to online services on your browser";

//reset password alert
"reset_password_alert_title" = "Reset password";
"reset_password_alert_description" = "By resetting your password, you will need to re-verify your identity";
"reset_password_alert_ok_btn" = "Ok";
"reset_password_alert_cancel_btn" = "Back";

"reset_password_alert_onAnotherDevice_description" = "Next you will submit an authentication request using your myGovID.\n\nYou will need access to a device with your myGovID already setup.";
"reset_password_onAnotherDevice_alert_ok_btn" = "Continue";

//Account Recovery - LinkAccount
"link_account_alert_title" = "Link account";
"link_account_alert_message" = "Next you will submit an authentication request using your myGovID.\n\n You will need access to a device with your myGovID already setup.";
"link_account_alert_update_btn" = "Continue";
"link_account_alert_dismiss_btn" = "Back";

//Account Recovery - Link account using another device alert
"link_using_device_alert_title" = "Link account";
"link_using_device_alert_message" = "Next you will submit an authentication request using your myGovID.\n\nYou will need access to a device with your myGovID already setup.";
"link_using_device_alert_update_btn" = "Continue";
"link_using_device_alert_dismiss_btn" = "Back";

//Account Recovery - Link account using myGovID account details alert
"link_using_details_alert_title" = "Link account";
"link_using_details_alert_message" = "You will need your existing myGovID account details to re-verify your identity.";
"link_using_details_alert_update_btn" = "Continue";
"link_using_details_alert_dismiss_btn" = "Back";