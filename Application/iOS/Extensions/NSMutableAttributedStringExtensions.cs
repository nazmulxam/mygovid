﻿using System;
using Foundation;
using UIKit;

namespace myGovID.iOS.Extensions
{
    public static class NSMutableAttributedStringExtensions
    {
        /// <summary>
        /// Adds hyperlink functionality and styling to the string.
        /// </summary>
        /// <param name="target">The instance.</param>
        /// <param name="url">The URL.</param>
        /// <param name="applyToRange">The range to apply the hyperlink to. Defualts to the whole string.</param>
        public static void MakeHyperlink(this NSMutableAttributedString target, NSUrl url, NSRange? applyToRange = null)
        {
            var linkAttrs = new NSMutableDictionary();
            linkAttrs.SetValueForKey(url, new NSString(UIStringAttributeKey.Link));
            linkAttrs.SetValueForKey(UIColor.Blue, UIStringAttributeKey.ForegroundColor);
            linkAttrs.SetValueForKey(NSNumber.FromInt32((int)NSUnderlineStyle.Single), UIStringAttributeKey.UnderlineStyle);

            target.AddAttributes(linkAttrs, applyToRange ?? new NSRange(0, target.Length));
        }

        /// <summary>
        /// Adds the attributes from the given source string.
        /// </summary>
        /// <param name="target">The instance.</param>
        /// <param name="source">The source string to copy attributes from.</param>
        /// <param name="atIndex">THe index in the source string to copy attributes from.</param>
        /// <param name="applyToRange">The range to apply the attributes to. Defualts to the whole string.</param>
        public static void AddAttributesFrom(this NSMutableAttributedString target, NSAttributedString source, nint atIndex, NSRange? applyToRange = null)
        {
            var attrRange = new NSRange();
            var attrs = new NSMutableDictionary(source.GetAttributes(atIndex, out attrRange));

            target.AddAttributes(attrs, applyToRange ?? new NSRange(0, target.Length));
        }

        /// <summary>
        /// Appends a string using the attributes from the current mutable attributed string.
        /// </summary>
        /// <param name="target">The instance.</param>
        /// <param name="other">The string to append.</param>
        /// <param name="atIndex">The index to get attributes from, defaults to the end of the attributed string.</param>
        public static void AppendStringWithAttributes(this NSMutableAttributedString target, string other, nint? atIndex = null)
        {
            var attributedOther = new NSMutableAttributedString(other);
            attributedOther.AddAttributesFrom(target, atIndex ?? (target.Length - 1));
            target.Append(attributedOther);
        }
    }
}