﻿using System;
using Foundation;

namespace myGovID.iOS.Extensions
{
    public static class NSAttributedStringExtensions
    {
        public static void EnumerateAttributedSubstrings(this NSAttributedString target, Action<NSDictionary, NSRange> callback)
        {
            var copy = new NSAttributedString(target);
            nint location = 0;
            while (location < target.Length)
            {
                var attributes = copy.GetAttributes(location, out NSRange attributesRange);
                callback(attributes, attributesRange);
                location = attributesRange.Location + attributesRange.Length;
            }
        }
    }
}
