﻿using System;
using myGovID.Services.Dependency.iOS;
using myGovID.Services.Timer.Contract;
using myGovID.Services.Timer.iOS;

namespace myGovID.iOS
{
    public class MyGovIDApplication : TimerApplication
    {
        private IDeviceActivityTimer _deviceActivityTimer;
        public override IDeviceActivityTimer DeviceActivityTimer {
            get
            {
                if(_deviceActivityTimer == null)
                {
                    _deviceActivityTimer = DependencyService.Shared.Resolve<IDeviceActivityTimer>();
                }
                return _deviceActivityTimer;
            }
            set
            {
                throw new NotSupportedException();
            }
        }
    }
}