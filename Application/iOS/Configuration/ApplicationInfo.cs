﻿using Foundation;
using myGovID.Application.Launcher.Contract;

namespace myGovID.iOS.Configuration
{
    public class ApplicationInfo : IApplicationInfo
    {
        public string Name { get; }
        public string Version { get; }
        public string BuildDate { get; }
        public string FilesDirectory { get; }
        public ApplicationInfo(NSBundle bundle)
        {
            bool success = bundle.InfoDictionary.TryGetValue(new NSString("CFBundleDisplayName"), out NSObject displayName);
            Name = success ? displayName.ToString() : null;
            if (Name == null)
            {
                success = bundle.InfoDictionary.TryGetValue(new NSString("CFBundleName"), out displayName);
                Name = success ? displayName.ToString() : null;
            }
            success = bundle.InfoDictionary.TryGetValue(new NSString("CFBundleShortVersionString"), out NSObject version);
            Version = success ? version.ToString() : null;
            // We're limited to 3 version number items for short version string in iOS, so add on bundle version as well
            if (Version != null)
            {
                success = bundle.InfoDictionary.TryGetValue(new NSString("CFBundleVersion"), out NSObject buildVersion);
                if (success)
                {
                    Version += "." + buildVersion;
                }
            }
            BuildDate = null;
            FilesDirectory = null;
        }
    }
}