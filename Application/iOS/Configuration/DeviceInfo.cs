﻿using System;
using System.Collections.Generic;
using Foundation;
using LocalAuthentication;
using myGovID.Application.Launcher.Contract;
using myGovID.Presentation.Contract;
using UIKit;

namespace myGovID.iOS.Configuration
{
    public class DeviceInfo : IDeviceInfo
    {
        public string PlatformType { get; }
        public string RuntimeType { get; }
        public string RuntimeVersion { get; }
        public string FormFactorType { get; }
        public IEnumerable<DeviceCapabilityInfo> Capabilities { get; }
        public DeviceInfo()
        {
            PlatformType = "Apple";
            RuntimeType = Presentation.Contract.RuntimeType.iOS.ToString();
            RuntimeVersion = UIDevice.CurrentDevice.SystemVersion;

            string model = UIDevice.CurrentDevice.Model;
            FormFactorType = (model.ToLower().Contains("phone") ? FormFactor.Phone : FormFactor.Pad).ToString();

            var capabilityList = new List<DeviceCapabilityInfo>();

            var bioCapability = GetBiometricCapability();
            if (bioCapability.HasValue)
            {
                capabilityList.Add(bioCapability.Value);
            }

            if (HasCamera())
            {
                capabilityList.Add(DeviceCapabilityInfo.Camera);
            }

            Capabilities = capabilityList;
        }

        private bool HasCamera()
        {
            return UIImagePickerController.IsSourceTypeAvailable(UIImagePickerControllerSourceType.Camera);
        }

        private DeviceCapabilityInfo? GetBiometricCapability()
        {
            LAContext authContext = new LAContext();
            if (UIDevice.CurrentDevice.CheckSystemVersion(11, 0))
            {
                if (authContext.CanEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, out NSError error))
                {
                    switch (authContext.BiometryType)
                    {
                        case LABiometryType.TouchId:
                            return DeviceCapabilityInfo.Touch;
                        case LABiometryType.FaceId:
                            return DeviceCapabilityInfo.FacialVerification;
                    }
                }
            }
            else
            {
                if (authContext.CanEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, out NSError error))
                {
                    return DeviceCapabilityInfo.Touch;
                }
            }

            return null;
        }
    }

}
