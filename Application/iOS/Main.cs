﻿using UIKit;

namespace myGovID.iOS
{
    public class Application
    {
        static void Main(string[] args)
        {
#if MOCK
            UIApplication.Main(args, typeof(Mock.MyGovIDMockApplication), typeof(Mock.MockAppDelegate));
#else
            UIApplication.Main(args, typeof(MyGovIDApplication), typeof(AppDelegate));
#endif
        }
    }
}
