﻿using Foundation;
using UIKit;
using myGovID.Presentation.Contract;
using myGovID.iOS.Steps;
using System;
using myGovID.Services.Dependency.Contract;
using myGovID.Application.Launcher.Contract;
using myGovID.Application.Launcher;
using myGovID.Services.Dependency.iOS;
using myGovID.iOS.Configuration;
using System.Threading.Tasks;
using myGovID.Services.Timer.iOS;
using myGovID.Services.Timer.Contract;
using myGovID.iOS.Views;

namespace myGovID.iOS
{
    public class AppDelegate : UIApplicationDelegate
    {
        public override UIWindow Window { get; set; }
        // Step Continuation
        private Action<Task<IStep>> StepContinuation;
        protected ILauncher Launcher;
        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            StepContinuation = (taskResult) =>
            {
                if (taskResult.IsCanceled || taskResult.IsFaulted)
                {
                    Exception exception = taskResult.Exception.GetBaseException();
                    TransitionOnError(exception);
                    return;
                }
                TransitionToStep(taskResult.Result);
            };
            Window = new UIWindow(UIScreen.MainScreen.Bounds);
            Window.MakeKeyAndVisible();
            SetupLauncher();
            SetupKeyboardManager();

            if (AutoLaunchInitialStep())
            {
                RegisterDependencies();
                LaunchInitialStep();
            }

            return true;
        }

        private void SetupKeyboardManager()         {
            // Enables the UIToolbar with Done button on top of the keyboard
            Xamarin.IQKeyboardManager.SharedManager.EnableAutoToolbar = true;
             // Enables the Focused field to be always visible when the Keyboard is shown
            Xamarin.IQKeyboardManager.SharedManager.Enable = true;
             // Dismiss the keyboard if you touch outside
            Xamarin.IQKeyboardManager.SharedManager.ShouldResignOnTouchOutside = true;

            // Don't add an extra placeholder label on the keyboard toolbar
            Xamarin.IQKeyboardManager.SharedManager.ShouldShowToolbarPlaceholder = false;
        }


        protected void RegisterDependencies()
        {
            DependencyService.Shared.Register<StepOrchestrator, StepOrchestrator>(Scope.Singleton);
        }

        protected virtual void SetupLauncher()
        {
            Launcher = new Launcher(DependencyService.Shared);
            LaunchConfiguration configuration = new LaunchConfiguration.Builder()
                .SetDeviceInfo(new DeviceInfo())
                .SetApplicationInfo(new ApplicationInfo(NSBundle.MainBundle)).Build();
            Launcher.Configure(configuration);
        }

        protected virtual void LaunchInitialStep()
        {
            IStepRepository stepRepository = DependencyService.Shared.Resolve<IStepRepository>();

            // We need an initial RootViewController to display while the async
            // call to the step repository executes. This will stay on
            // the launch screen view.
            var storyboard = UIStoryboard.FromName("LaunchScreen", null);
            var viewController = storyboard.InstantiateInitialViewController();
            Window.RootViewController = viewController;

            stepRepository.GetInitialStepAsync().ContinueWith(StepContinuation);
        }

        protected virtual void TransitionToStep(IStep newStep)
        {
            BeginInvokeOnMainThread(() =>
            {
                StepOrchestrator stepOrchestrator = DependencyService.Shared.Resolve<StepOrchestrator>();
                var tuple = stepOrchestrator.GetViewController(newStep);
                if (!tuple.HasValue)
                {
                    return;
                }
                (UIViewController viewController, bool overlay) = tuple.Value;
                if (overlay)
                {
                    Window.RootViewController.PresentViewController(viewController, true, null);
                }
                else
                {
                    Window.RootViewController = viewController;
                    Window.MakeKeyAndVisible();
                }
            });
        }
        private void TransitionOnError(Exception exception)
        {
            Console.WriteLine("Exception " + exception.Message);
            StepOrchestrator stepOrchestrator = DependencyService.Shared.Resolve<StepOrchestrator>();
            TransitionToStep(stepOrchestrator.GetDeadEndStep("UNK000002", "Uncaught Exception", exception.Message));
        }
        protected virtual bool AutoLaunchInitialStep()
        {
            return true;
        }
    }
}