﻿using System;
using UIKit;
using Foundation;
namespace myGovID.iOS
{
    public static class Base64
    {
        public static UIImage ToImage(string base64)
        {
            byte[] encodedDataAsBytes = Convert.FromBase64String(base64);
            NSData data = NSData.FromArray(encodedDataAsBytes);
            return UIImage.LoadFromData(data);
        }
        public static string FromImage(UIImage image, float CompressionQuality)
        {
            return image.AsJPEG(CompressionQuality).GetBase64EncodedString(NSDataBase64EncodingOptions.None);
        }
    }
}