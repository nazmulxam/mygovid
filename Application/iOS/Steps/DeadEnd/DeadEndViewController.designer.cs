// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace myGovID.iOS.Steps.DeadEnd
{
    [Register ("DeadEndViewController")]
    partial class DeadEndViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel ErrorCode { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextView ErrorDescription { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel ErrorTitle { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ErrorCode != null) {
                ErrorCode.Dispose ();
                ErrorCode = null;
            }

            if (ErrorDescription != null) {
                ErrorDescription.Dispose ();
                ErrorDescription = null;
            }

            if (ErrorTitle != null) {
                ErrorTitle.Dispose ();
                ErrorTitle = null;
            }
        }
    }
}