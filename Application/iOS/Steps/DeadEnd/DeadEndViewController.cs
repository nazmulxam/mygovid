﻿using myGovID.Presentation.Contract.Steps.DeadEnd;

namespace myGovID.iOS.Steps.DeadEnd
{
    public partial class DeadEndViewController : StepViewController<IDeadEndStep, DeadEndViewModel>
    {
        protected override void OnViewModelLoaded()
        {
            ErrorTitle.Text = ViewModel.Error.Title;
            ErrorTitle.AccessibilityIdentifier = ViewModel.ErrorTitleAccessibilityIdentifier;
            SetErrorCodeText(ViewModel.Error.Code);
            SetErrorDescriptionAsHtml(ViewModel.Error.Description);
        }
        private void SetErrorCodeText(string code)
        {
            ErrorCode.Text = "Error code: " + ViewModel.Error.Code;
            ErrorCode.AccessibilityIdentifier = ViewModel.ErrorCodeAccessibilityIdentifier; 
        }
        private void SetErrorDescriptionAsHtml(string message)
        {
            RenderHtml(ErrorDescription, message);
            ErrorDescription.TextAlignment = UIKit.UITextAlignment.Center;
            ErrorDescription.AccessibilityIdentifier = ViewModel.ErrorDescriptionAccessibilityIdentifier;
        }
    }
}