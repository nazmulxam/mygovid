﻿using Foundation;
using myGovID.iOS.Constants;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.ResentEmailVerificationCode;
using UIKit;

namespace myGovID.iOS.Steps.ResentEmailVerificationCode
{
    public partial class ResentEmailVerificationCodeController : StepViewController<IResentEmailVerificationCodeStep, ResentEmailVerificationCodeViewModel>
    {
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            DismissButton.SetTitle(NSBundle.MainBundle.GetLocalizedString("code_sent_dismiss_btn", "Dismiss"), UIControlState.Normal);
            AlertIcon.Image = UIImage.FromBundle(Images.GreenTickAlert);
            AlertTitle.Text = NSBundle.MainBundle.GetLocalizedString("code_sent_alert_title", "Code resent");
            AlertMessage.Text = NSBundle.MainBundle.GetLocalizedString("code_sent_alert_message", "We have resent a verification code to your email address.");
        }
        protected override void OnViewModelLoaded()
        {
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(DismissButton, ActionType.Next, null, "OnNextOverlay");
        }
    }
}