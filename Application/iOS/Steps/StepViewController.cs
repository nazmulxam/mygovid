using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreAnimation;
using GalaSoft.MvvmLight.Helpers;
using myGovID.iOS.Views;
using myGovID.iOS.Views.Common;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Services.Dependency.iOS;
using myGovID.Services.Event.Contract;
using myGovID.Services.Log.Contract;
using UIKit;

namespace myGovID.iOS.Steps
{
    public abstract class StepViewController<S, V> : StepViewController<S, V, Enum> where S : IStep where V : IViewModel
    { }
    public abstract class StepViewController<S, V, E> : UIViewController, IStepViewController where S : IStep where V : IViewModel
    {
        protected PageErrorView PageErrorView { get; set; }
        private S _step;
        protected V ViewModel => (V)_step.ViewModel;
        public IStep Step { get => _step; set => _step = (S)value; }
        // Data bindings.
        private readonly List<Binding> _bindings = new List<Binding>();
        private IAction<ActionType, E> GetPreviousStep(E subType) => FindAction(ActionType.Previous, subType);
        // Step Continuation
        protected Action<Task<IStep>> StepContinuation;
        private IEventService _eventService = DependencyService.Shared.Resolve<IEventService>();
        protected ILogger Logger = DependencyService.Shared.Resolve<ILogger>();

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            StepContinuation = (taskResult) =>
            {
                if (taskResult.IsCanceled || taskResult.IsFaulted)
                {
                    Exception exception = taskResult.Exception.GetBaseException();
                    TransitionOnError(exception);
                    return;
                }
                TransitionToStep(taskResult.Result);
            };
            StepOrchestrator stepOrchestrator = DependencyService.Shared.Resolve<StepOrchestrator>();
            OnViewModelLoaded();
            UpdatePageError();
            if (_step.ChildStep != null)
            {
                TransitionToStep(_step.ChildStep);
            }
        }
        private void UpdatePageError()
        {
            UIView pageErrorContainer = GetPageErrorViewContainer();
            if (pageErrorContainer == null)
            {
                return;
            }
            IEnumerable<PageMessage> messages = _step.Messages;
            if (messages == null)
            {
                pageErrorContainer.Hidden = true;
                return;
            }
            PageMessage firstError = messages.First((el) => el.Type.ToLower() == "error");
            pageErrorContainer.Hidden = firstError == null;
            if (firstError != null)
            {
                foreach (UIView child in pageErrorContainer.Subviews)
                {
                    child.RemoveFromSuperview();
                }
                pageErrorContainer.Add(new PageErrorView(pageErrorContainer, firstError));
                pageErrorContainer.SizeToFit();
                pageErrorContainer.SetNeedsLayout();
                pageErrorContainer.Superview.SizeToFit();
                pageErrorContainer.Superview.SetNeedsLayout();
            }
        }
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            AttachBindings();
            _eventService.Subscribe<IStep>(this, EventKey.Step, TransitionToStep);
        }
        public override void ViewWillDisappear(bool animated)
        {
            foreach (Binding binding in _bindings)
            {
                binding.Detach();
            }
            _bindings.Clear();
            OnBindingsDetached();
            _eventService.UnSubscribe<IStep>(this, EventKey.Step);
            base.ViewWillDisappear(animated);
        }
        public override void WillMoveToParentViewController(UIViewController parent)
        {
            base.WillMoveToParentViewController(parent);
            IEventService eventService = DependencyService.Shared.Resolve<IEventService>();
            if (parent == null)
            {
                eventService.Subscribe<IStep>(this, EventKey.Step, TransitionToStep);
            }
            else
            {
                eventService.UnSubscribe<IStep>(this, EventKey.Step);
            }
        }

        protected abstract void OnViewModelLoaded();
        protected virtual void AttachBindings() { }
        protected virtual void OnBindingsDetached() { }
        protected virtual UIView GetPageErrorViewContainer() { return null; }
        protected void TransitionToStep(IStep newStep)
        {
            BeginInvokeOnMainThread(() =>
            {
                HideLoading(() =>
                {
                    if (newStep == null)
                    {
                        return;
                    }
                    StepOrchestrator stepOrchestrator = DependencyService.Shared.Resolve<StepOrchestrator>();
                    var tuple = stepOrchestrator.GetViewController(newStep);
                    if (!tuple.HasValue)
                    {
                        return;
                    }
                    (UIViewController viewController, bool overlay) = tuple.Value;
                    UIWindow window = ((AppDelegate)UIApplication.SharedApplication.Delegate).Window;
                    window.Layer.AddAnimation(animation: GetFadeOutAnimation(true), key: CAAnimation.TransitionFade);
                    if (overlay)
                    {
                        viewController.ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;
                        viewController.ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve;
                        PresentViewController(viewController, true, null);
                    }
                    else
                    {
                        window.RootViewController = viewController;
                        window.MakeKeyAndVisible();
                    }
                });
            });
        }
        protected void TransitionOnError(Exception exception)
        {
            Logger.Log(level: LogLevel.Error,
                       message: $"{this.GetType().Name} transitioning with exception:",
                       exception: exception);
            StepOrchestrator stepOrchestrator = DependencyService.Shared.Resolve<StepOrchestrator>();
            TransitionToStep(stepOrchestrator.GetDeadEndStep("UNK000002", "Uncaught Exception", exception.Message));
        }
        protected IAction<ActionType, E> FindAction(ActionType actionType, E subType = default)
        {
            return Step.GetActions<E>().FirstOrDefault((action) =>
            {
                if (action.Type != actionType)
                {
                    return false;
                }
                if (action.SubType == null && subType == null)
                {
                    return true;
                }
                if (action.SubType != null && subType != null)
                {
                    return action.SubType.Equals(subType);
                }
                return false;
            });
        }

        public void Bind(FormInput<string> input, UITextField textField, UILabel errorLabel, UILabel titleLabel = null, string accessibilityId = null, string placeholder = null)
        {
            textField.EditingChanged += (s, e) => { }; // To fool the linker
            AddBindings(BindingSupport.Bind()
                .AccessibilityId(input, textField, accessibilityId)
                .Placeholder(input, textField)
                .Enabled(input, textField)
                .AccessibilityId(input, errorLabel, accessibilityId, "Error")
                .Value(input, textField)
                .Error(input, errorLabel)
                .GetBindings());

            if (titleLabel != null)
            {
                AddBindings(BindingSupport.Bind()
                    .Label(input, titleLabel)
                    .GetBindings());
            }
        }

        public void Bind(FormInput<string> input, UIView container, UITextField textField, UILabel errorLabel, UILabel titleLabel = null, string accessibilityId = null, string placeholder = null)
        {
            Bind(input, textField, errorLabel, titleLabel, accessibilityId);
            AddBindings(BindingSupport.Bind()
                .Visibility(input, container)
                .GetBindings());
        }

        public void Bind(FormInput<bool> input, UIControl button, UILabel titleView, string accessibilityId = null)
        {
            AddBindings(BindingSupport.Bind()
                .Visibility(input, button)
                .Visibility(input, titleView)
                .Label(input, titleView, true)
                .AccessibilityId(input, button, accessibilityId)
                .AccessibilityId(input, titleView, accessibilityId, "Title")
                .Value(input, button)
                .GetBindings());
        }

        public void Bind(FormInput<bool> input, UIControl button, UILabel titleView, UILabel errorLabel, string accessibilityId = null)
        {
            Bind(input, button, titleView, accessibilityId);
            AddBindings(BindingSupport.Bind()
                    .Error(input, errorLabel)
                    .GetBindings());
        }

        public void Bind(ButtonControl buttonViewModel, UIButton uiButton)
        {
            AddBindings(BindingSupport.Bind()
                .Visibility(buttonViewModel, uiButton)
                .OnTapped(buttonViewModel, uiButton)
                .GetBindings());
        }

        public void Bind(UIControl view, ActionType actionType, E subType = default, string accessibilityId = null)
        {
            IAction<ActionType, E> action = FindAction(actionType, subType);
            if (action != null)
            {
                AddBindings(BindingSupport.Bind()
                    .AccessibilityId(action, view, accessibilityId)
                    .GetBindings());

                view.TouchUpInside += delegate
                {
                    ExecuteAction(action);
                };
            }
        }

        public void Bind(UIBarButtonItem view, ActionType actionType, E subType = default, string accessibilityId = null)
        {
            IAction<ActionType, E> action = FindAction(actionType, subType);
            if (action != null)
            {
                AddBindings(BindingSupport.Bind()
                    .AccessibilityId(action, view, accessibilityId)
                    .GetBindings());

                view.Clicked += delegate
                {
                    ExecuteAction(action);
                };
            }
        }

        public void Bind(UIView view, ActionType actionType, E subType = default, string accessibilityId = null)
        {
            IAction<ActionType, E> action = FindAction(actionType, subType);
            if (action != null)
            {
                AddBindings(BindingSupport.Bind()
                    .AccessibilityId(action, view, accessibilityId)
                    .GetBindings());
                view.AccessibilityTraits = UIAccessibilityTrait.Button;
                var tapGestureRecognizer = new UITapGestureRecognizer(() =>
                {
                    ExecuteAction(action);
                });
                view.AddGestureRecognizer(tapGestureRecognizer);
            }
        }

        protected void ExecuteAction(IAction<ActionType, E> action)
        {
            if (!action.SkipValidation && !Step.ViewModel.IsValid())
            {
                return;
            }
            ShowLoading(async () =>
            {
                //Wait for 1 sec or Interaction task completion, whichever is longer.
                Task waitTask = Task.Delay(250);
                Task<IStep> interactionTask = action.Run();
                await Task.WhenAll(waitTask, interactionTask);
                StepContinuation.Invoke(interactionTask);
            });
        }

        public void AddBinding(Binding binding)
        {
            _bindings.Add(binding);
        }

        public void AddBindings(IEnumerable<Binding> bindings)
        {
            _bindings.AddRange(bindings);
        }

        protected void ShowLoading(Action action)
        {
            GetWindow().ShowLoading();
            action.Invoke();
        }
        protected void HideLoading(Action action)
        {
            action.Invoke();
            GetWindow().DismissLoading();
        }
        protected UIWindow GetWindow()
        {
            return GetAppDelegate().Window;
        }
        protected AppDelegate GetAppDelegate()
        {
            return UIApplication.SharedApplication.Delegate as AppDelegate;
        }
        protected void RenderHtml(UILabel label, string html)
        {
            new ThemedHtmlParser(html)
                .ApplyToLabel(label);
        }

        protected void RenderHtml(UITextView textView, string html)
        {
            new ThemedHtmlParser(html)
                .ApplyToTextView(textView);
        }

        private CATransition GetFadeOutAnimation(bool leftToRight)
        {
            CATransition transition = new CATransition();
            transition.Duration = 0.8;
            transition.Type = CAAnimation.TransitionFade;
            transition.TimingFunction = CAMediaTimingFunction.FromName(CAMediaTimingFunction.EaseInEaseOut);
            return transition;
        }
    }
}
