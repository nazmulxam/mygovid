﻿using System;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.AccountSetup;
using myGovID.Presentation.Contract.Steps.Passport;
using UIKit;

namespace myGovID.iOS.Steps.PassportOCRIntro
{
    public partial class PassportOCRIntroViewController : StepViewController<IPassportOCRIntroStep, PassportOCRIntroViewModel, PassportOCRIntroAction>
    {
        protected override void OnViewModelLoaded()
        {
            Title.Text = ViewModel.Title;
            Description.Text = ViewModel.Description;

        }

        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(ScanButton, ActionType.Next, PassportOCRIntroAction.OCRCapture);
            Bind(EnterManualButton, ActionType.Next, PassportOCRIntroAction.ManualEntry);
            Bind(BackButton, ActionType.Previous);
        }

    }
}

