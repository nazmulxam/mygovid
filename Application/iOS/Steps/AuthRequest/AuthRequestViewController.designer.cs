﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace myGovID.iOS.Steps.AuthRequest
{
    [Register ("AuthRequestViewController")]
    partial class AuthRequestViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIButtonTypeAware AcceptButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware AlertMessage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware AlertTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.CodeEntryTextField CodeEntry { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware CodeErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView PageErrorViewContainer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIButtonTypeAware RejectButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AcceptButton != null) {
                AcceptButton.Dispose ();
                AcceptButton = null;
            }

            if (AlertMessage != null) {
                AlertMessage.Dispose ();
                AlertMessage = null;
            }

            if (AlertTitle != null) {
                AlertTitle.Dispose ();
                AlertTitle = null;
            }

            if (CodeEntry != null) {
                CodeEntry.Dispose ();
                CodeEntry = null;
            }

            if (CodeErrorLabel != null) {
                CodeErrorLabel.Dispose ();
                CodeErrorLabel = null;
            }

            if (PageErrorViewContainer != null) {
                PageErrorViewContainer.Dispose ();
                PageErrorViewContainer = null;
            }

            if (RejectButton != null) {
                RejectButton.Dispose ();
                RejectButton = null;
            }
        }
    }
}