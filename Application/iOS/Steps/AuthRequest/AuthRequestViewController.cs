﻿using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.AuthRequest;
using UIKit;

namespace myGovID.iOS.Steps.AuthRequest
{
    public partial class AuthRequestViewController : StepViewController<IAuthRequestStep, AuthRequestViewModel>
    {
        protected override void OnViewModelLoaded()
        {
            CodeEntry.SetCodeLength(ViewModel.AuthRequestLength);
            AlertTitle.Text = ViewModel.AlertTitle ;
            AlertMessage.Text = ViewModel.AlertMessage;
            AcceptButton.SetTitle(ViewModel.AcceptButtonTitle, UIControlState.Normal);
            RejectButton.SetTitle(ViewModel.RejectButtonTitle, UIControlState.Normal);
        }

        protected override void AttachBindings()
        {
            base.AttachBindings();

            Bind(ViewModel.CodeEntry, CodeEntry.TextField, CodeErrorLabel);
            Bind(RejectButton, ActionType.Previous);
            Bind(AcceptButton, ActionType.Next);
        }

        protected override UIView GetPageErrorViewContainer()
        {
            return PageErrorViewContainer;
        }
    }
}

