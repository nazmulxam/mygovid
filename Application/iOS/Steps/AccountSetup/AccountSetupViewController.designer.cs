// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace myGovID.iOS.Steps.AccountSetup
{
    [Register ("AccountSetupViewController")]
    partial class AccountSetupViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIButtonTypeAware CreateAccountButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIButtonTypeAware LinkAccountButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIButtonTypeAware NeedHelpButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (CreateAccountButton != null) {
                CreateAccountButton.Dispose ();
                CreateAccountButton = null;
            }

            if (LinkAccountButton != null) {
                LinkAccountButton.Dispose ();
                LinkAccountButton = null;
            }

            if (NeedHelpButton != null) {
                NeedHelpButton.Dispose ();
                NeedHelpButton = null;
            }
        }
    }
}