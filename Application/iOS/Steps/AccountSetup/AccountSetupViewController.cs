﻿using myGovID.iOS.Constants;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.AccountSetup;
using UIKit;

namespace myGovID.iOS.Steps.AccountSetup
{
    public partial class AccountSetupViewController : StepViewController<IAccountSetupStep, AccountSetupViewModel, AccountSetupAction>
    {
        protected override void OnViewModelLoaded()
        {
            base.View.Layer.Contents = UIImage.FromBundle(Images.LaunchBackground).CGImage;
        }

        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(CreateAccountButton, ActionType.Next, AccountSetupAction.Register);
            Bind(LinkAccountButton, ActionType.Next, AccountSetupAction.Recover);
            Bind(NeedHelpButton, ActionType.Browse, AccountSetupAction.Help);
        }
    }
}

