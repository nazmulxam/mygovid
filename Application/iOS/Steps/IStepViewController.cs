﻿using myGovID.Presentation.Contract;
namespace myGovID.iOS.Steps
{
    interface IStepViewController
    {
        IStep Step { get; set; }
    }
}
