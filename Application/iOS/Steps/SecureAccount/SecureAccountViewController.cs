﻿using System.Linq;
using myGovID.iOS.Constants;
using myGovID.iOS.Views.Common;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Presentation.Contract.Steps.SecureYourAccount;
using UIKit;

namespace myGovID.iOS.Steps.SecureAccount
{
    public partial class SecureAccountViewController : StepViewController<ISecureYourAccountStep, SecureYourAccountViewModel>
    {
        protected override void OnViewModelLoaded()
        {
            SwitchControl.OnTintColor = ColorsHelper.BlueButtonColor;
            ImageView.Image = UIImage.FromBundle(ViewModel.BiometricType.Equals("Face") ? Images.FaceID : Images.TouchID);
            RenderHtml(DescriptionLabel, ViewModel.Description);
            DescriptionLabel.TextColor = UIColor.DarkGray;

        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(NextButton, ActionType.Next);
            Bind((BooleanInput)ViewModel.Inputs.First(), SwitchControl, SwitchTitle);
        }
    }
}