﻿using System;
using System.Linq;
using myGovID.Contract.Steps.CapturePersonalDetails;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Presentation.Contract.Steps.CapturePersonalDetails;
using UIKit;
using myGovID.iOS.Views.Common;

namespace myGovID.iOS.Steps.CapturePersonalDetails
{
    public partial class CapturePersonalDetailsViewController : StepViewController<ICapturePersonalDetailsStep, CapturePersonalDetailsViewModel>
    {
        private NameInput _givenNameInput => (NameInput)ViewModel.Inputs.First(input => input.MetaData == InputMetaData.GivenName);
        private NameInput _familyNameInput => (NameInput)ViewModel.Inputs.First(input => input.MetaData == InputMetaData.FamilyName);
        private BirthDateInput _dateOfBirthInput => (BirthDateInput)ViewModel.Inputs.First(input => input.MetaData == InputMetaData.DateOfBirth);

        protected override void OnViewModelLoaded()
        {

        }

        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(_givenNameInput, GivenNamesTextField, GivenNamesErrorLabel);
            Bind(_familyNameInput, FamilyNameTextField, FamilyNameErrorLabel);
            Bind(_dateOfBirthInput, DateOfBirthTextField, DateOfBirthErrorLabel);
            Bind(NextButton, ActionType.Next);
        }

    }
}

