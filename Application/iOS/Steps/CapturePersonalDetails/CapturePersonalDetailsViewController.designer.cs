// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace myGovID.iOS.Steps.CapturePersonalDetails
{
    [Register ("CapturePersonalDetailsViewController")]
    partial class CapturePersonalDetailsViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView contentView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware DateOfBirthErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIDateTextField DateOfBirthTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware FamilyNameErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UITextFieldTypeAware FamilyNameTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware GivenNamesErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UITextFieldTypeAware GivenNamesTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIButtonTypeAware NextButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView scrollView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (contentView != null) {
                contentView.Dispose ();
                contentView = null;
            }

            if (DateOfBirthErrorLabel != null) {
                DateOfBirthErrorLabel.Dispose ();
                DateOfBirthErrorLabel = null;
            }

            if (DateOfBirthTextField != null) {
                DateOfBirthTextField.Dispose ();
                DateOfBirthTextField = null;
            }

            if (FamilyNameErrorLabel != null) {
                FamilyNameErrorLabel.Dispose ();
                FamilyNameErrorLabel = null;
            }

            if (FamilyNameTextField != null) {
                FamilyNameTextField.Dispose ();
                FamilyNameTextField = null;
            }

            if (GivenNamesErrorLabel != null) {
                GivenNamesErrorLabel.Dispose ();
                GivenNamesErrorLabel = null;
            }

            if (GivenNamesTextField != null) {
                GivenNamesTextField.Dispose ();
                GivenNamesTextField = null;
            }

            if (NextButton != null) {
                NextButton.Dispose ();
                NextButton = null;
            }

            if (scrollView != null) {
                scrollView.Dispose ();
                scrollView = null;
            }
        }
    }
}