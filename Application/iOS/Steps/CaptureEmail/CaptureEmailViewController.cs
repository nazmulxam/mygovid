﻿using myGovID.Presentation.Contract.Steps.CaptureEmail;
using UIKit;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Inputs;
using System.Linq;
using Foundation;

namespace myGovID.iOS.Steps.CaptureEmail
{
    public partial class CaptureEmailViewController : StepViewController<ICaptureEmailStep, CaptureEmailViewModel>
    {
        // For binding purposes
        private EmailInput _emailInput => (EmailInput) ViewModel.Inputs.First(input => input.MetaData == InputMetaData.EmailAddress);
        protected override void OnViewModelLoaded()
        {
            SpecifyEmailDescriptionlabel.Text = NSBundle.MainBundle.GetLocalizedString("specify_email_desc", "");
        }
        protected override void AttachBindings() {
            Bind(_emailInput, EmailAddressTextField, EmailAddressErrorLabel);
            Bind(NextButton, ActionType.Next);
        }
        protected override UIView GetPageErrorViewContainer()
        {
            return PageErrorViewContainer;
        }
    }
}