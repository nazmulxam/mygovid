// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace myGovID.iOS.Steps.CaptureEmail
{
    [Register ("CaptureEmailViewController")]
    partial class CaptureEmailViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware EmailAddressErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UITextFieldTypeAware EmailAddressTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIButtonTypeAware NextButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView PageErrorViewContainer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware SpecifyEmailDescriptionlabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (EmailAddressErrorLabel != null) {
                EmailAddressErrorLabel.Dispose ();
                EmailAddressErrorLabel = null;
            }

            if (EmailAddressTextField != null) {
                EmailAddressTextField.Dispose ();
                EmailAddressTextField = null;
            }

            if (NextButton != null) {
                NextButton.Dispose ();
                NextButton = null;
            }

            if (PageErrorViewContainer != null) {
                PageErrorViewContainer.Dispose ();
                PageErrorViewContainer = null;
            }

            if (SpecifyEmailDescriptionlabel != null) {
                SpecifyEmailDescriptionlabel.Dispose ();
                SpecifyEmailDescriptionlabel = null;
            }
        }
    }
}