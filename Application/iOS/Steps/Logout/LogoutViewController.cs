﻿using Foundation;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Logout;
using UIKit;

namespace myGovID.iOS.Steps.Logout
{
    public partial class LogoutViewController : StepViewController<ILogoutStep, LogoutViewModel>
    {
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            OkButton.SetTitle(NSBundle.MainBundle.GetLocalizedString("logout_alert_btn", "Ok"), UIControlState.Normal);
            AlertIcon.Image = UIImage.FromBundle("DeclinedAlert");
            AlertTitle.Text = NSBundle.MainBundle.GetLocalizedString("logout_alert_title", "Session expired");
            AlertMessage.Text = NSBundle.MainBundle.GetLocalizedString("logout_alert_description", "Your session has expired due to inactivity.");
        }

        protected override void OnViewModelLoaded()
        {
        }

        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(OkButton, ActionType.Next);
        }
    }
}