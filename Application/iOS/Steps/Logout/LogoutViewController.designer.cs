// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace myGovID.iOS.Steps.Logout
{
    [Register ("LogoutViewController")]
    partial class LogoutViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView AlertIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel AlertMessage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel AlertTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIButtonTypeAware OkButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AlertIcon != null) {
                AlertIcon.Dispose ();
                AlertIcon = null;
            }

            if (AlertMessage != null) {
                AlertMessage.Dispose ();
                AlertMessage = null;
            }

            if (AlertTitle != null) {
                AlertTitle.Dispose ();
                AlertTitle = null;
            }

            if (OkButton != null) {
                OkButton.Dispose ();
                OkButton = null;
            }
        }
    }
}