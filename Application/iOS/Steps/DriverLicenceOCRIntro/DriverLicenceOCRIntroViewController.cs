﻿using System.Threading.Tasks;
using myGovID.iOS.Steps.HelpOverlay;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.DriverLicence;

namespace myGovID.iOS.Steps.DriverLicenceOCRIntro
{
    public partial class DriverLicenceOCRIntroViewController : StepViewController<IDriverLicenceOCRIntroStep, DriverLicenceOCRIntroViewModel, DriverLicenceOCRIntroAction>
    {
        private HelpOverlayPageViewController _pageViewController;
        protected override void OnViewModelLoaded()
        {
            _pageViewController = new HelpOverlayPageViewController(PageView, ViewModel.Pages, OnPageChanged);
            AddChildViewController(_pageViewController);
            _pageViewController.DidMoveToParentViewController(this);
            PageControl.CurrentPage = 0;
            PageControl.Pages = ViewModel.Pages.Length;
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(ScanButton, ActionType.Next, DriverLicenceOCRIntroAction.OCRCapture);
            Bind(EnterManuallyButton, ActionType.Next, DriverLicenceOCRIntroAction.ManualEntry);
            Bind(BackButton, ActionType.Previous);
        }
        private void OnPageChanged(int index)
        {
            PageControl.CurrentPage = index;
        }
    }
}

