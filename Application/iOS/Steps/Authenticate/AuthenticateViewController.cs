﻿using myGovID.Presentation.Contract.Steps.Authenticate;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Inputs;
using System.Linq;
using System.Threading.Tasks;

using UIKit;
using LocalAuthentication;
using Foundation;
using myGovID.Services.LocalAuthentication.Contract;

namespace myGovID.iOS.Steps.Authenticate
{
    public partial class AuthenticateViewController : StepViewController<IAuthenticateStep, AuthenticateViewModel, AuthenticateAction>
    {
        // For binding purposes
        private EnterExistingPasswordInput _passwordInput => (EnterExistingPasswordInput)ViewModel.Inputs.First(input => input.MetaData == InputMetaData.Password);
        private LAContext _context;
        private bool _hideControls = true;
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            View.Layer.Contents = UIImage.FromBundle("LaunchBackground").CGImage;
        }
        public override UIStatusBarStyle PreferredStatusBarStyle()
        {
            return UIStatusBarStyle.LightContent;
        }
        protected override void OnViewModelLoaded()
        {
            PasswordTextField.ShouldReturn = (textField) =>
            {
                textField.ResignFirstResponder();
                return true;
            };
            _context = new LAContext();
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(_passwordInput, PasswordTextField, PasswordErrorLabel);
            Bind(LoginButton, ActionType.Next, AuthenticateAction.Password);
        }
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            if (_hideControls)
            {
                ShowBiometricChallenge();
            }
        }
        protected override UIView GetPageErrorViewContainer()
        {
            return PageErrorViewContainer;
        }
        public LocalAuthenticationMethod GetSupportedAuthenticationMethod()
        {
            switch (_context.BiometryType)
            {
                case LABiometryType.None:
                    return LocalAuthenticationMethod.None;
                case LABiometryType.TouchId:
                    return LocalAuthenticationMethod.Touch;
                case LABiometryType.FaceId:
                    return LocalAuthenticationMethod.Face;
                default:
                    return LocalAuthenticationMethod.None;
            }
        }
        protected void LAContextReplyHandler(bool success, NSError error)
        {
            LAContextReplyCompletion(success);
        }
        private bool CanEvaluatePolicy()
        {
            bool success = _context.CanEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, out NSError error);
            return success;
        }
        private void EvaluatePolicy()
        {
            LocalAuthenticationMethod method = GetSupportedAuthenticationMethod();
            _context.EvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, string.Format("Login using your {0}", method == LocalAuthenticationMethod.Face ? "Face ID." : method == LocalAuthenticationMethod.Touch ? "Touch ID." : "password."), LAContextReplyHandler);
        }
        private void HideLoginUIControls(bool hide = true)
        {
            _hideControls = hide;
            PasswordTextField.Hidden = hide;
            PasswordErrorLabel.Hidden = hide;
            LoginButton.Hidden = hide;
            ForgottenPasswordButton.Hidden = hide;
        }
        private void LAContextReplyCompletion(bool success) 
        {
            InvokeOnMainThread(() =>
            {
                if (success)
                {
                    var action = FindAction(ActionType.Next, AuthenticateAction.Biometric);
                    ExecuteAction(action);
                }
                else
                {
                    HideLoginUIControls(false);
                }
            });
        }
        private void ShowBiometricChallenge()
        {
            if (Step.ChildStep == null)
            {
                if (ViewModel.UseBiometrics && CanEvaluatePolicy())
                {
                    HideLoginUIControls();
                    if (GetSupportedAuthenticationMethod() != LocalAuthenticationMethod.None)
                    {
                        EvaluatePolicy();
                    }
                }
                else
                {
                    HideLoginUIControls(false);
                }
            } 
            else
            {
                HideLoginUIControls();
            }
        }
    }
}