﻿using Foundation;
using myGovID.iOS.Constants;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.VersionUpgrade;
using UIKit;

namespace myGovID.iOS.Steps.VersionUpgrade
{
    public partial class VersionUpgradeViewController : StepViewController<IVersionUpgradeStep, VersionUpgradeViewModel>
    {
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            AlertIcon.Image = UIImage.FromBundle(Images.DeclinedAlert);
        }
        protected override void OnViewModelLoaded()
        {
            if (ViewModel.ForcedUpdate)
            {
                AlertTitle.Text = NSBundle.MainBundle.GetLocalizedString("version_update_required_alert_title", "Update available");
                AlertMessage.Text = NSBundle.MainBundle.GetLocalizedString("version_update_required_alert_message", "A newer version is available.\nYou must update to continue using myGovID.");
                UpdateButton.SetTitle(NSBundle.MainBundle.GetLocalizedString("version_update_required_alert_update_btn", "Update"), UIControlState.Normal);
                NotNowButton.Hidden = true;
            }
            else 
            {
                AlertTitle.Text = NSBundle.MainBundle.GetLocalizedString("version_update_alert_title", "Update available");
                AlertMessage.Text = NSBundle.MainBundle.GetLocalizedString("version_update_alert_message", "A newer version is available.\nWould you like to update now?");
                UpdateButton.SetTitle(NSBundle.MainBundle.GetLocalizedString("version_update_alert_update_btn", "Update"), UIControlState.Normal);
                NotNowButton.SetTitle(NSBundle.MainBundle.GetLocalizedString("version_update_alert_dismiss_btn", "Not now"), UIControlState.Normal);
                NotNowButton.Hidden = false;
            }

        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            //TODO: Bind Update button once we have the implementation for Upgrade. 
            //Bind(UpdateButton, ActionType.Next);
            Bind(NotNowButton, ActionType.Previous, null, "OnPreviousOverlay");
        }
    }
}