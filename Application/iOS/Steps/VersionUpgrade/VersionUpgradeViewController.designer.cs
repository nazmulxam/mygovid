// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace myGovID.iOS.Steps.VersionUpgrade
{
    [Register ("VersionUpgradeViewController")]
    partial class VersionUpgradeViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView AlertIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware AlertMessage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware AlertTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIButtonTypeAware NotNowButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIButtonTypeAware UpdateButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AlertIcon != null) {
                AlertIcon.Dispose ();
                AlertIcon = null;
            }

            if (AlertMessage != null) {
                AlertMessage.Dispose ();
                AlertMessage = null;
            }

            if (AlertTitle != null) {
                AlertTitle.Dispose ();
                AlertTitle = null;
            }

            if (NotNowButton != null) {
                NotNowButton.Dispose ();
                NotNowButton = null;
            }

            if (UpdateButton != null) {
                UpdateButton.Dispose ();
                UpdateButton = null;
            }
        }
    }
}