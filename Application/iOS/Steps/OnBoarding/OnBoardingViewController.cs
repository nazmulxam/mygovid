﻿using myGovID.Presentation.Contract.Steps.OnBoarding;
using myGovID.Presentation.Contract;
using myGovID.iOS.Steps.HelpOverlay;

namespace myGovID.iOS.Steps.OnBoarding
{
    public partial class OnBoardingViewController : StepViewController<IOnBoardingStep, OnBoardingViewModel>
    {
        private HelpOverlayPageViewController _pageViewController;
        protected override void OnViewModelLoaded()
        {
            _pageViewController = new HelpOverlayPageViewController(PageView, ViewModel.Pages, OnPageChanged);
            AddChildViewController(_pageViewController);
            _pageViewController.DidMoveToParentViewController(this);
            PageControl.CurrentPage = 0;
            PageControl.Pages = ViewModel.Pages.Length;
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(NextButton, ActionType.Next, default);
        }
        private void OnPageChanged(int index)
        {
            PageControl.CurrentPage = index;
            NextButton.Hidden = index != ViewModel.Pages.Length - 1;
        }
    }
}