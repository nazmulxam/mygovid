﻿using System.Linq;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.CapturePassword;
using Foundation;

namespace myGovID.iOS.Steps.CapturePassword
{
    public partial class CapturePasswordViewController : StepViewController<ICapturePasswordStep, CapturePasswordViewModel>
    {
        protected override void OnViewModelLoaded()
        {
            DescriptionLabel.Text = NSBundle.MainBundle.GetLocalizedString("password_desc", "");
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind((FormInput<string>)ViewModel.Inputs.First((input) => input.MetaData == InputMetaData.Password),
                 PasswordTF, PasswordError, PasswordTitle);
            Bind((FormInput<string>)ViewModel.Inputs.First((input) => input.MetaData == InputMetaData.ConfirmPassword),
                 ConfirmPasswordTextField, ConfirmPasswordError, ConfirmPasswordTitle);
            Bind(NextButton, ActionType.Next);
        }
    }
}