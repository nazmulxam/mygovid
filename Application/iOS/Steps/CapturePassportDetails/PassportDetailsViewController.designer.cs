﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace myGovID.iOS.Steps.CapturePassportDetails
{
    [Register ("PassportDetailsViewController")]
    partial class PassportDetailsViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem BackButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIButtonTypeAware ConfirmButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.RadioButton ConsentButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware ConsentErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware consentLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware DateOfBirthErrorField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIDateTextField DateOfBirthTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware DocumentNumberErrorField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UITextFieldTypeAware DocumentNumberTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware FamilyNameErrorField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UITextFieldTypeAware FamilyNameTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware GenderErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISegmentedControl GenderSegmentInput { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UITextFieldTypeAware GenderTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware GivenNameErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UITextFieldTypeAware GivenNameTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem HelpButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView PageErrorViewContainer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIButtonTypeAware RescanButton { get; set; }

        [Action ("OnGenderValueChanged:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void OnGenderValueChanged (UIKit.UISegmentedControl sender);

        void ReleaseDesignerOutlets ()
        {
            if (BackButton != null) {
                BackButton.Dispose ();
                BackButton = null;
            }

            if (ConfirmButton != null) {
                ConfirmButton.Dispose ();
                ConfirmButton = null;
            }

            if (ConsentButton != null) {
                ConsentButton.Dispose ();
                ConsentButton = null;
            }

            if (ConsentErrorLabel != null) {
                ConsentErrorLabel.Dispose ();
                ConsentErrorLabel = null;
            }

            if (consentLabel != null) {
                consentLabel.Dispose ();
                consentLabel = null;
            }

            if (DateOfBirthErrorField != null) {
                DateOfBirthErrorField.Dispose ();
                DateOfBirthErrorField = null;
            }

            if (DateOfBirthTextField != null) {
                DateOfBirthTextField.Dispose ();
                DateOfBirthTextField = null;
            }

            if (DocumentNumberErrorField != null) {
                DocumentNumberErrorField.Dispose ();
                DocumentNumberErrorField = null;
            }

            if (DocumentNumberTextField != null) {
                DocumentNumberTextField.Dispose ();
                DocumentNumberTextField = null;
            }

            if (FamilyNameErrorField != null) {
                FamilyNameErrorField.Dispose ();
                FamilyNameErrorField = null;
            }

            if (FamilyNameTextField != null) {
                FamilyNameTextField.Dispose ();
                FamilyNameTextField = null;
            }

            if (GenderErrorLabel != null) {
                GenderErrorLabel.Dispose ();
                GenderErrorLabel = null;
            }

            if (GenderSegmentInput != null) {
                GenderSegmentInput.Dispose ();
                GenderSegmentInput = null;
            }

            if (GenderTextField != null) {
                GenderTextField.Dispose ();
                GenderTextField = null;
            }

            if (GivenNameErrorLabel != null) {
                GivenNameErrorLabel.Dispose ();
                GivenNameErrorLabel = null;
            }

            if (GivenNameTextField != null) {
                GivenNameTextField.Dispose ();
                GivenNameTextField = null;
            }

            if (HelpButton != null) {
                HelpButton.Dispose ();
                HelpButton = null;
            }

            if (PageErrorViewContainer != null) {
                PageErrorViewContainer.Dispose ();
                PageErrorViewContainer = null;
            }

            if (RescanButton != null) {
                RescanButton.Dispose ();
                RescanButton = null;
            }
        }
    }
}