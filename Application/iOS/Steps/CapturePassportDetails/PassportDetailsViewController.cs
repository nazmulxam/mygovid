﻿using System;
using System.Linq;
using myGovID.iOS.Views.Common;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Presentation.Contract.Steps.Passport;
using UIKit;

namespace myGovID.iOS.Steps.CapturePassportDetails
{
    public partial class PassportDetailsViewController : StepViewController<ICapturePassportDetailsStep, PassportViewModel, CapturePassportDetailsAction>
    {

        private NameInput _givenNameInput => (NameInput)ViewModel.Inputs.First(input => input.MetaData == InputMetaData.GivenName);
        private NameInput _familyNameInput => (NameInput)ViewModel.Inputs.First(input => input.MetaData == InputMetaData.FamilyName);
        private BirthDateInput _dateOfBirthInput => (BirthDateInput)ViewModel.Inputs.First(input => input.MetaData == InputMetaData.DateOfBirth);
        private PassportDocumentNumberInput _passportDocumentInput => (PassportDocumentNumberInput)ViewModel.Inputs.First(input => input.MetaData == InputMetaData.PassportDocumentNumber);
        private GenderInput _genderInput => (GenderInput)ViewModel.Inputs.First(input => input.MetaData == InputMetaData.Gender);
        private ConsentInput _consentInput => (ConsentInput)ViewModel.Inputs.First(input => input.MetaData == InputMetaData.Consent);
        protected override void OnViewModelLoaded()
        {
            RescanButton.Hidden = !ViewModel.IsUsingOCR;
            ConsentButton.Selected = ViewModel.ConsentProvided.Value;
        }

        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(_givenNameInput, GivenNameTextField, GivenNameErrorLabel);
            Bind(_familyNameInput, FamilyNameTextField, FamilyNameErrorField);
            Bind(_dateOfBirthInput, DateOfBirthTextField, DateOfBirthErrorField);
            Bind(_passportDocumentInput, DocumentNumberTextField, DocumentNumberErrorField);
            Bind(_genderInput, GenderTextField, GenderErrorLabel);
            Bind(_consentInput, ConsentButton, consentLabel, ConsentErrorLabel);
            Bind(RescanButton, ActionType.Previous, CapturePassportDetailsAction.Rescan);
            Bind(ConfirmButton, ActionType.Next, CapturePassportDetailsAction.Submit);
            Bind(BackButton, ActionType.Previous, CapturePassportDetailsAction.Back);
            Bind(HelpButton, ActionType.Browse, CapturePassportDetailsAction.Help);
        }

        partial void OnGenderValueChanged(UISegmentedControl sender)
        {
            var index = GenderSegmentInput.SelectedSegment;
            switch (index)
            {
                case 0:
                    GenderTextField.Text = Gender.Male.ToString();
                    break;
                case 1:
                    GenderTextField.Text = Gender.Female.ToString();
                    break;
                case 2:
                    GenderTextField.Text = Gender.Unspecified.ToString();
                    break;
                default:
                    GenderTextField.Text = null;
                    break;
            }
            _genderInput.Value = GenderTextField.Text;
        }

        protected override UIView GetPageErrorViewContainer()
        {
            return PageErrorViewContainer;
        }
    }
}

