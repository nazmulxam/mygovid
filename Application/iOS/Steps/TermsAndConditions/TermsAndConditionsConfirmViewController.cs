using Foundation;
using myGovID.iOS.Constants;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.TermsAndConditionsConfirm;
using UIKit;

namespace myGovID.iOS.Steps.TermsAndConditionsConfirm
{
    public partial class TermsAndConditionsConfirmViewController : StepViewController<ITermsAndConditionsConfirmStep, TermsAndConditionsConfirmViewModel>
    {
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            OkButton.SetTitle(NSBundle.MainBundle.GetLocalizedString("tnc_alert_decline_btn", "Ok"), UIControlState.Normal);
            BackButton.SetTitle(NSBundle.MainBundle.GetLocalizedString("tnc_alert_back_btn", "Back"), UIControlState.Normal);
            AlertIcon.Image = UIImage.FromBundle(Images.DeclinedAlert);
            AlertTitle.Text = NSBundle.MainBundle.GetLocalizedString("tnc_title", "Terms of use");
            AlertMessage.Text = NSBundle.MainBundle.GetLocalizedString("tnc_decline_description", "By declining the terms of use, you will not be able to create a myGovID account and access online government services.");
        }
        protected override void OnViewModelLoaded() 
        {
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(OkButton, ActionType.Next, null, "OnNextOverlay");
            Bind(BackButton, ActionType.Previous, null, "OnPreviousOverlay");
        }
    }
}

