// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace myGovID.iOS.Steps.TermsAndConditions
{
    [Register ("TermsAndConditionsViewController")]
    partial class TermsAndConditionsViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIButtonTypeAware AcceptButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIButtonTypeAware DeclineButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextView TermsAndConditionsTextView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware TermsOfUseLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AcceptButton != null) {
                AcceptButton.Dispose ();
                AcceptButton = null;
            }

            if (DeclineButton != null) {
                DeclineButton.Dispose ();
                DeclineButton = null;
            }

            if (TermsAndConditionsTextView != null) {
                TermsAndConditionsTextView.Dispose ();
                TermsAndConditionsTextView = null;
            }

            if (TermsOfUseLabel != null) {
                TermsOfUseLabel.Dispose ();
                TermsOfUseLabel = null;
            }
        }
    }
}