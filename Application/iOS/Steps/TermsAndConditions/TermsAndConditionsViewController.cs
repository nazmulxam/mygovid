﻿using Foundation;
using myGovID.iOS.Constants;
using myGovID.iOS.Views.Common;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.TermsAndConditions;
using UIKit;

namespace myGovID.iOS.Steps.TermsAndConditions
{
    public partial class TermsAndConditionsViewController : StepViewController<ITermsAndConditionsStep, TermsAndConditionsViewModel>
    {
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            View.BackgroundColor = ColorsHelper.BackgroundColor;
            AcceptButton.SetTitle(NSBundle.MainBundle.GetLocalizedString("tnc_accept_btn", "Accept"), UIControlState.Normal);
            DeclineButton.SetTitle(NSBundle.MainBundle.GetLocalizedString("tnc_decline_btn", "Decline"), UIControlState.Normal);

        }
        protected override void OnViewModelLoaded()
        {
            if (ViewModel.IsHtml)
            {
                new ThemedHtmlParser(ViewModel.TermsAndConditions)
                    .BodyFontColor(UIColor.White)
                    .LinkColor(UIColor.White)
                    .LinkFont(FontType.Bold)
                    .LinkUnderline(false)
                    .ApplyToTextView(TermsAndConditionsTextView);
            }
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(AcceptButton, ActionType.Next);
            Bind(DeclineButton, ActionType.Cancel);
        }
    }
}

