﻿using System;
using System.Linq;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.CaptureEmailVerification;
using UIKit;

namespace myGovID.iOS.Steps.VerifyEmail
{
    public partial class VerifyEmailViewController : StepViewController<ICaptureEmailVerificationStep, CaptureEmailVerificationViewModel>
    {
        protected override void OnViewModelLoaded()
        {
            HeadingLabel.Text = "Verify your email";
            BodyLine1Label.Text = "Enter the 6 digit code sent to your email:";
            BodyLine2Label.Text = ViewModel.EmailAddress;
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            FormInput<string> formInput = (FormInput<string>)ViewModel.Inputs.First();
            Bind(formInput, CodeTextField.TextField, CodeErrorLabel);
            Bind(BackButton, ActionType.Previous);
            Bind(NextButton, ActionType.Next);
            Bind(ResendButton, ActionType.Clone);
        }
        protected override UIView GetPageErrorViewContainer()
        {
            return PageErrorViewContainer;
        }
    }
}