﻿using UIKit;
using myGovID.Presentation.Contract.Steps.HelpOverlay;

namespace myGovID.iOS.Steps.HelpOverlay
{
    public partial class HelpOverlayContentViewController : UIViewController
    {
        public IHelpOverlayModel Model { get; set; }
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            if (Model != null)
            {
                TitleLabel.Text = Model.Title;
                DescriptionLabel.Text = Model.Description;
                Image.Image = UIImage.FromBundle(Model.Image);
            }
        }
    }
}

