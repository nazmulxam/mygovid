﻿using System;
using UIKit;

namespace myGovID.iOS.Steps.HelpOverlay
{
    public class HelpOverlayPageViewDelegate : UIPageViewControllerDelegate
    {
        private readonly HelpOverlayContentViewController[] _pages;
        private readonly Action<int> _onPageChanged;
        public HelpOverlayPageViewDelegate(HelpOverlayContentViewController[] pages, Action<int> onPageChanged)
        {
            this._pages = pages;
            _onPageChanged = onPageChanged;
        }
        public override void DidFinishAnimating(UIPageViewController pageViewController, bool finished, UIViewController[] previousViewControllers, bool completed)
        {
            if (pageViewController.ViewControllers.Length > 0)
            {
                UIViewController viewController = pageViewController.ViewControllers[0];
                int index = Array.FindIndex(_pages, (page) => page.Equals((HelpOverlayContentViewController)viewController));
                if (_onPageChanged != null && completed)
                {
                    _onPageChanged(index);
                }
            }
        }
    }
}
