﻿using System;
using UIKit;

namespace myGovID.iOS.Steps.HelpOverlay
{
    public class HelpOverlayPageDataSource: UIPageViewControllerDataSource
    {
        private readonly HelpOverlayContentViewController[] _pages;
        public HelpOverlayPageDataSource(HelpOverlayContentViewController[] pages)
        {
            this._pages = pages;
        }
        public override UIViewController GetPreviousViewController(UIPageViewController pageViewController, UIViewController referenceViewController)
        {
            int index = Array.FindIndex(_pages, (page) => page.Equals((HelpOverlayContentViewController)referenceViewController));
            return index == 0 ? null : _pages[index - 1];
        }
        public override UIViewController GetNextViewController(UIPageViewController pageViewController, UIViewController referenceViewController)
        {
            int index = Array.FindIndex(_pages, (page) => page.Equals((HelpOverlayContentViewController)referenceViewController));
            return index < _pages.Length - 1 ? _pages[index + 1] : null;
        }
    }
}
