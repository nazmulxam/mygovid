﻿using System;
using UIKit;
using myGovID.Presentation.Contract.Steps.HelpOverlay;

namespace myGovID.iOS.Steps.HelpOverlay
{
    public class HelpOverlayPageViewController : UIPageViewController
    {
        public HelpOverlayPageViewController(UIView parentView, IHelpOverlayModel[] overlayModels, Action<int> onPageChanged) : base(UIPageViewControllerTransitionStyle.Scroll, UIPageViewControllerNavigationOrientation.Horizontal)
        {
            View.Frame = parentView.Frame;
            parentView.AddSubview(View);
            HelpOverlayContentViewController[] pages = new HelpOverlayContentViewController[overlayModels.Length];
            int index = 0;
            foreach (IHelpOverlayModel model in overlayModels)
            {
                HelpOverlayContentViewController pageContentViewController = new HelpOverlayContentViewController
                {
                    Model = model
                };
                pages[index++] = pageContentViewController;
            }
            DataSource = new HelpOverlayPageDataSource(pages);
            Delegate = new HelpOverlayPageViewDelegate(pages, onPageChanged);
            SetViewControllers(new UIViewController[] { pages[0] }, UIPageViewControllerNavigationDirection.Forward, false, null);
        }
    }
}
