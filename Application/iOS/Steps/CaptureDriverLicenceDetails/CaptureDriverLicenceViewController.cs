﻿using System;
using System.Linq;
using myGovID.iOS.Views.Common;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Presentation.Contract.Steps.DriverLicence;
using UIKit;

namespace myGovID.iOS.Steps.CaptureDriverLicence
{
    public partial class CaptureDriverLicenceViewController : StepViewController<ICaptureDriverLicenceDetailsStep, DriverLicenceViewModel, CaptureDriverLicenceDetailsAction>
    {
        protected override void OnViewModelLoaded() { }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(ViewModel.DriverLicenceDocumentNumber, DocumentNumberTextField, DocumentNumberErrorField);
            Bind(ViewModel.GivenName, GivenNamesTextField, GivenNamesErrorLabel);
            Bind(ViewModel.MiddleName, MiddleNameTextField, MiddleNameErrorField);
            Bind(ViewModel.FamilyName, FamilyNameTextField, FamilyNameErrorField);
            Bind(ViewModel.DateOfBirth, DateOfBirthTextField, DateOfBirthErrorField);
            Bind(ViewModel.State, StateTextField, StateErrorLabel);
            Bind(ViewModel.ConsentProvided, ConsentButton, consentLabel, ConsentErrorLabel);
            Bind(ConfirmButton, ActionType.Next, CaptureDriverLicenceDetailsAction.Submit);
            Bind(BackButton, ActionType.Previous, CaptureDriverLicenceDetailsAction.Back);
            Bind(HelpButton, ActionType.Browse, CaptureDriverLicenceDetailsAction.Help);
        }

        partial void ClickState(UITextFieldTypeAware sender)
        {
            var statePickerView = new UIPickerView();

            var statePickerDataModel = new PickerViewModel(ViewModel.State.Options.ToList());
            StateTextField.InputView = statePickerView;
            statePickerView.Model = statePickerDataModel;
            StateTextField.Text = statePickerDataModel.SelectedItem;
            ViewModel.State.Value = StateTextField.Text;
            statePickerDataModel.ValueChanged += (pickerSender, args) =>
            {
                StateTextField.Text = ((PickerViewModel)pickerSender).SelectedItem;
            };
        }
        protected override UIView GetPageErrorViewContainer()
        {
            return PageErrorViewContainer;
        }
    }
}

