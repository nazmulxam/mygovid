﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace myGovID.iOS.Steps.CaptureDriverLicence
{
    [Register ("CaptureDriverLicenceViewController")]
    partial class CaptureDriverLicenceViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem BackButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIButtonTypeAware ConfirmButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.RadioButton ConsentButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware ConsentErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware consentLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware DateOfBirthErrorField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIDateTextField DateOfBirthTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware DocumentNumberErrorField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UITextFieldTypeAware DocumentNumberTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware FamilyNameErrorField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UITextFieldTypeAware FamilyNameTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware GivenNamesErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UITextFieldTypeAware GivenNamesTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem HelpButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware MiddleNameErrorField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UITextFieldTypeAware MiddleNameTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView PageErrorViewContainer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIButtonTypeAware RescanButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware StateErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UITextFieldTypeAware StateTextField { get; set; }

        [Action ("ClickState:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void ClickState (myGovID.iOS.Views.Common.UITextFieldTypeAware sender);

        void ReleaseDesignerOutlets ()
        {
            if (BackButton != null) {
                BackButton.Dispose ();
                BackButton = null;
            }

            if (ConfirmButton != null) {
                ConfirmButton.Dispose ();
                ConfirmButton = null;
            }

            if (ConsentButton != null) {
                ConsentButton.Dispose ();
                ConsentButton = null;
            }

            if (ConsentErrorLabel != null) {
                ConsentErrorLabel.Dispose ();
                ConsentErrorLabel = null;
            }

            if (consentLabel != null) {
                consentLabel.Dispose ();
                consentLabel = null;
            }

            if (DateOfBirthErrorField != null) {
                DateOfBirthErrorField.Dispose ();
                DateOfBirthErrorField = null;
            }

            if (DateOfBirthTextField != null) {
                DateOfBirthTextField.Dispose ();
                DateOfBirthTextField = null;
            }

            if (DocumentNumberErrorField != null) {
                DocumentNumberErrorField.Dispose ();
                DocumentNumberErrorField = null;
            }

            if (DocumentNumberTextField != null) {
                DocumentNumberTextField.Dispose ();
                DocumentNumberTextField = null;
            }

            if (FamilyNameErrorField != null) {
                FamilyNameErrorField.Dispose ();
                FamilyNameErrorField = null;
            }

            if (FamilyNameTextField != null) {
                FamilyNameTextField.Dispose ();
                FamilyNameTextField = null;
            }

            if (GivenNamesErrorLabel != null) {
                GivenNamesErrorLabel.Dispose ();
                GivenNamesErrorLabel = null;
            }

            if (GivenNamesTextField != null) {
                GivenNamesTextField.Dispose ();
                GivenNamesTextField = null;
            }

            if (HelpButton != null) {
                HelpButton.Dispose ();
                HelpButton = null;
            }

            if (MiddleNameErrorField != null) {
                MiddleNameErrorField.Dispose ();
                MiddleNameErrorField = null;
            }

            if (MiddleNameTextField != null) {
                MiddleNameTextField.Dispose ();
                MiddleNameTextField = null;
            }

            if (PageErrorViewContainer != null) {
                PageErrorViewContainer.Dispose ();
                PageErrorViewContainer = null;
            }

            if (RescanButton != null) {
                RescanButton.Dispose ();
                RescanButton = null;
            }

            if (StateErrorLabel != null) {
                StateErrorLabel.Dispose ();
                StateErrorLabel = null;
            }

            if (StateTextField != null) {
                StateTextField.Dispose ();
                StateTextField = null;
            }
        }
    }
}