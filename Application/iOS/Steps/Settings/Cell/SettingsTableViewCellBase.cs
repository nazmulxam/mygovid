﻿using System;
using System.Collections.Generic;
using Foundation;
using GalaSoft.MvvmLight.Helpers;
using myGovID.Presentation.Contract.Steps.Settings;
using ObjCRuntime;
using UIKit;

namespace myGovID.iOS.Steps.Settings.Cell
{
    public abstract class SettingsTableViewCellBase<TBase, TModel> : UITableViewCell
        where TBase : SettingsTableViewCellBase<TBase, TModel>
        where TModel : SettingsSectionBase
    {
        private static readonly NSString Key = new NSString(typeof(TBase).Name);

        protected TModel Item;
        protected Action<TModel> Callback;
        protected IList<Binding> BindingList = new List<Binding>();

        protected SettingsTableViewCellBase(IntPtr handle) : base(handle)
        {
            SelectionStyle = UITableViewCellSelectionStyle.None;
        }

        public void Unbind()
        {
            foreach (var item in BindingList)
            {
                item.Detach();
            }
        }

        public void Bind(TModel item)
        {
            Item = item;

            CheckItemIsEnabled();

            Unbind();

            BindSetup();
        }

        public override void RemoveFromSuperview()
        {
            base.RemoveFromSuperview();
            Unbind();
        }

        public void SetCallback(Action<TModel> callback)
        {
            Callback = callback;
        }

        public static TBase Create()
        {
            NSArray topLevelObjects = NSBundle.MainBundle.LoadNib(Key, null, null);
            return Runtime.GetNSObject(topLevelObjects.ValueAt(0)) as TBase;
        }

        protected abstract void BindSetup();

        protected void CheckItemIsEnabled()
        {
            UserInteractionEnabled = Item.Enabled;
            foreach (var item in this.Subviews)
            {
                item.Alpha = Item.Enabled ? 1.0f : 0.4f;
            }
        }
    }
}
