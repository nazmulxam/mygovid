﻿using System;
using myGovID.Presentation.Contract.Steps.Settings;

namespace myGovID.iOS.Steps.Settings.Cell
{
    public partial class SettingsTableViewCell : SettingsTableViewCellBase<SettingsTableViewCell, SettingsSectionItem>
    {
        protected SettingsTableViewCell(IntPtr handle) : base(handle)
        {
        }

        protected override void BindSetup()
        {
            lblTitle.Text = Item.Title;
        }
    }
}
