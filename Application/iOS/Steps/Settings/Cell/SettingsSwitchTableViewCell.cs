﻿using System;
using GalaSoft.MvvmLight.Helpers;
using myGovID.Presentation.Contract.Steps.Settings;
using UIKit;

namespace myGovID.iOS.Steps.Settings.Cell
{
    public partial class SettingsSwitchTableViewCell : SettingsTableViewCellBase<SettingsSwitchTableViewCell, SettingsSectionSwitchItem>
    {
        protected SettingsSwitchTableViewCell(IntPtr handle) : base(handle)
        {
        }

        protected override void BindSetup()
        {
            lblTitle.Text = Item.Title;
            uiSwitch.On = Item.Selected;

            var uiSwitchChangedBinding = new Binding<bool, bool>(uiSwitch, () => uiSwitch.On).WhenSourceChanges(() =>
            {
                if (Item.Selected == uiSwitch.On) return;
                Item.Selected = uiSwitch.On;
                Callback.Invoke(Item);
            });
            BindingList.Add(uiSwitchChangedBinding);

            var switchBinding = new Binding<bool, bool>(Item, () => Item.Selected, uiSwitch, () => uiSwitch.On, BindingMode.TwoWay);
            BindingList.Add(switchBinding);
        }
    }
}
