﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using myGovID.iOS.Steps.Settings.Cell;
using myGovID.Presentation.Contract.Steps.Settings;
using UIKit;

namespace myGovID.iOS.Steps.Settings
{
    public class SettingsTableSource : UITableViewSource
    {
        private IList<SettingsSection> _tableItems;
        private readonly string _cellIdentifier = typeof(SettingsTableViewCell).Name;
        private Action<SettingsSectionItem> _callback;

        public SettingsTableSource SetCallback(Action<SettingsSectionItem> callback)
        {
            _callback = callback;
            return this;
        }

        public SettingsTableSource SetItems(IEnumerable<SettingsSection> items)
        {
            _tableItems = items.ToList();
            return this;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return 60;
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return _tableItems.Count;
        }

        public override string TitleForHeader(UITableView tableView, nint section)
        {
            var group = _tableItems[(int)section];
            return group.Header;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            var group = _tableItems[(int)section];
            return group.ItemList.Count;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            var group = _tableItems[indexPath.Section];
            var item = group.ItemList[indexPath.Row];
            if (item.Clickable)
            {
                _callback?.Invoke(item);
            }
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var group = _tableItems[(int)indexPath.Section];
            var item = group.ItemList[indexPath.Row];

            switch (item)
            {
                case SettingsSectionSwitchItem switchItem:
                    var switchViewCell = GetCell<SettingsSwitchTableViewCell, SettingsSectionSwitchItem>(tableView);
                    switchViewCell.Bind(switchItem);
                    switchViewCell.SetCallback(_callback);
                    return switchViewCell;

                default:
                    var viewCell = GetCell<SettingsTableViewCell, SettingsSectionItem>(tableView);
                    viewCell.Bind(item);
                    return viewCell;
            }
        }

        private TCell GetCell<TCell, TModel>(UITableView tableView)
            where TCell : SettingsTableViewCellBase<TCell, TModel>
            where TModel : SettingsSectionBase
        {
            return (TCell)tableView.DequeueReusableCell(_cellIdentifier)
                               ?? SettingsTableViewCellBase<TCell, TModel>.Create();
        }
    }
}
