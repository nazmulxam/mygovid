﻿using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Dashboard;
using myGovID.Presentation.Contract.Steps.Settings;

namespace myGovID.iOS.Steps.Settings
{
    public partial class SettingsViewController : StepViewController<ISettingsStep, SettingsViewModel, SettingsAction>
    {
        protected override void OnViewModelLoaded()
        {
            tableView.Source = new SettingsTableSource()
                                        .SetItems(ViewModel.SettingsSectionList)
                                        .SetCallback(RunAction);
        }

        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(BackButton, ActionType.Previous);
        }

        private void RunAction(SettingsSectionItem item)
        {
            var action = FindAction(item.Action, item.SettingsAction);
            ExecuteAction(action);
        }
    }
}

