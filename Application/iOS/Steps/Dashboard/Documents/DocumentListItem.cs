using System;
using Foundation;
using myGovID.iOS.Views.Common;
using myGovID.Presentation.Contract.Steps.Dashboard;
using myGovID.Presentation.Contract.Steps.Dashboard.Document;
using UIKit;

namespace myGovID.iOS
{
    public partial class DocumentListItem : NSObject
    {
        public static DocumentListItem Build(IDocument document)
        {
            var instance = new DocumentListItem();
            NSBundle.MainBundle.LoadNib("DocumentListItem", instance, null);
            instance.ViewDidLoad();
            instance.LoadDocument(document);
            return instance;
        }

        public DashboardAction Action { get; protected set; }

        public DocumentListItem (IntPtr handle) : base (handle)
        {
        }

        protected DocumentListItem()
        {
        }

        protected void ViewDidLoad()
        {
            View.Layer.BorderWidth = 1;
            View.Layer.BorderColor = ColorsHelper.DocumentListItemBorderColor.CGColor;
        }

        public void LoadDocument(IDocument document) {
            TitleLabel.Text = document.Title;
            DescriptionLabel.Text = document.Description;
            ImageView.Image = UIImage.FromBundle(document.Image);
            Action = document.Action;
            View.SetNeedsLayout();
        }

        public UIView GetView()
        {
            return View;
        }
    }
}