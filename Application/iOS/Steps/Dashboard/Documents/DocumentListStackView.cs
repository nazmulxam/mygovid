﻿using System;
using Foundation;
using UIKit;
using myGovID.Presentation.Contract.Steps.Dashboard.Document;
using System.Collections.Generic;
using CoreGraphics;
using myGovID.iOS.Steps.Dashboard;
using myGovID.Presentation.Contract;
using System.Linq;

namespace myGovID.iOS.Views.Documents
{
    [Register("DocumentListStackView")]
    public class DocumentListStackView : UIStackView
    {

        public IEnumerable<DocumentListItem> Documents { get; protected set; }

        public DocumentListStackView()
        {
            CommonInit();
        }

        public DocumentListStackView(IntPtr handle) : base(handle)
        {
            CommonInit();
        }

        public DocumentListStackView(NSCoder coder) : base(coder)
        {
            CommonInit();
        }

        void CommonInit()
        {
            Distribution = UIStackViewDistribution.EqualSpacing;
            Alignment = UIStackViewAlignment.Fill;
            Spacing = 18;
            Axis = UILayoutConstraintAxis.Vertical;
            TranslatesAutoresizingMaskIntoConstraints = false;
        }

        public void LoadDocuments(IEnumerable<IDocument> documents)
        {
            foreach (var view in ArrangedSubviews)
            {
                RemoveArrangedSubview(view);
            }

            Documents = documents.Select(DocumentListItem.Build).ToList();

            foreach (var document in Documents)
            {
                AddArrangedSubview(document.GetView());
            }
        }
    }
}
