﻿using System;
using Foundation;
using GalaSoft.MvvmLight.Helpers;
using myGovID.iOS.Constants;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Dashboard;
using UIKit;

namespace myGovID.iOS.Steps.Dashboard
{
    public partial class DashboardViewController : StepViewController<IDashboardStep, DashboardViewModel, DashboardAction>, IUITextViewDelegate
    {
        private NSUrl ShowTutorialUrl;
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            View.Layer.Contents = UIImage.FromBundle(Images.LaunchBackground).CGImage;
            NextStepsDescriptionTextView.Delegate = this;
        }

        public override UIStatusBarStyle PreferredStatusBarStyle()
        {
            return UIStatusBarStyle.LightContent;
        }

        [Export("textView:shouldInteractWithURL:inRange:interaction:")]
        public bool ShouldInteractWithUrl(UITextView textView, NSUrl url, NSRange characterRange, UITextItemInteraction interaction)
        {
            if (url == ShowTutorialUrl)
            {
                TriggerTutorialLink();
            }
            return false;
        }

        protected override void OnViewModelLoaded()
        {
            ShowTutorialUrl = new NSUrl(ViewModel.TutorialLink);
            IdentityStrengthBar.Strength = ViewModel.AuthStrengthType;
            IdentityStrengthLabel.Text = ViewModel.AuthStrengthType.ToString();
            DocumentListStackView.LoadDocuments(ViewModel.Documents);
            RenderNextSteps(ViewModel.NextSteps);
        }

        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(SettingsTabBarButton, ActionType.Next, DashboardAction.Settings);

            NameLabel.AccessibilityIdentifier = "Name";
            NameLabel.Text = ViewModel.WelcomeMessage;
            AddBinding(ViewModel.SetBinding(() => ViewModel.WelcomeMessage, NameLabel, () => NameLabel.Text));

            BindAuthentationStrength(() =>
            {
                InvokeOnMainThread(() =>
                {
                    AuthStrengthType authStrengthType = ViewModel.AuthStrengthType;
                    IdentityStrengthBar.Strength = authStrengthType;
                    IdentityStrengthLabel.Text = authStrengthType.ToString();
                    RenderNextSteps(ViewModel.NextSteps);
                });
            });
            foreach (var item in DocumentListStackView.Documents)
            {
                Bind(item.GetView(), ActionType.Next, item.Action);
            }
        }

        protected void RenderNextSteps(string nextSteps)
        {
            // For links to work we need Selectable == true & Editable == false
            NextStepsDescriptionTextView.Selectable = true;
            NextStepsDescriptionTextView.Editable = false;
            RenderHtml(NextStepsDescriptionTextView, nextSteps);
        }

        protected void TriggerTutorialLink()
        {
            var action = FindAction(ActionType.Next, DashboardAction.Tutorial);
            ExecuteAction(action);
        }
        private void BindAuthentationStrength(Action action)
        {
            Binding authBinding = new Binding<AuthStrengthType, AuthStrengthType>(ViewModel, nameof(ViewModel.AuthStrengthType)).WhenSourceChanges(action);
            AddBinding(authBinding);
        }
    }
}