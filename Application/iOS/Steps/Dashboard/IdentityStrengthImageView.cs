﻿using System;
using System.ComponentModel;
using Foundation;
using myGovID.iOS.Constants;
using myGovID.Presentation.Contract.Steps.Dashboard;
using UIKit;

namespace myGovID.iOS.Steps.Dashboard
{
    [Register("IdentityStrengthImageView"), DesignTimeVisible(true)]
    public class IdentityStrengthImageView : UIImageView
    {
        private AuthStrengthType _strength;

        public IdentityStrengthImageView(IntPtr p) : base(p)
        {
        }

        public AuthStrengthType Strength
        {
            get { return _strength; }
            set
            {
                _strength = value;
                UpdateImage();
            }
        }

        private void UpdateImage()
        {
            switch (_strength)
            {
                case AuthStrengthType.Unknown:
                    Image = UIImage.FromBundle(Images.IdentityBarUnknown);
                    break;
                case AuthStrengthType.Weak:
                    Image = UIImage.FromBundle(Images.IdentityBarWeak);
                    break;
                case AuthStrengthType.Intermediate:
                    Image = UIImage.FromBundle(Images.IdentityBarIntermediate);
                    break;
                case AuthStrengthType.Strong:
                    Image = UIImage.FromBundle(Images.IdentityBarStrong);
                    break;
            }
        }
    }
}
