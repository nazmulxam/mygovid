﻿using System;
using System.ComponentModel;
using CoreGraphics;
using Foundation;
using myGovID.iOS.Constants;
using myGovID.iOS.Views.Common;
using UIKit;

namespace myGovID.iOS.Views
{
    [Register("UIDashboardTabBarButton"), DesignTimeVisible(true)]
    public class DashboardTabBarButton : UIButton
    {
        // Parameters to customise the look
        private readonly nfloat Padding = 4;
        private const FontSize LabelFontSize = FontSize.Small;
        private const string FontFamily = FontType.Bold;

        public DashboardTabBarButton() : base()
        {
            CommonInit();
        }

        public DashboardTabBarButton(IntPtr p) : base(p)
        {
            CommonInit();
        }

        public DashboardTabBarButton(CGRect rect) : base(rect)
        {
            CommonInit();
        }

        public DashboardTabBarButton(NSCoder coder) : base(coder)
        {
            CommonInit();
        }

        void CommonInit()
        {
            // Adjust backdround
            Layer.BorderWidth = 0.4f;
            Layer.BorderColor = ColorsHelper.DashboardTabButtonBorderColor.CGColor;
            BackgroundColor = ColorsHelper.DashboardTabButtonBackgroundColor;

            // Adjust title label
            TitleLabel.TextAlignment = UITextAlignment.Center;
            TitleLabel.ClipsToBounds = false;
            TitleLabel.Font = UIFont.FromName(FontFamily, (int) LabelFontSize);
            TintColor = ColorsHelper.TabBarTitleColor;
            SetTitleColor(ColorsHelper.TabBarTitleColor, UIControlState.Normal);

            // Show a slight highlight when tapped
            ShowsTouchWhenHighlighted = true;
        }

        public override CGRect TitleRectForContentRect(CGRect rect)
        {
            var fontSize = (int) LabelFontSize;
            return new CGRect(0, rect.Height - fontSize - Padding, rect.Width, fontSize);
        }

        public override CGRect ImageRectForContentRect(CGRect rect)
        {
            var titleRect = TitleRectForContentRect(rect);
            var baseImageRect = base.ImageRectForContentRect(rect.Inset(0, -titleRect.Height - Padding));
            // Images are actually 22x22px, the app is stretching them to 25x25px
            baseImageRect.Width = (nfloat) Math.Max(baseImageRect.Width, 25);
            baseImageRect.Height = (nfloat) Math.Max(baseImageRect.Height, 25);
            var x = (rect.Width - baseImageRect.Width) / 2;
            var y = (rect.Height - titleRect.Height - baseImageRect.Height - Padding) / 2;
            return new CGRect(x, y, baseImageRect.Width, baseImageRect.Height);
        }

        public override CGSize IntrinsicContentSize
        {
            get
            {
                var baseContentSize = base.IntrinsicContentSize;
                if (ImageView == null || ImageView.Image == null)
                {
                    return baseContentSize;
                }

                if (TitleLabel == null)
                {
                    return new CGSize(baseContentSize.Width, ImageView.Image.Size.Height);
                }

                var titleSize = TitleLabel.SizeThatFits(new CGSize(ContentRectForBounds(Bounds).Width, Double.MaxValue));
                return new CGSize(
                    Math.Max(baseContentSize.Width, titleSize.Width),
                    ImageView.Image.Size.Height + Padding + titleSize.Height + Padding
                );
            }
        }
    }
}
