// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace myGovID.iOS.Steps.Dashboard
{
    [Register ("DashboardViewController")]
    partial class DashboardViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.DashboardTabBarButton AboutTabBarButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.DashboardTabBarButton AccountTabBarButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Documents.DocumentListStackView DocumentListStackView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Steps.Dashboard.IdentityStrengthImageView IdentityStrengthBar { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware IdentityStrengthLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware NameLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextView NextStepsDescriptionTextView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.DashboardTabBarButton SettingsTabBarButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AboutTabBarButton != null) {
                AboutTabBarButton.Dispose ();
                AboutTabBarButton = null;
            }

            if (AccountTabBarButton != null) {
                AccountTabBarButton.Dispose ();
                AccountTabBarButton = null;
            }

            if (DocumentListStackView != null) {
                DocumentListStackView.Dispose ();
                DocumentListStackView = null;
            }

            if (IdentityStrengthBar != null) {
                IdentityStrengthBar.Dispose ();
                IdentityStrengthBar = null;
            }

            if (IdentityStrengthLabel != null) {
                IdentityStrengthLabel.Dispose ();
                IdentityStrengthLabel = null;
            }

            if (NameLabel != null) {
                NameLabel.Dispose ();
                NameLabel = null;
            }

            if (NextStepsDescriptionTextView != null) {
                NextStepsDescriptionTextView.Dispose ();
                NextStepsDescriptionTextView = null;
            }

            if (SettingsTabBarButton != null) {
                SettingsTabBarButton.Dispose ();
                SettingsTabBarButton = null;
            }
        }
    }
}