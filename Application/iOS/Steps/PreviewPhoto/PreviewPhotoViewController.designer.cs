// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace myGovID.iOS.Steps.PreviewPhoto
{
    [Register ("PreviewPhotoViewController")]
    partial class PreviewPhotoViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ConsentButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel ConsentErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ImageView { get; set; }

        [Action ("OnCloseButtonClicked:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void OnCloseButtonClicked (UIKit.UIButton sender);

        [Action ("OnConfirmButtonClicked:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void OnConfirmButtonClicked (UIKit.UIButton sender);

        [Action ("OnConsentButtonClicked:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void OnConsentButtonClicked (UIKit.UIButton sender);

        [Action ("OnRetakeButtonClicked:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void OnRetakeButtonClicked (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (ConsentButton != null) {
                ConsentButton.Dispose ();
                ConsentButton = null;
            }

            if (ConsentErrorLabel != null) {
                ConsentErrorLabel.Dispose ();
                ConsentErrorLabel = null;
            }

            if (ImageView != null) {
                ImageView.Dispose ();
                ImageView = null;
            }
        }
    }
}