﻿using System;
using UIKit;
using myGovID.Presentation.Contract.Steps.PreviewPhoto;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Inputs;
using System.Linq;

namespace myGovID.iOS.Steps.PreviewPhoto
{
    public partial class PreviewPhotoViewController : StepViewController<IPreviewPhotoStep, PreviewPhotoViewModel>
    {
        public bool IsConsentAccepted { get; set; }
        public bool HasError { get => _consentInput.Error == null; }
        private BooleanInput _consentInput => (BooleanInput) ViewModel.Inputs.First();
        protected override void OnViewModelLoaded()
        {
            try
            {
                ImageView.Image = Base64.ToImage(ViewModel.ImageInBase64);
            }
            catch (Exception exception)
            {
                TransitionOnError(exception);
            }
        }
        partial void OnConfirmButtonClicked(UIButton sender)
        {
            if (IsConsentAccepted)
            {
                var action = FindAction(ActionType.Next);
                //action?.Run(new Promise<IStep>(OnTransition, OnError));
            } else {
                ConsentErrorLabel.Hidden = IsConsentAccepted;
            }
        }
        partial void OnConsentButtonClicked(UIButton sender)
        {
            sender.Selected = !sender.Selected;
            IsConsentAccepted = sender.Selected;
            ConsentErrorLabel.Hidden = IsConsentAccepted;
            string imageName = sender.Selected ? "CheckboxWhiteOn" : "CheckboxWhiteOff";
            ConsentButton.SetImage(UIImage.FromBundle(imageName), UIControlState.Normal);
        }
        partial void OnRetakeButtonClicked(UIButton sender)
        {
            var action = FindAction(ActionType.Previous);
            //action?.Run(new Promise<IStep>(OnTransition, OnError));
        }
        partial void OnCloseButtonClicked(UIButton sender)
        {
            var action = FindAction(ActionType.Cancel);
            //action?.Run(new Promise<IStep>(OnTransition, OnError));
        }
    }
}
