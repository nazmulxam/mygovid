using System;
using Foundation;
using myGovID.Contract.Steps.CapturePersonalDetails;
using myGovID.iOS.Steps.AccountSetup;
using myGovID.iOS.Steps.Authenticate;
using myGovID.iOS.Steps.CaptureDriverLicence;
using myGovID.iOS.Steps.CaptureEmail;
using myGovID.iOS.Steps.CapturePassportDetails;
using myGovID.iOS.Steps.CapturePassword;
using myGovID.iOS.Steps.CapturePersonalDetails;
using myGovID.iOS.Steps.Dashboard;
using myGovID.iOS.Steps.DeadEnd;
using myGovID.iOS.Steps.DocumentIdentitySuccess;
using myGovID.iOS.Steps.DriverLicenceOCRIntro;
using myGovID.iOS.Steps.Logout;
using myGovID.iOS.Steps.Medicare;
using myGovID.iOS.Steps.OnBoarding;
using myGovID.iOS.Steps.PassportOCRIntro;
using myGovID.iOS.Steps.PreviewPhoto;
using myGovID.iOS.Steps.ResentEmailVerificationCode;
using myGovID.iOS.Steps.SecureAccount;
using myGovID.iOS.Steps.TermsAndConditions;
using myGovID.iOS.Steps.TermsAndConditionsConfirm;
using myGovID.iOS.Steps.VerifyEmail;
using myGovID.iOS.Steps.VersionUpgrade;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.AccountSetup;
using myGovID.Presentation.Contract.Steps.Authenticate;
using myGovID.Presentation.Contract.Steps.Browse;
using myGovID.Presentation.Contract.Steps.CaptureEmail;
using myGovID.Presentation.Contract.Steps.CaptureEmailVerification;
using myGovID.Presentation.Contract.Steps.CapturePassword;
using myGovID.Presentation.Contract.Steps.CapturePhoto;
using myGovID.Presentation.Contract.Steps.Dashboard;
using myGovID.Presentation.Contract.Steps.DeadEnd;
using myGovID.Presentation.Contract.Steps.DocumentIdentitySuccess;
using myGovID.Presentation.Contract.Steps.DriverLicence;
using myGovID.Presentation.Contract.Steps.Logout;
using myGovID.Presentation.Contract.Steps.MedicareCardInput;
using myGovID.Presentation.Contract.Steps.MedicareOcrIntroStep;
using myGovID.Presentation.Contract.Steps.OnBoarding;
using myGovID.Presentation.Contract.Steps.Passport;
using myGovID.Presentation.Contract.Steps.PreviewPhoto;
using myGovID.Presentation.Contract.Steps.RecoverableDeadEnd;
using myGovID.Presentation.Contract.Steps.ResentEmailVerificationCode;
using myGovID.Presentation.Contract.Steps.SecureYourAccount;
using myGovID.Presentation.Contract.Steps.Settings;
using myGovID.iOS.Steps.Settings;
using myGovID.Presentation.Contract.Steps.TermsAndConditionsConfirm;
using UIKit;
using myGovID.Presentation.Contract.Steps.VersionUpgrade;
using myGovID.Presentation.Contract.Steps.TermsAndConditions;
using myGovID.Presentation.Contract.Steps.AuthRequest;
using myGovID.iOS.Steps.AuthRequest;

namespace myGovID.iOS.Steps
{
    public class StepOrchestrator
    {
        private readonly IStepRepository _repository;
        public StepOrchestrator(IStepRepository repository)
        {
            this._repository = repository;
        }
        public virtual (UIViewController ViewController, bool Overlay)? GetViewController(IStep newStep)
        {
            UIViewController result = null;
            bool overlay = false;
            try
            {
                switch (newStep)
                {
                    case IOnBoardingStep step:
                        result = new OnBoardingViewController();
                        break;
                    case IVersionUpgradeStep step:
                        result = new VersionUpgradeViewController();
                        overlay = true;
                        break;
                    case IAuthenticateStep step:
                        result = new AuthenticateViewController();
                        break;
                    case IAccountSetupStep step:
                        result = new AccountSetupViewController();
                        break;
                    case ITermsAndConditionsStep step:
                        result = new TermsAndConditionsViewController();
                        break;
                    case ITermsAndConditionsConfirmStep step:
                        result = new TermsAndConditionsConfirmViewController();
                        overlay = true;
                        break;
                    case ICaptureEmailStep step:
                        result = new CaptureEmailViewController();
                        break;
                    case IResentEmailVerificationCodeStep step:
                        result = new ResentEmailVerificationCodeController();
                        overlay = true;
                        break;
                    case ISecureYourAccountStep step:
                        result = new SecureAccountViewController();
                        break;
                    case ICapturePasswordStep step:
                        result = new CapturePasswordViewController();
                        break;
                    case ICaptureEmailVerificationStep step:
                        result = new VerifyEmailViewController();
                        break;
                    case IDeadEndStep step:
                        result = new DeadEndViewController();
                        break;
                    case IPreviewPhotoStep step:
                        result = new PreviewPhotoViewController();
                        break;
                    case ICapturePersonalDetailsStep step:
                        result = new CapturePersonalDetailsViewController();
                        break;
                    case IDashboardStep step:
                        result = new DashboardViewController();
                        break;
                    case ISettingsStep step:
                        result = new SettingsViewController();
                        break;
                    case ICapturePassportDetailsStep step:
                        result = new PassportDetailsViewController();
                        break;
                    case IPassportOCRIntroStep step:
                        result = new PassportOCRIntroViewController();
                        break;
                    case ILogoutStep step:
                        result = new LogoutViewController();
                        break;
                    case IRecoverableDeadEndStep step:
                        result = new RecoverableDeadendErrorViewController();
                        overlay = true;
                        break;
                    case IDriverLicenceOCRIntroStep step:
                        result = new DriverLicenceOCRIntroViewController();
                        break;
                    case ICaptureDriverLicenceDetailsStep step:
                        result = new CaptureDriverLicenceViewController();
                        break;
                    case IDocumentIdentitySuccessStep step:
                        result = new DocumentSubmissionSuccessViewController();
                        overlay = true;
                        break;
                    case IMedicareOcrIntroStep step:
                        result = new MedicareOCRIntroViewController();
                        break;
                    case IAuthRequestStep step:
                        result = new AuthRequestViewController();
                        break;
                    case IMedicareCardInputStep step:
                        result = new MedicareCardInputViewController();
                        break;
                    case IBrowseStep step:
                        OpenUrlInBrowser(((BrowseViewModel)newStep.ViewModel).Url);
                        return null;
                    default:
                        throw new NotImplementedException(newStep.GetType().FullName);
                }
                return (result, overlay);
            }
            finally
            {
                if (result is IStepViewController)
                {
                    ((IStepViewController)result).Step = newStep;
                }
            }
        }
        public IStep GetDeadEndStep(string code, string title, string message)
        {
            return _repository.GetDeadEndStep(new PresentationError(code, title, message));
        }
        public void OpenUrlInBrowser(string urlLink)
        {
            if (string.IsNullOrWhiteSpace(urlLink))
            {
                throw new ArgumentNullException(nameof(urlLink));
            }
            var url = new NSUrl(urlLink);
            if (UIApplication.SharedApplication.CanOpenUrl(url))
            {
                UIApplication.SharedApplication.OpenUrl(url);
            }
        }
    }
}
