﻿using System;
using System.ComponentModel;
using Foundation;
using myGovID.iOS.Views.Common;
using UIKit;

namespace myGovID.iOS.Steps.Medicare
{
    [Register("MedicareIrnInputView"), DesignTimeVisible(true)]
    public class MedicareIrnInputView : UITextFieldTypeAware
    {

        public MedicareIrnInputView(IntPtr p) : base(p)
        {
            var inputView = new UIPickerView();
            inputView.Model = new IrnPickerViewModel(this);
            InputView = inputView;

            var defaultValue = inputView.Model.GetTitle(inputView, 0, 0);
            EditingDidBegin += (sender, args) =>
            {
                if (string.IsNullOrWhiteSpace(Text))
                {
                    SetTextAndNotify(defaultValue);
                }
            };
        }

        public void SetTextAndNotify(string text)
        {
            Text = text;
            SendActionForControlEvents(UIControlEvent.EditingChanged);
        }

        private class IrnPickerViewModel : UIPickerViewModel
        {
            private MedicareIrnInputView _medicareIrnInputView;

            public IrnPickerViewModel(MedicareIrnInputView medicareIrnInputView)
            {
                _medicareIrnInputView = medicareIrnInputView;
            }

            [Export("pickerView:titleForRow:forComponent:")]
            public override string GetTitle(UIPickerView pickerView, nint row, nint component)
            {
                return (row + 1).ToString();
            }

            [Export("pickerView:numberOfRowsInComponent:")]
            public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
            {
                return 9;
            }

            [Export("numberOfComponentsInPickerView:")]
            public override nint GetComponentCount(UIPickerView pickerView)
            {
                return 1;
            }

            [Export("pickerView:didSelectRow:inComponent:")]
            public override void Selected(UIPickerView pickerView, nint row, nint component)
            {
                var newIrn = GetTitle(pickerView, row, component);
                _medicareIrnInputView.SetTextAndNotify(newIrn);
            }
        }
    }
}
