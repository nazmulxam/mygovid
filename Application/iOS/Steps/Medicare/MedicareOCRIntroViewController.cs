﻿using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.MedicareOcrIntroStep;

namespace myGovID.iOS.Steps.Medicare
{
    public partial class MedicareOCRIntroViewController : StepViewController<IMedicareOcrIntroStep, MedicareOcrIntroViewModel, MedicareOcrIntroAction>
    {
        protected override void OnViewModelLoaded()
        {
            Title.Text = ViewModel.Title;
            Description.Text = ViewModel.Body;
        }

        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(ScanButton, ActionType.Next, MedicareOcrIntroAction.ScanDetails);
            Bind(EnterManualButton, ActionType.Next, MedicareOcrIntroAction.EnterManually);
            Bind(BackButton, ActionType.Previous, MedicareOcrIntroAction.Back);
        }

    }
}

