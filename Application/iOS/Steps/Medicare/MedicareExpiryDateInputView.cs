﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using Foundation;
using myGovID.iOS.Views.Common;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Inputs;
using UIKit;

namespace myGovID.iOS.Steps.Medicare
{
    [Register("MedicareExpiryDateInputView"), DesignTimeVisible(true)]
    public class MedicareExpiryDateInputView : UITextFieldTypeAware
    {

        private DateFormat _dateFormat = DateFormat.GreenMedicareCardExpiry;
        public DateFormat DateFormat
        {
            get => _dateFormat;
            set
            {
                if (_dateFormat != value)
                {
                    _dateFormat = value;
                    if (_dateFormat == DateFormat.GreenMedicareCardExpiry)
                    {
                        SwitchToMonthYearPicker();
                    }
                    else
                    {
                        SwitchToDatePicker();
                    }
                }
            }
        }

        private UIDatePicker datePicker;
        private UIPickerView monthYearPicker;

        public MedicareExpiryDateInputView(IntPtr p) : base(p)
        {
            SwitchToMonthYearPicker();
            EditingDidBegin += (sender, args) =>
            {
                if (string.IsNullOrWhiteSpace(Text))
                {
                    SetTextAndNotify(DateTime.Now.ToString(_dateFormat.GetFormat()));
                    if (InputView is UIPickerView)
                    {
                        (InputView as UIPickerView).Select(DateTime.Now.Month - 1, 0, false);
                    }
                }
            };
        }

        public void SetTextAndNotify(string text)
        {
            Text = text;
            SendActionForControlEvents(UIControlEvent.EditingChanged);
        }

        protected void SwitchToDatePicker()
        {
            if (datePicker == null)
            {
                datePicker = new UIDatePicker();
                datePicker.Mode = UIDatePickerMode.Date;
                datePicker.EditingDidBegin += HandleDateChanged;
                datePicker.ValueChanged += HandleDateChanged;
            }

            if (InputView != datePicker)
            {
                InputView = datePicker;
                SetTextAndNotify(string.Empty);
            }
        }

        protected void SwitchToMonthYearPicker()
        {
            if (monthYearPicker == null)
            {
                monthYearPicker = new UIPickerView();
                monthYearPicker.Model = new MonthYearPickerViewModel(this);
            }

            if (InputView != monthYearPicker)
            {
                InputView = monthYearPicker;
                SetTextAndNotify(string.Empty);
            }
        }

        private void HandleDateChanged(object sender, EventArgs e)
        {
            var localTime = TimeZone.CurrentTimeZone.ToLocalTime((DateTime)datePicker.Date);
            SetTextAndNotify(localTime.ToString(_dateFormat.GetFormat()));
        }

        private class MonthYearPickerViewModel : UIPickerViewModel
        {
            private readonly MedicareExpiryDateInputView _medicareExpiryDateInputView;

            private IEnumerable<string> _months = Enumerable.Range(1, 12)
                .Select((month) => month.ToString().PadLeft(2, '0'))
                .ToList();

            private IEnumerable<string> _years = Enumerable.Range(
                    DateTime.Now.Year, MedicareCardExpiryDateInput.CardExpiryYears + 1)
                .Select((year) => year.ToString())
                .ToList();

            private int _monthIndex = 0;
            private int _yearIndex = 0;

            public MonthYearPickerViewModel(MedicareExpiryDateInputView medicareExpiryDateInputView)
            {
                _medicareExpiryDateInputView = medicareExpiryDateInputView;
            }

            [Export("pickerView:titleForRow:forComponent:")]
            public override string GetTitle(UIPickerView pickerView, nint row, nint component)
            {
                return component == 0 ? _months.ElementAt((int)row) : _years.ElementAt((int)row);
            }

            [Export("pickerView:numberOfRowsInComponent:")]
            public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
            {
                return component == 0 ? 12 : MedicareCardExpiryDateInput.CardExpiryYears + 1;
            }

            [Export("numberOfComponentsInPickerView:")]
            public override nint GetComponentCount(UIPickerView pickerView)
            {
                return 2;
            }

            [Export("pickerView:didSelectRow:inComponent:")]
            public override void Selected(UIPickerView pickerView, nint row, nint component)
            {
                if (component == 0)
                {
                    _monthIndex = (int)row;
                }
                else
                {
                    _yearIndex = (int)row;
                }
                var month = GetTitle(pickerView, _monthIndex, 0);
                var year = GetTitle(pickerView, _yearIndex, 1);
                _medicareExpiryDateInputView.SetTextAndNotify($"{month}/{year}");
            }

            [Export("pickerView:widthForComponent:")]
            public override nfloat GetComponentWidth(UIPickerView pickerView, nint component)
            {
                return 80f;
            }
        }

    }
}
