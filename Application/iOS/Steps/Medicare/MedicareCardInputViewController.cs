﻿using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.MedicareCardInput;
using GalaSoft.MvvmLight.Helpers;
using myGovID.iOS.Views.Common;
using UIKit;

namespace myGovID.iOS.Steps.Medicare
{
    //public partial class MedicareCardInputViewController : UIViewController
    public partial class MedicareCardInputViewController : StepViewController<IMedicareCardInputStep, MedicareCardInputViewModel, MedicareCardInputAction>
    {
        protected override void OnViewModelLoaded() { }

        protected override void AttachBindings()
        {
            base.AttachBindings();

            // Card number
            Bind(ViewModel.CardNumber, CardNumberTextField, CardNumberErrorLabel);

            // Name fields
            Bind(ViewModel.NameLine1, Name1Container, Name1TextField, Name1ErrorLabel);
            Bind(ViewModel.NameLine2, Name2Container, Name2TextField, Name2ErrorLabel);
            Bind(ViewModel.NameLine3, Name3Container, Name3TextField, Name3ErrorLabel);
            Bind(ViewModel.NameLine4, Name4Container, Name4TextField, Name4ErrorLabel);

            // Name buttons
            Bind(ViewModel.AddNameButton, AddNameButton);
            Bind(ViewModel.RemoveNameButton, RemoveNameButton);

            // Irn
            Bind(ViewModel.Irn, IrnTextField, IrnErrorLabel);

            // Color
            Bind(ViewModel.CardType, CardColorTextField, CardColorErrorLabel);

            // Expiry
            Bind(ViewModel.Expiry, CardExpiryTextField, CardExpiryErrorLabel);

            CardExpiryTextField.DateFormat = ViewModel.Expiry.DateFormat;
            // For some reason the binding randomly throws reflection exceptions
            // if these aren't locally defined
            var expiryInput = ViewModel.Expiry;
            var expiryView = CardExpiryTextField;
            AddBinding(new Binding<DateFormat, DateFormat>(
                expiryInput, nameof(expiryInput.DateFormat),
                expiryView, nameof(expiryView.DateFormat)));

            // Consent
            Bind(ViewModel.ConsentProvided, ConsentCheckboxView, ConsentCheckboxLabel, ConsentErrorLabel);

            // Submit button
            Bind(ConfirmButton, ActionType.Next, MedicareCardInputAction.Submit);

            // Nav bar buttons
            Bind(BackButton, ActionType.Previous, MedicareCardInputAction.Back);
            Bind(HelpButton, ActionType.Browse, MedicareCardInputAction.Help);
        }

        protected override UIView GetPageErrorViewContainer()
        {
            return PageErrorViewContainer;
        }
    }
}

