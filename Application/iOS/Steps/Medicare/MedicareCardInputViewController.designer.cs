﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace myGovID.iOS.Steps.Medicare
{
    [Register ("MedicareCardInputViewController")]
    partial class MedicareCardInputViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIButtonTypeAware AddNameButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem BackButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware CardColorErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Steps.Medicare.MedicareCardTypeInputView CardColorTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware CardExpiryErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Steps.Medicare.MedicareExpiryDateInputView CardExpiryTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware CardNumberErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UITextFieldTypeAware CardNumberTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIButtonTypeAware ConfirmButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware ConsentCheckboxLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.RadioButton ConsentCheckboxView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware ConsentErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem HelpButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware IrnErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Steps.Medicare.MedicareIrnInputView IrnTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIStackView Name1Container { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware Name1ErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UITextFieldTypeAware Name1TextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIStackView Name2Container { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware Name2ErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UITextFieldTypeAware Name2TextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIStackView Name3Container { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware Name3ErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UITextFieldTypeAware Name3TextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIStackView Name4Container { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UILabelTypeAware Name4ErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UITextFieldTypeAware Name4TextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIStackView NameFieldStackView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView PageErrorViewContainer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Views.Common.UIButtonTypeAware RemoveNameButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AddNameButton != null) {
                AddNameButton.Dispose ();
                AddNameButton = null;
            }

            if (BackButton != null) {
                BackButton.Dispose ();
                BackButton = null;
            }

            if (CardColorErrorLabel != null) {
                CardColorErrorLabel.Dispose ();
                CardColorErrorLabel = null;
            }

            if (CardColorTextField != null) {
                CardColorTextField.Dispose ();
                CardColorTextField = null;
            }

            if (CardExpiryErrorLabel != null) {
                CardExpiryErrorLabel.Dispose ();
                CardExpiryErrorLabel = null;
            }

            if (CardExpiryTextField != null) {
                CardExpiryTextField.Dispose ();
                CardExpiryTextField = null;
            }

            if (CardNumberErrorLabel != null) {
                CardNumberErrorLabel.Dispose ();
                CardNumberErrorLabel = null;
            }

            if (CardNumberTextField != null) {
                CardNumberTextField.Dispose ();
                CardNumberTextField = null;
            }

            if (ConfirmButton != null) {
                ConfirmButton.Dispose ();
                ConfirmButton = null;
            }

            if (ConsentCheckboxLabel != null) {
                ConsentCheckboxLabel.Dispose ();
                ConsentCheckboxLabel = null;
            }

            if (ConsentCheckboxView != null) {
                ConsentCheckboxView.Dispose ();
                ConsentCheckboxView = null;
            }

            if (ConsentErrorLabel != null) {
                ConsentErrorLabel.Dispose ();
                ConsentErrorLabel = null;
            }

            if (HelpButton != null) {
                HelpButton.Dispose ();
                HelpButton = null;
            }

            if (IrnErrorLabel != null) {
                IrnErrorLabel.Dispose ();
                IrnErrorLabel = null;
            }

            if (IrnTextField != null) {
                IrnTextField.Dispose ();
                IrnTextField = null;
            }

            if (Name1Container != null) {
                Name1Container.Dispose ();
                Name1Container = null;
            }

            if (Name1ErrorLabel != null) {
                Name1ErrorLabel.Dispose ();
                Name1ErrorLabel = null;
            }

            if (Name1TextField != null) {
                Name1TextField.Dispose ();
                Name1TextField = null;
            }

            if (Name2Container != null) {
                Name2Container.Dispose ();
                Name2Container = null;
            }

            if (Name2ErrorLabel != null) {
                Name2ErrorLabel.Dispose ();
                Name2ErrorLabel = null;
            }

            if (Name2TextField != null) {
                Name2TextField.Dispose ();
                Name2TextField = null;
            }

            if (Name3Container != null) {
                Name3Container.Dispose ();
                Name3Container = null;
            }

            if (Name3ErrorLabel != null) {
                Name3ErrorLabel.Dispose ();
                Name3ErrorLabel = null;
            }

            if (Name3TextField != null) {
                Name3TextField.Dispose ();
                Name3TextField = null;
            }

            if (Name4Container != null) {
                Name4Container.Dispose ();
                Name4Container = null;
            }

            if (Name4ErrorLabel != null) {
                Name4ErrorLabel.Dispose ();
                Name4ErrorLabel = null;
            }

            if (Name4TextField != null) {
                Name4TextField.Dispose ();
                Name4TextField = null;
            }

            if (NameFieldStackView != null) {
                NameFieldStackView.Dispose ();
                NameFieldStackView = null;
            }

            if (PageErrorViewContainer != null) {
                PageErrorViewContainer.Dispose ();
                PageErrorViewContainer = null;
            }

            if (RemoveNameButton != null) {
                RemoveNameButton.Dispose ();
                RemoveNameButton = null;
            }
        }
    }
}