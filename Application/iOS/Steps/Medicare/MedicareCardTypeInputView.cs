﻿using System;
using System.Linq;
using System.ComponentModel;
using Foundation;
using myGovID.iOS.Views.Common;
using myGovID.Presentation.Contract;
using UIKit;
using System.Collections.Generic;

namespace myGovID.iOS.Steps.Medicare
{
    [Register("MedicareCardTypeInputView"), DesignTimeVisible(true)]
    public class MedicareCardTypeInputView : UITextFieldTypeAware
    {

        public MedicareCardTypeInputView(IntPtr p) : base(p)
        {
            var inputView = new UIPickerView();
            inputView.Model = new CardTypePickerViewModel(this);
            InputView = inputView;

            var defaultValue = inputView.Model.GetTitle(inputView, 0, 0);
            EditingDidBegin += (sender, args) =>
            {
                if (string.IsNullOrWhiteSpace(Text))
                {
                    SetTextAndNotify(defaultValue);
                }
            };
        }

        public void SetTextAndNotify(string text)
        {
            Text = text;
            SendActionForControlEvents(UIControlEvent.EditingChanged);
        }

        private class CardTypePickerViewModel : UIPickerViewModel
        {
            private MedicareCardTypeInputView _medicareCardTypeInputView;
            private IEnumerable<string> _values = MedicareCardType.All;

            public CardTypePickerViewModel(MedicareCardTypeInputView medicareCardTypeInputView)
            {
                _medicareCardTypeInputView = medicareCardTypeInputView;
            }

            [Export("pickerView:titleForRow:forComponent:")]
            public override string GetTitle(UIPickerView pickerView, nint row, nint component)
            {
                return _values.ElementAt((int)row);
            }

            [Export("pickerView:numberOfRowsInComponent:")]
            public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
            {
                return _values.Count();
            }

            [Export("numberOfComponentsInPickerView:")]
            public override nint GetComponentCount(UIPickerView pickerView)
            {
                return 1;
            }

            [Export("pickerView:didSelectRow:inComponent:")]
            public override void Selected(UIPickerView pickerView, nint row, nint component)
            {
                var text = GetTitle(pickerView, row, component);
                _medicareCardTypeInputView.SetTextAndNotify(text);
            }
        }
    }
}
