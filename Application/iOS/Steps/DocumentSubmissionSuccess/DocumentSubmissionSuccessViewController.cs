using myGovID.Presentation.Contract.Steps;
using myGovID.Presentation.Contract.Steps.DocumentIdentitySuccess;
using UIKit;

namespace myGovID.iOS.Steps.DocumentIdentitySuccess
{
    public partial class DocumentSubmissionSuccessViewController : StepViewController<IDocumentIdentitySuccessStep, EmptyViewModel>
    {
        private SuccessCheckMarkView _successView = new SuccessCheckMarkView(UIApplication.SharedApplication.KeyWindow.Bounds);

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            _successView.SetParent(View).AddTransparentBackground().Start(CallNext);
        }
        protected override void OnViewModelLoaded()
        {
        }
        private void CallNext()
        {
            ExecuteAction(FindAction(Presentation.Contract.ActionType.Next, default));
        }
    }
}

