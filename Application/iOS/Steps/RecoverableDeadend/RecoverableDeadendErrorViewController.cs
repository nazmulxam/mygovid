﻿using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.RecoverableDeadEnd;

namespace myGovID.iOS.Steps.VerifyEmail
{
    public partial class RecoverableDeadendErrorViewController : StepViewController<IRecoverableDeadEndStep, RecoverableDeadEndViewModel>
    {
        protected override void OnViewModelLoaded() {
            AlertTitle.Text = ViewModel.Exception.Title;
            RenderHtml(AlertMessage, ViewModel.Exception.Description);
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(OkButton, ActionType.Next, null, "OnNextOverlay");
        }
    }
}

