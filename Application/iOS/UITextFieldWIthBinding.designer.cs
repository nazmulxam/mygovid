// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace myGovID.iOS
{
    [Register ("UITextFieldWIthBinding")]
    partial class UITextFieldWIthBinding
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Steps.CaptureEmail.CaptureEmailViewController @delegate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        myGovID.iOS.Steps.CapturePersonalDetails.CapturePersonalDetailsViewController @delegate { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (@delegate != null) {
                @delegate.Dispose ();
                @delegate = null;
            }

            if (@delegate != null) {
                @delegate.Dispose ();
                @delegate = null;
            }
        }
    }
}