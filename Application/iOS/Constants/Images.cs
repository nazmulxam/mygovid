﻿namespace myGovID.iOS.Constants
{
    public static class Images
    {
        public const string LaunchBackground = "LaunchBackground";
        public const string IdentityBarUnknown = "IdentityBarUnknown";
        public const string IdentityBarWeak = "IdentityBarWeak";
        public const string IdentityBarIntermediate = "IdentityBarIntermediate";
        public const string IdentityBarStrong = "IdentityBarStrong";
        public const string GreenTickAlert = "GreenTickAlert";
        public const string DropDown = "DropDown";
        public const string DeclinedAlert = "DeclinedAlert";
        public const string MockPreview = "MockPreview";
        public const string FaceID = "FaceID";
        public const string TouchID = "TouchID";
        public const string CheckboxOn = "CheckboxOn";
        public const string CheckboxOff = "CheckboxOff";
    }
}
