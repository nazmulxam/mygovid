﻿namespace myGovID.iOS.Constants
{
    public static class FontType
    {
        public const string Bold = "HelveticaNeueLTCom-Bd";
        public const string Medium = "HelveticaNeueLTCom-Md";
        public const string UltraLight = "HelveticaNeueLTCom-Roman";
    }
}
