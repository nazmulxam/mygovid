﻿using System.Collections.Generic;
using myGovID.Business.Contract.Common.Application;
namespace myGovID.Application.Launcher
{
    public class Device: IDevice
    {
        public string PlatformType { get; set; }

        public string RuntimeType { get; set; }

        public string RuntimeVersion { get; set; }

        public string FormFactorType { get; set; }

        public IEnumerable<DeviceCapability> Capabilities { get; set; }
    }
}