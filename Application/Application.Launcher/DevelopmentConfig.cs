﻿using myGovID.Business.Contract.Common.Application;
using myGovID.Services.Api.Contract;
using myGovID.Services.Http.Contract;

namespace myGovID.Application.Launcher
{
    public class DevelopmentConfig : IHttpConfig
    {
        public string BaseUrl { get; }
        public string ApplicationName { get; }
        public string ApplicationVersion { get; }
        public DevelopmentConfig(IApplicationMetaData appMetaData)
        {
            BaseUrl = "https://mygovid.acc.ato.gov.au/";
            ApplicationName = appMetaData.Name;
            ApplicationVersion = appMetaData.Version;
        }
    }
}
