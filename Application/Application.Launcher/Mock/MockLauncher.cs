﻿using System;
using myGovID.Application.Launcher.Contract;
using myGovID.Business.Contract.Component;
using myGovID.Services.Api.Contract;
using myGovID.Services.Dependency.Contract;
using myGovID.Application.Launcher.Contract.Mock;
using myGovID.Services.Http.Mock;
using myGovID.Services.Http.Contract;
using myGovID.Business.Mock;

namespace myGovID.Application.Launcher.Mock
{
    public class MockLauncher : Launcher
    {
        public MockLauncher(IDependencyService dependencyService) : base(dependencyService)
        {
        }
        public override void Configure(LaunchConfiguration configuration)
        {
            base.Configure(configuration);
            MockLaunchConfiguration launchConfiguration = (MockLaunchConfiguration)configuration;
            if (launchConfiguration.TestcaseId == null)
            {
                throw new ArgumentNullException($"TestcaseId is missing from configuration.");
            }
            if (launchConfiguration.DependencyService == null)
            {
                throw new ArgumentNullException($"DependencyService is missing from configuration.");
            }
            //TODO: Refactor these dependencies. If mock is required outside of the http service, it should be available on its own - too much work for now
            MockHttpService httpService = (MockHttpService)launchConfiguration.DependencyService.Resolve<IHttpService>();
            Setup setup = httpService.Setup(launchConfiguration.TestcaseId);
            setup.UseBiometrics = launchConfiguration.UseBiometrics;
            if (setup.Registered)
            {
                MockLocalCredentialComponent localCredentialComponent = (MockLocalCredentialComponent)launchConfiguration.DependencyService.Resolve<ILocalCredentialComponent>();
                localCredentialComponent.Setup(setup.CredentialToken, setup.PoiId, setup.EmailAddress, setup.UserPassword, setup.UseBiometrics, setup.FirstName, setup.LastName, setup.DateOfBirth);
            }
        }
    }
}
