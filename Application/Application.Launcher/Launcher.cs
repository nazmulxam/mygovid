﻿using AutoMapper;
using myGovID.Business.Mapper;
using myGovID.Presentation.Mapper;
using myGovID.Services.Dependency.Contract;
using myGovID.Application.Launcher.Contract;
using myGovID.Business.Contract.Common.Application;
using System;
using myGovID.Services.Http.Contract;

namespace myGovID.Application.Launcher
{
    public class Launcher : ILauncher
    {
        private readonly IDependencyService _dependencyService;
        public Launcher(IDependencyService dependencyService)
        {
            _dependencyService = dependencyService;
            Scheme scheme = GetScheme();
            dependencyService.Register(scheme);
            if (scheme == Scheme.Production)
            {
                dependencyService.Register<IHttpConfig, ProductionConfig>(Scope.Singleton);
            }
            else
            {
                dependencyService.Register<IHttpConfig, DevelopmentConfig>(Scope.Singleton);
            }
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<BusinessMapperProfile>();
                cfg.AddProfile<ServiceMapperProfile>();
            });
        }
        public virtual void Configure(LaunchConfiguration configuration)
        {
            if (configuration.ApplicationInfo == null)
            {
                throw new ArgumentNullException($"ApplicationInfo is missing from configuration.");
            }
            if (configuration.DeviceInfo == null)
            {
                throw new ArgumentNullException($"DeviceInfo is missing from configuration.");
            }
            _dependencyService.Register<IApplicationMetaData>(Mapper.Map<ApplicationMetaData>(configuration.ApplicationInfo));
            _dependencyService.Register<IDevice>(Mapper.Map<Device>(configuration.DeviceInfo));
        }
        private Scheme GetScheme()
        {
#if MOCK || UCTD
            return Scheme.Development;
#else
            return Scheme.Production;
#endif
        }
    }
}