﻿using myGovID.Business.Contract.Common.Application;
namespace myGovID.Application.Launcher
{
    public class ApplicationMetaData : IApplicationMetaData
    {
        public string Name { get; set; }
        public string Version { get; set; }
        public string BuildDate { get; set; }
        public string FilesDirectory { get; set; }
    }
}