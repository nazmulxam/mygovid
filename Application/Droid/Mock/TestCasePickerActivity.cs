﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using myGovID.Application.Launcher.Contract;
using myGovID.Services.Http.Mock;

namespace myGovID.Droid.Mock
{
    [Activity(Label = "TestCasePickerActivity", Theme = "@style/myGovId.Theme.NoActionBar",
              WindowSoftInputMode = SoftInput.StateHidden,
              NoHistory = true)]
    public class TestCasePickerActivity : AppCompatActivity
    {
        private EditText _testcaseEditText;
        private Spinner _testcaseSpinner;
        private Spinner _scenarioSpinner;
        private Switch _biometricSwitch;
        private Switch _fastForwardSwitch;
        private IDictionary<string, string> _testCases;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            ChangeNavigationThemeToLight();
            SetContentView(Resource.Layout.TestCasePicker);
            MockFileManager mockFileManager = new MockFileManager();
            _testCases = mockFileManager.GetTestCases();
            _testcaseEditText = FindViewById<EditText>(Resource.Id.testCaseET);
            ArrayAdapter<string> adapter = new ArrayAdapter<string>(this, Resource.Layout.SpinnerWrapText, Android.Resource.Id.Text1, _testCases.Keys.ToArray());
            adapter.SetDropDownViewResource(Resource.Layout.SpinnerWrapText);
            _testcaseSpinner = FindViewById<Spinner>(Resource.Id.testCases);
            _testcaseSpinner.Adapter = adapter;
            ArrayAdapter<string> scenarioAdapter = new ArrayAdapter<string>(this, Resource.Layout.SpinnerWrapText, Android.Resource.Id.Text1, _testCases.Values.ToArray());
            scenarioAdapter.SetDropDownViewResource(Resource.Layout.SpinnerWrapText);
            _scenarioSpinner = FindViewById<Spinner>(Resource.Id.scenarios);
            _scenarioSpinner.Adapter = scenarioAdapter;
            if (_testCases.Any())
            {
                _testcaseSpinner.SetSelection(0, false);
                _scenarioSpinner.SetSelection(0, false);
                _testcaseEditText.Text = _testCases.Keys.First();
            }
            else
            {
                FindViewById<Button>(Resource.Id.nextButton).Visibility = ViewStates.Gone;
            }
            _testcaseSpinner.ItemSelected += (sender, args) =>
            {
                _scenarioSpinner.SetSelection(args.Position, true);
                _testcaseEditText.Text = _testCases.Keys.ElementAt(args.Position);
            };
            _scenarioSpinner.ItemSelected += (sender, args) =>
            {
                _testcaseSpinner.SetSelection(args.Position, true);
                _testcaseEditText.Text = _testCases.Keys.ElementAt(args.Position);
            };
            _biometricSwitch = FindViewById<Switch>(Resource.Id.enableBiometric);
            _fastForwardSwitch = FindViewById<Switch>(Resource.Id.enableFastForwardToDashboard);
            FindViewById<Button>(Resource.Id.nextButton).Click += delegate
            {
                Intent data = new Intent();
                data.PutExtra("TestCasePickerActivity.UseBiometrics", _biometricSwitch.Checked);
                data.PutExtra("TestCasePickerActivity.TestCaseName", _testcaseEditText.Text);
                data.PutExtra("TestCasePickerActivity.FastForward", _fastForwardSwitch.Checked);
                SetResult(Result.Ok, data);
                Finish();
            };
        }
        private void SetActionBar(int layoutId, int titleId)
        {
            SupportActionBar.SetDisplayShowCustomEnabled(true);
            SupportActionBar.SetCustomView(layoutId);
            SupportActionBar.SetDisplayShowHomeEnabled(false);
            TextView abTitle = (TextView)FindViewById(Resource.Id.actionbarTitle);
            if (abTitle == null)
            {
                return;
            }
            abTitle.Text = Resources.GetString(titleId);
        }
        private void ChangeNavigationThemeToLight()
        {
            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            var uiFlags = Window.DecorView.SystemUiVisibility;
            var newUiFlags = (int)uiFlags;
            newUiFlags |= (int)SystemUiFlags.LightNavigationBar;
            Window.DecorView.SystemUiVisibility = (StatusBarVisibility)newUiFlags;
        }
    }
}