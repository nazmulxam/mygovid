﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Android.Content;
using myGovID.Droid.Steps;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps;
using myGovID.Presentation.Contract.Steps.AccountSetup;
using myGovID.Presentation.Contract.Steps.CaptureEmail;
using myGovID.Presentation.Contract.Steps.CaptureEmailVerification;
using myGovID.Presentation.Contract.Steps.CapturePassword;
using myGovID.Presentation.Contract.Steps.CapturePersonalDetails;
using myGovID.Presentation.Contract.Steps.Dashboard;
using myGovID.Presentation.Contract.Steps.DocumentIdentitySuccess;
using myGovID.Presentation.Contract.Steps.OnBoarding;
using myGovID.Presentation.Contract.Steps.SecureYourAccount;
using myGovID.Presentation.Contract.Steps.TermsAndConditions;
using myGovID.Presentation.Contract.Steps.TermsAndConditionsConfirm;
using myGovID.Presentation.Contract.Steps.VersionUpgrade;

namespace myGovID.Droid.Mock
{
    public class FastForwardOrchestrator : StepOrchestrator
    {
        public FastForwardOrchestrator(IStepRepository repository) : base(repository)
        {
        }
        private static bool _isInDashboard = false;
        public override Intent GetActivity(Context context, IStep newStep)
        {
            if (!_isInDashboard)
            {
                switch (newStep.ViewModel)
                {
                    case AccountSetupViewModel vm:
                        return ExecuteAction(context, newStep, ActionType.Next, AccountSetupAction.Register);
                    case OnBoardingViewModel vm:
                        return ExecuteAction(context, newStep, ActionType.Next);
                    case TermsAndConditionsViewModel vm:
                        return ExecuteAction(context, newStep, ActionType.Next);
                    case TermsAndConditionsConfirmViewModel vm:
                        return ExecuteAction(context, newStep, ActionType.Next);
                    case SecureYourAccountViewModel vm:
                        return ExecuteAction(context, newStep, ActionType.Next);
                    case CaptureEmailViewModel vm:
                        vm.Inputs.FirstOrDefault()?.FromString("test@email.com");
                        return ExecuteAction(context, newStep, ActionType.Next);
                    case CaptureEmailVerificationViewModel vm:
                        vm.Inputs.FirstOrDefault()?.FromString("111111");
                        return ExecuteAction(context, newStep, ActionType.Next);
                    case CapturePasswordViewModel vm:
                        foreach (var input in vm.Inputs)
                            input.FromString("Pass123456");
                        return ExecuteAction(context, newStep, ActionType.Next);
                    case CapturePersonalDetailsViewModel vm:
                        foreach (var input in vm.Inputs)
                        {
                            switch (input.MetaData)
                            {
                                case InputMetaData.GivenName: input.FromString("Shane"); break;
                                case InputMetaData.FamilyName: input.FromString("Foreman"); break;
                                case InputMetaData.DateOfBirth: input.FromString("01/01/1980"); break;
                            }
                        }
                        return ExecuteAction(context, newStep, ActionType.Next);
                    case EmptyViewModel vm:
                        if (newStep is IDocumentIdentitySuccessStep)
                        {
                            return ExecuteAction(context, newStep, ActionType.Next);
                        }
                        break;
                    case DashboardViewModel vm:
                        _isInDashboard = true;
                        break;
                }
            }
            if (newStep.ViewModel is VersionUpgradeViewModel versionVM)
            {
                if (!versionVM.ForcedUpdate)
                    return ExecuteAction(context, newStep, ActionType.Previous);
            }
            return base.GetActivity(context, newStep);
        }
        private Intent ExecuteAction(Context context, IStep step, ActionType actionType)
        {
            return ExecuteAction<Enum>(context, step, actionType);
        }
        private Intent ExecuteAction<E>(Context context, IStep step, ActionType actionType, E subType = default)
        {
            var targetAction = step.GetActions<E>().FirstOrDefault((action) =>
            {
                if (action.Type != actionType)
                {
                    return false;
                }
                if (action.SubType == null && subType == null)
                {
                    return true;
                }
                if (action.SubType != null && subType != null)
                {
                    return action.SubType.Equals(subType);
                }
                return false;
            });
            // For some reason the credential component didn't work if this
            // happened synchronously on the same thread. Using a background
            // thread seems to do the trick.
            var source = new TaskCompletionSource<IStep>();
            Task.Run(async () => source.TrySetResult(await targetAction.Run())).ConfigureAwait(false);
            source.Task.Wait();
            return GetActivity(context, source.Task.Result);
        }
    }
}
