﻿using Android.Content;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace myGovID.Droid.Views
{
    public class HintArrayAdapter<T> : ArrayAdapter<T>
    {
        private readonly int _textViewResourceId;
        public HintArrayAdapter(Context context, int resource, int textViewResourceId, T[] options) : base(context, resource, textViewResourceId, options)
        {
            _textViewResourceId = textViewResourceId;
        }
        public override bool IsEnabled(int position)
        {
            return position > 0;
        }
        public override View GetDropDownView(int position, View convertView, ViewGroup parent)
        {
            View view = base.GetDropDownView(position, convertView, parent);
            view.FindViewById<TextView>(_textViewResourceId).SetTextColor(position == 0 ? Color.Gray : Color.Black);
            return view;
        }
        public override long GetItemId(int position)
        {
            return position;
        }
    }
}