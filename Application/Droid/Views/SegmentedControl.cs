﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace myGovID.Droid.Views
{
    public class SegmentedControl: RadioGroup
    {
        private int _margin;
        private Color _tintColor;
        private Color _uncheckedTintColor;
        private Color _checkedTextColor = Color.White;
        private Color _uncheckedTextColor = Color.Black;
        private Color _borderColor = Color.Black;
        private float _cornerRadius;
        private LayoutSelector _layoutSelector;
        private Dictionary<int, TransitionDrawable> _drawableMap;
        private int _lastCheckId;
        private Action<string> _checkedChangeAction;
        public Color TintColor { 
            get
            {
                return _tintColor; 
            }
            set
            {
                _tintColor = value;
                UpdateBackground();
            }
        }
        public Color UnCheckedTintColor
        {
            get
            {
                return _uncheckedTintColor;
            }
            set
            {
                _uncheckedTintColor = value;
                UpdateBackground();
            }
        }
        public Color CheckedTextColor { 
            get
            {
                return _checkedTextColor;
            }
            set
            {
                _checkedTextColor = value;
                UpdateBackground();
            }
        }
        public Color UncheckedTextColor
        {
            get
            {
                return _uncheckedTextColor;
            }
            set
            {
                _uncheckedTextColor = value;
                UpdateBackground();
            }
        }
        public Color BorderColor
        {
            get
            {
                return _borderColor;
            }
            set
            {
                _borderColor = value;
                UpdateBackground();
            }
        }
        public string CheckedText
        { 
            get
            {
                int selectedRadio = CheckedRadioButtonId;
                if (selectedRadio == 0)
                {
                    return null;
                }
                return FindViewById<RadioButton>(selectedRadio)?.Text ?? null;
            }
            set
            {
                View view = FindViewWithTag(value);
                if(view != null)
                {
                    ((RadioButton)view).Checked = true;
                }
            }
        }
        public SegmentedControl(Context context): base(context)
        {
            CommonInit(context);
        }

        public SegmentedControl(Context context, IAttributeSet attrs): base(context, attrs)
        {
            CommonInit(context, attrs);
        }
        private void CommonInit(Context context, IAttributeSet attrs = null)
        {
            _tintColor = Resources.GetColor(Resource.Color.segmentedControlSelected, context.Theme);
            _borderColor = Resources.GetColor(Resource.Color.gray, context.Theme);
            _uncheckedTintColor = Resources.GetColor(Resource.Color.segmentedControlUnselected, context.Theme);
            _margin = (int)Resources.GetDimension(Resource.Dimension.sc_radio_button_stroke_border);
            _cornerRadius = Resources.GetDimension(Resource.Dimension.sc_radio_button_conner_radius);
            if (attrs != null)
            {
                TypedArray typedArray = context.Theme.ObtainStyledAttributes(attrs,
                                                    Resource.Styleable.SegmentedControl, 0, 0);
                try
                {
                    _margin = (int)typedArray.GetDimension(Resource.Styleable.SegmentedControl_sc_border_width,
                                                _margin);
                    _cornerRadius = typedArray.GetDimension(Resource.Styleable.SegmentedControl_sc_corner_radius,
                                                _cornerRadius);
                    _tintColor = typedArray.GetColor(Resource.Styleable.SegmentedControl_sc_tint_color,
                                                _tintColor);
                    _uncheckedTintColor = typedArray.GetColor(Resource.Styleable.SegmentedControl_sc_unchecked_tint_color,
                                                _uncheckedTintColor);
                    _checkedTextColor = typedArray.GetColor(Resource.Styleable.SegmentedControl_sc_checked_text_color,
                                                _checkedTextColor);
                    _uncheckedTextColor = typedArray.GetColor(Resource.Styleable.SegmentedControl_sc_unchecked_text_color,
                                                _uncheckedTextColor);
                    _borderColor = typedArray.GetColor(Resource.Styleable.SegmentedControl_sc_border_color,
                                                _tintColor);
                }
                finally
                {
                    typedArray.Recycle();
                }
            }
            _layoutSelector = new LayoutSelector(this, _cornerRadius);
            CheckedChange += (object sender, CheckedChangeEventArgs e) => {
                int checkedId = e.CheckedId;
                TransitionDrawable current = _drawableMap.GetValueOrDefault(checkedId);
                current?.ReverseTransition(200);
                if (_lastCheckId != 0)
                {
                    TransitionDrawable last = _drawableMap.GetValueOrDefault(_lastCheckId);
                    if (last != null)
                    {
                        last.ReverseTransition(200);
                    }
                }
                _lastCheckId = checkedId;
                _checkedChangeAction.Invoke(CheckedText);
            };
        }
        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();
            UpdateBackground();
        }
        private void UpdateBackground()
        {
            _drawableMap = new Dictionary<int, TransitionDrawable>();
            for (int i = 0, count = ChildCount; i < count; i++)
            {
                View child = GetChildAt(i);
                UpdateBackground(child);
                // If this is the last view, don't set LayoutParams
                if (i == count - 1)
                {
                    break;
                }
                LayoutParams initParams = (LayoutParams) child.LayoutParameters;
                LayoutParams layoutParams = new LayoutParams(initParams.Width, initParams.Height, initParams.Weight);
                // Check orientation for proper margins
                if (Orientation == Android.Widget.Orientation.Horizontal)
                {
                    layoutParams.SetMargins(0, 0, -_margin, 0);
                }
                else
                {
                    layoutParams.SetMargins(0, 0, 0, -_margin);
                }
                child.LayoutParameters = layoutParams;
            }
        }
        private void UpdateBackground(View view)
        {
            int selected = _layoutSelector.GetSelectedDrawable();
            int unSelected = _layoutSelector.GetUnselectedDrawable();
            //Set text color
            ColorStateList colorStateList = new ColorStateList(new int[][]
                        {
                            new int[] { -Android.Resource.Attribute.StateChecked },
                            new int[] { Android.Resource.Attribute.StateChecked }
                        },
                        new int[] { _uncheckedTextColor, _checkedTextColor });
            ((Button) view).SetTextColor(colorStateList);
            //Redraw with tint color
            Drawable checkedDrawable = Context.GetDrawable(selected).Mutate();
            Drawable uncheckedDrawable = Context.GetDrawable(unSelected).Mutate();
            ((GradientDrawable)checkedDrawable).SetColor(_tintColor);
            ((GradientDrawable)checkedDrawable).SetStroke(_margin, _borderColor);
            ((GradientDrawable)uncheckedDrawable).SetStroke(_margin, _borderColor);
            ((GradientDrawable)uncheckedDrawable).SetColor(_uncheckedTintColor);

            float[] childRadii = _layoutSelector.GetChildRadii(view);
            //Set proper radius
            ((GradientDrawable)checkedDrawable).SetCornerRadii(childRadii);
            ((GradientDrawable)uncheckedDrawable).SetCornerRadii(childRadii);

            int maskColor = Color.Argb(50, _tintColor.R, _tintColor.G, _tintColor.B);
            GradientDrawable maskDrawable = (GradientDrawable)Context.GetDrawable(unSelected).Mutate();
            maskDrawable.SetStroke(_margin, _borderColor);
            maskDrawable.SetCornerRadii(childRadii);
            maskDrawable.SetColor(maskColor);
            LayerDrawable pressedDrawable = new LayerDrawable(new Drawable[] { uncheckedDrawable, maskDrawable });

            Drawable[] drawables = { uncheckedDrawable, checkedDrawable };
            TransitionDrawable transitionDrawable = new TransitionDrawable(drawables);
            if (((RadioButton)view).Checked)
            {
                transitionDrawable.ReverseTransition(0);
            }
         
            StateListDrawable stateListDrawable = new StateListDrawable();
            stateListDrawable.AddState(new int[] { -Android.Resource.Attribute.StateChecked,
                                                    Android.Resource.Attribute.StatePressed
                                                    },
                                                    pressedDrawable);

            stateListDrawable.AddState(StateSet.WildCard.ToArray(), transitionDrawable);
            _drawableMap.Add(view.Id, transitionDrawable);
            view.Background = stateListDrawable;
        }
        public override void OnViewRemoved(View child)
        {
            base.OnViewRemoved(child);
            _drawableMap.Remove(child.Id);
        }
        public void Configure(IEnumerable<string> values, string selected, Action<string> checkedChangeAction)
        {
            _checkedChangeAction = checkedChangeAction;
            RemoveAllViews();
            foreach (string value in values)
            {
                RadioButton radioButton = new RadioButton(new ContextThemeWrapper(Context, Resource.Style.SegmentedControlRadioButton), null, 0)
                {
                    Id = View.GenerateViewId(),
                    Checked = value.Equals(selected),
                    Clickable = true,
                    Text = value,
                    Tag = value,
                    ContentDescription = value
                };
                radioButton.LayoutParameters = new RadioGroup.LayoutParams(0, ViewGroup.LayoutParams.MatchParent, 1);
                AddView(radioButton);
            }
            UpdateBackground();
        }
        /// <summary>
        /// This class is used to provide the proper layout based on the view.
        /// Also provides the proper radius for corners.
        /// The layout is the same for each selected left/top middle or right/bottom button.
        /// float tables for setting the radius via Gradient.setCornerRadii are used instead
        /// of multiple xml drawables.
        /// </summary>
        private class LayoutSelector
        {
            private int _children;
            private int _child;
            private float[] _lastQueriedChildRadii;    // last view radii float table

            private const int SELECTED_LAYOUT = Resource.Drawable.segmentedcontrolchecked;
            private const int UNSELECTED_LAYOUT = Resource.Drawable.segmentedcontrolunchecked;
            private readonly float _layoutRadius;    //this is the radius read by attributes or xml dimens
            private readonly float _minDivider;
            private readonly float[] _leftRadioButtonRadiusTable;    // left radio button
            private readonly float[] _rightRadioButtonRadiusTable;   // right radio button
            private readonly float[] _middleRadioButtonRadiusTable;  // middle radio button
            private readonly float[] _defaultRadioButtonRadiusTable; // default radio button
            private readonly float[] _topRadioButtonRadiusTable;     // top radio button
            private readonly float[] _bottomRadioButtonRadiusTable;     // bottom radio button
            private readonly SegmentedControl _segmentedControl;
            private readonly Android.Widget.Orientation _orientation;
            public LayoutSelector(SegmentedControl segmentControl, float cornerRadius)
            {
                _segmentedControl = segmentControl;
                _orientation = _segmentedControl.Orientation;
                _minDivider = TypedValue.ApplyDimension(ComplexUnitType.Dip, 0.1f, _segmentedControl.Resources.DisplayMetrics);    //0.1 dp to px
                _children = -1; // Init this to force setChildRadii() to enter for the first time.
                _child = -1; // Init this to force setChildRadii() to enter for the first time
                _layoutRadius = cornerRadius;
                _leftRadioButtonRadiusTable = new float[] { _layoutRadius, _layoutRadius, _minDivider, _minDivider, _minDivider, _minDivider, _layoutRadius, _layoutRadius };
                _rightRadioButtonRadiusTable = new float[] { _minDivider, _minDivider, _layoutRadius, _layoutRadius, _layoutRadius, _layoutRadius, _minDivider, _minDivider };
                _middleRadioButtonRadiusTable = new float[] { _minDivider, _minDivider, _minDivider, _minDivider, _minDivider, _minDivider, _minDivider, _minDivider };
                _defaultRadioButtonRadiusTable = new float[] { _layoutRadius, _layoutRadius, _layoutRadius, _layoutRadius, _layoutRadius, _layoutRadius, _layoutRadius, _layoutRadius };
                _topRadioButtonRadiusTable = new float[] { _layoutRadius, _layoutRadius, _layoutRadius, _layoutRadius, _minDivider, _minDivider, _minDivider, _minDivider };
                _bottomRadioButtonRadiusTable = new float[] { _minDivider, _minDivider, _minDivider, _minDivider, _layoutRadius, _layoutRadius, _layoutRadius, _layoutRadius };
            }
            public int GetSelectedDrawable()
            {
                return SELECTED_LAYOUT;
            }
            public int GetUnselectedDrawable()
            {
                return UNSELECTED_LAYOUT;
            }
            public float[] GetChildRadii(View view)
            {
                int newChildren = _segmentedControl.ChildCount;
                int newChild = _segmentedControl.IndexOfChild(view);
                // If same values are passed, just return. No need to update anything
                if (_children == newChildren && _child == newChild)
                {
                    return _lastQueriedChildRadii;
                }
                // Set the new values
                _children = newChildren;
                _child = newChild;
                // if there is only one child provide the default radio button
                if (_children == 1)
                {
                    _lastQueriedChildRadii = _defaultRadioButtonRadiusTable;
                }
                else if (_child == 0)
                { //left or top
                    _lastQueriedChildRadii = (_orientation == Android.Widget.Orientation.Horizontal) ? _leftRadioButtonRadiusTable : _topRadioButtonRadiusTable;
                }
                else if (_child == _children - 1)
                {  //right or bottom
                    _lastQueriedChildRadii = (_orientation == Android.Widget.Orientation.Horizontal) ? _rightRadioButtonRadiusTable : _bottomRadioButtonRadiusTable;
                }
                else
                {  //middle
                    _lastQueriedChildRadii = _middleRadioButtonRadiusTable;
                }
                return _lastQueriedChildRadii;
            }
        }
    }
}