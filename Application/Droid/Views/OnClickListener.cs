﻿using System;
using Android.Views;
namespace myGovID.Droid.Views
{
    public class OnClickListener : Java.Lang.Object, View.IOnClickListener
    {
        private Action _action;
        public OnClickListener(Action action)
        {
            _action = action;
        }
        public void OnClick(View v)
        {
            // TODO: Protect against double clicks.
            _action.Invoke();
        }
    }
}