﻿using System;
using Android.Content;
using Android.Graphics;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables;

namespace myGovID.Droid.Views
{
    public class VerificationCode : EditText
    {
        private const String XML_NAMESPACE_ANDROID = "http://schemas.android.com/apk/res/android";
        private readonly int _maxLength;
        private Drawable _rectangleBackground;
        public VerificationCode(Context context) :
            base(context)
        {
            Initialize();
        }
        public VerificationCode(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            _maxLength = attrs.GetAttributeIntValue(XML_NAMESPACE_ANDROID, "maxLength", 0);
            Initialize();
        }
        public VerificationCode(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            _maxLength = attrs.GetAttributeIntValue(XML_NAMESPACE_ANDROID, "maxLength", 0);
            Initialize();
        }
        private void Initialize()
        {
            _rectangleBackground = Context.Resources.GetDrawable(Resource.Drawable.rectanglebackground, null);
            if (_rectangleBackground != null)
            {
                MovementMethod = null;
                SetCursorVisible(false);
                SetBackgroundColor(Color.Transparent);
                SetTextColor(Color.Transparent);
                FocusChange += (object sender, FocusChangeEventArgs e) =>
                {
                    if(e.HasFocus)
                    {
                        SetSelection(Text.Length, Text.Length);
                    }
                };
                Touch += (object sender, TouchEventArgs e) => 
                {
                    base.OnTouchEvent(e.Event);
                    SetSelection(Text.Length, Text.Length);
                    e.Handled = true;
                };
            }
        }
        public override void Draw(Canvas canvas)
        {
            if (_rectangleBackground == null)
            {
                base.Draw(canvas);
                return;
            }
            int totalWeight = (3 * _maxLength) + _maxLength - 1;
            int totalWidth = MeasuredWidth;
            int width = 0;
            // Ensure that all the box borders are visible.
            do
            {
                if(width > 0) {
                    totalWidth--;
                }
                width = (int)Math.Ceiling((float)totalWidth / totalWeight);
            } while (totalWeight * width > MeasuredWidth);
            int rectangleWidth = 3 * width;
            int rectangleHeight = MeasuredHeight;
            int x = 0;
            for (int index = 0; index < _maxLength; index++)
            {
                Rect rect = new Rect(x, 0, x + rectangleWidth, rectangleHeight);
                DrawRect(canvas, rect);
                if (index < Text.Length)
                {
                    DrawText(canvas, rect, "" + Text[index]);
                }
                x += 3 * width;
                if (index < _maxLength - 1)
                {
                    x += width;
                }
            }
        }
        private void DrawRect(Canvas canvas, Rect rect) {
            _rectangleBackground.SetBounds(rect.Left, rect.Top, rect.Right, rect.Bottom);
            _rectangleBackground.Draw(canvas);
        }
        private void DrawText(Canvas canvas, Rect rect, string text) {
            Paint textPaint = new Paint()
            {
                Color = Color.Gray,
                TextSize = Resources.GetDimensionPixelSize(Resource.Dimension.verificationCodeFontSize),
                AntiAlias = true
            };
            textPaint.SetTypeface(Typeface);
            Rect textMathRect = new Rect();
            textPaint.GetTextBounds(text, 0, 1, textMathRect);
            canvas.DrawText(text,
                            rect.Left + rect.Width() / 2f - textMathRect.Width() / 2f,
                            rect.Top + rect.Height() / 2 + textMathRect.Height() / 2f,
                            textPaint);
        }
    }
}