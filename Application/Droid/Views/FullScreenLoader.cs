﻿using Android.App;
using Android.Content;
using Android.Graphics.Drawables;
using Android.Views;
using Android.Widget;
using System;

namespace myGovID.Droid.Views
{
    public class FullScreenLoader
    {
        private readonly Dialog _dialog;
        private readonly AnimatedVectorDrawable _tickView;
        private EventHandler _showEventHandler;
        private EventHandler _dismissEventHandler;
        public FullScreenLoader(Context context, string title = null, bool cancelable = false, IDialogInterfaceOnCancelListener cancelListener = null)
        {
            LayoutInflater inflator = (LayoutInflater)context
                    .GetSystemService(Context.LayoutInflaterService);
            View view = inflator.Inflate(Resource.Layout.FullScreenLoader, null);
            View _tickViewContainer = view.FindViewById<View>(Resource.Id.tickView);
            _tickView = (AnimatedVectorDrawable)_tickViewContainer.Background;
            TextView tv = view.FindViewById<TextView>(Resource.Id.title);
            tv.Visibility = string.IsNullOrWhiteSpace(title) ? ViewStates.Gone : ViewStates.Visible;
            if (title != null)
            {
                tv.Text = title;
            }
            _dialog = new Dialog(context, Resource.Style.FullScreenDialog);
            _dialog.SetContentView(view);
            _dialog.SetCancelable(cancelable);
            _dialog.SetOnCancelListener(cancelListener);
        }
        public void Show(Action action = null)
        {
            if (_dialog.IsShowing)
            {
                action?.Invoke();
                return;
            }
            if (action != null)
            {
                _dialog.ShowEvent -= _showEventHandler;
                _showEventHandler = (dialog, args) =>
                {
                    _dialog.ShowEvent -= _showEventHandler;
                    action.Invoke();
                };
                _dialog.ShowEvent += _showEventHandler;
            }
            _dialog.Show();
        }
        public void Dismiss(Action action = null, bool showCompletionMarker = false)
        {
            if (showCompletionMarker)
            {
                ShowCompletionAnimation(() =>
                {
                    _tickView.Reset();
                    action?.Invoke();
                });
                return;
            }
            if (!_dialog.IsShowing)
            {
                action?.Invoke();
                return;
            }
            if (action != null)
            {
                _dialog.DismissEvent -= _dismissEventHandler;
                _dismissEventHandler = (dialog, args) =>
                {
                    _dialog.DismissEvent -= _dismissEventHandler;
                    // Reset the animation tick.
                    _tickView.Reset();
                    _tickView.ClearAnimationCallbacks();
                    action.Invoke();
                };
                _dialog.DismissEvent += _dismissEventHandler;
            }
            _dialog.Dismiss();
        }
        private void ShowCompletionAnimation(Action action)
        {
            _tickView.RegisterAnimationCallback(new CallbackAnimationListener(action));
            _tickView.Start();
        }
        public Dialog GetDialog()
        {
            return _dialog;
        }
        private sealed class CallbackAnimationListener : Animatable2AnimationCallback
        {
            private Action _action;
            public CallbackAnimationListener(Action action)
            {
                _action = action;
            }
            public override void OnAnimationEnd(Drawable drawable)
            {
                _action.Invoke();
            }
        }
    }
}
