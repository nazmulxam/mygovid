﻿using System;
namespace myGovID.Droid.Constants
{
    public static class ActivityConstants
    {
        public const string Label = "myGovID";
        public const string ThemeBase = "@style/myGovId.Theme";
        public const string ThemeNoActionBar = "@style/myGovId.Theme.NoActionBar";
        public const string ThemeTransparent = "@style/myGovId.Theme.Transparent";
        public const string ThemeDark = "@style/myGovId.Theme.Dark";
        public const string ThemeSplash = "@style/myGovId.Theme.Splash";
    }
}
