﻿using System.Linq;
using Android.Content;
using Android.Content.PM;
using myGovID.Application.Launcher.Contract;

namespace myGovID.Droid.Configuration
{
    public class ApplicationInfo : IApplicationInfo
    {
        public string Name { get; }
        public string Version { get; }
        public string BuildDate { get; }
        public string FilesDirectory { get; }

        public ApplicationInfo(Context context)
        {
            PackageInfo pInfo = context.PackageManager.GetPackageInfo(context.PackageName, 0);
            Name = pInfo.PackageName.Split('.').Last();
            Version = pInfo.VersionName;
            BuildDate = null;
            FilesDirectory = context.FilesDir.AbsolutePath;
        }
    }
}