﻿using myGovID.Services.Dependency.Droid;
using myGovID.Services.Crypto.Droid;
using myGovID.Business.Contract.Common.Application;

namespace myGovID.Droid.Configuration
{
    public class CryptoConfig : ICryptoConfig
    {
        public string Version { get; }
        public string BuildDate { get; }
        public string KeystoreDirectory { get; }
        public CryptoConfig()
        {
            IApplicationMetaData applicationInfo = DependencyService.Shared.Resolve<IApplicationMetaData>();
            Version = applicationInfo.Version;
            BuildDate = applicationInfo.BuildDate;
            KeystoreDirectory = applicationInfo.FilesDirectory;
        }
    }
}