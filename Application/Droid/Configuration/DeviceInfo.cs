﻿using System.Collections.Generic;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Support.V4.Hardware.Fingerprint;
using myGovID.Application.Launcher.Contract;
using myGovID.Presentation.Contract;

namespace myGovID.Droid.Configuration
{
    public class DeviceInfo : IDeviceInfo
    {
        public string PlatformType { get; }
        public string RuntimeType { get; }
        public string RuntimeVersion { get; }
        public string FormFactorType => FormFactor.Phone.ToString();
        public IEnumerable<DeviceCapabilityInfo> Capabilities { get; }
        public DeviceInfo(Context context)
        {
            // TODO: Change the below once spartans has got entries.
            PlatformType = "Apple";
            RuntimeType = Presentation.Contract.RuntimeType.iOS.ToString();
            //PlatformType = "Android";
            //RuntimeType = Presentation.Contract.RuntimeType.Android.ToString();
            RuntimeVersion = Build.VERSION.Release;

            var capabilityList = new List<DeviceCapabilityInfo>();

            if (HasFingerprint(context))
            {
                capabilityList.Add(DeviceCapabilityInfo.Fingerprint);
            }

            if (HasFacialVerification(context))
            {
                capabilityList.Add(DeviceCapabilityInfo.FacialVerification);
            }

            if (HasCamera(context))
            {
                capabilityList.Add(DeviceCapabilityInfo.Camera);
            }

            Capabilities = capabilityList;
        }

        private bool HasFingerprint(Context context)
        {
            return context.PackageManager.HasSystemFeature(PackageManager.FeatureFingerprint) && HasFingerprintEnrolled(context);
        }

        private bool HasFacialVerification(Context context)
        {
            return false;
        }

        private bool HasCamera(Context context)
        {
            return context.PackageManager.HasSystemFeature(PackageManager.FeatureCamera);
        }

        private bool HasFingerprintEnrolled(Context context)
        {
            FingerprintManagerCompat fingerprintManager = FingerprintManagerCompat.From(context);
            return fingerprintManager.IsHardwareDetected && fingerprintManager.HasEnrolledFingerprints;
        }
    }
}