﻿using System;
using System.Linq;
using Android.App;
using Android.Text;
using Android.Widget;
using myGovID.Droid.Constants;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.CaptureEmailVerification;

namespace myGovID.Droid.Steps.VerifyEmail
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeBase,
              WindowSoftInputMode = Android.Views.SoftInput.StateHidden,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class VerifyEmailActivity : StepActivity<ICaptureEmailVerificationStep, CaptureEmailVerificationViewModel>
    {
        private ImageButton _backButton;
        private Button _nextButton;
        private TextView _resendCodeButton;
        private EditText _verificationCode;
        private TextView _errorLabel;
        private TextView _descriptionLabel;
        protected override void OnViewModelLoaded()
        {
            SetActionBar(Resource.Layout.ActionBarCenterTitleWithBackButton, Resource.String.create_an_account);
            SetContentView(Resource.Layout.VerifyEmail);
            _backButton = FindViewById<ImageButton>(Resource.Id.actionbarBack);
            _nextButton = FindViewById<Button>(Resource.Id.nextButton);
            _resendCodeButton = FindViewById<TextView>(Resource.Id.resendCode);
            _verificationCode = FindViewById<EditText>(Resource.Id.verificationCode);
            _errorLabel = FindViewById<TextView>(Resource.Id.errorLabel);
            _descriptionLabel = FindViewById<TextView>(Resource.Id.emailVerificationDescription);
            string emailAddressFormatted = string.Format("<br>{0}", ViewModel.EmailAddress ?? "");
            string description = String.Format(Resources.GetString(Resource.String.email_verification_description), emailAddressFormatted);
            _descriptionLabel.TextFormatted = Html.FromHtml(description, FromHtmlOptions.ModeCompact);
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            FormInput<string> formInput = (FormInput<string>)ViewModel.Inputs.First();
            Bind(formInput, _verificationCode, _errorLabel);
            Bind(_nextButton, ActionType.Next);
            Bind(_resendCodeButton, ActionType.Clone);
            Bind(_backButton, ActionType.Previous);
        }
    }
}
