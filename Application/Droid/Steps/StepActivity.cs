﻿using System;
using System.Linq;
using System.Collections.Generic;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Views;
using myGovID.Presentation.Contract;
using GalaSoft.MvvmLight.Helpers;
using Newtonsoft.Json;
using myGovID.Services.Dependency.Droid;
using Android.Text;
using myGovID.Droid.Views;
using System.Threading.Tasks;
using myGovID.Presentation.Contract.Inputs;
using Android.App;
using myGovID.Presentation.Contract.Constraints;
using System.Globalization;
using myGovID.Services.Timer.Droid;
using myGovID.Services.Timer.Contract;
using myGovID.Services.Event.Contract;
using Android.Graphics;
using Java.Text;

namespace myGovID.Droid.Steps
{
    public abstract class StepActivity<S, V> : StepActivity<S, V, Enum> where S : IStep where V : IViewModel
    { }
    public abstract class StepActivity<S, V, E> : TimerActivity, IStepActivity where S : IStep where V : IViewModel
    {
        public override IDeviceActivityTimer DeviceActivityTimer { get; set; }
        protected ViewGroup PageErrorView { get; set; }
        private S _step;
        protected V ViewModel => (V)_step.ViewModel;
        public IStep Step { get => _step; set => _step = (S)value; }
        // Data bindings.
        private readonly List<Binding> _bindings = new List<Binding>();
        private IAction<ActionType, E> GetPreviousStep(E subType = default) => FindAction(_step, ActionType.Previous, subType);
        // Loading dialog
        private FullScreenLoader _fullScreenDialog;
        // Datepicker dialog
        private Dialog _datePickerDialog;
        // Step Continuation
        private Action<Task<IStep>> StepContinuation;
        // Events
        private IEventService _eventService;
        // Step Orchestrator
        private StepOrchestrator _stepOrchestrator;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            // Ensure that the screens are rendered in a secure space so that they are unavailable for screenshots or recording
            Window.SetFlags(WindowManagerFlags.Secure, WindowManagerFlags.Secure);
            DeviceActivityTimer = DependencyService.Shared.Resolve<IDeviceActivityTimer>();
            _eventService = DependencyService.Shared.Resolve<IEventService>();
            _stepOrchestrator = DependencyService.Shared.Resolve<StepOrchestrator>();
            base.OnCreate(savedInstanceState);
            StepContinuation = (taskResult) =>
            {
                if (taskResult.IsCanceled || taskResult.IsFaulted)
                {
                    Exception exception = taskResult.Exception.GetBaseException();
                    TransitionOnError(exception);
                    return;
                }
                TransitionToStep(taskResult.Result);
            };
        }
        protected override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);
            Intent = intent;
        }
        private void Transition(Intent intent)
        {
            StartActivity(intent);
            OverridePendingTransition(Android.Resource.Animation.FadeIn, Android.Resource.Animation.FadeOut);
        }
        protected bool HasPageError()
        {
            return _step.Messages?.First((el) => el.Type.ToLower() == "error") != null;
        }
        protected virtual void UpdatePageError()
        {
            IEnumerable<PageMessage> messages = _step.Messages;
            if (messages == null)
            {
                PageErrorView.Visibility = ViewStates.Gone;
                return;
            }
            PageMessage firstError = messages.First((el) => el.Type.ToLower() == "error");
            PageErrorView.Visibility = firstError != null ? ViewStates.Visible : ViewStates.Gone;
            if (firstError != null)
            {
                PageErrorView.FindViewById<TextView>(Resource.Id.title).Text = firstError.Title;
                PageErrorView.FindViewById<TextView>(Resource.Id.description).TextFormatted = Html.FromHtml(firstError.Description, FromHtmlOptions.ModeCompact);
            }
        }
        private void ChangeNavigationThemeToLight()
        {
            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            var uiFlags = Window.DecorView.SystemUiVisibility;
            var newUiFlags = (int)uiFlags;
            newUiFlags |= (int)SystemUiFlags.LightNavigationBar;
            Window.DecorView.SystemUiVisibility = (StatusBarVisibility)newUiFlags;
        }
        protected override void OnResume()
        {
            base.OnResume();
            if (!Intent.HasExtra("StepId"))
            {
                throw new NotSupportedException("StepId is required.");
            }
            if (!Intent.GetBooleanExtra("DarkNavigationTheme", false))
            {
                ChangeNavigationThemeToLight();
            }
            string stepId = Intent.Extras.GetString("StepId");
            _step = _stepOrchestrator.GetStep<S>(stepId);
            OnViewModelLoaded();
            PageErrorView = FindViewById<ViewGroup>(Resource.Id.pageErrorView);
            if (PageErrorView != null)
            {
                UpdatePageError();
            }
            if (_step.ChildStep != null)
            {
                TransitionToStep(_step.ChildStep);
            }
            // Detach the bindings just before attaching them so that the bindings dont run twice.
            DetachBindings();
            AttachBindings();
        }
        private void DetachBindings()
        {
            foreach (Binding binding in _bindings)
            {
                binding.Detach();
            }
            _bindings.Clear();
            OnBindingsDetached();
        }
        protected override void OnPause()
        {
            base.OnPause();
            DetachBindings();
        }
        protected override void OnStart()
        {
            _eventService.Subscribe<IStep>(this, EventKey.Step, TransitionToStep);
            base.OnStart();
        }
        protected override void OnStop()
        {
            _eventService.UnSubscribe<IStep>(this, EventKey.Step);
            base.OnStop();
        }
        public override void OnBackPressed()
        {
            var previousStep = GetPreviousStep();
            if (previousStep != null)
            {
                previousStep.Run().ContinueWith(StepContinuation);
            }
        }
        protected abstract void OnViewModelLoaded();
        protected virtual void AttachBindings() { }
        protected virtual void OnBindingsDetached() { }
        protected void TransitionToStep(IStep newStep)
        {
            RunOnUiThread(() =>
            {
                if (IsSuccessStep(newStep))
                {
                    HideLoading(() => ExecuteAction<Enum>(newStep, ActionType.Next), true);
                    return;
                }
                HideLoading(() =>
                {
                    if (newStep == null)
                    {
                        return;
                    }
                    Intent intent = _stepOrchestrator.GetActivity(this, newStep);
                    Transition(intent);
                });
            });
        }
        protected void TransitionOnError(Exception exception)
        {
            Console.WriteLine("Exception " + exception.Message);
            TransitionToStep(_stepOrchestrator.GetDeadEndStep("UNK000002", "Uncaught Exception", exception.Message));
        }
        private bool IsSuccessStep(IStep step)
        {
            StepOrchestrator stepOrchestrator = DependencyService.Shared.Resolve<StepOrchestrator>();
            return stepOrchestrator.IsSuccessStep(step);
        }
        protected IAction<ActionType, A> FindAction<A>(IStep step, ActionType actionType, A subType = default)
        {
            return step.GetActions<A>().FirstOrDefault((action) =>
            {
                if (action.Type != actionType)
                {
                    return false;
                }
                if (action.SubType == null && subType == null)
                {
                    return true;
                }
                if (action.SubType != null && subType != null)
                {
                    return action.SubType.Equals(subType);
                }
                return false;
            });
        }
        public void Bind(FormInput<string> input, EditText textField, TextView errorLabel, TextView titleLabel = null, string accessibilityId = null)
        {
            string accessibilityIdentifier = accessibilityId ?? input.AccessibilityIdentifier;
            if (titleLabel != null)
            {
                titleLabel.Text = input.Label;
                titleLabel.ContentDescription = accessibilityIdentifier + "Title";
            }
            textField.ContentDescription = accessibilityIdentifier;
            errorLabel.ContentDescription = accessibilityIdentifier + "Error";
            textField.Hint = input.Placeholder;
            textField.Text = input.Value;
            textField.TextChanged += (s, e) => { }; // To fool the linker.
            Binding hideErrorBinding = new Binding<string, string>(input, () => input.Error).WhenSourceChanges(() => errorLabel.Visibility = input.Error == null ? ViewStates.Gone : ViewStates.Visible);
            Binding errorBinding = new Binding<string, string>(input, () => input.Error, errorLabel, () => errorLabel.Text, BindingMode.OneWay);
            Binding textBinding = new Binding<string, string>(textField, () => textField.Text, input, () => input.Value, BindingMode.TwoWay);
            Binding visibilityBinding = new Binding<bool, bool>(input, () => input.Visible).WhenSourceChanges(() =>
            {
                if (titleLabel != null)
                {
                    titleLabel.Visibility = input.Visible ? ViewStates.Visible : ViewStates.Gone;
                }
                textField.Visibility = input.Visible ? ViewStates.Visible : ViewStates.Gone;
                errorLabel.Visibility = input.Error == null || !input.Visible ? ViewStates.Gone : ViewStates.Visible;
            });
            Binding hintBinding = new Binding<string, string>(input, () => input.Placeholder, textField, () => textField.Hint, BindingMode.OneWay);
            AddBinding(hideErrorBinding);
            AddBinding(errorBinding);
            AddBinding(textBinding);
            AddBinding(hintBinding);
            AddBinding(visibilityBinding);
        }
        public void Bind(string propertyName, TextView label, string accessibilityIdentifier)
        {
            label.ContentDescription = accessibilityIdentifier;
            Binding textBinding = new Binding<string, string>(ViewModel, propertyName, label, "Text", BindingMode.OneWay);
            AddBinding(textBinding);
        }
        public void Bind(BooleanInput input, CompoundButton switchField, TextView errorLabel, TextView titleView = null, string accessibilityId = null)
        {
            string accessibilityIdentifier = accessibilityId ?? input.AccessibilityIdentifier;
            switchField.ContentDescription = accessibilityIdentifier;
            if (titleView != null)
            {
                titleView.ContentDescription = accessibilityIdentifier + "Title";
                titleView.TextFormatted = Html.FromHtml(input.Label, FromHtmlOptions.ModeCompact);
            }
            switchField.Checked = input.Value;
            switchField.CheckedChange += (s, e) => { }; // To fool the linker.
            Binding checkedBinding = new Binding<bool, bool>(switchField, () => switchField.Checked, input, () => input.Value, BindingMode.TwoWay);
            if (errorLabel != null)
            {
                Binding hideErrorBinding = new Binding<string, string>(input, () => input.Error).WhenSourceChanges(() => errorLabel.Visibility = input.Error == null ? ViewStates.Gone : ViewStates.Visible);
                Binding errorBinding = new Binding<string, string>(input, () => input.Error, errorLabel, () => errorLabel.Text, BindingMode.OneWay);
                AddBinding(hideErrorBinding);
                AddBinding(errorBinding);
            }
            AddBinding(checkedBinding);
        }
        public void Bind(MultipleChoiceInput<string> input, SegmentedControl segmentedControl, TextView errorLabel, TextView titleView, string accessibilityId = null)
        {
            string accessibilityIdentifier = accessibilityId ?? input.AccessibilityIdentifier;
            segmentedControl.ContentDescription = accessibilityIdentifier;
            titleView.ContentDescription = accessibilityIdentifier + "Title";
            titleView.Text = input.Label;
            segmentedControl.Configure(input.Options, input.Value, (value) => { input.Value = value; });
            segmentedControl.CheckedChange += (s, e) => { }; // To fool the linker.
            Binding hideErrorBinding = new Binding<string, string>(input, () => input.Error).WhenSourceChanges(() => errorLabel.Visibility = input.Error == null ? ViewStates.Gone : ViewStates.Visible);
            Binding errorBinding = new Binding<string, string>(input, () => input.Error, errorLabel, () => errorLabel.Text, BindingMode.OneWay);
            AddBinding(hideErrorBinding);
            AddBinding(errorBinding);
        }
        public void Bind(MultipleChoiceInput<string> input, Spinner spinnerControl, TextView errorLabel, TextView titleView, string accessibilityId = null)
        {
            string accessibilityIdentifier = accessibilityId ?? input.AccessibilityIdentifier;
            spinnerControl.ContentDescription = accessibilityIdentifier;
            titleView.ContentDescription = accessibilityIdentifier + "Title";
            titleView.Text = input.Label;
            List<string> options = new List<string> { input.Placeholder };
            options.AddRange(input.Options);
            ArrayAdapter<string> adapter = new HintArrayAdapter<string>(this, Resource.Layout.SpinnerWrapText, Android.Resource.Id.Text1, options.ToArray<string>());
            adapter.SetDropDownViewResource(Resource.Layout.SpinnerWrapText);
            spinnerControl.Adapter = adapter;
            spinnerControl.ItemSelected += (s, e) =>
            {
                int position = e.Position;
                if (position == 0)
                {
                    input.Value = null;
                    spinnerControl.SelectedView?.FindViewById<TextView>(Android.Resource.Id.Text1).SetTextColor(Color.Gray);
                }
                else
                {
                    input.Value = input.Options.ElementAt(position - 1);
                }
            };
            Binding valueBinding = new Binding<string, string>(input, () => input.Value).WhenSourceChanges(() =>
            {
                int position = string.IsNullOrWhiteSpace(input.Value) ? -1 : input.Options.ToList().FindIndex(item => item == input.Value);
                position += 1;
                spinnerControl.SetSelection(position);
                if (position == 0)
                {
                    spinnerControl.SelectedView?.FindViewById<TextView>(Android.Resource.Id.Text1).SetTextColor(Color.Gray);
                }
            });
            Binding hideErrorBinding = new Binding<string, string>(input, () => input.Error).WhenSourceChanges(() => errorLabel.Visibility = input.Error == null ? ViewStates.Gone : ViewStates.Visible);
            Binding errorBinding = new Binding<string, string>(input, () => input.Error, errorLabel, () => errorLabel.Text, BindingMode.OneWay);
            AddBinding(hideErrorBinding);
            AddBinding(errorBinding);
            AddBinding(valueBinding);
        }
        public void Bind(View view, ActionType actionType, E subType = default, string accessibilityId = null)
        {
            IAction<ActionType, E> action = FindAction<E>(Step, actionType, subType);
            if (action == null)
            {
                return;
            }
            view.ContentDescription = accessibilityId ?? action.AccessibilityIdentifier;
            view.SetOnClickListener(new OnClickListener(() => ExecuteAction(action)));
        }
        public void Bind(ButtonControl buttonControl, Button button, string accessibilityId = null)
        {
            string accessibilityIdentifier = accessibilityId ?? buttonControl.AccessibilityIdentifier;
            button.ContentDescription = accessibilityIdentifier;
            button.SetCommand(buttonControl.OnTapped);
            Binding visibilityBinding = new Binding<bool, bool>(buttonControl, () => buttonControl.Visible).WhenSourceChanges(() => button.Visibility = buttonControl.Visible ? ViewStates.Visible : ViewStates.Gone);
            Binding enabledBinding = new Binding<bool, bool>(buttonControl, () => buttonControl.Enabled).WhenSourceChanges(() => button.Enabled = buttonControl.Enabled);
            Binding labelBinding = new Binding<string, string>(buttonControl, () => buttonControl.Label, button, () => button.Text, BindingMode.OneWay);
            AddBinding(visibilityBinding);
            AddBinding(enabledBinding);
            AddBinding(labelBinding);
        }
        protected void ExecuteAction<A>(IAction<ActionType, A> action, bool showLoading = true)
        {
            if (!action.SkipValidation && !Step.ViewModel.IsValid())
            {
                return;
            }
            if (!showLoading)
            {
                action.Run();
                return;
            }
            ShowLoading(() => ExecuteActionWithDelay(action));
        }
        private async void ExecuteActionWithDelay<A>(IAction<ActionType, A> action)
        {
            Task waitTask = Task.Delay(250);
            Task<IStep> interactionTask = action.Run();
            await Task.WhenAll(waitTask, interactionTask);
            StepContinuation.Invoke(interactionTask);
        }
        protected void ExecuteAction<A>(IStep step, ActionType actionType, A subType = default)
        {
            IAction<ActionType, A> action = FindAction(step, actionType, subType);
            if (action == null)
            {
                return;
            }
            ExecuteAction(action);
        }
        public void AddBinding(Binding binding)
        {
            _bindings.Add(binding);
        }
        public void SetActionBar(int layoutId, int titleId)
        {
            SupportActionBar.SetDisplayShowCustomEnabled(true);
            SupportActionBar.SetCustomView(layoutId);
            SupportActionBar.SetDisplayShowHomeEnabled(false);
            TextView abTitle = (TextView)FindViewById(Resource.Id.actionbarTitle);
            if (abTitle == null)
            {
                return;
            }
            abTitle.Text = Resources.GetString(titleId);
        }
        protected void ShowLoading(Action action)
        {
            if (_fullScreenDialog == null)
            {
                _fullScreenDialog = new FullScreenLoader(this);
            }
            _fullScreenDialog.Show(action);
        }
        protected void HideLoading(Action action, bool showCompletionMarker = false)
        {
            if (_fullScreenDialog == null)
            {
                action?.Invoke();
            }
            else
            {
                _fullScreenDialog.Dismiss(action, showCompletionMarker);
            }
        }
        protected void ShowDatePicker(FormInput<string> input, string dateFormat = null)
        {
            string dateFormatToUse = dateFormat;
            if (string.IsNullOrWhiteSpace(dateFormat))
            {
                DateFormatConstraint dateFormatConstraint = (DateFormatConstraint)input.Constraints.FirstOrDefault((constraint) => constraint.Type == ConstraintType.DateFormat);
                dateFormatToUse = dateFormatConstraint?.DateFormat.GetFormat() ?? "dd/MM/yyyy";
            }
            DateTime currently = string.IsNullOrWhiteSpace(input.Value) ? DateTime.Now : GetDateTime(input.Value, dateFormatToUse);
            _datePickerDialog = CreateDatePickerDialog(currently.Year, currently.Month - 1, currently.Day, dateFormatToUse.Contains("dd"), new DatePickerListener(input, dateFormatToUse));
            _datePickerDialog.SetTitle(input.Label);
            _datePickerDialog.Show();
        }
        private Dialog CreateDatePickerDialog(int year, int month, int day, bool showDayPicker, DatePickerListener datePickerListener)
        {
            return showDayPicker
                ? GetDatePickerDialog(day, month, year, 30, datePickerListener)
                : GetMonthYearDialog(year, month, 30, datePickerListener);
        }
        private DateTime GetDateTime(string dateTimeString, string dateFormat)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            bool success = DateTime.TryParseExact(dateTimeString, dateFormat, provider, DateTimeStyles.None, out DateTime dateTime);
            return success ? dateTime : DateTime.Now;
        }
        private Dialog GetDatePickerDialog(int day, int month, int year, int diffYears, DatePickerListener datePickerListener)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            View dialog = LayoutInflater.Inflate(Resource.Layout.DatePickerDialog, null);
            DatePicker datePicker = dialog.FindViewById<DatePicker>(Resource.Id.datePicker);
            DateTime currentDate = DateTime.Now;
            currentDate.AddYears(diffYears);
            datePicker.MaxDate = currentDate.ToFileTimeUtc();
            builder.SetView(dialog);
            builder.SetPositiveButton("OK", (d, id) =>
            {
                datePickerListener.OnDateSet(null, datePicker.Year, datePicker.Month, datePicker.DayOfMonth);
            });
            builder.SetNegativeButton("CANCEL", (d, id) =>
            {
            });
            return builder.Create();
        }
        private Dialog GetMonthYearDialog(int month, int year, int maxYears, DatePickerListener datePickerListener)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            Java.Util.Calendar cal = new Java.Util.GregorianCalendar();

            View dialog = LayoutInflater.Inflate(Resource.Layout.MonthYearDatePickerDialog, null);
            NumberPicker monthPicker = dialog.FindViewById<NumberPicker>(Resource.Id.monthPicker);
            NumberPicker yearPicker = dialog.FindViewById<NumberPicker>(Resource.Id.yearPicker);

            DateFormatSymbols dfs = new DateFormatSymbols();
            string[] months = dfs.GetShortMonths();

            monthPicker.SetDisplayedValues(months);
            monthPicker.MinValue = 0;
            monthPicker.MaxValue = 11;
            monthPicker.Value = cal.Get(Java.Util.CalendarField.Month);

            int currentYear = cal.Get(Java.Util.CalendarField.Year);
            yearPicker.MinValue = currentYear - maxYears;
            yearPicker.MaxValue = currentYear + maxYears;
            yearPicker.Value = year < yearPicker.MinValue || year > yearPicker.MaxValue ? currentYear : year;

            builder.SetView(dialog);
            builder.SetPositiveButton("OK", (d, id) =>
            {
                datePickerListener.OnDateSet(null, yearPicker.Value, monthPicker.Value, 1);
            });
            builder.SetNegativeButton("CANCEL", (d, id) =>
            {
            });
            return builder.Create();
        }
    }

    internal class DatePickerListener : Java.Lang.Object, DatePickerDialog.IOnDateSetListener
    {
        private readonly FormInput<string> _input;
        private readonly string _dateFormat;
        internal DatePickerListener(FormInput<string> input, string dateFormat)
        {
            _input = input;
            _dateFormat = dateFormat;
        }
        public void OnDateSet(DatePicker view, int year, int month, int dayOfMonth)
        {
            DateTime selectedDate = new DateTime(year, month + 1, dayOfMonth);
            _input.Value = selectedDate.ToString(_dateFormat);
        }
    }
}