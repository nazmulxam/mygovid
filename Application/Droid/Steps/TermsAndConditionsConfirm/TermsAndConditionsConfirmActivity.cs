﻿using Android.App;
using Android.Widget;
using myGovID.Droid.Constants;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.TermsAndConditionsConfirm;

namespace myGovID.Droid.Steps.TermsAndConditionsConfirm
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeTransparent,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class TermsAndConditionsConfirmActivity : StepActivity<ITermsAndConditionsConfirmStep, TermsAndConditionsConfirmViewModel>
    {
        private Button _nextButton;
        private Button _previousButton;
        protected override void OnViewModelLoaded()
        {
            SetContentView(Resource.Layout.TermsAndConditionsConfirm);
            _nextButton = FindViewById<Button>(Resource.Id.nextButton);
            _previousButton = FindViewById<Button>(Resource.Id.previousButton);
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(_nextButton, ActionType.Next, null, "OnNextOverlay");
            Bind(_previousButton, ActionType.Previous, null, "OnPreviousOverlay");
        }
    }
}
