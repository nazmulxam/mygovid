﻿using System.Linq;
using Android.App;
using Android.Widget;
using myGovID.Contract.Steps.CapturePersonalDetails;
using myGovID.Droid.Constants;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.CapturePersonalDetails;

namespace myGovID.Droid.Steps.CapturePersonalDetails
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeBase,
              WindowSoftInputMode = Android.Views.SoftInput.StateHidden,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class CapturePersonalDetailsActivity : StepActivity<ICapturePersonalDetailsStep, CapturePersonalDetailsViewModel>
    {
        private EditText _givenName;
        private EditText _familyName;
        private EditText _dateOfBirth;
        private TextView _givenNameTitle;
        private TextView _familyNameTitle;
        private TextView _dateOfBirthTitle;
        private TextView _givenNameError;
        private TextView _familyNameError;
        private TextView _dateOfBirthError;
        private Button _nextButton;
        protected override void OnViewModelLoaded()
        {
            SetActionBar(Resource.Layout.ActionBarCenterTitle, Resource.String.create_an_account);
            SetContentView(Resource.Layout.CapturePersonalDetails);
            _nextButton = FindViewById<Button>(Resource.Id.nextButton);
            _givenName = FindViewById<EditText>(Resource.Id.givenNameET);
            _familyName = FindViewById<EditText>(Resource.Id.familyNameET);
            _dateOfBirth = FindViewById<EditText>(Resource.Id.dateOfBirthET);
            _givenNameTitle = FindViewById<TextView>(Resource.Id.givenNameTitle);
            _familyNameTitle = FindViewById<TextView>(Resource.Id.familyNameTitle);
            _dateOfBirthTitle = FindViewById<TextView>(Resource.Id.dateOfBirthTitle);
            _givenNameError = FindViewById<TextView>(Resource.Id.givenNameError);
            _familyNameError = FindViewById<TextView>(Resource.Id.familyNameError);
            _dateOfBirthError = FindViewById<TextView>(Resource.Id.dateOfBirthError);
            _dateOfBirth.Click += (sender, args) =>
            {
                ShowDatePicker((FormInput<string>)ViewModel.Inputs.First((input) => input.MetaData == InputMetaData.DateOfBirth));
            };
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind((FormInput<string>)ViewModel.Inputs.First((input) => input.MetaData == InputMetaData.GivenName), _givenName, _givenNameError, _givenNameTitle);
            Bind((FormInput<string>)ViewModel.Inputs.First((input) => input.MetaData == InputMetaData.FamilyName), _familyName, _familyNameError, _familyNameTitle);
            Bind((FormInput<string>)ViewModel.Inputs.First((input) => input.MetaData == InputMetaData.DateOfBirth), _dateOfBirth, _dateOfBirthError, _dateOfBirthTitle);
            Bind(_nextButton, ActionType.Next);
        }
    }
}