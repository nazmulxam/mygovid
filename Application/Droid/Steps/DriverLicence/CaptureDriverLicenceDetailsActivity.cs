﻿using System;
using System.Linq;
using Android.App;
using Android.Widget;
using myGovID.Droid.Constants;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Presentation.Contract.Steps.DriverLicence;

namespace myGovID.Droid.Steps.DriverLicence
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeBase,
              WindowSoftInputMode = Android.Views.SoftInput.StateHidden,
              NoHistory = true,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class CaptureDriverLicenceDetailsActivity : StepActivity<ICaptureDriverLicenceDetailsStep, DriverLicenceViewModel, CaptureDriverLicenceDetailsAction>
    {
        private EditText _licenceNumber;
        private EditText _givenName;
        private EditText _middleName;
        private EditText _familyName;
        private EditText _dateOfBirth;
        private Spinner _state;
        private CheckBox _consent;

        private TextView _licenceNumberTitle;
        private TextView _givenNameTitle;
        private TextView _middleNameTitle;
        private TextView _familyNameTitle;
        private TextView _dateOfBirthTitle;
        private TextView _stateTitle;
        private TextView _consentTitle;

        private TextView _licencetNumberError;
        private TextView _givenNameError;
        private TextView _middleNameError;
        private TextView _familyNameError;
        private TextView _dateOfBirthError;
        private TextView _stateError;
        private TextView _consentError;

        private Button _nextButton;
        private ImageButton _backButton;
        private Button _helpButton;
        protected override void OnViewModelLoaded()
        {
            SetActionBar(Resource.Layout.ActionBarCenterTitleWithBackButtonAndHelp, Resource.String.buildYourIdentity);
            SetContentView(Resource.Layout.CaptureDriverLicenceManually);
            _backButton = FindViewById<ImageButton>(Resource.Id.actionbarBack);
            _helpButton = FindViewById<Button>(Resource.Id.actionbarHelp);
            _nextButton = FindViewById<Button>(Resource.Id.nextButton);

            _licenceNumber = FindViewById<EditText>(Resource.Id.licenceNumberET);
            _givenName = FindViewById<EditText>(Resource.Id.givenNameET);
            _middleName = FindViewById<EditText>(Resource.Id.middleNameET);
            _familyName = FindViewById<EditText>(Resource.Id.familyNameET);
            _dateOfBirth = FindViewById<EditText>(Resource.Id.dateOfBirthET);
            _state = FindViewById<Spinner>(Resource.Id.stateSpinner);
            _consent = FindViewById<CheckBox>(Resource.Id.consent);

            _licenceNumberTitle = FindViewById<TextView>(Resource.Id.licenceNumberTitle);
            _givenNameTitle = FindViewById<TextView>(Resource.Id.givenNameTitle);
            _middleNameTitle = FindViewById<TextView>(Resource.Id.middleNameTitle);
            _familyNameTitle = FindViewById<TextView>(Resource.Id.familyNameTitle);
            _dateOfBirthTitle = FindViewById<TextView>(Resource.Id.dateOfBirthTitle);
            _stateTitle = FindViewById<TextView>(Resource.Id.stateTitle);
            _consentTitle = FindViewById<TextView>(Resource.Id.consentTitle);

            _licencetNumberError = FindViewById<TextView>(Resource.Id.licenceNumberError);
            _givenNameError = FindViewById<TextView>(Resource.Id.givenNameError);
            _middleNameError = FindViewById<TextView>(Resource.Id.middleNameError);
            _familyNameError = FindViewById<TextView>(Resource.Id.familyNameError);
            _dateOfBirthError = FindViewById<TextView>(Resource.Id.dateOfBirthError);
            _stateError = FindViewById<TextView>(Resource.Id.stateError);
            _consentError = FindViewById<TextView>(Resource.Id.consentError);

            _dateOfBirth.Click += (sender, args) =>
            {
                ShowDatePicker((FormInput<string>)ViewModel.Inputs.First((input) => input.MetaData == InputMetaData.DateOfBirth));
            };
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(GetInput<string>(InputMetaData.DriverLicenceDocumentNumber), _licenceNumber, _licencetNumberError, _licenceNumberTitle);
            Bind(GetInput<string>(InputMetaData.GivenName), _givenName, _givenNameError, _givenNameTitle);
            Bind(GetInput<string>(InputMetaData.MiddleName), _middleName, _middleNameError, _middleNameTitle);
            Bind(GetInput<string>(InputMetaData.FamilyName), _familyName, _familyNameError, _familyNameTitle);
            Bind(GetInput<string>(InputMetaData.DateOfBirth), _dateOfBirth, _dateOfBirthError, _dateOfBirthTitle);
            Bind((MultipleChoiceInput<string>)GetInput<string>(InputMetaData.DriverLicenceState), _state, _stateError, _stateTitle);
            Bind((BooleanInput)GetInput<bool>(InputMetaData.Consent), _consent, _consentError, _consentTitle);
            Bind(_nextButton, ActionType.Next, CaptureDriverLicenceDetailsAction.Submit);
            Bind(_backButton, ActionType.Previous, CaptureDriverLicenceDetailsAction.Back);
            Bind(_helpButton, ActionType.Browse, CaptureDriverLicenceDetailsAction.Help);
        }
        private FormInput<T> GetInput<T>(InputMetaData metaData) where T : IComparable
        {
            return (FormInput<T>)ViewModel.Inputs.First((input) => input.MetaData == metaData);
        }
    }
}