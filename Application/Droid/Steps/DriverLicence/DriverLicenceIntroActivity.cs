﻿using Android.App;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Views;
using Android.Widget;
using myGovID.Droid.Constants;
using myGovID.Droid.Steps.HelpOverlay;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.DriverLicence;

namespace myGovID.Droid.Steps.DriverLicence
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeBase,
              WindowSoftInputMode = SoftInput.StateHidden,
              NoHistory = true,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class DriverLicenceIntroActivity : StepActivity<IDriverLicenceOCRIntroStep, DriverLicenceOCRIntroViewModel, DriverLicenceOCRIntroAction>
    {
        private ImageButton _backButton;
        private Button _nextButton;
        private Button _enterManually;
        private View _actionContainer;
        protected override void OnViewModelLoaded()
        {
            SetActionBar(Resource.Layout.ActionBarCenterTitleWithBackButton, Resource.String.buildYourIdentity);
            SetContentView(Resource.Layout.DriverLicenceIntro);
            _backButton = FindViewById<ImageButton>(Resource.Id.actionbarBack);
            _nextButton = FindViewById<Button>(Resource.Id.nextButton);
            _enterManually = FindViewById<Button>(Resource.Id.enterManuallyButton);
            _actionContainer = FindViewById<View>(Resource.Id.actionContainer);
            ViewPager viewPager = FindViewById<ViewPager>(Resource.Id.helpOverlayViewPager);
            viewPager.Adapter = new HelpOverlayPageAdapter(ViewModel.Pages);
            (FindViewById<TabLayout>(Resource.Id.helpOverlayIndicator)).SetupWithViewPager(viewPager, true);
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(_nextButton, ActionType.Next, DriverLicenceOCRIntroAction.OCRCapture);
            Bind(_enterManually, ActionType.Next, DriverLicenceOCRIntroAction.ManualEntry);
            Bind(_backButton, ActionType.Previous);

        }
    }
}