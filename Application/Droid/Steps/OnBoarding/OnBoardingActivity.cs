﻿using Android.App;
using Android.Views;
using Android.Widget;
using Android.Support.V4.View;
using myGovID.Droid.Steps.HelpOverlay;
using Android.Support.Design.Widget;
using myGovID.Presentation.Contract.Steps.OnBoarding;
using myGovID.Presentation.Contract;
using myGovID.Droid.Constants;

namespace myGovID.Droid.Steps.OnBoarding
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeBase,
              WindowSoftInputMode = SoftInput.StateHidden,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class OnBoardingActivity : StepActivity<IOnBoardingStep, OnBoardingViewModel>
    {
        private Button _nextButton;
        private View _actionContainer;
        protected override void OnViewModelLoaded()
        {
            SetActionBar(Resource.Layout.ActionBarCenterTitle, Resource.String.on_boarding_title);
            SetContentView(Resource.Layout.OnBoarding);
            _nextButton = FindViewById<Button>(Resource.Id.nextButton);
            _actionContainer = FindViewById<View>(Resource.Id.actionContainer);
            ViewPager viewPager = FindViewById<ViewPager>(Resource.Id.helpOverlayViewPager);
            viewPager.Adapter = new HelpOverlayPageAdapter(ViewModel.Pages);
            (FindViewById<TabLayout>(Resource.Id.helpOverlayIndicator)).SetupWithViewPager(viewPager, true);
            viewPager.AddOnPageChangeListener(new OnHelpOverlayPageChangeListener(OnPageChanged));
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(_nextButton, ActionType.Next);
        }
        private void OnPageChanged(int index)
        {
            _actionContainer.Visibility = index == ViewModel.Pages.Length - 1 ? ViewStates.Visible : ViewStates.Invisible;
        }
    }
}