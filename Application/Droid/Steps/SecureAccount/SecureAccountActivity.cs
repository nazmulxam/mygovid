﻿using System.Linq;
using Android.App;
using Android.Text;
using Android.Widget;
using myGovID.Droid.Constants;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Presentation.Contract.Steps.SecureYourAccount;

namespace myGovID.Droid.Steps.SecureAccount
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeBase,
              WindowSoftInputMode = Android.Views.SoftInput.StateHidden,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class SecureAccountActivity : StepActivity<ISecureYourAccountStep, SecureYourAccountViewModel>
    {
        private Android.Widget.Button _nextButton;
        private TextView _switchTitle;
        private Switch _switchField;
        protected override void OnViewModelLoaded()
        {
            SetActionBar(Resource.Layout.ActionBarCenterTitle, Resource.String.create_an_account);
            SetContentView(Resource.Layout.SecureAccount);
            _switchTitle = FindViewById<TextView>(Resource.Id.switchTitle);
            _switchField = FindViewById<Switch>(Resource.Id.biometricSwitch);
            TextView descriptionTV = FindViewById<TextView>(Resource.Id.description);
            descriptionTV.TextFormatted = Html.FromHtml(ViewModel.Description, FromHtmlOptions.ModeCompact);
            _nextButton = FindViewById<Android.Widget.Button>(Resource.Id.nextButton);
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(_nextButton, ActionType.Next);
            Bind((BooleanInput)ViewModel.Inputs.First(), _switchField, null, _switchTitle);
        }
    }
}
