﻿using System.Linq;
using Android.App;
using Android.Content.PM;
using Android.Support.V4.Hardware.Fingerprint;
using Android.Views;
using Android.Widget;
using myGovID.Droid.Constants;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Authenticate;
using myGovID.Services.LocalAuthentication.Droid;

namespace myGovID.Droid.Steps.Authenticate
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeSplash,
              WindowSoftInputMode = Android.Views.SoftInput.StateHidden,
              ScreenOrientation = ScreenOrientation.Portrait)]
    public class AuthenticateActivity : StepActivity<IAuthenticateStep, AuthenticateViewModel, AuthenticateAction>
    {
        private View _passwordContainer;
        private Button _loginButton;
        private EditText _passwordTextField;
        private TextView _passwordErrorLabel;
        private TextView _forgotPassword;
        private BiometricManager _biometricManager;
        protected override void OnViewModelLoaded()
        {
            SetContentView(Resource.Layout.Authenticate);
            _passwordContainer = FindViewById<View>(Resource.Id.passwordContainer);
            _loginButton = FindViewById<Button>(Resource.Id.loginButton);
            _passwordTextField = FindViewById<EditText>(Resource.Id.passwordET);
            _passwordErrorLabel = FindViewById<TextView>(Resource.Id.passwordErrorLabel);
            _forgotPassword = FindViewById<TextView>(Resource.Id.forgotPassword);
            if (!HasPageError() && IsBiometricAvailable() && Step.ChildStep == null && ViewModel.UseBiometrics)
            {
                _passwordContainer.Visibility = ViewStates.Gone;
                _biometricManager = new BiometricManager.BiometricBuilder(this)
                    .SetTitle("myGovID")
                    .SetSubtitle("Login using your fingerprint")
                    .SetDescription("")
                    .SetNegativeButtonText("Cancel")
                    .Build();
                _biometricManager.Authenticate(new AuthenticationCallback(OnAuthentication));
            }
            else
            {
                _passwordContainer.Visibility = ViewStates.Visible;
            }
        }
        protected override void UpdatePageError()
        {
            base.UpdatePageError();
            if (PageErrorView.Visibility == ViewStates.Visible && _passwordContainer.Visibility == ViewStates.Gone)
            {
                _passwordContainer.Visibility = ViewStates.Visible;
            }
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind((FormInput<string>)ViewModel.Inputs.First(), _passwordTextField, _passwordErrorLabel);
            Bind(_loginButton, ActionType.Next, AuthenticateAction.Password);
            Bind(_forgotPassword, ActionType.Next, AuthenticateAction.ForgottenPassword);
        }
        private bool IsBiometricAvailable()
        {
            FingerprintManagerCompat fingerprintManager = FingerprintManagerCompat.From(this);
            return fingerprintManager.IsHardwareDetected && fingerprintManager.HasEnrolledFingerprints;
        }
        private void OnAuthentication(AuthenticationResult result)
        {
            if (result == AuthenticationResult.Success)
            {
                _passwordContainer.Visibility = ViewStates.Gone;
                ExecuteAction(Step, ActionType.Next, AuthenticateAction.Biometric);
            }
            else
            {
                _passwordContainer.Visibility = ViewStates.Visible;
            }
        }
    }
}