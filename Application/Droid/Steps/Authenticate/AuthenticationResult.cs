﻿namespace myGovID.Droid.Steps.Authenticate
{
    public enum AuthenticationResult
    {
        Failed, Success, Error, Cancelled
    }
}
