﻿using System;
using Android.OS;
using myGovID.Services.LocalAuthentication.Droid;

namespace myGovID.Droid.Steps.Authenticate
{
    public class AuthenticationCallback: IBiometricCallback    {
        private Action<AuthenticationResult> _callback;
        public AuthenticationCallback(Action<AuthenticationResult> action)
        {
            _callback = action;
        }
        public void OnSdkVersionNotSupported()
        {
            _callback.Invoke(AuthenticationResult.Cancelled);
        }
        public void OnBiometricAuthenticationNotSupported()
        {
            _callback.Invoke(AuthenticationResult.Cancelled);
        }
        public void OnBiometricAuthenticationNotAvailable()
        {
            _callback.Invoke(AuthenticationResult.Cancelled);
        }
        public void OnBiometricAuthenticationPermissionNotGranted()
        {
            _callback.Invoke(AuthenticationResult.Cancelled);
        }
        public void OnBiometricAuthenticationInternalError(string error)
        {
            _callback.Invoke(AuthenticationResult.Error);
        }
        public void OnAuthenticationCancelled(CancellationSignal cancellationSignal)
        {
            _callback.Invoke(AuthenticationResult.Cancelled);
            cancellationSignal.Cancel();
        }
        public void OnAuthenticationSuccessful()
        {
            _callback.Invoke(AuthenticationResult.Success);
        }
        public void OnAuthenticationHelp(int helpCode, string helpString)
        {
            // Do nothing.
        }
        public void OnAuthenticationError(int errorCode, string errString)
        {
            _callback.Invoke(AuthenticationResult.Error);
        }
        public void OnAuthenticationFailed()
        {
            _callback.Invoke(AuthenticationResult.Failed);
        }
    }
}