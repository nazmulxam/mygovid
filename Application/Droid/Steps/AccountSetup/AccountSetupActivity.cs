﻿using Android.App;
using Android.Views;
using Android.Widget;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.AccountSetup;
using myGovID.Droid.Constants;

namespace myGovID.Droid.Steps.AccountSetup
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeSplash,
              WindowSoftInputMode = SoftInput.StateHidden,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class AccountSetupActivity : StepActivity<IAccountSetupStep, AccountSetupViewModel, AccountSetupAction>
    {
        private Button _registerButton;
        private Button _recoverButton;
        private TextView _needHelp;
        protected override void OnViewModelLoaded()
        {
            SetContentView(Resource.Layout.AccountSetup);
            _registerButton = FindViewById<Button>(Resource.Id.registerButton);
            _recoverButton = FindViewById<Button>(Resource.Id.recoverButton);
            _needHelp = FindViewById<TextView>(Resource.Id.needHelp);
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(_registerButton, ActionType.Next, AccountSetupAction.Register);
            Bind(_recoverButton, ActionType.Next, AccountSetupAction.Recover);
            Bind(_needHelp, ActionType.Browse, AccountSetupAction.Help);
        }
    }
}