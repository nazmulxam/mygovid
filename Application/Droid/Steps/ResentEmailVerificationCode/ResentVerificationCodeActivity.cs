﻿using Android.App;
using Android.Widget;
using myGovID.Droid.Constants;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.ResentEmailVerificationCode;

namespace myGovID.Droid.Steps.ResentEmailVerificationCode
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeTransparent,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class ResentEmailVerificationCodeActivity : StepActivity<IResentEmailVerificationCodeStep, ResentEmailVerificationCodeViewModel>
    {
        private Button _nextButton;
        protected override void OnViewModelLoaded()
        {
            SetContentView(Resource.Layout.ResentEmailVerificationCode);
            _nextButton = FindViewById<Button>(Resource.Id.nextButton);
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(_nextButton, ActionType.Next, null, "OnNextOverlay");
        }
    }
}