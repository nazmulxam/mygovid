﻿using Android.App;
using Android.Widget;
using myGovID.Droid.Constants;
using myGovID.Presentation.Contract.Steps.DeadEnd;

namespace myGovID.Droid.Steps.DeadEnd
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeNoActionBar,
              WindowSoftInputMode = Android.Views.SoftInput.StateHidden,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class DeadEndActivity : StepActivity<IDeadEndStep, DeadEndViewModel>
    {
        protected override void OnViewModelLoaded()
        {
            SetContentView(Resource.Layout.DeadEnd);
            var errorCode = FindViewById<TextView>(Resource.Id.errorCode);
            errorCode.Text = "Error code: " + ViewModel.Error.Code;
            errorCode.ContentDescription = ViewModel.ErrorCodeAccessibilityIdentifier;
            var errorTitle = FindViewById<TextView>(Resource.Id.errorTitle);
            errorTitle.Text = string.IsNullOrEmpty(ViewModel.Error.Title) ? Resources.GetString(Resource.String.error_title) : ViewModel.Error.Title;
            errorTitle.ContentDescription = ViewModel.ErrorTitleAccessibilityIdentifier;

            //FindViewById<TextView>(Resource.Id.errorDescription).TextFormatted = Html.FromHtml(ViewModel.Error.Message,
            //FromHtmlOptions.ModeCompact);
            var errorDescription = FindViewById<TextView>(Resource.Id.errorDescription);
            errorDescription.Text = Resources.GetString(Resource.String.error_description);
            errorDescription.ContentDescription = ViewModel.ErrorDescriptionAccessibilityIdentifier;
        }
    }
}
