﻿using Android.App;
using Android.Text;
using Android.Text.Method;
using Android.Text.Style;
using Android.Widget;
using myGovID.Droid.Constants;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.TermsAndConditions;

namespace myGovID.Droid.Steps.TermsAndConditions
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeDark,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class TermsAndConditionsActivity : StepActivity<ITermsAndConditionsStep, TermsAndConditionsViewModel>
    {
        private Button _nextButton;
        private Button _cancelButton;
        private TextView _descriptionText;
        protected override void OnViewModelLoaded()
        {
            SetActionBar(Resource.Layout.ActionBarLeftTitle, Resource.String.empty);
            SetContentView(Resource.Layout.TermsAndConditions);
            FindViewById<TextView>(Resource.Id.actionbarTitle).SetTextColor(Android.Graphics.Color.White);
            _nextButton = FindViewById<Button>(Resource.Id.nextButton);
            _cancelButton = FindViewById<Button>(Resource.Id.cancelButton);
            _descriptionText = FindViewById<TextView>(Resource.Id.content);
            _descriptionText.SetTextColor(Android.Graphics.Color.White);
            SpannableStringBuilder descriptionSpannable = (SpannableStringBuilder)Html.FromHtml(ViewModel.TermsAndConditions, FromHtmlOptions.ModeCompact);
            _descriptionText.MovementMethod = new LinkMovementMethod();
            _descriptionText.SetLinkTextColor(Android.Graphics.Color.White);
            foreach (URLSpan urlSpan in descriptionSpannable.GetSpans(0, descriptionSpannable.Length(), Java.Lang.Class.FromType(typeof(URLSpan))))
            {
                descriptionSpannable.SetSpan(new NoUnderlineSpan(), descriptionSpannable.GetSpanStart(urlSpan), descriptionSpannable.GetSpanEnd(urlSpan), 0);
            }
            _descriptionText.TextFormatted = descriptionSpannable;
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(_nextButton, ActionType.Next);
            Bind(_cancelButton, ActionType.Cancel);
        }
    }
    class NoUnderlineSpan : UnderlineSpan
    {
        public override void UpdateDrawState(TextPaint ds)
        {
            ds.UnderlineText = false;
        }
    }
}