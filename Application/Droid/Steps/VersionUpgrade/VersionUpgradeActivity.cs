﻿using Android.App;
using Android.Views;
using Android.Widget;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.VersionUpgrade;
using myGovID.Droid.Constants;

namespace myGovID.Droid.Steps.VersionUpgrade
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeTransparent,
              WindowSoftInputMode = SoftInput.StateHidden,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class VersionUpgradeActivity : StepActivity<IVersionUpgradeStep, VersionUpgradeViewModel>
    {
        private Button _nextButton;
        private Button _cancelButton;
        private TextView _upgradeTitle;
        private TextView _upgradeDescription;
        protected override void OnViewModelLoaded()
        {
            SetContentView(Resource.Layout.VersionUpgrade);
            _nextButton = FindViewById<Button>(Resource.Id.nextButton);
            _cancelButton = FindViewById<Button>(Resource.Id.cancelButton);
            _upgradeTitle = FindViewById<TextView>(Resource.Id.title);
            _upgradeDescription = FindViewById<TextView>(Resource.Id.description);
            if (ViewModel.ForcedUpdate)
            {
                _upgradeTitle.Text = Resources.GetString(Resource.String.version_update_required_alert_title);
                _upgradeDescription.Text = Resources.GetString(Resource.String.version_update_required_alert_message);
                _cancelButton.Visibility = ViewStates.Gone;
            }
            else
            {
                _upgradeTitle.Text = Resources.GetString(Resource.String.version_update_alert_title);
                _upgradeDescription.Text = Resources.GetString(Resource.String.version_update_alert_message);
            }
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(_nextButton, ActionType.Next, null, "OnNextOverlay");
            Bind(_cancelButton, ActionType.Previous, null, "OnPreviousOverlay");
        }
    }
}
