﻿using Android.App;
using Android.Views;
using Android.Widget;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.AuthRequest;
using myGovID.Droid.Constants;
using myGovID.Droid.Views;

namespace myGovID.Droid.Steps.AuthRequest
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeDark,
              WindowSoftInputMode = SoftInput.StateHidden,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class AuthRequestActivity : StepActivity<IAuthRequestStep, AuthRequestViewModel>
    {
        private Button _acceptButton;
        private Button _rejectButton;
        private TextView _title;
        private TextView _description;
        private TextView _errorLabel;
        private VerificationCode _verificationCode;

        protected override void OnViewModelLoaded()
        {
            SetContentView(Resource.Layout.AuthRequest);

            _title = FindViewById<TextView>(Resource.Id.title);
            _errorLabel = FindViewById<TextView>(Resource.Id.errorLabel);
            _acceptButton = FindViewById<Button>(Resource.Id.acceptButton);
            _rejectButton = FindViewById<Button>(Resource.Id.rejectButton);
            _description = FindViewById<TextView>(Resource.Id.description);
            _verificationCode = FindViewById<VerificationCode>(Resource.Id.verificationCode);

            _title.Text = ViewModel.AlertTitle;
            _description.Text = ViewModel.AlertMessage;
            _acceptButton.Text = ViewModel.AcceptButtonTitle;
            _rejectButton.Text = ViewModel.RejectButtonTitle;
        }

        protected override void AttachBindings()
        {
            base.AttachBindings();

            Bind(ViewModel.CodeEntry, _verificationCode, _errorLabel);
            Bind(_acceptButton, ActionType.Next, null, "OnNextOverlay");
            Bind(_rejectButton, ActionType.Previous, null, "OnPreviousOverlay");
        }
    }
}
