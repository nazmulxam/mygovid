﻿using Android.App;
using Android.Text;
using Android.Widget;
using myGovID.Droid.Constants;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.RecoverableDeadEnd;

namespace myGovID.Droid.Steps.RecoverableDeadEnd
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeTransparent,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class RecoverableDeadEndActivity : StepActivity<IRecoverableDeadEndStep, RecoverableDeadEndViewModel>
    {
        private Button _nextButton;
        protected override void OnViewModelLoaded()
        {
            SetContentView(Resource.Layout.RecoverableDeadEnd);
            FindViewById<TextView>(Resource.Id.errorCode).Text = "Error code: " + ViewModel.Exception.Code;
            FindViewById<TextView>(Resource.Id.errorTitle).Text = string.IsNullOrEmpty(ViewModel.Exception.Title) ? Resources.GetString(Resource.String.error_title) : ViewModel.Exception.Title;
            FindViewById<TextView>(Resource.Id.errorDescription).TextFormatted = Html.FromHtml(ViewModel.Exception.Description,
                                                                                               FromHtmlOptions.ModeCompact);
            _nextButton = FindViewById<Button>(Resource.Id.nextButton);
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(_nextButton, ActionType.Next, null, "OnNextOverlay");
        }
    }
}