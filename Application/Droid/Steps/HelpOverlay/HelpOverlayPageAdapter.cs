﻿using System;
using Android.Support.V4.View;
using Android.Views;
using Android.Widget;
using myGovID.Presentation.Contract.Steps.HelpOverlay;

namespace myGovID.Droid.Steps.HelpOverlay
{
    public class HelpOverlayPageAdapter : PagerAdapter
    {
        private readonly IHelpOverlayModel[] _helpOverlays;
        public HelpOverlayPageAdapter(IHelpOverlayModel[] helpOverlays)
        {
            _helpOverlays = helpOverlays;
        }
        public override int Count => _helpOverlays.Length;
        public override bool IsViewFromObject(View view, Java.Lang.Object @object) => view == @object;
        [Obsolete]
        public override Java.Lang.Object InstantiateItem(View container, int position)
        {
            LayoutInflater layoutInflater = LayoutInflater.FromContext(container.Context);
            View view = layoutInflater.Inflate(Resource.Layout.HelpOverlayContentView, null);
            IHelpOverlayModel model = _helpOverlays[position];
            int imageResourceId = container.Resources.GetIdentifier(model.Image.ToLower(), "drawable", container.Context.PackageName);
            view.FindViewById<ImageView>(Resource.Id.helpOverlayContentImage).SetImageResource(imageResourceId);
            view.FindViewById<TextView>(Resource.Id.helpOverlayContentTitle).Text = model.Title;
            view.FindViewById<TextView>(Resource.Id.helpOverlayContentDescription).Text = model.Description;
            ((ViewPager)container).AddView(view);
            return view;
        }
        [Obsolete]
        public override void DestroyItem(View container, int position, Java.Lang.Object @object) => ((ViewPager)container).RemoveView((View)@object);
    }
}
