﻿using System;
using Android.Support.V4.View;

namespace myGovID.Droid.Steps.HelpOverlay
{
    public class OnHelpOverlayPageChangeListener : Java.Lang.Object, ViewPager.IOnPageChangeListener
    {
        private readonly Action<int> _onPageChangedAction;
        public OnHelpOverlayPageChangeListener(Action<int> onPageChangedAction)
        {
            _onPageChangedAction = onPageChangedAction;
        }
        public void OnPageScrolled(int position, float positionOffset, int positionOffsetPixels)
        {}
        public void OnPageScrollStateChanged(int state)
        {}
        public void OnPageSelected(int position)
        {
            _onPageChangedAction.Invoke(position);
        }
    }
}
