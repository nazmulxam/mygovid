﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Text;
using Android.Text.Method;
using GalaSoft.MvvmLight.Helpers;
using Android.Text.Style;
using Android.Views;
using Android.Widget;
using myGovID.Droid.Constants;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Dashboard;
using myGovID.Presentation.Contract.Steps.Dashboard.Document;

namespace myGovID.Droid.Steps.Dashboard
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeSplash,
              WindowSoftInputMode = SoftInput.StateHidden,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class DashboardActivity : StepActivity<IDashboardStep, DashboardViewModel, DashboardAction>
    {
        private ImageView _identityStrengthBar;
        private TextView _identityStrengthLabel;
        private TextView _nameLabel;
        private TextView _nextSteps;
        private LinearLayout _documentContainer;
        private View _about, _account, _settings;
        protected override void OnViewModelLoaded()
        {
            SetContentView(Resource.Layout.Dashboard);
            _identityStrengthBar = FindViewById<ImageView>(Resource.Id.identityStrength);
            _identityStrengthLabel = FindViewById<TextView>(Resource.Id.identityStrengthLabel);
            _nameLabel = FindViewById<TextView>(Resource.Id.nameLabel);
            _nextSteps = FindViewById<TextView>(Resource.Id.nextStepsLabel);
            _documentContainer = FindViewById<LinearLayout>(Resource.Id.documentContainer);
            _account = FindViewById(Resource.Id.account);
            _about = FindViewById(Resource.Id.about);
            _settings = FindViewById(Resource.Id.settings);
            UpdateIdentityStrength(ViewModel.AuthStrengthType);
            _identityStrengthLabel.Text = ViewModel.AuthStrengthType.ToString();
            LoadDocuments(ViewModel.Documents);
            SetNextSteps(ViewModel.NextSteps);
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(nameof(ViewModel.WelcomeMessage), _nameLabel, "Name");
            BindAuthentationStrength(() =>
            {
                RunOnUiThread(() =>
                {
                    AuthStrengthType authStrengthType = ViewModel.AuthStrengthType;
                    _identityStrengthLabel.Text = authStrengthType.ToString();
                    UpdateIdentityStrength(authStrengthType);
                    SetNextSteps(ViewModel.NextSteps);
                });
            });
            Bind(_account, ActionType.Next, DashboardAction.Account);
            Bind(_about, ActionType.Next, DashboardAction.About);
            Bind(_settings, ActionType.Next, DashboardAction.Settings);
            for (int index = 0, length = _documentContainer.ChildCount; index < length; index++)
            {
                View view = _documentContainer.GetChildAt(index);
                Enum.TryParse(view.Tag.ToString(), out DashboardAction action);
                Bind(view, ActionType.Next, action);
            }
        }
        private void UpdateIdentityStrength(AuthStrengthType strength)
        {
            string imageName = "progressbarunknown";
            switch (strength)
            {
                case AuthStrengthType.Weak:
                    imageName = "progressbarweak";
                    break;
                case AuthStrengthType.Intermediate:
                    imageName = "progressbarintermediate";
                    break;
                case AuthStrengthType.Strong:
                    imageName = "progressbarstrong";
                    break;
                default:
                    imageName = "progressbarunknown";
                    break;
            }
            int imageResourceId = Resources.GetIdentifier(imageName.ToLower(), "drawable", PackageName);
            _identityStrengthBar.SetImageResource(imageResourceId);
        }
        private void LoadDocuments(IEnumerable<IDocument> documents)
        {
            _documentContainer.RemoveAllViews();
            foreach (IDocument document in documents)
            {
                DashboardDocumentView view = new DashboardDocumentView(this);
                view.Update(document);
                _documentContainer.AddView(view);
            }
        }
        private void OpenTutorialLink()
        {
            ExecuteAction(Step, ActionType.Next, DashboardAction.Tutorial);
        }
        private void SetNextSteps(string nextSteps)
        {
            SpannableStringBuilder strBuilder = (SpannableStringBuilder)Html.FromHtml(nextSteps, FromHtmlOptions.ModeCompact);
            foreach (URLSpan span in strBuilder.GetSpans(0, strBuilder.Length(), Java.Lang.Class.FromType(typeof(URLSpan))))
            {
                MakeLinkClickable(strBuilder, span);
            }
            _nextSteps.TextFormatted = strBuilder;
            _nextSteps.MovementMethod = new LinkMovementMethod();

        }
        private void MakeLinkClickable(SpannableStringBuilder strBuilder, URLSpan span)
        {
            int start = strBuilder.GetSpanStart(span);
            int end = strBuilder.GetSpanEnd(span);
            SpanTypes flags = strBuilder.GetSpanFlags(span);
            strBuilder.SetSpan(new ActionClickableSpan(OpenTutorialLink), start, end, flags);
            strBuilder.RemoveSpan(span);
        }
        private sealed class ActionClickableSpan : ClickableSpan
        {
            private readonly Action _action;
            public ActionClickableSpan(Action action)
            {
                this._action = action;
            }
            public override void OnClick(View widget)
            {
                _action?.Invoke();
            }
        }
        private void BindAuthentationStrength(Action action)
        {
            Binding authBinding = new Binding<AuthStrengthType, AuthStrengthType>(ViewModel, nameof(ViewModel.AuthStrengthType)).WhenSourceChanges(action);
            AddBinding(authBinding);
        }
    }
}