﻿using System;
using Android.Content;
using Android.Util;
using Android.Widget;
using Android.Views;
using myGovID.Presentation.Contract.Steps.Dashboard.Document;

namespace myGovID.Droid.Steps.Dashboard
{
    public class DashboardDocumentView : FrameLayout
    {
        private TextView _title;
        private TextView _description;
        private ImageView _icon;
        public DashboardDocumentView(Context context) : base(context)
        {
            CommonInit(context);
        }
        public DashboardDocumentView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            CommonInit(context);
        }
        public DashboardDocumentView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
            CommonInit(context);
        }
        public DashboardDocumentView(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
            CommonInit(context);
        }
        private void CommonInit(Context context)
        {
            LayoutInflater.FromContext(context).Inflate(Resource.Layout.DashboardDocument, this, true);
            _title = FindViewById<TextView>(Resource.Id.title);
            _description = FindViewById<TextView>(Resource.Id.description);
            _icon = FindViewById<ImageView>(Resource.Id.icon);
        }
        public void Update(IDocument document)
        {
            _title.Text = document.Title;
            _description.Text = document.Description;
            Tag = document.Action.ToString();
            int imageResourceId = Resources.GetIdentifier(document.Image.ToLower(), "drawable", Context.PackageName);
            _icon.SetImageResource(imageResourceId);
        }
    }
}
