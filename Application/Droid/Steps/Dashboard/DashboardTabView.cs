﻿using System;
using Android.Content;
using Android.Util;
using Android.Widget;
using Android.Views;
using Android.Content.Res;

namespace myGovID.Droid.Steps.Dashboard
{
    public class DashboardTabView : LinearLayout
    {
        public DashboardTabView(Context context) : base(context)
        {
            CommonInit(context);
        }
        public DashboardTabView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            CommonInit(context, attrs);
        }
        public DashboardTabView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
            CommonInit(context, attrs);
        }
        public DashboardTabView(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
            CommonInit(context, attrs);
        }
        private void CommonInit(Context context, IAttributeSet attrs = null)
        {
            SetGravity(GravityFlags.Center);
            Orientation = Android.Widget.Orientation.Vertical;
            LayoutInflater inflater = LayoutInflater.FromContext(context);
            inflater.Inflate(Resource.Layout.DashboardTabItem, this, true);
            if (attrs != null)
            {
                TypedArray typedArray = context.Theme.ObtainStyledAttributes(attrs, Resource.Styleable.DashboardTabView, 0, 0);
                FindViewById<TextView>(Resource.Id.title).Text = typedArray.GetString(Resource.Styleable.DashboardTabView_text);
                FindViewById<ImageView>(Resource.Id.icon).SetImageDrawable(typedArray.GetDrawable(Resource.Styleable.DashboardTabView_icon));
            }
        }
    }
}