﻿using System;
using Android.Content;
using myGovID.Presentation.Contract;
using myGovID.Droid.Steps.ResentEmailVerificationCode;
using myGovID.Droid.Steps.OnBoarding;
using myGovID.Droid.Steps.TermsAndConditions;
using myGovID.Droid.Steps.TermsAndConditionsConfirm;
using myGovID.Droid.Steps.CaptureEmail;
using myGovID.Droid.Steps.VerifyEmail;
using myGovID.Droid.Steps.SecureAccount;
using myGovID.Droid.Steps.CapturePassword;
using myGovID.Droid.Steps.VersionUpgrade;
using myGovID.Droid.Steps.DeadEnd;
using myGovID.Presentation.Contract.Steps.DeadEnd;
using myGovID.Presentation.Contract.Steps.CapturePassword;
using myGovID.Presentation.Contract.Steps.SecureYourAccount;
using myGovID.Presentation.Contract.Steps.ResentEmailVerificationCode;
using myGovID.Presentation.Contract.Steps.CaptureEmailVerification;
using myGovID.Presentation.Contract.Steps.CaptureEmail;
using myGovID.Presentation.Contract.Steps.TermsAndConditionsConfirm;
using myGovID.Presentation.Contract.Steps.TermsAndConditions;
using myGovID.Presentation.Contract.Steps.OnBoarding;
using myGovID.Presentation.Contract.Steps.VersionUpgrade;
using myGovID.Presentation.Contract.Steps.Browse;
using myGovID.Contract.Steps.CapturePersonalDetails;
using myGovID.Droid.Steps.CapturePersonalDetails;
using myGovID.Presentation.Contract.Steps.AccountSetup;
using myGovID.Droid.Steps.AccountSetup;
using myGovID.Presentation.Contract.Steps.Logout;
using myGovID.Droid.Steps.Logout;
using myGovID.Presentation.Contract.Steps.Dashboard;
using myGovID.Droid.Steps.Dashboard;
using myGovID.Presentation.Contract.Steps.Authenticate;
using myGovID.Droid.Steps.Authenticate;
using myGovID.Presentation.Contract.Steps.Passport;
using myGovID.Droid.Steps.Passport;
using System.Collections.Generic;
using myGovID.Droid.Steps.RecoverableDeadEnd;
using myGovID.Presentation.Contract.Steps.RecoverableDeadEnd;
using myGovID.Presentation.Contract.Steps.DocumentIdentitySuccess;
using myGovID.Presentation.Contract.Steps.DriverLicence;
using myGovID.Droid.Steps.DriverLicence;
using myGovID.Presentation.Contract.Steps.MedicareOcrIntroStep;
using myGovID.Presentation.Contract.Steps.MedicareCardInput;
using myGovID.Droid.Steps.Medicare;
using System.Linq;
using myGovID.Presentation.Contract.Steps.Settings;
using myGovID.Presentation.Contract.Steps.AuthRequest;
using myGovID.Droid.Steps.AuthRequest;

namespace myGovID.Droid.Steps
{
    public class StepOrchestrator
    {
        private readonly IStepRepository _repository;
        private Dictionary<string, IStep> _stepDictionary = new Dictionary<string, IStep>();
        private IStep _rootStep;
        public StepOrchestrator(IStepRepository repository)
        {
            _repository = repository;
        }
        public virtual Intent GetActivity(Context context, IStep newStep)
        {
            if (!IsOverlay(newStep))
            {
                _rootStep = newStep;
            }
            string stepId = Guid.NewGuid().ToString();
            _stepDictionary.Remove(stepId);
            _stepDictionary.Add(stepId, newStep);
            return GetIntentForStep(context, newStep, stepId);
        }
        public Intent GetLastActivity(Context context)
        {
            if (_rootStep == null)
            {
                return null;
            }
            string stepId = _stepDictionary.FirstOrDefault(entry => entry.Value == _rootStep).Key;
            return stepId == null ? GetActivity(context, _rootStep) : GetIntentForStep(context, _rootStep, stepId);
        }
        private Intent GetIntentForStep(Context context, IStep step, string stepId)
        {
            bool isOverlay = IsOverlay(step);
            Intent intent = step is IBrowseStep ? CreateBrowserIntent(((BrowseViewModel)step.ViewModel).Url) : new Intent(context, GetTypeForStep(step));
            intent.PutExtra("StepId", stepId);
            intent.PutExtra("IsOverlay", isOverlay);
            intent.PutExtra("DarkNavigationTheme", ShouldApplyDarkNavigationTheme(step));
            intent.AddFlags(isOverlay ? ActivityFlags.NoHistory : (ActivityFlags.SingleTop | ActivityFlags.ClearTop));
            return intent;
        }
        private Type GetTypeForStep(IStep step)
        {
            switch (step)
            {
                case IVersionUpgradeStep newStep: return typeof(VersionUpgradeActivity);
                case IAccountSetupStep newStep: return typeof(AccountSetupActivity);
                case IOnBoardingStep newStep: return typeof(OnBoardingActivity);
                case ITermsAndConditionsStep newStep: return typeof(TermsAndConditionsActivity);
                case ITermsAndConditionsConfirmStep newStep: return typeof(TermsAndConditionsConfirmActivity);
                case ICaptureEmailStep newStep: return typeof(CaptureEmailActivity);
                case ICaptureEmailVerificationStep newStep: return typeof(VerifyEmailActivity);
                case IResentEmailVerificationCodeStep newStep: return typeof(ResentEmailVerificationCodeActivity);
                case ISecureYourAccountStep newStep: return typeof(SecureAccountActivity);
                case ICapturePasswordStep newStep: return typeof(CapturePasswordActivity);
                case ICapturePersonalDetailsStep newStep: return typeof(CapturePersonalDetailsActivity);
                case IAuthenticateStep newStep: return typeof(AuthenticateActivity);
                case IDeadEndStep newStep: return typeof(DeadEndActivity);
                case ILogoutStep newStep: return typeof(LogoutActivity);
                case IRecoverableDeadEndStep newStep: return typeof(RecoverableDeadEndActivity);
                case IDashboardStep newStep: return typeof(DashboardActivity);
                case IPassportOCRIntroStep newStep: return typeof(PassportOCRIntroActivity);
                case ICapturePassportDetailsStep newStep: return typeof(CapturePassportDetailsActivity);
                case IDriverLicenceOCRIntroStep newStep: return typeof(DriverLicenceIntroActivity);
                case ICaptureDriverLicenceDetailsStep newStep: return typeof(CaptureDriverLicenceDetailsActivity);
                case IMedicareOcrIntroStep newStep: return typeof(MedicareOCRIntroActivity);
                case IMedicareCardInputStep newStep: return typeof(CaptureMedicareCardDetailsActivity);
                case ISettingsStep newStep: return typeof(SettingsActivity);
                case IAuthRequestStep newStep: return typeof(AuthRequestActivity);
                default: throw new NotImplementedException(step.GetType().FullName);
            }
        }
        private bool IsOverlay(IStep step)
        {
            switch (step)
            {
                case IVersionUpgradeStep st: return true;
                case ITermsAndConditionsConfirmStep st: return true;
                case IResentEmailVerificationCodeStep st: return true;
                case IRecoverableDeadEndStep st: return true;
                default: return false;
            }
        }
        private bool ShouldApplyDarkNavigationTheme(IStep step)
        {
            switch (step)
            {
                case IAccountSetupStep st: return true;
                case ITermsAndConditionsStep st: return true;
                case IAuthenticateStep st: return true;
                case ILogoutStep st: return true;
                case IDashboardStep st: return true;
                default: return false;
            }
        }
        public IStep GetDeadEndStep(string code, string title, string message)
        {
            return _repository.GetDeadEndStep(new PresentationError(code, title ?? "Error", message));
        }
        public S GetStep<S>(string stepId)
        {
            IStep step = _stepDictionary.GetValueOrDefault(stepId);
            _stepDictionary.Remove(stepId);
            if (step == null && _rootStep is S)
            {
                return (S)_rootStep;
            }
            return (S)step;
        }
        private Intent CreateBrowserIntent(string url)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                throw new ArgumentNullException(nameof(url));
            }
            var intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse(url));
            intent.AddFlags(ActivityFlags.NewTask);
            return intent;
        }
        public bool IsSuccessStep(IStep step)
        {
            return step is IDocumentIdentitySuccessStep;
        }
    }
}