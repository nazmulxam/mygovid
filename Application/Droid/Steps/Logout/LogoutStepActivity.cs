﻿using Android.App;
using Android.Widget;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Logout;
using myGovID.Droid.Constants;

namespace myGovID.Droid.Steps.Logout
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeTransparent,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class LogoutActivity : StepActivity<ILogoutStep, LogoutViewModel>
    {
        private Button _nextButton;
        protected override void OnViewModelLoaded()
        {
            SetContentView(Resource.Layout.Logout);
            _nextButton = FindViewById<Button>(Resource.Id.nextButton);
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(_nextButton, ActionType.Next);
        }
    }
}