﻿using System;
using System.Linq;
using Android.App;
using Android.Widget;
using myGovID.Droid.Constants;
using myGovID.Droid.Views;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Presentation.Contract.Steps.Passport;

namespace myGovID.Droid.Steps.Passport
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeBase,
              WindowSoftInputMode = Android.Views.SoftInput.StateHidden,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class CapturePassportDetailsActivity : StepActivity<ICapturePassportDetailsStep, PassportViewModel, CapturePassportDetailsAction>
    {
        private EditText _documentNumber;
        private EditText _givenName;
        private EditText _familyName;
        private EditText _dateOfBirth;
        private SegmentedControl _gender;
        private CheckBox _consent;

        private TextView _documentNumberTitle;
        private TextView _givenNameTitle;
        private TextView _familyNameTitle;
        private TextView _dateOfBirthTitle;
        private TextView _genderTitle;
        private TextView _consentTitle;

        private TextView _documentNumberError;
        private TextView _givenNameError;
        private TextView _familyNameError;
        private TextView _dateOfBirthError;
        private TextView _genderError;
        private TextView _consentError;

        private Button _nextButton;
        private ImageButton _backButton;
        private Button _helpButton;
        protected override void OnViewModelLoaded()
        {
            SetActionBar(Resource.Layout.ActionBarCenterTitleWithBackButtonAndHelp, Resource.String.buildYourIdentity);
            SetContentView(Resource.Layout.CapturePassportManually);
            _backButton = FindViewById<ImageButton>(Resource.Id.actionbarBack);
            _helpButton = FindViewById<Button>(Resource.Id.actionbarHelp);
            _nextButton = FindViewById<Button>(Resource.Id.nextButton);

            _documentNumber = FindViewById<EditText>(Resource.Id.documentNumberET);
            _givenName = FindViewById<EditText>(Resource.Id.givenNameET);
            _familyName = FindViewById<EditText>(Resource.Id.familyNameET);
            _dateOfBirth = FindViewById<EditText>(Resource.Id.dateOfBirthET);
            _gender = FindViewById<SegmentedControl>(Resource.Id.genderSC);
            _consent = FindViewById<CheckBox>(Resource.Id.consent);

            _documentNumberTitle = FindViewById<TextView>(Resource.Id.documentNumberTitle);
            _givenNameTitle = FindViewById<TextView>(Resource.Id.givenNameTitle);
            _familyNameTitle = FindViewById<TextView>(Resource.Id.familyNameTitle);
            _dateOfBirthTitle = FindViewById<TextView>(Resource.Id.dateOfBirthTitle);
            _genderTitle = FindViewById<TextView>(Resource.Id.genderTitle);
            _consentTitle = FindViewById<TextView>(Resource.Id.consentTitle);

            _documentNumberError = FindViewById<TextView>(Resource.Id.documentNumberError);
            _givenNameError = FindViewById<TextView>(Resource.Id.givenNameError);
            _familyNameError = FindViewById<TextView>(Resource.Id.familyNameError);
            _dateOfBirthError = FindViewById<TextView>(Resource.Id.dateOfBirthError);
            _genderError = FindViewById<TextView>(Resource.Id.genderError);
            _consentError = FindViewById<TextView>(Resource.Id.consentError);

            _dateOfBirth.Click += (sender, args) =>
            {
                ShowDatePicker((FormInput<string>)ViewModel.Inputs.First((input) => input.MetaData == InputMetaData.DateOfBirth));
            };
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(GetInput<string>(InputMetaData.PassportDocumentNumber), _documentNumber, _documentNumberError, _documentNumberTitle);
            Bind(GetInput<string>(InputMetaData.GivenName), _givenName, _givenNameError, _givenNameTitle);
            Bind(GetInput<string>(InputMetaData.FamilyName), _familyName, _familyNameError, _familyNameTitle);
            Bind(GetInput<string>(InputMetaData.DateOfBirth), _dateOfBirth, _dateOfBirthError, _dateOfBirthTitle);
            Bind((MultipleChoiceInput<string>)GetInput<string>(InputMetaData.Gender), _gender, _genderError, _genderTitle);
            Bind((BooleanInput)GetInput<bool>(InputMetaData.Consent), _consent, _consentError, _consentTitle);
            Bind(_nextButton, ActionType.Next, CapturePassportDetailsAction.Submit);
            Bind(_backButton, ActionType.Previous, CapturePassportDetailsAction.Back);
            Bind(_helpButton, ActionType.Browse, CapturePassportDetailsAction.Help);
        }
        private FormInput<T> GetInput<T>(InputMetaData metaData) where T : IComparable
        {
            return (FormInput<T>)ViewModel.Inputs.First((input) => input.MetaData == metaData);
        }
    }
}
