﻿using Android.App;
using Android.Widget;
using myGovID.Droid.Constants;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Passport;

namespace myGovID.Droid.Steps.Passport
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeBase,
              WindowSoftInputMode = Android.Views.SoftInput.StateHidden,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class PassportOCRIntroActivity : StepActivity<IPassportOCRIntroStep, PassportOCRIntroViewModel, PassportOCRIntroAction>
    {
        private ImageButton _backButton;
        private Button _ocrCaptureButton;
        private Button _enterManuallyButton;
        protected override void OnViewModelLoaded()
        {
            SetActionBar(Resource.Layout.ActionBarCenterTitleWithBackButton, Resource.String.buildYourIdentity);
            SetContentView(Resource.Layout.PassportIntro);
            _backButton = FindViewById<ImageButton>(Resource.Id.actionbarBack);
            _ocrCaptureButton = FindViewById<Button>(Resource.Id.ocrCaptureButton);
            _enterManuallyButton = FindViewById<Button>(Resource.Id.enterManuallyButton);
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(_ocrCaptureButton, ActionType.Next, PassportOCRIntroAction.OCRCapture);
            Bind(_enterManuallyButton, ActionType.Next, PassportOCRIntroAction.ManualEntry);
            Bind(_backButton, ActionType.Previous);
        }
    }
}