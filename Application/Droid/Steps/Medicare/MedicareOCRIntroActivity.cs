﻿
using Android.App;
using Android.Widget;
using myGovID.Droid.Constants;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.MedicareOcrIntroStep;

namespace myGovID.Droid.Steps.Medicare
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeBase,
              WindowSoftInputMode = Android.Views.SoftInput.StateHidden,
              NoHistory = true,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MedicareOCRIntroActivity : StepActivity<IMedicareOcrIntroStep, MedicareOcrIntroViewModel, MedicareOcrIntroAction>
    {
        private ImageButton _backButton;
        private Button _ocrCaptureButton;
        private Button _enterManuallyButton;
        private TextView _title;
        private TextView _description;
        protected override void OnViewModelLoaded()
        {
            SetActionBar(Resource.Layout.ActionBarCenterTitleWithBackButton, Resource.String.buildYourIdentity);
            SetContentView(Resource.Layout.MedicareOcrIntro);
            _title = FindViewById<TextView>(Resource.Id.title);
            _description = FindViewById<TextView>(Resource.Id.description);
            _backButton = FindViewById<ImageButton>(Resource.Id.actionbarBack);
            _ocrCaptureButton = FindViewById<Button>(Resource.Id.ocrCaptureButton);
            _enterManuallyButton = FindViewById<Button>(Resource.Id.enterManuallyButton);
            _title.Text = ViewModel.Title;
            _description.Text = ViewModel.Body;
            _ocrCaptureButton.Text = ViewModel.ScanButtonLabel;
            _enterManuallyButton.Text = ViewModel.ManualButtonLabel;
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(_ocrCaptureButton, ActionType.Next, MedicareOcrIntroAction.ScanDetails);
            Bind(_enterManuallyButton, ActionType.Next, MedicareOcrIntroAction.EnterManually);
            Bind(_backButton, ActionType.Previous);
        }
    }
}
