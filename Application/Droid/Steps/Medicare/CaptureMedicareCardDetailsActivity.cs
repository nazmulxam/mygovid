﻿
using System;
using System.Linq;
using Android.App;
using Android.Widget;
using myGovID.Droid.Constants;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Presentation.Contract.Steps.MedicareCardInput;
using Android.Views;
using GalaSoft.MvvmLight.Helpers;

namespace myGovID.Droid.Steps.Medicare
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeBase,
                  WindowSoftInputMode = Android.Views.SoftInput.StateHidden,
                  NoHistory = true,
                  ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class CaptureMedicareCardDetailsActivity : StepActivity<IMedicareCardInputStep, MedicareCardInputViewModel, MedicareCardInputAction>
    {
        private TextView _title;
        private TextView _description;
        private EditText _cardNumber;
        private EditText _name1;
        private EditText _name2;
        private EditText _name3;
        private EditText _name4;
        private Spinner _irn;
        private Spinner _colour;
        private EditText _expiryDate;
        private CheckBox _consent;
        private Button _add;
        private Button _remove;

        private TextView _cardNumberTitle;
        private TextView _name1Title;
        private TextView _irnTitle;
        private TextView _colourTitle;
        private TextView _expiryDateTitle;
        private TextView _consentTitle;

        private TextView _cardNumberError;
        private TextView _name1Error;
        private TextView _name2Error;
        private TextView _name3Error;
        private TextView _name4Error;
        private TextView _irnError;
        private TextView _colourError;
        private TextView _expiryDateError;
        private TextView _consentError;

        private Button _nextButton;
        private ImageButton _backButton;
        private Button _helpButton;
        protected override void OnViewModelLoaded()
        {
            SetActionBar(Resource.Layout.ActionBarCenterTitleWithBackButtonAndHelp, Resource.String.buildYourIdentity);
            SetContentView(Resource.Layout.CaptureMedicareManually);
            _backButton = FindViewById<ImageButton>(Resource.Id.actionbarBack);
            _helpButton = FindViewById<Button>(Resource.Id.actionbarHelp);
            _nextButton = FindViewById<Button>(Resource.Id.nextButton);

            _title = FindViewById<TextView>(Resource.Id.title);
            _description = FindViewById<TextView>(Resource.Id.description);
            _title.Text = ViewModel.Title;
            _description.Text = ViewModel.Body;
            _nextButton.Text = ViewModel.NextButtonLabel;

            _cardNumber = FindViewById<EditText>(Resource.Id.cardNumberET);
            _name1 = FindViewById<EditText>(Resource.Id.name1ET);
            _name2 = FindViewById<EditText>(Resource.Id.name2ET);
            _name3 = FindViewById<EditText>(Resource.Id.name3ET);
            _name4 = FindViewById<EditText>(Resource.Id.name4ET);
            _irn = FindViewById<Spinner>(Resource.Id.irnSpinner);
            _colour = FindViewById<Spinner>(Resource.Id.cardColourSpinner);
            _expiryDate = FindViewById<EditText>(Resource.Id.expiryDateET);
            _consent = FindViewById<CheckBox>(Resource.Id.consent);
            _add = FindViewById<Button>(Resource.Id.add);
            _remove = FindViewById<Button>(Resource.Id.remove);

            _cardNumberTitle = FindViewById<TextView>(Resource.Id.cardNumberTitle);
            _name1Title = FindViewById<TextView>(Resource.Id.nameTitle);
            _irnTitle = FindViewById<TextView>(Resource.Id.irnTitle);
            _colourTitle = FindViewById<TextView>(Resource.Id.cardColourTitle);
            _expiryDateTitle = FindViewById<TextView>(Resource.Id.expiryDateTitle);
            _consentTitle = FindViewById<TextView>(Resource.Id.consentTitle);

            _cardNumberError = FindViewById<TextView>(Resource.Id.cardNumberError);
            _name1Error = FindViewById<TextView>(Resource.Id.name1Error);
            _name2Error = FindViewById<TextView>(Resource.Id.name2Error);
            _name3Error = FindViewById<TextView>(Resource.Id.name3Error);
            _name4Error = FindViewById<TextView>(Resource.Id.name4Error);
            _irnError = FindViewById<TextView>(Resource.Id.irnError);
            _expiryDateError = FindViewById<TextView>(Resource.Id.expiryDateError);
            _colourError = FindViewById<TextView>(Resource.Id.cardColourError);
            _consentError = FindViewById<TextView>(Resource.Id.consentError);

            _expiryDate.Click += (sender, args) =>
            {
                MedicareCardExpiryDateInput dateInput = ViewModel.Expiry;
                ShowDatePicker(dateInput, dateInput.DateFormat.GetFormat());
            };
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(ViewModel.CardNumber, _cardNumber, _cardNumberError, _cardNumberTitle);
            Bind(ViewModel.NameLine1, _name1, _name1Error, _name1Title);
            Bind(ViewModel.NameLine2, _name2, _name2Error);
            Bind(ViewModel.NameLine3, _name3, _name3Error);
            Bind(ViewModel.NameLine4, _name4, _name4Error);
            Bind(ViewModel.Irn, _irn, _irnError, _irnTitle);
            Bind(ViewModel.CardType, _colour, _colourError, _colourTitle);
            Bind(ViewModel.Expiry, _expiryDate, _expiryDateError, _expiryDateTitle);
            Bind(ViewModel.ConsentProvided, _consent, _consentError, _consentTitle);
            Bind(ViewModel.AddNameButton, _add);
            Bind(ViewModel.RemoveNameButton, _remove);

            Bind(_nextButton, ActionType.Next, MedicareCardInputAction.Submit);
            Bind(_backButton, ActionType.Previous, MedicareCardInputAction.Back);
            Bind(_helpButton, ActionType.Browse, MedicareCardInputAction.Help);
        }
    }
}