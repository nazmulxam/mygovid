﻿using Android.App;
using Android.Widget;
using myGovID.Presentation.Contract.Steps.Settings;
using myGovID.Presentation.Contract.Steps.Dashboard;
using myGovID.Droid.Steps.Settings;
using myGovID.Presentation.Contract;
using myGovID.Droid.Constants;
using Android.Support.V7.Widget;
using GalaSoft.MvvmLight.Helpers;

namespace myGovID.Droid.Steps.CaptureEmail
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeBase,
              WindowSoftInputMode = Android.Views.SoftInput.StateHidden,
              NoHistory = true,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class SettingsActivity : StepActivity<ISettingsStep, SettingsViewModel, SettingsAction>
    {
        private ImageButton _backButton;
        private LinearLayoutManager _layoutManager;

        protected override void OnViewModelLoaded()
        {
            SetActionBar(Resource.Layout.ActionBarCenterTitleWithBackButton, Resource.String.settings);
            SetContentView(Resource.Layout.Settings);

            var recyclerViewer = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            _backButton = FindViewById<ImageButton>(Resource.Id.actionbarBack);
            _layoutManager = new LinearLayoutManager(this);
            recyclerViewer.SetLayoutManager(_layoutManager);
            recyclerViewer.SetAdapter(new SettingsListViewAdapter(this, ViewModel.SettingsSectionList, RunAction));
        }

        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind(_backButton, ActionType.Previous);
        }

        private void RunAction(SettingsSectionBase item)
        {
            if (item is SettingsSectionItem sectionItem)
            {
                var action = FindAction(Step, sectionItem.Action, sectionItem.SettingsAction);
                ExecuteAction(action);
            }
        }
    }
}
