﻿using Android.Views;
using Android.Widget;
using GalaSoft.MvvmLight.Helpers;
using myGovID.Presentation.Contract.Steps.Settings;

namespace myGovID.Droid.Steps.Settings
{
    public class SettingsTableSwitchViewCell
        : SettingsTableBaseViewCell<SettingsSectionSwitchItem>
    {
        private Switch _switch;

        public SettingsTableSwitchViewCell(View view)
            : base(view)
        {
            _switch = view.FindViewById<Switch>(Resource.Id.switch1);
        }

        protected override void BindSetup()
        {
            Title.Text = Item.Title;
            _switch.Checked = Item.Selected;

            var switchChangedBinding = new Binding<bool, bool>(_switch, () => _switch.Checked).WhenSourceChanges(() =>
            {
                if (Item.Selected == _switch.Checked) return;
                Item.Selected = _switch.Checked;
                Callback.Invoke(Item);
            });
            BindingList.Add(switchChangedBinding);

            var switchBinding = new Binding<bool, bool>(Item, () => Item.Selected, _switch, () => _switch.Checked, BindingMode.TwoWay);
            BindingList.Add(switchBinding);
        }
    }
}