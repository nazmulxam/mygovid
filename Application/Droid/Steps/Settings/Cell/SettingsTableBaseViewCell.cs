﻿using System;
using System.Collections.Generic;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using GalaSoft.MvvmLight.Helpers;
using myGovID.Presentation.Contract.Steps.Settings;
using static Android.Views.View;

namespace myGovID.Droid.Steps.Settings
{
    public abstract class SettingsTableBaseViewCell<TItem> : RecyclerView.ViewHolder, IOnClickListener
        where TItem : SettingsSectionBase
    {
        protected TItem Item;

        protected Action<TItem> Callback;

        protected TextView Title;

        protected LinearLayout MainLinearLayout;

        protected IList<Binding> BindingList = new List<Binding>();

        public SettingsTableBaseViewCell(View view) : base(view)
        {
            MainLinearLayout = view.FindViewById<LinearLayout>(Resource.Id.linearLayout);

            Title = view.FindViewById<TextView>(Resource.Id.textView);

            MainLinearLayout.SetOnClickListener(this);
        }

        public void SetCallback(Action<TItem> callbak)
        {
            Callback = callbak;
        }

        public void Bind(TItem item)
        {
            Item = item;

            MainLinearLayout.Enabled = item.Enabled;
            MainLinearLayout.Clickable = item.Clickable;
            MainLinearLayout.Alpha = MainLinearLayout.Enabled ? 1 : 0.4f;

            Unbind();

            BindSetup();
        }

        protected abstract void BindSetup();

        public void OnClick(View v)
        {
            Callback?.Invoke(Item);
        }

        public void Unbind()
        {
            foreach (var item in BindingList)
            {
                item.Detach();
            }
        }
    }
}