﻿using Android.Views;
using myGovID.Presentation.Contract.Steps.Settings;

namespace myGovID.Droid.Steps.Settings
{
    public class SettingsTableHeaderViewCell : SettingsTableBaseViewCell<SettingsSection>
    {
        public SettingsTableHeaderViewCell(View view) 
            : base(view)
        {
        }

        protected override void BindSetup()
        {
            Title.Text = Item.Header;
        }
    }
}