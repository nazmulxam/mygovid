﻿using Android.Views;
using myGovID.Presentation.Contract.Steps.Settings;

namespace myGovID.Droid.Steps.Settings
{
    public class SettingsTableViewCell : SettingsTableBaseViewCell<SettingsSectionItem>
    {
        public SettingsTableViewCell(View view)
            : base(view)
        {
        }

        protected override void BindSetup()
        {
            Title.Text = Item.Title;
        }
    }
}