﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Java.Lang;
using myGovID.Presentation.Contract.Steps.Settings;

namespace myGovID.Droid.Steps.Settings
{
    public class SettingsListViewAdapter : RecyclerView.Adapter
    {
        public Dictionary<int, SettingsSectionBase> _list = new Dictionary<int, SettingsSectionBase>();

        private Action<SettingsSectionBase> _callback;

        private Context _context;

        public SettingsListViewAdapter(Context context, IEnumerable<SettingsSection> list, Action<SettingsSectionBase> callback)
        {
            _context = context;
            _callback = callback;
            SetData(list);
        }

        private void SetData(IEnumerable<SettingsSection> list)
        {
            var count = 0;

            foreach (var section in list)
            {
                _list.Add(count++, section);

                foreach (var item in section.ItemList)
                {
                    _list.Add(count++, item);
                }
            }
        }

        public override int ItemCount => _list.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override int GetItemViewType(int position)
        {
            var item = _list[position];

            switch (item)
            {
                case SettingsSection section:
                    return 1;

                case SettingsSectionSwitchItem switchItem:
                    return 2;

                default:
                    return 3;
            }
        }

        public override void OnViewDetachedFromWindow(Java.Lang.Object holder)
        {
            base.OnViewDetachedFromWindow(holder);

            if (holder is SettingsTableBaseViewCell<SettingsSection> holderHeader)
            {
                holderHeader.Unbind();
                return;
            }
            if (holder is SettingsTableBaseViewCell<SettingsSectionSwitchItem> holderSwitch)
            {
                holderSwitch.Unbind();
                return;
            }

            var holderView = holder as SettingsTableBaseViewCell<SettingsSectionItem>;
            holderView?.Unbind();
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var item = _list[position];
            switch (item)
            {
                case SettingsSection section:
                    var vh1 = holder as SettingsTableBaseViewCell<SettingsSection>;
                    vh1.Bind(section);
                    break;

                case SettingsSectionSwitchItem switchItem:
                    var vh2 = holder as SettingsTableBaseViewCell<SettingsSectionSwitchItem>;
                    vh2.Bind(switchItem);
                    break;

                default:
                    var vh3 = holder as SettingsTableBaseViewCell<SettingsSectionItem>;
                    vh3.Bind(item as SettingsSectionItem);
                    break;
            }
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var layoutInflater = LayoutInflater.From(parent.Context);

            switch (viewType)
            {
                case 1:
                    var itemViewHeader = layoutInflater
                                            .Inflate(Resource.Layout.SettingsHeaderViewHolder, parent, false);

                    var headerViewCell = new SettingsTableHeaderViewCell(itemViewHeader);
                    headerViewCell.SetCallback(_callback);
                    return headerViewCell;

                case 2:
                    var itemViewSwitch = layoutInflater
                                            .Inflate(Resource.Layout.SettingsSwitchViewHolder, parent, false);

                    var switchViewCell = new SettingsTableSwitchViewCell(itemViewSwitch);
                    switchViewCell.SetCallback(_callback);
                    return switchViewCell;

                default:
                    var itemView = layoutInflater
                                            .Inflate(Resource.Layout.SettingsViewHolder, parent, false);

                    var viewCell = new SettingsTableViewCell(itemView);
                    viewCell.SetCallback(_callback);
                    return viewCell;
            }
        }
    }
}