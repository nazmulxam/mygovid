﻿using System.Linq;
using Android.App;
using Android.Widget;
using myGovID.Droid.Constants;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.CaptureEmail;
using Android.Widget;
using System.Linq;

namespace myGovID.Droid.Steps.CaptureEmail
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeBase,
              WindowSoftInputMode = Android.Views.SoftInput.StateHidden,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class CaptureEmailActivity : StepActivity<ICaptureEmailStep, CaptureEmailViewModel>
    {
        private Button _nextButton;
        private EditText _emailAddress;
        private TextView _errorLabel;
        protected override void OnViewModelLoaded()
        {
            SetActionBar(Resource.Layout.ActionBarCenterTitle, Resource.String.create_an_account);
            SetContentView(Resource.Layout.CaptureEmail);
            _nextButton = FindViewById<Button>(Resource.Id.nextButton);
            _emailAddress = FindViewById<EditText>(Resource.Id.emailAddressET);
            _errorLabel = FindViewById<TextView>(Resource.Id.errorLabel);
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind((FormInput<string>)ViewModel.Inputs.First(), _emailAddress, _errorLabel);
            Bind(_nextButton, ActionType.Next);
        }
    }
}
