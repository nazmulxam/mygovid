using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using myGovID.Application.Launcher.Contract;
using myGovID.Droid.Configuration;
using myGovID.Droid.Constants;
using myGovID.Presentation.Contract;
using myGovID.Services.Dependency.Contract;
using myGovID.Services.Dependency.Droid;
using myGovID.Services.Crypto.Droid;
using System.Linq;
using myGovID.Droid.Mock;
using myGovID.Application.Launcher.Contract.Mock;

namespace myGovID.Droid.Steps.Launch
{
#if MOCK
    [Activity(Label = ActivityConstants.Label, MainLauncher = true,
              Name = "au.gov.ato.myGovID.LaunchActivity", Theme = ActivityConstants.ThemeSplash,
              LaunchMode = Android.Content.PM.LaunchMode.SingleInstance,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class LaunchActivity : Activity
    {
        private ILauncher _launcher;
        // Step Continuation
        private Action<Task<IStep>> StepContinuation;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            _launcher = new Application.Launcher.Mock.MockLauncher(DependencyService.Shared);
            StepContinuation = (taskResult) =>
            {
                if (taskResult.IsCanceled || taskResult.IsFaulted)
                {
                    Exception exception = taskResult.Exception.GetBaseException();
                    TransitionOnError(exception);
                    return;
                }
                TransitionToStep(taskResult.Result);
            };
            StartActivityForResult(new Intent(this, typeof(Mock.TestCasePickerActivity)), 100);
        }
        private void LaunchInitialStep(string testcaseId, bool useBiometrics, bool fastForward)
        {
            if (fastForward)
            {
                DependencyService.Shared.Register<StepOrchestrator, Mock.FastForwardOrchestrator>(Scope.Singleton);
            }
            else
            {
                DependencyService.Shared.Register<StepOrchestrator, StepOrchestrator>(Scope.Singleton);
            }

            var deviceInfo = new MockDeviceInfoWrapper(new DeviceInfo(this))
            {
                HasBiometricsOverride = useBiometrics
            };

            Application.Launcher.Contract.Mock.MockLaunchConfiguration configuration = new Application.Launcher.Contract.Mock.MockLaunchConfiguration.Builder()
                .SetDeviceInfo(deviceInfo)
                .SetApplicationInfo(new ApplicationInfo(this))
                .SetTestcaseId(testcaseId)
                .SetUseBiometrics(useBiometrics)
                .SetDependencyService(DependencyService.Shared).Build();
            DependencyService.Shared.Register<ICryptoConfig, CryptoConfig>(Scope.Singleton);
            _launcher.Configure(configuration);
            IStepRepository stepRepository = DependencyService.Shared.Resolve<IStepRepository>();
            // Fire and forget
            stepRepository.GetInitialStepAsync().ContinueWith(StepContinuation);
        }
        protected override void OnActivityResult(int requestCode, [Android.Runtime.GeneratedEnum] Result resultCode, Intent data)
        {
            if (requestCode == 100)
            {
                switch (resultCode)
                {
                    case Result.Ok:
                        LaunchInitialStep(
                            data.GetStringExtra("TestCasePickerActivity.TestCaseName"),
                            data.GetBooleanExtra("TestCasePickerActivity.UseBiometrics", true),
                            data.GetBooleanExtra("TestCasePickerActivity.FastForward", false));
                        return;

                    case Result.Canceled:
                        StartActivityForResult(new Intent(this, typeof(Mock.TestCasePickerActivity)), 100);
                        return;
                }
            }

            base.OnActivityResult(requestCode, resultCode, data);
        }
        public override void OnBackPressed()
        {
            // Do nothing.
        }
        private void TransitionToStep(IStep newStep)
        {
            RunOnUiThread(() =>
            {
                StepOrchestrator stepOrchestrator = DependencyService.Shared.Resolve<StepOrchestrator>();
                Intent intent = stepOrchestrator.GetActivity(this, newStep);
                StartActivity(intent);
                if (!intent.GetBooleanExtra("IsOverlay", false))
                {
                    Finish();
                }
                OverridePendingTransition(Android.Resource.Animation.FadeIn, Android.Resource.Animation.FadeOut);
            });
        }
        private void TransitionOnError(Exception exception)
        {
            Console.WriteLine("Exception " + exception.Message);
            StepOrchestrator stepOrchestrator = DependencyService.Shared.Resolve<StepOrchestrator>();
            TransitionToStep(stepOrchestrator.GetDeadEndStep("UNK000002", "Uncaught Exception", exception.Message));
        }
    }
#else
    [Activity(Label = ActivityConstants.Label, MainLauncher = true,
              Name = "au.gov.ato.myGovID.LaunchActivity", Theme = ActivityConstants.ThemeSplash,
              LaunchMode = Android.Content.PM.LaunchMode.SingleInstance,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class LaunchActivity : Activity
    {
        private ILauncher _launcher;
        // Step Continuation
        private Action<Task<IStep>> StepContinuation;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            _launcher = new Application.Launcher.Launcher(DependencyService.Shared);
            StepContinuation = (taskResult) =>
            {
                if (taskResult.IsCanceled || taskResult.IsFaulted)
                {
                    Exception exception = taskResult.Exception.GetBaseException();
                    TransitionOnError(exception);
                    return;
                }
                TransitionToStep(taskResult.Result);
            };

            DependencyService.Shared.Register<StepOrchestrator, StepOrchestrator>(Scope.Singleton);
            LaunchInitialStep();
        }
        private void LaunchInitialStep()
        {
            LaunchConfiguration configuration = new LaunchConfiguration.Builder()
                .SetDeviceInfo(new DeviceInfo(this))
                .SetApplicationInfo(new ApplicationInfo(this)).Build();
            DependencyService.Shared.Register<ICryptoConfig, CryptoConfig>(Scope.Singleton);
            _launcher.Configure(configuration);
            IStepRepository stepRepository = DependencyService.Shared.Resolve<IStepRepository>();
            // Fire and forget
            stepRepository.GetInitialStepAsync().ContinueWith(StepContinuation);
        }
        public override void OnBackPressed()
        {
            // Do nothing.
        }
        private void TransitionToStep(IStep newStep)
        {
            RunOnUiThread(() =>
            {
                StepOrchestrator stepOrchestrator = DependencyService.Shared.Resolve<StepOrchestrator>();
                Intent intent = stepOrchestrator.GetActivity(this, newStep);
                StartActivity(intent);
                if (!intent.GetBooleanExtra("IsOverlay", false))
                {
                    Finish();
                }
                OverridePendingTransition(Android.Resource.Animation.FadeIn, Android.Resource.Animation.FadeOut);
            });
        }
        private void TransitionOnError(Exception exception)
        {
            Console.WriteLine("Exception " + exception.Message);
            StepOrchestrator stepOrchestrator = DependencyService.Shared.Resolve<StepOrchestrator>();
            TransitionToStep(stepOrchestrator.GetDeadEndStep("UNK000002", "Uncaught Exception", exception.Message));
        }
    }
#endif
}