﻿using System.Linq;
using Android.App;
using Android.Views;
using Android.Widget;
using myGovID.Droid.Constants;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.CapturePassword;

namespace myGovID.Droid.Steps.CapturePassword
{
    [Activity(Label = ActivityConstants.Label, Theme = ActivityConstants.ThemeBase,
              WindowSoftInputMode = SoftInput.StateHidden,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class CapturePasswordActivity : StepActivity<ICapturePasswordStep, CapturePasswordViewModel>
    {
        private Button _nextButton;
        private EditText _passwordET;
        private TextView _passwordError;
        private TextView _passwordTitle;
        private EditText _confirmPasswordET;
        private TextView _confirmPasswordError;
        private TextView _confirmPasswordTitle;
        protected override void OnViewModelLoaded()
        {
            SetActionBar(Resource.Layout.ActionBarCenterTitle, Resource.String.create_an_account);
            SetContentView(Resource.Layout.CreatePassword);
            _nextButton = FindViewById<Button>(Resource.Id.nextButton);
            _passwordET = FindViewById<EditText>(Resource.Id.passwordET);
            _passwordError = FindViewById<TextView>(Resource.Id.passwordErrorLabel);
            _passwordTitle = FindViewById<TextView>(Resource.Id.passwordTitle);
            _confirmPasswordET = FindViewById<EditText>(Resource.Id.confirmPasswordET);
            _confirmPasswordError = FindViewById<TextView>(Resource.Id.confirmPasswordErrorLabel);
            _confirmPasswordTitle = FindViewById<TextView>(Resource.Id.confirmPasswordTitle);
        }
        protected override void AttachBindings()
        {
            base.AttachBindings();
            Bind((FormInput<string>)ViewModel.Inputs.First((input) => input.MetaData == InputMetaData.Password), _passwordET, _passwordError, _passwordTitle);
            Bind((FormInput<string>)ViewModel.Inputs.First((input) => input.MetaData == InputMetaData.ConfirmPassword), _confirmPasswordET, _confirmPasswordError, _confirmPasswordTitle);
            Bind(_nextButton, ActionType.Next);
        }
    }
}