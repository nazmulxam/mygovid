﻿using myGovID.Presentation.Contract;
namespace myGovID.Droid.Steps
{
    public interface IStepActivity
    {
        IStep Step { get; set; }
    }
}