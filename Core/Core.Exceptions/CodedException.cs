﻿using System;
namespace myGovID.Core.Exceptions
{
    public class CodedException: Exception
    {
        public string Code { get; }
        public string Title { get; }
        public string Description { get; }

        public CodedException(string code, string title, string description, Exception innerException = null) : base(string.Format("{0} - {1} - {2}", code, title, description), innerException)
        {
            Code = code;
            Title = title;
            Description = description;
        }
    }
}