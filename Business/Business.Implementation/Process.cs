﻿using System.Collections.Generic;
using myGovID.Business.Contract.Common.Process;

namespace myGovID.Business
{
    public class Process: IProcess
    {
        public string ProcessId { get; set; }

        public ProcessState ProcessState { get; set; }

        public IEnumerable<ResourceLink> CurrentLinks { get; }

        public string CurrentResource { get; set; }

        public IdentityProofingLevel ProcessIPLevel { get; set; }

        public Process()
        {
            CurrentLinks = new List<ResourceLink>();
            ProcessState = ProcessState.Okay;
            ProcessIPLevel = IdentityProofingLevel.Unknown;
        }
    }
}
