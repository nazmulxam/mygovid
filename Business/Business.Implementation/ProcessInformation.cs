﻿using System.Collections.Generic;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Session;

namespace myGovID.Business
{
    public class ProcessInformation: IProcessInformation
    {
        public IProcess CurrentProcess { get; }

        public AuthenticationState AuthenticatedState { get; }

        public IEnumerable<Capability> Capabilities { get; }

        public ResourceLink UpgradeLink { get; }

        public IEnumerable<DeviceCapability> DeviceCapabilities { get; }

        public bool UpgradeActioned { get; }

        public ProcessInformation(IProcess currentProcess, AuthenticationState authenticatedState, IEnumerable<Capability> capabilities, ResourceLink upgradeLink, bool upgradeActioned, IEnumerable<DeviceCapability> deviceCapabilities)
        {
            CurrentProcess = currentProcess;
            AuthenticatedState = authenticatedState;
            Capabilities = capabilities;
            UpgradeLink = upgradeLink;
            UpgradeActioned = upgradeActioned;
            DeviceCapabilities = deviceCapabilities;
        }
    }
}
