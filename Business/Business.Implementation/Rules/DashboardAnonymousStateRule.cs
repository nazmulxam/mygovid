﻿using myGovID.Business.Contract;
using myGovID.Business.Specification.Link.Identity;
using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Rules
{
    public class DashboardAnonymousStateRule : Rule
    {
        public DashboardAnonymousStateRule(IUriService uriService) : base(BusinessState.AnonymousDashboard,
                                                                          new PassportLinkAnonymousSpecification(uriService)
                                                                          .Or(new DriverLicenceAnonymousLinkSpecification(uriService))
                                                                          .Or(new MedicareLinkAnonymousSpecification(uriService))
                                                                          .Or(new FVSLinkAnonymousSpecification(uriService)))
        {
        }
    }
}