﻿using myGovID.Business.Contract;
using myGovID.Business.Specification.Link;
using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Rules
{
    public class PersonalDetailsStateRule : Rule
    {
        public PersonalDetailsStateRule(IUriService uriService) : base(BusinessState.PersonalDetails, new PersonalDetailsLinkSpecification(uriService))
        {}
    }
}