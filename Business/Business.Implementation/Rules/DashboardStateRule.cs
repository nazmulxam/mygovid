﻿using myGovID.Business.Contract;
using myGovID.Business.Specification.Link;
using myGovID.Business.Specification.Link.Identity;
using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Rules
{
    public class DashboardStateRule : Rule
    {
        public DashboardStateRule(IUriService uriService) : base(BusinessState.Dashboard,
                                                                 new HasNoLinksSpecification()
                                                                 .Or(new PassportLinkSpecification(uriService))
                                                                 .Or(new DriverLicenceLinkSpecification(uriService))
                                                                 .Or(new MedicareLinkSpecification(uriService))
                                                                 .Or(new FVSLinkSpecification(uriService)))
        {
        }
    }
}