﻿using myGovID.Business.Contract;
using myGovID.Business.Specification.Capability;
using myGovID.Business.Specification.Link.Identity;
using myGovID.Business.Specification.Link.Resource;
using myGovID.Services.Uri.Contract;
using myGovID.Business.Specification.DeviceCapability;

namespace myGovID.Business.Rules
{
    public class InProcessFVSStateRule: Rule
    {
        public InProcessFVSStateRule(IUriService uriService) : base(BusinessState.InProcessFVS,
                                                                    new PassportResourceSpecification(uriService)
                                                                    .And(new FvsPassportCapabilitySpecification())
                                                                    .And(new CameraDeviceCapabilitySpecification())
                                                                    .And(new FVSLinkSpecification(uriService)
                                                                         .Or(new FVSLinkAnonymousSpecification(uriService))))
        {
        }
    }
}