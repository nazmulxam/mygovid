﻿using myGovID.Business.Contract;
using myGovID.Business.Specification.Link;
using myGovID.Business.Specification.Link.Identity;
using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Rules
{
    public class MedicareStateRule : Rule
    {
        public MedicareStateRule(IUriService uriService) : base(BusinessState.Medicare,
                                                                new MedicareLinkSpecification(uriService)
                                                                .Or(new MedicareLinkAnonymousSpecification(uriService)))
        {
        }
    }
}