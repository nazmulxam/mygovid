﻿using myGovID.Business.Contract;
using myGovID.Business.Specification;

namespace myGovID.Business.Rules
{
    public class VersionUpgradeStateRule : Rule
    {
        public VersionUpgradeStateRule() : base(BusinessState.VersionUpgrade, new VersionUpgradeSpecification())
        {
        }
    }
}
