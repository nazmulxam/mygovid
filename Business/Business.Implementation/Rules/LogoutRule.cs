﻿using myGovID.Business.Specification;

namespace myGovID.Business.Rules
{
    public class LogoutRule : Rule
    {
        public LogoutRule() : base(Contract.BusinessState.Logout, new AuthenticationExpiredSpecification())
        {
        }
    }
}
