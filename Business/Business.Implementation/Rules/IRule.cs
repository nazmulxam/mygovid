﻿using myGovID.Business.Contract;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Specification;

namespace myGovID.Business.Rules
{
    public interface IRule
    {
        BusinessState BusinessState { get; }
        ISpecification<IProcessInformation> Specification { get; }
    }
}
