﻿using myGovID.Business.Contract;
using myGovID.Business.Specification.Process;

namespace myGovID.Business.Rules
{
    public class DashboardDisabledStateRule: Rule
    {
        public DashboardDisabledStateRule() : base(BusinessState.DefaultDashboard, new LockedProcessSpecification().Or(new UnableToVerifyProcessSpecification()))
        {
        }
    }
}
