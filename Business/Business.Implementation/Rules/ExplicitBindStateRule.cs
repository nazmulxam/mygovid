﻿using myGovID.Business.Contract;
using myGovID.Business.Specification.Link.Identity;
using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Rules
{
    public class ExplicitBindStateRule : Rule
    {
        public ExplicitBindStateRule(IUriService uriService) : base(BusinessState.ExplicitBind, new ExplicitBindLinkSpecification(uriService))
        {
        }
    }
}
