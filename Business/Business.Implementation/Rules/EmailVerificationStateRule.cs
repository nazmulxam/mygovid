﻿using myGovID.Business.Contract;
using myGovID.Services.Uri.Contract;
using myGovID.Business.Specification.Link;
namespace myGovID.Business.Rules
{
    public class EmailVerificationStateRule : Rule
    {
        public EmailVerificationStateRule(IUriService uriService) : base(BusinessState.EmailVerification, new EmailVerificationLinkSpecification(uriService))
        {
        }
    }
}