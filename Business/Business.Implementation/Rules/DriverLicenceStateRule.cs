﻿using myGovID.Business.Contract;
using myGovID.Business.Specification.Link.Identity;
using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Rules
{
    public class DriverLicenceStateRule : Rule
    {
        public DriverLicenceStateRule(IUriService uriService) : base(BusinessState.DriverLicence,
                                                                     new DriverLicenceLinkSpecification(uriService)
                                                                    .Or(new DriverLicenceAnonymousLinkSpecification(uriService)))
        {
        }
    }
}