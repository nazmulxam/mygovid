﻿using myGovID.Business.Contract;
using myGovID.Business.Specification.Link;
using myGovID.Business.Specification.Link.Identity;
using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Rules
{
    public class FVSStateRule : Rule
    {
        public FVSStateRule(IUriService uriService) : base(BusinessState.FVS,
                                                           new FVSLinkSpecification(uriService)
                                                           .Or(new FVSLinkAnonymousSpecification(uriService)))
        {
        }
    }
}