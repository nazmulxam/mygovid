﻿using myGovID.Business.Contract;
using myGovID.Business.Specification.Link;
using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Rules
{
    public class CaptureEmailStateRule : Rule
    {
        public CaptureEmailStateRule(IUriService uriService) : base(BusinessState.Email, new EmailLinkSpecification(uriService))
        {
        }
    }
}
