﻿using myGovID.Business.Contract;
using myGovID.Business.Specification.Link;
using myGovID.Business.Specification.Link.Identity;
using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Rules
{
    public class PassportStateRule : Rule
    {
        public PassportStateRule(IUriService uriService) : base(BusinessState.Passport,
                                                                new PassportLinkSpecification(uriService)
                                                                .Or(new PassportLinkAnonymousSpecification(uriService)))
        {
        }
    }
}