﻿namespace myGovID.Business.Specification.Capability
{
    public class FvsPassportCapabilitySpecification : CapabilitySpecification
    {
        protected override Contract.Common.Application.Capability Capability => Contract.Common.Application.Capability.DVS_Passport;
    }
}