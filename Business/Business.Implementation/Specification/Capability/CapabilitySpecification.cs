﻿using System.Linq;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Specification;

namespace myGovID.Business.Specification.Capability
{
    public abstract class CapabilitySpecification: CompositeSpecification<IProcessInformation>
    {
        protected abstract Contract.Common.Application.Capability Capability { get; }
        public override bool IsSatisfiedBy(IProcessInformation candidate)
        {
            return candidate.Capabilities.Contains(Capability);
        }
    }
}