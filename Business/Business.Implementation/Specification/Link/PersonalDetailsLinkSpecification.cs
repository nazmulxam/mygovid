﻿using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Specification.Link
{
    public class PersonalDetailsLinkSpecification : LinkSpecification
    {
        public PersonalDetailsLinkSpecification(IUriService uriService) : base(uriService)
        {
        }
        public override string GetLinkUrlFragment()
        {
            return "/poi/{processId}/documents/personalDetailsDocuments";
        }
    }
}
