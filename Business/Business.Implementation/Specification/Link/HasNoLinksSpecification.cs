﻿using System.Collections.Generic;
using System.Linq;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Specification;

namespace myGovID.Business.Specification.Link
{
    public class HasNoLinksSpecification : CompositeSpecification<IProcessInformation>
    {
        public override bool IsSatisfiedBy(IProcessInformation candidate)
        {
            IEnumerable<ResourceLink> links = candidate.CurrentProcess.CurrentLinks;
            return !links?.Any(link => link.Target == TargetType.Next) ?? true;
        }
    }
}