﻿using myGovID.Business.Contract.Common.Process;
using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Specification.Link
{
    public class ResendEmailVerificationLinkSpecification : LinkSpecification
    {
        public override TargetType Target => TargetType.Clone;
        public override string Verb => "POST";
        public ResendEmailVerificationLinkSpecification(IUriService uriService) : base(uriService)
        {
        }
        public override string GetLinkUrlFragment()
        {
            return "/poi/{processId}/tasks?taskId={taskId}";
        }
    }
}
