﻿using myGovID.Business.Contract.Common.Process;
using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Specification.Link
{
    public class EmailLinkSpecification: LinkSpecification
    {
        public EmailLinkSpecification(IUriService uriService) : base(uriService)
        {
        }
        public override string GetLinkUrlFragment()
        {
            return "/poi/{processId}/documents/emails";
        }
    }
}