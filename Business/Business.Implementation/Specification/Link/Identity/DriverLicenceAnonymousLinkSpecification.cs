﻿using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Specification.Link.Identity
{
    public class DriverLicenceAnonymousLinkSpecification : DriverLicenceLinkSpecification
    {
        public DriverLicenceAnonymousLinkSpecification(IUriService uriService) : base(uriService)
        {}
        protected override bool IdentityAvailable => false;
    }
}
