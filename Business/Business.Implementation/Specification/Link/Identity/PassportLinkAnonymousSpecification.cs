﻿using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Specification.Link.Identity
{
    public class PassportLinkAnonymousSpecification : PassportLinkSpecification
    {
        public PassportLinkAnonymousSpecification(IUriService uriService) : base(uriService)
        {
        }
        protected override bool IdentityAvailable => false;
    }
}