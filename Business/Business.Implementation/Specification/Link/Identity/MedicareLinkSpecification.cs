﻿using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Specification.Link.Identity
{
    public class MedicareLinkSpecification : IdentityLinkSpecification
    {
        public MedicareLinkSpecification(IUriService uriService) : base(uriService)
        {}
        public override string GetLinkUrlFragment()
        {
            return "/poi/{processId}/documents/medicareDocuments";
        }
    }
}
