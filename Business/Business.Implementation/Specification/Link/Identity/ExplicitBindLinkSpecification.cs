﻿using myGovID.Business.Contract.Common.Process;
using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Specification.Link.Identity
{
    public class ExplicitBindLinkSpecification : IdentityLinkSpecification
    {
        public ExplicitBindLinkSpecification(IUriService uriService) : base(uriService)
        {}
        public override string GetLinkUrlFragment()
        {
            return "/poi/{processId}/bind";
        }
    }
}
