﻿using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Specification.Link.Identity
{
    public class MedicareLinkAnonymousSpecification : MedicareLinkSpecification
    {
        public MedicareLinkAnonymousSpecification(IUriService uriService) : base(uriService)
        {
        }
        protected override bool IdentityAvailable => false;
    }
}
