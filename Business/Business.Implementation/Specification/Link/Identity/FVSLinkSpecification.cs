﻿using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Specification.Link.Identity
{
    public class FVSLinkSpecification : IdentityLinkSpecification
    {
        public FVSLinkSpecification(IUriService uriService) : base(uriService)
        {}
        public override string GetLinkUrlFragment()
        {
            return "/poi/{processId}/documents/{type}/faceBiometric";
        }
    }
}
