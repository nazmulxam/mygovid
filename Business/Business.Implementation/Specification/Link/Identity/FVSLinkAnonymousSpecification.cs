﻿using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Specification.Link.Identity
{
    public class FVSLinkAnonymousSpecification : FVSLinkSpecification
    {
        public FVSLinkAnonymousSpecification(IUriService uriService) : base(uriService)
        {
        }
        protected override bool IdentityAvailable => false;
    }
}
