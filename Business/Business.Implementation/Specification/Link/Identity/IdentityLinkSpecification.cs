﻿using myGovID.Business.Contract.Common.Process;
using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Specification.Link.Identity
{
    public abstract class IdentityLinkSpecification: LinkSpecification
    {
        protected IdentityLinkSpecification(IUriService uriService) : base(uriService)
        {
        }
        protected virtual bool IdentityAvailable { get { return true; } }
        public override AuthenticationType AuthenticationMethod
        {
            get
            {
                return IdentityAvailable ? AuthenticationType.Extension : AuthenticationType.Credential;
            }
        }
    }
}
