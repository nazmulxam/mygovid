﻿using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Specification.Link.Identity
{
    public class DriverLicenceLinkSpecification : IdentityLinkSpecification
    {
        public DriverLicenceLinkSpecification(IUriService uriService) : base(uriService)
        {}
        public override string GetLinkUrlFragment()
        {
            return "/poi/{processId}/documents/driverLicenceDocuments";
        }
    }
}