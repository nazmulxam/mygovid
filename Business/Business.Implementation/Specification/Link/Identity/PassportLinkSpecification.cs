﻿using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Specification.Link.Identity
{
    public class PassportLinkSpecification : IdentityLinkSpecification
    {
        public PassportLinkSpecification(IUriService uriService) : base(uriService)
        {
        }
        public override string GetLinkUrlFragment()
        {
            return "/poi/{processId}/documents/passportDocuments";
        }
    }
}