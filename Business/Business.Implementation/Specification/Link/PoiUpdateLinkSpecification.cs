﻿using myGovID.Business.Contract.Common.Process;
using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Specification.Link
{
    public class PoiUpdateLinkSpecification : LinkSpecification
    {
        public PoiUpdateLinkSpecification(IUriService uriService) : base(uriService)
        {
        }
        public override string GetLinkUrlFragment()
        {
            return "/poi/{processId}";
        }
        public override TargetType Target { get { return TargetType.Process; } }
    }
}
