﻿using myGovID.Business.Contract.Common.Process;
using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Specification.Link
{
    public class ChangeEmailLinkSpecification : LinkSpecification
    {
        public override TargetType Target => TargetType.Cancel;
        public override string Verb => "DELETE";
        public ChangeEmailLinkSpecification(IUriService uriService) : base(uriService)
        {
        }
        public override string GetLinkUrlFragment()
        {
            return "/poi/{processId}/tasks/{taskId}";
        }
    }
}