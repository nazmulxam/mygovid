﻿using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Specification.Link.Resource
{
    public class PassportResourceSpecification : ResourceSpecification
    {
        public PassportResourceSpecification(IUriService uriService) : base(uriService)
        {
        }
        public override string GetLinkUrlFragment()
        {
            return "/poi/{processId}/documents/passportDocuments/{documentId}";
        }
    }
}
