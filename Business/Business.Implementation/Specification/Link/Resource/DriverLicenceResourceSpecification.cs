﻿using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Specification.Link.Resource
{
    public class DriverLicenceResourceSpecification : ResourceSpecification
    {
        public DriverLicenceResourceSpecification(IUriService uriService) : base(uriService)
        {
        }
        public override string GetLinkUrlFragment()
        {
            return "/poi/{processId}/documents/driverLicenceDocuments/{documentId}";
        }
    }
}
