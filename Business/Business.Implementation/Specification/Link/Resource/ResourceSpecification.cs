﻿using myGovID.Business.Contract.Common.Process;
using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Specification.Link
{
    public abstract class ResourceSpecification : LinkSpecification
    {
        protected ResourceSpecification(IUriService uriService) : base(uriService)
        {}
        public override bool IsSatisfiedBy(IProcessInformation candidate)
        {
            string resource = candidate.CurrentProcess.CurrentResource;
            return !string.IsNullOrWhiteSpace(resource) && IsMatched(resource).Match;
        }
    }
}
