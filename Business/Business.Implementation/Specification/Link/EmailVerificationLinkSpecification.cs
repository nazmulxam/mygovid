﻿using myGovID.Business.Contract.Common.Process;
using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Specification.Link
{
    public class EmailVerificationLinkSpecification : LinkSpecification
    {
        public EmailVerificationLinkSpecification(IUriService uriService) : base(uriService)
        {
        }
        public override string GetLinkUrlFragment()
        {
            return "/poi/{processId}/tasks/{taskId}/emailVerificationResponse";
        }
    }
}