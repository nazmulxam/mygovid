﻿using System;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Specification;
using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Specification.Link
{
    public abstract class LinkSpecification : CompositeSpecification<IProcessInformation>
    {
        private readonly IUriService _uriService;
        protected LinkSpecification(IUriService uriService)
        {
            _uriService = uriService;
        }
        public abstract string GetLinkUrlFragment();
        public virtual string Verb { get { return "POST"; } }
        public virtual AuthenticationType AuthenticationMethod { get { return AuthenticationType.None; } }
        public virtual TargetType Target { get { return TargetType.Next; } }
        public override bool IsSatisfiedBy(IProcessInformation candidate)
        {
            IProcess process = candidate.CurrentProcess;
            if (process.CurrentLinks != null)
            {
                foreach (ResourceLink serverLink in process.CurrentLinks)
                {
                    if (IsMatched(serverLink)?.Match ?? false)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public ResourceLink FindLink(IProcessInformation candidate)
        {
            IProcess process = candidate.CurrentProcess;
            if (process.CurrentLinks != null)
            {
                foreach (ResourceLink serverLink in process.CurrentLinks)
                {
                    if (IsMatched(serverLink)?.Match ?? false)
                    {
                        return serverLink;
                    }
                }
            }
            return null;
        }
        private UriMatchResult IsMatched(ResourceLink link)
        {
            AuthenticationType authenticationType = AuthenticationMethod;
            var linkAttributesMatch = (authenticationType == AuthenticationType.None || authenticationType == link.AuthenticationMethod)
                                    && Target == link.Target
                                    && link.Verb.ToUpper() == Verb;
            if (!linkAttributesMatch)
            {
                return null;
            }
            var template = string.Concat("/api/{apiVersion}", this.GetLinkUrlFragment());
            var url = FrameUrl("http://localhost", link.Url);
            return _uriService.Match(template, url);
        }
        protected UriMatchResult IsMatched(string link)
        {
            var template = string.Concat("/api/{apiVersion}", this.GetLinkUrlFragment());
            var url = FrameUrl("http://localhost", link);
            return _uriService.Match(template, url);
        }
        private string FrameUrl(string baseUrl, string contextPath)
        {
            if (contextPath.StartsWith("http", StringComparison.InvariantCulture))
            {
                return contextPath;
            }
            string baseUrlWithSlash = baseUrl.EndsWith("/", StringComparison.InvariantCulture) ? baseUrl : (baseUrl + "/");
            string contextPathWithoutSlash = contextPath.StartsWith("/", StringComparison.InvariantCulture)
                                      ? contextPath.Substring(1)
                                      : contextPath;
            return baseUrlWithSlash + contextPathWithoutSlash;
        }
    }
}