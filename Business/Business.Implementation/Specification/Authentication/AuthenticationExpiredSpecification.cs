﻿using myGovID.Business.Contract.Common.Session;
using myGovID.Business.Specification.Authentication;

namespace myGovID.Business.Specification
{
    public class AuthenticationExpiredSpecification : AuthenticationSpecification
    {
        protected override AuthenticationState AuthenticationState => AuthenticationState.AuthenticationExpired;
    }
}
