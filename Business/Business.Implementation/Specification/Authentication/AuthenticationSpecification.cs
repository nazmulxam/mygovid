﻿using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Session;
using myGovID.Business.Contract.Common.Specification;

namespace myGovID.Business.Specification.Authentication
{
    public abstract class AuthenticationSpecification : CompositeSpecification<IProcessInformation>
    {
        protected abstract AuthenticationState AuthenticationState { get; }
        public override bool IsSatisfiedBy(IProcessInformation candidate)
        {
            return candidate.AuthenticatedState == AuthenticationState;
        }
    }
}
