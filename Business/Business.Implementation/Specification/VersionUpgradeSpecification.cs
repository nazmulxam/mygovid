﻿using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Specification;

namespace myGovID.Business.Specification
{
    public class VersionUpgradeSpecification : CompositeSpecification<IProcessInformation>
    {
        public override bool IsSatisfiedBy(IProcessInformation candidate)
        {
            return candidate.UpgradeLink != null && !candidate.UpgradeActioned;
        }
    }
}