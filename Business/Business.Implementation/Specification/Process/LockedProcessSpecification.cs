﻿using myGovID.Business.Contract.Common.Process;

namespace myGovID.Business.Specification.Process
{
    public class LockedProcessSpecification : ProcessStateSpecification
    {
        protected override ProcessState ProcessState => ProcessState.Locked;
    }
}
