﻿using myGovID.Business.Contract.Common.Process;

namespace myGovID.Business.Specification.Process
{
    public class UnableToVerifyProcessSpecification : ProcessStateSpecification
    {
        protected override ProcessState ProcessState => ProcessState.UnableToVerify;
    }
}
