﻿using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Specification;

namespace myGovID.Business.Specification.Process
{
    public abstract class ProcessStateSpecification : CompositeSpecification<IProcessInformation>
    {
        protected abstract ProcessState ProcessState { get; }
        public override bool IsSatisfiedBy(IProcessInformation candidate)
        {
            return candidate.CurrentProcess.ProcessState == ProcessState;
        }
    }
}