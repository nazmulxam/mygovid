﻿using myGovID.Business.Contract.Common.Process;

namespace myGovID.Business.Specification.Process
{
    public class OkayProcessSpecification : ProcessStateSpecification
    {
        protected override ProcessState ProcessState => ProcessState.Okay;
    }
}