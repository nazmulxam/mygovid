﻿namespace myGovID.Business.Specification.DeviceCapability
{
    public class CameraDeviceCapabilitySpecification : DeviceCapabilitySpecification
    {
        protected override Contract.Common.Application.DeviceCapability Capability => Contract.Common.Application.DeviceCapability.Camera;
    }
}