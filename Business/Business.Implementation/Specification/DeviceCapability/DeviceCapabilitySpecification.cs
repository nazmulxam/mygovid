﻿using System.Linq;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Specification;

namespace myGovID.Business.Specification.DeviceCapability
{
    public abstract class DeviceCapabilitySpecification : CompositeSpecification<IProcessInformation>
    {
        protected abstract Contract.Common.Application.DeviceCapability Capability { get; }
        public override bool IsSatisfiedBy(IProcessInformation candidate)
        {
            return candidate.DeviceCapabilities.Contains(Capability);
        }
    }
}
