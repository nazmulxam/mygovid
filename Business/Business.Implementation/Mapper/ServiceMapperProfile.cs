﻿using AutoMapper;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Model;
using myGovID.Services.Api.Contract.Model;
using myGovID.Services.Api.Contract.Model.Poi.Email;
using myGovID.Services.Api.Contract.Model.Poi.EmailVerification;
using myGovID.Services.Api.Contract.Model.Poi.Register;
using myGovID.Services.Api.Contract.Model.System.Version;
using System.Collections.Generic;
using myGovID.Services.Api.Contract.Model.Task;
using myGovID.Services.Api.Contract.Model.Poi.PersonalDetails;
using System;
using myGovID.Services.Api.Contract.Model.Poi;
using myGovID.Services.Api.Contract.Model.Poi.Passport;
using myGovID.Services.Api.Contract.Model.Poi.MedicareDetails;
using myGovID.Services.Api.Contract.Model.Poi.DriverLicence;
using myGovID.Services.Api.Contract.Model.Poi.DriverLicenceDetails;

namespace myGovID.Business.Mapper
{
    public class ServiceMapperProfile : Profile
    {
        public ServiceMapperProfile()
        {
            LinkMap();
            MessageMap();
            EmailModelMap();
            EmailVerificationModelMap();
            RegisterPOIModelMap();
            AlertMap();
            TermsAndConditionsMap();
            CredentialResponseModelMap();
            CaptureEmailVerificationResponseModelMap();
            CreateCryptoResultMap();
            TaskResponseModelMap();
            PersonalDetailsModelMap();
            PassportDetailsModelMap();
            GetPoiModelMap();
            MedicareDetailsModelMap();
            DriverLicenceDetailsModelMap();
        }

        /// <summary>
        /// Creates the mapping between ResourceLink (Business layer) class and Link (Services Layer) class.
        /// </summary>
        private void LinkMap()
        {
            CreateMap<Link, ResourceLink>()
                .ConvertUsing((Link src, ResourceLink arg2) => arg2 = new ResourceLink(src.Location, src.Method,
                                                   AutoMapper.Mapper.Map<Contract.Common.Process.AuthenticationType>(src.AuthType),
                                                   AutoMapper.Mapper.Map<Contract.Common.Process.TargetType>(src.TargetType)));

            CreateMap<ResourceLink, Link>()
                .ForMember(dest => dest.Location, opt => opt.MapFrom(src => src.Url))
                .ForMember(dest => dest.Method, opt => opt.MapFrom(src => src.Verb))
                .ForMember(dest => dest.TargetType, opt => opt.MapFrom(src => src.Target))
                .ForMember(dest => dest.AuthType, opt => opt.MapFrom(src => src.AuthenticationMethod));
        }
        private void CredentialResponseModelMap()
        {
            CreateMap<Services.Api.Contract.Model.Credential.CredentialResponseModel, CredentialResponseModel>()
                .ConvertUsing((Services.Api.Contract.Model.Credential.CredentialResponseModel src, CredentialResponseModel arg2) => arg2 = new CredentialResponseModel(src.CredentialToken, src.P7));
        }
        private void CaptureEmailVerificationResponseModelMap()
        {
            CreateMap<Services.Api.Contract.Model.Poi.EmailVerification.EmailVerificationResponseModel, 
                        Contract.Model.EmailVerificationResponseModel>()
            .ConvertUsing((Services.Api.Contract.Model.Poi.EmailVerification.EmailVerificationResponseModel src, Contract.Model.EmailVerificationResponseModel arg2) => arg2 = new Contract.Model.EmailVerificationResponseModel(
                                        AutoMapper.Mapper.Map<IEnumerable<Message>>(src.Messages),
                                        AutoMapper.Mapper.Map<IEnumerable<ResourceLink>>(src.Links),
                                        src.ProcessId, src.Status, src.VerificationCodeResult, src.PoiAssuranceToken));
        }
        private void CreateCryptoResultMap()
        {
            CreateMap<Services.Crypto.Contract.Model.CreateResult, CryptoCreateResultModel>()
            .ConvertUsing((Services.Crypto.Contract.Model.CreateResult src, CryptoCreateResultModel arg2) => arg2 = new CryptoCreateResultModel(src.P10base64, src.RequestId));
        }

        private void TaskResponseModelMap()
        {
            CreateMap<TaskResponseModel, BusinessTaskResponseModel>()
                .ConvertUsing((TaskResponseModel src, BusinessTaskResponseModel arg2) => arg2 = new BusinessTaskResponseModel(AutoMapper.Mapper.Map<IEnumerable<ResourceLink>>(src.Links),
                                                                   AutoMapper.Mapper.Map<IEnumerable<Message>>(src.Messages), src.Eta,
                                                                   AutoMapper.Mapper.Map<TaskStatus>(src.Status), src.Id));
        }
        private void PersonalDetailsModelMap()
        {
            CreateMap<DateOfBirthModel, DateTime>()
                .ConvertUsing((DateOfBirthModel src, DateTime arg2) => arg2 = new DateTime(src.Year, src.Month, src.Date));

            CreateMap<DateTime, DateOfBirthModel>()
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.Day))
                .ForMember(dest => dest.Month, opt => opt.MapFrom(src => src.Month))
                .ForMember(dest => dest.Year, opt => opt.MapFrom(src => src.Year));

            CreateMap<CapturePersonalDetailsModel, PersonalDetailsRequestModel>()
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(dest => dest.DateOfBirth, opt => opt.MapFrom(src => src.BirthDate));

            CreateMap<PersonalDetailsResponseModel, PersonalDetailsBusinessResponseModel>()
                .ConvertUsing((PersonalDetailsResponseModel src, PersonalDetailsBusinessResponseModel arg2) => arg2 = new PersonalDetailsBusinessResponseModel(src.FirstName, src.LastName,
                                                                              AutoMapper.Mapper.Map<DateTime>(src.DateOfBirth),
                                                                              AutoMapper.Mapper.Map<IEnumerable<Message>>(src.Messages),
                                                                              AutoMapper.Mapper.Map<IEnumerable<ResourceLink>>(src.Links),
                                                                              src.ProcessId, src.Status));
        }

        private void PassportDetailsModelMap()
        {
            CreateMap<PassportModel, PassportDocumentRequestModel>()
                .ForMember(dest => dest.GivenName, opt => opt.MapFrom(src => src.GivenName))
                .ForMember(dest => dest.FamilyName, opt => opt.MapFrom(src => src.FamilyName))
                .ForMember(dest => dest.DateOfBirth, opt => opt.MapFrom(src => src.BirthDate))
                .ForMember(dest => dest.Gender , opt => opt.MapFrom(src => src.Gender))
                .ForMember(dest => dest.ConsentProvided, opt => opt.MapFrom(src => src.ConsentProvided))
                .ForMember(dest => dest.PassportNumber, opt => opt.MapFrom(src => src.PassportNumber));

            CreateMap<PassportDocumentResponseModel, PassportDocumentBusinessResponseModel>()
                .ConvertUsing((PassportDocumentResponseModel src, PassportDocumentBusinessResponseModel arg2) => arg2 = new PassportDocumentBusinessResponseModel(
                                                                              AutoMapper.Mapper.Map<IEnumerable<Message>>(src.Messages),
                                                                              AutoMapper.Mapper.Map<IEnumerable<ResourceLink>>(src.Links),
                                                                              src.ProcessId, src.Status));
        }
        private void DriverLicenceDetailsModelMap()
        {
            CreateMap<DriverLicenceModel, DriverLicenceDocumentRequestModel>()
                .ForMember(dest => dest.DocumentNumber, opt => opt.MapFrom(src => src.DocumentNumber))
                .ForMember(dest => dest.GivenNames, opt => opt.MapFrom(src => src.GivenNames))
                .ForMember(dest => dest.MiddleName, opt => opt.MapFrom(src => src.MiddleName))
                .ForMember(dest => dest.FamilyName, opt => opt.MapFrom(src => src.FamilyName))
                .ForMember(dest => dest.DateOfBirth, opt => opt.MapFrom(src => src.BirthDate))
                .ForMember(dest => dest.State, opt => opt.MapFrom(src => src.State))
                .ForMember(dest => dest.ConsentProvided, opt => opt.MapFrom(src => src.ConsentProvided));

            CreateMap<DriverLicenceDocumentResponseModel, DriverLicenceDocumentBusinessResponseModel>()
                .ConvertUsing((DriverLicenceDocumentResponseModel src, DriverLicenceDocumentBusinessResponseModel arg2) => arg2 = new DriverLicenceDocumentBusinessResponseModel(
                                                                              AutoMapper.Mapper.Map<IEnumerable<Message>>(src.Messages),
                                                                              AutoMapper.Mapper.Map<IEnumerable<ResourceLink>>(src.Links),
                                                                              src.ProcessId, src.Status));
        }
        private void MedicareDetailsModelMap()
        {
            CreateMap<MedicareDetailsModel, MedicareDetailsRequestModel>()
                .ForMember(dest => dest.FullName1, opt => opt.MapFrom(src => src.NameLine1))
                .ForMember(dest => dest.FullName2, opt => opt.MapFrom(src => src.NameLine2))
                .ForMember(dest => dest.FullName3, opt => opt.MapFrom(src => src.NameLine3))
                .ForMember(dest => dest.FullName4, opt => opt.MapFrom(src => src.NameLine4))
                .ForMember(dest => dest.MedicareCardNumber, opt => opt.MapFrom(src => src.CardNumber))
                .ForMember(dest => dest.CardExpiry, opt => opt.MapFrom(src => src.CardExpiry))
                .ForMember(dest => dest.CardType, opt => opt.MapFrom(src => src.CardType))
                .ForMember(dest => dest.IndividualReferencenumber, opt => opt.MapFrom(src => src.IndividualReferenceNumber))
                .ForMember(dest => dest.ConsentProvided, opt => opt.MapFrom(src => src.ConsentProvided));

            // TODO: Double check if there are more fields we're interested in from the Medicare submission response
            CreateMap<MedicareDetailsResponseModel, MedicareDetailsBusinessResponseModel>()
                .ConvertUsing((MedicareDetailsResponseModel src, MedicareDetailsBusinessResponseModel arg2) => arg2 = new MedicareDetailsBusinessResponseModel(
                    AutoMapper.Mapper.Map<IEnumerable<Message>>(src.Messages),
                    AutoMapper.Mapper.Map<IEnumerable<ResourceLink>>(src.Links),
                    src.ProcessId, src.Status));
        }

        /// <summary>
        /// Creates the mapping between AppMessage (Network layer) class and Message (Business Layer) class.
        /// </summary>
        private void MessageMap()
        {
            CreateMap<AppMessage, Message>();
        }
        /// <summary>
        /// Creates the mapping between CaptureEmailModel (Business layer) class and EmailRequestModel (Services Layer) class.
        /// </summary>
        private void EmailModelMap()
        {
            CreateMap<CaptureEmailModel, EmailRequestModel>();
        }
        /// <summary>
        /// Creates the mapping between CaptureEmailVerificationModel (Business layer) class and EmailRequestModel (Services Layer) class.
        /// </summary>
        private void EmailVerificationModelMap()
        {
            CreateMap<CaptureEmailVerificationModel, EmailVerificationRequestModel>();
        }
        /// <summary>
        /// Creates the mapping between RegisterPOIModel (Business layer) class and RegisterPOIRequestModel (Services Layer) class.
        /// </summary>
        private void RegisterPOIModelMap()
        {
            CreateMap<StartPoiModel, RegisterPOIRequestModel>();
        }
        /// <summary>
        /// Creates the mapping between Alert (Business layer) class and AppAlert (Services Layer) class.
        /// </summary>
        private void AlertMap()
        {
            CreateMap<Alert, AppAlert>();
        }
        private void TermsAndConditionsMap()
        {
            CreateMap<Services.Api.Contract.Model.Poi.TermsAndConditions.TermsAndConditionsResponseModel, TermsAndConditionsResponseModel>()
            .ConvertUsing((Services.Api.Contract.Model.Poi.TermsAndConditions.TermsAndConditionsResponseModel src, TermsAndConditionsResponseModel arg2) => arg2 = new TermsAndConditionsResponseModel(null, src?.TermsAndConditions?.Url, src?.TermsAndConditions?.Version));
        }
        private void GetPoiModelMap()
        {
            CreateMap<myGovID.Services.Api.Contract.Model.Poi.GetPoi.GetPoiResponseModel, GetPoiBusinessResponseModel>()
                .ConvertUsing((myGovID.Services.Api.Contract.Model.Poi.GetPoi.GetPoiResponseModel src, GetPoiBusinessResponseModel arg2) => arg2 = new GetPoiBusinessResponseModel(src.AcceptedTermsAndConditionsVersion, src.Strength,
                                                                  AutoMapper.Mapper.Map<IEnumerable<Message>>(src.Messages),
                                                                  AutoMapper.Mapper.Map<IEnumerable<ResourceLink>>(src.Links),
                                                                  src.ProcessId, src.Status));
        }
    }
}