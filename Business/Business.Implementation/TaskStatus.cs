﻿using System;
namespace myGovID.Business
{
    /// <summary>
    /// Status of the asynchronous task.
    /// </summary>
    public enum TaskStatus
    {
        Submitted,
        Pending,
        TechnicalError,
        Failed,
        Timeout,
        Verified
    }
}
