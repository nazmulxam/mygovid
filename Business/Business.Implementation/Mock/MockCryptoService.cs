﻿using myGovID.Services.Crypto.Contract;
using myGovID.Services.Crypto.Contract.Model;

namespace myGovID.Business.Mock
{
    public class MockCryptoService : ICryptoService
    {
        public void ChangePassword(ChangePasswordRequest operation)
        {
            // Do nothing, incase of a failure raise an exception.
        }
        public void CheckPassword(CheckPasswordRequest operation)
        {
            // Do nothing. incase of a wrong password an exception is thrown.
        }
        public CreateResult CreateRequest(CreateRequest operation)
        {
            return new CreateResult("MIIBpjCCAQ8CAQAwRDEUMBIGA1UEAwwLbmFtZTEgbmFtZTIxETAPBgNVBC4TCHBlcnNvbklkMQwwCgYDVQQKDANhYm4xCzAJBgNVBAYTAkFVMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCH+AOJ6tUjSxVW8S0V1IoXayLLBtH9VqdWtl/ZAQB/j35bmyRrkWPvEiNBQ+88mKcoSUgxk9HTgAqZWsiRK7NnqQzH89tw/mA4r/dMPkVgcqAFRbvV2W0/W5AFOy1112bQqsvZGz/AK1lry7LeCyCh/XdiyPHtcozkksGVFKECgQIDAQABoCIwIAYJKoZIhvcNAQkOMRMwETAPBgYqJAGCTQEEBRYDYWJuMA0GCSqGSIb3DQEBBQUAA4GBAGOD3rOCex++lJgabveAu4cOJTQawgmW1liYpECxegg+Dg3LZqyPkK/KQedMFFTxnohmyKFZtegHbJ8UV5c+JkiF11M34ux07EBtCSyafENBxzk0+HS/kpdPLIWn4uf8QmbHmCb20BxZ37MaJXXpTCUgHNPhuI+qtqgGW8fH0u1H",
                                                         "requestID");
        }
        public void Delete(DeleteRequest operation)
        {
            // Do nothing. incase if delete failed an exception is thrown.
        }

        public GetCredentialResult GetCredential(GetCredentialRequest operation)
        {
            return new GetCredentialResult(new byte[] { }, new byte[] { });
        }

        public void InsertCredential(InsertCredentialRequest operation)
        {
            // Do nothing, incase if insert credential failed an exception is thrown.
        }
        public LoadResult Load(LoadRequest operation)
        {
            return new LoadResult("1.0");
        }
    }
}