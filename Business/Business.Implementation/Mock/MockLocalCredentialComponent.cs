﻿using System;
using myGovID.Business.Component;
using myGovID.Business.Contract;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Session;
using myGovID.Business.Contract.Component;
using myGovID.Business.Session;
using myGovID.Services.Event.Contract;
using myGovID.Services.Storage.Contract;

namespace myGovID.Business.Mock
{
    public class MockLocalCredentialComponent : LocalCredentialComponent
    {
        private IApplication _application;
        private IStorage _secureStorage;
        public MockLocalCredentialComponent(IApplication application, ICredentialComponent credentialComponent, ICryptoComponent cryptoComponent, ITokenStorageComponent tokenStorageComponent, IStorage secureStorage, IStorage fileStorage, IStorage memoryStorage, IEventService eventService, ISessionManager sessionManager)
            : base(application, credentialComponent, cryptoComponent, tokenStorageComponent, secureStorage, fileStorage, memoryStorage, eventService, sessionManager)
        {
            _application = application;
            _secureStorage = secureStorage;
        }

        public async void Setup(string credentialToken, string processId, string emailAddress, string userPassword, bool useBiometrics, string firstName, string lastName, string dateOfBirth)
        {
            string credentialId = TokenStorageComponent.GetTokenClaim(credentialToken, "urn:mygovid.gov.au:credentialId");
            _secureStorage.SetValue(StorageKey.CredentialId.ToString(), credentialId);
            _secureStorage.SetValue(StorageKey.CredentialToken.ToString(), credentialToken);
            _application.CurrentSession.SessionProcess.ProcessId = processId;
            RegisterUser(emailAddress, userPassword, useBiometrics);
            UpdateIdentity(firstName, lastName, dateOfBirth);
        }
    }
}