﻿using System.Collections.Generic;
using myGovID.Services.Storage.Contract;

namespace myGovID.Business.Mock
{
    public class MockStorage : IStorage
    {
        private readonly Dictionary<string, string> _dictionary;

        public MockStorage()
        {
            _dictionary = new Dictionary<string, string>();
        }

        public void DeleteKey(string key)
        {
            _dictionary.Remove(key);
        }

        public string GetValue(string key)
        {
            if (_dictionary.TryGetValue(key, out string value))
            {
                return value;
            }
            return null;
        }

        public bool HasKey(string key)
        {
            return _dictionary.ContainsKey(key);
        }

        public void SetValue(string key, string value)
        {
            if (HasKey(key))
            {
                _dictionary.Remove(key);
            }
            _dictionary.Add(key, value);
        }

        public void Clear()
        {
            foreach (var key in _dictionary.Keys)
            {
                _dictionary.Remove(key);
            }
        }
    }
}