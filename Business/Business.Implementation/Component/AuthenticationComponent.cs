﻿using System.Threading.Tasks;
using myGovID.Business.Contract.Common.Session;
using myGovID.Business.Contract.Component;
using myGovID.Business.Session;
using myGovID.Services.Event.Contract;
using myGovID.Services.Storage.Contract;

namespace myGovID.Business.Component
{
    public class AuthenticationComponent: IAuthenticationComponent
    {
        private readonly IStorage _secureStorage;
        private readonly IEventService _eventService;
        private readonly ICryptoComponent _cryptoComponent;
        private readonly IAuthenticationProvider _openIdAuthenticationProvider;
        private readonly IAuthenticationProvider _selfAssertedAuthenticationProvider;

        public AuthenticationComponent(IStorage secureStorage,
                                        IEventService eventService,
                                        IAuthenticationProvider openIdAuthenticationProvider,
                                        IAuthenticationProvider selfAssertedAuthenticationProvider,
                                        ICryptoComponent cryptoComponent)
        {
            _secureStorage = secureStorage;
            _eventService = eventService;
            _openIdAuthenticationProvider = openIdAuthenticationProvider;
            _selfAssertedAuthenticationProvider = selfAssertedAuthenticationProvider;
            _cryptoComponent = cryptoComponent;
        }
        public async Task<IAuthenticationContext> LoginUsingKeystoreAsync()
        {
            ICredential credential = new KeystoreCredential(_cryptoComponent);
            IMyGovPrincipal identityResponse = await _openIdAuthenticationProvider.GetIdentity(credential);
            if (identityResponse != null)
            {
                return new AuthenticationContext(_secureStorage, _openIdAuthenticationProvider.AuthenticatedState, identityResponse, credential, _eventService);
            }
            return null;
        }
        public async Task<IAuthenticationContext> LoginUsingPasswordAsync(string password)
        {
            ICredential credential = new SelfAssertedCredential(password, "emailAddress"); // TODO: Consider updating this to the actual email
            IMyGovPrincipal identityResponse = await _selfAssertedAuthenticationProvider.GetIdentity(credential);
            if (identityResponse != null)
            {
                return new AuthenticationContext(_secureStorage, _selfAssertedAuthenticationProvider.AuthenticatedState, identityResponse, credential, _eventService);
            }
            return null;
        }
    }
}