using System.Threading.Tasks;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Component;
using myGovID.Business.Contract.Model;
using myGovID.Business.Contract;
using myGovID.Business.Specification.Link.Identity;
using myGovID.Business.Specification.Link;
using myGovID.Services.Api.Contract.Model.Poi.MedicareDetails;
using myGovID.Services.Api.Contract.Model.Poi.PersonalDetails;
using myGovID.Services.Api.Contract.Model.Poi.Passport;
using myGovID.Business.Session;
using myGovID.Business.Contract.Common.Session;
using System.Linq;
using myGovID.Services.Api.Contract.Model;
using myGovID.Services.Api.Contract;
using myGovID.Services.Uri.Contract;
using AuthenticationType = myGovID.Services.Api.Contract.Model.AuthenticationType;
using myGovID.Services.Api.Contract.Model.Poi.DriverLicenceDetails;
using myGovID.Services.Api.Contract.Model.Poi.DriverLicence;
using myGovID.Services.Log.Contract;

namespace myGovID.Business.Component
{
    public class DocumentComponent : BaseApiComponent, IDocumentComponent
    {
        private readonly IUriService _uriService;
        private readonly ITokenServiceComponent _tokenServiceComponent;
        private readonly ILocalCredentialComponent _localCredentialComponent;
        public DocumentComponent(ILogger logger, IApiService apiService, IUriService uriService,
                                 ITokenServiceComponent tokenServiceComponent,
                                 ILocalCredentialComponent localCredentialComponent,
                                 IApplication application) : base(logger, apiService, application)
        {
            _uriService = uriService;
            _tokenServiceComponent = tokenServiceComponent;
            _localCredentialComponent = localCredentialComponent;
        }
        public async Task<PersonalDetailsBusinessResponseModel> CreatePersonalDetailsAsync(
                        CapturePersonalDetailsModel model)
        {
            ResourceLink resourceLinkResponse = FindLink(new PersonalDetailsLinkSpecification(_uriService));
            Link link = AutoMapper.Mapper.Map<Link>(resourceLinkResponse);
            PersonalDetailsRequestModel requestModel = AutoMapper.Mapper.Map<PersonalDetailsRequestModel>(model);
            BusinessTokenType tokenType = GetBusinessToken(link);
            PersonalDetailsBusinessResponseModel response = await SendAsync<PersonalDetailsRequestModel, PersonalDetailsBusinessResponseModel, PersonalDetailsResponseModel>
                (link, requestModel, null, await _tokenServiceComponent.CreateAsync(tokenType));
            return response;
        }
        public async Task<PassportDocumentBusinessResponseModel> CreatePassportDetailsAsync(PassportModel passportModel)
        {
            ResourceLink resourceLinkResponse = FindLink(new PassportLinkSpecification(_uriService));
            Link link = AutoMapper.Mapper.Map<Link>(resourceLinkResponse);
            PassportDocumentRequestModel requestModel = AutoMapper.Mapper.Map<PassportDocumentRequestModel>(passportModel);
            BusinessTokenType tokenType = GetBusinessToken(link);
            return await SendPoiAsync<PassportDocumentRequestModel, PassportDocumentBusinessResponseModel, PassportDocumentResponseModel>
                (link, requestModel, null, await _tokenServiceComponent.CreateAsync(tokenType));
        }
        public async Task<MedicareDetailsBusinessResponseModel> CreateMedicareDetailsAsync(
                                MedicareDetailsModel model)
        {
            ResourceLink resourceLinkResponse = FindLink(new MedicareLinkSpecification(_uriService));
            Link link = AutoMapper.Mapper.Map<Link>(resourceLinkResponse);
            MedicareDetailsRequestModel requestModel = AutoMapper.Mapper.Map<MedicareDetailsRequestModel>(model);
            BusinessTokenType tokenType = GetBusinessToken(link);
            return await SendPoiAsync<MedicareDetailsRequestModel, MedicareDetailsBusinessResponseModel, MedicareDetailsResponseModel>
                (link, requestModel, null, await _tokenServiceComponent.CreateAsync(tokenType));
        }
        public async Task<DriverLicenceDocumentBusinessResponseModel> CreateDriverLicenceDetailsAsync(
                DriverLicenceModel driverLicenceModel)
        {
            ResourceLink resourceLinkResponse = FindLink(new DriverLicenceLinkSpecification(_uriService));
            Link link = AutoMapper.Mapper.Map<Link>(resourceLinkResponse);
            DriverLicenceDocumentRequestModel requestModel = AutoMapper.Mapper.Map<DriverLicenceDocumentRequestModel>(driverLicenceModel);
            BusinessTokenType tokenType = GetBusinessToken(link);
            return await SendPoiAsync<DriverLicenceDocumentRequestModel, DriverLicenceDocumentBusinessResponseModel, DriverLicenceDocumentResponseModel>
                (link, requestModel, null, await _tokenServiceComponent.CreateAsync(tokenType));
        }
        private BusinessTokenType GetBusinessToken(Link link)
        {
            return link.AuthType == AuthenticationType.Extension
                ? BusinessTokenType.ExtensionGrant
                : BusinessTokenType.Credential;
        }
    }
}
