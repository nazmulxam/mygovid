﻿using System;
using Microsoft.IdentityModel.Tokens;

namespace myGovID.Business.Component.Token
{
    public class CustomSignatureProviderFactory : CryptoProviderFactory
    {
        public override SignatureProvider CreateForSigning(SecurityKey key, string algorithm)
        {
            return (key is AsymmetricSecurityKey asymmetricKey)
                ? new CustomAsymmetricSignatureProvider(asymmetricKey, algorithm, true)
                : base.CreateForSigning(key, algorithm);
        }

        public override SignatureProvider CreateForVerifying(SecurityKey key, string algorithm)
        {
            return (key is AsymmetricSecurityKey asymmetricKey)
                ? new CustomAsymmetricSignatureProvider(asymmetricKey, algorithm, false)
                : base.CreateForVerifying(key, algorithm);
        }
    }
}
