﻿using System.Threading.Tasks;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Component;
using myGovID.Business.Contract.Model;
using myGovID.Services.Api.Contract;
using myGovID.Services.Api.Contract.Model;
using myGovID.Services.Log.Contract;

namespace myGovID.Business.Component
{
    public class TermsAndConditionsComponent : BaseApiComponent, ITermsAndConditionsComponent
    {
        public TermsAndConditionsComponent(ILogger logger, IApiService apiService, IApplication application) : base(logger, apiService, application)
        { }
        public async Task<TermsAndConditionsResponseModel> GetTermsAndConditionsAsync()
        {
            Link link = new Link
            {
                Location = "/api/v1/termsAndConditions",
                Method = "get",
                AuthType = AuthenticationType.None,
                TargetType = TargetType.Next
            };
            return await SendAsync<IApiRequestModel, TermsAndConditionsResponseModel,
                Services.Api.Contract.Model.Poi.TermsAndConditions.TermsAndConditionsResponseModel>(link, null);
        }
    }
}