using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using myGovID.Business.Contract;
using myGovID.Business.Contract.Common.Session;
using myGovID.Business.Contract.Component;
using myGovID.Business.Session;
using myGovID.Services.Event.Contract;
using myGovID.Services.Storage.Contract;
using myGovID.Services.Http.Contract;

namespace myGovID.Business.Component
{
    public class TokenServiceComponent : BaseTokenComponent, ITokenServiceComponent
    {
        private readonly ISession _session;
        private readonly ICryptoComponent _cryptoComponent;
        private readonly IEventService _eventService;
        private readonly ITokenStorageComponent _tokenStorageComponent;
        private readonly ISessionManager _sessionManager;

        public TokenServiceComponent(ISession session,
                                     ICryptoComponent cryptoComponent,
                                     IEventService eventService,
                                     ITokenStorageComponent tokenStorageComponent,
                                     ISessionManager sessionManager)
        {
            _session = session;
            _cryptoComponent = cryptoComponent;
            _eventService = eventService;
            _tokenStorageComponent = tokenStorageComponent;
            _sessionManager = sessionManager;
        }

        /// <summary>
        /// Get the value of specified tokenType.
        /// </summary>
        /// <returns>The value of the specified token type</returns>
        /// <param name="tokenType">Token type.</param>
        public async Task<string> CreateAsync(BusinessTokenType tokenType)
        {
            switch (tokenType)
            {
                case BusinessTokenType.ExtensionGrant:
                    var existingToken = GetFromStorage(tokenType);

                    if (string.IsNullOrEmpty(existingToken) || IsExpiring(existingToken))
                    {
                        // reauthenticate with the current credential
                        var loggedIn = await _sessionManager.LoginUsingKeystoreAsync();
                        // TODO: what if it is not logged in? - throw error (shouldn't happen)
                        return GetFromStorage(tokenType);
                    }
                    return existingToken;
                case BusinessTokenType.Bespoke:
                    // TODO: make sure we are logged in - this means we can't log until we have an identity - fix in future
                    // we shouldn't depend on the keystore credential in future
                    return CreateBespokeToken(_session.AuthenticatedContext.CurrentCredential as KeystoreCredential);
                case BusinessTokenType.Credential:
                    existingToken = GetFromStorage(tokenType);
                    if (string.IsNullOrEmpty(existingToken) || HasExpired(existingToken))
                    {
                        throw new BusinessException(BusinessExceptionCode.CLR000001);
                    }
                    return existingToken;
            }
            return GetFromStorage(tokenType);
        }

        public void Clear()
        {
        }

        private string GetFromStorage(BusinessTokenType type)
        {
            switch (type)
            {
                case BusinessTokenType.ExtensionGrant:
                case BusinessTokenType.Credential:
                    return _tokenStorageComponent.Get(type);
                default: return null;
            }
        }

        private string CreateBespokeToken(KeystoreCredential credential)
        {
            return GetStringToken(new JwtSecurityToken(
                issuer: GetClientId(credential.CredentialId),
                audience: "https://mygovid.gov.au/ss",
                claims: new List<Claim>(new[] {
                new Claim(JwtRegisteredClaimNames.Jti, new Guid().ToString()),
                new Claim(JwtRegisteredClaimNames.Sub, GetClientId(credential.CredentialId))}),
                notBefore: DateTime.Now,
                expires: DateTime.Now.AddHours(1),
                credential.CreateSigningCredentials()));
        }
    }
}