using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using myGovID.Business.Contract;
using myGovID.Business.Contract.Common.Session;
using myGovID.Business.Contract.Component;
using myGovID.Business.Session;
using myGovID.Services.Event.Contract;
using myGovID.Services.Storage.Contract;
using myGovID.Services.Http.Contract;
using System.Linq;

namespace myGovID.Business.Component
{
    public class TokenStorageComponent : BaseTokenComponent, ITokenStorageComponent
    {
        private readonly ISession _session;
        private readonly ICryptoComponent _cryptoComponent;
        private readonly IEventService _eventService;
        private readonly IStorage _secureStorage;
        private readonly IStorage _memoryStorage;
        private readonly IOpenIdComponent _openIdComponent;

        public TokenStorageComponent(IHttpConfig config,
                                     ISession session,
                                     ICryptoComponent cryptoComponent,
                                     IEventService eventService,
                                     IStorage secureStorage,
                                     IStorage memoryStorage,
                                     IOpenIdComponent openIdComponent)
        {
            _session = session;
            _cryptoComponent = cryptoComponent;
            _eventService = eventService;
            _secureStorage = secureStorage;
            _memoryStorage = memoryStorage;
            _openIdComponent = openIdComponent;
        }

        /// <summary>
        /// Get the value of specified tokenType.
        /// </summary>
        /// <returns>The value of the specified token type</returns>
        /// <param name="tokenType">Token type.</param>
        public string Get(BusinessTokenType tokenType)
        {
            switch (tokenType)
            {
                case BusinessTokenType.Bespoke:
                    return CreateBespokeToken(_session.AuthenticatedContext.CurrentCredential as KeystoreCredential);
                case BusinessTokenType.Credential:
                    var existingToken = GetFromStorage(tokenType);
                    if (string.IsNullOrEmpty(existingToken) || HasExpired(existingToken))
                    {
                        throw new BusinessException(BusinessExceptionCode.CLR000001);
                    }
                    return existingToken;
            }
            return GetFromStorage(tokenType);
        }
        public void Set(BusinessTokenType tokenType, string value)
        {
            switch (tokenType)
            {
                case BusinessTokenType.Credential:
                    _secureStorage.SetValue(StorageKey.CredentialToken.ToString(), value);
                    break;
                case BusinessTokenType.ExtensionGrant: _memoryStorage.SetValue(StorageKey.ExtensionGrant.ToString(), value); break;
            }
        }

        public void Clear()
        {
            _memoryStorage.DeleteKey(StorageKey.ExtensionGrant.ToString());
            _secureStorage.DeleteKey(StorageKey.CredentialToken.ToString());
        }

        private string GetFromStorage(BusinessTokenType type)
        {
            switch (type)
            {
                case BusinessTokenType.ExtensionGrant:
                    string key = StorageKey.ExtensionGrant.ToString();
                    return _memoryStorage.HasKey(key) ? _memoryStorage.GetValue(key) : null;
                case BusinessTokenType.Credential:
                    return _secureStorage.GetValue(StorageKey.CredentialToken.ToString());
                default: return null;
            }
        }

        public string GetTokenClaim(string token, string claimName)
        {
            JwtSecurityTokenHandler jwtHandler = new JwtSecurityTokenHandler();
            if (jwtHandler.CanReadToken(token))
            {
                JwtSecurityToken jwttoken = jwtHandler.ReadJwtToken(token);
                return jwttoken.Claims.FirstOrDefault((arg) => arg.Type.Equals(claimName))?.Value;
            }
            return null;
        }

        private string CreateBespokeToken(KeystoreCredential credential)
        {
            return GetStringToken(new JwtSecurityToken(
                issuer: GetClientId(credential.CredentialId),
                audience: "https://mygovid.gov.au/ss",
                claims: new List<Claim>(new[] {
                new Claim(JwtRegisteredClaimNames.Jti, new Guid().ToString()),
                new Claim(JwtRegisteredClaimNames.Sub, GetClientId(credential.CredentialId))}),
                notBefore: DateTime.Now,
                expires: DateTime.Now.AddHours(1),
                credential.CreateSigningCredentials()));
        }
    }
}