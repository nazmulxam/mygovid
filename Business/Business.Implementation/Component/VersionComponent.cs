﻿using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Component;
using myGovID.Services.Api.Contract;
using myGovID.Services.Api.Contract.Model;
using myGovID.Business.Contract.Model;
using System.Linq;
using System.Threading.Tasks;
using myGovID.Services.Log.Contract;

namespace myGovID.Business.Component
{
    public class VersionComponent : BaseApiComponent, IVersionComponent
    {
        public VersionComponent(ILogger logger, IApiService apiService, IApplication application) : base(logger, apiService, application)
        { }
        private VersionResponseModel _lastRetrievedVersionResponse;
        public async Task<VersionResponseModel> GetConfigurationAsync()
        {
            if (_lastRetrievedVersionResponse != null)
            {
                return _lastRetrievedVersionResponse;
            }
            IDevice device = Application.Device;
            IApplicationMetaData applicationMetaData = Application.ApplicationMetaData;
            string location = string.Format("/api/v1/system/metadata/{0}/{1}/{2}/{3}/{4}",
                                            device.PlatformType,
                                            device.RuntimeType,
                                            device.RuntimeVersion,
                                            device.FormFactorType,
                                            applicationMetaData.Version);
            Link link = new Link
            {
                Location = location,
                Method = "get",
                AuthType = AuthenticationType.None,
                TargetType = TargetType.Next
            };
            VersionResponseModel result = await SendAsync<IApiRequestModel, VersionResponseModel,
                    Services.Api.Contract.Model.System.Version.VersionResponseModel>(link, null);
            _lastRetrievedVersionResponse = result;
            Application.Alerts = result.Alerts;
            Application.Capabilities = result.Capabilities;
            Application.UpgradeLink = result.Links.FirstOrDefault((resLink) => resLink.Target == Contract.Common.Process.TargetType.Update);
            return result;
        }

        public void DismissUpgrade()
        {
            Application.UpgradeActioned = true;
        }
    }
}