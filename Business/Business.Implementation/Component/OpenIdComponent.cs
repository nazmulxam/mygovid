﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using myGovID.Business.Component.Token;
using myGovID.Business.Contract;
using myGovID.Business.Contract.Common.Session;
using myGovID.Business.Contract.Component;
using myGovID.Business.Contract.Model;
using myGovID.Business.Session;
using myGovID.Services.Http.Contract;
using myGovID.Services.OpenId.Contract;

namespace myGovID.Business.Component
{
    public class OpenIdComponent : BaseTokenComponent, IOpenIdComponent
    {
        private readonly IHttpConfig _config;
        private readonly IOpenIdService _openIdService;

        public OpenIdComponent(IHttpConfig httpConfig, IOpenIdService openIdService)
        {
            _openIdService = openIdService;
            _config = httpConfig;
        }

        public async Task<OpenIdResponseModel> AuthenticateAsync(ICredential credential, string userIdentity)
        {
            if (!(credential is KeystoreCredential keystoreCredential))
            {
                throw new ArgumentException("Unable to perform a token authentication with an unsupported credential type: {0}", credential.GetType().Name);
            }

            string deviceToken = CreateExtensionGrantAssertionDeviceToken(keystoreCredential);
            string userToken = CreateExtensionGrantAssertionUserToken(keystoreCredential, userIdentity);

            var request = new ExtensionGrantRequest(userToken, deviceToken);
            var response = await _openIdService.AuthenticateAsync(request);

            if (response.Response.IdentityToken != null)
            {
                return new OpenIdResponseModel(
                    await ValidateTokenAsync(response.Response.IdentityToken, credential.CredentialId),
                    response.Response.AccessToken);
            }
            else
            {
                return new OpenIdResponseModel(response.Response.AccessToken);
            }
        }

        private string CreateExtensionGrantAssertionUserToken(KeystoreCredential credential, string userIdentity)
        {
            return GetStringToken(new JwtSecurityToken(
                issuer: GetClientId(credential.CredentialId),
                audience: "https://myGovId.gov.au/connect/token",
                claims: new List<Claim>(new[] {
                new Claim(JwtRegisteredClaimNames.Jti, new Guid().ToString()),
                new Claim(JwtRegisteredClaimNames.Sub, userIdentity)}),
                notBefore: DateTime.Now,
                expires: DateTime.Now.AddHours(1),
                credential.CreateSigningCredentials()));
        }

        private string CreateExtensionGrantAssertionDeviceToken(KeystoreCredential credential)
        {
            return GetStringToken(new JwtSecurityToken(
                 issuer: GetClientId(credential.CredentialId),
                 audience: "https://myGovId.gov.au/connect/token",
                 claims: new List<Claim>(new[] {
                new Claim(JwtRegisteredClaimNames.Jti, new Guid().ToString()),
                new Claim(JwtRegisteredClaimNames.Sub, GetClientId(credential.CredentialId))}),
                 notBefore: DateTime.Now,
                 expires: DateTime.Now.AddHours(1),
                 credential.CreateSigningCredentials()));
        }

        private async Task<ClaimsPrincipal> ValidateTokenAsync(string jwt, string credentialId)
        {
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            try
            {
                var principal = handler.ValidateToken(jwt, await GetTokenValidationParameters(credentialId), out SecurityToken validatedToken);
                return principal;
            }
            catch (SecurityTokenException ex)
            {
                throw new BusinessException(BusinessExceptionCode.OIT000001, ex);
            }
        }
        protected virtual async Task<TokenValidationParameters> GetTokenValidationParameters(string credentialId)
        {
            string auth0Domain = $"{_config.BaseUrl}/core/.well-known/openid-configuration";
            IConfigurationManager<OpenIdConnectConfiguration> configurationManager = new ConfigurationManager<OpenIdConnectConfiguration>($"{auth0Domain}", new OpenIdConnectConfigurationRetriever());
            OpenIdConnectConfiguration openIdConfig = await configurationManager.GetConfigurationAsync(CancellationToken.None);
            TokenValidationParameters tokenValidationParameters =
                new TokenValidationParameters
                {
                    //TODO add any additional or remove parameters we want to validate.
                    ValidIssuer = openIdConfig.Issuer,
                    ValidAudiences = new[] { GetClientId(credentialId) },
                    IssuerSigningKeys = openIdConfig.SigningKeys,
                    SignatureValidator = HandleSignatureValidator, //TODO: Remove this once we get mono verification working
                    ValidateLifetime = false, //TODO: Remove this
                    CryptoProviderFactory = new CustomSignatureProviderFactory()
                };
            return tokenValidationParameters;
        }
        protected SecurityToken HandleSignatureValidator(string token, TokenValidationParameters validationParameters)
        {
            return new JwtSecurityToken(token);
        }

    }
}
