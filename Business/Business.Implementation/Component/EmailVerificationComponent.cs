﻿using myGovID.Business.Contract;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Component;
using myGovID.Business.Contract.Model;
using myGovID.Services.Api.Contract;
using myGovID.Services.Api.Contract.Model;
using myGovID.Services.Api.Contract.Model.Poi.EmailVerification;
using myGovID.Business.Specification.Link;
using myGovID.Services.Uri.Contract;
using myGovID.Business.Contract.Common.Process;
using System.Threading;
using System.Threading.Tasks;
using myGovID.Services.Storage.Contract;
using System.Linq;
using myGovID.Services.Log.Contract;

namespace myGovID.Business.Component
{
    public class EmailVerificationComponent : BaseApiComponent, IEmailVerificationComponent
    {
        private readonly IUriService _uriService;
        public EmailVerificationComponent(ILogger logger, IApiService apiService, IUriService uriService, IApplication application) : base(logger, apiService, application)
        {
            _uriService = uriService;
        }
        public async Task<PoiResponseModel> ChangeEmailAsync()
        {
            ResourceLink resourceLinkResponse = FindLink(new ChangeEmailLinkSpecification(_uriService));
            Link link = AutoMapper.Mapper.Map<Link>(resourceLinkResponse);
            return await SendAsync<IApiRequestModel, PoiResponseModel, Services.Api.Contract.Model.Poi.PoiResponseModel>(link, null);
        }
        public async Task<PoiResponseModel> ResendEmailVerificationCodeAsync()
        {
            ResourceLink resourceLinkResponse = FindLink(new ResendEmailVerificationLinkSpecification(_uriService));
            Link link = AutoMapper.Mapper.Map<Link>(resourceLinkResponse);
            return await SendAsync<IApiRequestModel, PoiResponseModel, Services.Api.Contract.Model.Poi.PoiResponseModel>(link, null);
        }
        public async Task<Contract.Model.EmailVerificationResponseModel> SubmitEmailVerificationAsync(CaptureEmailVerificationModel model)
        {
            ResourceLink resourceLinkResponse = FindLink(new EmailVerificationLinkSpecification(_uriService));
            Link link = AutoMapper.Mapper.Map<Link>(resourceLinkResponse);
            EmailVerificationRequestModel requestModel = AutoMapper.Mapper.Map<EmailVerificationRequestModel>(model);
            return await SendAsync<EmailVerificationRequestModel, Contract.Model.EmailVerificationResponseModel,
                Services.Api.Contract.Model.Poi.EmailVerification.EmailVerificationResponseModel>(link, requestModel);
        }
    }
}