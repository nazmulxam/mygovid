﻿using System.Threading;
using System.Threading.Tasks;
using myGovID.Business.Contract;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Component;
using myGovID.Business.Contract.Model;
using myGovID.Services.Api.Contract;
using myGovID.Services.Api.Contract.Model;
using myGovID.Services.Crypto.Contract;
using myGovID.Services.Log.Contract;
using myGovID.Services.Uri.Contract;

namespace myGovID.Business.Component
{
    public class CredentialComponent : BaseApiComponent, ICredentialComponent
    {
        private readonly IUriService _uriService;
        private readonly ICryptoService _cryptoService;

        public CredentialComponent(ILogger logger, ICryptoService cryptoService, IApiService apiService, IUriService uriService, IApplication application) :
                                    base(logger, apiService, application)
        {
            _uriService = uriService;
            _cryptoService = cryptoService;
        }

        public async Task<CredentialResponseModel> CreateCredentialAsync(string token, string p10)
        {
            CredentialRequestModel credRequestModel = new CredentialRequestModel(p10);
            var apiCredRequestModel = AutoMapper.Mapper.Map<Services.Api.Contract.Model.Credential.CredentialRequestModel>(credRequestModel);
            return await SendPoiAsync<IApiRequestModel, CredentialResponseModel, Services.Api.Contract.Model.Credential.CredentialResponseModel>
                                                                             (CredentialLink(), apiCredRequestModel, null, token);
        }
        private Link CredentialLink()
        {
            return new Link
            {
                Location = "/api/v1/credentials/x509s",
                Method = "post",
                AuthType = AuthenticationType.Assurance,
                TargetType = TargetType.Next
            };
        }
    }
}
