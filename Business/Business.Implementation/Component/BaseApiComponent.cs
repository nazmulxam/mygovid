﻿using System.Collections.Generic;
using myGovID.Services.Api.Contract;
using myGovID.Business.Contract.Common.Application;
using myGovID.Services.Api.Contract.Model;
using myGovID.Business.Contract.Model;
using System.Linq;
using myGovID.Business.Contract.Common.Process;
using System;
using myGovID.Business.Contract;
using System.Threading;
using Newtonsoft.Json;
using myGovID.Services.Api.Contract.Model.Task;
using myGovID.Business.Specification.Link;
using System.Threading.Tasks;
using myGovID.Services.Log.Contract;

namespace myGovID.Business.Component
{
    public class BaseApiComponent
    {
        protected readonly ILogger Logger;
        private IApiService _apiService;
        protected IApplication Application { get; }
        public BaseApiComponent(ILogger logger, IApiService apiService, IApplication application)
        {
            Logger = logger;
            _apiService = apiService ?? throw new ArgumentNullException(nameof(apiService));
            Application = application ?? throw new ArgumentNullException(nameof(application));
        }
        private async Task<ApiResult<T>> SendRequest<I, T>(Link link,
                                          I request,
                                          Dictionary<string, string> headers = null,
                                          string token = null) where I : class, IApiRequestModel where T : class
        {
            IApiRequest<I> apiRequest = new ApiHttpRequest<I>(link, request, Application.CurrentSession.SessionIdentity, token, headers);
            Logger.Debug(message: apiRequest.ToString());
            return await InternalSendRequest<I, T>(apiRequest);
        }
        private async Task<ApiResult<T>> SendRequestWithNoBody<T>(Link link,
                                          Dictionary<string, string> headers = null,
                                          string token = null) where T : class
        {
            IApiRequest<IApiRequestModel> apiRequest = new ApiHttpRequestWithNoBody(link, Application.CurrentSession.SessionIdentity, token, headers);
            Logger.Debug(message: apiRequest.ToString());
            return await InternalSendRequest<IApiRequestModel, T>(apiRequest);
        }
        private async Task<ApiResult<T>> InternalSendRequest<I, T>(IApiRequest<I> apiRequest) where I : class, IApiRequestModel where T : class
        {
            // TODO: bring the token out for cancellable tasks.
            ApiResult<T> result = await _apiService.SendAsync<I, T>(apiRequest);
            Logger.Debug(message: result.ToString());
            return result;
        }
        /// <summary>
        /// Send the specified link, request, headers and token.
        /// </summary>
        /// <returns>The send.</returns>
        /// <param name="link">Link.</param>
        /// <param name="request">Request.</param>
        /// <param name="headers">Headers.</param>
        /// <param name="token">Token.</param>
        /// <typeparam name="I">The 1st type parameter.</typeparam>
        /// <typeparam name="T">The 2nd type parameter.</typeparam>
        /// <typeparam name="E">The 3rd type parameter.</typeparam>
        public async Task<T> SendAsync<I, T, E>(Link link, I request,
                                     Dictionary<string, string> headers = null,
                                     string token = null) where I : class, IApiRequestModel
                                                    where T : class, IBusinessApiResponseModel
                                                    where E : class, IResponseModel
        {
            ApiResult<E> result = await SendRequest<I, E>(link, request, headers, token);
            return ProcessApiResponse<T, E>(result);
        }

        /// <summary>
        /// Sends the async request
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="link">Link.</param>
        /// <param name="request">Request.</param>
        /// <param name="headers">Headers.</param>
        /// <param name="token">Token.</param>
        /// <typeparam name="I">The 1st type parameter.</typeparam>
        /// <typeparam name="T">The 2nd type parameter.</typeparam>
        /// <typeparam name="E">The 3rd type parameter.</typeparam>
        public async Task<T> SendPoiAsync<I, T, E>(Link link,
                                          I request,
                                          Dictionary<string, string> headers = null,
                                          string token = null)
                                                    where I : class, IApiRequestModel
                                                    where T : class, IBusinessApiResponseModel
                                                    where E : class, IResponseModel
        {
            ApiResult<TaskResponseModel> result = await SendRequest<I, TaskResponseModel>(link, request, headers, token);
            BusinessTaskResponseModel businessTaskResponse = ProcessApiResponse<BusinessTaskResponseModel, TaskResponseModel>(result);

            ResourceLink taskLink = null;
            if (businessTaskResponse.Links != null)
            {
                taskLink = businessTaskResponse.Links.FirstOrDefault(t => t.Target.Equals(Contract.Common.Process.TargetType.Self));
            }

            if (taskLink == null)
            {
                throw new LinkNotFoundException();
            }

            return await ProcessTaskResponseAsync<T, E>(businessTaskResponse, taskLink, headers, token);
        }

        /// <summary>
        /// Finds the link.
        /// </summary>
        /// <returns>The link.</returns>
        /// <param name="specification">Specification.</param>
        protected ResourceLink FindLink(LinkSpecification specification)
        {
            ResourceLink link = specification.FindLink(Application is IProcessInformation ? (IProcessInformation)Application :
                                                       new ProcessInformation(
                                                            Application.CurrentSession.SessionProcess,
                                                            Application.CurrentSession.AuthenticatedContext.AuthenticatedState,
                                                            Application.Capabilities,
                                                            Application.UpgradeLink,
                                                            Application.UpgradeActioned,
                                                            Application.Device.Capabilities));
            return link ?? throw new LinkNotFoundException();
        }
        private async Task<T> ProcessTaskResponseAsync<T, E>(BusinessTaskResponseModel taskResponseModel,
                                               ResourceLink taskLink,
                                               Dictionary<string, string> headers = null,
                                               string token = null)
                                                    where T : class, IBusinessApiResponseModel
                                                    where E : class, IResponseModel
        {
            switch (taskResponseModel.Status)
            {
                case TaskStatus.Pending:
                case TaskStatus.Submitted:
                    //sleep for ETA seconds
                    Thread.Sleep(taskResponseModel.Eta * 1000);
                    ApiResult<String> nextTaskResult = await SendRequestWithNoBody<String>(AutoMapper.Mapper.Map<Link>(taskLink), headers, token);
                    string jsonResponse = nextTaskResult.Resource;
                    TaskResponseModel tModel = JsonConvert.DeserializeObject<TaskResponseModel>(jsonResponse);
                    if (string.IsNullOrWhiteSpace(tModel.Id))
                    {
                        E expectedResult = JsonConvert.DeserializeObject<E>(jsonResponse);
                        if (expectedResult is IResponseModel responseModel)
                        {
                            responseModel.Validate();
                        }
                        ApiResult<E> eApiResult = new ApiResult<E>(nextTaskResult.ResourceStatus, nextTaskResult.ResourceLocation, expectedResult);
                        return ProcessApiResponse<T, E>(eApiResult);
                    }
                    BusinessTaskResponseModel response = ProcessApiResponse<BusinessTaskResponseModel, TaskResponseModel>(
                    new ApiResult<TaskResponseModel>(nextTaskResult.ResourceStatus, nextTaskResult.ResourceLocation, tModel));
                    if (nextTaskResult.ResourceLocation == null)
                    {
                        throw new ResourceNotFoundException();
                    }
                    if (taskLink.Url != nextTaskResult.ResourceLocation)
                    {
                        throw new ResourceMismatchedException();
                    }
                    return await ProcessTaskResponseAsync<T, E>(response, taskLink, headers, token);
                case TaskStatus.TechnicalError:
                    throw new BusinessException(BusinessExceptionCode.SYS000002);
                case TaskStatus.Timeout:
                    throw new Contract.TimeoutException();
                case TaskStatus.Verified:
                    throw new BusinessException(BusinessExceptionCode.SYS000006);
                default:
                    if (!taskResponseModel.HasNextLink())
                    {
                        Message message = taskResponseModel.Messages.FirstOrDefault();

                        if (message != null)
                        {
                            throw new RecoverableDeadEndError(message.Code, message.Title, message.Description);
                        }
                    }
                    else
                    {
                        IProcess process = Application.CurrentSession.SessionProcess;
                        UpdateLinks(process, AutoMapper.Mapper.Map<IEnumerable<ResourceLink>>(taskResponseModel.Links));
                    }
                    throw new TaskFailedException(taskResponseModel.Messages);
            }
        }

        /// <summary>
        /// Processes the response.
        /// </summary>
        /// <returns>The response.</returns>
        /// <param name="result">Result.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        /// <typeparam name="E">The 2nd type parameter.</typeparam>
        private T ProcessApiResponse<T, E>(ApiResult<E> result)
        {
            var isSuccess = result.ResourceStatus == ResourceStatus.Success ||
                result.ResourceStatus == ResourceStatus.UnprocessableEntity;

            if (isSuccess)
            {
                if (result.Resource is Services.Api.Contract.Model.Poi.PoiResponseModel poiResponseModel &&
                    poiResponseModel.Links != null)
                {
                    UpdateProcess(poiResponseModel, result.ResourceLocation);
                }

                return AutoMapper.Mapper.Map<T>(result.Resource);
            }

            throw GetProcessErrorException(result);
        }

        private Exception GetProcessErrorException<E>(ApiResult<E> result)
        {
            switch (result.ResourceStatus)
            {
                case ResourceStatus.Error:
                case ResourceStatus.NotFound:
                    if (result.Resource is BaseResponseModel model && model.Messages.Any())
                    {
                        AppMessage message = model.Messages.First();
                        return new RecoverableDeadEndError(
                            message.Code,
                            message.Title ?? "An error has occurred",
                            message.Description);
                    }

                    var errorCode = BusinessExceptionCode.SYS000001.ToString();
                    return new RecoverableDeadEndError(
                        errorCode,
                        "An error has occurred",
                        $"<p>Status code: {(int)result.ResourceStatus}</p><p>Error code: {errorCode}</p>");

                case ResourceStatus.Unauthorised:
                case ResourceStatus.Unauthenticated:
                    // TODO: 403 Unauthorised status should be a dead end, app reset most likely
                    // TODO: 401 Unauthenticated status when using extension grant means our token expired and we can try to get a new one
                    break;
            }

            return new ProcessFailedException();
        }

        private void UpdateProcess(Services.Api.Contract.Model.Poi.PoiResponseModel poiModel, string resourceLocation)
        {
            IProcess process = Application.CurrentSession.SessionProcess;
            UpdateLinks(process, AutoMapper.Mapper.Map<IEnumerable<ResourceLink>>(poiModel.Links));
            process.ProcessId = poiModel.ProcessId;
            process.CurrentResource = resourceLocation;
            //TODO: Populate process.ProcessStatus
        }
        private void UpdateLinks(IProcess process, IEnumerable<ResourceLink> links)
        {
            List<ResourceLink> currentLinks = (List<ResourceLink>)process.CurrentLinks;
            currentLinks.Clear();
            currentLinks.AddRange(links);
        }
    }
}