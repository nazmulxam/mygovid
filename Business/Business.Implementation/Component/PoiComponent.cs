﻿using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Model;
using myGovID.Business.Contract.Component;
using myGovID.Services.Api.Contract.Model;
using myGovID.Services.Api.Contract.Model.Poi.Register;
using myGovID.Services.Api.Contract;
using System.Threading.Tasks;
using myGovID.Business.Contract;
using myGovID.Services.Api.Contract.Model.Poi.GetPoi;
using System.Linq;
using System;
using myGovID.Services.Log.Contract;
using myGovID.Services.Storage.Contract;

namespace myGovID.Business.Component
{
    public class PoiComponent : BaseApiComponent, IPoiComponent
    {
        private readonly ITokenServiceComponent _tokenServiceComponent;
        private readonly IStorage _secureStorage;
        public PoiComponent(ILogger logger, IApiService apiService, IApplication application, ITokenServiceComponent tokenServiceComponent, IStorage secureStorage) : base(logger, apiService, application)
        {
            _tokenServiceComponent = tokenServiceComponent;
            _secureStorage = secureStorage;
        }
        public async Task<PoiResponseModel> StartPoiAsync(StartPoiModel model)
        {
            Link link = new Link()
            {
                Location = "/api/v1/poi",
                Method = "post",
                AuthType = AuthenticationType.None,
                TargetType = TargetType.Next
            };
            RegisterPOIRequestModel requestModel = AutoMapper.Mapper.Map<RegisterPOIRequestModel>(model);
            PoiResponseModel response = await SendAsync<RegisterPOIRequestModel, PoiResponseModel, Services.Api.Contract.Model.Poi.PoiResponseModel>(link, requestModel);
            if (response.Messages == null || !response.Messages.Any())
            {
                Application.CurrentSession.SessionProcess.ProcessId = response.ProcessId;
            }
            return response;
        }
        public async Task<GetPoiBusinessResponseModel> GetPoi()
        {
            string poiId = GetProcessIdentity();
            string extensionGrant = null;
            try
            {
                extensionGrant = await _tokenServiceComponent.CreateAsync(BusinessTokenType.ExtensionGrant);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Console.WriteLine(exception);
            }
            bool useExtensionGrant = string.IsNullOrWhiteSpace(extensionGrant);
            Link link = new Link()
            {
                Location = string.Format("/api/v1/poi/{0}", poiId),
                Method = "get",
                AuthType = useExtensionGrant ? AuthenticationType.Extension : AuthenticationType.Credential,
                TargetType = TargetType.Next
            };
            GetPoiBusinessResponseModel businessResponseModel = await SendAsync<RegisterPOIRequestModel, GetPoiBusinessResponseModel,
                    GetPoiResponseModel>(link, null, null, useExtensionGrant
                        ? extensionGrant
                        : await _tokenServiceComponent.CreateAsync(BusinessTokenType.Credential));
            if (businessResponseModel.Messages == null || !businessResponseModel.Messages.Any())
            {
                Application.CurrentSession.SessionProcess.ProcessIPLevel = businessResponseModel.Strength;
            }
            return businessResponseModel;
        }
        private string GetProcessIdentity()
        {
            string identity = Application.CurrentSession.SessionProcess.ProcessId;
            if (string.IsNullOrWhiteSpace(identity))
            {
                identity = _secureStorage.GetValue(StorageKey.PoiId.ToString());
            }
            return identity;
        }
    }
}