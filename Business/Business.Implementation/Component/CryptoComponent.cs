﻿using myGovID.Business.Contract.Component;
using myGovID.Services.Crypto.Contract;
using myGovID.Services.Crypto.Contract.Model;
using myGovID.Business.Contract.Model;
using System.Security.Cryptography;
using System;
using System.Text;
using myGovID.Services.Storage.Contract;
using Org.BouncyCastle.Crypto.Parameters;
using System.IO;
using Org.BouncyCastle.OpenSsl;

namespace myGovID.Business.Component
{
    public class CryptoComponent : ICryptoComponent
    {
        private readonly ICryptoService _cryptoService;
        private readonly IStorage _secureStorage;
        private string someStuff = @"-----BEGIN PRIVATE KEY-----
MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCsfryl/ySdFAkb
Nru+7T8VtRlXNKYfIBOzv3+JS1iGzKcWPQ2tCQTfj/HiLboZRS6jD67Xl1t5BzNx
wO2jLxKOobJL0xKplax4x5T5/Yv0wLPm6kizKRcqtDCbNh5f9ntQzAvcFazfsdEv
FVrojD7RC5MuwUXtrUQ0vn3TjVks7wnhXl90qQn209Y3G4zjOrFspkA1r25J/nH+
3ED5kJDaSfi+DsHy6hRXG5ee9ThMlpTlzUV6wIZm5t8pHvdglofo2q34Wz1iLcay
lWcQ3pZuc0jXKss3q5EkkN0Da9/ZM15QgDZmG/WAYIBcLOv187fkZhZiWK7PD7PS
muJ4g8AhAgMBAAECggEAJeNUlnV7WlE7StHz3RUJZUA5B7GVx4JpMXMflU2KbVlo
XfcdGOPBQUXB0HdQyBotCz33nn0tZ2EQiKzixZQiVFfp7MO49qzcjE7yRxLM+f6V
xWOygj/FSej4QC9b7qIaJf4boMTWCvBozRm4tRmQ9qO/OWbUcEc9kwRAaQZEDx05
rpPvUqbXT60PkQtxDYReAWxJKw4XGH6wyd21DFgsw7kjAkQ5ENBScpEf33BLapW7
MKZs/7O3P/L4zEULfakRl0QNm3deVJCujVX4XxcYOjp6L5AgBdlje8OG4y38+6LB
m825kMHe7a+PmxeEhIMsKscbVBvtI6UX4iq82WyoQQKBgQDeG0EWk9XUCM3g5cym
byQeelkjOzkkgnluklcu0qy5d6x1HLJdEdUnffVXv4WtvpJbMtEad3XkVwhJRrnw
/HFYtvKEw6ijNawmdQ8er6Whj1xtXaS+wanuAh26wQ/2Ve/IxMRcZ9Kn6l+VvEBt
LPj3zxhuPv6xBHbQr9omVE6G+QKBgQDG0WDiHLNgx6gbqG6AJKPJflzJwt1lmdGR
eON8yJh0BzCTD0L5kfjJTNLn9OpuUOPzlomcK30EDro0OnarwGqOGhm6IsWZX+/0
m2oeP57MsRZjOAmfKUdzfRgoRoFBbO6XHl/Ur2OTV64OTjRQVTYi+wyjAzGQZGOC
GhS+++aEaQKBgBgktkqR/2hxHJAZ6skcw7xcaFwgD/IcDL8sQTXhyMzQaXrBh9H+
IkGMu6KyOwHL+XTYbUqHOdV8cHYmrge4KUoPeQVdj3NOXkw/5UjcpxCM2Os6hrIF
s0cgC8NFQUrxxIL6gK3ay1ddbs6EDCzR+q5VYL57Moxjg9bjhr8r4u1hAoGAPbLo
cAMcH6KWJs8kS5wTQAp43Rwd/NO5V32+ixDtTsSUbG7GCBEmtcGjidxVjBqKQyOq
XM6aI1I2stKZ1T8HUkYDSeJUk/5TxwMb8Xso2rjyC6RuR/hVdh+SCZ2nqxgVCsyg
TK6JJkaFe2cFkRCAlmZ2S3hIP1kIEG3rlkEyDlECgYAkCar7is3hwX0PDywqWm3J
cuRvjcJo4T3d9JVLFqOjAuvBs2iPltS4ix/nL2yTY6q6QcTfrTTNW+aCV6n7FgiL
BJJBJ10cF0TusXFRJiS2PEXceQj8W3x15+fxAF5STMwxE3aRFO3dALn1gYxavSVb
YMPmf0dGlpEypr43lJ5PUQ==
-----END PRIVATE KEY-----";

        public CryptoComponent(
            ICryptoService cryptoService,
            IStorage secureStorage)
        {
            _cryptoService = cryptoService;
            _secureStorage = secureStorage;
        }

        public string GenerateRandomString(RandomStringSpecification specification)
        {
            char[] randomString = new char[specification.Length];
            int currentIndex = 0;
            int discardBiasResultThreshold = (int)(Math.Floor(256D / (double)specification.CharacterSet.Length) * (double)specification.CharacterSet.Length);
            using (RandomNumberGenerator rng = new RNGCryptoServiceProvider())
            {
                while (currentIndex < specification.Length)
                {
                    byte[] tokenData = new byte[1];
                    rng.GetBytes(tokenData);
                    int randomIndex = tokenData[0];
                    // Discard any results that are above the bias threshold
                    if (randomIndex < discardBiasResultThreshold)
                    {
                        // Assign the character to the return value
                        randomIndex = randomIndex % specification.CharacterSet.Length;
                        randomString[currentIndex++] = specification.CharacterSet[randomIndex];
                    }
                }
                return new string(randomString);
            }
        }

        public void Clear()
        {
            _cryptoService.Delete(new DeleteRequest());
            _secureStorage.DeleteKey(StorageKey.CredentialId.ToString());
            _secureStorage.DeleteKey(StorageKey.CredentialAlias.ToString());
            _secureStorage.DeleteKey(StorageKey.CredentialPassword.ToString());
        }

        public CryptoCreateResultModel CreateRequest()
        {
            return CreateRequest(GetCredentialAlias());
        }

        public CryptoCreateResultModel CreateRequest(string alias)
        {
            LoadCryptoStore();
            CreateRequest keystoreRequest = new CreateRequest(alias, GetPassword());
            CreateResult serviceResponse = _cryptoService.CreateRequest(keystoreRequest);

            return AutoMapper.Mapper.Map<CryptoCreateResultModel>(serviceResponse);
        }

        public void InsertCredential(string requestId, string p7cbase64, string credentialId)
        {
            InsertCredential(requestId, p7cbase64, credentialId, GetCredentialAlias());
        }

        public void InsertCredential(string requestId, string p7cbase64, string credentialId, string credentialAlias)
        {
            _secureStorage.SetValue(StorageKey.CredentialId.ToString(), credentialId);

            InsertCredentialRequest insertCredentialRequest = new InsertCredentialRequest(credentialAlias, requestId, Hash(p7cbase64), p7cbase64);
            _cryptoService.InsertCredential(insertCredentialRequest);
        }

        private string Hash(string input)
        {
            var hash = (new SHA1Managed()).ComputeHash(Encoding.UTF8.GetBytes(input));
            return Convert.ToBase64String(hash);
        }
        private string GetCredentialAlias()
        {
            if (_secureStorage.HasKey(StorageKey.CredentialAlias.ToString()))
            {
                return _secureStorage.GetValue(StorageKey.CredentialAlias.ToString());
            }
            string alias = Guid.NewGuid().ToString();
            _secureStorage.SetValue(StorageKey.CredentialAlias.ToString(), alias);
            return alias;
        }

        private string GetPassword()
        {
            string password;
            lock (this)
            {
                if (_secureStorage.HasKey(StorageKey.CredentialPassword.ToString()))
                {
                    password = _secureStorage.GetValue(StorageKey.CredentialPassword.ToString());
                }
                else
                {
                    password = GenerateRandomString(new PasswordRandomStringSpecification());
                    _secureStorage.SetValue(StorageKey.CredentialPassword.ToString(), password);
                }
            }
            return password;
        }

        public RSAParameters GetRSAParameters()
        {
            var credential = _cryptoService.GetCredential(new GetCredentialRequest(GetCredentialId(), GetPassword()));

            return GetRsaParams(someStuff);
        }


        private RSAParameters GetRsaParams(string key)
        {
            RsaPrivateCrtKeyParameters keyParam;

            using (var sr = new StringReader(key))
            {
                PemReader pr = new PemReader(sr);

                keyParam = (RsaPrivateCrtKeyParameters)pr.ReadObject();
            }

            return ToRsaParameters(keyParam);

        }

        private RSAParameters ToRsaParameters(RsaPrivateCrtKeyParameters privateKey)
        {
            RSAParameters rp = new RSAParameters();
            rp.Modulus = privateKey.Modulus.ToByteArrayUnsigned();
            rp.Exponent = privateKey.PublicExponent.ToByteArrayUnsigned();
            rp.D = privateKey.Exponent.ToByteArrayUnsigned();
            rp.P = privateKey.P.ToByteArrayUnsigned();
            rp.Q = privateKey.Q.ToByteArrayUnsigned();
            rp.DP = privateKey.DP.ToByteArrayUnsigned();
            rp.DQ = privateKey.DQ.ToByteArrayUnsigned();
            rp.InverseQ = privateKey.QInv.ToByteArrayUnsigned();

            return rp;
        }

        public string GetCredentialId()
        {
            return _secureStorage.GetValue(StorageKey.CredentialId.ToString());
        }
        private void LoadCryptoStore()
        {
            LoadRequest loadRequest = new LoadRequest();
            LoadResult result = _cryptoService.Load(loadRequest);
        }
    }
}