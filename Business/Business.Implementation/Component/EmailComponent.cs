﻿using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Component;
using myGovID.Business.Contract.Model;
using myGovID.Services.Api.Contract;
using myGovID.Services.Api.Contract.Model;
using myGovID.Services.Api.Contract.Model.Poi.Email;
using myGovID.Business.Specification.Link;
using myGovID.Business.Contract.Common.Process;
using myGovID.Services.Uri.Contract;
using System.Threading.Tasks;
using myGovID.Services.Log.Contract;

namespace myGovID.Business.Component
{
    public class EmailComponent : BaseApiComponent, IEmailComponent
    {
        private readonly IUriService _uriService;
        public EmailComponent(ILogger logger, IApiService apiService, IUriService uriService, ITokenServiceComponent tokenServiceComponent, IApplication application) : base(logger, apiService, application)
        {
            _uriService = uriService;
        }
        public async Task<PoiResponseModel> SubmitEmailAsync(CaptureEmailModel model)
        {
            ResourceLink resourceLinkResponse = FindLink(new EmailLinkSpecification(_uriService));
            Link link = AutoMapper.Mapper.Map<Link>(resourceLinkResponse);
            EmailRequestModel requestModel = AutoMapper.Mapper.Map<EmailRequestModel>(model);
            return await SendAsync<EmailRequestModel, PoiResponseModel, Services.Api.Contract.Model.Poi.PoiResponseModel>(link, requestModel);
        }
    }
}