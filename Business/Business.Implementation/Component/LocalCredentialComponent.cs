﻿using System.Threading.Tasks;
using myGovID.Business.Contract;
using myGovID.Business.Contract.Common.Session;
using myGovID.Business.Contract.Component;
using myGovID.Business.Contract.Model;
using myGovID.Services.Storage.Contract;
using myGovID.Business.Contract.Common.Application;
using System.Linq;
using myGovID.Services.Event.Contract;
using myGovID.Business.Contract.Common.Process;

namespace myGovID.Business.Component
{
    public class LocalCredentialComponent : ILocalCredentialComponent
    {
        private readonly ICredentialComponent _credentialComponent;
        private readonly ICryptoComponent _cryptoComponent;
        protected readonly ITokenStorageComponent TokenStorageComponent;
        private readonly IStorage _secureStorage;
        private readonly IStorage _fileStorage;
        private readonly IStorage _memoryStorage;
        private readonly IApplication _application;
        private readonly IEventService _eventService;
        private readonly ISessionManager _sessionManager;
        public LocalCredentialComponent(IApplication application,
                                        ICredentialComponent credentialComponent,
                                        ICryptoComponent cryptoComponent,
                                        ITokenStorageComponent tokenStorageComponent,
                                        IStorage secureStorage,
                                        IStorage fileStorage,
                                        IStorage memoryStorage,
                                        IEventService eventService,
                                        ISessionManager sessionManager)
        {
            _application = application;
            _credentialComponent = credentialComponent;
            _cryptoComponent = cryptoComponent;
            TokenStorageComponent = tokenStorageComponent;
            _secureStorage = secureStorage;
            _fileStorage = fileStorage;
            _memoryStorage = memoryStorage;
            _eventService = eventService;
            _sessionManager = sessionManager;
        }

        public void Clear()
        {
            _secureStorage.Clear();
            _fileStorage.Clear();
            _memoryStorage.Clear();
            _cryptoComponent.Clear();
            TokenStorageComponent.Clear();
        }

        public bool IsRegistered()
        {
            bool passwordRegistered = _secureStorage.HasKey(StorageKey.UserPassword.ToString());
            bool credIdRegistered = _secureStorage.HasKey(StorageKey.CredentialId.ToString());
            return passwordRegistered && credIdRegistered;
        }

        public async Task<bool> RegisterCredentialAsync(string assuranceToken)
        {
            CryptoCreateResultModel createStoreResponse = _cryptoComponent.CreateRequest();
            CredentialResponseModel credentialResponse =
                    await _credentialComponent.CreateCredentialAsync(assuranceToken,
                                                      createStoreResponse.P10base64);

            string credentialId = TokenStorageComponent.GetTokenClaim(credentialResponse.CredentialToken, "urn:mygovid.gov.au:credentialId");
            if (string.IsNullOrWhiteSpace(credentialId))
            {
                throw new BusinessException(BusinessExceptionCode.TKN000003);
            }
            TokenStorageComponent.Set(BusinessTokenType.Credential, credentialResponse.CredentialToken);

            _cryptoComponent.InsertCredential(createStoreResponse.RequestId, credentialResponse.P7, credentialId);

            return true;
        }
        public void RegisterUser(string email, string password, bool useBiometrics)
        {
            // Store the process Id.
            _secureStorage.SetValue(StorageKey.PoiId.ToString(), _application.CurrentSession.SessionProcess.ProcessId);
            // Store the email address
            _secureStorage.SetValue(StorageKey.EmailAddress.ToString(), email);
            // Store securely the biometric choice
            _secureStorage.SetValue(StorageKey.UseBiometrics.ToString(), useBiometrics ? bool.TrueString : bool.FalseString);
            // Store securely the password
            _secureStorage.SetValue(StorageKey.UserPassword.ToString(), password);
            // Store the IP level as IP1.
            _secureStorage.SetValue(StorageKey.IPLevel.ToString(), IdentityProofingLevel.IP1.ToString());
        }

        public bool ShowBiometricChallenge()
        {
            bool useBiometrics = _secureStorage.HasKey(StorageKey.UseBiometrics.ToString()) && bool.Parse(_secureStorage.GetValue(StorageKey.UseBiometrics.ToString()));
            return (useBiometrics &&
                    _application.Device.Capabilities.Any(e => (e.Equals(DeviceCapability.FacialVerification) ||
                                                                e.Equals(DeviceCapability.Fingerprint) ||
                                                                e.Equals(DeviceCapability.Touch))));
        }
        public void UpdateIdentity(string firstName, string lastName, string dateOfBirth)
        {
            _secureStorage.SetValue(StorageKey.FirstName.ToString(), firstName ?? "");
            _secureStorage.SetValue(StorageKey.LastName.ToString(), lastName);
            _secureStorage.SetValue(StorageKey.DateOfBirth.ToString(), dateOfBirth);
        }

        public async Task<bool> LoginUsingPasswordAsync(string password)
        {
            return await _sessionManager.LoginUsingPasswordAsync(password);
        }

        public async Task<bool> LoginUsingKeystoreAsync()
        {
            return await _sessionManager.LoginUsingKeystoreAsync();
        }

        public string GetUserPassword()
        {
            return _secureStorage.GetValue(StorageKey.UserPassword.ToString());
        }
    }
}