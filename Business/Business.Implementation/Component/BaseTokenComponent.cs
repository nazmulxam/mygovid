﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;

namespace myGovID.Business.Component
{
    public abstract class BaseTokenComponent
    {
        protected string GetClientId(string credentialId)
        {
            return string.Concat("https://ausidapp.gov.au/", credentialId);
        }

        protected bool IsExpiring(string existingtoken)
        {
            JwtSecurityTokenHandler jwtHandler = new JwtSecurityTokenHandler();
            if (jwtHandler.CanReadToken(existingtoken))
            {
                JwtSecurityToken jwttoken = jwtHandler.ReadJwtToken(existingtoken);
                return jwttoken.ValidTo < DateTime.Now.AddSeconds(30);
            }
            return true; // If the token cant be read, get a new token instead.
        }

        protected bool HasExpired(string existingtoken)
        {
            JwtSecurityTokenHandler jwtHandler = new JwtSecurityTokenHandler();
            if (jwtHandler.CanReadToken(existingtoken))
            {
                JwtSecurityToken jwttoken = jwtHandler.ReadJwtToken(existingtoken);
                return jwttoken.ValidTo < DateTime.Now;
            }
            return true; // If the token cant be read, treat it as expired.
        }

        protected string GetStringToken(JwtSecurityToken token)
        {
            JwtSecurityTokenHandler jwtHandler = new JwtSecurityTokenHandler();

            if (jwtHandler.CanWriteToken)
                return jwtHandler.WriteToken(token);

            return string.Empty;
        }
    }
}
