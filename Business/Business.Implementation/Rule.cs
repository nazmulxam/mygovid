﻿using System;
using myGovID.Business.Contract;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Specification;
using myGovID.Business.Rules;

namespace myGovID.Business
{
    public class Rule: IRule 
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:myGovID.Business.Rule"/> class.
        /// </summary>
        /// <param name="businessState">Business state of the rule.</param>
        /// <param name="specification">Specification that must be satisfied in 
        /// order to achieve specified business state</param>
        public Rule(BusinessState businessState, ISpecification<IProcessInformation> specification)
        {
            BusinessState = businessState;
            Specification = specification ?? throw new ArgumentNullException(nameof(specification));
        }
        public BusinessState BusinessState { get; }
        public ISpecification<IProcessInformation> Specification { get; }
    }
}
