﻿using System;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Session;

namespace myGovID.Business.Session
{
    internal class LocalIdentity : MyGovIdentity
    {
        public override string AuthenticationType => "LocalAuthentication";

        public LocalIdentity(string firstName, string lastName, string dob, string emailAddress, string identity)
        {
            FirstName = firstName;
            LastName = lastName;
            bool success = DateTime.TryParse(dob, out DateTime dobDt);
            DateOfBirth = success ? dobDt : DateTime.Now;
            EmailAddress = emailAddress;
            IdentityId = identity;
        }
    }
}