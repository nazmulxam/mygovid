﻿using Microsoft.IdentityModel.Tokens;
using myGovID.Business.Component.Token;
using myGovID.Business.Contract.Common.Session;
using myGovID.Business.Contract.Component;

namespace myGovID.Business.Session
{
    public class KeystoreCredential : ICredential
    {
        public ICryptoComponent _cryptoComponent;

        public string CredentialId { get { return _cryptoComponent.GetCredentialId(); } }

        public KeystoreCredential(ICryptoComponent cryptoComponent)
        {
            _cryptoComponent = cryptoComponent;
        }

        public SigningCredentials CreateSigningCredentials()
        {
            return new SigningCredentials(
                new RsaSecurityKey(_cryptoComponent.GetRSAParameters()),
                SecurityAlgorithms.RsaSha256Signature)
            {
                CryptoProviderFactory = new CustomSignatureProviderFactory()
            };
        }
    }
}