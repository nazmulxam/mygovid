﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using myGovID.Business.Contract;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Session;
namespace myGovID.Business.Session
{
    public class OpenIdIdentity: MyGovIdentity
    {
        private const string EmailClaim = ClaimTypes.Email;
        private const string FirstNameClaim = ClaimTypes.GivenName;
        private const string LastNameClaim = ClaimTypes.Surname;
        private const string DateOfBirthClaim = ClaimTypes.DateOfBirth;
        private const string Identity = ClaimTypes.NameIdentifier;

        public override string AuthenticationType => "OpenIdAuthentication";

        public OpenIdIdentity(IEnumerable<Claim> claims)
        {
            EmailAddress = claims.ExtractClaimValue(EmailClaim, BusinessExceptionCode.TKN000009);
            FirstName = claims.ExtractClaimValue(FirstNameClaim); // Optional
            LastName = claims.ExtractClaimValue(LastNameClaim, BusinessExceptionCode.TKN000007);
            DateOfBirth = DateTime.ParseExact(claims.ExtractClaimValue(DateOfBirthClaim, BusinessExceptionCode.TKN000008), "dd/MM/yyyy", CultureInfo.GetCultureInfo("en-AU"));
            IdentityId = claims.ExtractClaimValue(Identity, BusinessExceptionCode.TKN000013);
        }

        public bool HasSystemIdentity()
        {
            return true;
        }
     }
}
