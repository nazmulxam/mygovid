﻿using myGovID.Business.Contract.Common.Session;
using myGovID.Services.Event.Contract;
using myGovID.Services.Storage.Contract;

namespace myGovID.Business.Session
{
    public class AuthenticationContext : IAuthenticationContext
    {
        private IEventService _eventService;
        private IMyGovPrincipal _current;
        public IMyGovPrincipal Current
        {
            get => _current;
            private set
            {
                _current = value;
                _eventService.Publish<IAuthenticationContext>(EventKey.IdentityUpdated, this);
            }
        }

        public AuthenticationState AuthenticatedState { get; private set; }

        public ICredential CurrentCredential { get; private set; }

        private readonly IStorage _secureStorage;

        public AuthenticationContext()
        {
            AuthenticatedState = AuthenticationState.Unauthenticated;
        }

        public AuthenticationContext(IStorage secureStorage, AuthenticationState state, IMyGovPrincipal principal, ICredential credential, IEventService eventService)
        {
            _secureStorage = secureStorage;
            _eventService = eventService;
            Current = principal;
            CurrentCredential = credential;
            AuthenticatedState = state;
        }
    }

    public class UnauthenticatedContext : IAuthenticationContext
    {
        public IMyGovPrincipal Current => null;

        public AuthenticationState AuthenticatedState => AuthenticationState.Unauthenticated;

        public string CredentialId => null;

        private static UnauthenticatedContext instance;

        private UnauthenticatedContext() { }

        public static UnauthenticatedContext Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UnauthenticatedContext();
                }
                return instance;
            }
        }

        public ICredential CurrentCredential => null;
    }

    public class ExpiredContext : IAuthenticationContext
    {
        public IMyGovPrincipal Current => null;

        public AuthenticationState AuthenticatedState => AuthenticationState.AuthenticationExpired;

        public string CredentialId => null;

        private static ExpiredContext instance;

        private ExpiredContext() { }

        public static ExpiredContext Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ExpiredContext();
                }
                return instance;
            }
        }

        public ICredential CurrentCredential => null;
    }
}
