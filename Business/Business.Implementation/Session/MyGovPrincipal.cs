﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using myGovID.Business.Contract;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Session;

namespace myGovID.Business.Session
{
    /// <summary>
    /// A representation of an individual identity in the myGovID system
    /// </summary>
    public class MyGovPrincipal : IMyGovPrincipal
    {
        public MyGovIdentity MyGovIdentity { get; }

        public IIdentity Identity => MyGovIdentity;

        public IdentityProofingLevel IPLevel { get; }

        public bool IsInRole(string role)
        {
            // Not used yet
            throw new NotImplementedException();
        }

        public bool HasSystemIdentity => MyGovIdentity.AuthenticationType == "OpenIdAuthentication";

        public MyGovPrincipal(ClaimsPrincipal claimsPrincipal)
        {
            MyGovIdentity = new OpenIdIdentity(claimsPrincipal.Claims);
            var acrValues = claimsPrincipal.Claims.ExtractClaimValue("http://schemas.microsoft.com/claims/authnclassreference", BusinessExceptionCode.TKN000005)
                            .Split(':');
            if (acrValues.Length != 6) { throw new BusinessException(BusinessExceptionCode.TKN000004); }
            IPLevel = (IdentityProofingLevel)Enum.Parse(typeof(IdentityProofingLevel), acrValues[4].ToUpper());
        }

        public MyGovPrincipal(string firstName, string lastName, string dob, string emailAddress, string identity, IdentityProofingLevel ipLevel)
        {
            MyGovIdentity = new LocalIdentity(firstName, lastName, dob, emailAddress, identity);
            IPLevel = ipLevel;
        }
    }
}
