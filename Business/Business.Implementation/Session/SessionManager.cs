﻿using System.Threading.Tasks;
using myGovID.Business.Contract;
using myGovID.Business.Contract.Common.Session;
using myGovID.Business.Contract.Component;
using myGovID.Services.Log.Contract;

namespace myGovID.Business.Session
{
    public class SessionManager : ISessionManager
    {
        private readonly ISession _session;
        private readonly IAuthenticationComponent _authenticationComponent;
        protected readonly ILogger Logger;
        private IAuthenticationContext AuthenticatedContext
        {
            get => _session.AuthenticatedContext;
            set => _session.AuthenticatedContext = value;
        }
        public SessionManager(ISession session, IAuthenticationComponent authenticationComponent, ILogger logger)
        {
            _session = session;
            _authenticationComponent = authenticationComponent;
            Logger = logger;
        }
        public virtual async Task<bool> LoginUsingPasswordAsync(string password)
        {
            IAuthenticationContext authenticationContext = await _authenticationComponent.LoginUsingPasswordAsync(password);
            bool result = authenticationContext != null && authenticationContext.AuthenticatedState == AuthenticationState.AuthenticatedLocal;
            if (result)
            {
                AuthenticatedContext = authenticationContext;
            }
            return result;
        }
        public virtual async Task<bool> LoginUsingKeystoreAsync()
        {
            // Check if the user is previously authenticated.
            if (AuthenticatedContext == null ||
               !(AuthenticatedContext.AuthenticatedState == AuthenticationState.Authenticated || AuthenticatedContext.AuthenticatedState == AuthenticationState.AuthenticatedLocal))
            {
                throw new BusinessException(BusinessExceptionCode.AUT000001);
            }
            IAuthenticationContext authenticationContext = await _authenticationComponent.LoginUsingKeystoreAsync();
            bool result = authenticationContext != null && authenticationContext.AuthenticatedState == AuthenticationState.Authenticated;
            if (result)
            {
                AuthenticatedContext = authenticationContext;
            }
            return result;
        }
        public void Logout(bool expired)
        {
            AuthenticatedContext = expired ? ExpiredContext.Instance : (IAuthenticationContext)UnauthenticatedContext.Instance;
        }
    }
}
