﻿using System.Threading.Tasks;
using myGovID.Business.Contract.Common.Session;
using myGovID.Business.Contract.Component;
using myGovID.Services.Log.Contract;
using myGovID.Services.Timer.Contract;

namespace myGovID.Business.Session
{
    public class TimedSessionManager : SessionManager
    {
        private readonly IDeviceActivityTimer _timer;
        public TimedSessionManager(IDeviceActivityTimer timer, ISession session, IAuthenticationComponent authenticationComponent, ILogger logger) : base(session, authenticationComponent, logger)
        {
            _timer = timer;
        }
        public override async Task<bool> LoginUsingPasswordAsync(string password)
        {
            bool result = await base.LoginUsingPasswordAsync(password);
            if (!result)
            {
                return false;
            }
            StartTimer();
            return true;
        }
        public override async Task<bool> LoginUsingKeystoreAsync()
        {
            bool result = await base.LoginUsingKeystoreAsync();
            if (!result)
            {
                return false;
            }
            StartTimer();
            return true;
        }
        private void StartTimer()
        {
            if (_timer != null)
            {
                Logger.Info("Starting timeout timer.");
                _timer.Start(OnTimerExpired);
            }
        }
        private void OnTimerExpired()
        {
            Logger.Info("Executing timeout timer callback.");
            Logout(true);
        }
    }
}