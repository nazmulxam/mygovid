﻿using myGovID.Business.Contract.Common.Session;

namespace myGovID.Business.Session
{
    public class SelfAssertedCredential : ICredential
    {
        public string Password { get; }

        public string CredentialId { get; }

        public SelfAssertedCredential(string password, string credentialId)
        {
            Password = password;
            CredentialId = credentialId;
        }
    }
}