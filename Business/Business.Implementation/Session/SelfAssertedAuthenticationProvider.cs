﻿using System;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Session;
using myGovID.Services.Storage.Contract;
using System.Threading.Tasks;

namespace myGovID.Business.Session
{
    public class SelfAssertedAuthenticationProvider: IAuthenticationProvider
    {
        private readonly IStorage _secureStorage;
        public AuthenticationState AuthenticatedState => AuthenticationState.AuthenticatedLocal;
        public SelfAssertedAuthenticationProvider(IStorage secureStorage)
        {
            _secureStorage = secureStorage;
        }
        public async Task<IMyGovPrincipal> GetIdentity(ICredential credential)
        {
            if (!(credential is SelfAssertedCredential selfAssertedCredential))
            {
                throw new ArgumentException("Invalid credential. The type was not supported.");
            }
            string userEnteredPassword = selfAssertedCredential.Password;
            string password = _secureStorage.GetValue(StorageKey.UserPassword.ToString());
            if (string.IsNullOrWhiteSpace(userEnteredPassword) || string.IsNullOrWhiteSpace(password) || !userEnteredPassword.Equals(password))
            {
                return null;
            }
            string firstName = GetValue(StorageKey.FirstName);
            string lastName = GetValue(StorageKey.LastName);
            string dob = GetValue(StorageKey.DateOfBirth);
            string emailAddress = GetValue(StorageKey.EmailAddress);
            string ipLevel = GetValue(StorageKey.IPLevel);
            string identity = GetValue(StorageKey.PoiId);
            bool success = Enum.TryParse(ipLevel, true, out IdentityProofingLevel identityProofingLevel);
            return new MyGovPrincipal(firstName, lastName, dob, emailAddress, identity, success ? identityProofingLevel : IdentityProofingLevel.Unknown);
        }
        private string GetValue(StorageKey key)
        {
            return _secureStorage.HasKey(key.ToString()) ? _secureStorage.GetValue(key.ToString()) : "";
        }
    }
}