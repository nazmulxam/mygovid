﻿using System.Security.Claims;
using System.Threading.Tasks;
using myGovID.Business.Contract.Common.Session;
using myGovID.Business.Contract.Component;
using myGovID.Services.Storage.Contract;
using myGovID.Services.Log.Contract;
using myGovID.Business.Contract;

namespace myGovID.Business.Session
{
    public class OpenIdAuthenticationProvider : IAuthenticationProvider
    {
        public AuthenticationState AuthenticatedState => AuthenticationState.Authenticated;
        private readonly ITokenStorageComponent _tokenComponent;
        private readonly IStorage _secureStorage;
        private readonly ILogger _logger;
        private readonly IOpenIdComponent _openIdComponent;
        public OpenIdAuthenticationProvider(ITokenStorageComponent tokenComponent, IStorage secureStorage, ILogger logger, IOpenIdComponent openIdComponent)
        {
            _tokenComponent = tokenComponent;
            _secureStorage = secureStorage;
            _logger = logger;
            _openIdComponent = openIdComponent;
        }
        public async Task<IMyGovPrincipal> GetIdentity(ICredential credential)
        {
            string emailAddress = _secureStorage.GetValue(StorageKey.EmailAddress.ToString());
            _logger.Info("Attempting OpenId authentication.");
            var response = await _openIdComponent.AuthenticateAsync(credential, emailAddress);
            if (response.Identity != null)
            {
                _tokenComponent.Set(BusinessTokenType.ExtensionGrant, response.AccessToken);
                return new MyGovPrincipal(response.Identity);
            }
            // If the credential hasn't been bound to the identity yet, we won't have an id_token
            // This should be treated as not having authenticated with OpenId even though the call succeeded
            return null;
        }
    }
}