﻿using System;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Session;
using myGovID.Services.Event.Contract;
using myGovID.Services.Log.Contract;

namespace myGovID.Business.Session
{
    public class Session : ISession
    {
        private IAuthenticationContext _authenticatedContext;
        public string SessionIdentity { get; }
        public IProcess SessionProcess { get; }
        private readonly IEventService _eventService;
        protected readonly ILogger Logger;
        public IAuthenticationContext AuthenticatedContext
        {
            get => _authenticatedContext;
            set
            {
                AuthenticationState authContext = _authenticatedContext?.AuthenticatedState ?? AuthenticationState.Unauthenticated;
                _authenticatedContext = value;
                SessionProcess.ProcessIPLevel = value?.Current?.IPLevel ?? IdentityProofingLevel.Unknown;
                if (value != null )
                {
                    switch (value.AuthenticatedState)
                    {
                        case AuthenticationState.Authenticated:
                        case AuthenticationState.AuthenticatedLocal:
                            _eventService.Publish<object>(EventKey.Login, null);
                            break;
                        case AuthenticationState.Unauthenticated:
                            if (authContext == AuthenticationState.Authenticated || authContext == AuthenticationState.AuthenticatedLocal)
                            {
                                _eventService.Publish<object>(EventKey.Logout, null);
                            }
                            break;
                        case AuthenticationState.AuthenticationExpired:
                            _eventService.Publish<object>(EventKey.SessionExpired, null);
                            break;
                    }
                }
            }
        }
        public Session(IEventService eventService, ILogger logger) 
        {
            _eventService = eventService;
            Logger = logger;
            SessionIdentity = Guid.NewGuid().ToString();
            SessionProcess = new Process();
            AuthenticatedContext = UnauthenticatedContext.Instance;
        }
    }
}