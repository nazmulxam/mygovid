﻿using System.Collections.Generic;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Model;
using myGovID.Business.Contract.Common.Application;
using System.Linq;

namespace myGovID.Business
{
    public class BusinessTaskResponseModel : IBusinessResponseModel
    {
        public string Id { get; set; }

        public TaskStatus Status { get; set; }

        public int Eta { get; set; }

        public IEnumerable<ResourceLink> Links { get; set; }

        public IEnumerable<Message> Messages { get; set; }

        public BusinessTaskResponseModel(IEnumerable<ResourceLink> links, IEnumerable<Message> messages, string eta, TaskStatus status, string id)
        {
            Status = status;
            Eta = int.Parse(eta);
            Links = links;
            Messages = messages;
            Id = id;
        }

        public bool HasNextLink() {
            return Links.Any(i => i.Target == TargetType.Next);
        }
    }
}