﻿using System;
using System.Collections.Generic;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Session;
using myGovID.Services.Event.Contract;

namespace myGovID.Business
{
    public class Application: IApplication, IProcessInformation
    {
        public IEnumerable<Alert> Alerts { get; set; }

        public IEnumerable<Capability> Capabilities { get; set; }

        public ResourceLink UpgradeLink { get; set; }

        public bool UpgradeActioned { get; set; }

        public ISession CurrentSession { get; private set; }

        public IDevice Device { get; }

        public IApplicationMetaData ApplicationMetaData { get; }

        public IProcess CurrentProcess => CurrentSession.SessionProcess;

        public AuthenticationState AuthenticatedState => CurrentSession.AuthenticatedContext.AuthenticatedState;

        public IEnumerable<DeviceCapability> DeviceCapabilities => Device.Capabilities;

        public Application(IApplicationMetaData appMetaData, IDevice device, ISession session)
        {
            ApplicationMetaData = appMetaData ?? throw new ArgumentNullException(nameof(appMetaData));
            Device = device ?? throw new ArgumentNullException(nameof(device));
            CurrentSession = session;
        }
    }
}