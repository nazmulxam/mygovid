﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using myGovID.Business.Contract;

namespace myGovID.Business
{
    public static class ClaimExtensions
    {
        public static string ExtractClaimValue(this IEnumerable<Claim> claims, string claimName)
        {
            var claim = claims.FirstOrDefault(p => p.Type == claimName);
            if (claim != default && !string.IsNullOrEmpty(claim.Value))
            {
                return claim.Value;
            }
            return default;
        }

        public static string ExtractClaimValue(this IEnumerable<Claim> claims, string claimName, BusinessExceptionCode missingClaimError)
        {
            var claim = claims.FirstOrDefault(p => p.Type == claimName);
            if (claim != default && !string.IsNullOrEmpty(claim.Value))
            {
                return claim.Value;
            }
            else
            {
                throw new BusinessException(missingClaimError);
            }
        }
    }
}
