﻿using System.Linq;
using System.Collections.Generic;
using myGovID.Business.Contract;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Rules;
using myGovID.Business.Rules;
using myGovID.Services.Uri.Contract;

namespace myGovID.Business
{
    public class RulesEngine: IRulesEngine
    {
        private readonly List<IRule> _rules;
        public RulesEngine(IUriService uriService)
        {
            _rules = new List<IRule>
            {
                new LogoutRule(),
                new DashboardDisabledStateRule(),
                new InProcessFVSStateRule(uriService),
                new DashboardStateRule(uriService),
                new DashboardAnonymousStateRule(uriService),
                new CaptureEmailStateRule(uriService),
                new EmailVerificationStateRule(uriService),
                new PersonalDetailsStateRule(uriService),
                new ExplicitBindStateRule(uriService),
                new FVSStateRule(uriService),
                new MedicareStateRule(uriService),
                new DriverLicenceStateRule(uriService),
                new PassportStateRule(uriService)
            };
        }
        /// <summary>
        /// Finds the state of the app based on links present in the application
        /// </summary>
        /// <returns>The state.</returns>
        public BusinessState FindNextState(IProcessInformation processState)
        {
            foreach (IRule rule in _rules)
            {
                if (rule.Specification.IsSatisfiedBy(processState))
                {
                    return rule.BusinessState;
                }
            }
            throw new InvalidStateException();
        }

        /// <summary>
        /// Returns all possible valid next BusinessStates from the current processState
        /// </summary>
        public IEnumerable<BusinessState> AllStates(IProcessInformation processState)
        {
            return from rule in _rules
                   where rule.Specification.IsSatisfiedBy(processState)
                   select rule.BusinessState;
        }
    }
}