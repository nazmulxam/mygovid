﻿namespace myGovID.Business.Contract
{
    public class ResourceMismatchedException : BusinessException
    {
        public ResourceMismatchedException(BusinessExceptionCode exceptionCode = BusinessExceptionCode.RES000002) : base(exceptionCode)
        {
        }
    }
}