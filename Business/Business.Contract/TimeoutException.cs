﻿namespace myGovID.Business.Contract
{
    public class TimeoutException : BusinessException
    {
        public TimeoutException(BusinessExceptionCode exceptionCode = BusinessExceptionCode.SYS000003) : base(exceptionCode)
        {
        }
    }
}