﻿namespace myGovID.Business.Contract
{
    public class ProcessFailedException : BusinessException
    {
        public ProcessFailedException() : base(BusinessExceptionCode.SYS000004)
        {}
        public ProcessFailedException(string code, string title, string description) : base(code, title, description)
        {
        }
    }
}