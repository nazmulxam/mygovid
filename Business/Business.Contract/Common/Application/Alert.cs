﻿using System;

namespace myGovID.Business.Contract.Common.Application
{
    /// <summary>
    /// Alert class
    /// </summary>
    public class Alert
    {
        public DateTime StartDateTime { get; }
        public DateTime EndDateTime { get; }
        public string Message { get; }
        public AlertType Type { get; }
        public bool SystemOutage { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:myGovID.Business.Contract.Common.Application.Alert"/> class.
        /// </summary>
        /// <param name="message">Message.</param>
        /// <param name="systemOutage">If set to <c>true</c> system outage.</param>
        /// <param name="type">Type of the alert.</param>
        /// <param name="startDateTime">Start date time.</param>
        /// <param name="endDateTime">End date time.</param>
        public Alert(string message, bool systemOutage, AlertType type, DateTime startDateTime, DateTime endDateTime)
        {
            Message = message;
            SystemOutage = systemOutage;
            Type = type;
            StartDateTime = startDateTime;
            EndDateTime = endDateTime;
        }
    }
}
