﻿using System.Collections.Generic;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Session;

namespace myGovID.Business.Contract.Common.Application
{
    /// <summary>
    /// A representation of the current myGovID application
    /// </summary>
    public interface IApplication
    {
        /// <summary>
        /// Current system alerts that apply to this version of the application
        /// </summary>
        /// <value>The alerts.</value>
        IEnumerable<Alert> Alerts { get; set; }

        /// <summary>
        /// The system capabilities that are available in this version of the application
        /// </summary>
        /// <value>The capabilities.</value>
        IEnumerable<Capability> Capabilities { get; set; }

        /// <summary>
        /// The application metadata
        /// </summary>
        /// <value>The application metadata.</value>
        IApplicationMetaData ApplicationMetaData { get; }

        /// <summary>
        /// The link to the next version of the application
        /// </summary>
        /// <value>The upgrade link.</value>
        ResourceLink UpgradeLink { get; set; }

        /// <summary>
        /// If the optional upgrade has been actioned or not.
        /// </summary>
        /// <value><c>true</c> if upgrade actioned; otherwise, <c>false</c>.</value>
        bool UpgradeActioned { get; set; }

        /// <summary>
        /// The current session context
        /// </summary>
        /// <value>The current session.</value>
        ISession CurrentSession { get; }

        /// <summary>
        /// Gets the device information.
        /// </summary>
        /// <value>The device information.</value>
        IDevice Device { get; }
    }
}