﻿namespace myGovID.Business.Contract.Common.Application
{
    /// <summary>
    /// List of app capabilities
    /// </summary>
    public enum Capability
    {
        Facial_Verification_Passport,
        DVS_Driver_Licence,
        DVS_Medicare_Card,
        DVS_Passport,
        OCR_Passport,
        OCR_Driver_Licence,
        POI_Begin,
        POI_Resume,
        Authentication,
        Push_Notification,
        Menu_Settings,
        Menu_Account,
        Menu_About
    }
}
