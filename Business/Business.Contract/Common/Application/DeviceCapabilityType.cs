﻿namespace myGovID.Business.Contract.Common.Application
{
    /// <summary>
    /// List of device capabilities
    /// </summary>
    public enum DeviceCapabilityType
    {
        Fingerprint,
        FacialVerification,
        Camera
    }
}
