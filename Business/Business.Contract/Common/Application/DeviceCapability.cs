﻿namespace myGovID.Business.Contract.Common.Application
{
    public enum DeviceCapability
    {
        Touch,
        Fingerprint,
        FacialVerification,
        Camera
    }
}