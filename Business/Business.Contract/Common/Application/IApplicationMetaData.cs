﻿namespace myGovID.Business.Contract.Common.Application
{
    /// <summary>
    /// Application meta data.
    /// </summary>
    public interface IApplicationMetaData
    {
        /// <summary>
        /// Name of the app.
        /// </summary>
        /// <value>The name.</value>
        string Name { get; }
        /// <summary>
        /// Version of the app
        /// </summary>
        /// <value>The version.</value>
		string Version { get; }
        /// <summary>
        /// Build date of the app
        /// </summary>
        /// <value>The build date.</value>
		string BuildDate { get; }
        /// <summary>
        /// Gets the files directory.
        /// </summary>
        /// <value>The files directory.</value>
        string FilesDirectory { get; }
    }
}