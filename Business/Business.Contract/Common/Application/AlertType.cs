﻿namespace myGovID.Business.Contract.Common.Application
{
    /// <summary>
    /// Alert type.
    /// </summary>
    public enum AlertType
    {
        /// <summary>
        /// The warning.
        /// </summary>
        Warning,
        /// <summary>
        /// The error.
        /// </summary>
        Error
    }
}
