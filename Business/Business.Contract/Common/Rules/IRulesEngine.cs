﻿using System.Collections.Generic;
using myGovID.Business.Contract.Common.Process;

namespace myGovID.Business.Contract.Common.Rules
{
    public interface IRulesEngine
    {
        /// <summary>
        /// Finds the state of the app based on application links
        /// </summary>
        /// <returns>The state.</returns>
        BusinessState FindNextState(IProcessInformation processState);

        /// <summary>
        /// Returns all possible valid next BusinessStates from the current processState
        /// </summary>
        IEnumerable<BusinessState> AllStates(IProcessInformation processState);
    }
}
