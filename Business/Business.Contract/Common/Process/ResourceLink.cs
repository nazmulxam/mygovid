﻿namespace myGovID.Business.Contract.Common.Process
{
    /// <summary>
    /// Resource link.
    /// </summary>
    public class ResourceLink
    {
        public string Url { get; }
        public string Verb { get; }
        public AuthenticationType AuthenticationMethod { get; }
        public TargetType Target { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:myGovID.Business.Contract.Common.Process.ResourceLink"/> class.
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="verb">Verb</param>
        /// <param name="authenticationMethod">Authentication type</param>
        /// <param name="target">Target type</param>
        public ResourceLink(string url, string verb, AuthenticationType? authenticationMethod, TargetType? target)
        {
            Url = url;
            Verb = verb;
            AuthenticationMethod = authenticationMethod.Value;
            Target = target.Value;
        }
    }
}
