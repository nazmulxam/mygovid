﻿using System;
using System.Collections.Generic;

namespace myGovID.Business.Contract.Common.Process
{
    /// <summary>
    /// A representation of the current process state within the system
    /// </summary>
    public interface IProcess
    {
        /// <summary>
        /// The unique process identifier
        /// </summary>
        /// <value>The process identifier.</value>
        string ProcessId { get; set; }

        /// <summary>
        /// The current state of the process
        /// </summary>
        /// <value>The state of the process.</value>
        ProcessState ProcessState { get; set; }

        /// <summary>
        /// The current set of links associated with the process 
        /// ('what can the process do')
        /// </summary>
        /// <value>The current links.</value>
        IEnumerable<ResourceLink> CurrentLinks { get; }

        /// <summary>
        /// The current resource being acted upon in the process
        /// </summary>
        /// <value>The current resource.</value>
        string CurrentResource { get; set; }

        /// <summary>
        /// The processes identity proofing level 
        /// (this can be different to the identity's IP level)
        /// </summary>
        /// <value>The process IP Level.</value>
        IdentityProofingLevel ProcessIPLevel { get; set; }
    }
}
