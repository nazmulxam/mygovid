﻿namespace myGovID.Business.Contract.Common.Process
{
    /// <summary>
    /// Authentication type required for the links
    /// </summary>
    public enum AuthenticationType
    {
        User,
        /// <summary>
        /// The extension authentication type
        /// </summary>
        Extension, 
        /// <summary>
        /// The assurance authentication type
        /// </summary>
        Assurance, 
        /// <summary>
        /// The credential authentication type
        /// </summary>
        Credential,
        /// <summary>
        /// None authentication type
        /// </summary>
        None,
        /// <summary>
        /// The bespoke authentication type
        /// </summary>
        Bespoke
    }
}