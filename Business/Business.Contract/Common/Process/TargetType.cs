﻿namespace myGovID.Business.Contract.Common.Process
{
    /// <summary>
    /// Target type of the link
    /// </summary>
    public enum TargetType
    {
        /// <summary>
        /// The link representing the 'current' action
        /// </summary>
        Self, 
        /// <summary>
        /// The link representing the 'next' action
        /// </summary>
        Next, 
        /// <summary>
        /// The link representing the 'clone' action
        /// </summary>
        Clone, 
        /// <summary>
        /// The link representing the 'cancel' action
        /// </summary>
        Cancel, 
        /// <summary>
        /// The link representing the 'update' action.
        /// </summary>
        Update, 
        /// <summary>
        /// The link representing the 'process' action.
        /// </summary>
        Process
    }
}