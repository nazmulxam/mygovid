﻿using System;
namespace myGovID.Business.Contract.Common.Process
{
    /// <summary>
    /// Process state.
    /// </summary>
    public enum ProcessState
    {
        /// <summary>
        /// The process is locked.
        /// </summary>
        Locked, 
        /// <summary>
        /// Unable to verify the process
        /// </summary>
        UnableToVerify, 
        /// <summary>
        /// Process is looking okay.
        /// </summary>
        Okay
    }
}
