﻿using System;
namespace myGovID.Business.Contract.Common.Process
{
    /// <summary>
    /// Identity proofing level.
    /// </summary>
    public enum IdentityProofingLevel
    {
        None, 
        Unknown, 
        IP1, 
        IP2, 
        IP3, 
        IP4
    }
}
