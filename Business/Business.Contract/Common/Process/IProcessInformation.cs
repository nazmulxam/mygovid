﻿using System.Collections.Generic;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Common.Session;

namespace myGovID.Business.Contract.Common.Process
{
    /// <summary>
    /// A facade for making decisions within the application
    /// </summary>
    public interface IProcessInformation
    {
        /// <summary>
        /// The current process information
        /// </summary>
        /// <value>The current process.</value>
        IProcess CurrentProcess { get; }

        /// <summary>
        /// The authenticated state
        /// </summary>
        /// <value>The state of the authenticated.</value>
        AuthenticationState AuthenticatedState { get; }

        /// <summary>
        /// The system capabilities that are available in this version of the application
        /// </summary>
        /// <value>The capabilities.</value>
        IEnumerable<Capability> Capabilities { get; }

        /// <summary>
        /// The link to the next version of the application
        /// </summary>
        /// <value>The upgrade link.</value>
        ResourceLink UpgradeLink { get; }

        /// <summary>
        /// If the optional upgrade has been actioned.
        /// </summary>
        /// <value><c>true</c> if upgrade actioned; otherwise, <c>false</c>.</value>
        bool UpgradeActioned { get; }

        /// <summary>
        /// Gets the device capabilities.
        /// </summary>
        /// <value>The device capabilities.</value>
        IEnumerable<DeviceCapability> DeviceCapabilities { get; }
    }
}
