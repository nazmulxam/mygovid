﻿using System;
using System.Security.Principal;

namespace myGovID.Business.Contract.Common.Session
{
    public abstract class MyGovIdentity : IIdentity
    {
        public abstract string AuthenticationType { get; }

        public bool IsAuthenticated => throw new NotImplementedException(); // TODO: Hook up later

        public string Name { get { return string.Format("{0} {1}}", FirstName, LastName).Trim(); } }

        /// <summary>
        /// Gets the email address.
        /// </summary>
        /// <value>The email address.</value>
        public string EmailAddress { get; protected set; }

        /// <summary>
        /// Gets the first name.
        /// </summary>
        /// <value>The first name.</value>
        public string FirstName { get; protected set; }

        /// <summary>
        /// Gets the last name.
        /// </summary>
        /// <value>The last name.</value>
        public string LastName { get; protected set; }

        /// <summary>
        /// Gets the date of birth.
        /// </summary>
        /// <value>The date of birth.</value>
        public DateTime DateOfBirth { get; protected set; }

        /// <summary>
        /// Gets the identity identifier.
        /// </summary>
        /// <value>The identity identifier.</value>
        public string IdentityId { get; protected set; }
    }
}
