﻿using System.Threading.Tasks;

namespace myGovID.Business.Contract.Common.Session
{
    public interface ISessionManager
    {
        /// <summary>
        /// Login should set the AuthenticatedContext to a valid context
        /// </summary>
        Task<bool> LoginUsingPasswordAsync(string password);

        /// <summary>
        /// Logins using the keystore.
        /// </summary>
        /// <returns>true if successful</returns>
        Task<bool> LoginUsingKeystoreAsync();

        /// <summary>
        /// Logout should set the AuthenticatedContext to be unauthenticated
        /// </summary>
        void Logout(bool expired);
    }
}
