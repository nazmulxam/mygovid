﻿using System.Threading.Tasks;

namespace myGovID.Business.Contract.Common.Session
{
    public interface IAuthenticationProvider
    {
        Task<IMyGovPrincipal> GetIdentity(ICredential credential);
        AuthenticationState AuthenticatedState { get; }
    }
}