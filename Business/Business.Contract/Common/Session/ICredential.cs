﻿namespace myGovID.Business.Contract.Common.Session
{
    public interface ICredential
    {
        string CredentialId { get; }
    }
}