﻿namespace myGovID.Business.Contract.Common.Session
{
    /// <summary>
    /// Authenticated context.
    /// </summary>
    public interface IAuthenticationContext
    {
        /// <summary>
        /// The authenticated identity (can be null when not authenticated)
        /// </summary>
        /// <value>The current identity.</value>
        IMyGovPrincipal Current { get; }

        /// <summary>
        /// The authenticated state
        /// </summary>
        /// <value>The state of the authenticated.</value>
        AuthenticationState AuthenticatedState { get; }

        /// <summary>
        /// The identity of the credential used to authenticate
        /// </summary>
        /// <value>The credential identifier.</value>
        ICredential CurrentCredential { get; }
    }
}
