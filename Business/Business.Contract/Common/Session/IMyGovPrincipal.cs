﻿using System.Security.Principal;
using myGovID.Business.Contract.Common.Process;

namespace myGovID.Business.Contract.Common.Session
{
    public interface IMyGovPrincipal : IPrincipal
    {
        /// <summary>
        /// Gets my gov identity.
        /// </summary>
        /// <value>My gov identity.</value>
        MyGovIdentity MyGovIdentity { get; }

        /// <summary>
        /// Gets the IP Level.
        /// </summary>
        /// <value>The IPL evel.</value>
        IdentityProofingLevel IPLevel { get; }

        /// <summary>
        /// Determines if the identity has been issued a system identity
        /// </summary>
        /// <returns><c>true</c>, if system identity is available, <c>false</c> otherwise.</returns>
        bool HasSystemIdentity { get; }
    }
}