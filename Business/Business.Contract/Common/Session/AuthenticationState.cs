﻿namespace myGovID.Business.Contract.Common.Session
{
    /// <summary>
    /// Authentication state of the app
    /// </summary>
    public enum AuthenticationState
    {
        /// <summary>
        /// The unauthenticated state
        /// </summary>
        Unauthenticated, 
        /// <summary>
        /// The user has authenticated locally (password matches) but there 
        /// is no server side identity
        /// </summary>
        AuthenticatedLocal,
        /// <summary>
        /// The authenticated state
        /// </summary>
        Authenticated,
        /// <summary>
        /// The authentication expired state
        /// </summary>
        AuthenticationExpired
    }
}