﻿using myGovID.Business.Contract.Common.Process;

namespace myGovID.Business.Contract.Common.Session
{
    public interface ISession
    {
        /// <summary>
        /// Gets the session identity.
        /// </summary>
        /// <value>The session identity.</value>
        string SessionIdentity { get; }

        /// <summary>
        /// Gets the authenticated context.
        /// </summary>
        /// <value>The authenticated context.</value>
        IAuthenticationContext AuthenticatedContext { get; set; } // TODO: make the set more controllable.

        /// <summary>
        /// Gets the session process.
        /// </summary>
        /// <value>The session process.</value>
        IProcess SessionProcess { get; }
    }
}