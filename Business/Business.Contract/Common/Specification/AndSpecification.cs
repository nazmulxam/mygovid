﻿namespace myGovID.Business.Contract.Common.Specification
{
    public class AndSpecification<T> : CompositeSpecification<T> where T: class
    {
        private readonly ISpecification<T> _left;
        private readonly ISpecification<T> _right;

        /// <summary>
        /// Constructor that sets the left and right components of the And Equation
        /// </summary>
        /// <param name="left">ISpecification<T></param>
        /// <param name="right">ISpecification<T></param>
        public AndSpecification(ISpecification<T> left, ISpecification<T> right)
        {
            _left = left;
            _right = right;
        }

        /// <summary>
        /// Method that determines if the specification is satisfied 
        /// </summary>
        /// <param name="candidate">IProcess</param>
        /// <returns>True if both left and right specifications are satisfied</returns>
        public override bool IsSatisfiedBy(T candidate)
        {
            return _left.IsSatisfiedBy(candidate) && _right.IsSatisfiedBy(candidate);
        }
    }
}