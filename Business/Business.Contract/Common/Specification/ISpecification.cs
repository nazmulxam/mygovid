﻿namespace myGovID.Business.Contract.Common.Specification
{
    /// <summary>
    /// A specification designed to represent the specification design pattern
    /// and composite specification. 
    /// <see cref="https://www.martinfowler.com/apsupp/spec.pdf"/>
    /// </summary>
    public interface ISpecification<T> where T: class
    {
        /// <summary>
        /// Determines if a candidate satisfies this criteria
        /// </summary>
        /// <returns><c>true</c>, if candidate satisfies the criteria, <c>false</c> otherwise.</returns>
        /// <param name="candidate">Candidate.</param>
        bool IsSatisfiedBy(T candidate);

        /// <summary>
        /// And the specified other specification.
        /// </summary>
        /// <returns>A specification that has an and operation applied</returns>
        /// <param name="other">Other specification</param>
        ISpecification<T> And(ISpecification<T> other);

        /// <summary>
        /// Applies And not operation to the input specification
        /// </summary>
        /// <returns>Specification that has an And not operation applied</returns>
        /// <param name="other">Other specification</param>
        ISpecification<T> AndNot(ISpecification<T> other);

        /// <summary>
        /// Applies Or operation to the input specification.
        /// </summary>
        /// <returns>Specification that has an Or operation applied.</returns>
        /// <param name="other">Other.</param>
        ISpecification<T> Or(ISpecification<T> other);

        /// <summary>
        /// Applies Or not operation to the input specification.
        /// </summary>
        /// <returns>Specification that has an or not operation applied.</returns>
        /// <param name="other">Other.</param>
        ISpecification<T> OrNot(ISpecification<T> other);

        /// <summary>
        /// applies Not to the specification.
        /// </summary>
        /// <returns>The not.</returns>
        ISpecification<T> Not();
    }
}