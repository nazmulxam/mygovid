﻿namespace myGovID.Business.Contract.Common.Specification
{
    public class AndNotSpecification<T> : AndSpecification<T> where T: class
    {
        public AndNotSpecification(ISpecification<T> left, ISpecification<T> right) : base(left, right)
        {
        }
        public override bool IsSatisfiedBy(T candidate)
        {
            return !base.IsSatisfiedBy(candidate);
        }
    }
}
