﻿namespace myGovID.Business.Contract.Common.Specification
{
    public class OrNotSpecification<T> : OrSpecification<T> where T : class
    {
        public OrNotSpecification(ISpecification<T> left, ISpecification<T> right) : base(left, right)
        {
        }
        public override bool IsSatisfiedBy(T candidate)
        {
            return !base.IsSatisfiedBy(candidate);
        }
    }
}