﻿namespace myGovID.Business.Contract.Common.Specification
{
    public class NotSpecification<T> : CompositeSpecification<T> where T: class
    {
        private readonly ISpecification<T> _other;

        /// <summary>
        /// Constructor that sets the other components of the Not Equation
        /// </summary>
        /// <param name="other">ISpecification<T></param>
        public NotSpecification(ISpecification<T> other)
        {
            _other = other;
        }

        /// <summary>
        /// Method that determines if the specification is satisfied 
        /// </summary>
        /// <param name="candidate">IProcess</param>
        /// <returns>Returns True if the specification is not satisfied</returns>
        public override bool IsSatisfiedBy(T candidate)
        {
            return !_other.IsSatisfiedBy(candidate);
        }
    }
}