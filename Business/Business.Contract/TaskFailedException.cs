﻿using System;
using System.Collections.Generic;
using myGovID.Business.Contract.Common.Application;

namespace myGovID.Business.Contract
{
    public class TaskFailedException : Exception
    {
        public IEnumerable<Message> Messages { get; set; }
        public TaskFailedException(IEnumerable<Message> messages)
        {
            Messages = messages;
        }
    }
}