﻿using System.Threading.Tasks;
using myGovID.Business.Contract.Common.Session;

namespace myGovID.Business.Contract.Component
{
    public interface IAuthenticationComponent
    {
        /// <summary>
        /// Logins using password.
        /// </summary>
        /// <returns>Authentication context.</returns>
        /// <param name="password">Password Credential.</param>
        Task<IAuthenticationContext> LoginUsingPasswordAsync(string password);

        /// <summary>
        /// Logins using keystore.
        /// </summary>
        /// <returns>The usingkeystore.</returns>
        Task<IAuthenticationContext> LoginUsingKeystoreAsync();
    }
}