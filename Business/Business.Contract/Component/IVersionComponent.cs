﻿using System.Threading.Tasks;
using myGovID.Business.Contract.Model;
namespace myGovID.Business.Contract.Component
{
    public interface IVersionComponent
    {
        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <returns>
        /// The instance of business response that is generated after 
        /// the backend call is done.  Business response contains information 
        /// about the version response model as well as response error if applicable
        /// </returns>
        Task<VersionResponseModel> GetConfigurationAsync();

        /// <summary>
        /// Dismisses the upgrade.
        /// </summary>
        void DismissUpgrade();
    }
}