﻿using System.Threading.Tasks;
using myGovID.Business.Contract.Model;

namespace myGovID.Business.Contract.Component
{
    public interface IEmailVerificationComponent
    {
        /// <summary>
        /// Submits the email verification code
        /// </summary>
        /// <returns>
        /// The instance of business response that is generated after 
        /// the backend call is done.  Business response contains information 
        /// about the email verification response model as well as response error if applicable
        /// </returns>
        /// <param name="model">Model that contains email address and the verification code</param>
        Task<EmailVerificationResponseModel> SubmitEmailVerificationAsync(CaptureEmailVerificationModel model);
        /// <summary>
        /// Resends the email verification code.
        /// </summary>
        /// <returns>
        /// The instance of business response that is generated after 
        /// the backend call to resend email verification code is done.  
        /// Business response contains information about the poi response model 
        /// as well as response error if applicable
        /// </returns>
        Task<PoiResponseModel> ResendEmailVerificationCodeAsync();
        /// <summary>
        /// Changes the email.
        /// </summary>
        /// <returns>
        /// The instance of business response that is generated after 
        /// the backend call to change email is done.  Business response contains information 
        /// about the poi response model as well as response error if applicable
        /// </returns>
        Task<PoiResponseModel> ChangeEmailAsync();
    }
}