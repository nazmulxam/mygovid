﻿using System.Threading.Tasks;
using myGovID.Business.Contract.Common.Session;
using myGovID.Business.Contract.Model;

namespace myGovID.Business.Contract.Component
{
    public interface IOpenIdComponent
    {
        Task<OpenIdResponseModel> AuthenticateAsync(ICredential credential, string userIdentity);
    }
}