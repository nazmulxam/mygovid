using System.Threading.Tasks;
using myGovID.Business.Contract.Model;

namespace myGovID.Business.Contract.Component
{
    public interface IDocumentComponent
    {
        Task<PersonalDetailsBusinessResponseModel> CreatePersonalDetailsAsync(CapturePersonalDetailsModel personalDetailsModel);
        Task<MedicareDetailsBusinessResponseModel> CreateMedicareDetailsAsync(MedicareDetailsModel medicareDetailsModel);
        Task<DriverLicenceDocumentBusinessResponseModel> CreateDriverLicenceDetailsAsync(DriverLicenceModel driversLicenceModel);
        Task<PassportDocumentBusinessResponseModel> CreatePassportDetailsAsync(PassportModel passportModel);
    }
}
