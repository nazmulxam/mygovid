﻿using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using myGovID.Business.Contract.Common.Session;

namespace myGovID.Business.Contract.Component
{
    public interface ITokenServiceComponent
    {
        /// <summary>
        /// Get the value of specified tokenType.
        /// </summary>
        /// <returns>The value of the specified token type</returns>
        /// <param name="tokenType">Token type.</param>
        Task<string> CreateAsync(BusinessTokenType tokenType);

        /// <summary>
        /// Clear the tokens.
        /// </summary>
        void Clear();
    }
}