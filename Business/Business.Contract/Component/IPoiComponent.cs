﻿using System.Threading.Tasks;
using myGovID.Business.Contract.Model;

namespace myGovID.Business.Contract.Component
{
    public interface IPoiComponent
    {
        /// <summary>
        /// Starts the poi process (registration)
        /// </summary>
        /// <returns>
        /// The instance of business response that is generated after 
        /// the backend call is done.  Business response contains information 
        /// about the poi response model as well as response error if applicable
        /// </returns>
        /// <param name="model">Model that contains information about version of 
        /// accepted terms and conditions</param>
        Task<PoiResponseModel> StartPoiAsync(StartPoiModel model);

        /// <summary>
        /// Gets the poi.
        /// </summary>
        /// <returns>The poi.</returns>
        Task<GetPoiBusinessResponseModel> GetPoi();
    }
}