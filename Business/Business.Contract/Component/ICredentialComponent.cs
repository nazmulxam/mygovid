﻿using System.Threading.Tasks;
using myGovID.Business.Contract.Model;
namespace myGovID.Business.Contract.Component
{
    public interface ICredentialComponent
    {
        /// <summary>
        /// Creates the credential based on p10 and token
        /// </summary>
        /// <returns>The credential.</returns>
        /// <param name="token">Token.</param>
        /// <param name="p10">P10.</param>
        Task<CredentialResponseModel> CreateCredentialAsync(string token, string p10);
    }
}