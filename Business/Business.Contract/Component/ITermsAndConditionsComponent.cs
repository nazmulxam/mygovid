﻿using System.Threading.Tasks;
using myGovID.Business.Contract.Model;

namespace myGovID.Business.Contract.Component
{
    public interface ITermsAndConditionsComponent
    {
        /// <summary>
        /// Gets the terms and conditions.
        /// </summary>
        /// <returns>
        /// The instance of business response that is generated after 
        /// the backend call is done.  Business response contains information 
        /// about the 'Terms and Conditions' model as well as response error 
        /// if applicable
        /// </returns>
        Task<TermsAndConditionsResponseModel> GetTermsAndConditionsAsync();
    }
}