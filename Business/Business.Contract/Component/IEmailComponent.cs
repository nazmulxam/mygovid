﻿using System.Threading.Tasks;
using myGovID.Business.Contract.Model;

namespace myGovID.Business.Contract.Component
{
    public interface IEmailComponent
    {
        /// <summary>
        /// Submits the email address
        /// </summary>
        /// <returns>
        /// The instance of business response that is generated after 
        /// the backend call is done.  Business response contains information 
        /// about the poi response model as well as response error if applicable
        /// </returns>
        /// <param name="model">Email capture model that contains email address</param>
        Task<PoiResponseModel> SubmitEmailAsync(CaptureEmailModel model);
    }
}