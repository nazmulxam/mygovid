﻿using myGovID.Business.Contract.Model;
using System.Security.Cryptography;

namespace myGovID.Business.Contract.Component
{
    public interface ICryptoComponent
    {
        /// <summary>
        /// Creates the crypto keystore request.
        /// </summary>
        /// <returns>The request.</returns>
        /// <param name="alias">Credential identifier alias.</param>
        CryptoCreateResultModel CreateRequest(string alias);

        /// <summary>
        /// Creates the crypto keystore request.
        /// </summary>
        /// <returns>The request.</returns>
        CryptoCreateResultModel CreateRequest();

        /// <summary>
        /// Inserts the credential.
        /// </summary>
        /// <returns>The credential.</returns>
        /// <param name="requestId">Keystore Request identifier.</param>
        /// <param name="p7cbase64">P7cbase64.</param>
        /// <param name="alias">Credential identifier alias.</param>
        void InsertCredential(string requestId, string p7cbase64, string credentialId, string alias);

        /// <summary>
        /// Inserts the credential.
        /// </summary>
        /// <returns>The credential.</returns>
        /// <param name="requestId">Keystore Request identifier.</param>
        /// <param name="p7cbase64">P7cbase64.</param>
        void InsertCredential(string requestId, string p7cbase64, string credentialId);

        /// <summary>
        /// Clear this Crypto.
        /// </summary>
        void Clear();

        /// <summary>
        /// Gets the RSAParameters for Credential and password 
        /// </summary>
        /// <returns>The RSAParameters</returns>
        RSAParameters GetRSAParameters();

        /// <summary>
        /// Gets the credential identifier.
        /// </summary>
        /// <returns>The credential identifier.</returns>
        string GetCredentialId();

        /// <summary>
        /// Generates a random string adhering to a specification.
        /// </summary>
        /// <returns>The random string.</returns>
        /// <param name="specification">The rules around the secure random string.</param>
        string GenerateRandomString(RandomStringSpecification specification);
    }
}