﻿using System.Threading.Tasks;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Session;

namespace myGovID.Business.Contract.Component
{
    /// <summary>
    /// Local credential component
    /// </summary>
    public interface ILocalCredentialComponent
    {
        /// <summary>
        /// Register the device with specified password and biometric choice.
        /// </summary>
        /// <param name="email">Email address.</param>
        /// <param name="password">Password.</param>
        /// <param name="useBiometrics">If set to <c>true</c> use biometrics.</param>
        void RegisterUser(string email, string password, bool useBiometrics);

        /// <summary>
        /// Registers the credential for the device
        /// </summary>
        /// <returns>Success indicator</returns>
        Task<bool> RegisterCredentialAsync(string assuranceToken);

        /// <summary>
        /// Clear the local credentials. Clears entries from memory, file and secure storage.
        /// Also clears crypto key store
        /// </summary>
        void Clear();

        /// <summary>
        /// Checks is the user and device is registered.
        /// </summary>
        /// <returns><c>true</c>, if password, credential token and credential 
        /// id is present in the secure storage, <c>false</c> otherwise.</returns>
        bool IsRegistered();

        /// <summary>
        /// Checks if device has biometics and user has enabled this feature.
        /// </summary>
        /// <returns><c>true</c>, if device has biometics and user has enabled this feature 
        /// is present in the secure storage, <c>false</c> otherwise.</returns>
        bool ShowBiometricChallenge();

        /// <summary>
        /// Updates the identity.
        /// </summary>
        /// <param name="firstName">First name.</param>
        /// <param name="lastName">Last name.</param>
        /// <param name="dateOfBirth">Date of birth.</param>
        /// <param name="identityProofingLevel">Identity proofing level.</param>
        void UpdateIdentity(string firstName, string lastName, string dateOfBirth);

        /// <summary>
        /// Gets the user password.
        /// </summary>
        /// <returns>User's password</returns>
        string GetUserPassword();

        /// <summary>
        /// Login using the specified password.
        /// </summary>
        /// <returns><see langword="true"/> if successful</returns>
        /// <param name="password">Password.</param>
        Task<bool> LoginUsingPasswordAsync(string password);

        /// <summary>
        /// Logins the using keystore data.
        /// </summary>
        /// <returns><see langword="true"/> if successful</returns>
        Task<bool> LoginUsingKeystoreAsync();
    }
}
