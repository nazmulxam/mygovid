﻿using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using myGovID.Business.Contract.Common.Session;

namespace myGovID.Business.Contract.Component
{
    public interface ITokenStorageComponent
    {
        /// <summary>
        /// Set the value for specified tokenType.
        /// </summary>
        /// <param name="tokenType">Token type.</param>
        /// <param name="value">Value.</param>
        void Set(BusinessTokenType tokenType, string value);

        /// <summary>
        /// Get the value of specified tokenType.
        /// </summary>
        /// <returns>The value of the specified token type</returns>
        /// <param name="tokenType">Token type.</param>
        string Get(BusinessTokenType tokenType);

        /// <summary>
        /// Gets the claim value from the token - needs to be moved to another class later
        /// </summary>
        /// <returns>The token claim.</returns>
        /// <param name="token">Token.</param>
        /// <param name="claimName">Claim name.</param>
        string GetTokenClaim(string token, string claimName);

        /// <summary>
        /// Clear the tokens.
        /// </summary>
        void Clear();
    }
}