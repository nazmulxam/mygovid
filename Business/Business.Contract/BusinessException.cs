﻿using System;
using myGovID.Core.Exceptions;

namespace myGovID.Business.Contract
{
    public class BusinessException : CodedException
    {
        public BusinessException(BusinessExceptionCode exceptionCode, Exception innerException = null) : base(exceptionCode.ToString(), "", exceptionCode.GetDescription(), innerException)
        {
        }

        public BusinessException(string code, string title, string description, Exception innerException = null) : base(code, title, description, innerException)
        {
        }
    }
}