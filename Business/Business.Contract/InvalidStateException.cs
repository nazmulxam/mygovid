﻿namespace myGovID.Business.Contract
{
    public class InvalidStateException : BusinessException
    {
        public InvalidStateException(BusinessExceptionCode exceptionCode = BusinessExceptionCode.STA000001) : base(exceptionCode)
        {
        }
    }
}