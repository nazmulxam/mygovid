﻿namespace myGovID.Business.Contract
{
    public class ResourceNotFoundException : BusinessException
    {
        public ResourceNotFoundException(BusinessExceptionCode exceptionCode = BusinessExceptionCode.RES000001) : base(exceptionCode)
        {
        }
    }
}