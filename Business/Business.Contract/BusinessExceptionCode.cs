﻿namespace myGovID.Business.Contract
{
    public enum BusinessExceptionCode
    {
        LNK000001,
        RES000001,
        RES000002,
        STA000001,
        SYS000001,
        SYS000002,
        SYS000003,
        SYS000004,
        SYS000006,
        TKN000003,
        TKN000004,
        TKN000005,
        TKN000006,
        TKN000007,
        TKN000008,
        TKN000009,
        TKN000013,
        CLR000001,
        AUT000001,
        OIT000001
    }
    public static class Extensions
    {
        public static string GetDescription(this BusinessExceptionCode code)
        {
            switch (code)
            {
                case BusinessExceptionCode.LNK000001: return "Missing link.";
                case BusinessExceptionCode.RES000001: return "Resource not found.";
                case BusinessExceptionCode.RES000002: return "Resource mismatched.";
                case BusinessExceptionCode.STA000001: return "Cant find a state from the links.";
                case BusinessExceptionCode.SYS000001: return "Invalid error response.";
                case BusinessExceptionCode.SYS000002: return "Technical error."; // Technical error on the task
                case BusinessExceptionCode.SYS000003: return "Request Timeout.";
                case BusinessExceptionCode.SYS000004: return "Request failed.";
                case BusinessExceptionCode.SYS000006: return "Received verified status";
                case BusinessExceptionCode.TKN000003: return "Missing Credential Id";
                case BusinessExceptionCode.TKN000004: return "Identity information missing or incomplete.";
                case BusinessExceptionCode.TKN000005: return "Missing ACR value for identity token.";
                case BusinessExceptionCode.TKN000006: return "Missing first name value for identity token.";
                case BusinessExceptionCode.TKN000007: return "Missing last name value for identity token.";
                case BusinessExceptionCode.TKN000008: return "Missing date of birth value for identity token.";
                case BusinessExceptionCode.TKN000009: return "Missing email value for identity token.";
                case BusinessExceptionCode.TKN000013: return "Missing subject value for identity token.";
                case BusinessExceptionCode.CLR000001: return "Credential token expired. Reset App";
                case BusinessExceptionCode.AUT000001: return "Skipped password authentication.";
                case BusinessExceptionCode.OIT000001: return "Invalid token has been used";
                default: return "Business: Unknown error";
            }
        }
    }
}