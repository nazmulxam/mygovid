﻿namespace myGovID.Business.Contract
{
    public enum BusinessTokenType
    {
        Credential,
        ExtensionGrant,
        Bespoke
    }
}
