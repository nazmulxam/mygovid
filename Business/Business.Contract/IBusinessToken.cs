﻿namespace myGovID.Business.Contract
{
    public interface IBusinessToken
    {
        T Get<T>(string key);
        T Has<T>(string key);
    }
}
