﻿using System;
using myGovID.Core.Exceptions;

namespace myGovID.Business.Contract
{
    public class RecoverableDeadEndError : CodedException
    {
       
        public RecoverableDeadEndError(string code, string title, string description) : base(code, title, description)
        {
        }
    }
}
