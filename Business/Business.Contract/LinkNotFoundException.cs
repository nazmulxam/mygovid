﻿using myGovID.Core.Exceptions;

namespace myGovID.Business.Contract
{
    public class LinkNotFoundException: BusinessException
    {
        public LinkNotFoundException(BusinessExceptionCode exceptionCode = BusinessExceptionCode.LNK000001): base(exceptionCode)
        {
        }
    }
}