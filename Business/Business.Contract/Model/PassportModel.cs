﻿using System;
namespace myGovID.Business.Contract.Model
{
    public class PassportModel
    {
        public string GivenName { get; }
        public string FamilyName { get; }
        public string PassportNumber { get; }
        public DateTime BirthDate { get; }
        public string Gender { get; }
        public bool ConsentProvided { get; }


        public PassportModel(string givenName,
                             string familyName,
                             string passportNumber,
                             string gender,
                             DateTime birthDate,
                             bool consentProvided)
        {
            GivenName = givenName;
            FamilyName = familyName;
            PassportNumber = passportNumber;
            BirthDate = birthDate;
            Gender = gender;
            ConsentProvided = consentProvided;
        }
    }
}
