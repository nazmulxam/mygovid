﻿using System;
using System.Collections.Generic;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Common.Process;

namespace myGovID.Business.Contract.Model
{
    public class GetPoiBusinessResponseModel: PoiResponseModel
    {
        public string AcceptedTermsAndConditionsVersion { get; }
        public IdentityProofingLevel Strength { get; }
        public GetPoiBusinessResponseModel(string acceptedTermsVersion, string strength, IEnumerable<Message> messages, IEnumerable<ResourceLink> links, string processId, string status) : base(messages, links, processId, status)
        {
            AcceptedTermsAndConditionsVersion = acceptedTermsVersion;
            bool canConvert = Enum.TryParse(strength, true, out IdentityProofingLevel output);
            Strength = canConvert ? output : IdentityProofingLevel.Unknown;
        }
    }
}
