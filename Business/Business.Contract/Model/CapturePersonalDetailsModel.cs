﻿using System;
namespace myGovID.Business.Contract.Model
{
    public class CapturePersonalDetailsModel
    {
        public string FirstName { get; }
        public string LastName { get; }
        public DateTime BirthDate { get; }
        public CapturePersonalDetailsModel(string firstName, string lastName, DateTime birthDate)
        {
            if (string.IsNullOrWhiteSpace(lastName))
            {
                throw new ArgumentException(nameof(lastName));
            }
            FirstName = firstName;
            LastName = lastName;
            BirthDate = birthDate;
        }
    }
}
