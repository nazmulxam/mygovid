﻿using System;
using System.Security.Claims;

namespace myGovID.Business.Contract.Model
{
    public class OpenIdResponseModel
    {
        public ClaimsPrincipal Identity { get; }

        public string AccessToken { get; }

        public OpenIdResponseModel(ClaimsPrincipal identity, string accessToken)
        {
            Identity = identity;
            AccessToken = accessToken;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:myGovID.Business.Contract.Model.OpenIdResponseModel"/> class.
        /// This won't have an identity associated with it but the access token will be valid. Not suitable for
        /// authentication.
        /// </summary>
        /// <param name="accessToken">Access token.</param>
        public OpenIdResponseModel(string accessToken)
        {
            AccessToken = accessToken;
        }
    }
}
