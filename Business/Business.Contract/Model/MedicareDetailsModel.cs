﻿using System;
using EnsureThat;

namespace myGovID.Business.Contract.Model
{
    public class MedicareDetailsModel
    {
        public string NameLine1 { get; }
        public string NameLine2 { get; }
        public string NameLine3 { get; }
        public string NameLine4 { get; }
        public string CardNumber { get; }
        public string CardExpiry { get; }
        public string CardType { get; }
        public int IndividualReferenceNumber { get; }
        public bool ConsentProvided { get; }

        public MedicareDetailsModel(
            string nameLine1,
            string nameLine2,
            string nameLine3,
            string nameLine4,
            string cardNumber,
            string cardExpiry,
            string cardType,
            int individualReferenceNumber,
            bool consentProvided)
        {
            Ensure.String.IsNotNullOrWhiteSpace(nameLine1, nameof(nameLine1));
            Ensure.String.IsNotNullOrWhiteSpace(cardNumber, nameof(cardNumber));
            Ensure.String.IsNotNullOrWhiteSpace(cardExpiry, nameof(cardExpiry));
            Ensure.String.IsNotNullOrWhiteSpace(cardType, nameof(cardType));

            NameLine1 = nameLine1;
            NameLine2 = nameLine2;
            NameLine3 = nameLine3;
            NameLine4 = nameLine4;
            CardNumber = cardNumber;
            CardExpiry = cardExpiry;
            CardType = cardType;
            IndividualReferenceNumber = individualReferenceNumber;
            ConsentProvided = consentProvided;
        }
    }
}
