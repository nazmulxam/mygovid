﻿namespace myGovID.Business.Contract.Model
{
    /// <summary>
    /// Base interface for business response model.
    /// 
    /// Purpose of this interface is to provide common business layer contract 
    /// for all of the API responses.
    /// 
    /// The responses which contains links and messages must extend/implement 
    /// IBusinessResponseModel. All the other responses should implement 
    /// IBusinessApiResponseModel interface.
    /// </summary>
    public interface IBusinessApiResponseModel
    {
    }
}
