﻿using System;
namespace myGovID.Business.Contract.Model
{
    public class CredentialRequestModel
    {
        public string P10 { get; }
        public CredentialRequestModel(string p10)
        {
            if (string.IsNullOrEmpty(p10))
            {
                throw new ArgumentException(nameof(p10));
            }
            P10 = p10;
        }
    }
}
