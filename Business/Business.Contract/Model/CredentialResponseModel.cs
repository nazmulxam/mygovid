﻿namespace myGovID.Business.Contract.Model
{
    public class CredentialResponseModel : IBusinessApiResponseModel
    {
        public CredentialResponseModel(string credentialToken, string p7)
        {
            CredentialToken = credentialToken;
            P7 = p7;
        }

        public string CredentialToken { get; set; }

        public string P7 { get; set; }
    }
}
