﻿using System;
namespace myGovID.Business.Contract.Model
{
    public class DriverLicenceModel
    {
        public string DocumentNumber { get; }
        public string GivenNames { get; }
        public string MiddleName { get; }
        public string FamilyName { get; }
        public DateTime BirthDate { get; }
        public string State { get; }
        public bool ConsentProvided { get; }

        public DriverLicenceModel(
            string documentNumber,
            string givenNames,
            string middleName,
            string familyName,
            DateTime birthDate,
            string state,
            bool consentProvided)
        {
            DocumentNumber = documentNumber;
            GivenNames = givenNames;
            MiddleName = middleName;
            FamilyName = familyName;
            BirthDate = birthDate;
            State = state;
            ConsentProvided = consentProvided;
        }
    }
}
