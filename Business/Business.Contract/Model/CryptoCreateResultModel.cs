﻿using System;
namespace myGovID.Business.Contract.Model
{
    public class CryptoCreateResultModel
    {
        public string P10base64;
        public string RequestId;
        public CryptoCreateResultModel(string p10, string requestId)
        {
            if (string.IsNullOrEmpty(p10))
            {
                throw new ArgumentNullException(nameof(p10));
            }
            if (string.IsNullOrEmpty(requestId))
            {
                throw new ArgumentNullException(nameof(requestId));
            }
            P10base64 = p10;
            RequestId = requestId;
        }
    }
}
