﻿using System;
using System.Collections.Generic;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Common.Process;
namespace myGovID.Business.Contract.Model
{
    public class PersonalDetailsBusinessResponseModel : PoiResponseModel
    {
        public string FirstName { get; }
        public string LastName { get; }
        public DateTime BirthDate { get; }

        public PersonalDetailsBusinessResponseModel(
            string firstName, string lastName, DateTime birthDate,
            IEnumerable<Message> messages, IEnumerable<ResourceLink> links, string processId, string status) 
            :base(messages, links, processId, status)
        {

            FirstName = firstName;
            LastName = lastName;
            BirthDate = birthDate;
        }
    }
}
