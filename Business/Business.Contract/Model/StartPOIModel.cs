﻿using System;
namespace myGovID.Business.Contract.Model
{
    /// <summary>
    /// Start poi model which contains information about version of the
    /// terms and conditions accepted by the user.
    /// </summary>
    public class StartPoiModel
    {
        public string AcceptedVersion { get; }
        public StartPoiModel(string acceptedVersion)
        {
            if(string.IsNullOrEmpty(acceptedVersion)) 
            {
                throw new ArgumentNullException(nameof(acceptedVersion));
            }
            AcceptedVersion = acceptedVersion;
        }
    }
}
