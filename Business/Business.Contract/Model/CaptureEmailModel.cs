﻿using System;
namespace myGovID.Business.Contract.Model
{
    /// <summary>
    /// Business model that captures email address.
    /// </summary>
    public class CaptureEmailModel
    {
        public string EmailAddress { get; }
        public CaptureEmailModel(string emailAddress)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                throw new ArgumentException(nameof(emailAddress));
            }
            EmailAddress = emailAddress;
        }
    }
}
