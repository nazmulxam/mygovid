﻿using System.Collections.Generic;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Common.Process;

namespace myGovID.Business.Contract.Model
{
    public interface IBusinessResponseModel : IBusinessApiResponseModel
    {
        /// <summary>
        /// List of messages returned from backend call in case of any issues
        /// </summary>
        /// <value>The messages.</value>
        IEnumerable<Message> Messages { get; }
        /// <summary>
        /// List of links returned from backend call that are relevant to the 
        /// current state of app.
        /// </summary>
        /// <value>The links.</value>
        IEnumerable<ResourceLink> Links { get; }
    }
}