﻿using System;
namespace myGovID.Business.Contract.Model
{
    /// <summary>
    /// Business model that captures email address and verification code.
    /// </summary>
    public class CaptureEmailVerificationModel
    {
        public string EmailAddress { get; }
        public string VerificationCode { get; }
        public CaptureEmailVerificationModel(string emailAddress, string verificationCode)
        {
            if (string.IsNullOrEmpty(emailAddress))
            {
                throw new ArgumentNullException(nameof(emailAddress));
            }
            if (string.IsNullOrEmpty(verificationCode))
            {
                throw new ArgumentNullException(nameof(verificationCode));
            }
            EmailAddress = emailAddress;
            VerificationCode = verificationCode;
        }
    }
}
