﻿using System.Collections.Generic;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Common.Process;

namespace myGovID.Business.Contract.Model
{
    /// <summary>
    /// Version response model.
    /// This model contains:
    /// - List of messages from backend call if any issues are discovered
    /// - List of links returned from backend call that are relevant to the current state of app 
    /// - List of app capabilities
    /// - List of alerts
    /// </summary>
    public class VersionResponseModel : IBusinessResponseModel
    {
        public IEnumerable<Message> Messages { get; }
        public IEnumerable<ResourceLink> Links { get; }
        public IEnumerable<Capability> Capabilities { get; }
        public IEnumerable<Alert> Alerts { get; }
        public VersionResponseModel(IEnumerable<Message> messages, IEnumerable<ResourceLink> links, IEnumerable<Capability> capabilities, IEnumerable<Alert> alerts)
        {
            Messages = messages;
            Links = links;
            Capabilities = capabilities;
            Alerts = alerts;
        }
    }
}