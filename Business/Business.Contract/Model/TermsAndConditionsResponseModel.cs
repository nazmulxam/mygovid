﻿using System;
using System.Collections.Generic;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Common.Process;

namespace myGovID.Business.Contract.Model
{
    /// <summary>
    /// Terms and conditions response model.
    /// This model contains:
    /// - List of messages from backend call if any issues are discovered
    /// - List of links returned from backend call that are relevant to the current state of app 
    /// - Version of the terms and conditions 
    /// - Url to the terms and conditions 
    /// </summary>
    public class TermsAndConditionsResponseModel : IBusinessResponseModel
    {
        public IEnumerable<Message> Messages { get; }
        public IEnumerable<ResourceLink> Links => null;
        public string Version { get; }
        public string Url { get; }
        public TermsAndConditionsResponseModel(IEnumerable<Message> messages, string version, string url)
        {
            Messages = messages;
            Version = version;
            Url = url;
        }
    }
}