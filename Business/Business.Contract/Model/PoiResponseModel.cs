﻿using System;
using System.Collections.Generic;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Common.Process;
using System.Linq;
namespace myGovID.Business.Contract.Model
{
    /// <summary>
    /// Poi response model.
    /// This model contains:
    /// - List of messages from backend call if any issues are discovered
    /// - List of links returned from backend call that are relevant to the current state of app 
    /// - ProcessID 
    /// - Status of the api call
    /// </summary>
    public class PoiResponseModel: IBusinessResponseModel
    {
        public IEnumerable<Message> Messages { get; }
        public IEnumerable<ResourceLink> Links { get; }
        public string ProcessId { get; }
        public string Status { get; }
        public PoiResponseModel(IEnumerable<Message> messages, IEnumerable<ResourceLink> links, string processId, string status)
        {
            Messages = messages;
            Links = links;
            ProcessId = processId;
            Status = status;
        }
    }
}