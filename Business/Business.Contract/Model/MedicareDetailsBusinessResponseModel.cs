﻿using System.Collections.Generic;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Common.Process;
namespace myGovID.Business.Contract.Model
{
    public class MedicareDetailsBusinessResponseModel : PoiResponseModel
    {
        public MedicareDetailsBusinessResponseModel(
            IEnumerable<Message> messages,
            IEnumerable<ResourceLink> links,
            string processId, string status)
            : base(messages, links, processId, status)
        {
        }
    }
}
