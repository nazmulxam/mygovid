﻿﻿using System;
namespace myGovID.Business.Contract.Model
{
    public class RandomStringSpecification
    {
        /// <summary>
        /// The list of allowed characters when computing a random string. MUST be less than 255.
        /// </summary>
        /// <value>The character set.</value>
        public virtual char[] CharacterSet
        {
            get
            {
                return new[]
                {
                    'A'
                };
            }
        }

        /// <summary>
        /// The length of the secure random string that will be generated
        /// </summary>
        /// <value>The length.</value>
        public int Length { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:myGovID.Business.Contract.Model.RandomStringSpecification"/> class.
        /// </summary>
        /// <param name="requiredEntropy">Required entropy.</param>
        public RandomStringSpecification(double requiredEntropy)
        {
            Length = (int)(requiredEntropy / (Math.Log(CharacterSet.Length, 2)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:myGovID.Business.Contract.Model.RandomStringSpecification"/> class.
        /// </summary>
        /// <param name="length">Length.</param>
        public RandomStringSpecification(int length)
        {
            Length = length;
        }
    }
}