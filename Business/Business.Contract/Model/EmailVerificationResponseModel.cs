﻿using System;
using System.Collections.Generic;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Common.Process;

namespace myGovID.Business.Contract.Model
{
    public class EmailVerificationResponseModel : PoiResponseModel
    {
        public string VerificationCodeResult { get; }

        public string PoiAssuranceToken { get; }

        public EmailVerificationResponseModel(IEnumerable<Message> messages, IEnumerable<ResourceLink> links, string processId, string status,
                                              string verificationCodeResult, string poiAssuranceToken
                                             ) : base(messages, links, processId, status)
        {
            VerificationCodeResult = verificationCodeResult;
            PoiAssuranceToken = poiAssuranceToken;
        }
    }
}
