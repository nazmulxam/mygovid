﻿namespace myGovID.Business.Contract
{
    /// <summary>
    /// Business state that represents the state of the app
    /// </summary>
    public enum BusinessState
    {
        /// <summary>
        /// The account setup state where the user can choose either to register
        /// or recover an existing account.
        /// </summary>
        AccountSetup,
        /// <summary>
        /// The authenticate state
        /// </summary>
        Authenticate,
        /// <summary>
        /// The registration state
        /// </summary>
        Registration,
        /// <summary>
        /// The email state
        /// </summary>
        Email,
        /// <summary>
        /// The email verification state
        /// </summary>
        EmailVerification,
        /// <summary>
        /// The personal details state
        /// </summary>
        PersonalDetails,
        /// <summary>
        /// The capture credential state
        /// </summary>
        CaptureCredential,
        /// <summary>
        /// The dashboard state
        /// </summary>
        Dashboard,
        /// <summary>
        /// The default dashboard state
        /// </summary>
        DefaultDashboard,
        /// <summary>
        /// The anonymous dashboard state
        /// </summary>
        AnonymousDashboard,
        /// <summary>
        /// The passport state
        /// </summary>
        Passport,
        /// <summary>
        /// The driver license state
        /// </summary>
        DriverLicence,
        /// <summary>
        /// The medicare state
        /// </summary>
        Medicare,
        /// <summary>
        /// The fvs state
        /// </summary>
        FVS,
        /// <summary>
        /// The in process fvs state
        /// </summary>
        InProcessFVS,
        /// <summary>
        /// The explicit bind state
        /// </summary>
        ExplicitBind,
        /// <summary>
        /// The recoverable dead end state
        /// </summary>
        RecoverableDeadEnd,
        /// <summary>
        /// The version upgrade state
        /// </summary>
        VersionUpgrade,
        /// <summary>
        /// The Settings state
        /// </summary>
        Settings,
        /// Logout state.
        /// </summary>
        Logout
    }
}
