﻿namespace myGovID.Services.LocalAuthentication.Contract
{
    public enum LocalAuthenticationMethod
    {
        Face,
        Touch,
        None
    }
}
