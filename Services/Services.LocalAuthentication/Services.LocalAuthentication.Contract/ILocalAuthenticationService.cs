﻿using System;
namespace myGovID.Services.LocalAuthentication.Contract
{
    public interface ILocalAuthenticationService
    {
        /// <summary>
        /// Gets the supported authentication method.
        /// </summary>
        /// <returns>The supported authentication method.</returns>
        LocalAuthenticationMethod GetSupportedAuthenticationMethod();

        /// <summary>
        /// Authenticate
        /// </summary>
        /// <param name="action">Action to run on a success/failure authentication.</param>
        void Authenticate(Action<bool> action);
    }
}
