﻿using Android;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Hardware.Fingerprints;

namespace myGovID.Services.LocalAuthentication.Droid
{
    public class BiometricManager : BiometricManagerV23
    {
        protected BiometricManager(BiometricBuilder biometricBuilder)
        {
            Context = biometricBuilder.Context;
            Title = biometricBuilder.Title;
            Subtitle = biometricBuilder.Subtitle;
            Description = biometricBuilder.Description;
            NegativeButtonText = biometricBuilder.NegativeButtonText;
        }

        public void Authenticate(IBiometricCallback biometricCallback)
        {
            string error = null;
            if (Title == null)
            {
                error = "Biometric Dialog title cannot be null";
            }
            else if (Subtitle == null)
            {
                error = "Biometric Dialog subtitle cannot be null";
            }
            else if (Description == null)
            {
                error = "Biometric Dialog description cannot be null";
            }
            else if (NegativeButtonText == null)
            {
                error = "Biometric Dialog negative button text cannot be null";
            }
            if(error != null)
            {
                biometricCallback.OnBiometricAuthenticationInternalError(error);
                return;
            }
            if (!IsSdkVersionSupported())
            {
                biometricCallback.OnSdkVersionNotSupported();
                return;
            }
            if (!IsPermissionGranted(Context))
            {
                biometricCallback.OnBiometricAuthenticationPermissionNotGranted();
                return;
            }
            if (!IsHardwareSupported(Context))
            {
                biometricCallback.OnBiometricAuthenticationNotSupported();
                return;
            }
            if (!IsFingerprintAvailable(Context))
            {
                biometricCallback.OnBiometricAuthenticationNotAvailable();
                return;
            }
            DisplayBiometricDialog(biometricCallback);
        }
        private void DisplayBiometricDialog(IBiometricCallback biometricCallback)
        {
            if (IsBiometricPromptEnabled())
            {
                DisplayBiometricPrompt(biometricCallback);
            }
            else
            {
                DisplayBiometricPromptV23(biometricCallback);
            }
        }
        private void DisplayBiometricPrompt(IBiometricCallback biometricCallback)
        {
            new Android.Hardware.Biometrics.BiometricPrompt.Builder(Context)
                    .SetTitle(Title)
                    .SetSubtitle(Subtitle)
                    .SetDescription(Description)
                    .SetNegativeButton(NegativeButtonText, Context.MainExecutor, new BiometricPromptClickListener(biometricCallback, CancellationSignal))
                    .Build()
                    .Authenticate(CancellationSignal, Context.MainExecutor, new BiometricCallbackV28(biometricCallback));
        }
        private bool IsBiometricPromptEnabled()
        {
            return (Build.VERSION.SdkInt >= BuildVersionCodes.P);
        }
        private bool IsSdkVersionSupported()
        {
            return (Build.VERSION.SdkInt >= BuildVersionCodes.M);
        }
#pragma warning disable CS0618 // Type or member is obsolete
        private bool IsHardwareSupported(Context context)
        {
            FingerprintManager fingerprintManager = GetFingerprintManager(context);
            return fingerprintManager.IsHardwareDetected;
        }
        private bool IsFingerprintAvailable(Context context)
        {
            FingerprintManager fingerprintManager = GetFingerprintManager(context);
            return fingerprintManager.HasEnrolledFingerprints;
        }
        private bool IsPermissionGranted(Context context)
        {
            return context.CheckSelfPermission(Manifest.Permission.UseFingerprint) == Permission.Granted;
        }
        private FingerprintManager GetFingerprintManager(Context context)
        {
            return (FingerprintManager)Context.GetSystemService(Context.FingerprintService);
        }
#pragma warning restore CS0618 // Type or member is obsolete
        public sealed class BiometricPromptClickListener : Java.Lang.Object, IDialogInterfaceOnClickListener
        {
            private IBiometricCallback _biometricCallback;
            private CancellationSignal _cancellationSignal;
            public BiometricPromptClickListener(IBiometricCallback biometricCallback, CancellationSignal cancellationSignal)
            {
                _biometricCallback = biometricCallback;
                _cancellationSignal = cancellationSignal;
            }
            public void OnClick(IDialogInterface dialog, int which)
            {
                _biometricCallback.OnAuthenticationCancelled(_cancellationSignal);
            }
        }
        public sealed class BiometricBuilder
        {
            public string Title { get; private set; }
            public string Subtitle { get; private set; }
            public string Description { get; private set; }
            public string NegativeButtonText { get; private set; }
            public Context Context { get; private set; }
            public BiometricBuilder(Context context)
            {
                Context = context;
            }
            public BiometricBuilder SetTitle(string title)
            {
                Title = title;
                return this;
            }
            public BiometricBuilder SetSubtitle(string subtitle)
            {
                Subtitle = subtitle;
                return this;
            }
            public BiometricBuilder SetDescription(string description)
            {
                Description = description;
                return this;
            }
            public BiometricBuilder SetNegativeButtonText(string negativeButtonText)
            {
                NegativeButtonText = negativeButtonText;
                return this;
            }
            public BiometricManager Build()
            {
                return new BiometricManager(this);
            }
        }
    }
}