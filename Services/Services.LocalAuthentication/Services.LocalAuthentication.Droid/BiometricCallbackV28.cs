﻿using Android.Hardware.Biometrics;
using Android.Runtime;
using Java.Lang;

namespace myGovID.Services.LocalAuthentication.Droid
{
    public class BiometricCallbackV28: BiometricPrompt.AuthenticationCallback
    {
        private IBiometricCallback _biometricCallback;
        public BiometricCallbackV28(IBiometricCallback biometricCallback)
        {
            _biometricCallback = biometricCallback;
        }
        public override void OnAuthenticationSucceeded(BiometricPrompt.AuthenticationResult result)
        {
            base.OnAuthenticationSucceeded(result);
            _biometricCallback.OnAuthenticationSuccessful();
        }
        public override void OnAuthenticationHelp([GeneratedEnum] BiometricAcquiredStatus helpCode, ICharSequence helpString)
        {
            base.OnAuthenticationHelp(helpCode, helpString);
            _biometricCallback.OnAuthenticationHelp((int) helpCode, helpString.ToString());
        }
        public override void OnAuthenticationFailed()
        {
            base.OnAuthenticationFailed();
            _biometricCallback.OnAuthenticationFailed();
        }
    }
}