﻿using Android.Runtime;
using Java.Lang;
using Android.Hardware.Fingerprints;

namespace myGovID.Services.LocalAuthentication.Droid
{
#pragma warning disable CS0618 // Type or member is obsolete
    public sealed class AuthenticationCallbackV23 : FingerprintManager.AuthenticationCallback
    {
        private BiometricManagerV23 _biometricManager;
        private IBiometricCallback _biometricCallback;
        public AuthenticationCallbackV23(BiometricManagerV23 biometricManager, IBiometricCallback biometricCallback)
        {
            _biometricManager = biometricManager;
            _biometricCallback = biometricCallback;
        }
        public override void OnAuthenticationHelp([GeneratedEnum] FingerprintState helpCode, ICharSequence helpString)
        {
            base.OnAuthenticationHelp(helpCode, helpString);
            _biometricManager.UpdateStatus(helpString);
            _biometricCallback.OnAuthenticationHelp((int)helpCode, helpString.ToString());
        }
        public override void OnAuthenticationError([GeneratedEnum] FingerprintState errorCode, ICharSequence errString)
        {
            base.OnAuthenticationError(errorCode, errString);
            _biometricManager.UpdateStatus(errString);
            _biometricCallback.OnAuthenticationError((int)errorCode, errString.ToString());
        }
        public override void OnAuthenticationSucceeded(FingerprintManager.AuthenticationResult result)
        {
            base.OnAuthenticationSucceeded(result);
            _biometricManager.Dismiss();
            _biometricCallback.OnAuthenticationSuccessful();
        }
        public override void OnAuthenticationFailed()
        {
            base.OnAuthenticationFailed();
            _biometricManager.UpdateStatus("Not recognized");
            _biometricCallback.OnAuthenticationFailed();
        }
    }
#pragma warning restore CS0618 // Type or member is obsolete
}