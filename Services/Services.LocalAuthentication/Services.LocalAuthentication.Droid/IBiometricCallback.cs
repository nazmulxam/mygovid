﻿using Android.OS;

namespace myGovID.Services.LocalAuthentication.Droid
{
    public interface IBiometricCallback
    {
        void OnSdkVersionNotSupported();

        void OnBiometricAuthenticationNotSupported();

        void OnBiometricAuthenticationNotAvailable();

        void OnBiometricAuthenticationPermissionNotGranted();

        void OnBiometricAuthenticationInternalError(string error);

        void OnAuthenticationFailed();

        void OnAuthenticationCancelled(CancellationSignal cancellationSignal);

        void OnAuthenticationSuccessful();

        void OnAuthenticationHelp(int helpCode, string helpString);

        void OnAuthenticationError(int errorCode, string errString);
    }
}
