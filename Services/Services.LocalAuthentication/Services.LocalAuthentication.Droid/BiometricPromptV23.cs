﻿using System;
using Android.Content;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Widget;

namespace myGovID.Services.LocalAuthentication.Droid
{
    public class BiometricPromptV23: BottomSheetDialog {
        private Context _context;
        private Button _cancelButton;
        private TextView _titleView, _descriptionView, _subtitleView, _status;
        private IBiometricCallback _biometricCallback;
        private CancellationSignal _cancellationSignal;
        public BiometricPromptV23(Context context): this(context, Resource.Style.BottomSheetDialogTheme)
        {
        }
        public BiometricPromptV23(Context context, IBiometricCallback biometricCallback, CancellationSignal cancellationSignal) : this(context, Resource.Style.BottomSheetDialogTheme)
        {
            _biometricCallback = biometricCallback;
            _cancellationSignal = cancellationSignal;
        }

        public BiometricPromptV23(Context context, int theme): base(context, theme)
        {
            _context = context.ApplicationContext;
            SetDialogView();
        }
        protected BiometricPromptV23(Context context, bool cancelable, IDialogInterfaceOnCancelListener cancelListener): base(context, cancelable, cancelListener)
        {
        }

        private void SetDialogView() {
            SetContentView(Resource.Layout.BiometricPromptV23);
            _cancelButton = FindViewById<Button>(Resource.Id.CancelButton);
            _cancelButton.Click += (object sender, EventArgs e) => {
                Dismiss();
                _biometricCallback.OnAuthenticationCancelled(_cancellationSignal);
            };
            _status = FindViewById<TextView>(Resource.Id.Status);
            _titleView = FindViewById<TextView>(Resource.Id.Title);
            _subtitleView = FindViewById<TextView>(Resource.Id.Subtitle);
            _descriptionView = FindViewById<TextView>(Resource.Id.Description);
        }
        public new void SetTitle(string title) {
            _titleView.Text = title;
        }
        public void SetSubtitle(string subtitle) {
            _subtitleView.Text = subtitle;
        }
        public void SetDescription(string description) {
            _descriptionView.Text = description;
        }
        public void SetButtonText(string negativeButtonText) {
            _cancelButton.Text = negativeButtonText;
        }

        public void SetStatus(string status)
        {
            _status.Text = status;
        }
    }
}