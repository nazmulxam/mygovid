﻿using System;
using Android.Content;
using Android.Hardware.Fingerprints;
using Android.OS;
using Android.Runtime;
using Android.Security.Keystore;
using Java.Lang;
using Java.Security;
using Javax.Crypto;

namespace myGovID.Services.LocalAuthentication.Droid
{
#pragma warning disable CS0618 // Type or member is obsolete
    public class BiometricManagerV23
    {
        private static readonly string KEY_NAME = Guid.NewGuid().ToString();
        // We always use this keystore on Android.
        private static readonly string KEYSTORE_NAME = "AndroidKeyStore";
        // Should be no need to change these values.
        private static readonly string KEY_ALGORITHM = KeyProperties.KeyAlgorithmAes;
        private static readonly string BLOCK_MODE = KeyProperties.BlockModeCbc;
        private static readonly string ENCRYPTION_PADDING = KeyProperties.EncryptionPaddingPkcs7;
        private static readonly string TRANSFORMATION = KEY_ALGORITHM + "/" + BLOCK_MODE + "/" + ENCRYPTION_PADDING;
        private KeyStore _keystore;
        private Cipher _cipher;
        private KeyGenerator _keyGenerator;
        private FingerprintManager.CryptoObject _cryptoObject;

        protected Context Context;
        protected string Title;
        protected string Subtitle;
        protected string Description;
        protected string NegativeButtonText;
        private BiometricPromptV23 _biometricDialogV23;
        protected CancellationSignal CancellationSignal = new CancellationSignal();
        public void DisplayBiometricPromptV23(IBiometricCallback biometricCallback)
        {
            _keystore = KeyStore.GetInstance(KEYSTORE_NAME);
            _keystore.Load(null);
            _cipher = CreateCipher();
            _cryptoObject = new FingerprintManager.CryptoObject(_cipher);
            FingerprintManager fingerprintManager = (FingerprintManager) Context.GetSystemService(Context.FingerprintService);
            fingerprintManager.Authenticate(_cryptoObject, CancellationSignal, 0, new AuthenticationCallbackV23(this, biometricCallback), null);
            DisplayBiometricDialog(biometricCallback);
        }
        private void DisplayBiometricDialog(IBiometricCallback biometricCallback)
        {
            _biometricDialogV23 = new BiometricPromptV23(Context, biometricCallback, CancellationSignal);
            _biometricDialogV23.SetTitle(Title);
            _biometricDialogV23.SetSubtitle(Subtitle);
            _biometricDialogV23.SetDescription(Description);
            _biometricDialogV23.SetButtonText(NegativeButtonText);
            _biometricDialogV23.Show();
        }
        public void Dismiss()
        {
            if (_biometricDialogV23 != null)
            {
                _biometricDialogV23.Dismiss();
            }
        }
        private Cipher CreateCipher(bool retry = true)
        {
            IKey key = GetKey();
            Cipher cipher = Cipher.GetInstance(TRANSFORMATION);
            try
            {
                cipher.Init(CipherMode.EncryptMode | CipherMode.DecryptMode, key);
            }
            catch (KeyPermanentlyInvalidatedException e)
            {
                _keystore.DeleteEntry(KEY_NAME);
                if (retry)
                {
                    return CreateCipher(false);
                }
                throw e;
            }
            return cipher;
        }
        private IKey GetKey()
        {
            IKey secretKey;
            if (!_keystore.IsKeyEntry(KEY_NAME))
            {
                CreateKey();
            }
            secretKey = _keystore.GetKey(KEY_NAME, null);
            return secretKey;
        }
        private void CreateKey()
        {
            _keyGenerator = KeyGenerator.GetInstance(KeyProperties.KeyAlgorithmAes, KEYSTORE_NAME);
            KeyGenParameterSpec keyGenSpec =
                new KeyGenParameterSpec.Builder(KEY_NAME, KeyStorePurpose.Encrypt | KeyStorePurpose.Decrypt)
                    .SetBlockModes(BLOCK_MODE)
                    .SetEncryptionPaddings(ENCRYPTION_PADDING)
                    .SetUserAuthenticationRequired(true)
                    .Build();
            _keyGenerator.Init(keyGenSpec);
            _keyGenerator.GenerateKey();
        }
        public void UpdateStatus(ICharSequence status)
        {
            UpdateStatus(status.ToString());
        }
        public void UpdateStatus(string status)
        {
            _biometricDialogV23.SetStatus(status);
        }
    }
#pragma warning restore CS0618 // Type or member is obsolete
}