﻿using myGovID.Services.Timer.Contract;
using UIKit;

namespace myGovID.Services.Timer.iOS
{
    public abstract class TimerApplication: UIApplication
    {
        public abstract IDeviceActivityTimer DeviceActivityTimer { get; set; }
        public bool MonitorEvents { get; set; } = true;
        public override void SendEvent(UIEvent uievent)
        {
            base.SendEvent(uievent);
            if (MonitorEvents)
            {
                DeviceActivityTimer.OnUserInteraction();
            }
        }
    }
}