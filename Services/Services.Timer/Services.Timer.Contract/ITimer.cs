﻿using System;
namespace myGovID.Services.Timer.Contract
{
    public interface ITimer
    {
        void Reset();
        void Start(Action onExpire);
        void Pause();
		void Resume();
        void Expire();
        void Stop();
    }
}