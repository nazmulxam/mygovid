﻿namespace myGovID.Services.Timer.Contract
{
    public interface IDeviceActivityTimer : ITimer
    {
        /// <summary>
        /// Invoke when the user interacts with the app.
        /// </summary>
        void OnUserInteraction();

        /// <summary>
        /// Invoke when the device is locked.
        /// </summary>
        void OnDeviceLocked();
    }
}