﻿using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using myGovID.Services.Timer.Contract;

namespace myGovID.Services.Timer.Droid
{
    public abstract class TimerActivity : AppCompatActivity
    {
        public abstract IDeviceActivityTimer DeviceActivityTimer { get; set; }
        private PowerManager _powerManager;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            _powerManager = (PowerManager)GetSystemService(Context.PowerService);
        }
        public override void OnUserInteraction()
        {
            base.OnUserInteraction();
            DeviceActivityTimer.OnUserInteraction();
        }
        protected override void OnPause()
        {
            if (!_powerManager.IsInteractive)
            {
                DeviceActivityTimer.OnDeviceLocked();
            }
            base.OnPause();
        }
    }
}