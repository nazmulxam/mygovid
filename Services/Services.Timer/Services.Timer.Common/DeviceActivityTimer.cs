﻿using System;
using myGovID.Services.Timer.Contract;

namespace myGovID.Services.Timer.Common
{
    public class DeviceActivityTimer : IDeviceActivityTimer
    {
        private ITimer _timer { get; }

        public DeviceActivityTimer() : this(900000, 100)
        {
        }
        private DeviceActivityTimer(long numberOfMilliseconds, long intervalMilliseconds)
        {
            _timer = new Timer(numberOfMilliseconds, intervalMilliseconds);
        }

        public void OnUserInteraction()
        {
            _timer.Reset();
        }

        public void OnDeviceLocked()
        {
            _timer.Expire();
        }

        public void Reset()
        {
            _timer.Reset();
        }

        public void Start(Action onExpire)
        {
            _timer.Start(onExpire);
        }

        public void Pause()
        {
            _timer.Pause();
        }

        public void Resume()
        {
            _timer.Resume();
        }

        public void Expire()
        {
            _timer.Expire();
        }

        public void Stop()
        {
            _timer.Stop();
        }
    }
}