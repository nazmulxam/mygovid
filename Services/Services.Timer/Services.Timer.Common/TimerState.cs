﻿namespace myGovID.Services.Timer.Common
{
    public enum TimerState
    {
        Idle, Running, Expired
    }
}
