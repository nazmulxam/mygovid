﻿using System;
using myGovID.Services.Timer.Contract;

namespace myGovID.Services.Timer.Common
{
    public class Timer: ITimer
    {
        private System.Threading.Timer _timer;
        private readonly long _numberOfMilliseconds;
        private readonly long _intervalMilliseconds;
        private Action _onExpire; // TODO: Weak reference doesnt tend to work nicely in ios and android.
        private TimerState _timerState = TimerState.Idle;
        private long _elapsedMilliseconds;
        public Timer(long numberOfMilliseconds, long intervalMilliseconds = 100)
        {
            _numberOfMilliseconds = numberOfMilliseconds;
            _intervalMilliseconds = intervalMilliseconds;
        }
        public void Expire()
        {
            Console.WriteLine("Timer expired.");
            if(_timerState == TimerState.Idle)
            {
                Reset();
                return;
            }
            _onExpire?.Invoke();
            _timerState = TimerState.Expired;
            _timer?.Dispose();
            _timer = null;
        }
        public void Pause()
        {
            _timerState = TimerState.Idle;
        }
        public void Resume()
        {
            _timerState = TimerState.Running;
        }
        public void Reset()
        {
            _timerState = _timerState == TimerState.Expired ? TimerState.Idle : _timerState;
            _elapsedMilliseconds = 0;
            _timer?.Change(0, _intervalMilliseconds);
        }
        public void Start(Action onExpire)
        {
            _onExpire = onExpire;
            _timerState = TimerState.Running;
            _elapsedMilliseconds = 0;
            if (_timer != null)
            {
                _timer.Dispose();
            }
            _timer = new System.Threading.Timer((state) => {
                if (_timerState == TimerState.Running)
                {
                    Console.WriteLine($"Timer elapsed: {_elapsedMilliseconds} of {_numberOfMilliseconds}");
                    _elapsedMilliseconds += _intervalMilliseconds;
                    if(_elapsedMilliseconds >= _numberOfMilliseconds)
                    {
                        Expire();
                    }
               }
            }, this, 0, _intervalMilliseconds);
        }
        public void Stop()
        {
            _timer.Dispose();
            _timer = null;
            _onExpire = null;
            _timerState = TimerState.Idle;
        }
    }
}