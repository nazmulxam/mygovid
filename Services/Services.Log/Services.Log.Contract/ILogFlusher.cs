﻿using System.Threading.Tasks;

namespace myGovID.Services.Log.Contract
{
    public interface ILogFlusher
    {
        Task Flush(string message);
    }
}