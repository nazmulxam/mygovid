﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace myGovID.Services.Log.Contract
{
    public interface ILogger
    {
        void Verbose([CallerMemberName] string category = "", string message = "", params object[] args);
        void Debug([CallerMemberName] string category = "", string message = "", params object[] args);
        void Info([CallerMemberName] string category = "", string message = "", params object[] args);
        void Warning([CallerMemberName] string category = "", string message = "", params object[] args);
        void Error([CallerMemberName] string category = "", string message = "", params object[] args);
        void Fatal([CallerMemberName] string category = "", string message = "", params object[] args);
        bool IsEnabled(LogLevel level);
        void Log([CallerMemberName] string category = "", LogLevel level = LogLevel.Verbose, Exception exception = null, string message = "", params object[] args);
        Task FlushAsync();
    }
}