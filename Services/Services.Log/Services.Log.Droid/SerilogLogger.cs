﻿using System;
using myGovID.Services.Log.Contract;
using Serilog;
using System.Threading.Tasks;
using System.IO;
using Serilog.Formatting.Json;
using System.Runtime.CompilerServices;
using Serilog.Events;
using System.Text;
using Serilog.Sinks.RollingFile;

namespace myGovID.Services.Log.Droid
{
    public class SerilogLogger : Contract.ILogger
    {
        private readonly ILogFlusher _flusher;
        public SerilogLogger(ILogFlusher flusher, LogLevel level)
        {
            _flusher = flusher;
            LogEventLevel logEventLevel = GetLogEventLevel(level);
            LoggerConfiguration config = new LoggerConfiguration()
                .WriteTo.Async(a => a.Sink(new RollingFileSink(Path.Combine(GetLogPath(), "log-{Date}.txt"), new JsonFormatter(), null, 7, Encoding.UTF8, false, false)), 500, true)
                .WriteTo.AndroidLog();
            config.MinimumLevel.Is(logEventLevel);
            Serilog.Log.Logger = config.CreateLogger();
            RegisterForCrashes();
        }
        public void Debug([CallerMemberName] string category = "", string message = "", params object[] args)
        {
            Serilog.Log.Debug(category + " - " + message, args);
        }
        public void Error([CallerMemberName] string category = "", string message = "", params object[] args)
        {
            Serilog.Log.Error(category + " - " + message, args);
        }
        public void Fatal([CallerMemberName] string category = "", string message = "", params object[] args)
        {
            Serilog.Log.Fatal(category + " - " + message, args);
        }
        public async Task FlushAsync()
        {
            Serilog.Log.CloseAndFlush();
            await _flusher?.Flush(GetLogs(DateTime.Now));
        }
        public void Info([CallerMemberName] string category = "", string message = "", params object[] args)
        {
            Serilog.Log.Information(category + " - " + message, args);
        }
        public void Verbose([CallerMemberName] string category = "", string message = "", params object[] args)
        {
            Serilog.Log.Verbose(category + " - " + message, args);
        }
        public void Warning([CallerMemberName] string category = "", string message = "", params object[] args)
        {
            Serilog.Log.Warning(category + " - " + message, args);
        }
        private string GetLogs(DateTime date)
        {
            string fileName = Path.Combine(GetLogPath(), string.Format("log-{0}.txt", date.ToString("yyyyMMdd")));
            return File.ReadAllText(fileName);
        }
        private string GetLogPath()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        }
        private void RegisterForCrashes()
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, args) => {
                Exception exception = (Exception)args.ExceptionObject;
                Serilog.Log.Fatal(exception, "Unhandled crash");
            };
            TaskScheduler.UnobservedTaskException += (sender, args) => {
                Exception exception = args.Exception;
                Serilog.Log.Fatal(exception, "Unhandled task exception");
            };
        }
        public bool IsEnabled(LogLevel level)
        {
            return Serilog.Log.IsEnabled(GetLogEventLevel(level));
        }
        public void Log([CallerMemberName] string category = "", LogLevel level = LogLevel.Verbose, Exception exception = null, string message = "", params object[] args)
        {
            Serilog.Log.Write(GetLogEventLevel(level), exception, category + " - " + message, args);
        }
        private LogEventLevel GetLogEventLevel(LogLevel level)
        {
            switch(level)
            {
                case LogLevel.Debug: return LogEventLevel.Debug;
                case LogLevel.Error: return LogEventLevel.Error;
                case LogLevel.Fatal: return LogEventLevel.Fatal;
                case LogLevel.Information: return LogEventLevel.Information;
                case LogLevel.Verbose: return LogEventLevel.Verbose;
                default: return LogEventLevel.Warning;
            }
        }
    }
}