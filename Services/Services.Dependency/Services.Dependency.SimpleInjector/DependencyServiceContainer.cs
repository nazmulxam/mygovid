﻿using System;
using SimpleInjector;
using myGovID.Services.Dependency.Contract;

namespace myGovID.Services.Dependency.SimpleInjector
{
    public class DependencyServiceContainer: IDependencyService
    {
        protected readonly Container Container = new Container();

        /// <summary>
        /// Register all the dependencies for the specified scheme.
        /// </summary>
        /// <param name="scheme">Scheme.</param>
        public virtual void Register(Scheme scheme) {}

        /// <summary>
        /// Register the specified instance.
        /// </summary>
        /// <param name="instance">Instance.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public void Register<T>(T instance)
        {
            Container.RegisterInstance(typeof(T), instance);
        }

        /// <summary>
        /// Register this relationship.
        /// </summary>
        /// <typeparam name="I">The interface type.</typeparam>
        /// <typeparam name="T">The implementation type.</typeparam>
        public void Register<I, T>(Contract.Scope scope = Contract.Scope.Transient) where T: I
        {
            Container.Register(typeof(I), typeof(T), scope == Contract.Scope.Transient ? Lifestyle.Transient : Lifestyle.Singleton);
        }

        public void RegisterConditional<I, T>(string paramName, Contract.Scope scope = Contract.Scope.Transient) where T: I
        {
            Container.RegisterConditional(typeof(I), typeof(T), scope == Contract.Scope.Transient ? Lifestyle.Transient : Lifestyle.Singleton, condition => condition.Consumer.Target.Parameter.Name.Contains(paramName));
        }

        /// <summary>
        /// Resolve this instance.
        /// </summary>
        /// <returns>The resolve.</returns>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public T Resolve<T>() where T : class
        {
            return Container.GetInstance<T>();
        }

        /// <summary>
        /// Resolve the specified type.
        /// </summary>
        /// <returns>The resolve.</returns>
        /// <param name="type">Type.</param>
        public object Resolve(Type type)
        {
            return Container.GetInstance(type);
        }
    }
}