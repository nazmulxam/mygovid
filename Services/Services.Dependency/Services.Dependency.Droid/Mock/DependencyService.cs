﻿using myGovID.Services.Dependency.Common.Mock;
using myGovID.Services.Dependency.Contract;
using myGovID.Services.Log.Contract;
using myGovID.Services.Log.Droid;
using myGovID.Services.Uri.Contract;
using myGovID.Services.Uri.Droid;

namespace myGovID.Services.Dependency.Droid
{
    public class DependencyService : MockDependencyServiceCommon
    {
        public static DependencyService Shared = new DependencyService();
        private DependencyService()
        {
            Register<IDependencyService>(this);
        }
        protected override void InternalRegister(Scheme scheme)
        {
            base.InternalRegister(scheme);
            // -- Logging
            ILogger seriLogger = new SerilogLogger(null, LogLevel.Information);
            Register<ILogger>(seriLogger);
            Register<IUriService, UriService>(Scope.Singleton);
        }
    }
}
