﻿using myGovID.Services.Dependency.Common;
using myGovID.Services.Dependency.Contract;
using myGovID.Services.Uri.Contract;
using myGovID.Services.Uri.Droid;
using myGovID.Services.Log.Droid;
using myGovID.Services.Log.Contract;
using myGovID.Services.Crypto.Contract;
using myGovID.Services.Storage.Contract;
using myGovID.Services.Storage.Droid;
using myGovID.Services.Crypto.Droid;

namespace myGovID.Services.Dependency.Droid
{
    public class DependencyService : DependencyServiceCommon
    {
        public static DependencyService Shared = new DependencyService();
        private DependencyService()
        {
            Register<IDependencyService>(this);
        }
        protected override void InternalRegister(Scheme scheme)
        {
            base.InternalRegister(scheme);
            // -- Logging
            ILogger seriLogger = new SerilogLogger(null, LogLevel.Information);
            Register<ILogger>(seriLogger);
            Register<IUriService, UriService>(Scope.Singleton);
        }
        public override void RegisterCrypto()
        {
            Register<ICryptoService, CryptoService>(Scope.Singleton);
        }
        public override void RegisterStorage()
        {
            RegisterConditional<IStorage, MemoryStorage>("memoryStorage", Scope.Singleton);
            RegisterConditional<IStorage, FileStorage>("fileStorage", Scope.Singleton);
            RegisterConditional<IStorage, SecureStorage>("secureStorage", Scope.Singleton);
        }
    }
}
