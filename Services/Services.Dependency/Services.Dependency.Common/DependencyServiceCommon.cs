﻿using myGovID.Business.Contract.Common.Application;
using myGovID.Presentation;
using myGovID.Presentation.Contract;
using myGovID.Services.Dependency.Contract;
using myGovID.Services.Dependency.SimpleInjector;
using myGovID.Presentation.States;
using myGovID.Business.Contract.Common.Rules;
using myGovID.Business;
using myGovID.Business.Contract.Component;
using myGovID.Business.Component;
using myGovID.Services.Api.Contract;
using myGovID.Business.Contract.Common.Process;
using myGovID.Services.Event.Contract;
using myGovID.Services.Event.MVVMLight;
using myGovID.Business.Contract.Common.Session;
using myGovID.Business.Session;
using myGovID.Services.Timer.Contract;
using myGovID.Services.Timer.Common;
using myGovID.Services.Api.Http;
using myGovID.Services.Http.Contract;
using myGovID.Services.Http;
using myGovID.Services.OpenId.Contract;
using myGovID.Services.OpenId.Implementation;

namespace myGovID.Services.Dependency.Common
{
    public abstract class DependencyServiceCommon : DependencyServiceContainer
    {
        public override void Register(Scheme scheme)
        {
            InternalRegister(scheme);
        }
        protected virtual void InternalRegister(Scheme scheme)
        {
            RegisterHttpServices();
            Register<IApplication, Application>(Scope.Singleton);
            Register<IProcessInformation, Application>(Scope.Singleton);

            // Presentation
            Register<IStepRepository, DefaultStepRepository>(Scope.Singleton);
            // States
            Register<DeadEndState, DeadEndState>(Scope.Singleton);
            Register<RecoverableDeadEndState, RecoverableDeadEndState>(Scope.Singleton);
            Register<AccountSetupState, AccountSetupState>(Scope.Singleton);
            Register<AuthenticateState, AuthenticateState>(Scope.Singleton);
            Register<VersionUpgradeState, VersionUpgradeState>(Scope.Singleton);
            Register<InitialRegistrationState, InitialRegistrationState>(Scope.Singleton);
            Register<EmailState, EmailState>(Scope.Singleton);
            Register<EmailVerificationState, EmailVerificationState>(Scope.Singleton);
            Register<PersonalDetailsState, PersonalDetailsState>(Scope.Singleton);

            // Business
            Register<IRulesEngine, RulesEngine>(Scope.Singleton);
            Register<ISession, Session>(Scope.Singleton);
            Register<ISessionManager, TimedSessionManager>(Scope.Singleton);
            RegisterConditional<IAuthenticationProvider, OpenIdAuthenticationProvider>("openIdAuthenticationProvider", Scope.Singleton);
            RegisterConditional<IAuthenticationProvider, SelfAssertedAuthenticationProvider>("selfAssertedAuthenticationProvider", Scope.Singleton);
            RegisterComponents();

            //Services
            Register<IEventService, MvvmEventService>(Scope.Singleton);
            Register<IDeviceActivityTimer, DeviceActivityTimer>(Scope.Singleton); RegisterStorage();
            Register<IApiService, ApiService>(Scope.Singleton);
            Register<IOpenIdService, OpenIdService>(Scope.Singleton);

            RegisterCrypto();
        }
        protected virtual void RegisterHttpServices()
        {
            Register<IHttpService, HttpService>(Scope.Singleton);
        }
        public abstract void RegisterStorage();
        public abstract void RegisterCrypto();
        private void RegisterComponents()
        {
            // Components
            Register<IAuthenticationComponent, AuthenticationComponent>(Scope.Singleton);
            Register<IVersionComponent, VersionComponent>(Scope.Singleton);
            Register<ITermsAndConditionsComponent, TermsAndConditionsComponent>(Scope.Singleton);
            Register<IPoiComponent, PoiComponent>(Scope.Singleton);
            Register<IEmailComponent, EmailComponent>(Scope.Singleton);
            Register<IEmailVerificationComponent, EmailVerificationComponent>(Scope.Singleton);
            Register<ICredentialComponent, CredentialComponent>(Scope.Singleton);
            Register<ICryptoComponent, CryptoComponent>(Scope.Singleton);
            Register<IDocumentComponent, DocumentComponent>(Scope.Singleton);
            Register<ITokenStorageComponent, TokenStorageComponent>(Scope.Singleton);
            Register<IOpenIdComponent, OpenIdComponent>(Scope.Singleton);
            Register<ITokenServiceComponent, TokenServiceComponent>(Scope.Singleton);
            RegisterOverridableComponents();
        }
        protected virtual void RegisterOverridableComponents()
        {
            Register<ILocalCredentialComponent, LocalCredentialComponent>(Scope.Singleton);
        }
    }
}