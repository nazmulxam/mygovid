﻿using myGovID.Business.Contract.Component;
using myGovID.Services.Api.Contract;
using myGovID.Services.Crypto.Contract;
using myGovID.Services.Dependency.Contract;
using myGovID.Services.Http.Contract;
using myGovID.Services.Http.Mock;
using myGovID.Services.Storage.Contract;

namespace myGovID.Services.Dependency.Common.Mock
{
    public class MockDependencyServiceCommon : DependencyServiceCommon
    {
        protected override void RegisterHttpServices()
        {
            Register<MockProcessor, MockProcessor>(Scope.Singleton);
            Register<IHttpService, MockHttpService>(Scope.Singleton);
        }
        public override void RegisterStorage()
        {
            Register<IStorage, Business.Mock.MockStorage>(Scope.Singleton);
        }
        public override void RegisterCrypto()
        {
            Register<ICryptoService, Business.Mock.MockCryptoService>(Scope.Singleton);
        }
        protected override void RegisterOverridableComponents()
        {
            Register<ILocalCredentialComponent, Business.Mock.MockLocalCredentialComponent>(Scope.Singleton);
        }
    }
}
