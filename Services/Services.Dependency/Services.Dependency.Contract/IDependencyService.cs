﻿using System;
namespace myGovID.Services.Dependency.Contract
{
    public interface IDependencyService
    {
        /// <summary>
        /// Register this relationship.
        /// </summary>
        /// <typeparam name="I">The interface type.</typeparam>
        /// <typeparam name="T">The implementation type.</typeparam>
        void Register<I, T>(Scope scope = Scope.Transient) where T : I;

        /// <summary>
        /// Register the specified instance.
        /// </summary>
        /// <param name="instance">Instance.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        void Register<T>(T instance);

        /// <summary>
        /// Register the specified scheme.
        /// </summary>
        /// <param name="scheme">Scheme.</param>
        void Register(Scheme scheme);

        /// <summary>
        /// Resolve this instance.
        /// </summary>
        /// <returns>The resolve.</returns>
        /// <typeparam name="T">The class type</typeparam>
        T Resolve<T>() where T : class;

        /// <summary>
        /// Resolve the specified type.
        /// </summary>
        /// <returns>The resolve.</returns>
        /// <param name="type">Type.</param>
        object Resolve(Type type);
    }
}
