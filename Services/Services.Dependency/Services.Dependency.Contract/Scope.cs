﻿namespace myGovID.Services.Dependency.Contract
{
    public enum Scope
    {
        Singleton, Transient
    }
}