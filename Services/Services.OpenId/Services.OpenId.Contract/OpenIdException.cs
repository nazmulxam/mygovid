﻿using System;
using myGovID.Core.Exceptions;

namespace myGovID.Services.OpenId.Contract
{
    public class OpenIdException : CodedException
    {
        public OpenIdException(OpenIdErrorCode code, string description)
            : base(code.ToString(), code.GetDescription(), description)
        {
        }

        public OpenIdException(OpenIdErrorCode code)
            : this(code, code.GetDescription())
        {
        }

    }
}
