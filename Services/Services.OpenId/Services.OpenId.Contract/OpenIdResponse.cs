﻿using System;

namespace myGovID.Services.OpenId.Contract
{
    /// <summary>
    /// The response from the OpenId Service 
    /// </summary>
    public class OpenIdResponse
    {
        /// <summary>
        /// The response from a successful call to OpenId
        /// </summary>
        public TokenResponse Response { get; }

        public OpenIdResponse(TokenResponse response)
        {
            Response = response;
        }

    }
}
