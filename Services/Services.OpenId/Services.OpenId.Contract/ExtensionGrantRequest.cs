﻿using System;

namespace myGovID.Services.OpenId.Contract
{
    /// <summary>
    /// Request class containing the Assertion and Client Assertion tasks
    /// </summary>
    public class ExtensionGrantRequest : OpenIdRequest
    {
        /// <summary>
        /// The AssertionToken (User) token
        /// </summary>
        public string Assertion { get; }

        /// <summary>
        /// The ClientAssertion (Device) token
        /// </summary>
        public string ClientAssertion { get; }

        public ExtensionGrantRequest(string assertion, string clientAssertion)
        {
            Assertion = assertion;
            ClientAssertion = clientAssertion;
        }

        /// <summary>
        /// Get the OpenId content payload
        /// </summary>
        /// <returns>The formatted request content. Includes the Assertion and ClientAssertion tokens</returns>
        public override string GetPayload()
        {
            return
                "grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer&" +
                $"assertion=({Assertion})&" +
                $"client_assertion=({ClientAssertion})&" + 
                "client_assertion_type=urn%3Aietf%3Aparams%3Aoauth%3Aclient-assertion-type%3Ajwt-bearer";
        }
    }
}
