﻿using System;
using System.Collections.Generic;

namespace myGovID.Services.OpenId.Contract
{
    /// <summary>
    /// Request object for OpenId service calls
    /// </summary>
    internal interface IOpenIdRequest
    {
        /// <summary>
        /// Get the Request Payload for the OpenId call
        /// </summary>
        /// <returns></returns>
        string GetPayload();

        /// <summary>
        /// Get the Request Headers for the OpenId call
        /// </summary>
        /// <returns></returns>
        Dictionary<string, string> GetHeaders();
    }
}
