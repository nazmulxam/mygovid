﻿using System;
using Newtonsoft.Json;

namespace myGovID.Services.OpenId.Contract
{
    public class TokenResponse
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("id_token")]
        public string IdentityToken { get; set; }
    }
}
