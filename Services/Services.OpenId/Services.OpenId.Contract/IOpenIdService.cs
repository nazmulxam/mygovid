﻿using System;
using System.Threading.Tasks;

namespace myGovID.Services.OpenId.Contract
{
    /// <summary>
    /// Service that provides OpenId interactions
    /// </summary>
    public interface IOpenIdService
    {
        /// <summary>
        /// Authenticates the user using OpenId
        /// </summary>
        /// <param name="request">The OpenId Request</param>
        /// <returns>An OpenID Response that has the Response Status and if successful the Response token</returns>
        Task<OpenIdResponse> AuthenticateAsync(OpenIdRequest request);
    }
}
