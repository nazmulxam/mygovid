﻿using System;
namespace myGovID.Services.OpenId.Contract
{
    public enum OpenIdErrorCode
    {
        /// <summary>
        /// Invalid payload.
        /// </summary>
        OID000001,
        /// <summary>
        /// Resource cannot be processed as it requires authentication.
        /// </summary>
        OID000002,
        /// <summary>
        /// UnAuthenticated.
        /// </summary>
        OID000003,
        /// <summary>
        /// Unauthorised to process the resource
        /// </summary>
        OID000004,
        /// <summary>
        /// Unable to find the resource
        /// </summary>
        OID000005,
        /// <summary>
        /// Error occurred while processing the resource
        /// </summary>
        OID000006
    }

    public static class Extensions
    {
        public static string GetDescription(this OpenIdErrorCode code)
        {
            switch (code)
            {
                case OpenIdErrorCode.OID000001:
                    return "Invalid payload";
                case OpenIdErrorCode.OID000002:
                    return "Unprocessable Open Id entity";
                case OpenIdErrorCode.OID000003:
                    return "Unauthenticated";
                case OpenIdErrorCode.OID000004:
                    return "Unauthorised to access Open Id";
                case OpenIdErrorCode.OID000005:
                    return "The Open Id account was not found";
                case OpenIdErrorCode.OID000006:
                    return "An error occurred while tryong to authenticate with Open Id";
            }

            return "Unmapped Open Id Error";
        }
    }
}
