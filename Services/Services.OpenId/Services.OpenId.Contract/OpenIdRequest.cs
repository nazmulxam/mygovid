﻿using System;
using System.Collections.Generic;

namespace myGovID.Services.OpenId.Contract
{
    /// <summary>
    /// The base class for Request objects passed to the IopenIdService
    /// </summary>
    public abstract class OpenIdRequest : IOpenIdRequest
    {
        /// <summary>
        /// The headers that will be added to the OpenId request headers collection
        /// </summary>
        /// <returns>Key Value header pairs</returns>
        public Dictionary<string, string> GetHeaders()
        {
            return new Dictionary<string, string> {
                    { "Content-Type", "application/x-www-form-urlencoded" },
                    { "Accept", "application/json"}
                };
        }

        /// <summary>
        /// Get the OpenId content payload
        /// </summary>
        /// <returns>The formatted request content. Includes the Assertion and ClientAssertion tokens</returns>
        public abstract string GetPayload();
    }
}
