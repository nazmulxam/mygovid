﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using myGovID.Services.Http.Contract;
using myGovID.Services.OpenId.Contract;
using Newtonsoft.Json;

namespace myGovID.Services.OpenId.Implementation
{
    /// <summary>
    /// 
    /// </summary>
    public class OpenIdService : IOpenIdService
    {
        private IHttpConfig _config { get; }
        private IHttpService _httpService { get; }

        public OpenIdService(IHttpService httpService, IHttpConfig config)
        {
            _httpService = httpService;
            _config = config;
        }

        public async Task<OpenIdResponse> AuthenticateAsync(OpenIdRequest request)
        {
            HttpResponseMessage response = await _httpService.SendAsync(GetHttpRequestMessage((ExtensionGrantRequest)request));

            //error & not found scenario
            if (!response.IsSuccessStatusCode)
            {
                switch (response.StatusCode)
                {
                    case System.Net.HttpStatusCode.NotFound:
                        throw new OpenIdException(OpenIdErrorCode.OID000005, "Missing resource");
                    case System.Net.HttpStatusCode.Unauthorized:
                        throw new OpenIdException(OpenIdErrorCode.OID000003, "Unauthenticated");
                    case System.Net.HttpStatusCode.Forbidden:
                        throw new OpenIdException(OpenIdErrorCode.OID000004, "Unauthorised");
                    default:
                        throw new OpenIdException(OpenIdErrorCode.OID000006, $"Unexpected error returned: {response.StatusCode.ToString()}");
                }
            }
            return await GetHttpResponseAsync(response);
        }

        /// <summary>
        /// Gets the http request message.
        /// </summary>
        /// <returns>The http request message.</returns>
        /// <param name="request">Request.</param>
        /// <typeparam name="I">The 1st type parameter.</typeparam>
        private HttpRequestMessage GetHttpRequestMessage(ExtensionGrantRequest request)
        {
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(
                                        HttpMethod.Post,
                                        FormatLocation("core/connect/token"));
            var headers = request.GetHeaders();

            if (request.GetPayload() != null)
            {
                if (headers.Any(p => p.Key == "Content-Type"))
                {
                    httpRequestMessage.Content = new StringContent(request.GetPayload(), Encoding.UTF8, request.GetHeaders().First(p => p.Key == "Content-Type").Value);
                }
                else
                {
                    httpRequestMessage.Content = new StringContent(request.GetPayload(), Encoding.UTF8);
                }
            }
            foreach (KeyValuePair<string, string> header in headers.Where(p => p.Key != "Content-Type"))
            {
                httpRequestMessage.Headers.Add(header.Key, header.Value);
            }

            return httpRequestMessage;
        }

        /// <summary>
        /// Formats the location.
        /// </summary>
        /// <returns>The location.</returns>
        /// <param name="location">Location.</param>
        private string FormatLocation(string location)
        {
            return location.StartsWith("http", StringComparison.InvariantCulture)
                ? location
                : $"{_config.BaseUrl.TrimEnd('/')}/{location.TrimStart('/')}";
        }


        /// <summary>
        /// Gets the http response.
        /// </summary>
        /// <returns>The http response.</returns>
        /// <param name="message">Message.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        private async Task<OpenIdResponse> GetHttpResponseAsync(HttpResponseMessage message)
        {
            byte[] response = await message.Content.ReadAsByteArrayAsync();

            string json = Encoding.UTF8.GetString(response);
            var tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(json);

            //TODO: Move to a validate method
            if (tokenResponse.AccessToken != null)
            {
                return new OpenIdResponse(tokenResponse);
            }

            throw new OpenIdException(OpenIdErrorCode.OID000001, "Missing access token");
        }


    }
}
