﻿namespace myGovID.Services.Crypto.Droid
{
    public interface ICryptoConfig
    {
        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <value>The version.</value>
        string Version { get; }
        /// <summary>
        /// Gets the build date.
        /// </summary>
        /// <value>The build date.</value>
        string BuildDate { get; }
        /// <summary>
        /// Gets the keystore directory.
        /// </summary>
        /// <value>The keystore directory.</value>
        string KeystoreDirectory { get; }
    }
}
