﻿using AU.Gov.Abr.Akm.Exceptions;
using AU.Gov.Abr.Akm.Credential.Store;
using System;
using System.Text;
using System.IO;
using myGovID.Services.Crypto.Contract;
using myGovID.Services.Crypto.Contract.Model;

namespace myGovID.Services.Crypto.Droid
{
    public class CryptoService : ICryptoService
    {
        private readonly string _version;
        private readonly string _buildDate;
        private const string _keystoreFileName = "keystore.xml";
        private readonly string _keystoreDirectory;
        private string _keystorePath;
        private ABRKeyStore _store;
        private CreateRequest _createRequestOperation;
        public CryptoService(ICryptoConfig config)
        {
            if (config == null)
            {
                throw new ArgumentNullException(nameof(config));
            }
            _keystoreDirectory = config.KeystoreDirectory;
            _version = config.Version;
            _buildDate = config.BuildDate ?? new DateTime().ToShortDateString();
        }
        private void Save()
        {
            if (File.Exists(_keystorePath))
            {
                File.Delete(_keystorePath);
            }
            using (FileStream keystoreStream = new FileStream(_keystorePath, FileMode.OpenOrCreate))
            {
                _store.Save(keystoreStream);
            }
        }

        public LoadResult Load(LoadRequest operation)
        {
            ABRProperties.SetSoftwareInfo("ATO", "myGovID", _version, _buildDate);
            _keystorePath = operation.KeystoreDir ?? _keystoreDirectory;
            if (_keystorePath == null)
            {
                throw new ArgumentNullException(nameof(operation), "Keystore directory cannot be null.");
            }
            _keystorePath = Path.Combine(_keystorePath, _keystoreFileName);
            if (!File.Exists(_keystorePath))
            {
                FileStream stream = File.Create(_keystorePath);
                stream.Close();
            }
            try
            {
                string keystoreText = File.ReadAllText(_keystorePath);
                if (keystoreText.Trim().Length == 0)
                {
                    _store = new ABRKeyStore();
                    Save();
                }
                else
                {
                    using (MemoryStream keystoreStream = new MemoryStream(Encoding.UTF8.GetBytes(keystoreText)))
                    {
                        _store = new ABRKeyStore(keystoreStream);
                    }
                }
                return new LoadResult(_store.Version);
            }
            catch (KeyStoreException ex)
            {
                throw new CryptoException("" + ex.ErrorCode, ex.Message);
            }
        }

        public void CheckPassword(CheckPasswordRequest operation)
        {
            try
            {
                _store.CheckIsCorrectPassword(operation.Password.ToCharArray(), true);
            }
            catch (KeyStoreException ex)
            {
                throw new CryptoException("" + ex.ErrorCode, ex.Message);
            }
        }

        public CreateResult CreateRequest(CreateRequest operation)
        {
            if (operation.CredentialId == null)
            {
                throw new ArgumentNullException(nameof(operation), "Credential ID cannot be null.");
            }
            try
            {
                ABRCertificateRequest request = _store.CreateUserRequest(operation.Name1,
                                                                        operation.Name2,
                                                                        operation.ABN,
                                                                        operation.CredentialId,
                                                                        operation.Password.ToCharArray(),
                                                                        _store.GetSalt());
                Save();
                _createRequestOperation = operation;
                return new CreateResult(Convert.ToBase64String(request.GetRequest()),
                                        Convert.ToBase64String(request.GetRequestKey()));
            }
            catch (KeyStoreException ex)
            {
                throw new CryptoException("" + ex.ErrorCode, ex.Message);
            }
        }

        public GetCredentialResult GetCredential(GetCredentialRequest operation)
        {
            try
            {
                bool shouldRenew = _store.IsReadyForRenewal(operation.Alias);
                ABRCredential credential = _store.GetCredential(operation.Alias);
                return new GetCredentialResult(credential.GetPrivateKey(operation.Password.ToCharArray()).GetEncoded(),
                                               credential.PublicKey.GetEncoded());
            }
            catch (KeyStoreException ex)
            {
                throw new CryptoException("" + ex.ErrorCode, ex.Message);
            }
        }

        public void Delete(DeleteRequest operation)
        {
            try
            {
                if (_store == null)
                {
                    throw new ArgumentNullException(nameof(operation), "Store is null. Call load prior calling delete.");
                }
                _store.Delete();
                Save();
            }
            catch (KeyStoreException ex)
            {
                throw new CryptoException("" + ex.ErrorCode, ex.Message);
            }
        }

        public void InsertCredential(InsertCredentialRequest operation)
        {
            try
            {
                ABRCredential credential = _store.Add(_createRequestOperation.Password,
                                                     _createRequestOperation.Name1,
                                                     _createRequestOperation.Name2,
                                                     _createRequestOperation.ABN,
                                                     operation.P7CBase64,
                                                     operation.LegalName,
                                                     operation.RequestId,
                                                     _store.GetSalt(),
                                                     new Java.Util.Date(),
                                                     operation.SHA1FingerPrint);
                Save();
            }
            catch (KeyStoreException ex)
            {
                throw new CryptoException("" + ex.ErrorCode, ex.Message);
            }
        }
        public void ChangePassword(ChangePasswordRequest operation)
        {
            try
            {
                _store.ChangePassword(operation.Password.ToCharArray(), operation.NewPassword.ToCharArray());
                Save();
            }
            catch (KeyStoreException ex)
            {
                throw new CryptoException("" + ex.ErrorCode, ex.Message);
            }
        }
    }
}