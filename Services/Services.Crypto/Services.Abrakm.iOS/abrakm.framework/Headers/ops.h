//
//  ops.h
//  abrakm
//
//  Created by Eamonn Laffey on 11/7/17.
//  Copyright © 2017 ATO. All rights reserved.
//

#ifndef ops_h
#define ops_h

const char *run_op(const char *p);
void clean();

#endif /* ops_h */
