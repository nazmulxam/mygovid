﻿using System.Runtime.InteropServices;
using Foundation;
using ObjCRuntime;
using System;

namespace Abrakm
{
    public static class CFunctions
    {
        // extern const char * run_op (const char *p);
		[DllImport("__Internal", EntryPoint="run_op")]
        static extern unsafe IntPtr _run_op (IntPtr p);
		public static string run_op (string json)
        {
			return Marshal.PtrToStringAnsi(_run_op(Marshal.StringToHGlobalAnsi(json)));
        }
        // extern void clean ();
        [DllImport ("__Internal")]
        //[Verify (PlatformInvoke)]
        public static extern void clean ();
    }
}
