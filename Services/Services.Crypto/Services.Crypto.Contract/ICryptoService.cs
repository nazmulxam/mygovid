﻿using myGovID.Services.Crypto.Contract.Model;
namespace myGovID.Services.Crypto.Contract
{
    public interface ICryptoService
    {
        /// <summary>
        /// Load the specified crypto store.
        /// </summary>
        /// <returns>CrptoResponse with LoadResult instance which contains crypto store version</returns>
        /// <param name="operation">Load request object.</param>
        LoadResult Load(LoadRequest operation);

        /// <summary>
        /// Checks the password.
        /// </summary>
        /// <param name="operation">CheckPasswordRequest object.</param>
        void CheckPassword(CheckPasswordRequest operation);

        /// <summary>
        /// Creates the certificate signing request.
        /// </summary>
        /// <returns>CryptoResponse with CreateResult object.</returns>
        /// <param name="operation">CreateRequest object.</param>
        CreateResult CreateRequest(CreateRequest operation);

        /// <summary>
        /// Gets the credential.
        /// </summary>
        /// <returns>The credential in the CryptoResponse object</returns>
        /// <param name="operation">Get credential request object.</param>
        GetCredentialResult GetCredential(GetCredentialRequest operation);

        /// <summary>
        /// Delete the crypto store.
        /// </summary>
        /// <param name="operation">Operation.</param>
        void Delete(DeleteRequest operation);

        /// <summary>
        /// Inserts the credential into crypto store
        /// </summary>
        /// <param name="operation">Operation.</param>
        void InsertCredential(InsertCredentialRequest operation);

        /// <summary>
        /// Changes the password for given credential
        /// </summary>
        /// <param name="operation">Operation.</param>
        void ChangePassword(ChangePasswordRequest operation);
    }
}