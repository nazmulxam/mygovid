﻿namespace myGovID.Services.Crypto.Contract.Model
{
    public class GetCredentialResult
    {
        public byte[] PrivateKey;
        public byte[] PublicKey;
        public GetCredentialResult(byte[] privateKey, byte[] publicKey)
        {
            PrivateKey = privateKey;
            PublicKey = publicKey;
        }
    }
}