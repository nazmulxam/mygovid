﻿using System;

namespace myGovID.Services.Crypto.Contract.Model
{
    public class CreateRequest : ICryptoRequest
    {
        public string CredentialId { get; }
        public string CredentialType { get; }
        public string Password { get; }
        public string ABN { get; }
        public string Name1 { get; }
        public string Name2 { get; }
        public CreateRequest(string credentialId,
                                      string password,
                                      string name1 = "poi",
                                      string name2 = "id",
                                      string domain = "mygovid.gov.au",
                                      string credentialType = "U")
        {
            if(string.IsNullOrEmpty(credentialId)) 
            {
                throw new ArgumentException(nameof(credentialId));
            }
            if(string.IsNullOrEmpty(password)) 
            {
                throw new ArgumentException(nameof(password));
            }

            if (string.IsNullOrEmpty(name1))
            {
                throw new ArgumentException(nameof(name1));
            }

            if (string.IsNullOrEmpty(name2))
            {
                throw new ArgumentException(nameof(name2));
            }

            if (string.IsNullOrEmpty(domain))
            {
                throw new ArgumentException(nameof(domain));
            }

            if (string.IsNullOrEmpty(credentialType))
            {
                throw new ArgumentException(nameof(credentialType));
            }

            CredentialId = credentialId;
            Password = password;
            Name1 = name1;
            Name2 = name2;
            ABN = domain;
            CredentialType = credentialType;
        }
    }
}
