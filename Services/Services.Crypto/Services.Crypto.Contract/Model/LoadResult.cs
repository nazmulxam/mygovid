﻿using System;

namespace myGovID.Services.Crypto.Contract.Model
{
    public class LoadResult
    {
        public string Version { get; }
        public LoadResult(string version) 
        {
            if (string.IsNullOrEmpty(version))
            {
                throw new ArgumentException(nameof(version));
            }

            Version = version;
        }
    }
}