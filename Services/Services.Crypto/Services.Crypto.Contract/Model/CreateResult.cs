﻿using System;
namespace myGovID.Services.Crypto.Contract.Model
{
    public class CreateResult
    {
        public string P10base64;
        public string RequestId;
        public CreateResult(string p10, string requestId)
        {
            if(string.IsNullOrEmpty(p10)) 
            {
                throw new ArgumentNullException(nameof(p10));
            }
            if(string.IsNullOrEmpty(requestId)) 
            {
                throw new ArgumentNullException(nameof(requestId));
            }
            P10base64 = p10;
            RequestId = requestId;
        }
    }
}