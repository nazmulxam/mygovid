﻿using System;
namespace myGovID.Services.Crypto.Contract.Model
{
    public class GetCredentialRequest : ICryptoRequest
    {
        public string Alias { get; }
        public string Password { get; }
        public GetCredentialRequest(string credentialId,
                       string password,
                       string domain = "mygovid.gov.au")
        {
            if(string.IsNullOrEmpty(credentialId)) 
            {
                throw new ArgumentNullException(nameof(credentialId));
            }
            if(string.IsNullOrEmpty(password)) 
            {
                throw new ArgumentNullException(nameof(password));
            }

            if (string.IsNullOrEmpty(domain))
            {
                throw new ArgumentException(nameof(domain));
            }

            Password = password;
            Alias = String.Format("ABRP:{0}_{1}", domain, credentialId);
        }
    }
}