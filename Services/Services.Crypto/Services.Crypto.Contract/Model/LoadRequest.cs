﻿using System;

namespace myGovID.Services.Crypto.Contract.Model
{
    public class LoadRequest : ICryptoRequest
    {
        public string KeystoreDir { get; }
        public string RenewalEndPoint { get; }
        public LoadRequest(string keystoreDir = null,
                             string renewalEndpoint = "https://localhost:80/")
        {
            if (string.IsNullOrEmpty(renewalEndpoint))
            {
                throw new ArgumentException(nameof(renewalEndpoint));
            }

            KeystoreDir = keystoreDir;
            RenewalEndPoint = renewalEndpoint;
        }
    }
}