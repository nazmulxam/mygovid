﻿using System;

namespace myGovID.Services.Crypto.Contract.Model
{
    public class ChangePasswordRequest : ICryptoRequest
    {
        public string Password { get; }
        public string NewPassword { get; }
        public ChangePasswordRequest(string password, string newPassword)
        {
            if(string.IsNullOrEmpty(password)) 
            {
                throw new ArgumentNullException(nameof(password));
            }
            if(string.IsNullOrEmpty(newPassword)) 
            {
                throw new ArgumentNullException(nameof(newPassword));
            }
            Password = password;
            NewPassword = newPassword;
        }
    }
}
