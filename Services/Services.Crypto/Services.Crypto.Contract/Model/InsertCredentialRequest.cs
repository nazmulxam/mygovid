﻿using System;

namespace myGovID.Services.Crypto.Contract.Model
{
    public class InsertCredentialRequest : ICryptoRequest
    {
        public string CredentialId { get; }
        public string RequestId { get; }
        public string SHA1FingerPrint { get; }
        public string LegalName { get; }
        public string P7CBase64 { get; }
        public InsertCredentialRequest(string credentialId,
                                         string requestId,
                                         string sha1FingerPrint,
                                         string p7cbase64,
                                         string legalName = "poi")
        {
            if (string.IsNullOrEmpty(credentialId))
            {
                throw new ArgumentException(nameof(credentialId));
            }

            if (string.IsNullOrEmpty(requestId))
            {
                throw new ArgumentException(nameof(requestId));
            }

            if (string.IsNullOrEmpty(sha1FingerPrint))
            {
                throw new ArgumentException(nameof(sha1FingerPrint));
            }

            if (string.IsNullOrEmpty(p7cbase64))
            {
                throw new ArgumentException(nameof(p7cbase64));
            }

            if (string.IsNullOrEmpty(legalName))
            {
                throw new ArgumentException(nameof(legalName));
            }

            CredentialId = credentialId;
            RequestId = requestId;
            SHA1FingerPrint = sha1FingerPrint;
            P7CBase64 = p7cbase64;
            LegalName = legalName;
        }
    }
}