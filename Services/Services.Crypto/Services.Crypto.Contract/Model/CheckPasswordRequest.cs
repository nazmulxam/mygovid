﻿using System;

namespace myGovID.Services.Crypto.Contract.Model
{
    public class CheckPasswordRequest : ICryptoRequest
    {
        public string Password { get; }
        public CheckPasswordRequest(string password)
        {
            if(string.IsNullOrEmpty(password)) 
            {
                throw new ArgumentNullException(nameof(password));
            }
            Password = password;
        }
    }
}