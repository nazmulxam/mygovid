﻿using myGovID.Core.Exceptions;

namespace myGovID.Services.Crypto.Contract
{
    public class CryptoException : CodedException
    {
        public CryptoException(string code, string description) : base(code, "", description)
        {
        }
    }
}