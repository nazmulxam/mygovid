﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using myGovID.Services.Crypto.Contract;
using myGovID.Services.Crypto.Contract.Model;
using myGovID.Services.Crypto.iOS.Model;

namespace myGovID.Services.Crypto.iOS.Executor
{
    class GetCredentialExecutor : BaseExecutor<GetCredentialResult>
    {
        private readonly Dictionary<string, object> _params;
        internal GetCredentialExecutor(GetCredentialRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            _params = new Dictionary<string, object>
            {
                { "alias", request.Alias },
                { "password", request.Password }
            };
        }
        public override GetCredentialResult Execute()
        {
            Dictionary<string, object> dictionary = Execute(OperationType.getCrP12, _params);
            CheckForErrors(dictionary);
            string p12Base64 = (string)dictionary["p12"];
            string password = (string)_params["password"];
            X509Certificate2 cert = new X509Certificate2(Convert.FromBase64String(p12Base64),
                                                         password,
                                                         X509KeyStorageFlags.Exportable);
            byte[] publicKey = cert.PublicKey.EncodedKeyValue.RawData;
            byte[] privateKey = cert.PublicKey.EncodedKeyValue.RawData;
            return new GetCredentialResult(privateKey, publicKey);
        }
    }
}