﻿using System.Collections.Generic;
using Abrakm;
using myGovID.Services.Crypto.Contract;
using myGovID.Services.Crypto.iOS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace myGovID.Services.Crypto.iOS.Executor
{
    public abstract class BaseExecutor<T>
    {
        public abstract T Execute();
        protected void CheckForErrors(Dictionary<string, object> dictionary)
        {
            if (dictionary.ContainsKey("errorCode"))
            {
                throw new CryptoException("" + (long)dictionary["errorCode"], (string)dictionary["errorMessage"]);
            }
        }
        protected Dictionary<string, object> Execute(OperationType type, Dictionary<string, object> pairs)
        {
            string response = CFunctions.run_op(new CryptoOperation(type, pairs).ToJson());
            JContainer container = (JContainer)JsonConvert.DeserializeObject(response);
            return container.ToObject<Dictionary<string, object>>();
        }
    }
}