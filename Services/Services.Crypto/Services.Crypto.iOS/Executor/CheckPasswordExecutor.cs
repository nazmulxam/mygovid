﻿using System;
using System.Collections.Generic;
using myGovID.Services.Crypto.Contract.Model;
using myGovID.Services.Crypto.iOS.Model;

namespace myGovID.Services.Crypto.iOS.Executor
{
    class CheckPasswordExecutor : BaseExecutor<object>
    {
        private readonly Dictionary<string, object> _params;
        internal CheckPasswordExecutor(CheckPasswordRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            _params = new Dictionary<string, object>
            {
                { "password", request.Password }
            };
        }
        public override object Execute()
        {
            Dictionary<string, object> dictionary = Execute(OperationType.checkPassword, _params);
            CheckForErrors(dictionary);
            return null;
        }
    }
}