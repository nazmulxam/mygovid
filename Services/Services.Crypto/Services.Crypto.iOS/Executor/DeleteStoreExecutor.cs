﻿using System.Collections.Generic;
using myGovID.Services.Crypto.Contract.Model;
using myGovID.Services.Crypto.iOS.Model;

namespace myGovID.Services.Crypto.iOS.Executor
{
    class DeleteStoreExecutor : BaseExecutor<object>
    {
        private readonly Dictionary<string, object> _params;
        internal DeleteStoreExecutor(DeleteRequest request)
        {
            _params = new Dictionary<string, object>();
        }
        public override object Execute()
        {
            Dictionary<string, object> dictionary = Execute(OperationType.deleteStore, _params);
            CheckForErrors(dictionary);
            return null;
        }
    }
}