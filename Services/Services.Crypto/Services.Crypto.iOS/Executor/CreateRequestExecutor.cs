﻿using System;
using System.Collections.Generic;
using myGovID.Services.Crypto.Contract.Model;
using myGovID.Services.Crypto.iOS.Model;

namespace myGovID.Services.Crypto.iOS.Executor
{
    class CreateRequestExecutor : BaseExecutor<CreateResult>
    {
        private readonly Dictionary<string, object> _params;
        internal CreateRequestExecutor(CreateRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            _params = new Dictionary<string, object>
            {
                { "personId", request.CredentialId },
                { "name1", request.Name1 },
                { "password", request.Password },
                { "abn", request.ABN },
                { "name2", request.Name2 },
                { "credentialType", request.CredentialType }
            };
        }
        public override CreateResult Execute()
        {
            Dictionary<string, object> dictionary = Execute(OperationType.createRequest, _params);
            CheckForErrors(dictionary);
            return new CreateResult((string)dictionary["p10base64"], (string)dictionary["requestId"]);
        }
    }
}