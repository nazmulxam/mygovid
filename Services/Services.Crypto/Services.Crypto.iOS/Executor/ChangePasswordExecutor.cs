﻿using System.Collections.Generic;
using myGovID.Services.Crypto.iOS.Model;
using myGovID.Services.Crypto.Contract.Model;
using System;

namespace myGovID.Services.Crypto.iOS.Executor
{
    class ChangePasswordExecutor : BaseExecutor<object>
    {
        private readonly Dictionary<string, object> _params;
        internal ChangePasswordExecutor(ChangePasswordRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            _params = new Dictionary<string, object>
            {
                { "oldStorePassword", request.Password },
                { "newStorePassword", request.NewPassword }
            };
        }
        public override object Execute()
        {
            Dictionary<string, object> dictionary = Execute(OperationType.changePassword, _params);
            CheckForErrors(dictionary);
            return null;
        }
    }
}