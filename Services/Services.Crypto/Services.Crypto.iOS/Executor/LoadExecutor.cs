﻿using System;
using System.Collections.Generic;
using myGovID.Services.Crypto.Contract.Model;
using myGovID.Services.Crypto.iOS.Model;

namespace myGovID.Services.Crypto.iOS.Executor
{
    class LoadExecutor : BaseExecutor<LoadResult>
    {
        private readonly Dictionary<string, object> _params;
        internal LoadExecutor(LoadRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            _params = new Dictionary<string, object>
            {
                { "defaultKeystoreDir", request.KeystoreDir },
                { "renewalEndpoint", request.RenewalEndPoint }
            };
        }
        public override LoadResult Execute()
        {
            Dictionary<string, object> dictionary = Execute(OperationType.load, _params);
            CheckForErrors(dictionary);
            return new LoadResult((string)dictionary["version"]);
        }
    }
}