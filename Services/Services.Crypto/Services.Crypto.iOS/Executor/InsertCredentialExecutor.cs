﻿using System;
using System.Collections.Generic;
using myGovID.Services.Crypto.Contract.Model;
using myGovID.Services.Crypto.iOS.Model;

namespace myGovID.Services.Crypto.iOS.Executor
{
    class InsertCredentialExecutor : BaseExecutor<object>
    {
        private readonly Dictionary<string, object> _params;
        internal InsertCredentialExecutor(InsertCredentialRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            _params = new Dictionary<string, object>
            {
                { "personId", request.CredentialId },
                { "requestId", request.RequestId },
                { "sha1FingerPrint", request.SHA1FingerPrint },
                { "legalName", request.LegalName },
                { "p7cbase64", request.P7CBase64 }
            };
        }
        public override object Execute()
        {
            Dictionary<string, object> dictionary = Execute(OperationType.insertCredential, _params);
            CheckForErrors(dictionary);
            return null;
        }
    }
}