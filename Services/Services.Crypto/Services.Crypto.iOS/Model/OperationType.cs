﻿namespace myGovID.Services.Crypto.iOS.Model
{
    public enum OperationType
    {
        checkPassword = 6,
        createRequest = 7,
        insertCredential = 8,
        deleteStore = 13,
        getCrP12 = 19,
        load = 21,
        changePassword = 12
    }
}
