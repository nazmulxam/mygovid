﻿namespace myGovID.Services.Crypto.iOS.Model
{
    public class Operation
    {
        public string Name { get; }
        public string Id { get; }
        public Operation(OperationType operation)
        {
            Name = operation.ToString();
            Id = "" + ((int) operation);
        }
    }
}