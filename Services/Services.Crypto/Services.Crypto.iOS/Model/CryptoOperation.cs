﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
namespace myGovID.Services.Crypto.iOS.Model
{
    public class CryptoOperation
    {
        public Operation Operation { get; }
        public Dictionary<string, object> Params { get; }
        public CryptoOperation(OperationType type, Dictionary<string, object> pairs)
        {
			Operation = new Operation(type);
			Params = pairs;
		}
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });
        }
    }
}