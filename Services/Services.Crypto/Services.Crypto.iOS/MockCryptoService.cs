﻿using System;
using myGovID.Services.Crypto.Contract;
using myGovID.Services.Crypto.Contract.Model;

namespace myGovID.Services.Crypto.iOS
{
    public class MockCryptoService : ICryptoService
    {
        private string _privateKey = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCsfryl/ySdFAkbNru+7T8VtRlXNKYfIBOzv3+JS1iGzKcWPQ2tCQTfj/HiLboZRS6jD67Xl1t5BzNxwO2jLxKOobJL0xKplax4x5T5/Yv0wLPm6kizKRcqtDCbNh5f9ntQzAvcFazfsdEvFVrojD7RC5MuwUXtrUQ0vn3TjVks7wnhXl90qQn209Y3G4zjOrFspkA1r25J/nH+3ED5kJDaSfi+DsHy6hRXG5ee9ThMlpTlzUV6wIZm5t8pHvdglofo2q34Wz1iLcaylWcQ3pZuc0jXKss3q5EkkN0Da9/ZM15QgDZmG/WAYIBcLOv187fkZhZiWK7PD7PSmuJ4g8AhAgMBAAECggEAJeNUlnV7WlE7StHz3RUJZUA5B7GVx4JpMXMflU2KbVloXfcdGOPBQUXB0HdQyBotCz33nn0tZ2EQiKzixZQiVFfp7MO49qzcjE7yRxLM+f6VxWOygj/FSej4QC9b7qIaJf4boMTWCvBozRm4tRmQ9qO/OWbUcEc9kwRAaQZEDx05rpPvUqbXT60PkQtxDYReAWxJKw4XGH6wyd21DFgsw7kjAkQ5ENBScpEf33BLapW7MKZs/7O3P/L4zEULfakRl0QNm3deVJCujVX4XxcYOjp6L5AgBdlje8OG4y38+6LBm825kMHe7a+PmxeEhIMsKscbVBvtI6UX4iq82WyoQQKBgQDeG0EWk9XUCM3g5cymbyQeelkjOzkkgnluklcu0qy5d6x1HLJdEdUnffVXv4WtvpJbMtEad3XkVwhJRrnw/HFYtvKEw6ijNawmdQ8er6Whj1xtXaS+wanuAh26wQ/2Ve/IxMRcZ9Kn6l+VvEBtLPj3zxhuPv6xBHbQr9omVE6G+QKBgQDG0WDiHLNgx6gbqG6AJKPJflzJwt1lmdGReON8yJh0BzCTD0L5kfjJTNLn9OpuUOPzlomcK30EDro0OnarwGqOGhm6IsWZX+/0m2oeP57MsRZjOAmfKUdzfRgoRoFBbO6XHl/Ur2OTV64OTjRQVTYi+wyjAzGQZGOCGhS+++aEaQKBgBgktkqR/2hxHJAZ6skcw7xcaFwgD/IcDL8sQTXhyMzQaXrBh9H+IkGMu6KyOwHL+XTYbUqHOdV8cHYmrge4KUoPeQVdj3NOXkw/5UjcpxCM2Os6hrIFs0cgC8NFQUrxxIL6gK3ay1ddbs6EDCzR+q5VYL57Moxjg9bjhr8r4u1hAoGAPbLocAMcH6KWJs8kS5wTQAp43Rwd/NO5V32+ixDtTsSUbG7GCBEmtcGjidxVjBqKQyOqXM6aI1I2stKZ1T8HUkYDSeJUk/5TxwMb8Xso2rjyC6RuR/hVdh+SCZ2nqxgVCsygTK6JJkaFe2cFkRCAlmZ2S3hIP1kIEG3rlkEyDlECgYAkCar7is3hwX0PDywqWm3JcuRvjcJo4T3d9JVLFqOjAuvBs2iPltS4ix/nL2yTY6q6QcTfrTTNW+aCV6n7FgiLBJJBJ10cF0TusXFRJiS2PEXceQj8W3x15+fxAF5STMwxE3aRFO3dALn1gYxavSVbYMPmf0dGlpEypr43lJ5PUQ==";
        private string _publicKey = "MIGJAoGBAIB2Vgxl8MUkrLRVZUjw+i1vpfl+DWe8S1lZFYkuH2MTUlBLTORcHv0/RanKabFYXF1IaIlT70QjEt+7EBf/e4rbjUFHFOCYozP6F1N4xVitrlEL8XSmbVD6NKXfOnaaQN4TeVjdiH4Ck+B7dYuuM4ZZDd9lL0aQGvQfpGFy+Io3AgMBAAE=";
        private string _certificateRequest = "MIIBpjCCAQ8CAQAwRDEUMBIGA1UEAwwLbmFtZTEgbmFtZTIxETAPBgNVBC4TCHBlcnNvbklkMQwwCgYDVQQKDANhYm4xCzAJBgNVBAYTAkFVMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCH+AOJ6tUjSxVW8S0V1IoXayLLBtH9VqdWtl/ZAQB/j35bmyRrkWPvEiNBQ+88mKcoSUgxk9HTgAqZWsiRK7NnqQzH89tw/mA4r/dMPkVgcqAFRbvV2W0/W5AFOy1112bQqsvZGz/AK1lry7LeCyCh/XdiyPHtcozkksGVFKECgQIDAQABoCIwIAYJKoZIhvcNAQkOMRMwETAPBgYqJAGCTQEEBRYDYWJuMA0GCSqGSIb3DQEBBQUAA4GBAGOD3rOCex++lJgabveAu4cOJTQawgmW1liYpECxegg+Dg3LZqyPkK/KQedMFFTxnohmyKFZtegHbJ8UV5c+JkiF11M34ux07EBtCSyafENBxzk0+HS/kpdPLIWn4uf8QmbHmCb20BxZ37MaJXXpTCUgHNPhuI+qtqgGW8fH0u1H";

        public void ChangePassword(ChangePasswordRequest operation)
        {
            throw new NotImplementedException();
        }
        public void CheckPassword(CheckPasswordRequest operation)
        {
            throw new NotImplementedException();
        }
        public CreateResult CreateRequest(CreateRequest operation)
        {
            return new CreateResult(_certificateRequest, "requestID");
        }
        public void Delete(DeleteRequest operation)
        { }
        public GetCredentialResult GetCredential(GetCredentialRequest operation)
        {
            return new GetCredentialResult(
                Convert.FromBase64String(_privateKey),
                Convert.FromBase64String(_publicKey)
            );
        }
        public void InsertCredential(InsertCredentialRequest operation)
        { }
        public LoadResult Load(LoadRequest operation)
        {
            throw new NotImplementedException();
        }
    }
}
