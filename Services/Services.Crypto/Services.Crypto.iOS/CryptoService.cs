﻿using myGovID.Services.Crypto.Contract;
using myGovID.Services.Crypto.Contract.Model;
using myGovID.Services.Crypto.iOS.Executor;

namespace myGovID.Services.Crypto.iOS
{
    public class CryptoService : ICryptoService
    {
        public LoadResult Load(LoadRequest operation)
        {
            return new LoadExecutor(operation).Execute();
        }

        public void CheckPassword(CheckPasswordRequest operation)
        {
            new CheckPasswordExecutor(operation).Execute();
        }

        public CreateResult CreateRequest(CreateRequest operation)
        {
            return new CreateRequestExecutor(operation).Execute();
        }

        public GetCredentialResult GetCredential(GetCredentialRequest operation)
        {
            return new GetCredentialExecutor(operation).Execute();
        }

        public void Delete(DeleteRequest operation)
        {
            new DeleteStoreExecutor(operation).Execute();
        }

        public void InsertCredential(InsertCredentialRequest operation)
        {
            new InsertCredentialExecutor(operation).Execute();
        }

        public void ChangePassword(ChangePasswordRequest operation)
        {
            new ChangePasswordExecutor(operation).Execute();
        }
    }
}