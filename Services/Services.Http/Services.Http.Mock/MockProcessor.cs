using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Threading;
using myGovID.Services.Uri.Contract;
using System.IO;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net;

namespace myGovID.Services.Http.Mock
{
    public class MockProcessor
    {
        private readonly Dictionary<string, Dictionary<string, ScenariosLocation>> _requests = new Dictionary<string, Dictionary<string, ScenariosLocation>>();
        private readonly MockFileManager _fileManager;
        private readonly UriHandler _uriHandler;

        public MockProcessor(IUriService uriService)
        {
            _fileManager = new MockFileManager();
            _uriHandler = new UriHandler(uriService);
        }

        public Setup Setup(string testCaseName)
        {
            string configurationString = _fileManager.GetFileContents(Path.Combine(testCaseName, "configuration.json"));
            IEnumerable<UriConfig> configuration = JsonConvert.DeserializeObject<List<UriConfig>>(configurationString);
            if (configuration == null)
            {
                throw new MockError("MCK000001", "Configuration error", "Failed to deserialise ");
            }
            _requests.Clear();
            foreach (UriConfig config in configuration)
            {
                if (string.IsNullOrEmpty(config.Folder))
                {
                    config.Folder = testCaseName;
                }
                ProcessConfig(config);
            }
            string setupString = _fileManager.GetFileContents(System.IO.Path.Combine(testCaseName, "setup.json"));
            return JsonConvert.DeserializeObject<Setup>(setupString);
        }

        private void ProcessConfig(UriConfig config)
        {
            string url = config.Url ?? throw new ArgumentException("Url in config cannot be null");
            string method = (config.Method ?? "POST").ToUpper();
            string folder = config.Folder;
            string scenariosFile = config.Scenarios ?? "scenarios.json";
            if (!_requests.ContainsKey(url))
            {
                _requests[url] = new Dictionary<string, ScenariosLocation>();
            }
            _requests[url][method] = new ScenariosLocation(folder, scenariosFile);
        }

        public async Task<HttpResponseMessage> GetResponse(HttpRequestMessage request)
        {
            ScenariosLocation scenariosLocation = null;
            IDictionary<string, string> uriVariables = null;
            foreach (string urlTemplate in _requests.Keys)
            {
                UriMatchResult result = _uriHandler.Match(urlTemplate, request.RequestUri.AbsolutePath);
                if (result.Match)
                {
                    string method = request.Method.ToString();
                    scenariosLocation = _requests[urlTemplate].First((it) => it.Key.Equals(method)).Value;
                    if (scenariosLocation != null)
                    {
                        uriVariables = result.BoundVariables;
                        break;
                    }
                }
            }

            if (scenariosLocation == null)
            {
                throw new MockError("MCK000001", "", $"Could not find scenarios file for request to {request.RequestUri.AbsolutePath}");
            }

            IDictionary<string, object> criteriaParameters = GetCriteriaParameters(await GetBodyParameters(request), uriVariables);

            IEnumerable<Scenario> scenarios = null;

            try
            {
                string folder = scenariosLocation.Folder;
                var scenariosPath = Path.Combine(scenariosLocation.Folder, scenariosLocation.ScenarioFile);
                string scenariosString = _fileManager.GetFileContents(scenariosPath);
                scenarios = JsonConvert.DeserializeObject<IEnumerable<Scenario>>(scenariosString);
            }
            catch (Exception e)
            {
                throw new MockError("MCK000001", "Scenario JSON error", $"Failed to deserialize {scenariosLocation.ScenarioFile}", e);
            }

            foreach (Scenario scenario in scenarios)
            {
                var match = false;
                try
                {
                    match = scenario.Criteria == null || IsAMatch(scenario.Criteria, criteriaParameters);
                }
                catch (Exception e)
                {
                    throw new MockError("MCK000001", "Scenario criteria error", $"Failed to evaluate criteria for scenario \"{scenario.Name}\" of {scenariosLocation.ScenarioFile}", e);
                }

                if (match)
                {
                    return GetResponseForScenario(scenario, scenariosLocation);
                }
            }

            throw new MockError("MCK000001", "Scenario criteria fallthrough", $"No scenario in {scenariosLocation.ScenarioFile} matched criteria for request {request.RequestUri.AbsolutePath}");
        }

        private async Task<IDictionary<string, object>> GetBodyParameters(HttpRequestMessage request)
        {
            try
            {
                IDictionary<string, object> bodyJson = new Dictionary<string, object>();

                if (request.Content != null)
                {
                    var requestBody = await request.Content.ReadAsStringAsync();

                    if (request.Content.Headers.ContentType.MediaType == "application/json")
                    {
                        bodyJson = ((JObject)JsonConvert.DeserializeObject(requestBody)).ToObject<Dictionary<string, object>>();
                    }
                    else
                    {
                        foreach (var param in _uriHandler.DecodeParameters(requestBody))
                        {
                            bodyJson.Add(param.Key, param.Value);
                        }
                    }
                }

                return bodyJson;
            }
            catch (Exception e)
            {
                throw new MockError("MCK000001", "Response JSON error", $"Failed to deserialize response {request.RequestUri.AbsolutePath}", e);
            }
        }

        private IDictionary<string, object> GetCriteriaParameters(IDictionary<string, object> responseBody, IDictionary<string, string> uriVariables)
        {
            var result = new Dictionary<string, object>(responseBody ?? new Dictionary<string, object>());
            if (uriVariables != null)
            {
                foreach (var variable in uriVariables)
                {
                    result.Add(variable.Key, variable.Value);
                }
            }
            return result;
        }

        private HttpResponseMessage GetResponseForScenario(Scenario scenario, ScenariosLocation location)
        {
            if (scenario.Delay > 0)
            {
                Thread.Sleep(scenario.Delay * 1000);
            }

            try
            {
                // Assign the status code
                HttpResponseMessage response = new HttpResponseMessage((HttpStatusCode)scenario.StatusCode);

                // Assign the payload
                var payload = scenario.Response == null
                                         ? string.Empty
                                         : _fileManager.GetFileContents(location.Folder + "/" + scenario.Response);
                response.Content = new StringContent(payload);

                // Assign the headers
                foreach (var header in scenario.Headers)
                {
                    response.Headers.Add(header.Key, header.Value);
                }

                return response;
            }
            catch (Exception e)
            {
                throw new MockError("MCK000001", "Get file contents error", $"Failed to get file contents {location.Folder}/{scenario.Response}", e);
            }
        }

        private bool IsAMatch(Criteria[] criterias, IDictionary<string, object> body)
        {
            string anyValue = "*";
            foreach (Criteria criteria in criterias)
            {
                var expectedValue = criteria.Value?.ToString();
                if (expectedValue == null || expectedValue == anyValue)
                {
                    return true;
                }

                var actualValue = GetValueFromJson(criteria.Field, body)?.ToString();
                if (actualValue != expectedValue)
                {
                    return false;
                }
            }
            return true;
        }

        private object GetValueFromJson(string field, IDictionary<string, object> json)
        {
            var fieldSplits = field?.Split('.') ?? Array.Empty<string>();
            IDictionary<string, object> jsonTemp = json;
            object result = null;

            foreach (string fieldPart in fieldSplits)
            {
                result = GetValueForFieldPathComponent(fieldPart, jsonTemp);
                if (result == null)
                {
                    return null;
                }
                else if (result is IDictionary<string, object>)
                {
                    jsonTemp = (IDictionary<string, object>)result;
                }
            }

            return result;
        }

        private object GetValueForFieldPathComponent(string path, IDictionary<string, object> json)
        {
            if (json.ContainsKey(path))
            {
                return json[path];
            }

            // The URI matching framwork will uppercase variables extracted from
            // the uri template.
            var uppercasedFieldPart = path.ToUpper(CultureInfo.InvariantCulture);
            if (json.ContainsKey(uppercasedFieldPart))
            {
                return json[uppercasedFieldPart];
            }

            return null;
        }
    }
}