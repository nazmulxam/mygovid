﻿using System;
using System.Collections.Generic;
using System.Linq;
using myGovID.Services.Uri.Contract;

namespace myGovID.Services.Http.Mock
{
    public class UriHandler
    {
        private const string baseUri = "http://localhost";
        private readonly IUriService _uriService;
        public UriHandler(IUriService uriService)
        {
            _uriService = uriService;
        }
        public UriMatchResult Match(string template, string url)
        {
            return _uriService.Match(template, url, baseUri);
        }
        public IDictionary<string, string> DecodeQueryParameters(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                throw new ArgumentNullException(nameof(url));
            }
            System.Uri uri = new System.Uri(url);
            if(uri == null)
            {
                throw new Exception("Failed to convert to an URI for url: " + url);
            }
            return uri.Query.Length == 0
                ? new Dictionary<string, string>()
                : uri.Query.TrimStart('?')
                            .Split(new[] { '&', ';' }, StringSplitOptions.RemoveEmptyEntries)
                            .Select(parameter => parameter.Split(new[] { '=' }, StringSplitOptions.RemoveEmptyEntries))
                            .GroupBy(parts => parts[0],
                                     parts => parts.Length > 2 ? string.Join("=", parts, 1, parts.Length - 1) : (parts.Length > 1 ? parts[1] : ""))
                            .ToDictionary(grouping => grouping.Key,
                                          grouping => string.Join(",", grouping));
        }
        public IDictionary<string, string> DecodeParameters(string paramString)
        {
            if (string.IsNullOrEmpty(paramString))
            {
                throw new ArgumentNullException(nameof(paramString));
            }
            String url = baseUri + "/query?" + paramString;
            return DecodeQueryParameters(url);
        }
    }
}