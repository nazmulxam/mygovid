﻿using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using myGovID.Services.Http.Contract;
using System.Net.Http;

namespace myGovID.Services.Http.Mock
{
    public class MockHttpService : IHttpService
    {
        private readonly MockProcessor _mockProcessor;

        public readonly IList<HttpRequestMessage> RequestHistory = new List<HttpRequestMessage>();

        public MockHttpService(MockProcessor mockProcessor)
        {
            _mockProcessor = mockProcessor;
        }

        public Task<HttpResponseMessage> SendAsync(HttpRequestMessage request)
        {
            RequestHistory.Add(request);

            return _mockProcessor.GetResponse(request);
        }

        public Setup Setup(string configFileName)
        {
            return _mockProcessor.Setup(configFileName);
        }

        public HttpRequestMessage GetLastRequest()
        {
			return RequestHistory.LastOrDefault();
        }
    }
}