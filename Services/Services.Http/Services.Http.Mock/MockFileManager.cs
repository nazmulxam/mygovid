﻿using System.IO;
using System.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace myGovID.Services.Http.Mock
{
    public class MockFileManager
    {
        private const string _resourceNamespace = "myGovID.Services.Http.Mock.TestScenarios.";
        public string GetFileContents(string fileName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            string resourceName = _resourceNamespace + fileName.Replace('/', '.').Replace('\\', '.');
            string resource = null;
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    resource = reader.ReadToEnd();
                }
            }
            return resource;
        }
        public IDictionary<string, string> GetTestCases()
        {
            var assembly = Assembly.GetExecutingAssembly();
            IDictionary<string, string> result = new SortedDictionary<string, string>();
            foreach(string resource in assembly.GetManifestResourceNames())
            {
                if(resource.StartsWith(_resourceNamespace, StringComparison.InvariantCulture))
                {
                    var resourceParts = resource.Replace(_resourceNamespace, "").Split('.');
                    string testcaseName = resourceParts.First();
                    if (!result.ContainsKey(testcaseName))
                    {
                        result.Add(testcaseName, GetTestCaseInfo(testcaseName));
                    }
                }
            }

            return result;
        }
        private string GetTestCaseInfo(string testcaseName)
        {
            return GetFileContents(string.Concat(testcaseName, ".info.txt"));
        }
    }
}