﻿using System;
using myGovID.Core.Exceptions;

namespace myGovID.Services.Http.Mock
{
    public class MockError : CodedException
    {
        public MockError(string code, string title, string description) : base(code, title, description)
        {
        }

        public MockError(string code, string title, string description, Exception innerException) : base(code, title, description, innerException)
        {
        }
    }
}