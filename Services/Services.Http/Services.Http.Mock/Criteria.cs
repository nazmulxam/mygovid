﻿using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;

namespace myGovID.Services.Http.Mock
{
    public class Criteria
    {
        [JsonProperty("field")]
        public string Field { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}