﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;

namespace myGovID.Services.Http.Mock
{
    public class Scenario
    {
        [JsonProperty("criteria")]
        public Criteria[] Criteria { get; set; }

        [JsonProperty("delay")]
        public int Delay { get; set; }

        [JsonProperty("statusCode")]
        public int StatusCode { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("response")]
        public string Response { get; set;}

        [JsonProperty("headers")]
        public IDictionary<string, string> Headers { get; set; }

        public Scenario()
        {
            Criteria = new Criteria[0];
            Headers = new Dictionary<string, string>();
        }
    }
}