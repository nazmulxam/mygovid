﻿using Newtonsoft.Json;

namespace myGovID.Services.Http.Mock
{
    public class Setup
    {
        [JsonProperty("registered")]
        public bool Registered { get; set; }

        [JsonProperty("poiId")]
        public string PoiId { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("dateOfBirth")]
        public string DateOfBirth { get; set; }

        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("password")]
        public string UserPassword { get; set; }

        [JsonProperty("credentialToken")]
        public string CredentialToken { get; set; }

        [JsonProperty("useBiometrics")]
        public bool UseBiometrics { get; set; }

        [JsonProperty("ipLevel")]
        public string IPLevel { get; set; }
    }
}