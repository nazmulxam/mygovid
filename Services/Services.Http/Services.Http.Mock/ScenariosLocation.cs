﻿namespace myGovID.Services.Http.Mock
{
    public class ScenariosLocation
    {
        public string Folder { get; }
        public string ScenarioFile { get; }
        public ScenariosLocation(string folder, string scenarioFile)
        {
            Folder = folder;
            ScenarioFile = scenarioFile;
        }
    }
}