﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace myGovID.Services.Http.Mock
{
    public class UriConfig
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("method")]
        public string Method { get; set; }

        [JsonProperty("folder")]
        public string Folder { get; set; }

        [JsonProperty("scenarios")]
        public string Scenarios { get; set; }
    }
}
