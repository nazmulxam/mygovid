﻿using System.Net.Http;
using System.Threading.Tasks;

namespace myGovID.Services.Http.Contract
{
    public interface IHttpService
    {
        /// <summary>
        /// Sends the http request.
        /// </summary>
        /// <returns>The task for getting the response.</returns>
        /// <param name="request">The HTTP request</param>
        Task<HttpResponseMessage> SendAsync(HttpRequestMessage request);
    }
}