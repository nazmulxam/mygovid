﻿using System;
using System.Security.Cryptography;
using System.Text;
using Android.App;
using Android.Content;
using myGovID.Services.Storage.Contract;

namespace myGovID.Services.Storage.Droid
{
    public class SecureStorage : BaseStorage
    {
        private readonly string StorageName = "myGovID.SecureStorage";

        private DroidKeyStore _droidKeyStore;
        
        public SecureStorage() 
        {
            _droidKeyStore = new DroidKeyStore(Application.Context, StorageName);
        }
        /// <summary>
        /// Generates MD5 hash
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private string GetMD5Hash(string input)
        {
            var hash = new StringBuilder();
            var md5provider = new MD5CryptoServiceProvider();
            var bytes = md5provider.ComputeHash(Encoding.UTF8.GetBytes(input));
            for (var i = 0; i < bytes.Length; i++)
                hash.Append(bytes[i].ToString("x2"));
            return hash.ToString();
        }
        protected override void StoreValue(string key, string val)
        {
            using (ISharedPreferencesEditor preferencesEditor = GetSharedPreferencesEditor())
            {
                byte[] encryptedString = _droidKeyStore.Encrypt(val);
                preferencesEditor.PutString(GetMD5Hash(key), Convert.ToBase64String(encryptedString));
                preferencesEditor.Commit();
            }
        }
        protected override string RetrieveKey(string key)
        {
            using (ISharedPreferences preferences = GetSharedPreferences())
            {
                string encryptionString = preferences.GetString(GetMD5Hash(key), null);
                if (!string.IsNullOrEmpty(encryptionString))
                {
                    byte[] encryptedData = Convert.FromBase64String(encryptionString);
                    return _droidKeyStore.Decrypt(encryptedData);
                }
                throw new StorageReadFailedException(key);
            }
        }
        protected override bool CheckKey(string key)
        {
            using (ISharedPreferences preferences = GetSharedPreferences())
            {
                return preferences.Contains(GetMD5Hash(key));
            }
        }
        protected override void RemoveKey(string key)
        {
            if (CheckKey(key))
            {
                using (ISharedPreferencesEditor preferencesEditor = GetSharedPreferencesEditor())
                {
                    string md5Hash = GetMD5Hash(key);
                    preferencesEditor.Remove(md5Hash);
                    preferencesEditor.Commit();
                    return;
                }
            }
            throw new StorageKeyNotFoundException(key);
        }
        public override void Clear()
        {
            using (ISharedPreferencesEditor preferencesEditor = GetSharedPreferencesEditor())
            {
                preferencesEditor.Clear();
                preferencesEditor.Commit();
            }
        }
        private ISharedPreferences GetSharedPreferences()
        {
            ISharedPreferences preferences = Application.Context.GetSharedPreferences(StorageName, FileCreationMode.Private);
            if (preferences == null)
            {
                throw new StorageReadFailedException();
            }
            return preferences;
        }
        private ISharedPreferencesEditor GetSharedPreferencesEditor()
        {
            ISharedPreferences preferences = Application.Context.GetSharedPreferences(StorageName, FileCreationMode.Private);
            if (preferences == null)
            {
                throw new StorageReadFailedException();
            }
            ISharedPreferencesEditor preferencesEditor = preferences.Edit();
            if (preferencesEditor == null)
            {
                throw new StorageWriteFailedException();
            }
            return preferencesEditor;
        }
    }
}