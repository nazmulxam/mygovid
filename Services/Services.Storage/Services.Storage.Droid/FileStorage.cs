﻿using Android.App;
using Android.Content;
using myGovID.Services.Storage.Contract;

namespace myGovID.Services.Storage.Droid
{
    public class FileStorage : BaseStorage
    {
        private readonly string FilePreferenceName = "myGovID.FileStorage";
        protected override void StoreValue(string key, string val)
        {
            using (ISharedPreferences preferences = GetSharedPreferences())
            {
                using (ISharedPreferencesEditor preferencesEditor = GetSharedPreferencesEditor())
                {
                    preferencesEditor.PutString(key, val);
                    preferencesEditor.Commit();
                }
            }
        }
        protected override string RetrieveKey(string key)
        {
            using (ISharedPreferences preferences = GetSharedPreferences())
            {
                return preferences.GetString(key, null);
            }
        }
        protected override bool CheckKey(string key)
        {
            using (ISharedPreferences preferences = GetSharedPreferences())
            {
                return preferences.Contains(key);
            }
        }
        protected override void RemoveKey(string key)
        {
            if (CheckKey(key))
            {
                using (ISharedPreferencesEditor preferencesEditor = GetSharedPreferencesEditor())
                {
                    preferencesEditor.Remove(key);
                    preferencesEditor.Commit();
                    return;
                }
            }
            throw new StorageKeyNotFoundException(key);
        }
        public override void Clear()
        {
            using (ISharedPreferencesEditor preferencesEditor = GetSharedPreferencesEditor())
            {
                preferencesEditor.Clear();
                preferencesEditor.Commit();
            }
        }
        private ISharedPreferences GetSharedPreferences()
        {
            ISharedPreferences preferences = Application.Context.GetSharedPreferences(FilePreferenceName, FileCreationMode.Private);
            if (preferences == null)
            {
                throw new StorageReadFailedException();
            }
            return preferences;
        }
        private ISharedPreferencesEditor GetSharedPreferencesEditor()
        {
            ISharedPreferences preferences = Application.Context.GetSharedPreferences(FilePreferenceName, FileCreationMode.Private);
            if (preferences == null)
            {
                throw new StorageReadFailedException();
            }
            ISharedPreferencesEditor preferencesEditor = preferences.Edit();
            if (preferencesEditor == null)
            {
                throw new StorageWriteFailedException();
            }
            return preferencesEditor;
        }
    }
}