﻿using System;
using myGovID.Services.Storage.Contract;

namespace myGovID.Services.Storage.Droid
{
    /// <summary>
    /// Base storage implementation which primarily runs the validations.
    /// </summary>
    public abstract class BaseStorage : IStorage
    {
        protected abstract void StoreValue(string key, string val);
        protected abstract string RetrieveKey(string key);
        protected abstract bool CheckKey(string key);
        protected abstract void RemoveKey(string key);

        public virtual void DeleteKey(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Invalid parameter: " + nameof(key));
            }
			RemoveKey(key);
        }

        public virtual string GetValue(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Invalid parameter: " + nameof(key));
            }

			return RetrieveKey(key);
        }

        public virtual bool HasKey(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Invalid parameter: " + nameof(key));
            }

			return CheckKey(key);
        }

        public virtual void SetValue(string key, string val)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Invalid parameter: " + nameof(key));
            }

            if (val == null)
            {
                throw new ArgumentNullException(nameof(val) + " cannot be null.");
            }
			StoreValue(key, val);
        }

        public virtual void Clear()
        {  }
    }
}