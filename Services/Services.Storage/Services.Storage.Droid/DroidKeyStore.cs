﻿using System;
using System.Text;
using Android.Content;
using Android.Runtime;
using Android.Security.Keystore;
using Java.Security;
using Javax.Crypto;
using Javax.Crypto.Spec;

namespace myGovID.Services.Storage.Droid
{
    internal class DroidKeyStore
    {
        private const string KeyStoreType = "AndroidKeyStore"; 
        private const string Algorithm = "AES";
        private const string CipherTransformationSymmetric = "AES/GCM/NoPadding";
        private const int InitializationVectorLen = 12;

        private Context _appContext;
        private string _alias;
        private KeyStore _keyStore;

        internal DroidKeyStore(Context context, string keystoreAlias)
        {
            _appContext = context;
            _alias = keystoreAlias;
            _keyStore = KeyStore.GetInstance(KeyStoreType);
            _keyStore.Load(null);
        }

        internal byte[] Encrypt(string data)
        {
            ISecretKey key = GetSymmetricKey();
            Cipher cipher;
            // Combine the IV and the encrypted data into one array
            byte[] iv = new byte[InitializationVectorLen];
            new SecureRandom().NextBytes(iv);
            try
            {
                cipher = Cipher.GetInstance(CipherTransformationSymmetric);
                cipher.Init(CipherMode.EncryptMode, key, new GCMParameterSpec(128, iv));
            }
            catch (InvalidAlgorithmParameterException)
            {
                // If we encounter this error, it's likely an old bouncycastle provider version
                // is being used which does not recognize GCMParameterSpec, but should work
                // with IvParameterSpec, however we only do this as a last effort since other
                // implementations will error if you use IvParameterSpec when GCMParameterSpec
                // is recognized and expected.
                cipher = Cipher.GetInstance(CipherTransformationSymmetric);
                cipher.Init(CipherMode.EncryptMode, key, new IvParameterSpec(iv));
            }

            byte[] decryptedData = Encoding.UTF8.GetBytes(data);
            byte[] encryptedBytes = cipher.DoFinal(decryptedData);
            byte[] r = new byte[iv.Length + encryptedBytes.Length];
            Buffer.BlockCopy(iv, 0, r, 0, iv.Length);
            Buffer.BlockCopy(encryptedBytes, 0, r, iv.Length, encryptedBytes.Length);

            return r;
        }

        internal string Decrypt(byte[] data)
        {
            if (data.Length < InitializationVectorLen)
                return null;

            ISecretKey key = GetSymmetricKey();

            // IV will be the first 16 bytes of the encrypted data
            byte[] iv = new byte[InitializationVectorLen];
            Buffer.BlockCopy(data, 0, iv, 0, InitializationVectorLen);

            Cipher cipher;

            // Attempt to use GCMParameterSpec by default
            try
            {
                cipher = Cipher.GetInstance(CipherTransformationSymmetric);
                cipher.Init(CipherMode.DecryptMode, key, new GCMParameterSpec(128, iv));
            }
            catch (InvalidAlgorithmParameterException)
            {
                // If we encounter this error, it's likely an old bouncycastle provider version
                // is being used which does not recognize GCMParameterSpec, but should work
                // with IvParameterSpec, however we only do this as a last effort since other
                // implementations will error if you use IvParameterSpec when GCMParameterSpec
                // is recognized and expected.
                cipher = Cipher.GetInstance(CipherTransformationSymmetric);
                cipher.Init(CipherMode.DecryptMode, key, new IvParameterSpec(iv));
            }

            // Decrypt starting after the first 16 bytes from the IV
            byte[] decryptedData = cipher.DoFinal(data, InitializationVectorLen, data.Length - InitializationVectorLen);

            return Encoding.UTF8.GetString(decryptedData);
        }

        // API 23+ Only
        private ISecretKey GetSymmetricKey()
        {
            IKey existingKey = _keyStore.GetKey(_alias, null);

            if (existingKey != null)
            {
                return existingKey.JavaCast<ISecretKey>();
            }

            KeyGenerator keyGenerator = KeyGenerator.GetInstance(KeyProperties.KeyAlgorithmAes, KeyStoreType);
            KeyGenParameterSpec.Builder builder = new KeyGenParameterSpec.Builder(_alias, KeyStorePurpose.Encrypt | KeyStorePurpose.Decrypt)
                .SetBlockModes(KeyProperties.BlockModeGcm)
                .SetEncryptionPaddings(KeyProperties.EncryptionPaddingNone)
                .SetRandomizedEncryptionRequired(false);

            keyGenerator.Init(builder.Build());
            return keyGenerator.GenerateKey();
        }
    }

}
