﻿using Foundation;
namespace myGovID.Services.Storage.iOS
{
    public class FileStorage: BaseStorage
    {
        private readonly string FilePreferenceName = "myGovID.FileStorage";
        private readonly NSUserDefaults _nsUserDefaults;
        public FileStorage() 
        {
            _nsUserDefaults = new NSUserDefaults(FilePreferenceName);
        }
        protected override bool CheckKey(string key)
        {
            return !string.IsNullOrWhiteSpace(_nsUserDefaults.StringForKey(key));
        }
        protected override void RemoveKey(string key)
        {
            string value = _nsUserDefaults.StringForKey(key);
            if (!string.IsNullOrWhiteSpace(value))
            {
                _nsUserDefaults.RemoveObject(key);
            }
        }
        protected override string RetrieveKey(string key)
        {
            string value = _nsUserDefaults.StringForKey(key);
            return string.IsNullOrWhiteSpace(value) ? null : value;
        }
        protected override void StoreValue(string key, string val)
        {
            if (HasKey(key))
            {
                DeleteKey(key);
            }
            _nsUserDefaults.SetString(val, key);
        }
        public override void Clear()
        {
            NSDictionary dictionary = _nsUserDefaults.ToDictionary();
            foreach (object key in dictionary.Keys)
            {
                _nsUserDefaults.RemoveObject(key.ToString());
            }
        }
    }
}