﻿using myGovID.Services.Storage.Contract;
using Security;

namespace myGovID.Services.Storage.iOS
{
    public class SecureStorage : BaseStorage
    {
        private readonly SecureKeyChain secureKeyChain;
        private const string SecureService = "myGovID";

        public SecureStorage() 
        {
            secureKeyChain = new SecureKeyChain(SecureService);
        }
        protected override bool CheckKey(string key)
        {
            secureKeyChain.GetRecord(key, out SecStatusCode ssc);
            return ssc == SecStatusCode.Success;
        }
        protected override void RemoveKey(string key)
        {
            if(secureKeyChain.RemoveRecord(key) != SecStatusCode.Success)
            {
                throw new StorageWriteFailedException();
            }
        }
        protected override string RetrieveKey(string key)
        {
            var found = secureKeyChain.GetRecord(key, out SecStatusCode ssc);
            return ssc == SecStatusCode.Success ? found.ValueData.ToString() : null;
        }
        protected override void StoreValue(string key, string val)
        {
            secureKeyChain.RemoveRecord(key);
            if(secureKeyChain.AddRecord(key, val) != SecStatusCode.Success)
            {
                throw new StorageWriteFailedException();
            }
        }
        public override void Clear()
        {
            secureKeyChain.RemoveAll();
        }
    }
}