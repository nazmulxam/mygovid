﻿using System.Collections.Generic;
using myGovID.Services.Storage.Contract;

namespace myGovID.Services.Storage.iOS
{
    public class MemoryStorage : BaseStorage
    {
        private readonly Dictionary<string, string> _dictionary;

        public MemoryStorage()
        {
            _dictionary = new Dictionary<string, string>();
        }

        protected override bool CheckKey(string key)
        {
            return _dictionary.ContainsKey(key);
        }

        protected override void RemoveKey(string key)
        {
            if (!_dictionary.Remove(key))
            {
                throw new StorageWriteFailedException(key);
            }
        }
        protected override string RetrieveKey(string key)
        {
            if (!CheckKey(key))
            {
                throw new StorageKeyNotFoundException(key);
            }
            if (!_dictionary.TryGetValue(key, out string value))
            {
                throw new StorageReadFailedException(key);
            }
            return value;
        }
        protected override void StoreValue(string key, string val)
        {
            if (HasKey(key))
            {
                _dictionary.Remove(key);
            }
            if (!_dictionary.TryAdd(key, val))
            {
                throw new StorageWriteFailedException(key);
            }
        }
        public override void Clear()
        {
            _dictionary.Clear();
        }
    }
}