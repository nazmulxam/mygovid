﻿using System.Collections.Generic;
using System.Linq;
using Foundation;
using Security;

namespace myGovID.Services.Storage.iOS
{
    internal class SecureKeyChain
    {
        private const SecKind StoreSecKind = SecKind.GenericPassword;
        private static readonly SecAccessible DefaultAccessible = SecAccessible.Always;
        private readonly string _service;

        public SecureKeyChain(string service)
        {
            _service = service;
        }

        /// <summary>
        /// Retreives record from the store
        /// </summary>
        /// <returns>The record.</returns>
        /// <param name="key">Key.</param>
        /// <param name="ssc">Ssc.</param>
        internal SecRecord GetRecord(string key, out SecStatusCode ssc)
        {
            // create an instance of the record to query
            var sr = new SecRecord(StoreSecKind)
            {
                Account = key,
                Service = _service
            };
            if (DefaultAccessible != SecAccessible.Invalid)
            {
                sr.Accessible = DefaultAccessible;
            }

            return SecKeyChain.QueryAsRecord(sr, out ssc);
        }

        /// <summary>
        /// Adds the record of type GenericPassword
        /// </summary>
        /// <returns>The record.</returns>
        /// <param name="key">Key.</param>
        /// <param name="val">Value.</param>
        internal SecStatusCode AddRecord(string key, string val)
        {
            var sr = new SecRecord(StoreSecKind)
            {
                Account = key,
                Service = _service,
                ValueData = NSData.FromString(val)
            };
            if (DefaultAccessible != SecAccessible.Invalid)
            {
                sr.Accessible = DefaultAccessible;
            }

            return SecKeyChain.Add(sr);
        }

        /// <summary>
        /// Removes the record.
        /// </summary>
        /// <returns>The record.</returns>
        /// <param name="key">Key.</param>
        internal SecStatusCode RemoveRecord(string key)
        {
            SecRecord found = GetRecord(key, out SecStatusCode ssc);

            // if it exists, delete it
            if (ssc == SecStatusCode.Success)
            {
                // this has to be different that the one queried
                var sr = new SecRecord(StoreSecKind)
                {
                    Account = key,
                    Service = _service,
                    ValueData = found.ValueData
                };
                if (DefaultAccessible != SecAccessible.Invalid)
                {
                    sr.Accessible = DefaultAccessible;
                }
                return SecKeyChain.Remove(sr);
            }

            return SecStatusCode.NoSuchKeyChain;
        }

        internal void RemoveAll()
        {
            var query = new SecRecord(StoreSecKind)
            {
                Service = _service
            };

            var result = SecKeyChain.QueryAsRecord(query, int.MaxValue, out SecStatusCode status);
            if (status == SecStatusCode.Success)
            {
                foreach (var record in result)
                {
                    var outcome = SecKeyChain.Remove(new SecRecord(StoreSecKind) { AccessGroup = record.AccessGroup, Service = record.Service, Account = record.Account });
                }
            }
        }
    }
}
