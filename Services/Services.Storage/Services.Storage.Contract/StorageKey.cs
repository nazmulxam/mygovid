﻿namespace myGovID.Services.Storage.Contract
{
    public enum StorageKey
    {
        UseBiometrics,
        UserPassword,
        CredentialToken,
        CredentialAlias,
        PoiAssuranceToken,
        CredentialPassword,
        CredentialId,
        FirstName,
        LastName,
        DateOfBirth,
        EmailAddress,
        IPLevel,
        PoiId,
        ExtensionGrant
    }
}