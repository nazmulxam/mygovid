﻿using myGovID.Core.Exceptions;

namespace myGovID.Services.Storage.Contract
{
    public class StorageReadFailedException : CodedException
    {
        public StorageReadFailedException(StorageExceptionCode code = StorageExceptionCode.STO000001, string description = null) : base(code.ToString(), "", description ?? code.GetDescription())
        {
        }
        public StorageReadFailedException(string key) : this(StorageExceptionCode.STO000001, string.Format("Failed to read Key {0}", key))
        {
        }
    }
}