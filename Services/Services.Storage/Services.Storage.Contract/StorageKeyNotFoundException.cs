﻿using myGovID.Core.Exceptions;

namespace myGovID.Services.Storage.Contract
{
    public class StorageKeyNotFoundException : CodedException
    {
        public StorageKeyNotFoundException(StorageExceptionCode code = StorageExceptionCode.STO000003, string description = null) : base(code.ToString(), "", description ?? code.GetDescription())
        {
        }
        public StorageKeyNotFoundException(string key) : this(StorageExceptionCode.STO000003, string.Format("Key {0} not found.", key))
        {
        }
    }
}