﻿namespace myGovID.Services.Storage.Contract
{
    public enum StorageExceptionCode
    {
        STO000001, STO000002, STO000003
    }
    public static class Extensions
    {
        public static string GetDescription(this StorageExceptionCode code)
        {
            switch(code)
            {
                case StorageExceptionCode.STO000001: return "Failed to open/read.";
                case StorageExceptionCode.STO000002: return "Failed to write.";
                case StorageExceptionCode.STO000003: return "Key not found.";
                default: return "Storage: Unknown error";
            }
        }
    }
}