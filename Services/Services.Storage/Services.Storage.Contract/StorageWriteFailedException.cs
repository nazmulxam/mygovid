﻿using myGovID.Core.Exceptions;

namespace myGovID.Services.Storage.Contract
{
    public class StorageWriteFailedException: CodedException
    {
        public StorageWriteFailedException(StorageExceptionCode code = StorageExceptionCode.STO000002, string description = null): base(code.ToString(), "", description ?? code.GetDescription())
        {
        }
        public StorageWriteFailedException(string key) : this(StorageExceptionCode.STO000002, string.Format("Failed to write/remove Key {0}.", key))
        {
        }
    }
}