﻿using System;
using GalaSoft.MvvmLight.Messaging;
using myGovID.Services.Event.Contract;

namespace myGovID.Services.Event.MVVMLight
{
    public class MvvmEventService : IEventService
    {
        private readonly IMessenger _messenger = Messenger.Default;
        public void Publish<T>(EventKey topic, T eventObj) where T : class
        {
            _messenger.Send<T>(eventObj, topic.ToString());
        }
        public void Subscribe<T>(object subscriber, EventKey topic, Action<T> action) where T : class
        {
            _messenger.Register<T>(subscriber, topic.ToString(), action);
        }
        public void UnSubscribe<T>(object subscriber, EventKey topic) where T : class
        {
            _messenger.Unregister<T>(subscriber, topic.ToString());
        }
    }
}