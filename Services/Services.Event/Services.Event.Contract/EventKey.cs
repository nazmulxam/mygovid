﻿namespace myGovID.Services.Event.Contract
{
    public enum EventKey
    {
        Login,
        Logout,
        SessionExpired,
        Step,
        IdentityUpdated
    }
}