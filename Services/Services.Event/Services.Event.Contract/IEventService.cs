﻿using System;
namespace myGovID.Services.Event.Contract
{
    /// <summary>
    /// Event bus interface.
    /// </summary>
    public interface IEventService
    {
        /// <summary>
        /// Subscribe to the specified message from the specified source.
        /// Multiple subscribers can be listening for the same message.
        /// </summary>
        /// <param name="subscriber">The object that is subscribing to the messages. 
        /// Typically, this is specified with the 'this' keyword used within the subscribing object.</param>
        /// <param name="topic">Message to subscribe to</param>
        /// <param name="action">A callback, which takes the sender as a parameter, 
        /// that is run when the message is received by the subscriber</param>
        /// <typeparam name="T">The type of object that sends the message</typeparam>
        void Subscribe<T>(object subscriber, EventKey topic, Action<T> action) where T : class;

        /// <summary>
        /// Publish the named message to objects that are listening for it 
        /// on the type that is specified by T
        /// </summary>
        /// <param name="topic">The message that will be sent to objects that 
        /// are listening for the message from instances of type T</param>
        /// <param name="eventObj">The instance that is sending the message. 
        /// Typically, this is specified with the 'this' keyword used within the 
        /// sending object.</param>
        /// <typeparam name="T">The type of object that sends the message</typeparam>
        void Publish<T>(EventKey topic, T eventObj) where T : class;

        /// <summary>
        /// Unsubscribes the specified subscriber from the specified message.
        /// </summary>
        /// <param name="subscriber">The object that is unsubscribing to the 
        /// messages. Typically, this is specified with the 'this' keyword 
        /// used within the subscribing object.</param>
        /// <param name="topic">The message that will be sent to objects that are 
        /// listening for the message from instances of type T.</param>
        /// <typeparam name="T">The type of object that sends the message.</typeparam>
        void UnSubscribe<T>(object subscriber, EventKey topic) where T : class;
    }
}