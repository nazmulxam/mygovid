﻿namespace myGovID.Services.Api.Contract
{
    /// <summary>
    /// Status of the processed resource. 
    /// </summary>
    public enum ResourceStatus
    {
        /// <summary>
        /// Resource processing was successful.
        /// </summary>
        Success,
        /// <summary>
        /// Resource cannot be processed.
        /// </summary>
        UnprocessableEntity,
        /// <summary>
        /// Resource cannot be processed as it requires authentication.
        /// </summary>
        Unauthenticated,
        /// <summary>
        /// Unauthorised to process the resource
        /// </summary>
        Unauthorised,
        /// <summary>
        /// Unable to find the resource
        /// </summary>
        NotFound,
        /// <summary>
        /// Error occurred while processing the resource
        /// </summary>
        Error
    }
}