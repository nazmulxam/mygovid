﻿namespace myGovID.Services.Api.Contract.Model.System.Version
{
    public enum AppCapability
    {
        Facial_Verification_Passport,
        DVS_Driver_Licence,
        DVS_Medicare_Card,
        DVS_Passport,
        OCR_Driver_Licence,
        OCR_Passport,
        OCR_Medicare_Card,
        POI_Begin,
        POI_Resume,
        Authentication,
        Push_Notification,
        Menu_Settings,
        Menu_Account,
        Menu_About
    }
}
