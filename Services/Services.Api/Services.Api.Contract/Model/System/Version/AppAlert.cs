﻿using Newtonsoft.Json;
using System;
namespace myGovID.Services.Api.Contract.Model.System.Version
{
    public class AppAlert
    {

        [JsonProperty("startDateTime")]
        private string _startDateTime { get; set; }

        [JsonIgnore]
        public DateTime StartDateTime { 
            get
            {
                bool canConvert = DateTime.TryParse(_startDateTime, out DateTime output);
                return canConvert ? output : throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000046);
            }
            set{
                _startDateTime = value.ToString();
            }
        }

        [JsonProperty("endDateTime")]
        private string _endDateTime { get; set; }

        [JsonIgnore]
        public DateTime EndDateTime
        {
            get
            {
                bool canConvert = DateTime.TryParse(_endDateTime, out DateTime output);
                return canConvert ? output : throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000047);
            }
            set
            {
                _endDateTime = value.ToString();
            }
        }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("type")]
        private string _type { get; set; }

        [JsonIgnore]
        public AppAlertType Type
        {
            get
            {
                bool canConvert = Enum.TryParse(_type, true, out AppAlertType output);
                return canConvert ? output : throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000048);
            }
            set
            {
                _type = value.ToString();
            }
        }

        [JsonProperty("isSystemOutage")]
        public bool SystemOutage { get; set; }

        public void Validate()
        {
            if (string.IsNullOrWhiteSpace(_startDateTime))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000008);
            }
            DateTime startDateTime = StartDateTime;
            if (string.IsNullOrWhiteSpace(_endDateTime))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000009);
            }
            DateTime endDateTime = EndDateTime;
            if (string.IsNullOrWhiteSpace(Message))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000010);
            }
            if (string.IsNullOrWhiteSpace(_type))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000011);
            }
            AppAlertType alertType = Type;
        }
    }
}
