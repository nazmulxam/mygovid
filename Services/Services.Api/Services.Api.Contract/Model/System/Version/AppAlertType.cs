﻿namespace myGovID.Services.Api.Contract.Model.System.Version
{
    public enum AppAlertType
    {
        Information, Warning
    }
}
