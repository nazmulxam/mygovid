﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
namespace myGovID.Services.Api.Contract.Model.System.Version
{
    public class VersionResponseModel: BaseResponseModel
    {
        [JsonProperty("capabilities")]
        public IEnumerable<AppCapability> Capabilities { get; set; }

        [JsonProperty("alerts")]
        public IEnumerable<AppAlert> Alerts { get; set; }
        public override void Validate()
        {
            base.Validate();
            if (HasError())
            {
                return;
            }
            if (Alerts != null && Alerts.Any())
            {
                foreach(AppAlert appAlert in Alerts)
                {
                    appAlert.Validate();
                }
            }
        }
    }
}