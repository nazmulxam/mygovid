﻿namespace myGovID.Services.Api.Contract.Model
{
    /// <summary>
    /// Authentication type.
    /// </summary>
    public enum AuthenticationType
    {
        User, Extension, Assurance, Credential, None, Bespoke
    }
}