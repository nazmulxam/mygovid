﻿using Newtonsoft.Json;

namespace myGovID.Services.Api.Contract.Model.Poi.TermsAndConditions
{
    public class TermsAndConditionsModel
    {
        [JsonProperty("version")]
        public string Version { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        public void Validate()
        {
            if (string.IsNullOrWhiteSpace(Version))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000037);
            }
            if (string.IsNullOrWhiteSpace(Url))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000038);
            }
        }
    }
}