﻿using Newtonsoft.Json;

namespace myGovID.Services.Api.Contract.Model.Poi.TermsAndConditions
{
    public class TermsAndConditionsResponseModel: BaseResponseModel
    {
        [JsonProperty("termsAndConditions")]
        public TermsAndConditionsModel TermsAndConditions { get; set; }
        public override void Validate()
        {
            base.Validate();
            if (HasError())
            {
                return;
            }
            if (TermsAndConditions == null)
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000045);
            }
            TermsAndConditions.Validate();
        }
    }
}