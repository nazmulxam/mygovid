﻿using System;
using Newtonsoft.Json;
namespace myGovID.Services.Api.Contract.Model.Poi.Passport
{
    public class PassportDocumentRequestModel : IApiRequestModel
    {
        [JsonProperty("givenName")]
        public string GivenName { get; set; }

        [JsonProperty("familyName")]
        public string FamilyName { get; set; }

        [JsonProperty("dateOfBirth")]
        public DateOfBirthModel DateOfBirth { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("passportNumber")]
        public string PassportNumber { get; set; }

        [JsonProperty("consentProvided")]
        public bool ConsentProvided { get; set; }
    }
}
