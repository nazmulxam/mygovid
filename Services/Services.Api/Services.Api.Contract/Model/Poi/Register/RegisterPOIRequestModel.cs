﻿using Newtonsoft.Json;

namespace myGovID.Services.Api.Contract.Model.Poi.Register
{
    public class RegisterPOIRequestModel: IApiRequestModel
    {
        [JsonProperty("acceptedVersion")]
        public string AcceptedVersion { get; set; }
    }
}
