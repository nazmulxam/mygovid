﻿using Newtonsoft.Json;

namespace myGovID.Services.Api.Contract.Model.Poi.Register
{
    public class RegisterPOIResponseModel : PoiResponseModel
    {
        [JsonProperty("acceptedTermsAndConditionsVersion")]
        public string AcceptedTermsAndConditionsVersion { get; set; }
        public override void Validate()
        {
            base.Validate();
            if (HasError())
            {
                return;
            }
            if (string.IsNullOrWhiteSpace(AcceptedTermsAndConditionsVersion))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000044);
            }
        }
    }
}