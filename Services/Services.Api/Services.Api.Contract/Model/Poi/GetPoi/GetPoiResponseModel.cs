﻿using System;
using Newtonsoft.Json;

namespace myGovID.Services.Api.Contract.Model.Poi.GetPoi
{
    public class GetPoiResponseModel: PoiResponseModel
    {
        [JsonProperty("acceptedTermsAndConditionsVersion")]
        public string AcceptedTermsAndConditionsVersion { get; set; }

        [JsonProperty("strength")]
        public string Strength { get; set; }

        public override void Validate()
        {
            base.Validate();
            if (HasError())
            {
                return;
            }
            if (AcceptedTermsAndConditionsVersion == null)
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000044);
            }
            if (Strength == null)
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000050);
            }
        }
    }
}
