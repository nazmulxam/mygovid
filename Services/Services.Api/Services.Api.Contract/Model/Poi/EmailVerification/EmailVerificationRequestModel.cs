﻿using Newtonsoft.Json;
namespace myGovID.Services.Api.Contract.Model.Poi.EmailVerification
{
    public class EmailVerificationRequestModel: IApiRequestModel
    {
        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("verificationCode")]
        public string VerificationCode { get; set; }
    }
}