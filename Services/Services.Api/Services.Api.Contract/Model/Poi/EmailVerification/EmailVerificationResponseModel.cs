﻿using Newtonsoft.Json;

namespace myGovID.Services.Api.Contract.Model.Poi.EmailVerification
{
    public class EmailVerificationResponseModel: PoiResponseModel
    {
        [JsonProperty("verificationCodeResult")]
        public string VerificationCodeResult { get; set; }

        [JsonProperty("poiAssuranceToken")]
        public string PoiAssuranceToken { get; set; }

        public override void Validate()
        {
            base.Validate();
            if (HasError())
            {
                return;
            }
            if (string.IsNullOrWhiteSpace(PoiAssuranceToken))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000002);
            }
        }
    }
}
