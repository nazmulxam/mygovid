﻿using Newtonsoft.Json;

namespace myGovID.Services.Api.Contract.Model.Poi.PersonalDetails
{
    public class PersonalDetailsResponseModel : PoiResponseModel
    {
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("dateOfBirth")]
        public DateOfBirthModel DateOfBirth { get; set; }

        public override void Validate()
        {
            base.Validate();
            if (HasError())
            {
                return;
            }
            if (string.IsNullOrWhiteSpace(FirstName))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000038);
            }
            if (string.IsNullOrWhiteSpace(LastName))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000039);
            }
            if (DateOfBirth == null)
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000040);
            }
            DateOfBirth.Validate();
        }
    }
}
