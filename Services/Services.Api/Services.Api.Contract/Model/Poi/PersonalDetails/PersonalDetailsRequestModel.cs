﻿using System;
using Newtonsoft.Json;

namespace myGovID.Services.Api.Contract.Model.Poi.PersonalDetails
{
    public class PersonalDetailsRequestModel : IApiRequestModel
    {
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("dateOfBirth")]
        public DateOfBirthModel DateOfBirth { get; set; }
    }
}
