﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace myGovID.Services.Api.Contract.Model.Poi.MedicareDetails
{
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy), ItemNullValueHandling = NullValueHandling.Ignore)]
    public class MedicareDetailsRequestModel : IApiRequestModel
    {
        public string FullName1 { get; set; }
        public string FullName2 { get; set; }
        public string FullName3 { get; set; }
        public string FullName4 { get; set; }
        public string MedicareCardNumber { get; set; }
        public int IndividualReferencenumber { get; set; }
        public string CardExpiry { get; set; }
        public string CardType { get; set; }
        public bool ConsentProvided { get; set; }
    }
}
