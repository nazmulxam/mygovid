﻿using Newtonsoft.Json;
namespace myGovID.Services.Api.Contract.Model.Poi
{
    public class PoiResponseModel: BaseResponseModel
    {
        [JsonProperty("processId")]
        public string ProcessId { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        public override void Validate()
        {
            base.Validate();
            if(HasError())
            {
                return;
            }
            if (string.IsNullOrWhiteSpace(ProcessId))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000001);
            }
            // TODO: Do we get Status back always? Or Is mock missing it.
            //if (string.IsNullOrWhiteSpace(Status))
            //{
            //    throw ApiException.FIM000030);
            //}
        }
    }
}