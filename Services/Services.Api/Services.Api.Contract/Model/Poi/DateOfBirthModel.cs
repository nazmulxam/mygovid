﻿using Newtonsoft.Json;

namespace myGovID.Services.Api.Contract.Model.Poi
{
    public class DateOfBirthModel
    {
        [JsonProperty("year")]
        public int Year { get; set; }

        [JsonProperty("month")]
        public int Month { get; set; }

        [JsonProperty("day")]
        public int Date { get; set; }

        public void Validate()
        {
            if (Year <= 0)
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000041);
            }
            if (Month <= 0)
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000042);
            }
            if (Date <= 0)
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000043);
            }
        }
    }
}
