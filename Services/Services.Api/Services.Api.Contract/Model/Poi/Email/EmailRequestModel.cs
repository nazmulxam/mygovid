﻿using Newtonsoft.Json;

namespace myGovID.Services.Api.Contract.Model.Poi.Email
{
    public class EmailRequestModel: IApiRequestModel
    {
        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }
    }
}
