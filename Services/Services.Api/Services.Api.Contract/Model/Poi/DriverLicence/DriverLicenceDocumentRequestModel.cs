﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace myGovID.Services.Api.Contract.Model.Poi.DriverLicence
{
    public class DriverLicenceDocumentRequestModel : IApiRequestModel
    {
        [JsonProperty("licenceNumber")]
        public string DocumentNumber { get; set; }

        [JsonProperty("givenName")]
        public string GivenNames { get; set; }

        [JsonProperty("middleName")]
        public string MiddleName { get; set; }

        [JsonProperty("familyName")]
        public string FamilyName { get; set; }

        [JsonProperty("dateOfBirth")]
        public DateOfBirthModel DateOfBirth { get; set; }

        [JsonProperty("stateOfIssue")]
        public string State { get; set; }

        [JsonProperty("consentProvided")]
        public bool ConsentProvided { get; set; }   
    }
}
