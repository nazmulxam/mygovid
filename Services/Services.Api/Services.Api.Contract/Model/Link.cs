﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace myGovID.Services.Api.Contract.Model
{
    public class Link
    {
        [JsonProperty("href")]
        public string Location { get; set; }

        [JsonProperty("method")]
        public string Method { get; set; }

        [JsonProperty("authentication")]
        private string _authType { get; set; }

        [JsonProperty("rel")]
        private string _targetType { get; set; }

        [JsonIgnore]
        public AuthenticationType AuthType
        {
            get
            {
                bool canConvert = Enum.TryParse(_authType, true, out AuthenticationType output);
                return canConvert ? output : throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000019);
            }
            set
            {
                _authType = value.ToString();
            }
        }

        [JsonIgnore]
        public TargetType TargetType
        {
            get
            {
                bool canConvert = Enum.TryParse(_targetType, true, out TargetType output);
                return canConvert ? output : throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000020);
            }
            set
            {
                _targetType = value.ToString();
            }
        }

        internal void Validate()
        {
            if (string.IsNullOrWhiteSpace(Location))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000015);
            }
            if (string.IsNullOrWhiteSpace(Method))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000016);
            }
            if (string.IsNullOrWhiteSpace(_authType))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000017);
            }
            if (string.IsNullOrWhiteSpace(_targetType))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000018);
            }
            AuthenticationType authType = AuthType;
            TargetType targetType = TargetType;
        }
        public override string ToString()
        {
            return $"{GetType().Name} -- {Location} -- {Method} -- {_authType} -- {_targetType}";
        }
    }
}
