﻿using System;
using Newtonsoft.Json;

namespace myGovID.Services.Api.Contract.Model.Task
{
    public class TaskResponseModel : BaseResponseModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("status")]
        private string _status { get; set; }

        [JsonProperty("eta")]
        public string Eta { get; set; }

        [JsonIgnore]
        public TaskStatus Status
        {
            get
            {
                bool canConvert = Enum.TryParse(_status, true, out TaskStatus output);
                return canConvert ? output : throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000022);
            }
            set
            {
                _status = value.ToString();
            }
        }
        public override void Validate()
        {
            base.Validate();
            if (HasError())
            {
                return;
            }
            if (string.IsNullOrWhiteSpace(Id))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000049);
            }
            if (string.IsNullOrWhiteSpace(_status))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000021);
            }
            TaskStatus status = Status;
            if (string.IsNullOrWhiteSpace(Eta))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000023);
            }
        }
    }
}
