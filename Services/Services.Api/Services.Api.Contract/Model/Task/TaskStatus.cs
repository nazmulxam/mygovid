﻿using System;
namespace myGovID.Services.Api.Contract.Model.Task
{
    public enum TaskStatus
    {
        Submitted,
        Pending,
        TechnicalError,
        Failed,
        Timeout,
        Verified
    }
}
