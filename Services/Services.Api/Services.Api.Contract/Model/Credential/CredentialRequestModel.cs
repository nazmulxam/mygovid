﻿using System;
using Newtonsoft.Json;

namespace myGovID.Services.Api.Contract.Model.Credential
{
    public class CredentialRequestModel: IApiRequestModel
    {
        [JsonProperty("p10")]
        public string P10 { get; set; }
    }
}
