﻿using Newtonsoft.Json;

namespace myGovID.Services.Api.Contract.Model.Credential
{
    public class CredentialResponseModel: IResponseModel
    {
        [JsonProperty("credentialToken")]
        public string CredentialToken { get; set; }

        [JsonProperty("p7")]
        public string P7 { get; set; }

        public void Validate()
        {
            if(string.IsNullOrWhiteSpace(CredentialToken))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000003);
            }
            if (string.IsNullOrWhiteSpace(P7))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000004);
            }
        }
    }
}
