﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace myGovID.Services.Api.Contract.Model
{
    public class BaseResponseModel: IResponseModel
    {
        [JsonProperty("links")]
        public IEnumerable<Link> Links { get; set; }

        [JsonProperty("messages")]
        public IEnumerable<AppMessage> Messages { get; set; }

        public virtual void Validate()
        {
            if (Links != null && Links.Any())
            {
                foreach(Link link in Links)
                {
                    link.Validate();
                }
            }
            if (Messages != null && Messages.Any())
            {
                foreach (AppMessage message in Messages)
                {
                    message.Validate();
                }
            }
        }
        protected bool HasError()
        {
            return Messages != null && Messages.Any((it) => it.Type.ToLower().Equals("error"));
        }
    }
}