﻿namespace myGovID.Services.Api.Contract.Model
{
    /// <summary>
    /// Target type.
    /// </summary>
    public enum TargetType
    {
        Self, Next, Clone, Cancel, Update, Process
    }
}