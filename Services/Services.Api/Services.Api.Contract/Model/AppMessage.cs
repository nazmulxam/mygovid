﻿using Newtonsoft.Json;
namespace myGovID.Services.Api.Contract.Model
{
    public class AppMessage
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("parameter")]
        public string Parameter { get; set; }

        internal void Validate()
        {
            if (string.IsNullOrWhiteSpace(Code))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000012);
            }
            if (string.IsNullOrWhiteSpace(Type))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000013);
            }
            if (string.IsNullOrWhiteSpace(Description))
            {
                throw new InvalidApiResponseException(InvalidApiResponseCode.FIM000014);
            }
        }
    }
}