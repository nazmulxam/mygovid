﻿using System.Threading.Tasks;

namespace myGovID.Services.Api.Contract
{
    public interface IApiService
    {
        /// <summary>
        /// Send the specified apiRequest.
        /// </summary>
        /// <returns>Api response of type T</returns>
        /// <param name="apiRequest">API request of type IApiRequestModel</param>
        /// <typeparam name="I">The 1st type parameter - instance of class,IApiRequestModel</typeparam>
        /// <typeparam name="T">The 2nd type parameter - instance of class</typeparam>
        Task<ApiResult<T>> SendAsync<I, T>(IApiRequest<I> apiRequest) where I : class, IApiRequestModel where T: class;
    }
}