﻿using System;
using System.Collections.Generic;
using System.Text;
using myGovID.Services.Api.Contract.Model;
using Newtonsoft.Json;
using System.Linq;

namespace myGovID.Services.Api.Contract
{
    public class ApiHttpRequest<T> : IApiRequest<T> where T : class, IApiRequestModel
    {
        public Link Link { get; }
        public byte[] Payload { get; }
        public Dictionary<string, string> Headers { get; }
        public string SessionId { get; }
        public T Request { get; }
        public ApiHttpRequest(Link link, T request, string sessionId, string authorisation = null, Dictionary<string, string> headers = null)
        {
            if (string.IsNullOrWhiteSpace(sessionId))
            {
                throw new ArgumentException(nameof(sessionId));
            }
            if (link.AuthType != AuthenticationType.None && string.IsNullOrWhiteSpace(authorisation))
            {
                throw new ArgumentException("Authorisation token cannot be null.", nameof(authorisation));
            }
            Link = link ?? throw new ArgumentNullException(nameof(link));
            Request = request;
            SessionId = sessionId;
            Payload = request == null ? null : Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(request));
            Headers = new Dictionary<string, string>();
            if (headers != null)
            {
                foreach (KeyValuePair<string, string> pair in headers)
                {
                    Headers[pair.Key] = pair.Value;
                }
            }
            if (!Headers.ContainsKey("Content-Type"))
            {
                Headers.Add("Content-Type", "application/json");
            }
            if (!Headers.ContainsKey("Accept"))
            {
                Headers.Add("Accept", "application/json");
            }
            if (!Headers.ContainsKey("Authorization") && !string.IsNullOrWhiteSpace(authorisation))
            {
                Headers.Add("Authorization", "Bearer " + authorisation);
            }
        }
        public override string ToString()
        {
            string headers = Headers == null ? "" : Headers.Select(x => x.Key + "=" + x.Value).Aggregate((s1, s2) => s1 + ";" + s2);
            string requestType = Request == null ? "" : Request.GetType().Name;
            string result = $"\n{GetType().Name} ---HTTP Request ({requestType})---\n{Link}\n{headers}\n";
            if (Payload != null)
            {
                result += $"{Encoding.UTF8.GetString(Payload)}\n";
            }
            return result;
        }
    }
}