﻿using Newtonsoft.Json;
namespace myGovID.Services.Api.Contract
{
    public class ApiResult<T>
    {
        public ResourceStatus ResourceStatus { get; }
        public string ResourceLocation;
        public T Resource;
        public ApiResult(ResourceStatus resourceStatus, string resourceLocation, T resource)
        {
            ResourceStatus = resourceStatus;
            ResourceLocation = resourceLocation;
            Resource = resource;
        }
        public override string ToString()
        {
            string resourceType = Resource == null ? "" : Resource.GetType().Name;
            return $"{GetType().Name} ---HTTP Response ({resourceType})---\n{ResourceStatus}\n{ResourceLocation}\n{JsonConvert.SerializeObject(Resource)}";
        }
    }
}