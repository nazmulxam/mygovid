﻿namespace myGovID.Services.Api.Contract
{
    public interface IResponseModel {
        void Validate();
    }
}