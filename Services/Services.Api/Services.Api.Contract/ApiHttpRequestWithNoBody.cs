﻿using System.Collections.Generic;
using myGovID.Services.Api.Contract.Model;

namespace myGovID.Services.Api.Contract
{
    public class ApiHttpRequestWithNoBody : ApiHttpRequest<IApiRequestModel>
    {
        public ApiHttpRequestWithNoBody(Link link, string sessionId, string authorisation = null, Dictionary<string, string> headers = null) : base(link, null, sessionId, authorisation, headers)
        {
        }
    }
}