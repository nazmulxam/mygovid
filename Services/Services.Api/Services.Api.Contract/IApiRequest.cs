﻿using myGovID.Services.Api.Contract.Model;

namespace myGovID.Services.Api.Contract
{
    public interface IApiRequest<T> where T: class, IApiRequestModel
    {
        /// <summary>
        /// Gets the link of the api request
        /// </summary>
        /// <value>The link.</value>
        Link Link { get; }
        /// <summary>
        /// Gets the request model.
        /// </summary>
        /// <value>The request.</value>
        T Request { get; }
    }
}
