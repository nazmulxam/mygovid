﻿namespace myGovID.Services.Api.Contract
{
    public enum InvalidApiResponseCode
    {
        FIM000001, FIM000002, FIM000003, FIM000004, FIM000005, FIM000006, FIM000007, FIM000008, FIM000009, FIM000010,
        FIM000011, FIM000012, FIM000013, FIM000014, FIM000015, FIM000016, FIM000017, FIM000018, FIM000019, FIM000020,
        FIM000021, FIM000022, FIM000023, FIM000024, FIM000025, FIM000026, FIM000027, FIM000028, FIM000029, FIM000030,
        FIM000031, FIM000032, FIM000033, FIM000034, FIM000035, FIM000036, FIM000037, FIM000038, FIM000039, FIM000040,
        FIM000041, FIM000042, FIM000043, FIM000044, FIM000045, FIM000046, FIM000047, FIM000048, FIM000049, FIM000050,
        FIM000051
    }
    public static class Extensions
    {
        public static string GetDescription(this InvalidApiResponseCode code)
        {
            switch (code)
            {
                case InvalidApiResponseCode.FIM000001: return "ProcessId is missing.";
                case InvalidApiResponseCode.FIM000002: return "poiAssuranceToken is missing.";
                case InvalidApiResponseCode.FIM000003: return "CredentialToken is missing.";
                case InvalidApiResponseCode.FIM000004: return "p7 is missing.";
                case InvalidApiResponseCode.FIM000005: return "access_token is missing.";
                case InvalidApiResponseCode.FIM000006: return "token_type is missing.";
                case InvalidApiResponseCode.FIM000007: return "expires_in is missing.";
                case InvalidApiResponseCode.FIM000008: return "Alert startDateTime is missing.";
                case InvalidApiResponseCode.FIM000009: return "Alert endDateTime is missing.";
                case InvalidApiResponseCode.FIM000010: return "Alert message is missing.";
                case InvalidApiResponseCode.FIM000011: return "Alert type is missing.";
                case InvalidApiResponseCode.FIM000012: return "Message code is missing.";
                case InvalidApiResponseCode.FIM000013: return "Message type is missing.";
                case InvalidApiResponseCode.FIM000014: return "Message description is missing.";
                case InvalidApiResponseCode.FIM000015: return "Links uri is missing.";
                case InvalidApiResponseCode.FIM000016: return "Links method is missing.";
                case InvalidApiResponseCode.FIM000017: return "Links authentication is missing.";
                case InvalidApiResponseCode.FIM000018: return "Links rel is missing.";
                case InvalidApiResponseCode.FIM000019: return "Unknown authentication type.";
                case InvalidApiResponseCode.FIM000020: return "Unknown target(rel) type.";
                case InvalidApiResponseCode.FIM000021: return "Missing task status.";
                case InvalidApiResponseCode.FIM000022: return "Unknown task status type.";
                case InvalidApiResponseCode.FIM000023: return "Missing task eta.";
                case InvalidApiResponseCode.FIM000024: return "Unknown message type.";
                case InvalidApiResponseCode.FIM000025: return "Missing createdDate.";
                case InvalidApiResponseCode.FIM000026: return "Missing finalisedDate.";
                case InvalidApiResponseCode.FIM000027: return "Missing correlationId.";
                case InvalidApiResponseCode.FIM000028: return "Missing eventStatusType.";
                case InvalidApiResponseCode.FIM000029: return "Missing or invalid type.";
                case InvalidApiResponseCode.FIM000030: return "Invalid poi status type.";
                case InvalidApiResponseCode.FIM000031: return "Unsupported poi status type.";
                case InvalidApiResponseCode.FIM000032: return "Auth request action type.";
                case InvalidApiResponseCode.FIM000033: return "POI status is invalid.";
                case InvalidApiResponseCode.FIM000034: return "Identity strength is invalid.";
                case InvalidApiResponseCode.FIM000035: return "Event missing reference code.";
                case InvalidApiResponseCode.FIM000036: return "Event missing additional data.";
                case InvalidApiResponseCode.FIM000037: return "Missing Terms and Conditions version.";
                case InvalidApiResponseCode.FIM000038: return "Missing firstName.";
                case InvalidApiResponseCode.FIM000039: return "Missing lastName.";
                case InvalidApiResponseCode.FIM000040: return "Missing dateOfBirth.";
                case InvalidApiResponseCode.FIM000041: return "Missing year.";
                case InvalidApiResponseCode.FIM000042: return "Missing month.";
                case InvalidApiResponseCode.FIM000043: return "Missing date.";
                case InvalidApiResponseCode.FIM000044: return "Missing AcceptedTermsAndConditionsVersion";
                case InvalidApiResponseCode.FIM000045: return "Missing TermsAndConditions.";
                case InvalidApiResponseCode.FIM000046: return "Alert startDateTime parsing failed.";
                case InvalidApiResponseCode.FIM000047: return "Alert endDateTime parsing failed.";
                case InvalidApiResponseCode.FIM000048: return "Unknown Alert type.";
                case InvalidApiResponseCode.FIM000049: return "Missing Task Id.";
                case InvalidApiResponseCode.FIM000050: return "Missing Strength.";
                case InvalidApiResponseCode.FIM000051: return "Missing State of Issue on Driver Licence.";
                default: return "API: Unknown error";
            }
        }
    }
}