﻿using myGovID.Core.Exceptions;

namespace myGovID.Services.Api.Contract
{
    public class InvalidApiResponseException: CodedException
    {
        public InvalidApiResponseException(InvalidApiResponseCode serviceResponseCode): base(serviceResponseCode.ToString(), "", serviceResponseCode.GetDescription())
        {
        }
    }
}