﻿using System.Net.Http;
using System.Threading.Tasks;
using myGovID.Services.Api.Contract;
using myGovID.Services.Http.Contract;

namespace myGovID.Services.Api.Http
{
    public class ApiService : BaseApiService, IApiService
    {
        private readonly IHttpService _httpService;

        public ApiService(IHttpService httpService, IHttpConfig config) : base(config)
        {
            _httpService = httpService;
        }

        public async Task<ApiResult<T>> SendAsync<I, T>(IApiRequest<I> apiRequest) where I : class, IApiRequestModel where T : class
        {
            HttpResponseMessage response = await _httpService.SendAsync(GetHttpRequestMessage((ApiHttpRequest<I>)apiRequest));
            return await GetHttpResponseAsync<T>(response);
        }
    }
}