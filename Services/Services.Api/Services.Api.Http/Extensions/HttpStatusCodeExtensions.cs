﻿using System.Net;
using myGovID.Services.Api.Contract;

namespace myGovID.Services.Api.Http.Extensions
{
    public static class HttpStatusCodeExtensions
    {
        /// <summary>
        /// Gets the resource status based on http status code
        /// </summary>
        /// <returns>The resource status.</returns>
        /// <param name="code">Http status code</param>
        public static ResourceStatus GetResourceStatus(this HttpStatusCode code)
        {
            int statusCode = (int)code;
            if (statusCode >= 200 && statusCode < 300)
            {
                return ResourceStatus.Success;
            }
            if (statusCode == 422)
            {
                return ResourceStatus.UnprocessableEntity;
            }
            if (statusCode == 401)
            {
                return ResourceStatus.Unauthenticated;
            }
            if (statusCode == 403)
            {
                return ResourceStatus.Unauthorised;
            }
            return statusCode == 404 ? ResourceStatus.NotFound : ResourceStatus.Error;
        }
    }
}
