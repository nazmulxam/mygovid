﻿using System.Net.Http;

namespace myGovID.Services.Api.Http.Extensions
{
    internal static class StringExtensions 
    {
        internal static HttpMethod GetMethod(this string method)
        {
            if (method == null)
            {
                return null;
            }
            switch (method.ToLower())
            {
                case "get": return HttpMethod.Get;
                case "post": return HttpMethod.Post;
                case "put": return HttpMethod.Put;
                case "delete": return HttpMethod.Delete;
                default: return null;
            }
        }
    }
}
