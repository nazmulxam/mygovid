﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using myGovID.Services.Api.Contract;
using myGovID.Services.Api.Http.Extensions;
using myGovID.Services.Http.Contract;
using Newtonsoft.Json;

namespace myGovID.Services.Api.Http
{
    public class BaseApiService
    {
        protected IHttpConfig _config { get; }

        public BaseApiService(IHttpConfig config)
        {
            if (config == null)
            {
                throw new ArgumentNullException(nameof(config));
            }

            _config = config;
        }
        /// <summary>
        /// Gets the http response.
        /// </summary>
        /// <returns>The http response.</returns>
        /// <param name="message">Message.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public async Task<ApiResult<T>> GetHttpResponseAsync<T>(HttpResponseMessage message) where T : class
        {
            byte[] response = await message.Content.ReadAsByteArrayAsync();
            string json = Encoding.UTF8.GetString(response);
            var resObj = typeof(T) == typeof(string) ? json as T : JsonConvert.DeserializeObject<T>(json);
            if (resObj is IResponseModel responseModel)
            {
                responseModel.Validate();
            }
            return new ApiResult<T>(message.StatusCode.GetResourceStatus(),
                                    GetResourceLocation(message), resObj);
        }
        /// <summary>
        /// Gets the http request message.
        /// </summary>
        /// <returns>The http request message.</returns>
        /// <param name="request">Request.</param>
        /// <typeparam name="I">The 1st type parameter.</typeparam>
        public HttpRequestMessage GetHttpRequestMessage<I>(ApiHttpRequest<I> request) where I : class, IApiRequestModel
        {
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(
                                        request.Link.Method.GetMethod(),
                                        FormatLocation(request.Link.Location));
            if (request.Payload != null)
            {
                httpRequestMessage.Content = new ByteArrayContent(request.Payload);
            }
            foreach (KeyValuePair<string, string> header in request.Headers)
            {
                if (header.Key.Equals("Content-Type"))
                {
                    if (request.Payload != null)
                    {
                        httpRequestMessage.Content.Headers.Add(header.Key, header.Value);
                    }
                }
                else
                {
                    httpRequestMessage.Headers.Add(header.Key, header.Value);
                }
            }
            if (!request.Headers.ContainsKey("X-AuditCallingAppName"))
            {
                httpRequestMessage.Headers.Add("X-AuditCallingAppName", _config.ApplicationName);
            }
            if (!request.Headers.ContainsKey("X-AuditCallingAppVersion"))
            {
                httpRequestMessage.Headers.Add("X-AuditCallingAppVersion", _config.ApplicationVersion);
            }
            if (!request.Headers.ContainsKey("X-AuditSessionId"))
            {
                httpRequestMessage.Headers.Add("X-AuditSessionId", request.SessionId);
            }
            if (!request.Headers.ContainsKey("X-AuditRequestId"))
            {
                httpRequestMessage.Headers.Add("X-AuditRequestId", Guid.NewGuid().ToString());
            }
            return httpRequestMessage;
        }
        /// <summary>
        /// Gets the resource location header from http response message
        /// </summary>
        /// <returns>The resource location.</returns>
        /// <param name="message">Message.</param>
        public string GetResourceLocation(HttpResponseMessage message)
        {
            return message.Headers.Contains("Location") ?
                   message.Headers.GetValues("Location").First() : null;
        }
        /// <summary>
        /// Formats the location.
        /// </summary>
        /// <returns>The location.</returns>
        /// <param name="location">Location.</param>
        public string FormatLocation(string location)
        {
            return location.StartsWith("http", StringComparison.InvariantCulture)
                ? location
                : $"{_config.BaseUrl.TrimEnd('/')}/{location.TrimStart('/')}";
        }
    }
}
