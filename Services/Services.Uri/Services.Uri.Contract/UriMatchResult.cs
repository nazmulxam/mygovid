﻿using System.Collections.Generic;
namespace myGovID.Services.Uri.Contract
{
    public class UriMatchResult
    {
        public bool Match { get; }
        public IDictionary<string, string> BoundVariables { get; }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:myGovID.Services.Uri.Contract.UriMatchResult"/> class.
        /// </summary>
        /// <param name="match">If set to <c>true</c> match.</param>
        /// <param name="boundVariables">Bound variables.</param>
        public UriMatchResult(bool match, IDictionary<string, string> boundVariables)
        {
            Match = match;
            BoundVariables = boundVariables;
        }
    }
}