﻿namespace myGovID.Services.Uri.Contract
{
    /// <summary>
    /// URI service.
    /// </summary>
    public interface IUriService
    {
        /// <summary>
        /// Match the url against given uri template. 
        /// </summary>
        /// <returns>The matched result along with the bound variables.</returns>
        /// <param name="template">Url Template.</param>
        /// <param name="url">Actual URL.</param>
        /// <param name="baseUri">Base URL.</param>
        UriMatchResult Match(string template, string url, string baseUri = "http://localhost");
    }
}