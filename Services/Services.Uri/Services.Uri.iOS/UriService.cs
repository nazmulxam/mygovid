﻿using System;
using System.Collections.Generic;
using myGovID.Services.Uri.Contract;

namespace myGovID.Services.Uri.iOS
{
    public class UriService : IUriService
    {
        public UriMatchResult Match(string template, string url, string baseUri = "http://localhost")
        {
            if (template == url)
            {
                return new UriMatchResult(true, null);
            }
            if (FrameUrl(baseUri, template) == url)
            {
                return new UriMatchResult(true, null);
            }
            UriTemplate uriTemplate = new UriTemplate(template);
            UriTemplateMatch match = uriTemplate.Match(new System.Uri(baseUri), new System.Uri(url));
            if (match == null)
            {
                return new UriMatchResult(false, null);
            }
            IDictionary<string, string> variables = new Dictionary<string, string>();
            if (match.BoundVariables.HasKeys())
            {
                foreach (string varName in match.BoundVariables.Keys)
                {
                    variables.Add(varName, match.BoundVariables[varName]);
                }
            }
            return new UriMatchResult(match.BoundVariables.Keys.Count > 0, variables);
        }
        private string FrameUrl(string baseUrl, string contextPath)
        {
            if (contextPath.StartsWith("http", StringComparison.InvariantCulture))
            {
                return contextPath;
            }
            string baseUrlWithSlash = baseUrl.EndsWith("/", StringComparison.InvariantCulture) ? baseUrl : (baseUrl + "/");
            string contextPathWithoutSlash = contextPath.StartsWith("/", StringComparison.InvariantCulture)
                                      ? contextPath.Substring(1)
                                      : contextPath;
            return baseUrlWithSlash + contextPathWithoutSlash;
        }
    }
}