﻿using Moq;
using myGovID.Business.Component;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Model;
using myGovID.Services.Crypto.Contract;
using myGovID.Services.Storage.Contract;
using myGovID.Test;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace myGovID.Business.Test.Unit.Component
{
    [TestFixture()]
    public abstract class CryptoComponentRandomStringBaseSpecification : TestSpecification
    {
        protected const string PrivateKey = @"-----BEGIN PRIVATE KEY-----
MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCsfryl/ySdFAkb
Nru+7T8VtRlXNKYfIBOzv3+JS1iGzKcWPQ2tCQTfj/HiLboZRS6jD67Xl1t5BzNx
wO2jLxKOobJL0xKplax4x5T5/Yv0wLPm6kizKRcqtDCbNh5f9ntQzAvcFazfsdEv
FVrojD7RC5MuwUXtrUQ0vn3TjVks7wnhXl90qQn209Y3G4zjOrFspkA1r25J/nH+
3ED5kJDaSfi+DsHy6hRXG5ee9ThMlpTlzUV6wIZm5t8pHvdglofo2q34Wz1iLcay
lWcQ3pZuc0jXKss3q5EkkN0Da9/ZM15QgDZmG/WAYIBcLOv187fkZhZiWK7PD7PS
muJ4g8AhAgMBAAECggEAJeNUlnV7WlE7StHz3RUJZUA5B7GVx4JpMXMflU2KbVlo
XfcdGOPBQUXB0HdQyBotCz33nn0tZ2EQiKzixZQiVFfp7MO49qzcjE7yRxLM+f6V
xWOygj/FSej4QC9b7qIaJf4boMTWCvBozRm4tRmQ9qO/OWbUcEc9kwRAaQZEDx05
rpPvUqbXT60PkQtxDYReAWxJKw4XGH6wyd21DFgsw7kjAkQ5ENBScpEf33BLapW7
MKZs/7O3P/L4zEULfakRl0QNm3deVJCujVX4XxcYOjp6L5AgBdlje8OG4y38+6LB
m825kMHe7a+PmxeEhIMsKscbVBvtI6UX4iq82WyoQQKBgQDeG0EWk9XUCM3g5cym
byQeelkjOzkkgnluklcu0qy5d6x1HLJdEdUnffVXv4WtvpJbMtEad3XkVwhJRrnw
/HFYtvKEw6ijNawmdQ8er6Whj1xtXaS+wanuAh26wQ/2Ve/IxMRcZ9Kn6l+VvEBt
LPj3zxhuPv6xBHbQr9omVE6G+QKBgQDG0WDiHLNgx6gbqG6AJKPJflzJwt1lmdGR
eON8yJh0BzCTD0L5kfjJTNLn9OpuUOPzlomcK30EDro0OnarwGqOGhm6IsWZX+/0
m2oeP57MsRZjOAmfKUdzfRgoRoFBbO6XHl/Ur2OTV64OTjRQVTYi+wyjAzGQZGOC
GhS+++aEaQKBgBgktkqR/2hxHJAZ6skcw7xcaFwgD/IcDL8sQTXhyMzQaXrBh9H+
IkGMu6KyOwHL+XTYbUqHOdV8cHYmrge4KUoPeQVdj3NOXkw/5UjcpxCM2Os6hrIF
s0cgC8NFQUrxxIL6gK3ay1ddbs6EDCzR+q5VYL57Moxjg9bjhr8r4u1hAoGAPbLo
cAMcH6KWJs8kS5wTQAp43Rwd/NO5V32+ixDtTsSUbG7GCBEmtcGjidxVjBqKQyOq
XM6aI1I2stKZ1T8HUkYDSeJUk/5TxwMb8Xso2rjyC6RuR/hVdh+SCZ2nqxgVCsyg
TK6JJkaFe2cFkRCAlmZ2S3hIP1kIEG3rlkEyDlECgYAkCar7is3hwX0PDywqWm3J
cuRvjcJo4T3d9JVLFqOjAuvBs2iPltS4ix/nL2yTY6q6QcTfrTTNW+aCV6n7FgiL
BJJBJ10cF0TusXFRJiS2PEXceQj8W3x15+fxAF5STMwxE3aRFO3dALn1gYxavSVb
YMPmf0dGlpEypr43lJ5PUQ==
-----END PRIVATE KEY-----";
        protected Mock<ICryptoService> MockCryptoService;
        protected Mock<IStorage> MockStorage;
        protected CryptoComponent CryptoComponent;
        protected abstract RandomStringSpecification Specification { get; }
        protected string Result;

        protected override void Given()
        {
            MockCryptoService = new Mock<ICryptoService>();
            MockStorage = new Mock<IStorage>();

            CryptoComponent = new CryptoComponent(MockCryptoService.Object, MockStorage.Object);
        }

        protected override void When()
        {
            Result = CryptoComponent.GenerateRandomString(Specification);
        }
    }

    public class WhenPasswordSpecification : CryptoComponentRandomStringBaseSpecification
    {
        protected override RandomStringSpecification Specification => new PasswordRandomStringSpecification();

        [Test()]
        public void ShouldGeneratePasswordMatchingSpecificationLengthCharacters()
        {
            Assert.AreEqual(Specification.Length, Result.Length);
        }
    }

    [TestFixture()]
    public class CryptoComponentRandomStringUniquenessSpecification : TestSpecification
    {
        protected Mock<ICryptoService> MockCryptoService;
        protected Mock<IStorage> MockStorage;
        protected CryptoComponent CryptoComponent;
        protected RandomStringSpecification Specification = new PasswordRandomStringSpecification();
        protected string[] Result;
        protected readonly int SampleSize = 100000;

        protected override void Given()
        {
            MockCryptoService = new Mock<ICryptoService>();
            MockStorage = new Mock<IStorage>();

            CryptoComponent = new CryptoComponent(MockCryptoService.Object, MockStorage.Object);
        }

        protected override void When()
        {
            Result = new string[SampleSize];
            for (int i = 0; i < SampleSize; i++)
            {
                Result[i] = CryptoComponent.GenerateRandomString(Specification);
            }
        }

        [Test()]
        public void ShouldHaveEqualDistributionOfLettersWithin3Percent()
        {
            Dictionary<char, int> frequencyTable = new Dictionary<char, int>();
            foreach (var password in Result)
            {
                foreach (var letter in password)
                {
                    if (frequencyTable.ContainsKey(letter))
                    {
                        frequencyTable[letter] = frequencyTable[letter] + 1;
                    }
                    else
                    {
                        frequencyTable.Add(letter, 1);
                    }
                }
            }

            var frequencyBaseline = (Specification.Length * SampleSize) / Specification.CharacterSet.Length;
            foreach (var record in frequencyTable)
            {
                var encounteredInstances = (double)record.Value;
                var expectedInstances = (double)frequencyBaseline;
                var relativeFrequencyPercentage = encounteredInstances / expectedInstances * 100 - 100;
                Assert.IsTrue(relativeFrequencyPercentage < 3, string.Format("Letter {0} was higher than tolerance at {1} percent", record.Key, relativeFrequencyPercentage));
            }
        }
    }
}
