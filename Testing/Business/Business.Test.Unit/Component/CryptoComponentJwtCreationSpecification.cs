﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Moq;
using myGovID.Business.Component;
using myGovID.Business.Contract;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Session;
using myGovID.Business.Contract.Component;
using myGovID.Business.Mapper;
using myGovID.Business.Mock;
using myGovID.Business.Session;
using myGovID.Presentation.Mapper;
using myGovID.Services.Api.Contract;
using myGovID.Services.Crypto.Contract;
using myGovID.Services.Event.Contract;
using myGovID.Services.Http.Contract;
using myGovID.Services.OpenId.Contract;
using myGovID.Services.Storage.Contract;
using myGovID.Test;
using NUnit.Framework;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;

namespace myGovID.Business.Test.Unit.Component
{
    [TestFixture()]
    public abstract class CryptoComponentJwtCreationSpecification : TestSpecification
    {
        protected const string PrivateKey = @"-----BEGIN PRIVATE KEY-----
MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCsfryl/ySdFAkb
Nru+7T8VtRlXNKYfIBOzv3+JS1iGzKcWPQ2tCQTfj/HiLboZRS6jD67Xl1t5BzNx
wO2jLxKOobJL0xKplax4x5T5/Yv0wLPm6kizKRcqtDCbNh5f9ntQzAvcFazfsdEv
FVrojD7RC5MuwUXtrUQ0vn3TjVks7wnhXl90qQn209Y3G4zjOrFspkA1r25J/nH+
3ED5kJDaSfi+DsHy6hRXG5ee9ThMlpTlzUV6wIZm5t8pHvdglofo2q34Wz1iLcay
lWcQ3pZuc0jXKss3q5EkkN0Da9/ZM15QgDZmG/WAYIBcLOv187fkZhZiWK7PD7PS
muJ4g8AhAgMBAAECggEAJeNUlnV7WlE7StHz3RUJZUA5B7GVx4JpMXMflU2KbVlo
XfcdGOPBQUXB0HdQyBotCz33nn0tZ2EQiKzixZQiVFfp7MO49qzcjE7yRxLM+f6V
xWOygj/FSej4QC9b7qIaJf4boMTWCvBozRm4tRmQ9qO/OWbUcEc9kwRAaQZEDx05
rpPvUqbXT60PkQtxDYReAWxJKw4XGH6wyd21DFgsw7kjAkQ5ENBScpEf33BLapW7
MKZs/7O3P/L4zEULfakRl0QNm3deVJCujVX4XxcYOjp6L5AgBdlje8OG4y38+6LB
m825kMHe7a+PmxeEhIMsKscbVBvtI6UX4iq82WyoQQKBgQDeG0EWk9XUCM3g5cym
byQeelkjOzkkgnluklcu0qy5d6x1HLJdEdUnffVXv4WtvpJbMtEad3XkVwhJRrnw
/HFYtvKEw6ijNawmdQ8er6Whj1xtXaS+wanuAh26wQ/2Ve/IxMRcZ9Kn6l+VvEBt
LPj3zxhuPv6xBHbQr9omVE6G+QKBgQDG0WDiHLNgx6gbqG6AJKPJflzJwt1lmdGR
eON8yJh0BzCTD0L5kfjJTNLn9OpuUOPzlomcK30EDro0OnarwGqOGhm6IsWZX+/0
m2oeP57MsRZjOAmfKUdzfRgoRoFBbO6XHl/Ur2OTV64OTjRQVTYi+wyjAzGQZGOC
GhS+++aEaQKBgBgktkqR/2hxHJAZ6skcw7xcaFwgD/IcDL8sQTXhyMzQaXrBh9H+
IkGMu6KyOwHL+XTYbUqHOdV8cHYmrge4KUoPeQVdj3NOXkw/5UjcpxCM2Os6hrIF
s0cgC8NFQUrxxIL6gK3ay1ddbs6EDCzR+q5VYL57Moxjg9bjhr8r4u1hAoGAPbLo
cAMcH6KWJs8kS5wTQAp43Rwd/NO5V32+ixDtTsSUbG7GCBEmtcGjidxVjBqKQyOq
XM6aI1I2stKZ1T8HUkYDSeJUk/5TxwMb8Xso2rjyC6RuR/hVdh+SCZ2nqxgVCsyg
TK6JJkaFe2cFkRCAlmZ2S3hIP1kIEG3rlkEyDlECgYAkCar7is3hwX0PDywqWm3J
cuRvjcJo4T3d9JVLFqOjAuvBs2iPltS4ix/nL2yTY6q6QcTfrTTNW+aCV6n7FgiL
BJJBJ10cF0TusXFRJiS2PEXceQj8W3x15+fxAF5STMwxE3aRFO3dALn1gYxavSVb
YMPmf0dGlpEypr43lJ5PUQ==
-----END PRIVATE KEY-----";
        protected Mock<ICryptoComponent> MockCryptoComponent;
        protected Mock<ISession> MockSession;
        protected Mock<IEventService> MockEventService;
        protected Mock<ITokenStorageComponent> MockTokenStorageComponent;
        protected Mock<ISessionManager> MockSessionManager;
        protected TokenServiceComponent TokenComponent;
        protected Task<string> Result;
        protected Exception CaughtException;
        protected string ExtensionGrant;

        protected override void Given()
        {
            AutoMapper.Mapper.Reset();
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<BusinessMapperProfile>();
                cfg.AddProfile<ServiceMapperProfile>();
            });
            AutoMapper.Mapper.AssertConfigurationIsValid();

            MockCryptoComponent = InitialiseCryptoComponent();
            MockSession = InitialiseAuthenticatedContext();
            MockEventService = InitialiseEventService();
            MockTokenStorageComponent = InitialiseMockTokenStorageComponent();
            MockSessionManager = InitialiseMockSessionManager();

            TokenComponent = new TokenServiceComponent(MockSession.Object, MockCryptoComponent.Object, MockEventService.Object, MockTokenStorageComponent.Object, MockSessionManager.Object);
        }

        protected virtual Mock<ITokenStorageComponent> InitialiseMockTokenStorageComponent()
        {
            var mock = new Mock<ITokenStorageComponent>();
            mock.Setup(m => m.Get(It.IsAny<BusinessTokenType>()))
                .Returns("");
            mock.Setup(m => m.Set(It.IsAny<BusinessTokenType>(), It.IsAny<string>()));
            return mock;
        }

        protected virtual Mock<ISessionManager> InitialiseMockSessionManager()
        {
            var mock = new Mock<ISessionManager>();
            mock.Setup(m => m.LoginUsingKeystoreAsync())
                .Returns(GetLoginResult(true));
            return mock;
        }

        private Task<bool> GetLoginResult(bool result)
        {
            return Task.Factory.StartNew(() =>
            {
                return result;
            });
        }

        protected virtual Mock<IHttpConfig> InitialiseMockConfig()
        {
            var mock = new Mock<IHttpConfig>();
            mock.Setup(m => m.BaseUrl).Returns("http://localhost");
            return mock;
        }

        protected virtual Mock<IEventService> InitialiseEventService()
        {
            var mock = new Mock<IEventService>();
            mock.Setup(m => m.Publish<object>(It.IsAny<EventKey>(), It.IsAny<object>()));
            return mock;
        }

        protected virtual Mock<ISession> InitialiseAuthenticatedContext()
        {
            var mock = new Mock<ISession>();
            mock.Setup(m => m.AuthenticatedContext.CurrentCredential).Returns(new KeystoreCredential(MockCryptoComponent.Object));
            mock.Setup(m => m.AuthenticatedContext.Current).Returns(new MyGovPrincipal("Test", "User", "18/01/1981", "test@tester.com", "123456", IdentityProofingLevel.IP1));
            return mock;
        }

        protected virtual Mock<ICryptoComponent> InitialiseCryptoComponent()
        {
            var mock = new Mock<ICryptoComponent>();
            mock.Setup(m => m.GetRSAParameters())
                .Returns(GetRsaParams(PrivateKey));
            mock.Setup(m => m.GetCredentialId())
                .Returns("65539dd97bede48e4f9b854b604ce768");
            return mock;
        }

        protected override void When()
        {
            try
            {
                Result = TokenComponent.CreateAsync(Contract.BusinessTokenType.ExtensionGrant);
            }
            catch (Exception ex)
            {
                CaughtException = ex;
            }
        }

        private RSAParameters GetRsaParams(string key)
        {
            RsaPrivateCrtKeyParameters keyParam;

            using (var sr = new StringReader(key))
            {
                PemReader pr = new PemReader(sr);

                keyParam = (RsaPrivateCrtKeyParameters)pr.ReadObject();
            }

            return ToRsaParameters(keyParam);

        }

        private RSAParameters ToRsaParameters(RsaPrivateCrtKeyParameters privateKey)
        {
            RSAParameters rp = new RSAParameters();
            rp.Modulus = privateKey.Modulus.ToByteArrayUnsigned();
            rp.Exponent = privateKey.PublicExponent.ToByteArrayUnsigned();
            rp.D = privateKey.Exponent.ToByteArrayUnsigned();
            rp.P = privateKey.P.ToByteArrayUnsigned();
            rp.Q = privateKey.Q.ToByteArrayUnsigned();
            rp.DP = privateKey.DP.ToByteArrayUnsigned();
            rp.DQ = privateKey.DQ.ToByteArrayUnsigned();
            rp.InverseQ = privateKey.QInv.ToByteArrayUnsigned();

            return rp;
        }

    }

    public class CryptoComponentJwtCreationWithValidKeyAndNoExistingTokenAndValidAuthentication
        : CryptoComponentJwtCreationSpecification
    {
        [Test()]
        public void ShouldNotHaveHandledException()
        {
            Assert.IsNull(Result.Exception);
        }

        [Test()]
        public void ShouldNotHaveUnhandledException()
        {
            Assert.IsNull(CaughtException);
        }

        [Test()]
        public void ShouldReturnAccessToken()
        {
            Assert.IsNotNull(Result.Result);
        }
    }
}
