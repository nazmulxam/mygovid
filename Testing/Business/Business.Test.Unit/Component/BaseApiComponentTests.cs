﻿using myGovID.Test;
using NUnit.Framework;
using myGovID.Services.Api.Contract;
using Moq;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Component;
using myGovID.Services.Api.Contract.Model;
using myGovID.Services.Api.Contract.Model.Credential;
using System.Collections.Generic;
using myGovID.Presentation.Mapper;
using myGovID.Business.Mapper;
using myGovID.Services.Api.Contract.Model.Task;
using myGovID.Business.Session;
using myGovID.Services.Timer.Contract;
using myGovID.Services.Event.Contract;
using System.Threading.Tasks;
using myGovID.Business.Contract.Component;
using myGovID.Services.Log.Contract;

namespace myGovID.Business.Test.Unit.Component.BaseApiComponentTests
{
    [TestFixture()]
    public class BaseApiComponentSpecification : AsyncTestSpecification
    {
        protected Mock<IApiService> MockApiService;
        protected Mock<IApplication> MockApplication;
        protected BaseApiComponent BaseApiComponent;
        protected CredentialRequestModel CredentialRequestModel;
        protected Mock<IDeviceActivityTimer> MockDeviceActivityTimer;
        protected Mock<IEventService> MockEventService;
        protected Mock<IAuthenticationComponent> MockAuthenticationCredential;
        protected Mock<ILogger> MockLogger;
        protected Contract.Model.CredentialResponseModel BusinessResponse;

        protected override void Given()
        {
            AutoMapper.Mapper.Reset();
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<BusinessMapperProfile>();
                cfg.AddProfile<ServiceMapperProfile>();
            });
            AutoMapper.Mapper.AssertConfigurationIsValid();
            MockApiService = new Mock<IApiService>();
            MockApplication = new Mock<IApplication>();
            MockDeviceActivityTimer = new Mock<IDeviceActivityTimer>();
            MockEventService = new Mock<IEventService>();
            MockLogger = new Mock<ILogger>();

            BaseApiComponent = new BaseApiComponent(MockLogger.Object, MockApiService.Object, MockApplication.Object);
            MockApplication.Setup(A => A.CurrentSession).Returns(new myGovID.Business.Session.Session(MockEventService.Object, MockLogger.Object));

            MockApiService.SetupSequence(s => s.SendAsync<IApiRequestModel, TaskResponseModel>(It.IsAny<IApiRequest<IApiRequestModel>>()))
                          .Returns(GetFirstTaskResponse())
                          .Returns(GetFirstTaskResponse())
                          .Returns(GetFirstTaskResponse());

            MockApiService.SetupSequence(s => s.SendAsync<IApiRequestModel, string>(It.IsAny<IApiRequest<IApiRequestModel>>()))
                          .Returns(GetStringTaskResponse())
                          .Returns(GetStringTaskResponse())
                          .Returns(GetStringCredentialResponse());

        }
        protected override async Task WhenAsync()
        {
            Link link = new Link
            {
                Location = "/api/v1/credentials/x509s",
                Method = "post",
                AuthType = AuthenticationType.Assurance,
                TargetType = TargetType.Next
            };

            BusinessResponse = await BaseApiComponent.SendPoiAsync<IApiRequestModel, Contract.Model.CredentialResponseModel, CredentialResponseModel>
                                                                                    (link, CredentialRequestModel, null, "xyz");
        }

        private Task<ApiResult<TaskResponseModel>> GetFirstTaskResponse()
        {
            Link link = new Link
            {
                AuthType = AuthenticationType.Assurance,
                Location = "/api/v1/credentials/tasks/1",
                Method = "GET",
                TargetType = TargetType.Self
            };

            List<Link> links = new List<Link> { link };
            TaskResponseModel taskResponseModel = new TaskResponseModel
            {
                Links = links,
                Eta = "1",
                Id = "1",
                Status = Services.Api.Contract.Model.Task.TaskStatus.Submitted
            };
            return Task.Factory.StartNew(() =>
            {
                return new ApiResult<TaskResponseModel>(ResourceStatus.Success, "/api/v1/credentials/tasks/1", taskResponseModel);
            });
        }

        private Task<ApiResult<string>> GetStringTaskResponse()
        {
            ApiResult<string> apiResult = new ApiResult<string>(ResourceStatus.Success, "/api/v1/credentials/tasks/1",
                                                                @"{
                'id': 1,
                'status': 'Submitted',
                'eta':1,
                'links': [
                          {
                          'href': '/api/v1/credentials/tasks/1',
                          'rel': 'Self',
                          'method': 'GET',
                          'authentication': 'assurance'
                          }
                          ]
            }
            ");
            return Task.Factory.StartNew(() =>
            {
                return apiResult;
            });
        }
        private Task<ApiResult<string>> GetStringCredentialResponse()
        {
            ApiResult<string> apiResult = new ApiResult<string>(ResourceStatus.Success, null,
            @"{
                'credentialToken':'credentialToken',
                'p7': 'p7'
            }");
            return Task.Factory.StartNew(() =>
            {
                return apiResult;
            });
        }

        [Test()]
        public void ShouldReturnNotNullResponse()
        {
            Assert.IsNotNull(BusinessResponse);
        }

        [Test()]
        public void ShouldHaveCredentialInResponse()
        {
            Assert.IsNotNull(BusinessResponse.CredentialToken);
            Assert.AreEqual("credentialToken", BusinessResponse.CredentialToken);
        }

        [Test()]
        public void ShouldHaveP7InResponse()
        {
            Assert.IsNotNull(BusinessResponse.P7);
            Assert.AreEqual("p7", BusinessResponse.P7);
        }
    }
}