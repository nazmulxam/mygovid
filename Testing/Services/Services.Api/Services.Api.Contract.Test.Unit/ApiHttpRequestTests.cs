﻿using System;
using System.Collections.Generic;
using Moq;
using myGovID.Services.Api.Contract.Model;
using myGovID.Test;
using NUnit.Framework;

namespace myGovID.Services.Api.Contract.Test.Unit.HttpRequest
{
    [TestFixture()]
    public abstract class ApiHttpRequestSpecification : TestSpecification
    {
        protected Link link;
        protected IApiRequestModel requestModel;
        protected string sessionId;
        protected string authroisation = null;
        protected Dictionary<string, string> headers = null;
        protected ApiHttpRequest<IApiRequestModel> apiHttpRequest;

        protected override void Given()
        {
            link = (new Mock<Link>()).Object;
            requestModel = (new Mock<IApiRequestModel>()).Object;
            sessionId = Guid.NewGuid().ToString();
            authroisation = "authToken";
            headers = new Dictionary<string, string>();
        }

        protected override void When()
        { }
    }
    public class WhenLinkIsNullSpecification : ApiHttpRequestSpecification
    {
        private Exception _exception;
        protected override void Given()
        {
            base.Given();
            link = null;
        }
        protected override void When()
        {
            try
            {
                var request = new ApiHttpRequest<IApiRequestModel>(link, requestModel, sessionId, authroisation, headers);
            }
            catch (Exception e)
            {
                _exception = e;
            }
        }
        [Test()]
        public void ShouldThrowArgumentNullException()
        {
            Assert.NotNull(_exception);
        }
    }
    public class WhenSessionIsNullSpecification : ApiHttpRequestSpecification
    {
        private Exception _exception;
        protected override void Given()
        {
            base.Given();
            sessionId = null;
        }
        protected override void When()
        {
            try
            {
                var request = new ApiHttpRequest<IApiRequestModel>(link, requestModel, sessionId, authroisation, headers);
            }
            catch (Exception e)
            {
                _exception = e;
            }
        }
        [Test()]
        public void ShouldThrowArgumentNullException()
        {
            Assert.NotNull(_exception);
        }
    }
    public class WhenAuthTokenIsNullAndAuthTypeIsNotNoneSpecification : ApiHttpRequestSpecification
    {
        private Exception _exception;
        protected override void Given()
        {
            base.Given();
            link.AuthType = AuthenticationType.Extension;
            authroisation = null;
        }
        protected override void When()
        {
            try
            {
                apiHttpRequest = new ApiHttpRequest<IApiRequestModel>(link, requestModel, sessionId, authroisation, headers);
            }
            catch (Exception e)
            {
                _exception = e;
            }
        }
        [Test()]
        public void ShouldThrowArgumentException()
        {
            Assert.NotNull(_exception);
            Assert.True(_exception is ArgumentException);
        }
    }
    public class WhenAuthTokenIsEmptyAndAuthTypeIsNotNoneSpecification : ApiHttpRequestSpecification
    {
        private Exception _exception;
        protected override void Given()
        {
            base.Given();
            link.AuthType = AuthenticationType.Extension;
            authroisation = "";
        }
        protected override void When()
        {
            try
            {
                apiHttpRequest = new ApiHttpRequest<IApiRequestModel>(link, requestModel, sessionId, authroisation, headers);
            }
            catch (Exception e)
            {
                _exception = e;
            }
        }
        [Test()]
        public void ShouldThrowArgumentException()
        {
            Assert.NotNull(_exception);
            Assert.True(_exception is ArgumentException);
        }
    }
    public class WhenAuthTokenIsWhiteSpaceAndAuthTypeIsNotNoneSpecification : ApiHttpRequestSpecification
    {
        private Exception _exception;
        protected override void Given()
        {
            base.Given();
            link.AuthType = AuthenticationType.Extension;
            authroisation = "         ";
        }
        protected override void When()
        {
            try
            {
                apiHttpRequest = new ApiHttpRequest<IApiRequestModel>(link, requestModel, sessionId, authroisation, headers);
            }
            catch (Exception e)
            {
                _exception = e;
            }
        }
        [Test()]
        public void ShouldThrowArgumentException()
        {
            Assert.NotNull(_exception);
            Assert.True(_exception is ArgumentException);
        }
    }
    public class WhenAuthTokenIsNullAndAuthTypeIsNoneSpecification : ApiHttpRequestSpecification
    {
        protected override void Given()
        {
            base.Given();
            link.AuthType = AuthenticationType.None;
            authroisation = null;
        }
        protected override void When()
        {
            apiHttpRequest = new ApiHttpRequest<IApiRequestModel>(link, requestModel, sessionId, authroisation, headers);
        }
        [Test()]
        public void ShouldCreateRequestObject()
        {
            Assert.NotNull(apiHttpRequest);
        }
    }
    public class WhenValidRequestIsProvidedSpecification : ApiHttpRequestSpecification
    {
        protected override void Given()
        {
            base.Given();
            link.AuthType = AuthenticationType.None;
            authroisation = null;
        }
        protected override void When()
        {
            apiHttpRequest = new ApiHttpRequest<IApiRequestModel>(link, requestModel, sessionId, authroisation, headers);
        }
        [Test()]
        public void ShouldCreateRequestObject()
        {
            Assert.NotNull(apiHttpRequest);
        }
        [Test()]
        public void ShouldHaveNonNullRequestObject()
        {
            Assert.NotNull(apiHttpRequest.Request);
        }
        [Test()]
        public void ShouldPopulatePayloadObject()
        {
            Assert.NotNull(apiHttpRequest.Payload);
            Assert.NotNull(apiHttpRequest.Payload.Length != 0);
        }
    }
    public class WhenNoHeadersAreProvidedSpecification : ApiHttpRequestSpecification
    {
        protected override void Given()
        {
            base.Given();
        }
        protected override void When()
        {
            link = new Link()
            {
                AuthType = AuthenticationType.None,
                Method = "Get",
                Location = "http://google.com",
                TargetType = TargetType.Next
            };
            apiHttpRequest = new ApiHttpRequest<IApiRequestModel>(link, requestModel, sessionId, authroisation, headers);
        }
        [Test()]
        public void ShouldCreateRequestObject()
        {
            Assert.NotNull(apiHttpRequest);
        }
        [Test()]
        public void ShouldCreateHeadersDictionary()
        {
            Assert.NotNull(apiHttpRequest.Headers);
        }
        [Test()]
        public void ShouldAddContentTypeHeader()
        {
            Assert.True(apiHttpRequest.Headers.TryGetValue("Content-Type", out string val));
        }
        [Test()]
        public void ShouldAddAcceptHeader()
        {
            Assert.True(apiHttpRequest.Headers.TryGetValue("Accept", out string val));
        }
        [Test()]
        public void ShouldAddAuthorizationHeader()
        {
            Assert.True(apiHttpRequest.Headers.TryGetValue("Authorization", out string val));
        }
        [Test()]
        public void ShouldPopulatePayloadObject()
        {
            Assert.NotNull(apiHttpRequest.Payload);
            Assert.NotNull(apiHttpRequest.Payload.Length != 0);
        }
    }
    public class WhenHeadersAreProvidedSpecification : ApiHttpRequestSpecification
    {
        protected override void Given()
        {
            base.Given();
            headers.Add("Cookie", "PHPSESSID=r2t5uvjq435r4q7ib3vtdjq120");
        }
        protected override void When()
        {
            link = new Link()
            { 
                AuthType = AuthenticationType.None,
                Method = "Get",
                Location = "http://google.com",
                TargetType = TargetType.Next
            };
            apiHttpRequest = new ApiHttpRequest<IApiRequestModel>(link, requestModel, sessionId, authroisation, headers);
        }
        [Test()]
        public void ShouldCreateRequestObject()
        {
            Assert.NotNull(apiHttpRequest);
        }
        [Test()]
        public void ShouldCreateHeadersDictionary()
        {
            Assert.NotNull(apiHttpRequest.Headers);
        }
        [Test()]
        public void ShouldHaveSpecifiedHeader()
        {
            Assert.True(apiHttpRequest.Headers.TryGetValue("Cookie", out string val));
        }
        [Test()]
        public void ShouldAddContentTypeHeader()
        {
            Assert.True(apiHttpRequest.Headers.TryGetValue("Content-Type", out string val));
        }
        [Test()]
        public void ShouldAddAcceptHeader()
        {
            Assert.True(apiHttpRequest.Headers.TryGetValue("Accept", out string val));
        }
        [Test()]
        public void ShouldAddAuthorizationHeader()
        {
            Assert.True(apiHttpRequest.Headers.TryGetValue("Authorization", out string val));
        }
        [Test()]
        public void ShouldPopulatePayloadObject()
        {
            Assert.NotNull(apiHttpRequest.Payload);
            Assert.NotNull(apiHttpRequest.Payload.Length != 0);
        }
    }
}
