﻿using myGovID.Services.Api.Contract.Model.Poi;
using myGovID.Test;
using Newtonsoft.Json;
using NUnit.Framework;

namespace myGovID.Services.Api.Contract.Test.Unit.POIResponse
{
    [TestFixture()]
    public abstract class POIResponseModelSpecification : TestSpecification
    {
        protected string Json;
        protected PoiResponseModel Model;

        protected override void Given()
        {
            Json = @"{}";
        }

        protected override void When()
        {
            Model = JsonConvert.DeserializeObject<PoiResponseModel>(Json);
        }
    }
    public class POIResponseModelMappedParametersSpecification : POIResponseModelSpecification
    {
        protected override void Given()
        {
            Json = @"{
                        'processId': '52942edb-3cd6-4ab4-8ee7-b23cd64c4430',
                        'status': 'Submitted'
                    }";
        }
        [Test()]
        public void ShouldHaveNonNullModel()
        {
            Assert.IsNotNull(Model);
        }
        [Test()]
        public void ShouldHaveCorrectStatus()
        {
            Assert.AreEqual("Submitted", Model.Status);
        }
        [Test()]
        public void ShouldHaveCorrectProcessId()
        {
            Assert.AreEqual("52942edb-3cd6-4ab4-8ee7-b23cd64c4430", Model.ProcessId);
        }
    }
    public class POIResponseModelMissingProcessIdSpecification : POIResponseModelSpecification
    {
        protected override void Given()
        {
            Json = @"{
                        'status': 'Submitted'
                    }";
        }
        [Test()]
        public void ShouldHaveNonNullModel()
        {
            Assert.IsNotNull(Model);
        }
        [Test()]
        public void ShouldHaveCorrectStatus()
        {
            Assert.AreEqual("Submitted", Model.Status);
        }
        [Test()]
        public void ShouldHaveNullProcessId()
        {
            Assert.IsNull(Model.ProcessId);
        }
    }
    public class POIResponseModelMissingStatusSpecification : POIResponseModelSpecification
    {
        protected override void Given()
        {
            Json = @"{
                        'processId': '52942edb-3cd6-4ab4-8ee7-b23cd64c4430',
                    }";
        }
        [Test()]
        public void ShouldHaveNonNullModel()
        {
            Assert.IsNotNull(Model);
        }
        [Test()]
        public void ShouldHaveNullStatus()
        {
            Assert.IsNull(Model.Status);
        }
        [Test()]
        public void ShouldHaveCorrectProcessId()
        {
            Assert.AreEqual("52942edb-3cd6-4ab4-8ee7-b23cd64c4430", Model.ProcessId);
        }
    }
}
