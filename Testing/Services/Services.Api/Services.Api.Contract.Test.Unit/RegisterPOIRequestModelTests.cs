﻿using myGovID.Services.Api.Contract.Model.Poi.Register;
using myGovID.Test;
using Newtonsoft.Json;
using NUnit.Framework;
namespace myGovID.Services.Api.Contract.Test.Unit.RegisterPOIRequest
{
    [TestFixture()]
    public abstract class RegisterPOIRequestModelSpecification : TestSpecification
    {
        protected string Json;
        protected RegisterPOIRequestModel Model;

        protected override void When()
        {
            Model = JsonConvert.DeserializeObject<RegisterPOIRequestModel>(Json);
        }
    }
    public class RegisterPOIRequestModelMappedParameterSpecification : RegisterPOIRequestModelSpecification
    {
        protected override void Given()
        {
            Json = "{ \"acceptedVersion\": 1.0 }";
        }
        [Test()]
        public void ShouldHaveNonNullModel()
        {
            Assert.IsNotNull(Model);
        }
        [Test()]
        public void ShouldPopulateAcceptedVersion()
        {
            Assert.AreEqual("1.0", Model.AcceptedVersion);
        }
    }
    public class RegisterPOIRequestModelUnmappedParameterSpecification : RegisterPOIRequestModelSpecification
    {
        protected override void Given()
        {
            Json = "{ \"version\": 1.0 }";
        }
        [Test()]
        public void ShouldHaveNonNullModel()
        {
            Assert.IsNotNull(Model);
        }
        [Test()]
        public void ShouldNotPopulateAcceptedVersion()
        {
            Assert.IsNull(Model.AcceptedVersion);
        }
    }
    public class RegisterPOIRequestModelEmptySpecification : RegisterPOIRequestModelSpecification
    {
        protected override void Given()
        {
            Json = "";
        }
        [Test()]
        public void ShouldHaveNullModel()
        {
            Assert.IsNull(Model);
        }
    }
    public class RegisterPOIRequestModelEmptyJsonSpecification : RegisterPOIRequestModelSpecification
    {
        protected override void Given()
        {
            Json = "{}";
        }
        [Test()]
        public void ShouldHaveNonNullModel()
        {
            Assert.IsNotNull(Model);
        }
        [Test()]
        public void ShouldNotPopulateAcceptedVersion()
        {
            Assert.IsNull(Model.AcceptedVersion);
        }
    }
    public class RegisterPOIRequestModelSerializeSpecification : RegisterPOIRequestModelSpecification
    {
        protected override void Given()
        {
            Model = new RegisterPOIRequestModel
            {
                AcceptedVersion = "1.0"
            };
        }
        protected override void When()
        {
            Json = JsonConvert.SerializeObject(Model);
        }
        [Test()]
        public void ShouldHaveNonNullJson()
        {
            Assert.IsNotNull(Json);
        }
        [Test()]
        public void ShouldGenerateVersion()
        {
            Assert.AreEqual("{\"acceptedVersion\":\"1.0\"}", Json);
        }
    }
    public class RegisterPOIRequestModelNullVersionSerializeSpecification : RegisterPOIRequestModelSpecification
    {
        protected override void Given()
        {
            Model = new RegisterPOIRequestModel();
        }
        protected override void When()
        {
            Json = JsonConvert.SerializeObject(Model);
        }
        [Test()]
        public void ShouldHaveNonNullJson()
        {
            Assert.IsNotNull(Json);
        }
        [Test()]
        public void ShouldGenerateNullVersion()
        {
            Assert.AreEqual("{\"acceptedVersion\":null}", Json);
        }
    }
}
