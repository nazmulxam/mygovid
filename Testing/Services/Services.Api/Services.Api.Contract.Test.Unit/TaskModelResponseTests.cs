﻿using System;
using myGovID.Test;
using myGovID.Services.Api.Contract.Model.Task;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Linq;

namespace myGovID.Api.Contract.Test.Unit
{
    [TestFixture()]
    public class TaskModelResponseTests : TestSpecification
    {
        protected string Json;
        protected TaskResponseModel Model;

        protected override void Given()
        {
            Json = @"{                 'id': 1,                 'status': 'Some',                 'eta':1,                 'links': [                           {                           'href': '/api/v1/credentials/tasks/1',                           'rel': 'Self',                           'method': 'GET',                           'authentication': 'Assurance'                           }                           ]             }";
        }

        protected override void When()
        {
            Model = JsonConvert.DeserializeObject<TaskResponseModel>(Json);
        }

        [Test()]
        public void ShouldHaveCorrectLinkCount()
        {
            Assert.AreEqual(1, Model.Links.Count());
        }
    }
}
