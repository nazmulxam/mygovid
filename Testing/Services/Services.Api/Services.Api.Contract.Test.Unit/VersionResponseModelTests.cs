﻿using System;
using myGovID.Test;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Linq;
using myGovID.Services.Api.Contract.Model.System.Version;

namespace myGovID.Services.Api.Contract.Test.Unit.VersionResponse
{
    [TestFixture()]
    public abstract class VersionResponseModelSpecification : TestSpecification
    {
        protected string Json;
        protected VersionResponseModel Model;

        protected override void Given()
        {
            Json = null;
        }

        protected override void When()
        {
            Model = JsonConvert.DeserializeObject<VersionResponseModel>(Json);
        }
    }



    public class VersionResponseModelValidJsonSpecification : VersionResponseModelSpecification
    {
        protected override void Given()
        {
            Json = @"{
                        'capabilities':[
                                        'DVS_Driver_Licence',
                                        'DVS_Medicare_Card'
                                        ],
                        'alerts':[
                                  {
                                  'startDateTime':'2018-04-23T18:25:43.5112222',
                                  'endDateTime':'2018-08-23T18:25:43.5112222',
                                  'message':'POI not available',
                                  'isSystemOutage':false
                                  }
                                  ]
                    }";
        }
        [Test()]
        public void ShouldHaveCorrectCapabilitiesCount()
        {
            Assert.AreEqual(2, Model.Capabilities.Count());
        }
        [Test()]
        public void ShouldHaveCorrectAlertsCount()
        {
            Assert.AreEqual(1, Model.Alerts.Count());
        }
        [Test()]
        public void ShouldHaveCorrectCapabilities()
        {
            Assert.IsTrue(AppCapability.DVS_Driver_Licence.Equals(Model.Capabilities.First()));
            Assert.IsTrue(AppCapability.DVS_Medicare_Card.Equals(Model.Capabilities.ElementAt(1)));
        }
        [Test()]
        public void ShouldHaveCorrectAlertStartDate()
        {
            AppAlert appAlert = Model.Alerts.First();
            Assert.AreEqual(DateTime.Parse("2018-04-23T18:25:43.5112222"), appAlert.StartDateTime);
        }
        [Test()]
        public void ShouldHaveCorrectAlertEndDate()
        {
            AppAlert appAlert = Model.Alerts.First();
            Assert.AreEqual(DateTime.Parse("2018-08-23T18:25:43.5112222"), appAlert.EndDateTime);
        }
        [Test()]
        public void ShouldHaveCorrectAlertMessage()
        {
            AppAlert appAlert = Model.Alerts.First();
            Assert.AreEqual("POI not available", appAlert.Message);
        }
        [Test()]
        public void ShouldHaveCorrectAlertSystemOutageFlag()
        {
            AppAlert appAlert = Model.Alerts.First();
            Assert.AreEqual(false, appAlert.SystemOutage);
        }
    }
    public class VersionResponseModelNoCapabilitiesSpecification : VersionResponseModelSpecification
    {
        protected override void Given()
        {
            Json = @"{
                        'alerts':[
                                  {
                                  'startDateTime':'2018-04-23T18:25:43.5112222',
                                  'endDateTime':'2018-08-23T18:25:43.5112222',
                                  'message':'POI not available',
                                  'isSystemOutage':false
                                  }
                                  ]
                    }";
        }
        [Test()]
        public void ShouldHaveNullCapabilities()
        {
            Assert.IsNull(Model.Capabilities);
        }
    }
    public class VersionResponseModelNoAlertsSpecification : VersionResponseModelSpecification
    {
        protected override void Given()
        {
            Json = @"{
                        
                    }";
        }
        [Test()]
        public void ShouldHaveNullAlerts()
        {
            Assert.IsNull(Model.Alerts);
        }
    }
    public class VersionResponseModelUnsupportedCapabilitiesSpecification : VersionResponseModelSpecification
    {
        Exception Exception;
        protected override void Given()
        {
            Json = @"{
                        'capabilities':[ 'DVS_Driver_Licence123' ],
                    }";
        }
        protected override void When()
        {
            try
            {
                Model = JsonConvert.DeserializeObject<VersionResponseModel>(Json);
            }
            catch (Exception e)
            {
                Exception = e;
            }
        }
        [Test()]
        public void ShouldThrowJsonSerializationException()
        {
            Assert.IsNotNull(Exception);
            Assert.IsInstanceOf(typeof(JsonSerializationException), Exception);
        }
        [Test()]
        public void ShouldHaveNullModel()
        {
            Assert.IsNull(Model);
        }
    }

    public class VersionResponseModelInvalidDateFormatSpecification : VersionResponseModelSpecification
    {
        Exception Exception;
        protected override void Given()
        {
            Json = @"{
                        'alerts':[
                                  {
                                  'startDateTime':'ABC',
                                  'endDateTime':'2018-08-23T18:25:43.5112222',
                                  'message':'POI not available',
                                  'isSystemOutage':false
                                  }
                                  ]
                    }";
        }
        protected override void When()
        {
            try
            {
                Model = JsonConvert.DeserializeObject<VersionResponseModel>(Json);
                Model.Validate();
            }
            catch (Exception e)
            {
                Exception = e;
            }
        }
        [Test()]
        public void ShouldThrowJsonSerializationException()
        {
            Assert.IsNotNull(Exception);
        }
    }
    public class VersionResponseModelValidDateSpecification : VersionResponseModelSpecification
    {
        protected override void Given()
        {
            Json = @"{
                        'alerts':[
                                  {
                                  'startDateTime':'01-01-2013',
                                  'endDateTime':'2018-08-23T18:25:43.5112222',
                                  'message':'POI not available',
                                  'isSystemOutage':false
                                  }
                                  ]
                    }";
        }
        [Test()]
        public void ShouldHaveNonNullModel()
        {
            Assert.IsNotNull(Model);
        }
        [Test()]
        public void ShouldHaveValidStartDate()
        {
            AppAlert appAlert = Model.Alerts.First();
            Assert.IsNotNull(appAlert.StartDateTime);
        }
        [Test()]
        public void ShouldHaveValidEndDate()
        {
            AppAlert appAlert = Model.Alerts.First();
            Assert.IsNotNull(appAlert.EndDateTime);
            Assert.AreEqual(DateTime.Parse("23-08-2018"), appAlert.EndDateTime.Date);
        }
        [Test()]
        public void ShouldHaveValidEndDateWithTimeInfo()
        {
            AppAlert appAlert = Model.Alerts.First();
            Assert.AreEqual(18, appAlert.EndDateTime.Hour);
            Assert.AreEqual(25, appAlert.EndDateTime.Minute);
            Assert.AreEqual(43, appAlert.EndDateTime.Second);
        }
    }
    public class VersionResponseModelMessagesOnlySpecification : VersionResponseModelSpecification
    {
        protected override void Given()
        {
            Json = @"{'messages':[{'code':'SS31001 (400)','type':'Error','title':'System unavailable','description':'We are unable to process your request at this time. Try again and if the issue continues <a href=\'https://www.mygovid.gov.au/help\' target=\'_blank\' >contact us.</a>','parameter':null}]}";
        }
        [Test()]
        public void ShouldHaveNotNullMessage()
        {
            Assert.IsNotNull(Model.Messages);
        }
    }
}
