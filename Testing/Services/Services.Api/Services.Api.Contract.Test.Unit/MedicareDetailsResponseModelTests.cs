﻿using myGovID.Services.Api.Contract.Model.Poi.MedicareDetails;
using myGovID.Test;
using Newtonsoft.Json;
using NUnit.Framework;
namespace myGovID.Services.Api.Contract.Test.Unit.MedicareDetailsResponse
{
    [TestFixture()]
    public abstract class MedicareDetailsResponseModelSpecification : TestSpecification
    {
        protected string Json;
        protected MedicareDetailsResponseModel Model;
    }

    public class MedicareDetailsRequestModelDeserializeSpecification : MedicareDetailsResponseModelSpecification
    {
        protected override void Given()
        {
            Json = @"
            {
                ""id"": 5,
                ""status"": ""Submitted"",
                ""eta"": 1,
                ""remainingRetryCount"": 5,
                ""processId"": ""52942edb-3cd6-4ab4-8ee7-b23cd64c4430"",
                ""links"": [
                      {
                          ""href"": ""/api/v1/poi/52942edb-3cd6-4ab4-8ee7-b23cd64c4430/tasks/5"",
                          ""rel"": ""self"",
                          ""method"": ""get"",
                          ""authentication"": ""extension""
                      },
                      {
                          ""href"": ""/api/v1/poi/52942edb-3cd6-4ab4-8ee7-b23cd64c4430/tasks/5"",
                          ""rel"": ""cancel"",
                          ""method"": ""delete"",
                          ""authentication"": ""extension""
                     },
                     {
                          ""href"": ""/api/v1/poi/52942edb-3cd6-4ab4-8ee7-b23cd64c4430/tasks?taskId=5"",
                          ""rel"": ""clone"",
                          ""method"": ""post"",
                          ""authentication"": ""extension""
                     }
                 ]
            }";
        }

        protected override void When()
        {
            Model = JsonConvert.DeserializeObject<MedicareDetailsResponseModel>(Json);
        }

        [Test()]
        public void ShouldDeserializeCorrectly()
        {
            Assert.Multiple(() =>
            {
                Assert.That(Model.Links, Has.Count.EqualTo(3), "Links");
                Assert.That(Model.Status, Is.EqualTo("Submitted"), "Status");
                Assert.That(Model.ProcessId, Is.EqualTo("52942edb-3cd6-4ab4-8ee7-b23cd64c4430"), "ProcessId");
            });
        }
    }
}

