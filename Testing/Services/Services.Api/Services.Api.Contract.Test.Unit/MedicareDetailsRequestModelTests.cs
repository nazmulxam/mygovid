﻿using myGovID.Services.Api.Contract.Model.Poi.MedicareDetails;
using myGovID.Test;
using Newtonsoft.Json;
using NUnit.Framework;
namespace myGovID.Services.Api.Contract.Test.Unit.MedicareDetailsRequest
{
    [TestFixture()]
    public abstract class MedicareDetailsRequestModelSpecification : TestSpecification
    {
        protected string Json;
        protected MedicareDetailsRequestModel Model;
    }
    public class MedicareDetailsRequestModelSerializeSpecification : MedicareDetailsRequestModelSpecification
    {
        protected override void Given()
        {
            Model = new MedicareDetailsRequestModel
            {
                FullName1 = "Name 1",
                FullName2 = "Name 2",
                FullName3 = "Name 3",
                FullName4 = "Name 4",
                MedicareCardNumber = "1234567890",
                IndividualReferencenumber = 1,
                CardExpiry = "01/01/2020",
                CardType = "Green",
                ConsentProvided = true
            };
        }

        protected override void When()
        {
            Json = JsonConvert.SerializeObject(Model);
        }

        [Test()]
        public void ShouldSerilizeCorrectly()
        {
            var expected = "{" + string.Join(",",
                "\"fullName1\":\"Name 1\"",
                "\"fullName2\":\"Name 2\"",
                "\"fullName3\":\"Name 3\"",
                "\"fullName4\":\"Name 4\"",
                "\"medicareCardNumber\":\"1234567890\"",
                "\"individualReferencenumber\":1",
                "\"cardExpiry\":\"01/01/2020\"",
                "\"cardType\":\"Green\"",
                "\"consentProvided\":true"
            ) + "}";

            Assert.That(Json, Is.EqualTo(expected));
        }
    }

    public class MedicareDetailsRequestModelDeserializeSpecification : MedicareDetailsRequestModelSpecification
    {
        protected override void Given()
        {
            Json = "{" + string.Join(",",
                "\"fullName1\":\"Name 1\"",
                "\"fullName2\":\"Name 2\"",
                "\"fullName3\":\"Name 3\"",
                "\"fullName4\":\"Name 4\"",
                "\"medicareCardNumber\":\"1234567890\"",
                "\"individualReferencenumber\":1",
                "\"cardExpiry\":\"01/01/2020\"",
                "\"cardType\":\"Green\"",
                "\"consentProvided\":true"
            ) + "}";
        }

        protected override void When()
        {
            Model = JsonConvert.DeserializeObject<MedicareDetailsRequestModel>(Json);
        }

        [Test()]
        public void ShouldDeserializeCorrectly()
        {
            Assert.Multiple(() =>
            {
                Assert.That(Model.FullName1, Is.EqualTo("Name 1"), "FullName1");
                Assert.That(Model.FullName2, Is.EqualTo("Name 2"), "FullName2");
                Assert.That(Model.FullName3, Is.EqualTo("Name 3"), "FullName3");
                Assert.That(Model.FullName4, Is.EqualTo("Name 4"), "FullName4");
                Assert.That(Model.MedicareCardNumber, Is.EqualTo("1234567890"), "MedicareCardNumber");
                Assert.That(Model.IndividualReferencenumber, Is.EqualTo(1), "IndividualReferencenumber");
                Assert.That(Model.CardExpiry, Is.EqualTo("01/01/2020"), "CardExpiry");
                Assert.That(Model.CardType, Is.EqualTo("Green"), "CardType");
                Assert.That(Model.ConsentProvided, Is.EqualTo(true), "ConsentProvided");
            });
        }
    }
}

