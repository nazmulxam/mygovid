﻿using myGovID.Test;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Linq;
using myGovID.Services.Api.Contract.Model.Poi.Register;
using myGovID.Services.Api.Contract.Model;

namespace myGovID.Services.Api.Contract.Test.Unit.RegisterPOIResponse
{
    [TestFixture()]
    public abstract class RegisterPOIResponseModelSpecification : TestSpecification
    {
        protected string Json;
        protected RegisterPOIResponseModel Model;

        protected override void Given()
        {
            Json = @"{}";
        }

        protected override void When()
        {
            Model = JsonConvert.DeserializeObject<RegisterPOIResponseModel>(Json);
        }
    }
    public class RegisterPOIResponseModelMappedParameterSpecification : RegisterPOIResponseModelSpecification
    {
        protected override void Given()
        {
            Json = @"{
                        'acceptedTermsAndConditionsVersion': '1.0.0.0',
                        'processId': '52942edb-3cd6-4ab4-8ee7-b23cd64c4430',
                        'links': [
                                  {
                                      'href': '/api/v1/poi/52942edb-3cd6-4ab4-8ee7-b23cd64c4430',
                                      'rel': 'self',
                                      'method': 'get',
                                      'authentication': 'extension'
                                  }
                         ],
                         'messages': [
                                  {
                                     'code': 'POI31091 (400)',
                                     'type': 'Error',
                                     'title': 'An error has occurred',
                                     'description': '<p>An unexpected error has occurred while processing your request. <a href=‘https://www.ato.gov.au/ausidaccess’ target=‘_blank’ >Contact us</a> if you require any assistance.</p><p><b>Error code POI31091 (400)</b></p>',
                                     'parameter': 'Test'
                                  }
                          ]
                    }";
        }
        [Test()]
        public void ShouldHaveNonNullModel()
        {
            Assert.IsNotNull(Model);
        }
        [Test()]
        public void ShouldHaveCorrectAcceptedTermsAndConditionsVersion()
        {
            Assert.AreEqual("1.0.0.0", Model.AcceptedTermsAndConditionsVersion);
        }
        [Test()]
        public void ShouldHaveCorrectProcessId()
        {
            Assert.AreEqual("52942edb-3cd6-4ab4-8ee7-b23cd64c4430", Model.ProcessId);
        }
        //Tests for checking links mapping
        [Test()]
        public void ShouldHaveCorrectLinkCount()
        {
            Assert.AreEqual(1, Model.Links.Count());
        }
        [Test()]
        public void ShouldHaveCorrectLinkHref()
        {
            Link link = Model.Links.First();
            Assert.AreEqual("/api/v1/poi/52942edb-3cd6-4ab4-8ee7-b23cd64c4430", link.Location);
        }
        [Test()]
        public void ShouldHaveCorrectLinkTargetType()
        {
            Link link = Model.Links.First();
            Assert.True(link.TargetType.Equals(TargetType.Self));
        }
        [Test()]
        public void ShouldHaveCorrectLinkMethod()
        {
            Link link = Model.Links.First();
            Assert.True(link.Method == "get");
        }
        [Test()]
        public void ShouldHaveCorrectLinkAuthenticationType()
        {
            Link link = Model.Links.First();
            Assert.True(link.AuthType == AuthenticationType.Extension);
        }
        //Tests for checking messages mapping
        [Test()]
        public void ShouldHaveCorrectMessageCount()
        {
            Assert.AreEqual(1, Model.Messages.Count());
        }
        [Test()]
        public void ShouldHaveCorrectMessageCode()
        {
            AppMessage message = Model.Messages.First();
            Assert.AreEqual("POI31091 (400)", message.Code);
        }
        [Test()]
        public void ShouldHaveCorrectMessageType()
        {
            AppMessage message = Model.Messages.First();
            Assert.AreEqual("Error", message.Type);
        }
        [Test()]
        public void ShouldHaveCorrectMessageTitle()
        {
            AppMessage message = Model.Messages.First();
            Assert.AreEqual("An error has occurred", message.Title);
        }
        [Test()]
        public void ShouldHaveCorrectMessageDescription()
        {
            AppMessage message = Model.Messages.First();
            Assert.AreEqual("<p>An unexpected error has occurred while processing your request. <a href=‘https://www.ato.gov.au/ausidaccess’ target=‘_blank’ >Contact us</a> if you require any assistance.</p><p><b>Error code POI31091 (400)</b></p>", message.Description);
        }
        [Test()]
        public void ShouldHaveCorrectMessageParameter()
        {
            AppMessage message = Model.Messages.First();
            Assert.AreEqual("Test", message.Parameter);
        }
    }
}