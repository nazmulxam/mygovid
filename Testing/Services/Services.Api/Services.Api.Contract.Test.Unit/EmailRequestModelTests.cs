﻿using myGovID.Services.Api.Contract.Model.Poi.Email;
using myGovID.Test;
using Newtonsoft.Json;
using NUnit.Framework;

namespace myGovID.Services.Api.Contract.Test.Unit.EmailRequest
{
    [TestFixture()]
    public abstract class EmailRequestModelSpecification : TestSpecification
    {
        protected string Json;
        protected EmailRequestModel Model;

        protected override void When()
        {
            Model = JsonConvert.DeserializeObject<EmailRequestModel>(Json);
        }
    }
    public class EmailRequestModelMappedParameterSpecification : EmailRequestModelSpecification
    {
        protected override void Given()
        {
            Json = "{ \"emailAddress\": \"Test@ato.gov.au\" }";
        }
        [Test()]
        public void ShouldHaveNonNullModel()
        {
            Assert.IsNotNull(Model);
        }
        [Test()]
        public void ShouldPopulateEmailAddress()
        {
            Assert.AreEqual("Test@ato.gov.au", Model.EmailAddress);
        }
    }
    public class EmailRequestModelUnmappedParameterSpecification : EmailRequestModelSpecification
    {
        protected override void Given()
        {
            Json = "{ \"email\": \"Test@ato.gov.au\" }";
        }
        [Test()]
        public void ShouldHaveNonNullModel()
        {
            Assert.IsNotNull(Model);
        }
        [Test()]
        public void ShouldNotPopulateEmailAddress()
        {
            Assert.IsNull(Model.EmailAddress);
        }
    }
    public class EmailRequestModelEmptySpecification : EmailRequestModelSpecification
    {
        protected override void Given()
        {
            Json = "";
        }
        [Test()]
        public void ShouldNotPopulateEmailAddress()
        {
            Assert.IsNull(Model);
        }
    }
    public class EmailRequestModelEmptyJsonSpecification : EmailRequestModelSpecification
    {
        protected override void Given()
        {
            Json = "{}";
        }
        [Test()]
        public void ShouldHaveNonNullModel()
        {
            Assert.IsNotNull(Model);
        }
        [Test()]
        public void ShouldNotPopulateEmailAddress()
        {
            Assert.IsNull(Model.EmailAddress);
        }
    }
    public class EmailRequestModelSerializeSpecification : EmailRequestModelSpecification
    {
        protected override void Given()
        {
            Model = new EmailRequestModel
            {
                EmailAddress = "Test@ato.gov.au"
            };
        }
        protected override void When()
        {
            Json = JsonConvert.SerializeObject(Model);
        }
        [Test()]
        public void ShouldHaveNonNullJson()
        {
            Assert.IsNotNull(Json);
        }
        [Test()]
        public void ShouldGenerateEmailAddress()
        {
            Assert.AreEqual("{\"emailAddress\":\"Test@ato.gov.au\"}", Json);
        }
    }
    public class EmailRequestModelNullEmailSerializeSpecification : EmailRequestModelSpecification
    {
        protected override void Given()
        {
            Model = new EmailRequestModel();
        }
        protected override void When()
        {
            Json = JsonConvert.SerializeObject(Model);
        }
        [Test()]
        public void ShouldHaveNonNullJson()
        {
            Assert.IsNotNull(Json);
        }
        [Test()]
        public void ShouldGenerateNullEmailAddress()
        {
            Assert.AreEqual("{\"emailAddress\":null}", Json);
        }
    }
}
