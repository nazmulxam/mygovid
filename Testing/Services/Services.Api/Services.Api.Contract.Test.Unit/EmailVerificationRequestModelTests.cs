﻿using myGovID.Services.Api.Contract.Model.Poi.EmailVerification;
using myGovID.Test;
using Newtonsoft.Json;
using NUnit.Framework;

namespace myGovID.Services.Api.Contract.Test.Unit.EmailVerificationRequest
{
    [TestFixture()]
    public abstract class EmailVerficiationRequestModelSpecification : TestSpecification
    {
        protected string Json;
        protected EmailVerificationRequestModel Model;

        protected override void When()
        {
            Model = JsonConvert.DeserializeObject<EmailVerificationRequestModel>(Json);
        }
    }
    public class EmailVerificationRequestModelMappedParameterSpecification : EmailVerficiationRequestModelSpecification
    {
        protected override void Given()
        {
            Json = @"{ 
                        ""emailAddress"": ""Test@ato.gov.au"",
                        ""verificationCode"": ""123456""
                    }";
        }
        [Test()]
        public void ShouldHaveNonNullModel()
        {
            Assert.IsNotNull(Model);
        }
        [Test()]
        public void ShouldPopulateEmailAddress()
        {
            Assert.AreEqual("Test@ato.gov.au", Model.EmailAddress);
        }
        [Test()]
        public void ShouldPopulateVerificationCode()
        {
            Assert.AreEqual("123456", Model.VerificationCode);
        }
    }
    public class EmailVerificationRequestModelUnmappedParameterSpecification : EmailVerficiationRequestModelSpecification
    {
        protected override void Given()
        {
            Json = "{ \"email\": \"Test@ato.gov.au\" }";
        }
        [Test()]
        public void ShouldHaveNonNullModel()
        {
            Assert.IsNotNull(Model);
        }
        [Test()]
        public void ShouldNotPopulateEmailAddress()
        {
            Assert.IsNull(Model.EmailAddress);
        }
        [Test()]
        public void ShouldNotPopulateVerificationCode()
        {
            Assert.IsNull(Model.VerificationCode);
        }
    }
    public class EmailVerificationRequestModelEmptySpecification : EmailVerficiationRequestModelSpecification
    {
        protected override void Given()
        {
            Json = "";
        }
        [Test()]
        public void ShouldNotPopulateModel()
        {
            Assert.IsNull(Model);
        }
    }
    public class EmailVerificationRequestModelEmptyJsonSpecification : EmailVerficiationRequestModelSpecification
    {
        protected override void Given()
        {
            Json = "{}";
        }
        [Test()]
        public void ShouldHaveNonNullModel()
        {
            Assert.IsNotNull(Model);
        }
        [Test()]
        public void ShouldNotPopulateEmailAddress()
        {
            Assert.IsNull(Model.EmailAddress);
        }
        [Test()]
        public void ShouldNotPopulateVerificationCode()
        {
            Assert.IsNull(Model.VerificationCode);
        }
    }
    public class EmailVerificationRequestModelSerializeSpecification : EmailVerficiationRequestModelSpecification
    {
        protected override void Given()
        {
            Model = new EmailVerificationRequestModel
            {
                EmailAddress = "Test@ato.gov.au",
                VerificationCode = "123456"
            };
        }
        protected override void When()
        {
            Json = JsonConvert.SerializeObject(Model);
        }
        [Test()]
        public void ShouldHaveNonNullJson()
        {
            Assert.IsNotNull(Json);
        }
        [Test()]
        public void ShouldGenerateValidJson()
        {
            Assert.AreEqual("{\"emailAddress\":\"Test@ato.gov.au\",\"verificationCode\":\"123456\"}", Json);
        }
    }
    public class EmailVerificationRequestModelNullEmailSerializeSpecification : EmailVerficiationRequestModelSpecification
    {
        protected override void Given()
        {
            Model = new EmailVerificationRequestModel();
        }
        protected override void When()
        {
            Json = JsonConvert.SerializeObject(Model);
        }
        [Test()]
        public void ShouldHaveNonNullJson()
        {
            Assert.IsNotNull(Json);
        }
        [Test()]
        public void ShouldGenerateNullValuesInJson()
        {
            Assert.AreEqual("{\"emailAddress\":null,\"verificationCode\":null}", Json);
        }
    }
}
