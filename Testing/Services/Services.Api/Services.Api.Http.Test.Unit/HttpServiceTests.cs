﻿using System;
using Moq;
using System.Linq;
using myGovID.Services.Api.Contract;
using myGovID.Test;
using myGovID.Services.Api.Contract.Model;
using System.Collections.Generic;
using NUnit.Framework;
using System.Net.Http;
using myGovID.Services.Api.Contract.Model.Poi.Email;
using myGovID.Services.Api.Contract.Model.Poi;
using System.Threading.Tasks;
using myGovID.Services.Http.Contract;

namespace myGovID.Services.Api.Http.Test.Unit.HttpServiceTests
{
    [TestFixture()]
    public abstract class HttpServiceSpecification : AsyncTestSpecification
    {
        protected Mock<IHttpConfig> Config;
        protected IResponseModel Response;
        protected BaseApiService httpService;
        protected Link Link;
        protected string SessionId;
        protected string Authroisation;
        protected Dictionary<string, string> Headers;

        protected override void Given()
        {
            Config = new Mock<IHttpConfig>();
            Config.Setup(s => s.ApplicationName).Returns("myGovID");
            Config.Setup(s => s.ApplicationVersion).Returns("1.0");
            Config.Setup(s => s.BaseUrl).Returns("http://localhost:31001");
            httpService = new BaseApiService(Config.Object);

            //populated API http request related objects
            Link = new Link
            {
                Method = "post",
                Location = "/api/v1/poi",
                AuthType = AuthenticationType.None,
                TargetType = TargetType.Next
            };
            SessionId = Guid.NewGuid().ToString();
            Authroisation = "authToken";
            Headers = new Dictionary<string, string>();
        }

        protected override Task WhenAsync()
        {
            return null;
        }

    }

    public class WhenResponseContainLocationHeaderSpecification : HttpServiceSpecification
    {
        private HttpResponseMessage _responseMessage;
        private string _resourceLocation;
        protected override void Given()
        {
            base.Given();
            _responseMessage = new HttpResponseMessage();
            _responseMessage.Headers.Add("Location", "/api/v1/poi");
        }

        protected override Task WhenAsync()
        {
            return Task.Factory.StartNew(() =>
            {
                _resourceLocation = httpService.GetResourceLocation(_responseMessage);
            });
        }
        [Test()]
        public void ShouldReturnValidLocation()
        {
            Assert.AreEqual("file:///api/v1/poi", _resourceLocation);
        }
    }

    public class WhenResponseContainMultipleLocationHeadersSpecification : HttpServiceSpecification
    {
        private HttpResponseMessage _responseMessage;
        private string _resourceLocation;
        private Exception _exception;
        protected override void Given()
        {
            base.Given();
            _responseMessage = new HttpResponseMessage();
            try
            {
                _responseMessage.Headers.Add("Location", new List<string>()
                {
                    "/api/v1/poi", "/api/v1/poi/test"
                });
            }
            catch (Exception e) { _exception = e; }
        }
        protected override Task WhenAsync()
        {
            return Task.Factory.StartNew(() =>
            {
                _resourceLocation = httpService.GetResourceLocation(_responseMessage);
            });
        }
        [Test()]
        public void ShouldReturnFirstValidLocation()
        {
            Assert.AreEqual("file:///api/v1/poi", _resourceLocation);
        }
        [Test()]
        public void ShouldNotThrowException()
        {
            Assert.NotNull(_exception);
        }
    }

    public class WhenResponseDoesntContainLocationHeaderSpecification : HttpServiceSpecification
    {
        private HttpResponseMessage _responseMessage;
        private string _resourceLocation;
        protected override void Given()
        {
            base.Given();
            _responseMessage = new HttpResponseMessage();
            _responseMessage.Headers.Add("Location123", "/api/v1/poi");
        }

        protected override Task WhenAsync()
        {
            return Task.Factory.StartNew(() =>
            {
                _resourceLocation = httpService.GetResourceLocation(_responseMessage);
            });
        }
        [Test()]
        public void ShouldReturnNullLocation()
        {
            Assert.IsNull(_resourceLocation);
        }
    }

    public class WhenLocationStartsWithHttpSpecification : HttpServiceSpecification
    {
        private string _location;
        private string _returnLocation;
        protected override void Given()
        {
            base.Given();
            _location = "http://localhost/api/v1/abc";
        }

        protected override Task WhenAsync()
        {
            return Task.Factory.StartNew(() =>
            {
                _returnLocation = httpService.FormatLocation(_location);
            });
        }
        [Test()]
        public void ShouldReturnLocationWithoutModification()
        {
            Assert.AreEqual(_location, _returnLocation);
        }
    }

    public class WhenLocationDoesntStartWithHttpSpecification : HttpServiceSpecification
    {
        private string _location;
        private string _returnLocation;
        protected override void Given()
        {
            base.Given();
            _location = "/api/v1/abc";
        }

        protected override Task WhenAsync()
        {
            return Task.Factory.StartNew(() =>
            {
                _returnLocation = httpService.FormatLocation(_location);
            });
        }
        [Test()]
        public void ShouldReturnLocationWithPrependedBaseUrl()
        {
            Assert.AreEqual(Config.Object.BaseUrl + _location, _returnLocation);
        }
    }

    public class WhenCorrectRequestIsProvidedSpecification : HttpServiceSpecification
    {
        private HttpRequestMessage _httpRequestMessage;
        private ApiHttpRequest<EmailRequestModel> _apiHttpRequest;
        private EmailRequestModel _emailRequestModel;

        protected override void Given()
        {
            base.Given();
            _emailRequestModel = new EmailRequestModel
            {
                EmailAddress = "abc@a.com"
            };
            _apiHttpRequest = new ApiHttpRequest<EmailRequestModel>(Link, _emailRequestModel, SessionId, Authroisation, Headers);
        }

        protected override Task WhenAsync()
        {
            return Task.Factory.StartNew(() =>
            {
                _httpRequestMessage = httpService.GetHttpRequestMessage(_apiHttpRequest);
            });
        }
        [Test()]
        public void ShouldAddAuditCallingAppNameHeader()
        {
            Assert.IsTrue(_httpRequestMessage.Headers.TryGetValues("X-AuditCallingAppName", out IEnumerable<string> header));
            Assert.AreEqual(Config.Object.ApplicationName, header.First());
        }
        [Test()]
        public void ShouldAddAuditCallingAppVersionHeader()
        {
            Assert.IsTrue(_httpRequestMessage.Headers.TryGetValues("X-AuditCallingAppVersion", out IEnumerable<string> header));
            Assert.AreEqual(Config.Object.ApplicationVersion, header.First());
        }
        [Test()]
        public void ShouldAddAuditSessionIdHeader()
        {
            Assert.IsTrue(_httpRequestMessage.Headers.TryGetValues("X-AuditSessionId", out IEnumerable<string> header));
            Assert.AreEqual(_apiHttpRequest.SessionId, header.First());
        }
        [Test()]
        public void ShouldAddAuditRequestIdHeader()
        {
            Assert.IsTrue(_httpRequestMessage.Headers.TryGetValues("X-AuditRequestId", out IEnumerable<string> header));
        }
        [Test()]
        public void ShouldAddHeadersFromTheRequest()
        {
            Assert.IsTrue(_httpRequestMessage.Headers.TryGetValues("Accept", out IEnumerable<string> header));
            Assert.IsTrue(_httpRequestMessage.Headers.TryGetValues("Authorization", out header));
            Assert.IsFalse(_httpRequestMessage.Headers.TryGetValues("Content-Type", out header));
        }
        [Test()]
        public void ShouldAddContentHeader()
        {
            Assert.IsTrue(_httpRequestMessage.Content.Headers.TryGetValues("Content-Type", out IEnumerable<string> header));
        }
    }

    public class WhenCorrectRequestWithEmptyPayloadIsProvidedSpecification : HttpServiceSpecification
    {
        private HttpRequestMessage _httpRequestMessage;
        private ApiHttpRequest<EmailRequestModel> _apiHttpRequest;
        protected override void Given()
        {
            base.Given();
            _apiHttpRequest = new ApiHttpRequest<EmailRequestModel>(Link, null, SessionId, Authroisation, Headers);
        }

        protected override Task WhenAsync()
        {
            return Task.Factory.StartNew(() =>
            {
                _httpRequestMessage = httpService.GetHttpRequestMessage(_apiHttpRequest);
            });
        }
        [Test()]
        public void ShouldAddAuditCallingAppNameHeader()
        {
            Assert.IsTrue(_httpRequestMessage.Headers.TryGetValues("X-AuditCallingAppName", out IEnumerable<string> header));
            Assert.AreEqual(Config.Object.ApplicationName, header.First());
        }
        [Test()]
        public void ShouldAddAuditCallingAppVersionHeader()
        {
            Assert.IsTrue(_httpRequestMessage.Headers.TryGetValues("X-AuditCallingAppVersion", out IEnumerable<string> header));
            Assert.AreEqual(Config.Object.ApplicationVersion, header.First());
        }
        [Test()]
        public void ShouldAddAuditSessionIdHeader()
        {
            Assert.IsTrue(_httpRequestMessage.Headers.TryGetValues("X-AuditSessionId", out IEnumerable<string> header));
            Assert.AreEqual(_apiHttpRequest.SessionId, header.First());
        }
        [Test()]
        public void ShouldAddAuditRequestIdHeader()
        {
            Assert.IsTrue(_httpRequestMessage.Headers.TryGetValues("X-AuditRequestId", out IEnumerable<string> header));
        }
        [Test()]
        public void ShouldAddHeadersFromTheRequest()
        {
            Assert.IsTrue(_httpRequestMessage.Headers.TryGetValues("Accept", out IEnumerable<string> header));
            Assert.IsTrue(_httpRequestMessage.Headers.TryGetValues("Authorization", out header));
        }
        [Test()]
        public void ShouldHaveNullContent()
        {
            Assert.IsNull(_httpRequestMessage.Content);
        }
        [Test()]
        public void ShouldNotAddContentHeader()
        {
            Assert.IsFalse(_httpRequestMessage.Headers.TryGetValues("Content-Type", out IEnumerable<string> header));
        }
    }

    public class WhenCorrectResponseIsGeneratedSpecification : HttpServiceSpecification
    {
        private readonly HttpResponseMessage _httpResponseMessage = new HttpResponseMessage();
        private ApiResult<PoiResponseModel> _apiResult;

        protected override void Given()
        {
            base.Given();
            _httpResponseMessage.Content = new StringContent(@"{
                        'processId': '52942edb-3cd6-4ab4-8ee7-b23cd64c4430',
                        'status': 'Submitted'
                    }");
            _httpResponseMessage.Headers.Add("Location", "/api/v1/poi");
        }

        protected override async Task WhenAsync()
        {
            _apiResult = await httpService.GetHttpResponseAsync<PoiResponseModel>(_httpResponseMessage);
        }
        [Test()]
        public void ShouldHaveNonNullApiResult()
        {
            Assert.NotNull(_apiResult);
        }
        [Test()]
        public void ShouldHaveNonNullResponseModel()
        {
            Assert.NotNull(_apiResult.Resource);
        }
        [Test()]
        public void ShouldHaveSuccessfulResourceStatus()
        {
            Assert.IsTrue(ResourceStatus.Success == _apiResult.ResourceStatus);
        }
    }
}