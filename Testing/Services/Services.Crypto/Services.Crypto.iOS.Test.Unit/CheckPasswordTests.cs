﻿using NUnit.Framework;
using myGovID.Services.Crypto.Contract.Model;
using myGovID.Services.Crypto.Contract;
using System;

namespace myGovID.Services.Crypto.iOS.Test.Unit.Crypto
{
    [TestFixture()]
    public class CryptoCheckCorrectPassword : CryptoTestBase
    {
        CheckPasswordRequest CheckPasswordOperation;
        CryptoException _exception;
        protected override void Given()
        {
            Executor.Delete(new DeleteRequest());
            SetupDefaultKeystore();
            Executor.Load(new LoadRequest());
            CheckPasswordOperation = new CheckPasswordRequest("Pass123456");
        }
        protected override void When()
        {
            try
            {
                Executor.CheckPassword(CheckPasswordOperation);
            }
            catch (Exception e)
            {
                _exception = (CryptoException)e;
            }
        }
        [Test()]
        public void ShouldHaveNoErrors()
        {
            Assert.IsNull(_exception);
        }
    }
    [TestFixture()]
    public class CryptoCheckWrongPassword : CryptoTestBase
    {
        CheckPasswordRequest CheckPasswordOperation;
        CryptoException _exception;
        protected override void Given()
        {
            Executor.Delete(new DeleteRequest());
            SetupDefaultKeystore();
            Executor.Load(new LoadRequest());
            CheckPasswordOperation = new CheckPasswordRequest("Password3!");
        }
        protected override void When()
        {
            try
            {
                Executor.CheckPassword(CheckPasswordOperation);
            }
            catch (Exception e)
            {
                _exception = (CryptoException)e;
            }
        }
        [Test()]
        public void ShouldHaveError()
        {
            Assert.IsNotNull(_exception);
            Assert.AreEqual("4400", _exception.Code);
        }
    }
}