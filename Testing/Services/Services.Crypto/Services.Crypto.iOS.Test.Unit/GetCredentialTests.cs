﻿using NUnit.Framework;
using myGovID.Services.Crypto.Contract.Model;
using myGovID.Services.Crypto.Contract;
using System;

namespace myGovID.Services.Crypto.iOS.Test.Unit.Crypto
{
    [TestFixture()]
    public class CryptoGetCredentialEmptyKeystore : CryptoTestBase
    {
        GetCredentialRequest GetCredentialOperation;
        GetCredentialResult GetCredentialResponse;
        CryptoException _exception;
        protected override void Given()
        {
            Executor.Delete(new DeleteRequest());
            GetCredentialOperation = new GetCredentialRequest("123456789", "Password1!");
        }
        protected override void When()
        {
            try
            {
                GetCredentialResponse = Executor.GetCredential(GetCredentialOperation);
            }
            catch (Exception e)
            {
                _exception = (CryptoException)e;
            }
        }
        [Test()]
        public void ShouldHaveErrors()
        {
            Assert.IsNotNull(_exception);
        }
    }
    [TestFixture()]
    public class CryptoGetCredentialNonEmptyKeystore : CryptoTestBase
    {
        GetCredentialRequest GetCredentialOperation;
        GetCredentialResult GetCredentialResponse;
        protected override void Given()
        {
            Executor.Delete(new DeleteRequest());
            SetupDefaultKeystore();
            Executor.Load(new LoadRequest());
            GetCredentialOperation = new GetCredentialRequest("458ca823-9df4-49f0-8b2e-b5f6c78fa2bc", "Pass123456");
        }
        protected override void When()
        {
            GetCredentialResponse = Executor.GetCredential(GetCredentialOperation);
        }
        [Test()]
        public void ShouldHavePublicKey()
        {
            Assert.IsNotNull(GetCredentialResponse.PublicKey);
        }
        [Test()]
        public void ShouldHavePrivateKey()
        {
            Assert.IsNotNull(GetCredentialResponse.PrivateKey);
        }
    }
}