﻿using NUnit.Framework;
using myGovID.Services.Crypto.Contract.Model;
using myGovID.Services.Crypto.Contract;
using System;

namespace myGovID.Services.Crypto.iOS.Test.Unit.Crypto
{
    [TestFixture()]
    public class CryptoDeleteEmptyKeystore : CryptoTestBase
    {
        DeleteRequest DeleteOperation;
        CryptoException _exception;
        protected override void Given()
        {
            DeleteOperation = new DeleteRequest();
        }
        protected override void When()
        {
            try
            {
                Executor.Delete(DeleteOperation);
            }
            catch (Exception e)
            {
                _exception = (CryptoException)e;
            }
        }
        [Test()]
        public void ShouldHaveNoErrors()
        {
            Assert.IsNull(_exception);
        }
    }
    [TestFixture()]
    public class CryptoDeleteNonEmptyKeystore : CryptoTestBase
    {
        DeleteRequest DeleteOperation;
        GetCredentialResult GetCredentialResponse;
        CryptoException _exception;
        protected override void Given()
        {
            SetupDefaultKeystore();
            Executor.Load(new LoadRequest());
            DeleteOperation = new DeleteRequest();
        }
        protected override void When()
        {
            Executor.Delete(DeleteOperation);
            try
            {
                GetCredentialResponse = Executor.GetCredential(new GetCredentialRequest("458ca823-9df4-49f0-8b2e-b5f6c78fa2bc", "Pass123456"));
            }
            catch (Exception e)
            {
                _exception = (CryptoException)e;
            }
        }
        [Test()]
        public void ShouldHaveNoCredentials()
        {
            Assert.IsNotNull(_exception);
            Assert.AreEqual("4404", _exception.Code);
            Assert.IsNull(GetCredentialResponse);
        }
    }
}