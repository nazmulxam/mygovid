﻿using NUnit.Framework;
using myGovID.Services.Crypto.Contract.Model;
using myGovID.Services.Crypto.Contract;
using System;

namespace myGovID.Services.Crypto.iOS.Test.Unit.Crypto
{
    [TestFixture()]
    public class CryptoChangePassword : CryptoTestBase
    {
        ChangePasswordRequest ChangePasswordOperation;
        GetCredentialResult GetCredentialResponse;
        CryptoException _exception;
        protected override void Given()
        {
            Executor.Delete(new DeleteRequest());
            SetupDefaultKeystore();
            Executor.Load(new LoadRequest());
            ChangePasswordOperation = new ChangePasswordRequest("Pass123456", "Password1!");
        }
        protected override void When()
        {
            try
            {
                Executor.ChangePassword(ChangePasswordOperation);
                GetCredentialResponse = Executor.GetCredential(new GetCredentialRequest("458ca823-9df4-49f0-8b2e-b5f6c78fa2bc", "Password1!"));
            }
            catch (Exception e)
            {
                _exception = (CryptoException)e;
            }
        }
        [Test()]
        public void ShouldHaveNoErrors()
        {
            Assert.IsNull(_exception);
        }
        [Test()]
        public void ShouldHaveACredential()
        {
            Assert.IsNotNull(GetCredentialResponse);
        }
        [Test()]
        public void ShouldNotHaveErrorFetchingCredential()
        {
            Assert.IsNull(_exception);
        }
        [Test()]
        public void ShouldHavePublicKey()
        {
            Assert.IsNotNull(GetCredentialResponse.PublicKey);
        }
        [Test()]
        public void ShouldHavePrivateKey()
        {
            Assert.IsNotNull(GetCredentialResponse.PrivateKey);
        }
    }
    [TestFixture()]
    public class CryptoChangePasswordWithInvalidCharacters : CryptoTestBase
    {
        ChangePasswordRequest ChangePasswordOperation;
        CryptoException _exception;
        protected override void Given()
        {
            Executor.Delete(new DeleteRequest());
            SetupDefaultKeystore();
            Executor.Load(new LoadRequest());
            ChangePasswordOperation = new ChangePasswordRequest("Pass123456", "password");
        }
        protected override void When()
        {
            try
            {
                Executor.ChangePassword(ChangePasswordOperation);
            }
            catch (Exception e)
            {
                _exception = (CryptoException)e;
            }
        }
        [Test()]
        public void ShouldHaveErrors()
        {
            Assert.IsNotNull(_exception);
        }
    }
    [TestFixture()]
    public class CryptoChangePasswordOnEmptyKeystore : CryptoTestBase
    {
        ChangePasswordRequest ChangePasswordOperation;
        CryptoException _exception;
        protected override void Given()
        {
            Executor.Delete(new DeleteRequest());
            Executor.Load(new LoadRequest());
            ChangePasswordOperation = new ChangePasswordRequest("Pass123456", "password");
        }
        protected override void When()
        {
            try
            {
                Executor.ChangePassword(ChangePasswordOperation);
            }
            catch (Exception e)
            {
                _exception = (CryptoException)e;
            }
        }
        [Test()]
        public void ShouldHaveErrors()
        {
            Assert.IsNotNull(_exception);
        }
    }
}