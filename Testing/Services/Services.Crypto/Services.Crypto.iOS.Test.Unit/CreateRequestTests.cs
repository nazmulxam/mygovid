﻿using NUnit.Framework;
using System;
using myGovID.Services.Crypto.Contract.Model;
using myGovID.Services.Crypto.Contract;

namespace myGovID.Services.Crypto.iOS.Test.Unit.Crypto
{
    [TestFixture()]
    public class CryptoCreateRequestWithIdAndPassword : CryptoTestBase
    {
        CreateRequest CreateRequestOperation;
        CreateResult CreateRequestResponse;
        protected override void Given()
        {
            Executor.Delete(new DeleteRequest());
            Executor.Load(new LoadRequest());
            CreateRequestOperation = new CreateRequest("1234567890", "Password1!");
        }
        protected override void When()
        {
            CreateRequestResponse = Executor.CreateRequest(CreateRequestOperation);
        }
        [Test()]
        public void ShouldHaveP10AndRequestId()
        {
            Assert.IsNotNull(CreateRequestResponse);
        }
        [Test()]
        public void ShouldBeAValidBase64P10()
        {
            byte[] p10Bytes = Convert.FromBase64String(CreateRequestResponse.P10base64);
            Assert.IsNotNull(p10Bytes);
            Assert.IsTrue(p10Bytes.Length > 0);
        }
    }
    [TestFixture()]
    public class CryptoCreateRequestWithEmptyCredentialId : CryptoTestBase
    {
        private Exception _exception;
        CreateRequest CreateRequestOperation;
        protected override void Given()
        {
            Executor.Load(new LoadRequest());
            try {
                CreateRequestOperation = new CreateRequest("", "Password1!");
            } catch(Exception e) {
                _exception = e;
            }
        }
        protected override void When()
        {
        }
        [Test()]
        public void ShouldThrowArgumentException()
        {
            Assert.IsNotNull(_exception);
            Assert.IsTrue(_exception is ArgumentException);
            Assert.AreEqual("credentialId", _exception.Message);
        }
    }
    [TestFixture()]
    public class CryptoCreateRequestWithEmptyPassword : CryptoTestBase
    {
        private Exception _exception;
        CreateRequest CreateRequestOperation;
        protected override void Given()
        {
            Executor.Load(new LoadRequest());
            try {
                CreateRequestOperation = new CreateRequest("1234567890", "");
            }
            catch (Exception e)
            {
                _exception = e;
            }
        }
        protected override void When()
        {
        }
        [Test()]
        public void ShouldThrowArgumentException()
        {
            Assert.IsNotNull(_exception);
            Assert.IsTrue(_exception is ArgumentException);
            Assert.AreEqual("password", _exception.Message);
        }
    }
    [TestFixture()]
    public class CryptoCreateRequestWithBadPassword : CryptoTestBase
    {
        CreateRequest CreateRequestOperation;
        CreateResult CreateRequestResponse;
        CryptoException _exception;
        protected override void Given()
        {
            Executor.Delete(new DeleteRequest());
            SetupDefaultKeystore();
            Executor.Load(new LoadRequest());
            CreateRequestOperation = new CreateRequest("1234567890", "Test001!1");
        }
        protected override void When()
        {
            try
            {
                CreateRequestResponse = Executor.CreateRequest(CreateRequestOperation);
            }
            catch (Exception e)
            {
                _exception = (CryptoException)e;
            }
        }
        [Test()]
        public void ShouldNotHaveP10AndRequestId()
        {
            Assert.IsNull(CreateRequestResponse);
        }
        [Test()]
        public void ShouldHaveErrors()
        {
            Assert.IsNotNull(_exception);
            Assert.AreEqual("4400", _exception.Code);
            Assert.AreEqual("", _exception.Description);
        }
    }
    [TestFixture()]
    public class CryptoCreateRequestWithInvalidPassword : CryptoTestBase
    {
        CreateRequest CreateRequestOperation;
        CreateResult CreateRequestResponse;
        CryptoException _exception;
        protected override void Given()
        {
            Executor.Delete(new DeleteRequest());
            Executor.Load(new LoadRequest());
            CreateRequestOperation = new CreateRequest("1234567890", "password");
        }
        protected override void When()
        {
            try
            {
                CreateRequestResponse = Executor.CreateRequest(CreateRequestOperation);
            }
            catch (Exception e)
            {
                _exception = (CryptoException)e;
            }
        }
        [Test()]
        public void ShouldNotHaveP10AndRequestId()
        {
            Assert.IsNull(CreateRequestResponse);
        }
        [Test()]
        public void ShouldHaveErrors()
        {
            Assert.IsNotNull(_exception);
            Assert.AreEqual("4401", _exception.Code);
            Assert.AreEqual("", _exception.Description);
        }
    }
}