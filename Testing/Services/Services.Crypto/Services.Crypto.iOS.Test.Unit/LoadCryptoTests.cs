﻿using NUnit.Framework;
using myGovID.Services.Crypto.Contract.Model;
using myGovID.Services.Crypto.Contract;

namespace myGovID.Services.Crypto.iOS.Test.Unit.Crypto
{
    [TestFixture()]
    public class CryptoLoadWithDefaultParams : CryptoTestBase
    {
        LoadRequest LoadOperation;
        LoadResult LoadResponse;
        protected override void Given()
        {
            LoadOperation = new LoadRequest();
        }
        protected override void When()
        {
            LoadResponse = Executor.Load(LoadOperation);
        }
        [Test()]
        public void ShouldHaveVersion()
        {
            Assert.IsNotNull(LoadResponse.Version);
            Assert.AreEqual("2.1.6", LoadResponse.Version);
        }
    }
    [TestFixture()]
    public class CryptoLoadWithInvalidKeystoreDirectory : CryptoTestBase
    {
        LoadRequest LoadOperation;
        LoadResult LoadResponse;
        protected override void Given()
        {
            LoadOperation = new LoadRequest("dsfsdfdsfdsfdsfdsfdsfdsfdsfdsfdsfdsfdsfdsfdsfdsfdsfdsfdsfdffds");
        }
        protected override void When()
        {
            LoadResponse = Executor.Load(LoadOperation);
        }
        [Test()]
        public void ShouldBeSuccessfullyInvoked()
        {
            Assert.IsNotNull(LoadResponse);
        }
        [Test()]
        public void ShouldHaveVersion()
        {
            Assert.IsNotNull(LoadResponse.Version);
            Assert.AreEqual("2.1.6", LoadResponse.Version);
        }
    }
}