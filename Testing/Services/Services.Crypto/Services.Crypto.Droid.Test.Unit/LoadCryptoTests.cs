﻿using NUnit.Framework;
using myGovID.Services.Crypto.Contract.Model;
using myGovID.Services.Crypto.Contract;

namespace myGovID.Services.Crypto.Droid.Test.Unit
{
    [TestFixture()]
    public class CryptoLoadWithDefaultParams : CryptoTestBase
    {
        LoadRequest LoadOperation;
        LoadResult LoadResponse;
        protected override void Given()
        {
            LoadOperation = new LoadRequest(KeystoreDirectory);
        }
        protected override void When()
        {
            LoadResponse = Executor.Load(LoadOperation);
        }
        [Test()]
        public void ShouldBeSuccessfullyInvoked()
        {
            Assert.IsNotNull(LoadResponse);
        }
        [Test()]
        public void ShouldHaveVersion()
        {
            Assert.IsNotNull(LoadResponse.Version);
            Assert.AreEqual("0.0", LoadResponse.Version);
        }
    }
}