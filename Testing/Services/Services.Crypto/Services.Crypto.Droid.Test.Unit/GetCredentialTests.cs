﻿
using NUnit.Framework;
using myGovID.Services.Crypto.Contract.Model;
using myGovID.Services.Crypto.Contract;
using System;

namespace myGovID.Services.Crypto.Droid.Test.Unit
{
    [TestFixture()]
    public class CryptoGetCredentialEmptyKeystore : CryptoTestBase
    {
        GetCredentialRequest GetCredentialOperation;
        GetCredentialResult GetCredentialResponse;
        CryptoException _exception;
        protected override void Given()
        {
            Executor.Load(new LoadRequest(KeystoreDirectory));
            Executor.Delete(new DeleteRequest());
            GetCredentialOperation = new GetCredentialRequest("123456789", "Password1!");
        }
        protected override void When()
        {
            try
            {
                GetCredentialResponse = Executor.GetCredential(GetCredentialOperation);
            }
            catch (Exception e)
            {
                _exception = (CryptoException)e;
            }
        }
        [Test()]
        public void ShouldHaveErrors()
        {
            Assert.IsNotNull(_exception);
            Assert.AreEqual("4404", _exception.Code);
            Assert.IsNull(GetCredentialResponse);
        }
    }
    [TestFixture()]
    public class CryptoGetCredentialNonEmptyKeystore : CryptoTestBase
    {
        GetCredentialRequest GetCredentialOperation;
        GetCredentialResult GetCredentialResponse;
        CryptoException _exception;
        protected override void Given()
        {
            Executor.Load(new LoadRequest(SetupDefaultKeystore()));
            GetCredentialOperation = new GetCredentialRequest("458ca823-9df4-49f0-8b2e-b5f6c78fa2bc", "Pass123456");
        }
        protected override void When()
        {
            try
            {
                GetCredentialResponse = Executor.GetCredential(GetCredentialOperation);
            }
            catch (Exception e)
            {
                _exception = (CryptoException)e;
            }
        }
        [Test()]
        public void ShouldNotHaveErrors()
        {
            Assert.IsNull(_exception);
        }
        [Test()]
        public void ShouldHavePublicKey()
        {
            Assert.IsNotNull(GetCredentialResponse.PublicKey);
        }
        [Test()]
        public void ShouldHavePrivateKey()
        {
            Assert.IsNotNull(GetCredentialResponse.PrivateKey);
        }
    }
}