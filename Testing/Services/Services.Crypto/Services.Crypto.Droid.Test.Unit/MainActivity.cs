﻿using System.Reflection;
using System;
using Android;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Util;
using Xamarin.Android.NUnitLite;
using Android.Support.V4.App;
using Android.Support.Design.Widget;

namespace myGovID.Services.Crypto.Droid.Test.Unit
{
    [Activity(Label = "myGovID.Services.Crypto.Droid.Test.Unit", MainLauncher = true)]
    public class MainActivity : TestSuiteActivity, ActivityCompat.IOnRequestPermissionsResultCallback
    {
        const string TAG = "MainActivity";
        const int RC_WRITE_EXTERNAL_STORAGE_PERMISSION = 1000;
        private static readonly string[] PERMISSIONS_TO_REQUEST = { Manifest.Permission.WriteExternalStorage };
        public static MainActivity Shared;
        public MainActivity() : base()
        {
            Shared = this;
        }
        protected override void OnCreate(Bundle bundle)
        {
            // tests can be inside the main assembly
            AddTest(Assembly.GetExecutingAssembly());
            // or in any reference assemblies
            // AddTest (typeof (Your.Library.TestClass).Assembly);

            // Once you called base.OnCreate(), you cannot add more assemblies.
            base.OnCreate(bundle);
            RequestExternalStoragePermissionIfNecessary(RC_WRITE_EXTERNAL_STORAGE_PERMISSION);
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            switch (requestCode)
            {
                case RC_WRITE_EXTERNAL_STORAGE_PERMISSION:
                    if (grantResults.Length == 1 && grantResults[0] == Permission.Granted)
                    {
                        Console.WriteLine("Write permission granted.");
                    }
                    break;
                default:
                    base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
                    break;
            }
        }
        bool RequestExternalStoragePermissionIfNecessary(int requestCode)
        {
            if (Android.OS.Environment.MediaMounted.Equals(Android.OS.Environment.ExternalStorageState))
            {
                if (CheckSelfPermission(Manifest.Permission.WriteExternalStorage) == Permission.Granted)
                {
                    return true;
                }
                if (ShouldShowRequestPermissionRationale(Manifest.Permission.WriteExternalStorage))
                {
                    Snackbar.Make(FindViewById(Android.Resource.Id.Content),
                                  "Need access to the storage to setup default keystore.",
                                  Snackbar.LengthIndefinite)
                            .SetAction("Ok", delegate { RequestPermissions(PERMISSIONS_TO_REQUEST, requestCode); });
                }
                else
                {
                    RequestPermissions(PERMISSIONS_TO_REQUEST, requestCode);
                }
                return false;
            }
            Log.Warn(TAG, "External storage is not mounted; cannot request permission");
            return false;
        }
        public string GetFilesDir()
        {
            return FilesDir.AbsolutePath;
        }
    }
}
