﻿using NUnit.Framework;
using System;
using myGovID.Services.Crypto.Contract.Model;
using myGovID.Services.Crypto.Contract;

namespace myGovID.Services.Crypto.Droid.Test.Unit
{
    [TestFixture()]
    public class CryptoCreateRequestWithIdAndPassword : CryptoTestBase
    {
        private CreateRequest _createRequestOperation;
        private CreateResult _createRequestResponse;
        protected override void Given()
        {
            Executor.Load(new LoadRequest(KeystoreDirectory));
            Executor.Delete(new DeleteRequest());
            _createRequestOperation = new CreateRequest("1234567890", "Password1!");
        }
        protected override void When()
        {
            _createRequestResponse = Executor.CreateRequest(_createRequestOperation);
        }
        [Test()]
        public void ShouldHaveP10AndRequestId()
        {
            Assert.IsNotNull(_createRequestResponse.P10base64);
            Assert.IsNotNull(_createRequestResponse.RequestId);
        }
        [Test()]
        public void ShouldBeAValidBase64P10()
        {
            byte[] p10Bytes = Convert.FromBase64String(_createRequestResponse.P10base64);
            Assert.IsNotNull(p10Bytes);
            Assert.IsTrue(p10Bytes.Length > 0);
        }
    }
    [TestFixture()]
    public class CryptoCreateRequestWithEmptyPassword : CryptoTestBase
    {
        private Exception _exception;
        private CreateRequest _createRequestOperation;
        protected override void Given()
        {
            Executor.Load(new LoadRequest(KeystoreDirectory));
            try {
                _createRequestOperation = new CreateRequest("1234567890", "");
            } catch(Exception e) {
                _exception = e;
            }
        }
        protected override void When()
        {
        }
        [Test()]
        public void ShouldThrowArgumentException()
        {
            Assert.IsNotNull(_exception);
            Assert.IsTrue(_exception is ArgumentException);
            Assert.AreEqual("password", _exception.Message);
        }
    }
    [TestFixture()]
    public class CryptoCreateRequestWithBadPassword : CryptoTestBase
    {
        private CreateRequest _createRequestOperation;
        private CreateResult _createRequestResponse;
        private CryptoException _exception;
        protected override void Given()
        {
            Executor.Load(new LoadRequest(SetupDefaultKeystore()));
            Executor.Delete(new DeleteRequest());
            _createRequestOperation = new CreateRequest("1234567890", "Test001!1");
        }
        protected override void When()
        {
            try
            {
                _createRequestResponse = Executor.CreateRequest(_createRequestOperation);
            }
            catch (Exception e)
            {
                _exception = (CryptoException)e;
            }
        }
        [Test()]
        public void ShouldNotHaveP10AndRequestId()
        {
            Assert.IsNull(_createRequestResponse);
        }
        [Test()]
        public void ShouldHaveErrors()
        {
            Assert.IsNotNull(_exception);
            Assert.AreEqual("4401", _exception.Code);
        }
    }
    [TestFixture()]
    public class CryptoCreateRequestWithInvalidPassword : CryptoTestBase
    {
        private CreateRequest _createRequestOperation;
        private CreateResult _createRequestResponse;
        private CryptoException _exception;
        protected override void Given()
        {
            Executor.Load(new LoadRequest(KeystoreDirectory));
            Executor.Delete(new DeleteRequest());
            _createRequestOperation = new CreateRequest("1234567890", "password");
        }
        protected override void When()
        {
            try
            {
                _createRequestResponse = Executor.CreateRequest(_createRequestOperation);
            }
            catch(Exception e)
            {
                _exception = (CryptoException) e;
            }
        }
        [Test()]
        public void ShouldNotHaveP10AndRequestId()
        {
            Assert.IsNull(_createRequestResponse);
        }
        [Test()]
        public void ShouldHaveErrors()
        {
            Assert.IsNotNull(_exception);
            Assert.AreEqual("4401", _exception.Code);
        }
    }
}