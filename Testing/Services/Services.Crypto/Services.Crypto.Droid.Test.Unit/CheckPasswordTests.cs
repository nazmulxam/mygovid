﻿using NUnit.Framework;
using myGovID.Services.Crypto.Contract.Model;
using myGovID.Services.Crypto.Contract;
using System;

namespace myGovID.Services.Crypto.Droid.Test.Unit
{
    [TestFixture()]
    public class CryptoCheckCorrectPassword : CryptoTestBase
    {
        CheckPasswordRequest CheckPasswordOperation;
        CryptoException _exception;
        protected override void Given()
        {
            Executor.Load(new LoadRequest(SetupDefaultKeystore()));
            CheckPasswordOperation = new CheckPasswordRequest("Pass123456");
        }
        protected override void When()
        {
            try
            {
                Executor.CheckPassword(CheckPasswordOperation);
            }
            catch (Exception e)
            {
                _exception = (CryptoException)e;
            }
        }
        [Test()]
        public void ShouldHaveNoErrors()
        {
            Assert.IsNull(_exception);
        }
    }
    [TestFixture()]
    public class CryptoCheckWrongPassword : CryptoTestBase
    {
        CheckPasswordRequest CheckPasswordOperation;
        CryptoException _exception;
        protected override void Given()
        {
            Executor.Load(new LoadRequest(SetupDefaultKeystore()));
            CheckPasswordOperation = new CheckPasswordRequest("Password3!");
        }
        protected override void When()
        {
            try
            {
                Executor.CheckPassword(CheckPasswordOperation);
            }
            catch (Exception e)
            {
                _exception = (CryptoException)e;
            }
        }
        [Test()]
        public void ShouldHaveError()
        {
            Assert.IsNotNull(_exception);
            Assert.AreEqual("4400", _exception.Code);
        }
    }
}