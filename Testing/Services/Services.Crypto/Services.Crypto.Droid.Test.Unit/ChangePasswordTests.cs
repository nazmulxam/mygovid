﻿using NUnit.Framework;
using myGovID.Services.Crypto.Contract.Model;
using System;
using myGovID.Services.Crypto.Contract;

namespace myGovID.Services.Crypto.Droid.Test.Unit
{
    [TestFixture()]
    public class CryptoChangePassword : CryptoTestBase
    {
        ChangePasswordRequest ChangePasswordOperation;
        GetCredentialResult GetCredentialResponse;
        protected override void Given()
        {
            Executor.Load(new LoadRequest(SetupDefaultKeystore()));
            ChangePasswordOperation = new ChangePasswordRequest("Pass123456", "Password1!");
        }
        protected override void When()
        {
            Executor.ChangePassword(ChangePasswordOperation);
            GetCredentialResponse = Executor.GetCredential(new GetCredentialRequest("458ca823-9df4-49f0-8b2e-b5f6c78fa2bc", "Password1!"));
        }
        [Test()]
        public void ShouldHaveACredential()
        {
            Assert.IsNotNull(GetCredentialResponse);
        }
        [Test()]
        public void ShouldHavePublicKey()
        {
            Assert.IsNotNull(GetCredentialResponse.PublicKey);
        }
        [Test()]
        public void ShouldHavePrivateKey()
        {
            Assert.IsNotNull(GetCredentialResponse.PrivateKey);
        }
    }
    [TestFixture()]
    public class CryptoChangePasswordWithInvalidCharacters : CryptoTestBase
    {
        CryptoException _exception;
        ChangePasswordRequest ChangePasswordOperation;
        protected override void Given()
        {
            Executor.Load(new LoadRequest(SetupDefaultKeystore()));
            ChangePasswordOperation = new ChangePasswordRequest("Pass123456", "password");
        }
        protected override void When()
        {
            try
            {
                Executor.ChangePassword(ChangePasswordOperation);
            }
            catch (Exception e)
            {
                _exception = (CryptoException)e;
            }
        }
        [Test()]
        public void ShouldHaveErrors()
        {
            Assert.IsNotNull(_exception);
        }
    }
    [TestFixture()]
    public class CryptoChangePasswordOnEmptyKeystore : CryptoTestBase
    {
        ChangePasswordRequest ChangePasswordOperation;
        CryptoException _exception;
        protected override void Given()
        {
            Executor.Load(new LoadRequest(SetupEmptyKeystore()));
            ChangePasswordOperation = new ChangePasswordRequest("Pass123456", "password");
        }
        protected override void When()
        {
            try
            {
                Executor.ChangePassword(ChangePasswordOperation);
            }
            catch (Exception e)
            {
                _exception = (CryptoException)e;
            }
        }
        [Test()]
        public void ShouldHaveErrors()
        {
            Assert.IsNotNull(_exception);
        }
    }
}