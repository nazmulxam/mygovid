﻿namespace myGovID.Services.Uri.Droid.Test.Unit
{
    public abstract class TestSpecification
    {
        protected TestSpecification()
        {
            Setup();
        }
        protected void Setup()
        {
            Given();
            When();
        }
        protected abstract void Given();
        protected abstract void When();
    }
}