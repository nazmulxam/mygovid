﻿using System;
using myGovID.Services.Uri.Contract;
using NUnit.Framework;

namespace myGovID.Services.Uri.Droid.Test.Unit
{
    [TestFixture()]
    public abstract class UriServiceTests : TestSpecification
    {
        protected readonly UriService UriService = new UriService();
        protected string Template;
        protected string Url;
        protected string BaseUri;
        protected UriMatchResult uriMatchResult;

        protected override void Given()
        {
        }

        protected override void When()
        {
        }
    }
    public class WhenTemplateAndUriAreSame : UriServiceTests
    {
        protected override void Given()
        {
            Template = "/api/v1/poi";
            Url = Template;
        }
        protected override void When()
        {
            uriMatchResult = UriService.Match(Template, Url);
        }
        [Test()]
        public void ShouldReturnNonNullUriMatchResult()
        {
            Assert.IsNotNull(uriMatchResult);
        }
    }
}
