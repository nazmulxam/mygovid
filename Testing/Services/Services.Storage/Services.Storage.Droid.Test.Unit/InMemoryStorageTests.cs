﻿using myGovID.Test;
using NUnit.Framework;

namespace myGovID.Services.Storage.Droid.Test.Unit.InMemoryStorageTests
{
    [TestFixture]
    public abstract class InMemoryStorageTests : TestSpecification
    {
        protected MemoryStorage memoryStorage;

        protected override void Given()
        {
            memoryStorage = new MemoryStorage();
        }

        protected override void When()
        { }
    }

    public class WhenNewValueIsBeingSet : InMemoryStorageTests
    {
        protected override void When()
        {
            memoryStorage.SetValue("key", "value");
        }

        [Test]
        public void ShouldContainNewKeyInFileStorage()
        {
            Assert.IsTrue(memoryStorage.HasKey("key"));
        }

        [Test]
        public void ShouldContainNewValueInFileStorage()
        {
            Assert.AreEqual("value", memoryStorage.GetValue("key"));
        }
    }

    public class WhenKeyAlreadyExists : InMemoryStorageTests
    {
        protected override void Given()
        {
            memoryStorage = new MemoryStorage();
            memoryStorage.SetValue("key", "value");
        }

        protected override void When()
        {
            memoryStorage.SetValue("key", "newValue");
        }

        [Test]
        public void ShouldRetainExistingKeyInFileStorage()
        {
            Assert.IsTrue(memoryStorage.HasKey("key"));
        }

        [Test]
        public void ShouldOverwriteValueInFileStorage()
        {
            Assert.AreEqual("newValue", memoryStorage.GetValue("key"));
        }
    }

    public class WhenKeyIsBeingDeleted : InMemoryStorageTests
    {
        protected override void Given()
        {
            memoryStorage = new MemoryStorage();
            memoryStorage.SetValue("key", "value");
        }

        protected override void When()
        {
            memoryStorage.DeleteKey("key");
        }

        [Test]
        public void ShouldNotHaveDeletedKeyInMemory()
        {
            Assert.IsFalse(memoryStorage.HasKey("key"));
        }
    }

    public class WhenKeyToBeDeletedDoesntExist : InMemoryStorageTests
    {
        protected override void When()
        {
            memoryStorage.DeleteKey("key");
        }

        [Test]
        public void ShouldNotHaveKeyInMemory()
        {
            Assert.IsFalse(memoryStorage.HasKey("key"));
        }
    }

    public class WhenStorageClearIsCalled : InMemoryStorageTests
    {
        protected override void Given()
        {
            memoryStorage = new MemoryStorage();
            memoryStorage.SetValue("key", "value");
            memoryStorage.SetValue("key1", "value");
        }

        protected override void When()
        {
            memoryStorage.Clear();
        }

        [Test]
        public void ShouldNotHaveAnyKeysInMemory()
        {
            Assert.IsFalse(memoryStorage.HasKey("key"));
            Assert.IsFalse(memoryStorage.HasKey("key1"));
        }
    }
}
