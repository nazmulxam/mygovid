﻿using myGovID.Test;
using NUnit.Framework;

namespace myGovID.Services.Storage.Droid.Test.Unit.SecureStorageTests
{
    [TestFixture]
    public abstract class SecureStorageTests : TestSpecification
    {
        protected SecureStorage secureStorage;

        protected override void Given()
        {
            secureStorage = new SecureStorage();
        }

        protected override void When()
        { }
    }

    public class WhenNewValueIsBeingSet : SecureStorageTests
    {
        protected override void When()
        {
            secureStorage.SetValue("key", "value");
        }

        [Test]
        public void ShouldContainNewKeyInFileStorage()
        {
            Assert.IsTrue(secureStorage.HasKey("key"));
        }

        [Test]
        public void ShouldContainNewValueInFileStorage()
        {
            Assert.AreEqual("value", secureStorage.GetValue("key"));
        }
    }

    public class WhenKeyAlreadyExists : SecureStorageTests
    {
        protected override void Given()
        {
            secureStorage = new SecureStorage();
            secureStorage.SetValue("key1", "value");
        }

        protected override void When()
        {
            secureStorage.SetValue("key1", "newValue");
        }

        [Test]
        public void ShouldRetainExistingKeyInFileStorage()
        {
            Assert.IsTrue(secureStorage.HasKey("key1"));
        }

        [Test]
        public void ShouldOverwriteValueInFileStorage()
        {
            Assert.AreEqual("newValue", secureStorage.GetValue("key1"));
        }
    }

    public class WhenKeyIsBeingDeleted : SecureStorageTests
    {
        protected override void Given()
        {
            secureStorage = new SecureStorage();
            secureStorage.SetValue("key2", "value");
        }

        protected override void When()
        {
            secureStorage.DeleteKey("key2");
        }

        [Test]
        public void ShouldNotHaveDeletedKeyInSecuredStorage()
        {
            Assert.IsFalse(secureStorage.HasKey("key2"));
        }
    }

    public class WhenKeyToBeDeletedDoesntExist : SecureStorageTests
    {
        protected override void Given()
        {
            secureStorage = new SecureStorage();
        }

        protected override void When()
        {
            secureStorage.DeleteKey("key3");
        }

        [Test]
        public void ShouldNotHaveKeyInSecureStorage()
        {
            Assert.IsFalse(secureStorage.HasKey("key3"));
        }
    }

    public class WhenStorageClearIsCalled : SecureStorageTests
    {
        protected override void Given()
        {
            secureStorage = new SecureStorage();
            secureStorage.SetValue("key", "value");
            secureStorage.SetValue("key1", "value");
        }

        protected override void When()
        {
            secureStorage.Clear();
        }

        [Test]
        public void ShouldNotHaveAnyKeysInSecureStorage()
        {
            Assert.IsFalse(secureStorage.HasKey("key"));
            Assert.IsFalse(secureStorage.HasKey("key1"));
        }
    }
}
