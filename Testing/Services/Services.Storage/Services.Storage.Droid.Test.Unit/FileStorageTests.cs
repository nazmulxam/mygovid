﻿using myGovID.Test;
using NUnit.Framework;

namespace myGovID.Services.Storage.Droid.Test.Unit.FileStorageTests
{
    [TestFixture]
    public abstract class FileStorageTests : TestSpecification
    {
        protected FileStorage fileStorage;

        protected override void Given()
        {
            fileStorage = new FileStorage();
        }

        protected override void When()
        { }
    }

    public class WhenNewValueIsBeingSet : FileStorageTests
    {
        protected override void When()
        {
            fileStorage.SetValue("key", "value");
        }

        [Test]
        public void ShouldContainNewKeyInFileStorage()
        {
            Assert.IsTrue(fileStorage.HasKey("key"));
        }

        [Test]
        public void ShouldContainNewValueInFileStorage()
        {
			Assert.AreEqual("value", fileStorage.GetValue("key"));
        }
    }

    public class WhenKeyAlreadyExists : FileStorageTests
    {
        protected override void Given()
        {
            fileStorage = new FileStorage();
            fileStorage.SetValue("key1", "value");
        }

        protected override void When()
        {
            fileStorage.SetValue("key1", "newValue");
        }

        [Test]
        public void ShouldRetainExistingKeyInFileStorage()
        {
            Assert.IsTrue(fileStorage.HasKey("key1"));
        }

        [Test]
        public void ShouldOverwriteValueInFileStorage()
        {
            Assert.AreEqual("newValue", fileStorage.GetValue("key1"));
        }
    }

    public class WhenKeyIsBeingDeleted : FileStorageTests
    {
        protected override void Given()
        {
            fileStorage = new FileStorage();
            fileStorage.SetValue("key2", "value");
        }

        protected override void When()
        {
            fileStorage.DeleteKey("key2");
        }

        [Test]
        public void ShouldNotHaveDeletedKeyInFileStorage()
        {
            Assert.IsFalse(fileStorage.HasKey("key2"));
        }
    }

    public class WhenKeyToBeDeletedDoesntExist : FileStorageTests
    {
        protected override void Given()
        {
            fileStorage = new FileStorage();
        }

        protected override void When()
        {
            fileStorage.DeleteKey("key3");
        }

        [Test]
        public void ShouldNotHaveKeyInFileStorage()
        {
            Assert.IsFalse(fileStorage.HasKey("key3"));
        }
    }

    public class WhenStorageClearIsCalled : FileStorageTests
    {
        protected override void Given()
        {
            fileStorage = new FileStorage();
            fileStorage.SetValue("key", "value");
            fileStorage.SetValue("key1", "value");
        }

        protected override void When()
        {
            fileStorage.Clear();
        }

        [Test]
        public void ShouldNotHaveAnyKeyInFile()
        {
            Assert.IsFalse(fileStorage.HasKey("key"));
        }

        [Test]
        public void ShouldNotHaveAnyKey1InFile()
        {
            Assert.IsFalse(fileStorage.HasKey("key1"));
        }
    }
}
