﻿using System;

namespace myGovID.Test
{
    public abstract class TestSpecification
    {
        protected TestSpecification()
        {
            Setup();
        }
        protected void Setup()
        {
            try
            {
                Given();
                When();
            }
            catch(Exception ex)
            {
                // TODO: Add an Assert.Fail
            }
        }
        protected abstract void Given();
        protected abstract void When();
    }
}
