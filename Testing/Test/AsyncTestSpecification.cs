﻿using System.Threading.Tasks;

namespace myGovID.Test
{
    public abstract class AsyncTestSpecification
    {
        protected AsyncTestSpecification()
        {
            Task setup = SetupAsync();
            setup.Wait();
        }
        protected async Task SetupAsync()
        {
            Given();
            await WhenAsync();
        }
        protected abstract void Given();
        protected abstract Task WhenAsync();
    }
}