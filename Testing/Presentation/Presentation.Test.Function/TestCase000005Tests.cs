﻿using NUnit.Framework;
using myGovID.Presentation.Steps;
using System.Linq;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;

namespace myGovID.Presentation.Test.Function.TestCase000005
{
    [TestFixture()]
    public class TestCase000005NavigateToVersionUpgrade : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000005";
        }
        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
        }

        [Test()]
        public void ShouldHaveNonNullCurrentStep()
        {
            Assert.NotNull(CurrentStep);
        }

        [Test()]
        public void ShouldHaveAccountSetupStep()
        {
            Assert.IsTrue(CurrentStep is AccountSetupStep);
        }

        [Test()]
        public void ShouldHaveVersionUpgradeStep()
        {
            Assert.IsTrue(CurrentStep.ChildStep is VersionUpgradeStep);
        }

        [Test()]
        public void ShouldHaveForcedVersionUpgradeStep()
        {
            VersionUpgradeStep upgradeStep = CurrentStep.ChildStep as VersionUpgradeStep;
            Assert.IsTrue(upgradeStep.StepViewModel.ForcedUpdate);
        }

        [Test()]
        public void ShouldHaveUpgradeActionOnly()
        {
            VersionUpgradeStep upgradeStep = CurrentStep.ChildStep as VersionUpgradeStep;
            Assert.AreEqual(1, upgradeStep.Actions.Count());
            Assert.IsNotNull(upgradeStep.Actions.First((arg) => arg.Type == ActionType.Next));
        }

        [Test()]
        public void ShouldNotHavePageLevelMessages()
        {
            Assert.IsNull(CurrentStep.Messages);
        }
    }
}
