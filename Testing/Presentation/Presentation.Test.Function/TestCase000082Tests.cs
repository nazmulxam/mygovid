﻿using System;
using myGovID.Presentation.Contract.Steps.RecoverableDeadEnd;
using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000077;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000082
{
    public class TestCase000082Passport422 : SubmitPassportSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000082";
        }

        [Test()]
        public void ShouldBeInRecoverableDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(RecoverableDeadEndErrorStep)));
        }

        [Test()]
        public void ShouldNotPopulateErrorObject()
        {
            Assert.IsNull(CodedError);
        }

        [Test()]
        public void ShouldHaveErrorMessage()
        {
            RecoverableDeadEndErrorStep recoverableDeadendErrorStep = CurrentStep as RecoverableDeadEndErrorStep;
            RecoverableDeadEndViewModel model = recoverableDeadendErrorStep.ViewModel as RecoverableDeadEndViewModel;
            Assert.AreEqual("Could not verify details", model.Exception.Title);
        }

    }
}
