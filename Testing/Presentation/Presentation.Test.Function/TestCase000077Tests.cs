﻿using System.Linq;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.AccountSetup;
using myGovID.Presentation.Contract.Steps.Dashboard;
using myGovID.Presentation.Contract.Steps.Passport;
using myGovID.Presentation.Steps;
using myGovID.Presentation.Steps.Passport;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000077
{

    public abstract class SubmitPassportSpecification : FlowTestSpecification
    {
        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
            await GetNextStepAsync(AccountSetupAction.Register);
            await GetNextStepAsync();
            await GetNextStepAsync();

            CaptureEmailStep captureEmailStep = CurrentStep as CaptureEmailStep;
            captureEmailStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.EmailAddress).FromString("k@k.com");
            await GetNextStepAsync();

            CaptureEmailVerificationStep captureEmailVerificationStep = CurrentStep as CaptureEmailVerificationStep;
            captureEmailVerificationStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.VerificationCode)
                                        .FromString("111111");
            await GetNextStepAsync(); //secure your account
            await GetNextStepAsync(); //capture password
            CapturePasswordStep capturePasswordStep = CurrentStep as CapturePasswordStep;
            capturePasswordStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.Password).FromString("Password1!");
            capturePasswordStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.ConfirmPassword).FromString("Password1!");
            await GetNextStepAsync();
            CapturePersonalDetailsStep capturePersonalDetailsStep = CurrentStep as CapturePersonalDetailsStep;
            capturePersonalDetailsStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.GivenName).FromString("Shane");
            capturePersonalDetailsStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.FamilyName).FromString("Foreman");
            capturePersonalDetailsStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.DateOfBirth).FromString("18/01/1982");
            await GetNextStepAsync();
            await GetNextStepAsync();
            DashboardStep dashboardStep = CurrentStep as DashboardStep;
            await GetNextStepAsync(DashboardAction.Passport);
            PassportOCRIntroStep ocrIntroStep = CurrentStep as PassportOCRIntroStep;
            await GetNextStepAsync(PassportOCRIntroAction.ManualEntry);
            CapturePassportDetailsStep passportStep = CurrentStep as CapturePassportDetailsStep;
            passportStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.GivenName).FromString("Shane");
            passportStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.FamilyName).FromString("Foreman");
            passportStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.PassportDocumentNumber).FromString("PA1234567");
            passportStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.Gender).FromString("Male");
            passportStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.DateOfBirth).FromString("12/12/2000");
            passportStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.Consent).FromString("1");
            await GetNextStepAsync(CapturePassportDetailsAction.Submit);
        }
    }

    public class TestCase000077Passport400 : SubmitPassportSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000077";
        }

        [Test()]
        public void ShouldBeInRecoverableDeadendErrorStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<RecoverableDeadEndErrorStep>());
        }

        [Test()]
        public void ShouldMapServerErrorMessage()
        {
            var step = CurrentStep as RecoverableDeadEndErrorStep;
            Assert.That(step, Is.Not.Null, "CurrentStep as RecoverableDeadEndErrorStep");

            var exception = step.StepViewModel.Exception;
            Assert.That(exception, Is.Not.Null, "Exception");

            Assert.Multiple(() =>
            {
                Assert.That(exception.Code, Is.EqualTo("POI31091 (400)"));
                Assert.That(exception.Title, Is.EqualTo("An error has occurred"));
                Assert.That(exception.Description, Is.EqualTo("<p>An unexpected error has occurred while processing your request. <a href=‘https://www.ato.gov.au/ausidaccess’ target=‘_blank’ >Contact us</a> if you require any assistance.</p><p><b>Error code POI31091 (400)</b></p>"));
            });
        }
    }
}