﻿using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000077;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000091
{
    public class TestCase000091PassportSYS000002 : SubmitPassportSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000091";
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(DeadEndStep)));
        }

        [Test()]
        public void ShouldHaveCorrectError()
        {
            Assert.IsNotNull(CodedError, "Error thrown");
            Assert.Multiple(() =>
            {
                Assert.AreEqual("SYS000002", CodedError.Code, "Error Code ");
                Assert.AreEqual("System error", CodedError.Title, "Error Title");
            });
        }
    }
}