﻿using System.Linq;using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Steps;
using NUnit.Framework;
using myGovID.Presentation.Contract.Steps.AccountSetup;
namespace myGovID.Presentation.Test.Function.TestCase000049
{
    public class TestCase000049SubmitVerificationCode : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000049";
        }
        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
            await GetNextStepAsync(AccountSetupAction.Register);
            await GetNextStepAsync();
            await GetNextStepAsync();
            
            CaptureEmailStep captureEmailStep = CurrentStep as CaptureEmailStep;
            captureEmailStep.StepViewModel.Inputs.First<IInput>(input => input.MetaData == InputMetaData.EmailAddress).FromString("k@k.com");
            await GetNextStepAsync();
            
            CaptureEmailVerificationStep captureEmailVerificationStep = CurrentStep as CaptureEmailVerificationStep;
            captureEmailVerificationStep.StepViewModel.Inputs.First<IInput>(input => input.MetaData == InputMetaData.VerificationCode)
                                        .FromString("111111");
            await GetNextStepAsync();
            
        }

        [Test()]
        public void ShouldHaveNonNullCurrentStep()
        {
            Assert.NotNull(CurrentStep);
        }
     
        [Test()]
        public void ShouldHaveCaptureEmailVerificationStep()
        {
            Assert.IsTrue(CurrentStep is CaptureEmailVerificationStep);
        }

        [Test()]
        public void ShouldHaveMaxAttemptMessage()
        {
            Assert.IsNotNull(CurrentStep.Messages);
            Assert.IsTrue(CurrentStep.Messages.Any());
            PageMessage message = CurrentStep.Messages.First();
            Assert.AreEqual("POI31091 (422)", message.Code);
            Assert.AreEqual("<p>The code provided does not match. Try again.<b>You have 4 attempts remaining.</b></p>", message.Description);
            Assert.AreEqual("Could not verify your email", message.Title);
        }
    }
}

