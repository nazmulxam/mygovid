﻿using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000077;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000078
{
    public class TestCase000078PassportFIM0000018 : SubmitPassportSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000078";
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(DeadEndStep)));
        }

        [Test()]
        public void ShouldPopulateErrorObject()
        {
            Assert.IsNotNull(CodedError);
        }

        [Test()]
        public void ShouldHaveFIM000018Error()
        {
            Assert.AreEqual("FIM000018", CodedError.Code);
        }

    }
}