﻿using System.Linq;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000144MedicareFailedAttempt
{
    public class ServerResponseFailedAttempt : MedicareFlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000144";
        }

        protected override async Task GivenAsync()
        {
            await UserIsOnCardInputScreenViaManualEntryAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserProvidesCorrectCardDetailsAsync();
        }

        [Test()]
        public void ShouldBeInMedicareCardInputStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<MedicareCardInputStep>());
        }

        [Test()]
        public void ShouldHaveCorrectPageMessages()
        {
            Assert.IsNotNull(CurrentStep.Messages, "Messages");
            Assert.AreEqual(1, CurrentStep.Messages.Count(), "Message Count");
            Assert.Multiple(() =>
            {
                PageMessage message = CurrentStep.Messages.First();
                Assert.That(message.Code, Is.EqualTo("POI31091"), "Message Code");
                Assert.That(message.Title, Is.EqualTo("An error has occurred"), "Message Title");
                Assert.That(message.Description, Is.EqualTo("<p>Failed to verify. You have 1 attempt remaining.</p><p><b>Error code POI31091 (400)</b></p>"), "Message Description");
                Assert.AreEqual("POI31091", message.Code, "Message Code");
            });
        }
    }
}