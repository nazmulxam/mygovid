using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Steps;
using myGovID.Services.Api.Contract.Model.Poi.MedicareDetails;
using Newtonsoft.Json;
using NUnit.Framework;
using myGovID.Presentation.Steps.DocumentIdentitySuccess;

namespace myGovID.Presentation.Test.Function.TestCase000131MedicareHappyFlow
{
    public abstract class MedicareSuccessBase : MedicareFlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000131";
        }
    }

    #region Intro Screen Tests

    public class IntroScreenManualEntryTest : MedicareSuccessBase
    {
        protected override async Task GivenAsync()
        {
            await UserIsOnMedicareIntroScreenAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserSelectsManualEntryAsync();
        }

        [Test]
        public void ShouldShowCardInputScreen()
        {
            Assert.That(CurrentStep, Is.TypeOf<MedicareCardInputStep>());
        }
    }

    public class IntroScreenScanDetailsTest : MedicareSuccessBase
    {
        protected override async Task GivenAsync()
        {
            await UserIsOnMedicareIntroScreenAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserSelectsScanDetailsAsync();
        }

        [Test]
        public void ShouldShowOcrScreen()
        {
            Assert.That(CurrentStep, Is.TypeOf<MedicareOcrStep>());
        }
    }

    public class IntroScreenBackButtonTest : MedicareSuccessBase
    {
        protected override async Task GivenAsync()
        {
            await UserIsOnMedicareIntroScreenAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserSelectsBackAsync();
        }

        [Test]
        public void ShouldShowDashboard()
        {
            Assert.That(CurrentStep, Is.TypeOf<DashboardStep>());
        }
    }

    #endregion

    #region OCR screen tests

    public class OcrScreenEnterManuallyTest : MedicareSuccessBase
    {
        protected override async Task GivenAsync()
        {
            await UserIsOnOcrScreenAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserSelectsManualEntryAsync();
        }

        [Test]
        public void ShouldShowCardInputScreen()
        {
            Assert.That(CurrentStep, Is.TypeOf<MedicareCardInputStep>());
        }
    }

    public class OcrScreenScanDetailsTest : MedicareSuccessBase
    {
        protected override async Task GivenAsync()
        {
            await UserIsOnOcrScreenAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserSelectsScanDetailsAsync();
        }

        [Test]
        public void ShouldShowCardInputScreen()
        {
            Assert.That(CurrentStep, Is.TypeOf<MedicareCardInputStep>());
        }

        [Test]
        [Ignore("Needs Medicare OCR to be implemented.")]
        public void ShouldPopulateScannedDetails()
        {
            // TODO
            Assert.Fail("Medicare OCR not yet implemented");
        }
    }

    #endregion

    #region Medicare card input screen tests

    public class CardInputViaOcrRescanTest : MedicareSuccessBase
    {
        protected override async Task GivenAsync()
        {
            await UserIsOnCardInputScreenViaOcrAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserSelectsRescan();
        }

        [Test]
        public void ShouldShowOcrScreen()
        {
            Assert.That(CurrentStep, Is.TypeOf<MedicareOcrStep>());
        }
    }

    public class CardInputSubmitSuccessTest : MedicareSuccessBase
    {
        protected override async Task GivenAsync()
        {
            await UserIsOnCardInputScreenViaManualEntryAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserProvidesCorrectCardDetailsAsync();
        }

        [Test]
        public void ShouldShowSuccessTickScreen()
        {
            Assert.That(CurrentStep, Is.TypeOf<DocumentIdentitySuccessStep>());
        }

        [Test]
        public void ShouldSubmitCorrectLinkToServer()
        {
            var request = HttpService.RequestHistory.ElementAtOrDefault(1);
            Assert.That(request, Is.Not.Null, "Request");
            Assert.Multiple(() =>
            {
                Assert.That(request.RequestUri.AbsolutePath, Is.EqualTo("/api/v1/poi/52942edb-3cd6-4ab4-8ee7-b23cd64c4430/documents/medicareDocuments"), "Location");
                Assert.That(request.Method, Is.EqualTo(HttpMethod.Post), "Method");
                //Assert.That(request.Link.AuthType, Is.EqualTo(AuthenticationType.Extension), "AuthType");
                // TODO: See if we can work out what the extension grant response is for this guy and assert on it?
            });
        }

        [Test]
        public void ShouldSubmitCorrectPayloadToServer()
        {
            var request = HttpService.RequestHistory.ElementAtOrDefault(1);
            Assert.That(request, Is.Not.Null, "Request");

            var payload = request.Content?.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MedicareDetailsRequestModel>(payload);
            Assert.Multiple(() =>
            {
                Assert.That(model.FullName1, Is.EqualTo("Doctor Strange"), "FullName1");
                Assert.That(model.FullName2, Is.Null, "FullName2");
                Assert.That(model.FullName3, Is.Null, "FullName3");
                Assert.That(model.FullName4, Is.Null, "FullName4");
                Assert.That(model.MedicareCardNumber, Is.EqualTo("5983524861"), "MedicareCardNumber");
                Assert.That(model.CardType, Is.EqualTo(MedicareCardType.Green), "CardType");
                Assert.That(model.CardExpiry, Is.EqualTo("01/2030"), "CardExpiry");
                Assert.That(model.ConsentProvided, Is.True, "ConsentProvided");
            });
        }
    }

    public class CardInputSubmitValidationErrorTest : MedicareSuccessBase
    {
        protected override async Task GivenAsync()
        {
            await UserIsOnCardInputScreenViaManualEntryAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserProvidesIncorrectCardDetailsAsync();
        }

        [Test]
        public void ShouldRemainOnInputScreen()
        {
            Assert.That(CurrentStep, Is.TypeOf<MedicareCardInputStep>());
        }

        [Test]
        public void ShouldNotSubmitToServer()
        {
            Assert.That(HttpService.RequestHistory, Is.Empty);
        }
    }

    public class CardInputBackButtonTest : MedicareSuccessBase
    {
        protected override async Task GivenAsync()
        {
            await UserIsOnCardInputScreenViaOcrAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserSelectsBackAsync();
        }

        [Test]
        public void ShouldShowOcrScreen()
        {
            Assert.That(CurrentStep, Is.TypeOf<MedicareOcrStep>());
        }
    }

    #endregion

    #region Medicare card success tick test

    public class SuccessTickCompletedTest : MedicareSuccessBase
    {
        protected override async Task GivenAsync()
        {
            await UserIsOnSuccessTickScreenAsync();
        }

        protected override async Task WhenAsync()
        {
            await SuccessTickCompletes();
        }

        [Test]
        public void ShouldShowDashboardScreen()
        {
            Assert.That(CurrentStep, Is.TypeOf<DashboardStep>());
        }
    }


    #endregion
}
