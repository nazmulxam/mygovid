﻿using myGovID.Presentation.Contract.Steps.RecoverableDeadEnd;
using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000120;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000112
{
    [Ignore("This will be modified after failed attempt edge case discussion.")]
    public class TestCase000112DriverLicenceSystemUnavailable : SubmitDriverLicenceSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000112";
        }

        [Test()]
        public void ShouldBeInRecoverableDeadendStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(RecoverableDeadEndErrorStep)));
        }

        [Test()]
        public void ShouldPopulateErrorObject()
        {
            Assert.IsNull(CodedError);
        }

        [Test()]
        public void ShouldHaveUnableToVerifyError()
        {
            RecoverableDeadEndErrorStep recoverableDeadendErrorStep = CurrentStep as RecoverableDeadEndErrorStep;
            RecoverableDeadEndViewModel model = recoverableDeadendErrorStep.ViewModel as RecoverableDeadEndViewModel;
            Assert.AreEqual("POI11000 (500)", model.Exception.Code);
            Assert.AreEqual("System unavailable", model.Exception.Title);
        }
    }
}