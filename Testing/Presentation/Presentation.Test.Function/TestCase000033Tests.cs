﻿using System.Linq;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Authenticate;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000033
{
    [TestFixture()]
    public class TestCase000033ResumePOI404 : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000033";
        }
        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
            AuthenticateStep authenticateStep = CurrentStep as AuthenticateStep;
            authenticateStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.Password).FromString("Password!1");
            await GetNextStepAsync(AuthenticateAction.Password);
        }

        [Test()]
        public void ShouldBeInRecoverableDeadendErrorStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<RecoverableDeadEndErrorStep>());
        }

        [Test()]
        public void ShouldMapServerErrorMessage()
        {
            var step = CurrentStep as RecoverableDeadEndErrorStep;
            Assert.That(step, Is.Not.Null, "CurrentStep as RecoverableDeadEndErrorStep");

            var exception = step.StepViewModel.Exception;
            Assert.That(exception, Is.Not.Null, "Exception");

            Assert.Multiple(() =>
            {
                Assert.That(exception.Code, Is.EqualTo("SYS000001"));
                Assert.That(exception.Title, Is.EqualTo("An error has occurred"));
                // TODO: Confirm this, it's not currently possible to get the HTTP status code
                //Assert.That(exception.Description, Is.EqualTo("Http status code: 404"));
            });
        }
    }
}