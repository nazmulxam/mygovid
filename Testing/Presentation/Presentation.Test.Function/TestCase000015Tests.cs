﻿using NUnit.Framework;
using myGovID.Presentation.Steps;
using System.Linq;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;

namespace myGovID.Presentation.Test.Function.TestCase000015
{
    public class TestCase000015VersionUpgradeDenied : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000015";
        }
        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
            VersionUpgradeStep upgradeStep = CurrentStep.ChildStep as VersionUpgradeStep;
            CurrentStep = await upgradeStep.Actions.First(action => action.Type == ActionType.Previous)
                             .Run();
        }

        [Test()]
        public void ShouldHaveNonNullCurrentStep()
        {
            Assert.NotNull(CurrentStep);
        }

        [Test()]
        public void ShouldHaveAccountSetupStep()
        {
            Assert.IsTrue(CurrentStep is AccountSetupStep);
        }

        [Test()]
        public void ShouldHaveVersionUpgradeStep()
        {
            Assert.IsNull(CurrentStep.ChildStep);
        }

        [Test()]
        public void ShouldNotHavePageLevelMessages()
        {
            Assert.IsNull(CurrentStep.Messages);
        }
    }
}
