﻿using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000120;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000121
{
    public class TestCase000121DriverLicenceFIM000016 : SubmitDriverLicenceSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000121";
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(DeadEndStep)));
        }

        [Test()]
        public void ShouldPopulateErrorObject()
        {
            Assert.IsNotNull(CodedError);
        }

        [Test()]
        public void ShouldHaveFIM000016Error()
        {
            Assert.AreEqual("FIM000016", CodedError.Code);
        }

    }
}