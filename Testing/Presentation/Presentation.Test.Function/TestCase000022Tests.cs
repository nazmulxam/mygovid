﻿using System.Threading.Tasks;
using myGovID.Presentation.Contract.Steps.AccountSetup;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000022
{
    public class TestCase000022TestsStartPoiProcessIdIsMissing : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000022";
        }
        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
            await GetNextStepAsync(AccountSetupAction.Register);
            await GetNextStepAsync(); // T&C
            await GetNextStepAsync(); // Deadend
        }

        [Test()]
        public void ShouldPopulateErrorObject()
        {
            Assert.IsNotNull(CodedError);
        }

        [Test()]
        public void ShouldHaveFIM000001Error()
        {
            Assert.AreEqual("FIM000001", CodedError.Code);
        }
    }
}