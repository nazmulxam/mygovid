﻿using System.Threading.Tasks;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000132MedicareFIM000020
{
    public class ServerResponseUnknownTargetRel : MedicareFlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000132";
        }

        protected override async Task GivenAsync()
        {
            await UserIsOnCardInputScreenViaManualEntryAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserProvidesCorrectCardDetailsAsync();
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<DeadEndStep>());
        }

        [Test()]
        public void ShouldHaveFIM000020Error()
        {
            Assert.AreEqual("FIM000020", CodedError?.Code);
        }
    }
}