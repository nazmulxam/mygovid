﻿using System.Threading.Tasks;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000142Medicare400
{
    public class ServerResponseStatus400 : MedicareFlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000142";
        }

        protected override async Task GivenAsync()
        {
            await UserIsOnCardInputScreenViaManualEntryAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserProvidesCorrectCardDetailsAsync();
        }

        [Test()]
        public void ShouldBeInRecoverableDeadendErrorStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<RecoverableDeadEndErrorStep>());
        }

        [Test()]
        public void ShouldMapServerErrorMessage()
        {
            var step = CurrentStep as RecoverableDeadEndErrorStep;
            Assert.That(step, Is.Not.Null, "CurrentStep as RecoverableDeadEndErrorStep");

            var exception = step.StepViewModel.Exception;
            Assert.That(exception, Is.Not.Null, "Exception");

            Assert.Multiple(() =>
            {
                Assert.That(exception.Code, Is.EqualTo("POI31091 (400)"));
                Assert.That(exception.Title, Is.EqualTo("An error has occurred"));
                Assert.That(exception.Description, Is.EqualTo("<p>An unexpected error has occurred while processing your request. <a href=‘https://www.ato.gov.au/ausidaccess’ target=‘_blank’ >Contact us</a> if you require any assistance.</p><p><b>Error code POI31091 (400)</b></p>"));
            });
        }
    }
}