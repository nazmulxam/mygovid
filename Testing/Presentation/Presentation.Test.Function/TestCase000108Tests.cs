﻿using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000120;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000108
{
    public class TestCase000108DriverLicenceFIM000017 : SubmitDriverLicenceSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000108";
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(DeadEndStep)));
        }

        [Test()]
        public void ShouldPopulateErrorObject()
        {
            Assert.IsNotNull(CodedError);
        }

        [Test()]
        public void ShouldHaveFIM000017Error()
        {
            Assert.AreEqual("FIM000017", CodedError.Code);
        }

    }
}