﻿using myGovID.Presentation.Contract.Steps.RecoverableDeadEnd;
using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000077;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000097
{
    public class TestCase000097PassportMaxAttempts : SubmitPassportSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000097";
        }

        [Test()]
        public void ShouldBeInRecoverableDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(RecoverableDeadEndErrorStep)));
        }

        [Test()]
        public void ShouldNotPopulateErrorObject()
        {
            Assert.IsNull(CodedError);
        }

        [Test()]
        public void ShouldHaveMaxAttemptsError()
        {
            RecoverableDeadEndErrorStep recoverableDeadendErrorStep = CurrentStep as RecoverableDeadEndErrorStep;
            RecoverableDeadEndViewModel model = recoverableDeadendErrorStep.ViewModel as RecoverableDeadEndViewModel;
            Assert.AreEqual("Max attempts reached", model.Exception.Title);
        }

    }
}