﻿using System.Linq;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Passport;
using myGovID.Presentation.Steps.Passport;
using myGovID.Presentation.Test.Function.TestCase000077;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000100
{
    public class TestCase000100PassportFailedAttempt : SubmitPassportSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000100";
        }

        [Test()]
        public void ShouldBeInPassportStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(CapturePassportDetailsStep)));
        }

        [Test()]
        public void ShouldHavePageMessage()
        {

            Assert.IsNotNull(CurrentStep.Messages);
            Assert.IsTrue(CurrentStep.Messages.Any());
            Assert.AreEqual(1, CurrentStep.Messages.Count());

            PageMessage message = CurrentStep.Messages.First();

            Assert.Multiple(() =>
            {
                Assert.AreEqual("POI31091", message.Code, "Code is wrong");
                Assert.AreEqual("<p>The information provided does not match the official record. Check the details you provided and try again.<b>You have 1 attempt remaining.</b></p>", message.Description, "Description is wrong");
                Assert.AreEqual("Could not verify details", message.Title, "Title is wrong");
            });
        }

    }
}