﻿using myGovID.Presentation.Contract.Steps.RecoverableDeadEnd;
using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000120;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000119
{
    public class TestCase000119DriverLicenceMaxAttemptsReached : SubmitDriverLicenceSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000119";
        }

        [Test()]
        public void ShouldBeInRecoverableDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(RecoverableDeadEndErrorStep)));
        }

        [Test()]
        public void ShouldNotPopulateErrorObject()
        {
            Assert.IsNull(CodedError);
        }

        [Test()]
        public void ShouldHaveMaxAttemptsReachedError()
        {
            RecoverableDeadEndErrorStep recoverableDeadendErrorStep = CurrentStep as RecoverableDeadEndErrorStep;
            RecoverableDeadEndViewModel model = recoverableDeadendErrorStep.ViewModel as RecoverableDeadEndViewModel;
            Assert.AreEqual("POI11000", model.Exception.Code);
            Assert.AreEqual("Max attempts reached", model.Exception.Title);
        }

    }
}