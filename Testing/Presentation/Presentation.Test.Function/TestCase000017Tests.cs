﻿using System.Threading.Tasks;
using myGovID.Presentation.Contract.Steps.AccountSetup;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000017
{
    public class TestCase000017NavigateToTermsAndConditionsStep : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000017";
        }
        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
            await GetNextStepAsync(AccountSetupAction.Register);
            await GetNextStepAsync();
        }

        [Test()]
        public void ShouldBeInRecoverableDeadendErrorStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<RecoverableDeadEndErrorStep>());
        }

        [Test()]
        public void ShouldMapServerErrorMessage()
        {
            var step = CurrentStep as RecoverableDeadEndErrorStep;
            Assert.That(step, Is.Not.Null, "CurrentStep as RecoverableDeadEndErrorStep");

            var exception = step.StepViewModel.Exception;
            Assert.That(exception, Is.Not.Null, "Exception");

            Assert.Multiple(() =>
            {
                Assert.That(exception.Code, Is.EqualTo("POI11000 (500)"));
                Assert.That(exception.Title, Is.EqualTo("System unavailable"));
                Assert.That(exception.Description, Is.EqualTo("<p>We are unable to process your request at this time. Try again in 10 minutes</p><p>If the issue persists, contact us on <br><b>13 28 61</b></p><p><b>Error code POI11000 (500)</b></p>"));
            });
        }
    }
}
