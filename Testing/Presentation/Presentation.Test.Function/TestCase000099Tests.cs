﻿using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000077;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000099
{
    public class TestCase000099PassportSYS00006 : SubmitPassportSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000099";
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(DeadEndStep)));
        }

        [Test()]
        public void ShouldPopulateErrorObject()
        {
            Assert.IsNotNull(CodedError);
        }

        [Test()]
        public void ShouldHaveSYS000006Error()
        {
            Assert.AreEqual("SYS000006", CodedError.Code);
        }

    }
}