﻿using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000077;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000086
{
    public class TestCase000086PassportFIM000017 : SubmitPassportSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000086";
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(DeadEndStep)));
        }

        [Test()]
        public void ShouldPopulateErrorObject()
        {
            Assert.IsNotNull(CodedError);
        }

        [Test()]
        public void ShouldHaveFIM000017Error()
        {
            Assert.AreEqual("FIM000017", CodedError.Code);
        }

    }
}