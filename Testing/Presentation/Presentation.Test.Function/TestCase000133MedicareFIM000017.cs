﻿using System.Threading.Tasks;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000133MedicareFIM000017
{
    public class ServerResponseLinkAuthenticationMissing : MedicareFlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000133";
        }

        protected override async Task GivenAsync()
        {
            await UserIsOnCardInputScreenViaManualEntryAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserProvidesCorrectCardDetailsAsync();
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<DeadEndStep>());
        }

        [Test()]
        public void ShouldHaveFIM000017Error()
        {
            Assert.AreEqual("FIM000017", CodedError?.Code);
        }
    }
}