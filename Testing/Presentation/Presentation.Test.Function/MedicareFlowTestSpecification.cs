﻿using System.Linq;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.AccountSetup;
using myGovID.Presentation.Contract.Steps.Dashboard;
using myGovID.Presentation.Contract.Steps.MedicareCardInput;
using myGovID.Presentation.Contract.Steps.MedicareOcrIntroStep;
using myGovID.Presentation.Contract.Steps.MedicareOcrStep;
using myGovID.Presentation.Contract.Steps.Passport;
using myGovID.Presentation.Steps;
using myGovID.Presentation.Steps.Passport;

namespace myGovID.Presentation.Test.Function
{
    public abstract class MedicareFlowTestSpecification : FlowTestSpecification
    {
        protected override void Given()
        {
            base.Given();
            GivenAsync().Wait();
            HttpService.RequestHistory.Clear();
        }

        protected abstract Task GivenAsync();

        protected async Task UserIsOnMedicareIntroScreenAsync()
        {
            await GetInitialStepAsync();
            await GetNextStepAsync(AccountSetupAction.Register);
            await GetNextStepAsync();
            await GetNextStepAsync();

            CaptureEmailStep captureEmailStep = CurrentStep as CaptureEmailStep;
            captureEmailStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.EmailAddress).FromString("k@k.com");
            await GetNextStepAsync();

            CaptureEmailVerificationStep captureEmailVerificationStep = CurrentStep as CaptureEmailVerificationStep;
            captureEmailVerificationStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.VerificationCode)
                                        .FromString("111111");
            await GetNextStepAsync(); //secure your account
            await GetNextStepAsync(); //capture password

            CapturePasswordStep capturePasswordStep = CurrentStep as CapturePasswordStep;
            capturePasswordStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.Password).FromString("Password1!");
            capturePasswordStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.ConfirmPassword).FromString("Password1!");
            await GetNextStepAsync();

            CapturePersonalDetailsStep capturePersonalDetailsStep = CurrentStep as CapturePersonalDetailsStep;
            capturePersonalDetailsStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.GivenName).FromString("Shane");
            capturePersonalDetailsStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.FamilyName).FromString("Foreman");
            capturePersonalDetailsStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.DateOfBirth).FromString("18/01/1982");
            await GetNextStepAsync();
            await GetNextStepAsync();

            // Dashboard
            await GetNextStepAsync(DashboardAction.Passport);

            // OCR Intro
            await GetNextStepAsync(PassportOCRIntroAction.ManualEntry);

            // Capture
            var passportStep = CurrentStep as CapturePassportDetailsStep;
            passportStep.StepViewModel.GivenName.Value = "Shane";
            passportStep.StepViewModel.FamilyName.Value = "Foreman";
            passportStep.StepViewModel.PassportNumber.Value = "A0000001";
            passportStep.StepViewModel.Gender.Value = "Male";
            passportStep.StepViewModel.ConsentProvided.Value = true;
            passportStep.StepViewModel.DateOfBirth.Value = "18/01/1982";
            await GetNextStepAsync(CapturePassportDetailsAction.Submit);

            // Success tick
            await GetNextStepAsync();

            // Dashboard
            await GetNextStepAsync(DashboardAction.Medicare);
        }

        protected async Task UserSelectsManualEntryAsync()
        {
            if (CurrentStep is MedicareOcrIntroStep)
            {
                await GetNextStepAsync(MedicareOcrIntroAction.EnterManually);
            }
            else if (CurrentStep is MedicareOcrStep)
            {
                await GetNextStepAsync(MedicareOcrAction.EnterManually);
            }
        }

        protected async Task UserSelectsScanDetailsAsync()
        {
            if (CurrentStep is MedicareOcrIntroStep)
            {
                await GetNextStepAsync(MedicareOcrIntroAction.ScanDetails);
            }
            else if (CurrentStep is MedicareOcrStep)
            {
                await GetNextStepAsync(MedicareOcrAction.PerformScan);
            }
        }

        protected async Task UserSelectsBackAsync()
        {
            if (CurrentStep is MedicareOcrIntroStep)
            {
                await GetPreviousStepAsync(MedicareOcrIntroAction.Back);
            }
            else if (CurrentStep is MedicareCardInputStep)
            {
                await GetPreviousStepAsync(MedicareCardInputAction.Back);
            }
        }

        protected async Task UserIsOnCardInputScreenViaManualEntryAsync()
        {
            await UserIsOnMedicareIntroScreenAsync();
            await UserSelectsManualEntryAsync();
        }

        protected async Task UserIsOnOcrScreenAsync()
        {
            await UserIsOnMedicareIntroScreenAsync();
            await UserSelectsScanDetailsAsync();
        }

        protected async Task UserIsOnCardInputScreenViaOcrAsync()
        {
            await UserIsOnOcrScreenAsync();
            await UserSelectsScanDetailsAsync();
        }

        protected async Task UserIsOnSuccessTickScreenAsync()
        {
            await UserIsOnCardInputScreenViaManualEntryAsync();
            await UserProvidesCorrectCardDetailsAsync();
        }

        protected async Task UserSelectsRescan()
        {
            await GetPreviousStepAsync(MedicareCardInputAction.Rescan);
        }

        protected async Task UserProvidesCorrectCardDetailsAsync()
        {
            var medicareInputStep = CurrentStep as MedicareCardInputStep;
            medicareInputStep.StepViewModel.CardNumber.Value = "5983524861";
            medicareInputStep.StepViewModel.CardType.Value = MedicareCardType.Green;
            medicareInputStep.StepViewModel.ConsentProvided.Value = true;
            medicareInputStep.StepViewModel.Expiry.Value = "01/2030";
            medicareInputStep.StepViewModel.Irn.Value = "1";
            medicareInputStep.StepViewModel.NameLine1.Value = "Doctor Strange";

            await GetNextStepAsync(MedicareCardInputAction.Submit);
        }

        protected async Task UserProvidesIncorrectCardDetailsAsync()
        {
            var medicareInputStep = CurrentStep as MedicareCardInputStep;
            medicareInputStep.StepViewModel.CardType.Value = "Wrong";
            await GetNextStepAsync(MedicareCardInputAction.Submit);
        }

        protected async Task SuccessTickCompletes()
        {
            await GetNextStepAsync();
        }
    }
}
