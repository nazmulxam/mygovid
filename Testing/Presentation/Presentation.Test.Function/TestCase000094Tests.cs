﻿using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000077;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000094
{
    public class TestCase000094PassportFIM000015 : SubmitPassportSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000094";
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(DeadEndStep)));
        }

        [Test()]
        public void ShouldPopulateErrorObject()
        {
            Assert.IsNotNull(CodedError);
        }

        [Test()]
        public void ShouldHaveFIM000015Error()
        {
            Assert.AreEqual("FIM000015", CodedError.Code);
        }

    }
}