﻿using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000077;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000087
{
    public class TestCase000087PassportFIM000019 : SubmitPassportSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000087";
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(DeadEndStep)));
        }

        [Test()]
        public void ShouldPopulateErrorObject()
        {
            Assert.IsNotNull(CodedError);
        }

        [Test()]
        public void ShouldHaveFIM000019Error()
        {
            Assert.AreEqual("FIM000019", CodedError.Code);
        }

    }
}