﻿using System.Threading.Tasks;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000127MedicareTaskTimeout
{
    public class ServerResponseTaskTimeout : MedicareFlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000127";
        }

        protected override async Task GivenAsync()
        {
            await UserIsOnCardInputScreenViaManualEntryAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserProvidesCorrectCardDetailsAsync();
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<DeadEndStep>());
        }

        [Test()]
        public void ShouldHaveSYS000003Error()
        {
            Assert.AreEqual("SYS000003", CodedError?.Code);
        }
    }
}