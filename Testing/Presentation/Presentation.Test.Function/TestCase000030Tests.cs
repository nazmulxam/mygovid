﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Authenticate;
using myGovID.Presentation.Contract.Steps.Dashboard.Document;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000030
{
    [TestFixture()]
    public class TestCase000030ResumePOIIP2 : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000030";
        }
        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
            AuthenticateStep authenticateStep = CurrentStep as AuthenticateStep;
            authenticateStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.Password).FromString("Password!1");
            await GetNextStepAsync(AuthenticateAction.Password); // DashboardStep
        }

        [Test()]
        public void ShouldHaveNonNullCurrentStep()
        {
            Assert.NotNull(CurrentStep);
        }

        [Test()]
        public void ShouldHaveDashboardStep()
        {
            Assert.IsTrue(CurrentStep is DashboardStep);
        }

        [Test()]
        public void ShouldNotHavePageLevelMessages()
        {
            Assert.IsNull(CurrentStep.Messages);
        }

        [Test()]
        public void ShouldHaveNoDocuments()
        {
            DashboardStep dashboardStep = CurrentStep as DashboardStep;
            IEnumerable<IDocument> documents = dashboardStep.StepViewModel.Documents;
            Assert.AreEqual(0, documents.Count());
        }
    }
}