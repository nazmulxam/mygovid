﻿using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000077;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000089
{
    public class TestCase000089Passport404 : SubmitPassportSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000089";
        }

        [Test()]
        public void ShouldBeInRecoverableDeadendErrorStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<RecoverableDeadEndErrorStep>());
        }

        [Test()]
        public void ShouldMapServerErrorMessage()
        {
            var step = CurrentStep as RecoverableDeadEndErrorStep;
            Assert.That(step, Is.Not.Null, "CurrentStep as RecoverableDeadEndErrorStep");

            var exception = step.StepViewModel.Exception;
            Assert.That(exception, Is.Not.Null, "Exception");

            Assert.Multiple(() =>
            {
                Assert.That(exception.Code, Is.EqualTo("SYS000001"));
                Assert.That(exception.Title, Is.EqualTo("An error has occurred"));
                // TODO: Confirm this, it's not currently possible to get the HTTP status code
                //Assert.That(exception.Description, Is.EqualTo("Http status code: 404"));
            });
        }
    }
}