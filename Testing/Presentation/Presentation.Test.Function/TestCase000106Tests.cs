﻿using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000120;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000106
{
    public class TestCase000106DriverLicenceSYS000002 : SubmitDriverLicenceSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000106";
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(DeadEndStep)));
        }

        [Test()]
        public void ShouldHaveCorrectError()
        {
            Assert.IsNotNull(CodedError, "Error thrown");
            Assert.Multiple(() =>
            {
                Assert.AreEqual("SYS000002", CodedError.Code, "Error Code ");
                Assert.AreEqual("System error", CodedError.Title, "Error Title");
            });
        }
    }
}