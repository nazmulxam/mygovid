﻿using System.Linq;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Steps;
using NUnit.Framework;
using myGovID.Presentation.Contract.Steps.AccountSetup;
namespace myGovID.Presentation.Test.Function.TestCase000067
{
    public class TestCase000067CredTechnicalError : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000067";
        }
        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
            await GetNextStepAsync(AccountSetupAction.Register);
            await GetNextStepAsync();
            await GetNextStepAsync();

            CaptureEmailStep captureEmailStep = CurrentStep as CaptureEmailStep;
            captureEmailStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.EmailAddress).FromString("k@k.com");
            await GetNextStepAsync();

            CaptureEmailVerificationStep captureEmailVerificationStep = CurrentStep as CaptureEmailVerificationStep;
            captureEmailVerificationStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.VerificationCode)
                                        .FromString("111111");
            await GetNextStepAsync(); //Deadend step
        }

        [Test()]
        public void ShouldHaveDeadEndStep()
        {
            Assert.True(CurrentStep is DeadEndStep);
        }

        [Test()]
        public void ShouldHaveCorrectError()
        {
            Assert.IsNotNull(CodedError, "Error thrown");
            Assert.Multiple(() =>
            {
                Assert.AreEqual("SYS000002", CodedError.Code, "Error Code ");
                Assert.AreEqual("System error", CodedError.Title, "Error Title");
            });
        }
    }
}

