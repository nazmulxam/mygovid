﻿using System.Threading.Tasks;
using myGovID.Presentation.Steps;
using NUnit.Framework;
using myGovID.Presentation.Test.Function;

namespace myGovID.Presentation.Test.Function.TestCase000135Medicare422
{
    public class ServerResponseStatus422 : MedicareFlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000135";
        }

        protected override async Task GivenAsync()
        {
            await UserIsOnCardInputScreenViaManualEntryAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserProvidesCorrectCardDetailsAsync();
        }

        [Test()]
        public void ShouldBeInRecoverableDeadendErrorStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<RecoverableDeadEndErrorStep>());
        }

        [Test()]
        public void ShouldMapServerErrorMessage()
        {
            var step = CurrentStep as RecoverableDeadEndErrorStep;
            Assert.That(step, Is.Not.Null, "CurrentStep as RecoverableDeadEndErrorStep");

            var exception = step.StepViewModel.Exception;
            Assert.That(exception, Is.Not.Null, "Exception");

            Assert.Multiple(() =>
            {
                Assert.That(exception.Code, Is.EqualTo("45033 (422)"));
                Assert.That(exception.Title, Is.EqualTo("Could not verify details"));
                Assert.That(exception.Description, Is.EqualTo("<p>Your document cannot be verified at this time due to a system error. Try again.</p>"));
            });
        }
    }
}