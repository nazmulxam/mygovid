﻿using System.Threading.Tasks;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000128MedicarePOI31091
{
    public class ServerResponseTaskFailedPOI31091 : MedicareFlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000128";
        }

        protected override async Task GivenAsync()
        {
            await UserIsOnCardInputScreenViaManualEntryAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserProvidesCorrectCardDetailsAsync();
        }

        [Test()]
        public void ShouldBeInRecoverableDeadendErrorStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<RecoverableDeadEndErrorStep>());
        }

        [Test()]
        public void ShouldMapServerErrorMessage()
        {
            var step = CurrentStep as RecoverableDeadEndErrorStep;
            Assert.That(step, Is.Not.Null, "CurrentStep as RecoverableDeadEndErrorStep");

            var exception = step.StepViewModel.Exception;
            Assert.That(exception, Is.Not.Null, "Exception");

            Assert.Multiple(() =>
            {
                Assert.That(exception.Code, Is.EqualTo("POI31091"));
                Assert.That(exception.Title, Is.EqualTo("Unable to verify"));
                Assert.That(exception.Description, Is.EqualTo("<p>We are unable to verify your identity at this time.</p><p><b>Error code POI31091 (400)</b></p>"));
            });
        }
    }
}