﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Authenticate;
using myGovID.Presentation.Contract.Steps.Dashboard.Document;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000026
{
    public class TestCase000026TestsResumePoiMedicare : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000026";
        }
        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
            AuthenticateStep authenticateStep = CurrentStep as AuthenticateStep;
            authenticateStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.Password).FromString("Password!1");
            await GetNextStepAsync(AuthenticateAction.Password); // Dashboard Step
        }

        [Test()]
        public void ShouldHaveNonNullCurrentStep()
        {
            Assert.NotNull(CurrentStep);
        }

        [Test()]
        public void ShouldHaveDashboardStep()
        {
            Assert.IsTrue(CurrentStep is DashboardStep);
        }

        [Test()]
        public void ShouldNotHavePageLevelMessages()
        {
            Assert.IsNull(CurrentStep.Messages);
        }

        [Test()]
        public void ShouldHaveOneDocument()
        {
            DashboardStep dashboardStep = CurrentStep as DashboardStep;
            IEnumerable<IDocument> documents = dashboardStep.StepViewModel.Documents;
            Assert.AreEqual(1, documents.Count());
        }

        [Test()]
        public void ShouldHaveMedicareDocument()
        {
            DashboardStep dashboardStep = CurrentStep as DashboardStep;
            IEnumerable<IDocument> documents = dashboardStep.StepViewModel.Documents;
            Assert.IsNotNull(documents.FirstOrDefault((document) => document is MedicareDocument));
        }
    }
}