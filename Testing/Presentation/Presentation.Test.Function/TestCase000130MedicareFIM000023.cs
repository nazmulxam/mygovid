﻿using System.Threading.Tasks;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000130MedicareFIM000023
{
    public class ServerResponseTaskEtaMissing : MedicareFlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000130";
        }

        protected override async Task GivenAsync()
        {
            await UserIsOnCardInputScreenViaManualEntryAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserProvidesCorrectCardDetailsAsync();
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<DeadEndStep>());
        }

        [Test()]
        public void ShouldHaveFIM000023Error()
        {
            Assert.AreEqual("FIM000023", CodedError?.Code);
        }
    }
}