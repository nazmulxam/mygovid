﻿using myGovID.Presentation.Contract.Steps.RecoverableDeadEnd;
using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000077;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000090
{
    public class TestCase000090PassportMaxAttempts : SubmitPassportSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000090";
        }

        [Test()]
        public void ShouldBeInRecoverableDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(RecoverableDeadEndErrorStep)));
        }

        [Test()]
        public void ShouldHaveMaxAttemptsError()
        {
            RecoverableDeadEndErrorStep recoverableDeadendErrorStep = CurrentStep as RecoverableDeadEndErrorStep;
            RecoverableDeadEndViewModel model = recoverableDeadendErrorStep.ViewModel as RecoverableDeadEndViewModel;
            Assert.AreEqual("Max attempt reached", model.Exception.Title);
        }

    }
}