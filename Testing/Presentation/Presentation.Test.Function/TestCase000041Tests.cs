﻿using System.Linq;using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Steps;
using NUnit.Framework;
using myGovID.Presentation.Contract.Steps.AccountSetup;
namespace myGovID.Presentation.Test.Function.TestCase000041
{
    public class TestCase000041NavigateToEmailVerificationStep : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000041";
        }
        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
            await GetNextStepAsync(AccountSetupAction.Register);
            await GetNextStepAsync();
            await GetNextStepAsync();
            CaptureEmailStep captureEmailStep = CurrentStep as CaptureEmailStep;
            captureEmailStep.StepViewModel.Inputs.First<IInput>(input => input.MetaData == InputMetaData.EmailAddress).FromString("k@k.com");
            await GetNextStepAsync();
        }

        [Test()]
        public void ShouldPopulateErrorObject()
        {
            Assert.IsNotNull(CodedError);
        }

        [Test()]
        public void ShouldHaveFIM000013Error()
        {
            Assert.AreEqual("FIM000013", CodedError.Code);
        }
    }
}

