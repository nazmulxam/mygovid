﻿using System.Threading.Tasks;
using myGovID.Presentation.Contract.Steps.AccountSetup;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000023
{
    public class TestCase000023TestsResumePoi404 : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000023";
        }
        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
            await GetNextStepAsync(AccountSetupAction.Register);
            await GetNextStepAsync(); // T&C
            await GetNextStepAsync(); // Deadend
        }

        [Test()]
        public void ShouldBeInRecoverableDeadendErrorStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<RecoverableDeadEndErrorStep>());
        }

        [Test()]
        public void ShouldMapServerErrorMessage()
        {
            var step = CurrentStep as RecoverableDeadEndErrorStep;
            Assert.That(step, Is.Not.Null, "CurrentStep as RecoverableDeadEndErrorStep");

            var exception = step.StepViewModel.Exception;
            Assert.That(exception, Is.Not.Null, "Exception");

            Assert.Multiple(() =>
            {
                Assert.That(exception.Code, Is.EqualTo("SYS000001"));
                Assert.That(exception.Title, Is.EqualTo("An error has occurred"));
                // TODO: Confirm this, it's not currently possible to get the HTTP status code
                //Assert.That(exception.Description, Is.EqualTo("Http status code: 404"));
            });
        }
    }
}