﻿using System.Threading.Tasks;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000138MedicareFIM000022
{
    public class ServerResponseUnknownTaskStatus : MedicareFlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000138";
        }

        protected override async Task GivenAsync()
        {
            await UserIsOnCardInputScreenViaManualEntryAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserProvidesCorrectCardDetailsAsync();
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<DeadEndStep>());
        }

        [Test()]
        public void ShouldHaveFIM000022Error()
        {
            Assert.AreEqual("FIM000022", CodedError?.Code);
        }
    }
}