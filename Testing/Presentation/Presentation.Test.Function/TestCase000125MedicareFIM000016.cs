﻿using System.Threading.Tasks;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000125MedicareFIM000016
{
    public class ServerResponseLinkMethodMissing : MedicareFlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000125";
        }

        protected override async Task GivenAsync()
        {
            await UserIsOnCardInputScreenViaManualEntryAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserProvidesCorrectCardDetailsAsync();
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<DeadEndStep>());
        }

        [Test()]
        public void ShouldHaveFIM000016Error()
        {
            Assert.AreEqual("FIM000016", CodedError?.Code);
        }
    }
}