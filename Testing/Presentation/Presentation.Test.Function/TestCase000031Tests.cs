﻿using System.Linq;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Authenticate;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000031
{
    [TestFixture()]
    [Ignore(reason: "This scenario is not possible. Adam is checking with Lukas to determine if it does.")]
    public class TestCase000031ResumePOI422 : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000031";
        }
        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
            AuthenticateStep authenticateStep = CurrentStep as AuthenticateStep;
            authenticateStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.Password).FromString("Password!1");
            await GetNextStepAsync(AuthenticateAction.Password);
        }

        [Test()]
        public void ShouldHaveNonNullCurrentStep()
        {
            Assert.NotNull(CurrentStep);
        }

        [Test()]
        public void ShouldHaveAuthenticateStep()
        {
            Assert.IsTrue(CurrentStep is AuthenticateStep);
        }

        [Test()]
        public void ShouldHave422Message()
        {
            Assert.IsNotNull(CurrentStep.Messages);
            Assert.IsTrue(CurrentStep.Messages.Any());
            PageMessage message = CurrentStep.Messages.First();
            Assert.AreEqual("POI11000 (422)", message.Code);
        }
    }
}