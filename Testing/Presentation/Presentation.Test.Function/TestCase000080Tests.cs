﻿using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000077;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000080
{
    public class TestCase000080PassportFIM000022 : SubmitPassportSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000080";
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(DeadEndStep)));
        }

        [Test()]
        public void ShouldPopulateErrorObject()
        {
            Assert.IsNotNull(CodedError);
        }

        [Test()]
        public void ShouldHaveFIM000022Error()
        {
            Assert.AreEqual("FIM000022", CodedError.Code);
        }

    }
}