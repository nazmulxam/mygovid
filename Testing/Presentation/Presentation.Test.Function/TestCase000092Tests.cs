﻿using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000077;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000092
{
    public class TestCase000092PassportFIM000021 : SubmitPassportSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000092";
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(DeadEndStep)));
        }

        [Test()]
        public void ShouldPopulateErrorObject()
        {
            Assert.IsNotNull(CodedError);
        }

        [Test()]
        public void ShouldHaveFIM0000021Error()
        {
            Assert.AreEqual("FIM000021", CodedError.Code);
        }

    }
}