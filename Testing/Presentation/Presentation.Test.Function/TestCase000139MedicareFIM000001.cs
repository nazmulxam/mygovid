﻿using System.Threading.Tasks;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000139MedicareFIM000001
{
    [Ignore("We may never get FIM000001 for a Task Response.")]
    public class ServerResponseProcessIdMissing : MedicareFlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000139";
        }

        protected override async Task GivenAsync()
        {
            await UserIsOnCardInputScreenViaManualEntryAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserProvidesCorrectCardDetailsAsync();
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<DeadEndStep>());
        }

        [Test()]
        public void ShouldHaveFIM000001Error()
        {
            Assert.AreEqual("FIM000001", CodedError?.Code);
        }
    }
}