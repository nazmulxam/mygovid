﻿using System.Linq;using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Steps;
using NUnit.Framework;
using myGovID.Presentation.Contract.Steps.AccountSetup;
using myGovID.Presentation.Contract.Steps.MaxAttempts;

namespace myGovID.Presentation.Test.Function.TestCase000045
{
    public class TestCase000045MaxAttemptScenario : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000045";
        }
        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
            await GetNextStepAsync(AccountSetupAction.Register);
            await GetNextStepAsync();
            await GetNextStepAsync();
            CaptureEmailStep captureEmailStep = CurrentStep as CaptureEmailStep;
            IInput i = captureEmailStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.EmailAddress);
                i.FromString("k@k.com");
            await GetNextStepAsync();
            CaptureEmailVerificationStep captureEmailVerificationStep = CurrentStep as CaptureEmailVerificationStep;
            captureEmailVerificationStep.StepViewModel.Inputs.First<IInput>(input => input.MetaData == InputMetaData.VerificationCode)
                                        .FromString("111111");
            await GetNextStepAsync();
        }

        [Test()]
        public void ShouldHaveNonNullCurrentStep()
        {
            Assert.NotNull(CurrentStep);
        }

        [Test()]
        public void ShouldHaveMaxAttemptReachedStep()
        {
            Assert.IsTrue(CurrentStep is IMaxAttemptsReachedStep);
        }

        [Test()]
        public void ShouldHaveMaxAttemptMessage()
        {
            MaxAttemptsReachedStep maxAttempt = CurrentStep as MaxAttemptsReachedStep;
            Assert.IsNotNull(maxAttempt.Messages);
            Assert.IsTrue(maxAttempt.Messages.Any());
            PageMessage message = maxAttempt.Messages.First();
            Assert.AreEqual("POI31091 (422)", message.Code);
            Assert.AreEqual("<p>We could not verify your email. Try again.</p>", message.Description);
            Assert.AreEqual("Max attempt reached", message.Title);
        }
    }

    public class TestCase000045MaxAttemptScreenClickOK : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000045";
        }
        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
            await GetNextStepAsync(AccountSetupAction.Register);
            await GetNextStepAsync();
            await GetNextStepAsync();
            CaptureEmailStep captureEmailStep = CurrentStep as CaptureEmailStep;
            captureEmailStep.StepViewModel.Inputs.First<IInput>(input => input.MetaData == InputMetaData.EmailAddress).FromString("k@k.com");
            await GetNextStepAsync();
            CaptureEmailVerificationStep captureEmailVerificationStep = CurrentStep as CaptureEmailVerificationStep;
            captureEmailVerificationStep.StepViewModel.Inputs.First<IInput>(input => input.MetaData == InputMetaData.VerificationCode)
                                        .FromString("111111");
            //get verification max attempts reached step
            await GetNextStepAsync();
            await GetNextStepAsync();
        }

        [Test()]
        public void ShouldHaveNonNullCurrentStep()
        {
            Assert.NotNull(CurrentStep);
        }

        [Test()]
        public void ShouldHaveCaptureEmailStep()
        {
            Assert.IsTrue(CurrentStep is CaptureEmailStep);
        }
    }
}

