﻿using System.Threading.Tasks;
using myGovID.Presentation.Contract.Steps.AccountSetup;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000028
{
    public class TestCase000028TestsStartPoiSuccess : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000028";
        }
        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
            await GetNextStepAsync(AccountSetupAction.Register);
            await GetNextStepAsync(); // T&C
            await GetNextStepAsync(); // Capture email
        }

        [Test()]
        public void ShouldHaveNonNullCurrentStep()
        {
            Assert.NotNull(CurrentStep);
        }

        [Test()]
        public void ShouldHaveCaptureEmailStep()
        {
            Assert.IsTrue(CurrentStep is CaptureEmailStep);
        }

        [Test()]
        public void ShouldNotHavePageLevelMessages()
        {
            Assert.IsNull(CurrentStep.Messages);
        }
    }
}