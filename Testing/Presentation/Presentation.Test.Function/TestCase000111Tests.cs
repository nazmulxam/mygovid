﻿using myGovID.Presentation.Steps.DocumentIdentitySuccess;
using myGovID.Presentation.Steps.DriverLicence;
using myGovID.Presentation.Test.Function.TestCase000120;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000111
{
    public class TestCase000111DriverLicenceSuccessDS2 : SubmitDriverLicenceSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000111";
        }

        [Test()]
        public void ShouldBeInDriverLicenceSuccessStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(DocumentIdentitySuccessStep)));
        }

        [Test()]
        public void ShouldNotPopulateErrorObject()
        {
            Assert.IsNull(CodedError);
        }

    }
}