﻿using myGovID.Presentation.Steps;
using myGovID.Presentation.Steps.DocumentIdentitySuccess;
using myGovID.Presentation.Steps.Passport;
using myGovID.Presentation.Test.Function.TestCase000077;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000095
{
    public class TestCase000095PassportSuccess : SubmitPassportSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000095";
        }

        [Test()]
        public void ShouldBeInDocumentIdentitySuccessStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(DocumentIdentitySuccessStep)));
        }

        [Test()]
        public void ShouldNotPopulateErrorObject()
        {
            Assert.IsNull(CodedError);
        }

    }
}