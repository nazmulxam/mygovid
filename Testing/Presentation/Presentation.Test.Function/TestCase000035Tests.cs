﻿using System;
using System.Linq;using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Steps;
using NUnit.Framework;
using myGovID.Presentation.Contract.Steps.AccountSetup;
namespace myGovID.Presentation.Test.Function.TestCase000035
{
    public class TestCase000035NavigateToEmailVerificationStep : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000035";
        }
        protected override async Task WhenAsync()
        {
            try
            {
                await GetInitialStepAsync();
                await GetNextStepAsync(AccountSetupAction.Register);
                await GetNextStepAsync();
                await GetNextStepAsync();
                CaptureEmailStep captureEmailStep = CurrentStep as CaptureEmailStep;
                captureEmailStep.StepViewModel.Inputs.First<IInput>(input => input.MetaData == InputMetaData.EmailAddress).FromString("k@k.com");
                await GetNextStepAsync();
            } catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        [Test()]
        public void ShouldHaveNonNullCurrentStep()
        {
            Assert.NotNull(CurrentStep);
        }

        [Test()]
        public void ShouldHaveCaptureEmailVerificationCurrentStep()
        {
            Assert.IsTrue(CurrentStep is CaptureEmailVerificationStep);
        }

        [Test()]
        public void ShouldNotHavePageLevelMessages()
        {
            Assert.IsNull(CurrentStep.Messages);
        }
    }
}
