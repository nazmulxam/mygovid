﻿using System.Linq;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Steps;
using NUnit.Framework;
using myGovID.Presentation.Contract.Steps.AccountSetup;
namespace myGovID.Presentation.Test.Function.TestCase000001
{
    public class DashboardSuccessScenario : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000001";
        }

        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
            await GetNextStepAsync(AccountSetupAction.Register);
            await GetNextStepAsync();
            await GetNextStepAsync();

            CaptureEmailStep captureEmailStep = CurrentStep as CaptureEmailStep;
            captureEmailStep.StepViewModel.Inputs.First<IInput>(input => input.MetaData == InputMetaData.EmailAddress).FromString("k@k.com");
            await GetNextStepAsync();

            CaptureEmailVerificationStep captureEmailVerificationStep = CurrentStep as CaptureEmailVerificationStep;
            captureEmailVerificationStep.StepViewModel.Inputs.First<IInput>(input => input.MetaData == InputMetaData.VerificationCode).FromString("111111");
            await GetNextStepAsync(); //secure your account
            await GetNextStepAsync(); //capture password

            CapturePasswordStep capturePasswordStep = CurrentStep as CapturePasswordStep;
            capturePasswordStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.Password).FromString("Password1!");
            capturePasswordStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.ConfirmPassword).FromString("Password1!");
            await GetNextStepAsync();

            CapturePersonalDetailsStep capturePersonalDetailsStep = CurrentStep as CapturePersonalDetailsStep;
            capturePersonalDetailsStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.GivenName).FromString("Shane");
            capturePersonalDetailsStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.FamilyName).FromString("Foreman");
            capturePersonalDetailsStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.DateOfBirth).FromString("18/01/1982");
            await GetNextStepAsync();
            await GetNextStepAsync();
        }

        [Test()]
        public void ShouldBeInDashboardStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(DashboardStep)));
        }
    }
}

