﻿using System.Threading.Tasks;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000148MedicareMaxAttempts
{
    public class ServerResponseTaskFailedMaxAttempts : MedicareFlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000148";
        }

        protected override async Task GivenAsync()
        {
            await UserIsOnCardInputScreenViaManualEntryAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserProvidesCorrectCardDetailsAsync();
        }

        [Test()]
        public void ShouldBeInRecoverableDeadendErrorStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<RecoverableDeadEndErrorStep>());
        }

        [Test()]
        public void ShouldMapServerErrorMessage()
        {
            var step = CurrentStep as RecoverableDeadEndErrorStep;
            Assert.That(step, Is.Not.Null, "CurrentStep as RecoverableDeadEndErrorStep");

            var exception = step.StepViewModel.Exception;
            Assert.That(exception, Is.Not.Null, "Exception");

            Assert.Multiple(() =>
            {
                Assert.That(exception.Code, Is.EqualTo("POI31091"));
                Assert.That(exception.Title, Is.EqualTo("Max attempt reached"));
                Assert.That(exception.Description, Is.EqualTo("<p>You have reached the maximum amout of attempts for verifying a document.</p><p>Try again in 24 hours.</p><p><b>Error code POI31091 (400)</b></p>"));
            });
        }
    }
}