﻿using System.Threading.Tasks;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000136MedicareFIM000019
{
    public class ServerResponseUnkownAuthenticationType : MedicareFlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000136";
        }

        protected override async Task GivenAsync()
        {
            await UserIsOnCardInputScreenViaManualEntryAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserProvidesCorrectCardDetailsAsync();
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<DeadEndStep>());
        }

        [Test()]
        public void ShouldHaveFIM000019Error()
        {
            Assert.AreEqual("FIM000019", CodedError?.Code);
        }
    }
}