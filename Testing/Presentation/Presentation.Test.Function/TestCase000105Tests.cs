﻿using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000120;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000105
{
    public class TestCase000105DriverLicenceFIM000021 : SubmitDriverLicenceSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000105";
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(DeadEndStep)));
        }

        [Test()]
        public void ShouldPopulateErrorObject()
        {
            Assert.IsNotNull(CodedError);
        }

        [Test()]
        public void ShouldHaveFIM000021Error()
        {
            Assert.AreEqual("FIM000021", CodedError.Code);
        }

    }
}