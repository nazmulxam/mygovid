﻿using System.Threading.Tasks;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000147MedicareFIM000018
{
    public class ServerResponseLinkRelMissing : MedicareFlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000147";
        }

        protected override async Task GivenAsync()
        {
            await UserIsOnCardInputScreenViaManualEntryAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserProvidesCorrectCardDetailsAsync();
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<DeadEndStep>());
        }

        [Test()]
        public void ShouldHaveFIM000018Error()
        {
            Assert.AreEqual("FIM000018", CodedError?.Code);
        }
    }
}