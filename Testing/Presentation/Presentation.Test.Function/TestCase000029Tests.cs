﻿using System.Linq;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Authenticate;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000029
{
    public class TestCase000029TestsResumePoiProcessIdIsMissing : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000029";
        }
        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
            AuthenticateStep authenticateStep = CurrentStep as AuthenticateStep;
            authenticateStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.Password).FromString("Password!1");
            await GetNextStepAsync(AuthenticateAction.Password);
        }

        [Test()]
        public void ShouldPopulateErrorObject()
        {
            Assert.IsNotNull(CodedError);
        }

        [Test()]
        public void ShouldHaveFIM000001Error()
        {
            Assert.AreEqual("FIM000001", CodedError.Code);
        }
    }
}