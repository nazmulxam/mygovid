﻿using NUnit.Framework;
using myGovID.Presentation.Contract.Steps.AccountSetup;
using System.Threading.Tasks;
using myGovID.Presentation.Steps;

namespace myGovID.Presentation.Test.Function.TestCase000020
{
    public class TestCase000020TermsAndConditionsPayloadMissing : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000020";
        }
        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
            await GetNextStepAsync(AccountSetupAction.Register);
            await GetNextStepAsync();
        }

        [Test()]
        public void ShouldBeInRecoverableDeadendErrorStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<RecoverableDeadEndErrorStep>());
        }

        [Test()]
        public void ShouldMapServerErrorMessage()
        {
            var step = CurrentStep as RecoverableDeadEndErrorStep;
            Assert.That(step, Is.Not.Null, "CurrentStep as RecoverableDeadEndErrorStep");

            var exception = step.StepViewModel.Exception;
            Assert.That(exception, Is.Not.Null, "Exception");

            Assert.Multiple(() =>
            {
                Assert.That(exception.Code, Is.EqualTo("SYS000001"));
                Assert.That(exception.Title, Is.EqualTo("An error has occurred"));
                // TODO: Confirm this, it's not currently possible to get the HTTP status code
                //Assert.That(exception.Description, Is.EqualTo("Http status code: 404"));
            });
        }
    }
}
