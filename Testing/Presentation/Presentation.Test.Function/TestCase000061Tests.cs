﻿using System;
using System.Linq;using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Steps;
using NUnit.Framework;
using myGovID.Presentation.Contract.Steps.AccountSetup;
namespace myGovID.Presentation.Test.Function.TestCase000061
{
    public class TestCase000061GetCredentialLinkTargetTypeIsMissing : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000061";
        }
        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
            await GetNextStepAsync(AccountSetupAction.Register);
            await GetNextStepAsync();
            await GetNextStepAsync();
            
            CaptureEmailStep captureEmailStep = CurrentStep as CaptureEmailStep;
            captureEmailStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.EmailAddress).FromString("k@k.com");
            await GetNextStepAsync();
            
            CaptureEmailVerificationStep captureEmailVerificationStep = CurrentStep as CaptureEmailVerificationStep;
            captureEmailVerificationStep.StepViewModel.Inputs.First(input => input.MetaData == InputMetaData.VerificationCode)
                                        .FromString("111111");
            await GetNextStepAsync(); //secure your account
        }

        [Test()]
        public void ShouldPopulateErrorObject()
        {
            Assert.IsNotNull(CodedError);
        }

        [Test()]
        public void ShouldHaveFIM000018ErrorCode()
        {
            Assert.AreEqual("FIM000018", CodedError.Code);
        }
    }
}

