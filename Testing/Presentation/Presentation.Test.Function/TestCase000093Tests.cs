﻿using System;
using System.Linq;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Passport;
using myGovID.Presentation.Steps;
using myGovID.Presentation.Steps.Passport;
using myGovID.Presentation.Test.Function.TestCase000077;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000093
{
    public class TestCase000093PassportFailedAttempt : SubmitPassportSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000093";
        }

        [Test()]
        public void ShouldBeInDashBoardStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(DashboardStep)));
        }

        [Test()]
        public void ShouldNotHavePageMessage()
        {
            Assert.IsNull(CurrentStep.Messages);
        }

    }
}