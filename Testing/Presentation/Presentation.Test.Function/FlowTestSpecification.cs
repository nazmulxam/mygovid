﻿using System;
using System.Linq;
using System.Threading.Tasks;
using myGovID.Business.Contract.Component;
using myGovID.Business.Mapper;
using myGovID.Business.Mock;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Mapper;
using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.MockSetup;
using myGovID.Services.Dependency.Contract;
using myGovID.Services.Http.Contract;
using myGovID.Services.Http.Mock;
using myGovID.Services.Timer.Contract;
using myGovID.Test;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function
{
    [TestFixture()]
    public abstract class FlowTestSpecification : AsyncTestSpecification
    {
        protected IStep CurrentStep;
        protected PresentationError CodedError;
        protected IStepRepository StepRepository;
        protected MockHttpService HttpService;
        protected string TestCase;
        protected MockDependencyService DependencyService;
        protected override void Given()
        {
            DependencyService = new MockDependencyService();
            DependencyService.Register(Scheme.Development);
            AutoMapper.Mapper.Reset();
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<BusinessMapperProfile>();
                cfg.AddProfile<ServiceMapperProfile>();
            });
            AutoMapper.Mapper.AssertConfigurationIsValid();
            try
            {
                HttpService = (MockHttpService)DependencyService.Resolve<IHttpService>();
                StepRepository = DependencyService.Resolve<IStepRepository>();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Setup setup = HttpService.Setup(GetTestCase());
            if (setup.Registered)
            {
                MockLocalCredentialComponent localCredentialComponent = (MockLocalCredentialComponent)DependencyService.Resolve<ILocalCredentialComponent>();
                localCredentialComponent.Setup(setup.CredentialToken, setup.PoiId, setup.EmailAddress, setup.UserPassword, setup.UseBiometrics, setup.FirstName, setup.LastName, setup.DateOfBirth);
            }
        }

        [TearDown]
        private void StopTimers()
        {
            DependencyService.Resolve<IDeviceActivityTimer>()?.Stop();
        }

        protected abstract string GetTestCase();

        protected override Task WhenAsync()
        {
            return null;
        }

        protected async Task GetNextStepAsync()
        {
            await GetNextStepAsync<Enum>();
        }

        protected async Task GetNextStepAsync<E>(E subType = default)
        {
            await PerformActionAsync<E>(ActionType.Next, subType);
        }

        protected async Task GetPreviousStepAsync()
        {
            await GetPreviousStepAsync<Enum>();
        }

        protected async Task GetPreviousStepAsync<E>(E subType = default)
        {
            await PerformActionAsync<E>(ActionType.Previous, subType);
        }

        protected async Task PerformActionAsync(ActionType actionType)
        {
            await PerformActionAsync<Enum>(actionType);
        }

        protected async Task PerformActionAsync<E>(ActionType actionType, E subType = default)
        {
            var action = FindAction(CurrentStep, actionType, subType);
            if (!action.SkipValidation && !CurrentStep.ViewModel.IsValid())
            {
                return;
            }
            CurrentStep = await action.Run();
            if (CurrentStep is DeadEndStep deadEndStep)
            {
                CodedError = deadEndStep.StepViewModel.Error;
            }
        }

        protected IAction<ActionType, E> FindAction<E>(IStep step, ActionType actionType, E subType = default)
        {
            return step.GetActions<E>().FirstOrDefault((action) =>
            {
                if (action.Type != actionType)
                {
                    return false;
                }
                if (action.SubType == null && subType == null)
                {
                    return true;
                }
                if (action.SubType != null && subType != null)
                {
                    return action.SubType.Equals(subType);
                }
                return false;
            });
        }
        protected async Task GetInitialStepAsync()
        {
            try
            {
                CurrentStep = await StepRepository.GetInitialStepAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            if (CurrentStep is DeadEndStep deadEndStep)
            {
                CodedError = deadEndStep.StepViewModel.Error;
            }
        }
    }
}