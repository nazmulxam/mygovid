﻿using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000077;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000084
{
    [Ignore("We may never get FIM000001 for a Task Response.")]
    public class TestCase000084PassportFIM000001 : SubmitPassportSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000084";
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(DeadEndStep)));
        }

        [Test()]
        public void ShouldPopulateErrorObject()
        {
            Assert.IsNotNull(CodedError);
        }

        [Test()]
        public void ShouldHaveFIM000001Error()
        {
            Assert.AreEqual("FIM000001", CodedError.Code);
        }

    }
}