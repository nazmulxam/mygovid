﻿using System.Threading.Tasks;
using myGovID.Presentation.Steps;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000141MedicareSYS000002
{
    public class ServerResponseTaskTechnicalError : MedicareFlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000141";
        }

        protected override async Task GivenAsync()
        {
            await UserIsOnCardInputScreenViaManualEntryAsync();
        }

        protected override async Task WhenAsync()
        {
            await UserProvidesCorrectCardDetailsAsync();
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf<DeadEndStep>());
        }

        [Test()]
        public void ShouldHaveSYS000002Error()
        {
            Assert.AreEqual("SYS000002", CodedError?.Code);
        }
    }
}