﻿using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000077;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000079
{
    public class TestCase000079PassportFIM000028 : SubmitPassportSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000079";
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(DeadEndStep)));
        }

        [Test()]
        public void ShouldPopulateErrorObject()
        {
            Assert.IsNotNull(CodedError);
        }

        [Test()]
        public void ShouldHaveFIM000020Error()
        {
            Assert.AreEqual("FIM000020", CodedError.Code);
        }

    }
}