﻿using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000077;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000081
{
    public class TestCase000081PassportFIM000016 : SubmitPassportSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000081";
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(DeadEndStep)));
        }

        [Test()]
        public void ShouldPopulateErrorObject()
        {
            Assert.IsNotNull(CodedError);
        }

        [Test()]
        public void ShouldHaveFIM000016Error()
        {
            Assert.AreEqual("FIM000016", CodedError.Code);
        }

    }
}