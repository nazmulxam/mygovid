﻿using myGovID.Services.Api.Contract;
using myGovID.Services.Http.Contract;

namespace myGovID.Presentation.Test.Function.MockSetup
{
    public class MockConfig : IHttpConfig
    {
        public string BaseUrl { get; }
        public string ApplicationName { get; }
        public string ApplicationVersion { get; }
        public MockConfig()
        {
            BaseUrl = "https://mygovid.acc.ato.gov.au/";
            ApplicationName = "myGovID";
            ApplicationVersion = "1.0.0";
        }
    }
}
