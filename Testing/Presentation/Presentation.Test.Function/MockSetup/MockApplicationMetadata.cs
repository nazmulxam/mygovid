﻿using myGovID.Business.Contract.Common.Application;

namespace myGovID.Presentation.Test.Function.MockSetup
{
    public class MockApplicationMetadata : IApplicationMetaData
    {
        public string Name => "myGovID";
        public string Version => "1.0.0";
        public string BuildDate => "01-01-2018";
        public string FilesDirectory => "";
    }
}