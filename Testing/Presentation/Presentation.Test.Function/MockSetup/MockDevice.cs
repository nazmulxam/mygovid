﻿using System.Collections.Generic;
using myGovID.Business.Contract.Common.Application;
using myGovID.Presentation.Contract;

namespace myGovID.Presentation.Test.Function.MockSetup
{
    public class MockDevice : IDevice
    {
        public string PlatformType => "Apple";
        public string RuntimeType => Contract.RuntimeType.iOS.ToString();
        public string RuntimeVersion => "12.0";
        public string FormFactorType => FormFactor.Phone.ToString();
        public IEnumerable<DeviceCapability> Capabilities { get; }

        public MockDevice()
        {
            Capabilities = new List<DeviceCapability>
            {
                DeviceCapability.Fingerprint
            };
        }
    }
}
