using myGovID.Services.Dependency.Contract;
using myGovID.Services.Uri.Contract;
using myGovID.Services.Log.Contract;
using myGovID.Services.Dependency.Common.Mock;
using myGovID.Services.Api.Contract;
using myGovID.Business.Contract.Common.Application;
using myGovID.Services.Http.Contract;

namespace myGovID.Presentation.Test.Function.MockSetup
{
    public class MockDependencyService : MockDependencyServiceCommon
    {
        protected override void InternalRegister(Scheme scheme)
        {
            base.InternalRegister(scheme);
            Register<IDependencyService>(this);
            Register<IHttpConfig, MockConfig>(Scope.Singleton);
            Register<IDevice, MockDevice>(Scope.Singleton);
            Register<IApplicationMetaData, MockApplicationMetadata>(Scope.Singleton);
            Register<IUriService, MockUriService>(Scope.Singleton);
            Register<ILogger>(new MockLogger());
        }
    }
}
