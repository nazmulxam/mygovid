﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using myGovID.Services.Log.Contract;

namespace myGovID.Presentation.Test.Function.MockSetup
{
    public class MockLogger : ILogger
    {
        public void Debug([CallerMemberName] string category = "", string message = "", params object[] args)
        {
            Log(category, LogLevel.Debug, message: message, args: args);
        }

        public void Error([CallerMemberName] string category = "", string message = "", params object[] args)
        {
            Log(category, LogLevel.Error, message: message, args: args);
        }

        public void Fatal([CallerMemberName] string category = "", string message = "", params object[] args)
        {
            Log(category, LogLevel.Fatal, message: message, args: args);
        }

        public Task FlushAsync()
        {
            // Do nothing
            return Task.FromResult<object>(null);
        }

        public void Info([CallerMemberName] string category = "", string message = "", params object[] args)
        {
            Log(category, LogLevel.Information, message: message, args: args);
        }

        public bool IsEnabled(LogLevel level)
        {
            return true;
        }

        public void Log([CallerMemberName] string category = "", LogLevel level = LogLevel.Verbose, Exception exception = null, string message = "", params object[] args)
        {
            var formattedMessage = message;
            if (args != null && args.Length > 0)
            {
                try
                {
                    formattedMessage = string.Format(message, args);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Failed to format log message: {e.Message}.");
                }
            }

            Console.WriteLine($"{level.ToString()}: {category}: {formattedMessage}");
            if (exception != null)
            {
                Console.WriteLine(exception.StackTrace);
            }
        }

        public void Verbose([CallerMemberName] string category = "", string message = "", params object[] args)
        {
            Log(category, LogLevel.Verbose, message: message, args: args);
        }

        public void Warning([CallerMemberName] string category = "", string message = "", params object[] args)
        {
            Log(category, LogLevel.Warning, message: message, args: args);
        }
    }
}
