﻿using myGovID.Presentation.Contract.Steps.RecoverableDeadEnd;
using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000120;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000101
{
    public class TestCase000101DriverLicence422 : SubmitDriverLicenceSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000101";
        }

        [Test()]
        public void ShouldBeInRecoverableDeadendStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(RecoverableDeadEndErrorStep)));
        }

        [Test()]
        public void ShouldPopulateErrorObject()
        {
            Assert.IsNull(CodedError);
        }

        [Test()]
        public void ShouldHaveFailedError()
        {
            RecoverableDeadEndErrorStep recoverableDeadendErrorStep = CurrentStep as RecoverableDeadEndErrorStep;
            RecoverableDeadEndViewModel model = recoverableDeadendErrorStep.ViewModel as RecoverableDeadEndViewModel;
            Assert.AreEqual("POI11000 (422)", model.Exception.Code);
            Assert.AreEqual("Failed", model.Exception.Title);
        }
    }
}
