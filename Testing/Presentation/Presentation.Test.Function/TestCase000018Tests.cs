﻿using myGovID.Presentation.Steps;
using NUnit.Framework;
using myGovID.Presentation.Contract.Steps.AccountSetup;
using System.Threading.Tasks;

namespace myGovID.Presentation.Test.Function.TestCase000018
{
    public class TestCase000018NavigateToTermsAndConditionsStep : FlowTestSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000018";
        }
        protected override async Task WhenAsync()
        {
            await GetInitialStepAsync();
            await GetNextStepAsync(AccountSetupAction.Register);
            await GetNextStepAsync();
        }

        [Test()]
        public void ShouldHaveNonNullCurrentStep()
        {
            Assert.NotNull(CurrentStep);
        }

        [Test()]
        public void ShouldHaveTermsAndConditionsCurrentStep()
        {
            Assert.IsTrue(CurrentStep is TermsAndConditionsStep);
        }

        [Test()]
        public void ShouldNotHavePageLevelMessages()
        {
            Assert.IsNull(CurrentStep.Messages);
        }
    }
}
