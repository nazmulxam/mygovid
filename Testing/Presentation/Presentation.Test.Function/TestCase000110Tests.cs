﻿using myGovID.Presentation.Contract.Steps.RecoverableDeadEnd;
using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000120;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000110
{
    public class TestCase000110DriverLicenceMaxAttempts : SubmitDriverLicenceSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000110";
        }

        [Test()]
        public void ShouldBeInRecoverableDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(RecoverableDeadEndErrorStep)));
        }

        [Test()]
        public void ShouldHaveMaxAttemptsError()
        {
            RecoverableDeadEndErrorStep recoverableDeadendErrorStep = CurrentStep as RecoverableDeadEndErrorStep;
            RecoverableDeadEndViewModel model = recoverableDeadendErrorStep.ViewModel as RecoverableDeadEndViewModel;
            Assert.AreEqual("POI31091", model.Exception.Code);
            Assert.AreEqual("<p>You have reached the maximum amount of attempts for verifying a document.</p><p>Try again in 24 hours.</p><p><b>Error code POI31091 (400)</b></p>", model.Exception.Description);
            Assert.AreEqual("Max attempt reached", model.Exception.Title);
        }
    }
}