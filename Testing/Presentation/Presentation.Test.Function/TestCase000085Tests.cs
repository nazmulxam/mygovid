﻿using myGovID.Presentation.Steps;
using myGovID.Presentation.Test.Function.TestCase000077;
using NUnit.Framework;

namespace myGovID.Presentation.Test.Function.TestCase000085
{
    public class TestCase000085PassportSYS000003 : SubmitPassportSpecification
    {
        protected override string GetTestCase()
        {
            return "TestCase000085";
        }

        [Test()]
        public void ShouldBeInDeadEndStep()
        {
            Assert.That(CurrentStep, Is.TypeOf(typeof(DeadEndStep)));
        }

        [Test()]
        public void ShouldPopulateErrorObject()
        {
            Assert.IsNotNull(CodedError);
        }

        [Test()]
        public void ShouldHaveSYS000003Error()
        {
            Assert.AreEqual("SYS000003", CodedError.Code);
        }

    }
}