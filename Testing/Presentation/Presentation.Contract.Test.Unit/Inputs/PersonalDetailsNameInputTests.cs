﻿using NUnit.Framework;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Test;

namespace myGovID.Presentation.Contract.Test.Unit.Inputs.PersonalDetailsNameInputTests
{
    [TestFixture()]
    public abstract class PersonalDetailsNameInputSpecification : TestSpecification
    {
        protected bool isValid;
        protected PersonalDetailsNameInput nameInput;
        protected override void Given()
        {
            nameInput = new PersonalDetailsNameInput("First Name", "name", true, 3, 10);
        }
        protected override void When()
        {
            isValid = nameInput.IsValid();
        }
    }

    public class NameIsValidSpecification : PersonalDetailsNameInputSpecification
    {
        protected override void When()
        {
            nameInput.Value = "myGovId";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(nameInput.Error);
        }
    }
    public class NameWithLeadingAndTrailingWhiteSpacesIsValidSpecification : PersonalDetailsNameInputSpecification
    {
        protected override void When()
        {
            nameInput.Value = "  myGovId  ";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(nameInput.Error);
        }
        [Test()]
        public void ShouldHaveRemovedTrailingWhiteSpaces()
        {
            Assert.AreEqual("myGovId", nameInput.Value);
        }
    }

    public class NameIsNullSpecification : PersonalDetailsNameInputSpecification
    {
        protected override void When()
        {
            nameInput.Value = null;
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is required", nameInput.Error);
        }
    }

    public class NameTooShortSpecification : PersonalDetailsNameInputSpecification
    {
        protected override void When()
        {
            nameInput.Value = "my";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is required", nameInput.Error);
        }
    }

    public class NameTooLongSpecification : PersonalDetailsNameInputSpecification
    {
        protected override void When()
        {
            nameInput.Value = "myGovId myGovID";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", nameInput.Error);
        }
    }

    public class NameWithInvalidPatternSpecification : PersonalDetailsNameInputSpecification
    {
        protected override void When()
        {
            nameInput.Value = "myGovID.!#$%";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", nameInput.Error);
        }
    }
}
