﻿using System;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Test;
using NUnit.Framework;

namespace myGovID.Presentation.Contract.Test.Unit.Inputs.DriverLicenceNumberInputTests
{
    [TestFixture()]
    public abstract class DriverLicenceNumberInputSpecification : TestSpecification
    {
        protected DriverLicenceDocumentNumberInput Input;
        protected override void Given()
        {
            Input = new DriverLicenceDocumentNumberInput("Driver Licence number", "driverLicenceNumber", 1, 10);
        }

        protected void ValueIs(string value)
        {
            Input.Value = value;
            Input.IsValid();
        }
    }

    public class DriverLicenceNumberValidSpecification : DriverLicenceNumberInputSpecification
    {
        protected override void When()
        {
            ValueIs("59835248XX");
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(Input.Error);
        }
    }

    public class DriverLicenceNumberWithLeadingAndTrailingWhiteSpacesValidSpecification : DriverLicenceNumberInputSpecification
    {
        protected override void When()
        {
            ValueIs("  59835248XX  ");
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(Input.Error);
        }

        [Test()]
        public void ShouldHaveRemovedTrailingWhiteSpaces()
        {
            Assert.AreEqual("59835248XX", Input.Value);
        }
    }

    public class DriverLicenceNumberNullSpecification : DriverLicenceNumberInputSpecification
    {
        protected override void When()
        {
            ValueIs(null);
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveRequiredErrorMessage()
        {
            Assert.That(Input.Error, Is.EqualTo("This field is required"));
        }
    }

    public class DriverLicenceNumberEmptySpecification : DriverLicenceNumberInputSpecification
    {
        protected override void When()
        {
            ValueIs("");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveRequiredErrorMessage()
        {
            Assert.That(Input.Error, Is.EqualTo("This field is required"));
        }
    }

    public class DriverLicenceNumberContainsInvalidCharactersSpecification : DriverLicenceNumberInputSpecification
    {
        protected override void When()
        {
            ValueIs("59%352486!");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveRequiredErrorMessage()
        {
            Assert.That(Input.Error, Is.EqualTo("This field is invalid"));
        }
    }

    public class DriverLicenceNumberTooShortSpecification : DriverLicenceNumberInputSpecification
    {
        protected override void When()
        {
            ValueIs("");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveRequiredErrorMessage()
        {
            Assert.That(Input.Error, Is.EqualTo("This field is required"));
        }
    }

    public class DriverLicenceNumberTooLongSpecification : DriverLicenceNumberInputSpecification
    {
        protected override void When()
        {
            ValueIs("5983524861Y");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveRequiredErrorMessage()
        {
            Assert.That(Input.Error, Is.EqualTo("This field is invalid"));
        }
    }
}
