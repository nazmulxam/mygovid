﻿using System;
using NUnit.Framework;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Test;
namespace myGovID.Presentation.Contract.Test.Unit.Inputs.GenderInputTests
{
    [TestFixture()]
    public abstract class GenderInputSpecification : TestSpecification
    {
        protected bool isValid;
        protected GenderInput genderInput;
        protected override void Given()
        {
            genderInput = new GenderInput("GenderInputLabel","genderInput", true, InputMetaData.Gender);
        }
        protected override void When()
        {
            isValid = genderInput.IsValid();
        }
    }

    public class GenderIsValidSpecification : GenderInputSpecification
    {
        protected override void When()
        {
            genderInput.Value = "Female";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(genderInput.Error);
        }
    }

    public class GenderIsInvalidSpecification : GenderInputSpecification
    {
        protected override void When()
        {
            genderInput.Value = "Fem";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual("This field is invalid", genderInput.Error);
        }
    }

    public class GenderIsNullSpecification : GenderInputSpecification
    {
        protected override void When()
        {
            genderInput.Value = null;
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual("This field is required", genderInput.Error);
        }
    }
}