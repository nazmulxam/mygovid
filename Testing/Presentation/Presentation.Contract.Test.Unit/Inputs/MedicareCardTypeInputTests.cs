﻿using myGovID.Presentation.Contract.Inputs;
using myGovID.Test;
using NUnit.Framework;

namespace myGovID.Presentation.Contract.Test.Unit.Inputs
{
    public class MedicareCardTypeInputTests
    {
        [TestFixture()]
        public abstract class MedicareCardTypeInputSpecification : TestSpecification
        {
            protected MedicareCardTypeInput Input;
            protected override void Given()
            {
                Input = new MedicareCardTypeInput("Card type", "cardType");
            }

            protected void ValueIs(string value)
            {
                Input.Value = value;
                Input.IsValid();
            }
        }

        public class CardTypeInvalid : MedicareCardTypeInputSpecification
        {
            protected override void When()
            {
                ValueIs("Fuchsia");
            }

            [Test()]
            public void ShouldNotSatisfy()
            {
                Assert.IsFalse(Input.IsValid());
            }

            [Test()]
            public void ShouldHaveInvalidErrorMessage()
            {
                Assert.That(Input.Error, Is.EqualTo("This field is invalid"));
            }
        }

        public class CardTypeEmptySpecification : MedicareCardTypeInputSpecification
        {
            protected override void When()
            {
                ValueIs("");
            }

            [Test()]
            public void ShouldNotSatisfy()
            {
                Assert.IsFalse(Input.IsValid());
            }

            [Test()]
            public void ShouldHaveRequiredErrorMessage()
            {
                Assert.That(Input.Error, Is.EqualTo("This field is required"));
            }
        }

        public class CardTypeMissingSpecification : MedicareCardTypeInputSpecification
        {
            protected override void When()
            {
                ValueIs(null);
            }

            [Test()]
            public void ShouldNotSatisfy()
            {
                Assert.IsFalse(Input.IsValid());
            }

            [Test()]
            public void ShouldHaveRequiredErrorMessage()
            {
                Assert.That(Input.Error, Is.EqualTo("This field is required"));
            }
        }

        public class IrnValidSpecificationGreen : MedicareCardTypeInputSpecification
        {
            protected override void When()
            {
                ValueIs("Green");
            }

            [Test()]
            public void ShouldSatisfy()
            {
                Assert.IsTrue(Input.IsValid());
            }
        }
        public class IrnValidSpecificationBlue : MedicareCardTypeInputSpecification
        {
            protected override void When()
            {
                ValueIs("Blue");
            }

            [Test()]
            public void ShouldSatisfy()
            {
                Assert.IsTrue(Input.IsValid());
            }
        }

        public class IrnValidSpecificationYellow : MedicareCardTypeInputSpecification
        {
            protected override void When()
            {
                ValueIs("Yellow");
            }

            [Test()]
            public void ShouldSatisfy()
            {
                Assert.IsTrue(Input.IsValid());
            }
        }
    }
}

