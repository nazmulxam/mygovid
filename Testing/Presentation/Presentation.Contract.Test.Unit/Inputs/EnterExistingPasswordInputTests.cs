﻿using NUnit.Framework;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Test;

namespace myGovID.Presentation.Contract.Test.Unit.Inputs.EnterExistingPasswordInputTests
{
    public class EnterExistingPasswordInputTests
    {
        [TestFixture()]
        public abstract class EnterExistingPasswordInputSpecification : TestSpecification
        {
            protected PasswordInput passwordInput;
            protected bool isValid;
            protected override void Given()
            {
                passwordInput = new PasswordInput("Password", "password", 10, 15);
            }
            protected override void When()
            {
                isValid = passwordInput.IsValid();
            }
        }
        public class PasswordIsValidSpecification : EnterExistingPasswordInputSpecification
        {
            protected override void When()
            {
                passwordInput.Value = "Password1!";
                base.When();
            }
            [Test()]
            public void ShouldSatisfy()
            {
                Assert.IsTrue(isValid);
            }
            [Test()]
            public void ShouldHaveNullErrorMessage()
            {
                Assert.IsNull(passwordInput.Error);
            }
        }

        public class PasswordTooShortSpecification : EnterExistingPasswordInputSpecification
        {
            protected override void When()
            {
                passwordInput.Value = "Pass!1d";
                base.When();
            }
            [Test()]
            public void ShouldNotSatisfy()
            {
                Assert.IsFalse(isValid);
            }
            [Test()]
            public void ShouldShowTooShortErrorMsg()
            {
                Assert.AreEqual("Password must contain at least 10 characters", passwordInput.Error);
            }
        }

        public class PasswordTooLongSpecification : EnterExistingPasswordInputSpecification
        {
            protected override void When()
            {
                passwordInput.Value = "Password1!Password1!Pass!1d";
                base.When();
            }
            [Test()]
            public void ShouldNotSatisfy()
            {
                Assert.IsFalse(isValid);
            }
            [Test()]
            public void ShouldShowTooLongErrorMsg()
            {
                Assert.AreEqual("Password must contain less than 15 characters", passwordInput.Error);
            }
        }

    }
}
