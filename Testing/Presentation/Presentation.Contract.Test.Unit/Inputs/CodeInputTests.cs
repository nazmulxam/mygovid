﻿using NUnit.Framework;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Test;
namespace myGovID.Presentation.Contract.Test.Unit.Inputs.CodeInputTests
{
    [TestFixture()]
    public abstract class CodeInputSpecification : TestSpecification
    {
        protected bool isValid;
        protected CodeInput codeInput;
        protected override void Given()
        {
            codeInput = new CodeInput("Code", "code", 6);
        }
        protected override void When()
        {
            isValid = codeInput.IsValid();
        }
    }

    public class CodeIsValidSpecification : CodeInputSpecification
    {
        protected override void When()
        {
            codeInput.Value = "123456";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(codeInput.Error);
        }
    }

    public class CodeNullSpecification : CodeInputSpecification
    {
        protected override void When()
        {
            codeInput.Value = null;
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowRequiredErrorMsg()
        {
            Assert.AreEqual("This field is required", codeInput.Error);
        }
    }

    public class CodeLessThanMinLengthSpecification : CodeInputSpecification
    {
        protected override void When()
        {
            codeInput.Value = "12345";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowTooShortErrorMsg()
        {
            Assert.AreEqual("Value provided is too short", codeInput.Error);
        }
    }

    public class CodeMoreThanMaxLengthSpecification : CodeInputSpecification
    {
        protected override void When()
        {
            codeInput.Value = "123456789";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowTooLongErrorMsg()
        {
            Assert.AreEqual("Value provided is too long", codeInput.Error);
        }
    }

    public class CodeAlphanumericSpecification : CodeInputSpecification
    {
        protected override void When()
        {
            codeInput.Value = "ABCDEF";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", codeInput.Error);
        }
    }
}
