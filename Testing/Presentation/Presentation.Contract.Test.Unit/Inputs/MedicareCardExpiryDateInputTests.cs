﻿using System;
using NUnit.Framework;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Test;
namespace myGovID.Presentation.Contract.Test.Unit.Inputs.MedicareCardExpiryDateInputTests
{
    [TestFixture()]
    public abstract class MedicareCardExpiryDateInputSpecification : TestSpecification
    {
        protected MedicareCardExpiryDateInput Input;

        protected DateFormat DateFormat;

        protected void CardTypeIsGreen()
        {
            Input = new MedicareCardExpiryDateInput("Expiry Date", "expiryDate", () => "Green");
            DateFormat = DateFormat.GreenMedicareCardExpiry;
        }

        protected void CardTypeIsBlue()
        {
            Input = new MedicareCardExpiryDateInput("Expiry Date", "expiryDate", () => "Blue");
            DateFormat = DateFormat.BlueMedicareCardExpiry;
        }

        protected void CardTypeIsYellow()
        {
            Input = new MedicareCardExpiryDateInput("Expiry Date", "expiryDate", () => "Yellow");
            DateFormat = DateFormat.YellowMedicareCardExpiry;
        }

        protected void ValueIs(string value)
        {
            Input.Value = value;
            Input.IsValid();
        }

        protected void ValueIs(DateTime date, string format = null)
        {
            ValueIs(date.ToString(format ?? DateFormat.GetFormat()));
        }

        protected void ValueIs(int yearsInFuture = 0,
                               int daysInFuture = 0,
                               int monthsInFuture = 0,
                               int daysInPast = 0,
                               int monthsInPast = 0,
                               string format = null)
        {
            ValueIs(DateTime.Now
                .AddYears(yearsInFuture)
                .AddDays(daysInFuture)
                .AddMonths(monthsInFuture)
                .AddDays(-daysInPast)
                .AddMonths(-monthsInPast),
                format);
        }
    }

    #region Green card tests

    public class GreenCardExpiryDateIsValidSpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsGreen();
        }

        protected override void When()
        {
            ValueIs(yearsInFuture: 20);
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(Input.Error);
        }
    }

    public class GreenCardExpiryDateInvalidInFutureSpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsGreen();
        }

        protected override void When()
        {
            ValueIs(yearsInFuture: 20, monthsInFuture: 1);
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual("This field is invalid", Input.Error);
        }
    }

    public class GreenExpiryDateEmptyValueSpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsGreen();
        }

        protected override void When()
        {
            ValueIs("");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowMandatoryFieldErrorMsg()
        {
            Assert.AreEqual("This field is required", Input.Error);
        }
    }

    public class GreenExpiryDateYYYYMMDDDateFormatSpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsGreen();
        }

        protected override void When()
        {
            ValueIs(daysInFuture: 1, format: "yyyy-MM-dd");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidDateFormatErrorMsg()
        {
            Assert.AreEqual("Date format is incorrect", Input.Error);
        }
    }

    public class GreenExpiryDateDateFormatWithDaysSpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsGreen();
        }

        protected override void When()
        {
            ValueIs(monthsInFuture: 1, format: "dd/MM/yyyy");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidDateFormatErrorMsg()
        {
            Assert.AreEqual("Date format is incorrect", Input.Error);
        }
    }

    public class GreenDateValidTodaySpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsGreen();
        }

        protected override void When()
        {
            ValueIs(monthsInFuture: 0);
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldNotHaveError()
        {
            Assert.IsNull(Input.Error);
        }
    }

    public class GreenExpiryDateInvalidInPastSpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsGreen();
        }

        protected override void When()
        {
            ValueIs(monthsInPast: 1);
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", Input.Error);
        }
    }

    #endregion

    #region Yellow card tests

    public class YellowCardExpiryDateIsValidSpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsYellow();
        }

        protected override void When()
        {
            ValueIs(yearsInFuture: 20);
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(Input.Error);
        }
    }

    public class YellowCardExpiryDateInvalidInFutureSpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsYellow();
        }

        protected override void When()
        {
            ValueIs(yearsInFuture: 20, daysInFuture: 1);
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual("This field is invalid", Input.Error);
        }
    }

    public class YellowExpiryDateEmptyValueSpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsYellow();
        }

        protected override void When()
        {
            ValueIs("");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowMandatoryFieldErrorMsg()
        {
            Assert.AreEqual("This field is required", Input.Error);
        }
    }

    public class YellowExpiryDateYYYYMMDDDateFormatSpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsYellow();
        }

        protected override void When()
        {
            ValueIs(daysInFuture: 1, format: "yyyy-MM-dd");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidDateFormatErrorMsg()
        {
            Assert.AreEqual("Date format is incorrect", Input.Error);
        }
    }

    public class YellowExpiryDateDateFormatWithoutDaysSpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsYellow();
        }

        protected override void When()
        {
            ValueIs(daysInFuture: 1, format: "MM/yyyy");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidDateFormatErrorMsg()
        {
            Assert.AreEqual("Date format is incorrect", Input.Error);
        }
    }

    public class YellowDateValidTodaySpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsYellow();
        }

        protected override void When()
        {
            ValueIs(daysInFuture: 0);
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldNotHaveError()
        {
            Assert.IsNull(Input.Error);
        }
    }

    public class YellowExpiryDateInvalidInPastSpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsYellow();
        }

        protected override void When()
        {
            ValueIs(daysInPast: 1);
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", Input.Error);
        }
    }

    #endregion

    #region Blue card tests

    public class BlueCardExpiryDateIsValidSpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsBlue();
        }

        protected override void When()
        {
            ValueIs(yearsInFuture: 20);
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(Input.Error);
        }
    }

    public class BlueCardExpiryDateInvalidInFutureSpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsBlue();
        }

        protected override void When()
        {
            ValueIs(yearsInFuture: 20, daysInFuture: 1);
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual("This field is invalid", Input.Error);
        }
    }

    public class BlueExpiryDateEmptyValueSpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsBlue();
        }

        protected override void When()
        {
            ValueIs("");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowMandatoryFieldErrorMsg()
        {
            Assert.AreEqual("This field is required", Input.Error);
        }
    }

    public class BlueExpiryDateYYYYMMDDDateFormatSpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsBlue();
        }

        protected override void When()
        {
            ValueIs(daysInFuture: 1, format: "yyyy-MM-dd");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidDateFormatErrorMsg()
        {
            Assert.AreEqual("Date format is incorrect", Input.Error);
        }
    }

    public class BlueExpiryDateDateFormatWithoutDaysSpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsBlue();
        }

        protected override void When()
        {
            ValueIs(daysInFuture: 1, format: "MM/yyyy");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidDateFormatErrorMsg()
        {
            Assert.AreEqual("Date format is incorrect", Input.Error);
        }
    }

    public class BlueDateValidTodaySpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsBlue();
        }

        protected override void When()
        {
            ValueIs(daysInFuture: 0);
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldNotHaveError()
        {
            Assert.IsNull(Input.Error);
        }
    }

    public class BlueExpiryDateInvalidInPastSpecification : MedicareCardExpiryDateInputSpecification
    {
        protected override void Given()
        {
            CardTypeIsBlue();
        }

        protected override void When()
        {
            ValueIs(daysInPast: 1);
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", Input.Error);
        }
    }

    #endregion
}
