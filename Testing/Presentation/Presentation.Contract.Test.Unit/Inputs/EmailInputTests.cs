﻿using System;
using NUnit.Framework;
using myGovID.Presentation.Contract.Inputs;
using GalaSoft.MvvmLight;
using myGovID.Test;

namespace myGovID.Presentation.Contract.Test.Unit.Inputs.EmailInputTests
{
    public class EmailInputSpecification : TestSpecification
    {
        protected bool isValid;
        protected EmailInput EmailInput;
        protected override void Given()
        {
            EmailInput = new EmailInput("Email Address", "emailAddress", 10);
        }

        protected override void When()
        {
            isValid = EmailInput.IsValid();
        }
    }

    [TestFixture()]
    public class EmailIsValidSpecification : EmailInputSpecification
    {
        protected override void When()
        {
            EmailInput.Value = "k@k.com";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(EmailInput.Error);
        }
    }

    [TestFixture()]
    public class EmailWithLeadingAndTrailingWhiteSpacesIsValidSpecification : EmailInputSpecification
    {
        protected override void When()
        {
            EmailInput.Value = "  k@k.com   ";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(EmailInput.Error);
        }
        [Test()]
        public void ShouldHaveRemovedTrailingWhiteSpaces()
        {
            Assert.AreEqual("k@k.com", EmailInput.Value);
        }
    }

    [TestFixture()]
    public class EmailExceedsMaxLengthSpecification : EmailInputSpecification
    {
        protected override void When()
        {
            EmailInput.Value = "abcdefghijkl@k.com";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.False(isValid);
        }
        [Test()]
        public void ShouldShowMaxLengthExceedsErrorMsg()
        {
            Assert.AreEqual("This field is invalid", EmailInput.Error);
        }
    }
    [TestFixture()]
    public class EmailIsNullSpecification : EmailInputSpecification
    {
        protected override void When()
        {
            EmailInput.Value = null;
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.False(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is required", EmailInput.Error);
        }
    }
    [TestFixture()]
    public class EmailHasInvalidFormatSpecification : EmailInputSpecification
    {
        protected override void When()
        {
            EmailInput.Value = "abcd";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.False(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", EmailInput.Error);
        }
    }
}