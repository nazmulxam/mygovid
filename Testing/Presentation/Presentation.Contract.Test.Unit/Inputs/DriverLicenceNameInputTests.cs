﻿using System;
using myGovID.Presentation.Contract.Inputs;
using NUnit.Framework;
using myGovID.Test;

namespace myGovID.Presentation.Contract.Test.Unit.Inputs.DriverLicenceNameInputTests
{
    [TestFixture()]
    public abstract class DriverLicenceNameInputSpecification : TestSpecification
    {
        protected bool isValid;
        protected DriverLicenceNameInput NameInput;
        protected override void Given()
        {
            NameInput = new DriverLicenceNameInput("Given name/s", "givenNamesInput", true, 1, 31, InputMetaData.GivenName);
        }
        protected override void When()
        {
            isValid = NameInput.IsValid();
        }
    }

    public class DriverLicenceNameIsValidSpecification : DriverLicenceNameInputSpecification
    {
        protected override void When()
        {
            NameInput.Value = "ValidName";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(NameInput.Error);
        }
    }

    public class DriverLicenceNameWithLeadingAndTrailingWhiteSpaceIsValidSpecification : DriverLicenceNameInputSpecification
    {
        protected override void When()
        {
            NameInput.Value = "  Valid Name  ";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(NameInput.Error);
        }

        [Test()]
        public void ShouldHaveRemovedTrailingWhiteSpaces()
        {
            Assert.AreEqual("Valid Name", NameInput.Value);
        }
    }

    public class DriverLicenceNameIsNullSpecification : DriverLicenceNameInputSpecification
    {
        protected override void When()
        {
            NameInput.Value = null;
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.AreEqual("This field is required", NameInput.Error);
        }
    }

    public class DriverLicenceNameIsTooShortSpecification : DriverLicenceNameInputSpecification
    {
        protected override void When()
        {
            NameInput.Value = "";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is required", NameInput.Error);
        }
    }

    public class DriverLicenceNameIsTooLongSpecification : DriverLicenceNameInputSpecification
    {
        protected override void When()
        {
            //name to be of 32 characters
            NameInput.Value = "ValueIsTooLongValueIsTooLongXXXX";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", NameInput.Error);
        }
    }

    public class DriverLicenceNameHasInvalidPatternSpecification : DriverLicenceNameInputSpecification
    {
        protected override void When()
        {
            NameInput.Value = "12345678@";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", NameInput.Error);
        }
    }
}
