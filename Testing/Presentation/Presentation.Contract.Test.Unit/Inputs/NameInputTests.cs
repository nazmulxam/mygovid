﻿using System;
using NUnit.Framework;
using myGovID.Presentation.Contract.Inputs;
using GalaSoft.MvvmLight;
using myGovID.Test;

namespace myGovID.Presentation.Contract.Test.Unit.Inputs.NameInputTests
{
    [TestFixture()]
    public abstract class NameInputSpecification : TestSpecification
    {
        protected bool isValid;
        protected NameInput nameInput;
        protected override void Given()
        {
            nameInput = new NameInput("Name", "name", true, 3, 10);
        }
        protected override void When()
        {
            isValid = nameInput.IsValid();
        }
    }

    public class NameIsValidSpecification : NameInputSpecification
    {
        protected override void When()
        {
            nameInput.Value = "myGovId";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(nameInput.Error);
        }
    }

    public class NameWithLeadingAndTrailingWhiteSpacesIsValidSpecification : NameInputSpecification
    {
        protected override void When()
        {
            nameInput.Value = "  myGovId   ";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(nameInput.Error);
        }
        [Test()]
        public void ShouldHaveRemovedTrailingWhiteSpaces()
        {
            Assert.AreEqual("myGovId", nameInput.Value);
        }
    }

    public class NameIsNullSpecification : NameInputSpecification
    {
        protected override void When()
        {
            nameInput.Value = null;
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is required", nameInput.Error);
        }
    }

    public class NameTooShortSpecification : NameInputSpecification
    {
        protected override void When()
        {
            nameInput.Value = "my";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is required", nameInput.Error);
        }
    }

    public class NameTooLongSpecification : NameInputSpecification
    {
        protected override void When()
        {
            nameInput.Value = "myGovId myGovID";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", nameInput.Error);
        }
    }
}
