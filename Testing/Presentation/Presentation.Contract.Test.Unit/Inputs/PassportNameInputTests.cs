﻿using System;
using myGovID.Presentation.Contract.Inputs;
using NUnit.Framework;
using myGovID.Test;

namespace myGovID.Presentation.Contract.Test.Unit.Inputs.PassportNameInputTests
{

    [TestFixture()]
    public abstract class PassportNameInputSpecification : TestSpecification
    {
        protected bool isValid;
        protected PassportNameInput nameInput;
        protected override void Given()
        {
            nameInput = new PassportNameInput("Given name/s", "givenNamesInput", true, 1, 31);
        }
        protected override void When()
        {
            isValid = nameInput.IsValid();
        }
    }

    public class PassportNameIsValidSpecification : PassportNameInputSpecification
    {
        protected override void When()
        {
            nameInput.Value = "ValidName";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(nameInput.Error);
        }
    }

    public class PassportNameWithLeadingAndTrailingWhiteSpacesIsValidSpecification : PassportNameInputSpecification
    {
        protected override void When()
        {
            nameInput.Value = "   ValidName   ";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(nameInput.Error);
        }
        [Test()]
        public void ShouldHaveRemovedTrailingWhiteSpaces()
        {
            Assert.AreEqual("ValidName", nameInput.Value);
        }
    }

    public class PassportNameIsNullSpecification : PassportNameInputSpecification
    {
        protected override void When()
        {
            nameInput.Value = null;
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.AreEqual("This field is required", nameInput.Error);
        }
    }

    public class PassportNameIsTooShortSpecification : PassportNameInputSpecification
    {
        protected override void When()
        {
            nameInput.Value = "";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is required", nameInput.Error);
        }
    }

    public class PassportNameIsTooLongSpecification : PassportNameInputSpecification
    {
        protected override void When()
        {
            //name to be of 32 characters
            nameInput.Value = "ValueIsTooLongValueIsTooLongXXXX";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", nameInput.Error);
        }
    }

    public class PassportNameHasInvalidPatternSpecification : PassportNameInputSpecification
    {
        protected override void When()
        {
            nameInput.Value = "12345678@";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", nameInput.Error);
        }
    }
}
