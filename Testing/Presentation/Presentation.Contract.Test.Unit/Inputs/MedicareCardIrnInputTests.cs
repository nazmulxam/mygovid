﻿using myGovID.Presentation.Contract.Inputs;
using myGovID.Test;
using NUnit.Framework;

namespace myGovID.Presentation.Contract.Test.Unit.Inputs
{
    public class MedicareCardIrnInputTests
    {
        [TestFixture()]
        public abstract class MedicareCardIrnInputSpecification : TestSpecification
        {
            protected MedicareCardIrnInput Input;
            protected override void Given()
            {
                Input = new MedicareCardIrnInput("Individual Reference Number", "irn");
            }

            protected void ValueIs(string value)
            {
                Input.Value = value;
                Input.IsValid();
            }
        }

        public class IrnEmptySpecification : MedicareCardIrnInputSpecification
        {
            protected override void When()
            {
                ValueIs("");
            }

            [Test()]
            public void ShouldNotSatisfy()
            {
                Assert.IsFalse(Input.IsValid());
            }

            [Test()]
            public void ShouldHaveRequiredErrorMessage()
            {
                Assert.That(Input.Error, Is.EqualTo("This field is required"));
            }
        }

        public class IrnMissingSpecification : MedicareCardIrnInputSpecification
        {
            protected override void When()
            {
                ValueIs(null);
            }

            [Test()]
            public void ShouldNotSatisfy()
            {
                Assert.IsFalse(Input.IsValid());
            }

            [Test()]
            public void ShouldHaveRequiredErrorMessage()
            {
                Assert.That(Input.Error, Is.EqualTo("This field is required"));
            }
        }

        public class IrnInvalidSpecification : MedicareCardIrnInputSpecification
        {
            protected override void When()
            {
                ValueIs("10");
            }

            [Test()]
            public void ShouldNotSatisfy()
            {
                Assert.IsFalse(Input.IsValid());
            }

            [Test()]
            public void ShouldHaveInvalidErrorMessage()
            {
                Assert.That(Input.Error, Is.EqualTo("This field is invalid"));
            }
        }

        public class IrnValidSpecification1 : MedicareCardIrnInputSpecification
        {
            protected override void When()
            {
                ValueIs("1");
            }

            [Test()]
            public void ShouldSatisfy()
            {
                Assert.IsTrue(Input.IsValid());
            }
        }

        public class IrnValidSpecification2 : MedicareCardIrnInputSpecification
        {
            protected override void When()
            {
                ValueIs("2");
            }

            [Test()]
            public void ShouldSatisfy()
            {
                Assert.IsTrue(Input.IsValid());
            }
        }

        public class IrnValidSpecification3 : MedicareCardIrnInputSpecification
        {
            protected override void When()
            {
                ValueIs("3");
            }

            [Test()]
            public void ShouldSatisfy()
            {
                Assert.IsTrue(Input.IsValid());
            }
        }

        public class IrnValidSpecification4 : MedicareCardIrnInputSpecification
        {
            protected override void When()
            {
                ValueIs("4");
            }

            [Test()]
            public void ShouldSatisfy()
            {
                Assert.IsTrue(Input.IsValid());
            }
        }

        public class IrnValidSpecification5 : MedicareCardIrnInputSpecification
        {
            protected override void When()
            {
                ValueIs("5");
            }

            [Test()]
            public void ShouldSatisfy()
            {
                Assert.IsTrue(Input.IsValid());
            }
        }

        public class IrnValidSpecification6 : MedicareCardIrnInputSpecification
        {
            protected override void When()
            {
                ValueIs("6");
            }

            [Test()]
            public void ShouldSatisfy()
            {
                Assert.IsTrue(Input.IsValid());
            }
        }

        public class IrnValidSpecification7 : MedicareCardIrnInputSpecification
        {
            protected override void When()
            {
                ValueIs("7");
            }

            [Test()]
            public void ShouldSatisfy()
            {
                Assert.IsTrue(Input.IsValid());
            }
        }

        public class IrnValidSpecification8 : MedicareCardIrnInputSpecification
        {
            protected override void When()
            {
                ValueIs("8");
            }

            [Test()]
            public void ShouldSatisfy()
            {
                Assert.IsTrue(Input.IsValid());
            }
        }

        public class IrnValidSpecification9 : MedicareCardIrnInputSpecification
        {
            protected override void When()
            {
                ValueIs("9");
            }

            [Test()]
            public void ShouldSatisfy()
            {
                Assert.IsTrue(Input.IsValid());
            }
        }
    }
}
