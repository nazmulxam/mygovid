﻿using System;
using NUnit.Framework;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Test;
namespace myGovID.Presentation.Contract.Test.Unit.Inputs.DriverLicenceStateInputTests
{
    [TestFixture()]
    public abstract class DriverLicenceStateInputSpecification : TestSpecification
    {
        protected bool isValid;
        protected DriverLicenceStateInput stateInput;
        protected override void Given()
        {
            stateInput = new DriverLicenceStateInput("Driver Licence State", "driverLicenceState");
        }

        protected override void When()
        {
            isValid = stateInput.IsValid();
        }
    }

    public class DriverLicenceStateIsValidSpecification : DriverLicenceStateInputSpecification
    {
        protected override void When()
        {
            stateInput.Value = "ACT";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(stateInput.Error);
        }
    }

    public class DriverLicenceStateIsInValidSpecification : DriverLicenceStateInputSpecification
    {
        protected override void When()
        {
            stateInput.Value = "XYZABC";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", stateInput.Error);
        }
    }
}
