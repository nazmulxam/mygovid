﻿using System;
using NUnit.Framework;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Test;
namespace myGovID.Presentation.Contract.Test.Unit.Inputs.DriverLicenceDOBInputTests
{
    [TestFixture()]
    public abstract class DriverLicenceDOBInputSpecification : TestSpecification
    {
        protected bool isValid;
        protected BirthDateInput birthdateInput;
        protected override void Given()
        {
            birthdateInput = new BirthDateInput("Birth Date", "birthDate", InputMetaData.DateOfBirth, 16, 100);
        }

        protected override void When()
        {
            isValid = birthdateInput.IsValid();
        }
    }

    public class DriverLicenceDOBIsValidSpecification : DriverLicenceDOBInputSpecification
    {
        protected override void When()
        {
            birthdateInput.Value = DateTime.Now.AddYears(-20).AddMonths(0).AddDays(0).AddHours(0).ToString(DateFormat.DateOfBirth.GetFormat());
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(birthdateInput.Error);
        }
    }

    public class DriverLicenceDOBIsInValidSpecification : DriverLicenceDOBInputSpecification
    {
        protected override void When()
        {
            birthdateInput.Value = DateTime.Now.AddYears(-101).AddDays(0).AddHours(0).ToString(DateFormat.DateOfBirth.GetFormat());
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", birthdateInput.Error);
        }
    }

    public class DriverLicenceDOBIsInvalidMinAgeSpecification : DriverLicenceDOBInputSpecification
    {
        protected override void When()
        {
            birthdateInput.Value = DateTime.Now.AddYears(-15).AddDays(0).AddHours(0).ToString(DateFormat.DateOfBirth.GetFormat());
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", birthdateInput.Error);
        }
    }

    public class DriverLicenceDOBMinAgeEdgeCase : DriverLicenceDOBInputSpecification
    { 
        protected override void When()
        {
            birthdateInput.Value = DateTime.Now.AddYears(-16).AddDays(0).AddHours(0).ToString(DateFormat.DateOfBirth.GetFormat());
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(birthdateInput.Error);
        }
    }

    public class DriverLicenceDOBEmptyValueSpecification : DriverLicenceDOBInputSpecification
    {
        protected override void When()
        {
            birthdateInput.Value = "";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowFieldIsRequired()
        {
            Assert.AreEqual("This field is required", birthdateInput.Error);
        }
    }
}
