﻿using NUnit.Framework;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Test;
namespace myGovID.Presentation.Contract.Test.Unit.Inputs.PassportDocumentNumberInputTests
{
    [TestFixture()]
    public abstract class PassportDocumentNumberInputSpecification : TestSpecification
    {
        protected bool isValid;
        protected PassportDocumentNumberInput documentInput;
        protected override void Given()
        {
            documentInput = new PassportDocumentNumberInput("Document no", "number", 8, 9);
        }
        protected override void When()
        {
            isValid = documentInput.IsValid();
        }
    }

    public class PassportDocumentIsValidSpecification : PassportDocumentNumberInputSpecification
    {
        protected override void When()
        {
            documentInput.Value = "PA1234567";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(documentInput.Error);
        }
    }

    public class PassportDocumentWithLeadingAndTrailingWhiteSpacesIsValidSpecification : PassportDocumentNumberInputSpecification
    {
        protected override void When()
        {
            documentInput.Value = "  PA1234567   ";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(documentInput.Error);
        }
        [Test()]
        public void ShouldHaveRemovedTrailingWhiteSpaces()
        {
            Assert.AreEqual("PA1234567", documentInput.Value);
        }
    }

    public class PassportDocumentIsNullSpecification : PassportDocumentNumberInputSpecification
    {
        protected override void When()
        {
            documentInput.Value = null;
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.AreEqual("This field is required", documentInput.Error);
        }
    }

    public class PassportDocumentIsTooShortSpecification : PassportDocumentNumberInputSpecification
    {
        protected override void When()
        {
            documentInput.Value = "P123456";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", documentInput.Error);
        }
    }

    public class PassportDocumentIsTooLongSpecification : PassportDocumentNumberInputSpecification
    {
        protected override void When()
        {
            documentInput.Value = "PA123456789";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", documentInput.Error);
        }
    }

    public class PassportDocumentHasInvalidPatternSpecification : PassportDocumentNumberInputSpecification
    {
        protected override void When()
        {
            documentInput.Value = "12345678@";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", documentInput.Error);
        }
    }
}
