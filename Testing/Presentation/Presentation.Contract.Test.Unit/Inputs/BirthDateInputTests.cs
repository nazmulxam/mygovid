﻿using System;
using NUnit.Framework;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Test;
namespace myGovID.Presentation.Contract.Test.Unit.Inputs.BirthDateInputTests
{
    [TestFixture()]
    public abstract class BirthDateInputSpecification : TestSpecification
    {
        protected bool isValid;
        protected BirthDateInput birthdateInput;
        protected override void Given()
        {
            birthdateInput = new BirthDateInput("Birth Date", "birthDate");
        }

        protected override void When()
        {
            isValid = birthdateInput.IsValid();
        }
    }

    public class BirthDateIsValidSpecification : BirthDateInputSpecification
    {
        protected override void When()
        {
            birthdateInput.Value = DateTime.Now.AddYears(-14).ToString(DateFormat.DateOfBirth.GetFormat());
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(birthdateInput.Error);
        }
    }

    public class BirthDateEmptyValueSpecification : BirthDateInputSpecification
    {
        protected override void When()
        {
            birthdateInput.Value = "";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowMandatoryFieldErrorMsg()
        {
            Assert.AreEqual("This field is required", birthdateInput.Error);
        }
    }

    public class BirthDateYYYYMMDDDateFormatSpecification : BirthDateInputSpecification
    {
        protected override void When()
        {
            birthdateInput.Value = "2018-12-03";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidDateFormatErrorMsg()
        {
            Assert.AreEqual("Date format is incorrect", birthdateInput.Error);
        }
    }

    public class BirthDateInvalidMinAgeSpecification : BirthDateInputSpecification
    {
        protected override void When()
        {
            birthdateInput.Value = DateTime.Now.AddYears(-10).ToString(DateFormat.DateOfBirth.GetFormat());
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", birthdateInput.Error);
        }
    }

    public class BirthDateInvalidMaxAgeSpecification : BirthDateInputSpecification
    {
        protected override void When()
        {
            birthdateInput.Value = DateTime.Now.AddYears(-101).ToString(DateFormat.DateOfBirth.GetFormat());
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", birthdateInput.Error);
        }
    }

    public class MinAgeEdgeCase : BirthDateInputSpecification
    {
        protected override void When()
        {
            birthdateInput.Value = DateTime.Now.AddYears(-13).ToString(DateFormat.DateOfBirth.GetFormat());
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(birthdateInput.Error);
        }
    }

    public class MaxAgeEdgeCase : BirthDateInputSpecification
    {
        protected override void When()
        {
            birthdateInput.Value = DateTime.Now.AddYears(-100).ToString(DateFormat.DateOfBirth.GetFormat());
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(birthdateInput.Error);
        }
    }
}
