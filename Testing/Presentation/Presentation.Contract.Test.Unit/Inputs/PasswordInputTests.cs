﻿using NUnit.Framework;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Test;

namespace myGovID.Presentation.Contract.Test.Unit.Inputs.PasswordInputTests
{
    [TestFixture()]
    public abstract class PasswordInputSpecification : TestSpecification
    {
        protected PasswordInput passwordInput;
        protected bool isValid;
        protected override void Given()
        {
            passwordInput = new PasswordInput("Password", "password", 10, 15);
        }
        protected override void When()
        {
            isValid = passwordInput.IsValid();
        }
    }

    public class PasswordIsValidSpecification : PasswordInputSpecification
    {
        protected override void When()
        {
            passwordInput.Value = "Password1!";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(passwordInput.Error);
        }
    }

    public class PasswordTooShortSpecification : PasswordInputSpecification
    {
        protected override void When()
        {
            passwordInput.Value = "Pass!1d";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowTooShortErrorMsg()
        {
            Assert.AreEqual("Password must contain at least 10 characters", passwordInput.Error);
        }
    }

    public class PasswordTooLongSpecification : PasswordInputSpecification
    {
        protected override void When()
        {
            passwordInput.Value = "Password1!Password1!Pass!1d";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowTooLongErrorMsg()
        {
            Assert.AreEqual("Password must contain less than 15 characters", passwordInput.Error);
        }
    }

    public class PasswordMissingNumberOrSpecialCharacterSpecification : PasswordInputSpecification
    {
        protected override void When()
        {
            passwordInput.Value = "Passsswoword";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowMissingNumberErrorMsg()
        {
            Assert.AreEqual("Password must contain either a number or special character", passwordInput.Error);
        }
    }

    public class PasswordMissingLowercaseSpecification : PasswordInputSpecification
    {
        protected override void When()
        {
            passwordInput.Value = "PASSWORDSSWORD1";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowMissingLowercaseErrorMsg()
        {
            Assert.AreEqual("Password must contain at least one lowercase character", passwordInput.Error);
        }
    }

    public class PasswordMissingUppercaseSpecification : PasswordInputSpecification
    {
        protected override void When()
        {
            passwordInput.Value = "passwordssword1";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowMissingUppercaseErrorMsg()
        {
            Assert.AreEqual("Password must contain at least one uppercase character", passwordInput.Error);
        }
    }

    public class PasswordMissingSymbolSpecification : PasswordInputSpecification
    {
        protected override void When()
        {
            passwordInput.Value = "PasswordNO";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowMissingSymbolErrorMsg()
        {
            Assert.AreEqual("Password must contain either a number or special character", passwordInput.Error);
        }
    }

    public class PasswordWithUpperAndSymbolSpecification : PasswordInputSpecification
    {
        protected override void When()
        {
            passwordInput.Value = "PASSWORDWORD!";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowMissingLowercaseErrorMsg()
        {
            Assert.AreEqual("Password must contain at least one lowercase character", passwordInput.Error);
        }
    }

    public class PasswordWithLowerAndSymbolSpecification : PasswordInputSpecification
    {
        protected override void When()
        {
            passwordInput.Value = "passwordword!";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowMissingUppercaseErrorMsg()
        {
            Assert.AreEqual("Password must contain at least one uppercase character", passwordInput.Error);
        }
    }

    public class PasswordWithValidSymbolsSpecification : PasswordInputSpecification
    {
        protected override void Given()
        {
            passwordInput = new PasswordInput("Password", "password", 10, 45);
        }
        protected override void When()
        {
            passwordInput.Value = "Pass~!@#$%^&*()-_=+[{]}\\|;:/?.><,";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(passwordInput.Error);
        }
    }

    public class PasswordWithUnsupportedSymbolsSpecification : PasswordInputSpecification
    {
        protected override void When()
        {
            passwordInput.Value = "Passwordword1`";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidFieldErrorMsg()
        {
            Assert.AreEqual("This field is invalid", passwordInput.Error);
        }
    }

    public class PasswordWithSmartSingleQuoteSpecification : PasswordInputSpecification
    {
        protected override void When()
        {
            passwordInput.Value = "Passwordword1‘";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidFieldErrorMsg()
        {
            Assert.AreEqual("This field is invalid", passwordInput.Error);
        }
    }

    public class PasswordWithSmartDoubleQuoteSpecification : PasswordInputSpecification
    {
        protected override void When()
        {
            passwordInput.Value = "Passwordword1”";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidFieldErrorMsg()
        {
            Assert.AreEqual("This field is invalid", passwordInput.Error);
        }
    }

    public class PasswordWithSingleQuoteSpecification : PasswordInputSpecification
    {
        protected override void When()
        {
            passwordInput.Value = "Passwordword1'";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidFieldErrorMsg()
        {
            Assert.AreEqual("This field is invalid", passwordInput.Error);
        }
    }

    public class PasswordWithDoubleQuoteSpecifcation : PasswordInputSpecification
    {
        protected override void When()
        {
            passwordInput.Value = "Passwordword1\"";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(isValid);
        }
        [Test()]
        public void ShouldShowInvalidFieldErrorMsg()
        {
            Assert.AreEqual("This field is invalid", passwordInput.Error);
        }
    }

    public class PasswordWithLowerUpperNumberSpecification : PasswordInputSpecification
    {
        protected override void When()
        {
            passwordInput.Value = "Passwordword11";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(passwordInput.Error);
        }
    }

    public class PasswordWithLowerUpperSymbolSpecification : PasswordInputSpecification
    {
        protected override void When()
        {
            passwordInput.Value = "Passwordword!!";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(passwordInput.Error);
        }
    }
}
