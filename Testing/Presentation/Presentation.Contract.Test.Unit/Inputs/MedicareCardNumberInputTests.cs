﻿using System;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Test;
using NUnit.Framework;

namespace myGovID.Presentation.Contract.Test.Unit.Inputs.MedicareCardNumberInputTests
{
    [TestFixture()]
    public abstract class MedicareCardNumberInputSpecification : TestSpecification
    {
        protected MedicareCardNumberInput Input;
        protected override void Given()
        {
            Input = new MedicareCardNumberInput("Card number", "cardNumber");
        }

        protected void ValueIs(string value)
        {
            Input.Value = value;
            Input.IsValid();
        }
    }

    public class CardNumberValidSpecification : MedicareCardNumberInputSpecification
    {
        protected override void When()
        {
            ValueIs("5983524861");
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(Input.Error);
        }
    }

    public class CardNumberWithLeadingAndTrailingWhiteSpacesIsValidSpecification : MedicareCardNumberInputSpecification
    {
        protected override void When()
        {
            ValueIs("  5983524861  ");
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(Input.Error);
        }
        [Test()]
        public void ShouldHaveRemovedTrailingWhiteSpaces()
        {
            Assert.AreEqual("5983524861", Input.Value);
        }
    }

    public class CardNumberNullSpecification : MedicareCardNumberInputSpecification
    {
        protected override void When()
        {
            ValueIs(null);
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveRequiredErrorMessage()
        {
            Assert.That(Input.Error, Is.EqualTo("This field is required"));
        }
    }

    public class CardNumberEmptySpecification : MedicareCardNumberInputSpecification
    {
        protected override void When()
        {
            ValueIs("");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveRequiredErrorMessage()
        {
            Assert.That(Input.Error, Is.EqualTo("This field is required"));
        }
    }

    public class CardNumberContainsInvalidCharactersSpecification : MedicareCardNumberInputSpecification
    {
        protected override void When()
        {
            ValueIs("598352486!");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveInvalidErrorMessage()
        {
            Assert.That(Input.Error, Is.EqualTo("This field is invalid"));
        }
    }

    public class CardNumberTooShortSpecification : MedicareCardNumberInputSpecification
    {
        protected override void When()
        {
            ValueIs("598352486");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveTooShortErrorMessage()
        {
            Assert.That(Input.Error, Is.EqualTo("Value provided is too short"));
        }
    }

    public class CardNumberTooLongSpecification : MedicareCardNumberInputSpecification
    {
        protected override void When()
        {
            ValueIs("59835248610");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveTooLongErrorMessage()
        {
            Assert.That(Input.Error, Is.EqualTo("Value provided is too long"));
        }
    }
}
