﻿using NUnit.Framework;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Test;

namespace myGovID.Presentation.Contract.Test.Unit.Inputs.MedicareCardNameInputTests
{
    [TestFixture()]
    public abstract class MedicareCardNameInputSpecification : TestSpecification
    {
        protected MedicareCardNameInput Input;

        protected void InputIsForLine(int line)
        {
            switch (line)
            {
                case 1:
                    Input = MedicareCardNameInput.BuildLine1("Name 1 Label", "Name 1 Placeholder", "Name 1 Accessibility ID");
                    break;
                case 2:
                    Input = MedicareCardNameInput.BuildLine2("Name 2 Label", "Name 2 Placeholder", "Name 2 Accessibility ID");
                    break;
                case 3:
                    Input = MedicareCardNameInput.BuildLine3("Name 3 Label", "Name 3 Placeholder", "Name 3 Accessibility ID");
                    break;
                case 4:
                    Input = MedicareCardNameInput.BuildLine4("Name 4 Label", "Name 4 Placeholder", "Name 4 Accessibility ID");
                    break;
            }
        }

        protected void ValueIs(string value)
        {
            Input.Value = value;
            Input.IsValid();
        }
    }

    public class Line1MaxValidLengthSpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(1);
        }

        protected override void When()
        {
            ValueIs("abcdefghijklmnopqrstuvwxyza");
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(Input.Error);
        }
    }

    public class Line1WithLeadingAndTrailingWhiteSpacesValidLengthSpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(1);
        }

        protected override void When()
        {
            ValueIs("  abcdefghijklmnopqrstuvwxyza    ");
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(Input.Error);
        }

        [Test()]
        public void ShouldHaveRemovedTrailingWhiteSpaces()
        {
            Assert.AreEqual("abcdefghijklmnopqrstuvwxyza", Input.Value);
        }
    }

    public class Line1IsNullSpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(1);
        }

        protected override void When()
        {
            ValueIs(null);
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is required", Input.Error);
        }
    }

    public class Line1EmptySpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(1);
        }

        protected override void When()
        {
            ValueIs("");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is required", Input.Error);
        }
    }

    public class Line1TooLongSpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(1);
        }

        protected override void When()
        {
            ValueIs("abcdefghijklmnopqrstuvwxyzab");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", Input.Error);
        }
    }

    public class Line1InvalidCharactersSpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(1);
        }

        protected override void When()
        {
            ValueIs("abc@def");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", Input.Error);
        }
    }

    public class Line2MaxValidLengthSpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(2);
        }

        protected override void When()
        {
            ValueIs("abcdefghijklmnopqrstuvwxy");
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(Input.Error);
        }
    }

    public class Line2WithLeadingAndTrailingWhiteSpacesValidLengthSpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(2);
        }

        protected override void When()
        {
            ValueIs("  abcdefghijklmnopqrstuvwxy    ");
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(Input.Error);
        }

        [Test()]
        public void ShouldHaveRemovedTrailingWhiteSpaces()
        {
            Assert.AreEqual("abcdefghijklmnopqrstuvwxy", Input.Value);
        }
    }


    public class Line2IsNullSpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(2);
        }

        protected override void When()
        {
            ValueIs(null);
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(Input.Error);
        }
    }

    public class Line2EmptySpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(2);
        }

        protected override void When()
        {
            ValueIs("");
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.IsNull(Input.Error);
        }
    }

    public class Line2TooLongSpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(2);
        }

        protected override void When()
        {
            ValueIs("abcdefghijklmnopqrstuvwxyz");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", Input.Error);
        }
    }

    public class Line2InvalidCharactersSpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(2);
        }

        protected override void When()
        {
            ValueIs("abc@def");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", Input.Error);
        }
    }

    public class Line3MaxValidLengthSpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(3);
        }

        protected override void When()
        {
            ValueIs("abcdefghijklmnopqrstuvw");
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(Input.Error);
        }
    }

    public class Line3WithLeadingAndTrailingWhiteSpacesValidLengthSpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(3);
        }

        protected override void When()
        {
            ValueIs("  abcdefghijklmnopqrstuvw    ");
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(Input.Error);
        }

        [Test()]
        public void ShouldHaveRemovedTrailingWhiteSpaces()
        {
            Assert.AreEqual("abcdefghijklmnopqrstuvw", Input.Value);
        }
    }

    public class Line3IsNullSpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(3);
        }

        protected override void When()
        {
            ValueIs(null);
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(Input.Error);
        }
    }

    public class Line3EmptySpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(3);
        }

        protected override void When()
        {
            ValueIs("");
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.IsNull(Input.Error);
        }
    }

    public class Line3TooLongSpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(3);
        }

        protected override void When()
        {
            ValueIs("abcdefghijklmnopqrstuvwx");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", Input.Error);
        }
    }

    public class Line3InvalidCharactersSpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(3);
        }

        protected override void When()
        {
            ValueIs("abc@def");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", Input.Error);
        }
    }

    public class Line4MaxValidLengthSpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(4);
        }

        protected override void When()
        {
            ValueIs("abcdefghijklmnopqrstu");
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(Input.Error);
        }
    }

    public class Line4WithLeadingAndTrailingWhiteSpacesValidLengthSpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(4);
        }

        protected override void When()
        {
            ValueIs("  abcdefghijklmnopqrstu    ");
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(Input.Error);
        }

        [Test()]
        public void ShouldHaveRemovedTrailingWhiteSpaces()
        {
            Assert.AreEqual("abcdefghijklmnopqrstu", Input.Value);
        }
    }


    public class Line4IsNullSpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(4);
        }

        protected override void When()
        {
            ValueIs(null);
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(Input.Error);
        }
    }

    public class Line4EmptySpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(4);
        }

        protected override void When()
        {
            ValueIs("");
        }

        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.IsNull(Input.Error);
        }
    }

    public class Line4TooLongSpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(4);
        }

        protected override void When()
        {
            ValueIs("abcdefghijklmnopqrstuv");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", Input.Error);
        }
    }

    public class Line4InvalidCharactersSpecification : MedicareCardNameInputSpecification
    {
        protected override void Given()
        {
            InputIsForLine(4);
        }

        protected override void When()
        {
            ValueIs("abc@def");
        }

        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.IsFalse(Input.IsValid());
        }

        [Test()]
        public void ShouldShowInvalidInputErrorMsg()
        {
            Assert.AreEqual("This field is invalid", Input.Error);
        }
    }
}
