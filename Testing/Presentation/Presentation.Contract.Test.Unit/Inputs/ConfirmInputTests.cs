﻿using NUnit.Framework;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Test;
namespace myGovID.Presentation.Contract.Test.Unit.Inputs.ConfirmInputTests
{
    public class ConfirmInputSpecification : TestSpecification
    {
        protected bool isValid;
        protected PasswordInput passwordInput;
        protected ConfirmInput confirmPasswordInput;
        protected override void Given()
        {
            passwordInput = new PasswordInput("Password", "password", 1, 40);
            confirmPasswordInput = new ConfirmInput("Confirm Password", "confirmPassword", passwordInput);
        }
        protected override void When()
        {
            isValid = confirmPasswordInput.IsValid();
        }
    }
    [TestFixture()]
    public class PasswordMatchSpecification : ConfirmInputSpecification
    {
        protected override void When()
        {
            passwordInput.Value = "Password1!";
            confirmPasswordInput.Value = "Password1!";
            base.When();
        }
        [Test()]
        public void ShouldSatisfy()
        {
            Assert.IsTrue(isValid);
        }
        [Test()]
        public void ShouldHaveNullErrorMessage()
        {
            Assert.IsNull(confirmPasswordInput.Error);
        }
    }
    [TestFixture()]
    public class PasswordDoesntMatchSpecification : ConfirmInputSpecification
    {
        protected override void When()
        {
            passwordInput.Value = "Password1!";
            confirmPasswordInput.Value = "Test!";
            base.When();
        }
        [Test()]
        public void ShouldNotSatisfy()
        {
            Assert.False(isValid);
        }
        [Test()]
        public void ShouldShowPasswordDontMatchErrorMsg()
        {
            Assert.AreEqual("Passwords do not match", confirmPasswordInput.Error);
        }
    }
}
