﻿using NUnit.Framework;
using System;
using myGovID.Presentation.Contract.Constraints;
using myGovID.Test;
namespace myGovID.Presentation.Contract.Test.Unit.Constraints.MandatoryTests
{
    [TestFixture()]
    public abstract class MandatoryConstraintBaseSpecification: TestSpecification 
    {
        protected MandatoryConstraint Constraint;
        protected ConstraintValidationResult Result;

        protected override void Given() 
        {
            Constraint = new MandatoryConstraint();
        }

    }

    public class MandatoryConstraintValidSpecification: MandatoryConstraintBaseSpecification
    {
        protected override void When()
        {
            Result = Constraint.IsSatisfied("SomeValue");
        }

        [Test()]
        public void ShouldPass()
        {
            Assert.IsTrue(Result.Success);
        }
    }

    public class MandatoryConstraintEmptyStringSpecification: MandatoryConstraintBaseSpecification
    {
        protected override void When()
        {
            Result = Constraint.IsSatisfied("");
        }

        [Test()]
        public void ShouldNotPass()
        {
            Assert.IsTrue(!Result.Success);
        }
        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual("This field is required", Result.ErrorMessage);
        }
    }

    public class MandatoryConstraintNullSpecification: MandatoryConstraintBaseSpecification
    {
        protected override void When()
        {
            Result = Constraint.IsSatisfied(value: null);
        }

        [Test()]
        public void ShouldNotPass()
        {
            Assert.IsTrue(!Result.Success);
        }
        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual("This field is required", Result.ErrorMessage);
        }
    }

    public class MandatoryConstraintNullWithDifferentErrorMessageSpecification: MandatoryConstraintBaseSpecification
    {
        const string ErrorMessage = "Custom error message";

        protected override void Given()
        {
            Constraint = new MandatoryConstraint(ErrorMessage);
        }

        protected override void When()
        {
            Result = Constraint.IsSatisfied(value: null);
        }

        [Test()]
        public void ShouldNotPass()
        {
            Assert.IsTrue(!Result.Success);
        }
        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual(ErrorMessage, Result.ErrorMessage);
        }
    }
}
