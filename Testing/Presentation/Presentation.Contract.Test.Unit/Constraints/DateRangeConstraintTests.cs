﻿using NUnit.Framework;
using System;
using myGovID.Presentation.Contract.Constraints;
using myGovID.Test;

namespace myGovID.Presentation.Contract.Test.Unit.Constraints.DateRangeTests
{

    [TestFixture()]
    public abstract class DateRangeConstraintBaseSpecification : TestSpecification
    {
        protected DateRangeConstraint DateRangeConstraint;
        protected ConstraintValidationResult Result;
        protected const string ErrorMsg = "This field is invalid";
    }

    public abstract class DateRangeMinMaxDateBaseSpecification : DateRangeConstraintBaseSpecification
    {
        private DateTime? _minDate;
        private DateTime? _maxDate;

        protected void MinDate(DateTime value)
        {
            _minDate = value;
            TryBuild();
        }

        protected void MaxDate(DateTime value)
        {
            _maxDate = value;
            TryBuild();
        }

        protected void ValueIs(DateTime value)
        {
            Result = DateRangeConstraint.IsSatisfied(value.ToString(DateFormat.DateOfBirth.GetFormat()));
        }

        private void TryBuild()
        {
            if (_minDate.HasValue && _maxDate.HasValue)
            {
                DateRangeConstraint = new DateRangeConstraint(DateFormat.DateOfBirth, _minDate.Value, _maxDate.Value, ErrorMsg);
            }
        }
    }

    public class DateRangeValidMinDateEdgeCaseSpecification : DateRangeMinMaxDateBaseSpecification
    {
        protected override void Given()
        {
            MinDate(DateTime.Now);
            MaxDate(DateTime.Now.AddYears(1));
        }
        protected override void When()
        {
            ValueIs(DateTime.Now);
        }
        [Test()]
        public void ShouldMatchDateRange()
        {
            Assert.IsTrue(Result.Success);
        }
    }

    public class DateRangeInvalidMinDateEdgeCaseSpecification : DateRangeMinMaxDateBaseSpecification
    {
        protected override void Given()
        {
            MinDate(DateTime.Now);
            MaxDate(DateTime.Now.AddYears(1));
        }
        protected override void When()
        {
            ValueIs(DateTime.Now.AddDays(-1));
        }
        [Test()]
        public void ShouldNotMatchDateRange()
        {
            Assert.IsFalse(Result.Success);
        }
    }

    public class DateRangeValidMaxDateEdgeCaseSpecification : DateRangeMinMaxDateBaseSpecification
    {
        protected override void Given()
        {
            MinDate(DateTime.Now);
            MaxDate(DateTime.Now.AddYears(1));
        }
        protected override void When()
        {
            ValueIs(DateTime.Now.AddYears(1));
        }
        [Test()]
        public void ShouldMatchDateRange()
        {
            Assert.IsTrue(Result.Success);
        }
    }

    public class DateRangeInvalidMaxDateEdgeCaseSpecification : DateRangeMinMaxDateBaseSpecification
    {
        protected override void Given()
        {
            MinDate(DateTime.Now);
            MaxDate(DateTime.Now.AddYears(1));
        }
        protected override void When()
        {
            ValueIs(DateTime.Now.AddYears(1).AddDays(1));
        }
        [Test()]
        public void ShouldNotMatchDateRange()
        {
            Assert.IsFalse(Result.Success);
        }
    }
}