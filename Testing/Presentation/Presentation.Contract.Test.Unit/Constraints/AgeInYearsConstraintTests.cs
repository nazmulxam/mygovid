﻿using NUnit.Framework;
using System;
using myGovID.Presentation.Contract.Constraints;
using myGovID.Test;

namespace myGovID.Presentation.Contract.Test.Unit.Constraints.AgeInYearsTests
{

    [TestFixture()]
    public abstract class AgeInYearsConstraintBaseSpecification : TestSpecification
    {
        protected AgeInYearsConstraint AgeInYearsConstraint;
        protected ConstraintValidationResult Result;
        protected const string ErrorMsg = "This field is invalid";
    }
    public class AgeInYearsValidSpecification : AgeInYearsConstraintBaseSpecification
    {
        protected override void Given()
        {
            AgeInYearsConstraint = new AgeInYearsConstraint(DateFormat.DateOfBirth, 1, 100, ErrorMsg);
        }
        protected override void When()
        {
            Result = AgeInYearsConstraint.IsSatisfied(DateTime.Now.AddYears(-10).ToString(DateFormat.DateOfBirth.GetFormat()));
        }
        [Test()]
        public void ShouldMatchDateRange()
        {
            Assert.IsTrue(Result.Success);
        }
    }

    public class AgeInYearsValidEdgeCaseAgeSpecification : AgeInYearsConstraintBaseSpecification
    {
        protected override void Given()
        {
            AgeInYearsConstraint = new AgeInYearsConstraint(DateFormat.DateOfBirth, 1, 34, ErrorMsg);
        }
        protected override void When()
        {
            Result = AgeInYearsConstraint.IsSatisfied(DateTime.Now.AddYears(-34).ToString(DateFormat.DateOfBirth.GetFormat()));
        }
        [Test()]
        public void ShouldMatchDateRange()
        {
            Assert.IsTrue(Result.Success);
        }
    }

    public class AgeInYearsGreaterThanMaxSpecification : AgeInYearsConstraintBaseSpecification
    {
        protected override void Given()
        {
            AgeInYearsConstraint = new AgeInYearsConstraint(DateFormat.DateOfBirth, 1, 20, ErrorMsg);
        }
        protected override void When()
        {
            Result = AgeInYearsConstraint.IsSatisfied(DateTime.Now.AddYears(-21).ToString(DateFormat.DateOfBirth.GetFormat()));
        }
        [Test()]
        public void ShouldNotMatchDateRange()
        {
            Assert.IsTrue(!Result.Success);
        }
        [Test()]
        public void ShouldHaveErorMessage()
        {
            Assert.AreSame(ErrorMsg, Result.ErrorMessage);
        }
    }

    public class AgeInYearsTurnedMaxAgeYesterdaySpecification : AgeInYearsConstraintBaseSpecification
    {
        protected override void Given()
        {
            AgeInYearsConstraint = new AgeInYearsConstraint(DateFormat.DateOfBirth, 1, 33, ErrorMsg);
        }
        protected override void When()
        {
            Result = AgeInYearsConstraint.IsSatisfied(DateTime.Now.AddYears(-33).AddDays(-1).ToString(DateFormat.DateOfBirth.GetFormat()));
        }
        [Test()]
        public void ShouldNotMatchDateRange()
        {
            Assert.IsTrue(!Result.Success);
        }
        [Test()]
        public void ShouldHaveErorMessage()
        {
            Assert.AreSame(ErrorMsg, Result.ErrorMessage);
        }
    }

    public class AgeInYearsTurnsMaxAgeTomorrowSpecification : AgeInYearsConstraintBaseSpecification
    {
        protected override void Given()
        {
            AgeInYearsConstraint = new AgeInYearsConstraint(DateFormat.DateOfBirth, 1, 33, ErrorMsg);
        }
        protected override void When()
        {
            Result = AgeInYearsConstraint.IsSatisfied(DateTime.Now.AddYears(-33).AddDays(1).ToString(DateFormat.DateOfBirth.GetFormat()));
        }
        [Test()]
        public void ShouldMatchDateRange()
        {
            Assert.IsTrue(Result.Success);
        }
    }

    public class AgeInYearsTurnsMaxAgeTodaySpecification : AgeInYearsConstraintBaseSpecification
    {
        protected override void Given()
        {
            AgeInYearsConstraint = new AgeInYearsConstraint(DateFormat.DateOfBirth, 1, 33, ErrorMsg);
        }
        protected override void When()
        {
            Result = AgeInYearsConstraint.IsSatisfied(DateTime.Now.AddYears(-33).ToString(DateFormat.DateOfBirth.GetFormat()));
        }
        [Test()]
        public void ShouldMatchDateRange()
        {
            Assert.IsTrue(Result.Success);
        }
    }

    public class AgeInYearsLesserThanMinSpecification : AgeInYearsConstraintBaseSpecification
    {
        protected override void Given()
        {
            AgeInYearsConstraint = new AgeInYearsConstraint(DateFormat.DateOfBirth, 18, 100, ErrorMsg);
        }
        protected override void When()
        {
            Result = AgeInYearsConstraint.IsSatisfied(DateTime.Now.AddYears(-17).ToString(DateFormat.DateOfBirth.GetFormat()));
        }
        [Test()]
        public void ShouldNotMatchDateRange()
        {
            Assert.IsTrue(!Result.Success);
        }
        [Test()]
        public void ShouldHaveErorMessage()
        {
            Assert.AreSame(ErrorMsg, Result.ErrorMessage);
        }
    }


    public class AgeInYearsTurnsMinAgeTomorrowSpecification : AgeInYearsConstraintBaseSpecification
    {
        protected override void Given()
        {
            AgeInYearsConstraint = new AgeInYearsConstraint(DateFormat.DateOfBirth, 18, 100, ErrorMsg);
        }
        protected override void When()
        {
            Result = AgeInYearsConstraint.IsSatisfied(DateTime.Now.AddYears(-18).AddDays(1).ToString(DateFormat.DateOfBirth.GetFormat()));
        }
        [Test()]
        public void ShouldNotMatchDateRange()
        {
            Assert.IsTrue(!Result.Success);
        }
        [Test()]
        public void ShouldHaveErorMessage()
        {
            Assert.AreSame(ErrorMsg, Result.ErrorMessage);
        }
    }

    public class AgeInYearsTurnsMinAgeTodaySpecification : AgeInYearsConstraintBaseSpecification
    {
        protected override void Given()
        {
            AgeInYearsConstraint = new AgeInYearsConstraint(DateFormat.DateOfBirth, 18, 100, ErrorMsg);
        }
        protected override void When()
        {
            Result = AgeInYearsConstraint.IsSatisfied(DateTime.Now.AddYears(-18).ToString(DateFormat.DateOfBirth.GetFormat()));
        }
        [Test()]
        public void ShouldMatchDateRange()
        {
            Assert.IsTrue(Result.Success);
        }
    }

    public class AgeInYearsTurnedMinAgeYesterdaySpecification : AgeInYearsConstraintBaseSpecification
    {
        protected override void Given()
        {
            AgeInYearsConstraint = new AgeInYearsConstraint(DateFormat.DateOfBirth, 18, 100, ErrorMsg);
        }
        protected override void When()
        {
            Result = AgeInYearsConstraint.IsSatisfied(DateTime.Now.AddYears(-18).AddDays(-1).ToString(DateFormat.DateOfBirth.GetFormat()));
        }
        [Test()]
        public void ShouldMatchDateRange()
        {
            Assert.IsTrue(Result.Success);
        }
    }

    public class AgeInYearsInvalidSpecification : AgeInYearsConstraintBaseSpecification
    {
        ArgumentException Exception;
        protected override void Given()
        {
            try
            {
                AgeInYearsConstraint = new AgeInYearsConstraint(DateFormat.DateOfBirth, 100, 18, ErrorMsg);
            }
            catch (ArgumentException error)
            {
                Exception = error;
            }
        }
        protected override void When()
        {
        }
        [Test()]
        public void ShouldThrowError()
        {
            Assert.IsNotNull(Exception);
        }
        [Test()]
        public void ShouldNotCreateAgeInYearsConstraint()
        {
            Assert.IsNull(AgeInYearsConstraint);
        }
    }


    public class AgeInYearsInvalidDateInputSpecification : AgeInYearsConstraintBaseSpecification
    {
        protected override void Given()
        {
            AgeInYearsConstraint = new AgeInYearsConstraint(DateFormat.DateOfBirth, 18, 100, ErrorMsg);
        }
        protected override void When()
        {
            Result = AgeInYearsConstraint.IsSatisfied("12/12/200h");
        }
        [Test()]
        public void ShouldNotMatchDateRange()
        {
            Assert.IsTrue(!Result.Success);
        }
        [Test()]
        public void ShouldHaveErorMessage()
        {
            Assert.AreSame(ErrorMsg, Result.ErrorMessage);
        }
    }
}