﻿using NUnit.Framework;
using System;
using myGovID.Presentation.Contract.Constraints;
using myGovID.Test;
namespace myGovID.Presentation.Contract.Test.Unit.Constraints.DateFormatTests
{
    [TestFixture()]
    public abstract class DateFormatBaseSpecification : TestSpecification
    {
        protected DateFormatConstraint DateFormatConstraint;
        protected ConstraintValidationResult Result;

        protected override void Given()
        {
            DateFormatConstraint = new DateFormatConstraint(DateFormat.DateOfBirth);
        }
    }

    public class DateFormatValidSpecification : DateFormatBaseSpecification
    {

        protected override void When()
        {
            Result = DateFormatConstraint.IsSatisfied(value: "30/12/1984");
        }

        [Test()]
        public void ShouldSatisfyConstraint()
        {
            Assert.IsTrue(Result.Success);
        }
    }

    public class DateFormatInvalidDateSpecification : DateFormatBaseSpecification
    {
        protected override void When()
        {
            Result = DateFormatConstraint.IsSatisfied(value: "1984/hhh");
        }

        [Test()]
        public void ShouldNotSatisfyConstraint()
        {
            Assert.IsTrue(!Result.Success);
        }

        [Test()]
        public void ShouldHaveDefaultErrorMessage()
        {
            Assert.AreEqual("Date format is incorrect", Result.ErrorMessage);
        }
    }
    public class DateFormatWrongDateDifferentErrorMessageSpecification : DateFormatBaseSpecification
    {
        private readonly string _errorMsg = "Custom error message for Date Format";

        protected override void Given()
        {
            DateFormatConstraint = new DateFormatConstraint(DateFormat.DateOfBirth, _errorMsg);
        }

        protected override void When()
        {
            Result = DateFormatConstraint.IsSatisfied(value: "1984/30/12");
        }

        [Test()]
        public void ShouldNotSatisfyConstraint()
        {
            Assert.IsTrue(!Result.Success);
        }

        [Test()]
        public void ShouldHaveCustomErrorMessage()
        {
            Assert.AreEqual(_errorMsg, Result.ErrorMessage);
        }
    }
    public class DateFormatWrongDate31Sept2018Specification : DateFormatBaseSpecification
    {

        private readonly string _errorMsg = "Custom error message for Date Format";

        protected override void Given()
        {
            DateFormatConstraint = new DateFormatConstraint(DateFormat.DateOfBirth, _errorMsg);
        }

        protected override void When()
        {
            Result = DateFormatConstraint.IsSatisfied(value: "31/09/2018");
        }

        [Test()]
        public void ShouldNotSatisfyConstraint()
        {
            Assert.IsTrue(!Result.Success);
        }

        [Test()]
        public void ShouldHaveCustomErrorMessage()
        {
            Assert.AreEqual(_errorMsg, Result.ErrorMessage);
        }
    }
    public class DateFormatWrongDate29Feb2015Specification : DateFormatBaseSpecification
    {

        private readonly string _errorMsg = "Custom error message for Date Format";

        protected override void Given()
        {
            DateFormatConstraint = new DateFormatConstraint(DateFormat.DateOfBirth, _errorMsg);
        }

        protected override void When()
        {
            Result = DateFormatConstraint.IsSatisfied(value: "29/02/2015");
        }

        [Test()]
        public void ShouldNotSatisfyConstraint()
        {
            Assert.IsTrue(!Result.Success);
        }

        [Test()]
        public void ShouldHaveCustomErrorMessage()
        {
            Assert.AreEqual(_errorMsg, Result.ErrorMessage);
        }
    }
}
