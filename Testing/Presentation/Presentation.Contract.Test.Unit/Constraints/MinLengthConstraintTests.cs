﻿using NUnit.Framework;
using System;
using myGovID.Presentation.Contract.Constraints;
using myGovID.Test;

namespace myGovID.Presentation.Contract.Test.Unit.Constraints.MinLengthTests
{
    [TestFixture()]
    public abstract class MinLengthBaseSpecification : TestSpecification
    {
        protected MinLengthConstraint Constraint;
        protected ConstraintValidationResult Result;

        protected override void Given()
        {
            Constraint = new MinLengthConstraint(3);
        }
    }

    public class MinLengthConstraintValidSpecification: MinLengthBaseSpecification
    {
        protected override void When()
        {
            Result = Constraint.IsSatisfied("123456");
        }

        [Test()]
        public void ShouldPass()
        {
            Assert.IsTrue(Result.Success);
        }
    }

    public class MinLengthConstraintValidEdgeCaseSpecification: MinLengthBaseSpecification
    {
        protected override void When()
        {
            Result = Constraint.IsSatisfied("123");
        }

        [Test()]
        public void ShouldPass()
        {
            Assert.IsTrue(Result.Success);
        }
    }

    public class MinLengthConstraintNegativeCaseSpecification: MinLengthBaseSpecification
    {
        protected override void When()
        {
            Result = Constraint.IsSatisfied("1");
        }

        [Test()]
        public void ShouldPass()
        {
            Assert.IsTrue(!Result.Success);
        }
        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual("Value provided is too short", Result.ErrorMessage);
        }

    }

    public class MinLengthConstraintNegativeCaseDifferentMessageSpecification: MinLengthBaseSpecification
    {
        protected string ErrorMessage = "Cusotm error message";
        protected override void Given()
        {
            Constraint = new MinLengthConstraint(3, ErrorMessage);
        }
        protected override void When()
        {
            Result = Constraint.IsSatisfied("12");
        }

        [Test()]
        public void ShouldPass()
        {
            Assert.IsTrue(!Result.Success);
        }
        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual(ErrorMessage, Result.ErrorMessage);
        }
    }

    public class MinLengthConstraintTestNullSpecification : MinLengthBaseSpecification
    {
        protected override void When()
        {
            Result = Constraint.IsSatisfied(null);
        }

        [Test()]
        public void ShouldPass()
        {
            Assert.IsTrue(Result.Success);
        }
    }


    public class MinLengthConstraintTestNotStringSpecification : MinLengthBaseSpecification
    {
        protected override void When()
        {
            Result = Constraint.IsSatisfied(new object());
        }

        [Test()]
        public void ShouldFail()
        {
            Assert.IsFalse(Result.Success);
        }
    }
}
