﻿using NUnit.Framework;
using System;
using myGovID.Presentation.Contract.Constraints;
using myGovID.Test;
using System.Collections.Generic;

namespace myGovID.Presentation.Contract.Test.Unit.Constraints.MultipleChoiceTests
{
    [TestFixture()]
    public abstract class MultipleChoiceConstraintBaseSpecification : TestSpecification
    {
        protected MultipleChoiceConstraint<string> Constraint;
        protected ConstraintValidationResult Result;
        protected string DefaultErrorMessage = "This field is invalid";

        protected void OptionsAre(params string[] options)
        {
            Constraint = new MultipleChoiceConstraint<string>(options);
        }

        protected void ValueIs(string value)
        {
            Result = Constraint.IsSatisfied(value);
        }
    }

    public class MultipleChoiceConstraintValidSpecification : MultipleChoiceConstraintBaseSpecification
    {
        protected override void Given()
        {
            OptionsAre("A", "B", "C");
        }

        protected override void When()
        {
            ValueIs("A");
        }

        [Test()]
        public void ShouldPass()
        {
            Assert.IsTrue(Result.Success);
        }
    }

    public class MultipleChoiceConstraintNullSpecification : MultipleChoiceConstraintBaseSpecification
    {
        protected override void Given()
        {
            OptionsAre("A", "B", "C");
        }

        protected override void When()
        {
            ValueIs(null);
        }

        [Test()]
        public void ShouldNotPass()
        {
            Assert.IsFalse(Result.Success);
        }

        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual(DefaultErrorMessage, Result.ErrorMessage);
        }
    }


    public class MultipleChoiceConstraintEmptyStringValidSpecification : MultipleChoiceConstraintBaseSpecification
    {
        protected override void Given()
        {
            OptionsAre("", "A", "B", "C");
        }

        protected override void When()
        {
            ValueIs("");
        }

        [Test()]
        public void ShouldPass()
        {
            Assert.IsTrue(Result.Success);
        }
    }

    public class MultipleChoiceConstraintEmptyStringInvalidSpecification : MultipleChoiceConstraintBaseSpecification
    {
        protected override void Given()
        {
            OptionsAre("A", "B", "C");
        }

        protected override void When()
        {
            ValueIs("");
        }

        [Test()]
        public void ShouldNotPass()
        {
            Assert.IsFalse(Result.Success);
        }

        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual(DefaultErrorMessage, Result.ErrorMessage);
        }
    }
    public class MultipleChoiceConstraintCustomErrorMessageSpecification : MultipleChoiceConstraintBaseSpecification
    {
        const string CustomErrorMessage = "Custom error message";

        protected override void Given()
        {
            Constraint = new MultipleChoiceConstraint<string>(Array.Empty<string>(), CustomErrorMessage);
        }

        protected override void When()
        {
            ValueIs(null);
        }

        [Test()]
        public void ShouldNotPass()
        {
            Assert.IsFalse(Result.Success);
        }

        [Test()]
        public void ShouldHaveCustomErrorMessage()
        {
            Assert.AreEqual(CustomErrorMessage, Result.ErrorMessage);
        }
    }
}
