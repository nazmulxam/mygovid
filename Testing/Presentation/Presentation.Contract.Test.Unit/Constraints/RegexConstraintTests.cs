﻿using NUnit.Framework;
using System.Text.RegularExpressions;
using myGovID.Presentation.Contract.Constraints;
using myGovID.Test;
namespace myGovID.Presentation.Contract.Test.Unit.Constraints.RegexTests
{
    [TestFixture()]
    public abstract class RegexConstraintBaseSpecification: TestSpecification 
    {
        protected Regex Regex  
        {
            get 
            {
                return new Regex("^(?! )[A-Za-z .,\\u2019\\u0027-]*(?<! )$");
            }
        } 
        protected RegexConstraint Constraint;
        protected ConstraintValidationResult Result;

        protected override void Given()
        {
            Constraint = new RegexConstraint(Regex);
        }
    }

    public class RegexConstraintValidSpecification: RegexConstraintBaseSpecification
    {
        protected override void When()
        {
            Result = Constraint.IsSatisfied("Test'Oconnor.,");
        }

        [Test()]
        public void ShouldPass()
        {
            Assert.IsTrue(Result.Success);
        }

    }

    public class RegexConstraintNegativeSpecification: RegexConstraintBaseSpecification
    {
        protected override void When()
        {
            Result = Constraint.IsSatisfied("Test'Oconnor.,1234567");
        }

        [Test()]
        public void ShouldNotPass()
        {
            Assert.IsTrue(!Result.Success);
        }
        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual("This field is invalid", Result.ErrorMessage);
        }

    }

    public class RegexConstraintNegativeDifferentMessageSpecification: RegexConstraintBaseSpecification
    {
        string ErrorMessage = "Custom error message";
        protected override void Given()
        {
            Constraint = new RegexConstraint(Regex, ErrorMessage);
        }
        protected override void When()
        {
            Result = Constraint.IsSatisfied("Test'Oconnor.,$%^#&");
        }

        [Test()]
        public void ShouldNotPass()
        {
            Assert.IsTrue(!Result.Success);
        }
        [Test()]
        public void ShouldHaveNewErrorMessage()
        {
            Assert.AreEqual(ErrorMessage, Result.ErrorMessage);
        }
    }

    public class RegexConstraintTestNullSpecification : RegexConstraintBaseSpecification
    {
        protected override void When()
        {
            Result = Constraint.IsSatisfied(null);
        }

        [Test()]
        public void ShouldPass()
        {
            Assert.IsTrue(Result.Success);
        }
    }

    public class RegexConstraintTestNotStringSpecification : RegexConstraintBaseSpecification
    {
        protected override void When()
        {
            Result = Constraint.IsSatisfied(new object());
        }

        [Test()]
        public void ShouldNotPass()
        {
            Assert.IsTrue(!Result.Success);
        }
    }
}
