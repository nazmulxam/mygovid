﻿using NUnit.Framework;
using System;
using myGovID.Presentation.Contract.Constraints;
using myGovID.Test;
namespace myGovID.Presentation.Contract.Test.Unit.Constraints.ImageSizeTests
{
    [TestFixture()]
    public abstract class ImageSizeInKBBaseSpecification : TestSpecification
    {
        protected string Base64Image = "iVBORw0KGgoAAAANSUhEUgAAAQcAAAC/CAMAAADEm+k5AAAAYFBMVEX///" +
                    "8AAAD8/Py7u7vy8vLf39++vr7a2tq3t7enp6fX19ekpKTGxsaqqqrCwsKZmZmOjo" +
                    "6BgYHt7e3MzMx6enrn5+eTk5NDQ0NQUFDS0tKxsbFeXl6dnZ2Hh4d9fX11dXVZsm8" +
                    "CAAABXUlEQVR4nO3cS3ObMBQG0GuBjTEPY7Db2o3r//8vo5DpqpOu0kA952zQgBafr" +
                    "h7sFAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPxHXn4el46wBsVms7ktHWIFTrkOp6VD" +
                    "rMAx12G/dIgVSOfvzgcAAAAAAAAAAAAAAAAAAAAAAAAAAAD4XJelA6xDXUaMaW6mqKqI" +
                    "8v39Pi0Y6usVeTmMv2/FvvYRP7q5WV//7JuetDR5GUyH/HwvQ4q2iXicU27Fdr4pO6V0aNs" +
                    "Yo+x2uUpN95TXiLf3qPPwop+nuYzqJWKo+1yGbdzLaxn7MraXIRW/qusj99m1USwc+V/" +
                    "YDm3RjLfudp/K07Y7TueI7nbOY+2Gb4drfR9yp3E4TI9iypXJa6JuyqVDf76Uh9mm4t7uY" +
                    "t80U9PnXZKq8vK2C95OgjTPfb+ffyfziqnaZzwf0t+PvbL/siQr8HEpxo8+vAJRQwhn8P" +
            "CphgAAAABJRU5ErkJggg==";
        protected ImageSizeInKBConstraint constraint;
        protected ConstraintValidationResult result;
    }


    public class ImageSizeInKBValidSpecification : ImageSizeInKBBaseSpecification
    {
        protected override void Given()
        {
            constraint = new ImageSizeInKBConstraint((float)0.515);
        }

        protected override void When()
        {
            result = constraint.IsSatisfied(Base64Image);
        }

        [Test()]
        public void ShouldPassImageSizeConstraint()
        {
            Assert.IsTrue(result.Success);
        }
    }

    public class ImageSizeInKBNegativeSpecification : ImageSizeInKBBaseSpecification
    {
        protected override void Given()
        {
            constraint = new ImageSizeInKBConstraint(0.2f);
        }

        protected override void When()
        {
            result = constraint.IsSatisfied(Base64Image);
        }

        [Test()]
        public void ShouldNotPassImageSizeConstraint()
        {
            Assert.IsTrue(!result.Success);
        }

        [Test()]
        public void ShouldContainErrorMessage()
        {
            Assert.AreEqual("Image size is more than 0.2 KB", result.ErrorMessage);
        }

    }

    public class ImageSizeInKBPositiveEdgeCaseSpecification : ImageSizeInKBBaseSpecification
    {
        protected override void Given()
        {
            constraint = new ImageSizeInKBConstraint((float)0.514);
        }

        protected override void When()
        {
            result = constraint.IsSatisfied(Base64Image);
        }

        [Test()]
        public void ShouldPassImageSizeConstraint()
        {
            Assert.IsTrue(result.Success);
        }

    }

    public class ImageSizeInKBExceedsLimitEdgeCaseSpecification : ImageSizeInKBBaseSpecification
    {
        protected override void Given()
        {
            constraint = new ImageSizeInKBConstraint((float)0.513);
        }

        protected override void When()
        {
            result = constraint.IsSatisfied(Base64Image);
        }

        [Test()]
        public void ShouldNotPassImageSizeConstraint()
        {
            Assert.IsTrue(!result.Success);
        }

        [Test()]
        public void ShouldContainErrorMessage()
        {
            Assert.AreEqual("Image size is more than 0.513 KB", result.ErrorMessage);
        }
    }


    public class ImageSizeInKBInvalidImageSpecification : ImageSizeInKBBaseSpecification
    {
        protected override void Given()
        {
            constraint = new ImageSizeInKBConstraint(0.2f);
        }

        protected override void When()
        {
            result = constraint.IsSatisfied("asdfg");
        }

        [Test()]
        public void ShouldNotPassImageSizeConstraint()
        {
            Assert.IsTrue(!result.Success);
        }

        [Test()]
        public void ShouldContainErrorMessage()
        {
            Assert.AreEqual("Invalid image", result.ErrorMessage);
        }

    }

}