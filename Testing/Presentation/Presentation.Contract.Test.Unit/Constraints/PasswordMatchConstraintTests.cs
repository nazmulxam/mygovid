﻿using NUnit.Framework;
using System;
using myGovID.Presentation.Contract.Constraints;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Test;
namespace myGovID.Presentation.Contract.Test.Unit.Constraints.PasswordMatchTests
{
    [TestFixture()]
    public abstract class PasswordConstraintSpecification : TestSpecification
    {
        protected PasswordInput Password;
        protected PasswordMatchConstraint PasswordMatchConstraint;
        protected ConstraintValidationResult result;

        protected override void Given()
        {
            Password = new PasswordInput("password", "id", 8, 60)
            {
                Value = "Password!1"
            };
            PasswordMatchConstraint = new PasswordMatchConstraint(Password);
        }
    }


    public class PasswordMatchValidSpecification : PasswordConstraintSpecification
    {
        protected override void When()
        {
            result = PasswordMatchConstraint.IsSatisfied("Password!1");
        }

        [Test()]
        public void ShouldSucceed()
        {
            Assert.IsTrue(result.Success);
        }
    }

    public class PasswordNotMatchSpecification : PasswordConstraintSpecification
    {
        protected override void When()
        {
            result = PasswordMatchConstraint.IsSatisfied("Password11");
        }

        [Test()]
        public void ShouldNotSucceed()
        {
            Assert.IsTrue(!result.Success);
        }

        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual("Passwords do not match", result.ErrorMessage);
        }
    }

    public class PasswordNotMatchCaseSensitiveSpecification : PasswordConstraintSpecification
    {
        protected override void When()
        {
            result = PasswordMatchConstraint.IsSatisfied("password!1");
        }

        [Test()]
        public void ShouldNotSucceed()
        {
            Assert.IsTrue(!result.Success);
        }

        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual("Passwords do not match", result.ErrorMessage);
        }
    }

    public class PasswordNullSpecification : PasswordConstraintSpecification
    {
        protected override void When()
        {
            result = PasswordMatchConstraint.IsSatisfied(null);
        }

        [Test()]
        public void ShouldNotSucceed()
        {
            Assert.IsTrue(!result.Success);
        }

        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual("Passwords do not match", result.ErrorMessage);
        }
    }

    public class PasswordEmptySpecification : PasswordConstraintSpecification
    {
        protected override void When()
        {
            result = PasswordMatchConstraint.IsSatisfied("");
        }

        [Test()]
        public void ShouldNotSucceed()
        {
            Assert.IsTrue(!result.Success);
        }

        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual("Passwords do not match", result.ErrorMessage);
        }
    }
    public class PasswordWithLeadingSpaceSpecification : PasswordConstraintSpecification
    {
        protected override void When()
        {
            result = PasswordMatchConstraint.IsSatisfied(" Password!1");
        }

        [Test()]
        public void ShouldNotSucceed()
        {
            Assert.IsTrue(!result.Success);
        }

        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual("Passwords do not match", result.ErrorMessage);
        }
    }
    public class PasswordWithTrailingSpaceSpecification : PasswordConstraintSpecification
    {
        protected override void When()
        {
            result = PasswordMatchConstraint.IsSatisfied("Password!1 ");
        }

        [Test()]
        public void ShouldNotSucceed()
        {
            Assert.IsTrue(!result.Success);
        }

        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual("Passwords do not match", result.ErrorMessage);
        }
    }
}
