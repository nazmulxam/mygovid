﻿using NUnit.Framework;
using System;
using myGovID.Presentation.Contract.Constraints;
using myGovID.Test;
namespace myGovID.Presentation.Contract.Test.Unit.Constraints.MaxLengthTests
{
    [TestFixture()]
    public abstract class MaxLengthConstraintBaseSpecification : TestSpecification
    {
        protected MaxLengthConstraint Constraint;
        protected ConstraintValidationResult Result;

        protected override void Given()
        {
            Constraint = new MaxLengthConstraint(10);
        }
    }

    public  class MaxLengthConstraintValidSpecification: MaxLengthConstraintBaseSpecification
    {
        protected override void When()
        {
            Result = Constraint.IsSatisfied("123456789");
        }

        [Test()]
        public void ShouldPass()
        {
            Assert.IsTrue(Result.Success);
        }
    }

    public class MaxLengthConstraintValidEdgeCaseSpecification: MaxLengthConstraintBaseSpecification
    {
        protected override void When()
        {
            Result = Constraint.IsSatisfied("1234567890");
        }

        [Test()]
        public void ShouldPass()
        {
            Assert.IsTrue(Result.Success);
        }


    }
    public class MaxLengthConstraintWrongInputSpecification: MaxLengthConstraintBaseSpecification
    {
        protected override void When()
        {
            Result = Constraint.IsSatisfied("12345678901");
        }

        [Test()]
        public void ShouldNotPass()
        {
            Assert.IsTrue(!Result.Success);
        }

        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual("Value provided is too long", Result.ErrorMessage);
        }
    }

    public class MaxLengthConstraintNullInputSpecification : MaxLengthConstraintBaseSpecification
    {
        protected override void When()
        {
            Result = Constraint.IsSatisfied(null);
        }

        [Test()]
        public void ShouldPass()
        {
            Assert.IsTrue(Result.Success);
        }
    }

    public class MaxLengthConstraintEmptyInputSpecification : MaxLengthConstraintBaseSpecification
    {
        protected override void When()
        {
            Result = Constraint.IsSatisfied("");
        }

        [Test()]
        public void ShouldPass()
        {
            Assert.IsTrue(Result.Success);
        }
    }

    public class MaxLengthConstraintCustomErrorMessageSpecification: MaxLengthConstraintBaseSpecification
    {
        string ErrorMessage = "Custom error message";

        protected override void Given()
        {
            Constraint = new MaxLengthConstraint(10, ErrorMessage);
        }

        protected override void When()
        {
            Result = Constraint.IsSatisfied("asd123!@$#@*&^%&*&");
        }

        [Test()]
        public void ShouldNotPass()
        {
            Assert.IsTrue(!Result.Success);
        }

        [Test()]
        public void ShouldHaveErrorMessage()
        {
            Assert.AreEqual(ErrorMessage, Result.ErrorMessage);
        }
    }

}
