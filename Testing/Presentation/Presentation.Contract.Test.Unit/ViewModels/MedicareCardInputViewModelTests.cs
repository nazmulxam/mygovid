﻿using System.Collections.Generic;
using System.Linq;
using myGovID.Presentation.Contract.Inputs;
using myGovID.Presentation.Contract.Steps.MedicareCardInput;
using myGovID.Presentation.Contract.Test.Unit.ViewModels.MedicareCardInputViewModelTests;
using myGovID.Test;
using NUnit.Framework;

namespace myGovID.Presentation.Contract.Test.Unit.ViewModels.MedicareCardInputViewModelTests
{
    [TestFixture()]
    public abstract class MedicareCardInputViewModelSpecification : TestSpecification
    {
        protected MedicareCardInputViewModel ViewModel;

        protected IEnumerable<MedicareCardNameInput> NameInputs
        {
            get => new List<MedicareCardNameInput>
            {
                ViewModel.NameLine1,
                ViewModel.NameLine2,
                ViewModel.NameLine3,
                ViewModel.NameLine4
            };
        }

        protected IEnumerable<MedicareCardNameInput> VisibleNameInputs
        {
            get => NameInputs.Where((input) => input.Visible).ToList();
        }

        protected void VanillaViewModel()
        {
            ViewModel = new MedicareCardInputViewModel(false);
        }

        protected void ViewModelWithVisibleNameFields(int count)
        {
            ViewModel = new MedicareCardInputViewModel(false);
            foreach (var input in NameInputs)
            {
                input.Visible = --count >= 0;
            }
        }

        protected void ViewModelWithCardTypeAndExpiry(string cardType, string expiry)
        {
            ViewModel = new MedicareCardInputViewModel(false);
            ViewModel.CardType.Value = cardType;
            ViewModel.Expiry.Value = expiry;
        }

        protected void UserTappsAddNameField(int times)
        {
            while (--times >= 0)
            {
                ViewModel.AddNameButton.OnTapped.Execute(null);
            }
        }

        protected void UserTappsRemoveNameField(int times)
        {
            while (--times >= 0)
            {
                ViewModel.RemoveNameButton.OnTapped.Execute(null);
            }
        }

        protected void UserSelectsCardType(string cardType)
        {
            ViewModel.CardType.Value = cardType;
        }
    }



    public class WhenInInitialState : MedicareCardInputViewModelSpecification
    {
        protected override void Given()
        {
            VanillaViewModel();
        }

        protected override void When()
        {
            // Nothing has happened yet
        }

        [Test()]
        public void ShouldHaveOneNameFieldVisible()
        {
            Assert.That(VisibleNameInputs, Has.Count.EqualTo(1));
        }
    }

    public abstract class GivenFreshViewModel : MedicareCardInputViewModelSpecification
    {
        protected override void Given() => VanillaViewModel();

        public class WhenInitialised : GivenFreshViewModel
        {
            protected override void When() { }

            [Test()]
            public void ShouldHaveOneNameFieldVisible()
            {
                Assert.That(VisibleNameInputs, Has.Count.EqualTo(1));
            }

            [Test()]
            public void ShouldHaveCorrectExpiryDatePlaceholder()
            {
                Assert.That(ViewModel.Expiry.Placeholder, Is.EqualTo("mm/yyyy"));
            }

            [Test()]
            public void ShouldHaveCorrectCardTypePlaceholder()
            {
                Assert.That(ViewModel.CardType.Placeholder, Is.EqualTo("Colour"));
            }

            [Test()]
            public void ShouldHaveCorrectIrnPlaceholder()
            {
                Assert.That(ViewModel.Irn.Placeholder, Is.EqualTo("Individual reference number"));
            }
        }

        public class WhenUserTapsAddName : GivenFreshViewModel
        {
            protected override void When() => UserTappsAddNameField(times: 1);

            [Test()]
            public void ShouldHaveTwoNameFieldsVisible()
            {
                Assert.That(VisibleNameInputs, Has.Count.EqualTo(2));
            }
        }

        public class WhenUserTapsRemoveName : GivenFreshViewModel
        {
            protected override void When() => UserTappsRemoveNameField(times: 1);

            [Test()]
            public void ShouldHaveOneNameFieldVisible()
            {
                Assert.That(VisibleNameInputs, Has.Count.EqualTo(1));
            }
        }

        public class WhenUserSelectsGreenCardType : GivenFreshViewModel
        {
            protected override void When() => UserSelectsCardType(MedicareCardType.Green);

            [Test()]
            public void ShouldHaveCorrectExpiryDatePlaceholder()
            {
                Assert.That(ViewModel.Expiry.Placeholder, Is.EqualTo("mm/yyyy"));
            }

            [Test()]
            public void ShouldHaveCorrectExpiryDateFormat()
            {
                Assert.That(ViewModel.Expiry.DateFormat, Is.EqualTo(DateFormat.GreenMedicareCardExpiry));
            }
        }

        public class WhenUserSelectsYellowCardType : GivenFreshViewModel
        {
            protected override void When() => UserSelectsCardType(MedicareCardType.Yellow);

            [Test()]
            public void ShouldHaveCorrectExpiryDatePlaceholder()
            {
                Assert.That(ViewModel.Expiry.Placeholder, Is.EqualTo("dd/mm/yyyy"));
            }

            [Test()]
            public void ShouldHaveCorrectExpiryDateFormat()
            {
                Assert.That(ViewModel.Expiry.DateFormat, Is.EqualTo(DateFormat.YellowMedicareCardExpiry));
            }
        }

        public class WhenUserSelectsBlueCardType : GivenFreshViewModel
        {
            protected override void When() => UserSelectsCardType(MedicareCardType.Blue);

            [Test()]
            public void ShouldHaveCorrectExpiryDatePlaceholder()
            {
                Assert.That(ViewModel.Expiry.Placeholder, Is.EqualTo("dd/mm/yyyy"));
            }

            [Test()]
            public void ShouldHaveCorrectExpiryDateFormat()
            {
                Assert.That(ViewModel.Expiry.DateFormat, Is.EqualTo(DateFormat.BlueMedicareCardExpiry));
            }
        }
    }

    #region Name inputs

    public abstract class GivenViewModelWithTwoVisibleNameFields : MedicareCardInputViewModelSpecification
    {
        protected override void Given() => ViewModelWithVisibleNameFields(2);

        public class WhenUserTapsAddName : GivenViewModelWithTwoVisibleNameFields
        {
            protected override void When() => UserTappsAddNameField(times: 1);

            [Test()]
            public void ShouldHaveThreeNameFieldsVisible()
            {
                Assert.That(VisibleNameInputs, Has.Count.EqualTo(3));
            }
        }

        public class WhenUserTapsAddNameTwice : GivenViewModelWithTwoVisibleNameFields
        {
            protected override void When() => UserTappsAddNameField(times: 2);

            [Test()]
            public void ShouldHaveThreeNameFieldsVisible()
            {
                Assert.That(VisibleNameInputs, Has.Count.EqualTo(4));
            }
        }

        public class WhenUserTapsRemoveNameTwice : GivenViewModelWithTwoVisibleNameFields
        {
            protected override void When() => UserTappsRemoveNameField(times: 2);

            [Test()]
            public void ShouldHaveOneNameFieldVisible()
            {
                Assert.That(VisibleNameInputs, Has.Count.EqualTo(1));
            }
        }
    }

    public abstract class GivenViewModelWithFourVisibleNameFields : MedicareCardInputViewModelSpecification
    {
        protected override void Given() => ViewModelWithVisibleNameFields(4);

        public class WhenUserTapsAddName : GivenViewModelWithFourVisibleNameFields
        {
            protected override void When() => UserTappsAddNameField(times: 1);

            [Test()]
            public void ShouldHaveFourNameFieldsVisible()
            {
                Assert.That(VisibleNameInputs, Has.Count.EqualTo(4));
            }
        }

        public class WhenUserTapsRemoveName : GivenViewModelWithFourVisibleNameFields
        {
            protected override void When() => UserTappsRemoveNameField(times: 1);

            [Test()]
            public void ShouldHaveThreeNameFieldsVisible()
            {
                Assert.That(VisibleNameInputs, Has.Count.EqualTo(3));
            }
        }
    }

    #endregion

    #region Card type and expiry

    public abstract class GivenViewModelGreenCardExpiryDate : MedicareCardInputViewModelSpecification
    {
        protected override void Given() => ViewModelWithCardTypeAndExpiry(MedicareCardType.Green, "01/2025");

        public class WhenUserChangesToYellowCard : GivenViewModelGreenCardExpiryDate
        {
            protected override void When() => UserSelectsCardType(MedicareCardType.Yellow);

            [Test()]
            public void ShouldClearExpiryDateValue()
            {
                Assert.That(ViewModel.Expiry.Value, Is.Empty);
            }
        }
    }

    #endregion
}
