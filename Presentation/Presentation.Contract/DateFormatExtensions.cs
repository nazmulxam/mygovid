﻿using System;
namespace myGovID.Presentation.Contract
{
    public static class DateFormatExtensions
    {
        public static string GetFormat(this DateFormat dateFormat)
        {
            switch (dateFormat)
            {
                case DateFormat.DateOfBirth:
                case DateFormat.BlueMedicareCardExpiry:
                case DateFormat.YellowMedicareCardExpiry:
                    return "dd/MM/yyyy";
                case DateFormat.GreenMedicareCardExpiry:
                    return "MM/yyyy";
                default:
                    throw new NotSupportedException("Date format is not supported " + dateFormat);
            }
        }
    }
}
