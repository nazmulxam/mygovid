﻿using Newtonsoft.Json;

namespace myGovID.Presentation.Contract
{
    public class PresentationError
    {
        public string Code { get; }
        public string Title { get; }
        public string Description { get; }
        [JsonConstructor]
        public PresentationError(string code, string title, string description)
        {
            Code = code;
            Title = title;
            Description = description;
        }
    }
}