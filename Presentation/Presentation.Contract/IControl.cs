﻿namespace myGovID.Presentation.Contract
{
    /// <summary>
    /// IControl represents a UI element rendered on screen that
    /// a user interacts with in some way
    /// </summary>
    public interface IControl
    {
        /// <summary>
        /// Gets the element's label.
        /// </summary>
        /// <value>The visibility.</value>
        string Label { get; set; }

        /// <summary>
        /// Gets the accessibility identifier.
        /// </summary>
        /// <value>The accessibility identifier.</value>
        string AccessibilityIdentifier { get; set; }

        /// <summary>
        /// Gets the element's visibility.
        /// </summary>
        /// <value>The visibility.</value>
        bool Visible { get; set; }

        /// <summary>
        /// Whether this element is enabled for interaction.
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        bool Enabled { get; set; }
    }
}
