﻿using GalaSoft.MvvmLight;
namespace myGovID.Presentation.Contract
{
    public class BaseControl : ViewModelBase, IControl
    {
        private string _label;
        public string Label
        {
            get => _label;
            set { Set(nameof(Label), ref _label, value); }
        }

        private string _accessibilityIdentifier;
        public string AccessibilityIdentifier
        {
            get => _accessibilityIdentifier;
            set { Set(nameof(AccessibilityIdentifier), ref _accessibilityIdentifier, value); }
        }

        private bool _visible = true;
        public bool Visible
        {
            get => _visible;
            set { Set(nameof(Visible), ref _visible, value); }
        }

        private bool _enabled = true;
        public bool Enabled
        {
            get => _enabled;
            set { Set(nameof(Enabled), ref _enabled, value); }
        }
    }
}
