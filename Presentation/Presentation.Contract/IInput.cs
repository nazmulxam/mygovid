﻿using System.Collections.Generic;
namespace myGovID.Presentation.Contract
{
    public interface IInput : IControl
    {
        /// <summary>
        /// Gets the list of constraints.
        /// </summary>
        /// <value>The constraints.</value>
        IEnumerable<IConstraint<ConstraintType>> Constraints { get; }
        /// <summary>
        /// Gets the meta data attached to the input
        /// </summary>
        /// <value>The meta data.</value>
        InputMetaData MetaData { get; }
        /// <summary>
        /// Method that checks if the input is valid.
        /// </summary>
        /// <returns><c>true</c>, if valid <c>false</c> otherwise.</returns>
        bool IsValid();
        /// <summary>
        /// Creates input value from string.
        /// </summary>
        /// <param name="value">Value.</param>
        void FromString(string value);
    }
}
