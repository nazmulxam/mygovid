﻿using System;
namespace myGovID.Presentation.Contract
{
    public class ConstraintValidationResult
    {
        public bool Success { get; }
        public string ErrorMessage { get; }

        public ConstraintValidationResult(bool success, string errorMessage)
        {
            Success = success;
            ErrorMessage = errorMessage;
        }
    }
}
