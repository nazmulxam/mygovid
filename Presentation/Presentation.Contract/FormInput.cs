using EnsureThat;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;

namespace myGovID.Presentation.Contract
{
    public abstract class FormInput<T> : BaseControl, IInput where T : IComparable
    {
        public InputMetaData MetaData { get; protected set; }
        public IEnumerable<IConstraint<ConstraintType>> Constraints { get; protected set; }

        private T _value;
        public T Value
        {
            get => _value;
            set
            {
                if (value?.Equals(_value) ?? false)
                {
                    return;
                }
                Set(nameof(Value), ref _value, value);
                Error = null;
            }
        }

        private string _error;
        public string Error
        {
            get => _error;
            set
            {
                Set(nameof(Error), ref _error, value);
            }
        }


        private string _placeholder;
        public string Placeholder
        {
            get => _placeholder;
            set { Set(nameof(Placeholder), ref _placeholder, value); }
        }

        protected FormInput(string label, string accessibilityId, InputMetaData metadata) : this(label, label, accessibilityId, metadata) { }

        protected FormInput(string label, string placeholder, string accessibilityId, InputMetaData metadata)
        {
            Ensure.String.IsNotNull(label, nameof(label));
            Ensure.String.IsNotNullOrEmpty(accessibilityId, nameof(accessibilityId));

            Label = label;
            Placeholder = placeholder;
            AccessibilityIdentifier = accessibilityId;
            MetaData = metadata;
        }

        public virtual bool IsValid()
        {
            if (Constraints != null)
            {
                foreach (var constraint in Constraints)
                {
                    ConstraintValidationResult result = constraint.IsSatisfied(Value);
                    if (!result.Success)
                    {
                        Error = result.ErrorMessage;
                        return false;
                    }
                }
            }
            Error = null;
            return true;
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        public abstract void FromString(string value);
    }
}
