﻿using System;
namespace myGovID.Presentation.Contract
{
    /// <summary>
    /// Supported image types
    /// </summary>
    public enum ImageType
    {
        JPG, PNG, BMP
    }
}
