﻿namespace myGovID.Presentation.Contract
{
    /// <summary>
    /// Action type.
    /// </summary>
    public enum ActionType
    {
        Clone, Next, Cancel, Previous, Browse
    }
}