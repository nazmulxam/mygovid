﻿namespace myGovID.Presentation.Contract.Constraints
{
    public class MaxLengthConstraint : IConstraint<ConstraintType>
    {
        protected string ErrorMsg { get; }
        public ConstraintType Type => ConstraintType.MaxLength;
        public int Value { get; }
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:myGovID.Presentation.Contract.Constraints.MaxLengthConstraint"/> class.
        /// </summary>
        /// <param name="value">Maximum supported length of a string</param>
        /// <param name="errorMsg">Error message to be displayed if constraint fails</param>
        public MaxLengthConstraint(int value, string errorMsg = "Value provided is too long")
        {
            Value = value;
            ErrorMsg = errorMsg;
        }
        public ConstraintValidationResult IsSatisfied(object value)
        {
            string valueAsString = (string) value;
            bool success = valueAsString == null || (valueAsString != null && valueAsString.Length <= Value);
            return new ConstraintValidationResult(success, success ? "" : ErrorMsg);
        }
    }
}
