﻿using System;
using myGovID.Presentation.Contract.Inputs;
namespace myGovID.Presentation.Contract.Constraints
{
    public class PasswordMatchConstraint: IConstraint<ConstraintType>
    {
        public ConstraintType Type => ConstraintType.Equals;
        private readonly PasswordInput _input;
        private const string ErrorMsg = "Passwords do not match";
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:myGovID.Presentation.Contract.Constraints.PasswordMatchConstraint"/> class.
        /// </summary>
        /// <param name="input">Input against which password should be matched</param>
        public PasswordMatchConstraint(PasswordInput input)
        {
            _input = input ?? throw new ArgumentNullException(nameof(input));
        }
        public ConstraintValidationResult IsSatisfied(object value)
        {
            bool success = false;
            if(value is string) {
                success = value.Equals(_input.Value);
            }
            return new ConstraintValidationResult(success, success ? "" : ErrorMsg);
        }
    }
}
