﻿using System;
namespace myGovID.Presentation.Contract.Constraints
{
    public class AgeInYearsConstraint : DateRangeConstraint
    {
        public new ConstraintType Type => ConstraintType.AgeInYears;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:myGovID.Presentation.Contract.Constraints.AgeInYearsConstraint"/> class.
        /// </summary>
        /// <param name="dateFormat">Date format of the input date</param>
        /// <param name="minAge">Minimum age required to satisfy the date range constraint</param>
        /// <param name="maxAge">Max age required to satisfy the date range constraint</param>
        /// <param name="errorMsg">Error message to be shown if the constraint fails</param>
        public AgeInYearsConstraint(DateFormat dateFormat, 
                                   int minAge, int maxAge,
                                   string errorMsg = "This field is invalid")
            : base(dateFormat,
                   DateTime.Now.Date.AddYears(-maxAge),
                   DateTime.Now.Date.AddYears(-minAge))
        {
            if (minAge >= maxAge)
            {
                throw new ArgumentException("Minimum required age must be less than required maximum age", nameof(minAge));
            }
        }
    }
}
