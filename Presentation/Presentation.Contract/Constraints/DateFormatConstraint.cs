﻿using System;
using System.Globalization;
namespace myGovID.Presentation.Contract.Constraints
{
    public class DateFormatConstraint : IConstraint<ConstraintType>
    {
        protected string ErrorMsg { get; }
        public DateFormat DateFormat { get; }
        public ConstraintType Type => ConstraintType.DateFormat;
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:myGovID.Presentation.Contract.Constraints.DateFormatConstraint"/> class.
        /// </summary>
        /// <param name="dateFormat">Date format aganist which constraint should be validated</param>
        /// <param name="errorMsg">Error message to be shown when constraint is not met</param>
        public DateFormatConstraint(DateFormat dateFormat, string errorMsg = "Date format is incorrect")
        {
            ErrorMsg = errorMsg;
            DateFormat = dateFormat;
        }
        public ConstraintValidationResult IsSatisfied(object value)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            bool success = DateTime.TryParseExact((string) value, DateFormat.GetFormat(), provider, DateTimeStyles.None, out DateTime dateTime);
            return new ConstraintValidationResult(success, success ? "" : ErrorMsg);
        }
    }
}
