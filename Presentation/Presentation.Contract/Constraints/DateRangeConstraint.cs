﻿using System;
using System.Globalization;
namespace myGovID.Presentation.Contract.Constraints
{
    public class DateRangeConstraint : IConstraint<ConstraintType>
    {
        public ConstraintType Type => ConstraintType.DateRange;
        protected string ErrorMsg { get; }
        protected DateFormat DateFormat { get; }
        private readonly DateTime _minDate;
        private readonly DateTime _maxDate;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:myGovID.Presentation.Contract.Constraints.DateRangeConstraint"/> class.
        /// </summary>
        /// <param name="dateFormat">Date format of the input date</param>
        /// <param name="minDate">Minimum date that can satisfy the date range constraint</param>
        /// <param name="maxDate">Maximum date that can satisfy the date range constraint</param>
        /// <param name="errorMsg">Error message to be shown if the constraint fails</param>
        public DateRangeConstraint(DateFormat dateFormat,
                                   DateTime minDate, DateTime maxDate,
                                   string errorMsg = "This field is invalid")
        {
            if (minDate >= maxDate)
            {
                throw new ArgumentException("Minimum date must be before maximum date", nameof(minDate));
            }

            ErrorMsg = errorMsg;
            DateFormat = dateFormat;
            // Normalise dates based on the format of the value we'll be comparing with
            _minDate = normaliseDateWithFormat(minDate, dateFormat);
            _maxDate = normaliseDateWithFormat(maxDate, dateFormat);
        }

        public ConstraintValidationResult IsSatisfied(object value)
        {
            bool success = DateTime.TryParseExact(
                (string) value,
                DateFormat.GetFormat(),
                CultureInfo.InvariantCulture,
                DateTimeStyles.None,
                out DateTime dateTime);

            if (success)
            {
                success = dateTime <= _maxDate && dateTime >= _minDate;
            }

            return new ConstraintValidationResult(success, success ? "" : ErrorMsg);
        }

        private DateTime normaliseDateWithFormat(DateTime date, DateFormat format)
        {
            var parsed = date.ToString(format.GetFormat());
            return DateTime.ParseExact(
                parsed,
                format.GetFormat(),
                CultureInfo.InvariantCulture,
                DateTimeStyles.None);
        }
    }
}
 