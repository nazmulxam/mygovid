﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace myGovID.Presentation.Contract.Constraints
{
    public class ConditionalConstraintGroup : IConstraint<ConstraintType>
    {
        public ConstraintType Type => ConstraintType.Group;

        private readonly Func<object, bool> _condition;
        private readonly IEnumerable<IConstraint<ConstraintType>> _constraints;

        public ConditionalConstraintGroup(Func<object, bool> condition, params IConstraint<ConstraintType>[] constraints)
        {
            _condition = condition;
            _constraints = constraints;
        }

        public ConstraintValidationResult IsSatisfied(object value)
        {
            if (_condition(value))
            {
                foreach (var constraint in _constraints)
                {
                    var result = constraint.IsSatisfied(value);
                    if (!result.Success)
                    {
                        return result;
                    }
                }
            }

            return new ConstraintValidationResult(true, "");
        }
    }
}
