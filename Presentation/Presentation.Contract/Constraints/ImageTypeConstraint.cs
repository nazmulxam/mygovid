﻿using System;
namespace myGovID.Presentation.Contract.Constraints
{
    public class ImageTypeConstraint : IConstraint<ConstraintType>
    {
        public ConstraintType Type => ConstraintType.ImageType;
        public ImageType Value { get; }
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:myGovID.Presentation.Contract.Constraints.ImageTypeConstraint"/> class.
        /// </summary>
        /// <param name="value">Supported image type.</param>
        public ImageTypeConstraint(ImageType value)
        {
            Value = value;
        }
        public ConstraintValidationResult IsSatisfied(object value)
        {
            byte[] imageTypeBytes = Value.GetBytes();
            byte[] imageBytes = null;
            int bytesToReadFromStream = imageTypeBytes.Length;
            try {
                imageBytes = Convert.FromBase64String((string)value);
            } catch (ArgumentNullException)
            {
                return new ConstraintValidationResult(false, "Invalid image");
            } catch (FormatException)
            {
                return new ConstraintValidationResult(false, "Invalid image");
            }
             
            if (imageBytes.Length < bytesToReadFromStream) {
                return new ConstraintValidationResult(false, "Unsupported image type (" + Value.ToString() + ")");
            }
            var bytes = new byte[bytesToReadFromStream];
            Array.Copy(imageBytes, bytes, bytes.Length);
            for (int index = 0; index < bytesToReadFromStream; index++)
            {
                if(imageTypeBytes[index] != bytes[index])
                {
                    return new ConstraintValidationResult(false, "Unsupported image type (" + Value.ToString() + ")");
                }
            }
            return new ConstraintValidationResult(true, "");
        }
    }
}
