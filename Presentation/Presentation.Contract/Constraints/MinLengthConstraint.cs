﻿namespace myGovID.Presentation.Contract.Constraints
{
    public class MinLengthConstraint : IConstraint<ConstraintType>
    {
        protected string ErrorMsg { get; }
        public ConstraintType Type => ConstraintType.MinLength;
        public int Value { get; }
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:myGovID.Presentation.Contract.Constraints.MinLengthConstraint"/> class.
        /// </summary>
        /// <param name="value">Minimum required length of a string.</param>
        /// <param name="errorMsg">Error message to be displayed if constraint fails.</param>
        public MinLengthConstraint(int value, string errorMsg = "Value provided is too short")
        {
            Value = value;
            ErrorMsg = errorMsg;
        }
        public ConstraintValidationResult IsSatisfied(object value)
        {
            bool success = value == null || (value is string && (value as string).Length >= Value);
            return new ConstraintValidationResult(success, success ? "" : ErrorMsg);
        }
    }
}
