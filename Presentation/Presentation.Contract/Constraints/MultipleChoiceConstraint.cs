﻿using System;
using System.Collections.Generic;

namespace myGovID.Presentation.Contract.Constraints
{
    /// <summary>
    /// MultipleChoiceConstraint restricts inputs to a predefined set of options
    /// </summary>
    public class MultipleChoiceConstraint<T> : IConstraint<ConstraintType>
    {
        protected string ErrorMsg { get; }
        protected IEnumerable<T> Options { get; }
        protected bool AllowsMultpleSelection { get; }

        public ConstraintType Type => ConstraintType.MultipleChoice;

        /// <summary>
        /// MultipleChoiceConstraint restricts inputs to a defined set of options
        /// </summary>
        /// <param name="options">The valid set of options</param>
        public MultipleChoiceConstraint(IEnumerable<T> options) : this(options, "This field is invalid")
        {
        }

        /// <summary>
        /// MultipleChoiceConstraint restricts inputs to a defined set of options
        /// </summary>
        /// <param name="options">The valid set of options</param>
        /// <param name="errorMsg">Error message to display if the constraint fails</param>
        public MultipleChoiceConstraint(IEnumerable<T> options, string errorMsg)
        {
            Options = options;
            ErrorMsg = errorMsg;
        }

        public ConstraintValidationResult IsSatisfied(object value)
        {
            var success = value is T && new HashSet<T>(Options).Contains((T)value);
            return new ConstraintValidationResult(success, success ? "" : ErrorMsg);
        }
    }
}