﻿using System;
namespace myGovID.Presentation.Contract.Constraints
{
    public class MandatoryConstraint : IConstraint<ConstraintType>
    {
        protected string ErrorMsg { get; }
        public ConstraintType Type => ConstraintType.MinLength;
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:myGovID.Presentation.Contract.Constraints.MandatoryConstraint"/> class.
        /// </summary>
        /// <param name="errorMsg">Error message to display if the mandatory constraint fails</param>
        public MandatoryConstraint(string errorMsg = "This field is required")
        {
            ErrorMsg = errorMsg;
        }
        public ConstraintValidationResult IsSatisfied(object value)
        {
            string valueAsString = (string)value;
            bool success = !string.IsNullOrEmpty(valueAsString);
            return new ConstraintValidationResult(success, success ? "" : ErrorMsg);
        }
    }
}
