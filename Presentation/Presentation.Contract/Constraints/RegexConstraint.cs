﻿using System;
using System.Text.RegularExpressions;
namespace myGovID.Presentation.Contract.Constraints
{
    public class RegexConstraint : IConstraint<ConstraintType>
    {
        protected string ErrorMsg { get; }
        public ConstraintType Type => ConstraintType.Regex;
        private readonly Regex _regex;
        /// <summary>
        /// Initializes a new instance of the <see cref="T:myGovID.Presentation.Contract.Constraints.RegexConstraint"/> class.
        /// </summary>
        /// <param name="regex">Regex against which string must match</param>
        /// <param name="errorMsg">Error message to be displayed if constraint fails.</param>
        public RegexConstraint(Regex regex, string errorMsg = "This field is invalid")
        {
            _regex = regex ?? throw new ArgumentNullException(nameof(regex));
            ErrorMsg = errorMsg;
        }
        public virtual ConstraintValidationResult IsSatisfied(object value)
        {
            bool success = value == null || (value is string && _regex.Match(value as string).Success);
            return new ConstraintValidationResult(success, success ? "" : ErrorMsg);
        }
    }
}
