﻿using System;
namespace myGovID.Presentation.Contract.Constraints
{
    public class BooleanValueConstraint : IConstraint<ConstraintType>
    {
        protected string ErrorMsg { get; }
        private bool ValueToBe { get;  }
        public ConstraintType Type => ConstraintType.Equals;
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:myGovID.Presentation.Contract.Constraints.BooleanValueConstraint"/> class.
        /// </summary>
        /// <param name="valueToBe">Value to validate against</param>
        /// <param name="errorMsg">Error message to display if the Boolean constraint fails</param>
        public BooleanValueConstraint(bool valueToBe,string errorMsg = "This field is invalid")
        {
            ErrorMsg = errorMsg;
            ValueToBe = valueToBe;
        }
        public ConstraintValidationResult IsSatisfied(object value)
        {
            bool valueAsBool = (bool)value;
            bool success = valueAsBool == ValueToBe;
            return new ConstraintValidationResult(success, success ? "" : ErrorMsg);
        }
    }
}
