﻿using System;
namespace myGovID.Presentation.Contract.Constraints
{
    public class ImageSizeInKBConstraint : IConstraint<ConstraintType>
    {
        public ConstraintType Type => ConstraintType.ImageSizeInKB;
        private readonly float _value;
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:myGovID.Presentation.Contract.Constraints.ImageSizeInKBConstraint"/> class.
        /// </summary>
        /// <param name="value">Max image size in KB.</param>
        public ImageSizeInKBConstraint(float value)
        {
            _value = value;
        }
        public ConstraintValidationResult IsSatisfied(object value)
        {
            byte[] imageBytes = null;
            try
            {
                imageBytes = Convert.FromBase64String((string)value);
            }
            catch (ArgumentNullException)
            {
                return new ConstraintValidationResult(false, "Invalid image");
            }
            catch (FormatException)
            {
                return new ConstraintValidationResult(false, "Invalid image");
            }
            int len = imageBytes.Length;
            float sizeKb = (float)(imageBytes.Length * 0.001);
            bool success = sizeKb <= _value;
            return new ConstraintValidationResult(success, success ? "" : "Image size is more than " + _value + " KB");
        }
    }
}
