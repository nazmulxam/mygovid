﻿using System;
using System.Threading.Tasks;

namespace myGovID.Presentation.Contract
{
    public interface IAction<T>: IAction<T, Enum>
    {}
    public interface IAction<T, S>
    {
        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>The type.</value>
        T Type { get; }
        /// <summary>
        /// Gets the subtype of action.
        /// </summary>
        /// <value>The sub type of action</value>
        S SubType { get; }
        /// <summary>
        /// Gets the accessibility identifier.
        /// </summary>
        /// <value>The accessibility identifier.</value>
        string AccessibilityIdentifier { get; }
        /// <summary>
        /// Gets a value indicating whether this <see cref="T:myGovID.Presentation.Contract.IAction`2"/> skip validation.
        /// </summary>
        /// <value><c>true</c> if skip validation; otherwise, <c>false</c>.</value>
        bool SkipValidation { get; }
        /// <summary>
        /// Run the specified Promise.
        /// </summary>
        /// <param name="Promise">Promise.</param>
        Task<IStep> Run();
    }
}