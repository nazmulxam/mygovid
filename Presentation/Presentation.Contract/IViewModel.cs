﻿namespace myGovID.Presentation.Contract
{
    public interface IViewModel
    {
        /// <summary>
        /// Method that checks if the model is valid.
        /// </summary>
        /// <returns><c>true</c>, if valid <c>false</c> otherwise.</returns>
        bool IsValid();
    }
}