﻿using System.Threading.Tasks;
using myGovID.Presentation.Contract.States;

namespace myGovID.Presentation.Contract
{
    public interface IStepRepository
    {
        /// <summary>
        /// Gets the initial step.
        /// </summary>
        Task<IStep> GetInitialStepAsync();

        /// <summary>
        /// Gets the dead end step.
        /// </summary>
        /// <returns>The dead end step.</returns>
        /// <param name="error">Error.</param>
        IStep GetDeadEndStep(PresentationError error);

        /// <summary>
        /// Gets the recoverable dead end step async.
        /// </summary>
        /// <returns>The recoverable dead end step async.</returns>
        /// <param name="error">Error.</param>
        Task<IStep> GetRecoverableDeadEndStepAsync(PresentationError error);

        /// <summary>
        /// Evaluates and returns the next step based on the business rules.
        /// </summary>
        /// <param name="currentStep">Current step.</param>
        /// <param name="viewModel">View model.</param>
        Task<IStep> GetNextStepAsync(IStep currentStep = null, IViewModel viewModel = null);

        /// <summary>
        /// Gets the version upgrade step.
        /// </summary>
        /// <returns>The version upgrade step.</returns>
        Task<IStep> GetVersionUpgradeStepAsync(IStep parentStep = null);
        /// <summary>
        /// Checks if the State is valid for the current process information.
        /// </summary>
        /// <returns><c>true</c>, if valid state was used, <c>false</c> otherwise.</returns>
        /// <param name="state">State.</param>
        bool IsValidState(IState state);
    }
}