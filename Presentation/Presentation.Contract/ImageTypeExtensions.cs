﻿using System;
namespace myGovID.Presentation.Contract
{
    public static class ImageTypeExtensions
    {
        public static byte[] GetBytes(this ImageType imageType)
        {
            switch (imageType)
            {
                case ImageType.JPG: return new byte[] { (byte)0xff, (byte)0xd8 };
                case ImageType.PNG: return new byte[] { 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, 0x00, 0x00, 0x00, 0x0D, 0x49, 0x48, 0x44, 0x52 };
                default: return System.Text.Encoding.UTF8.GetBytes("BM");
            }
        }
    }
}
