﻿namespace myGovID.Presentation.Contract
{
    /// <summary>
    /// Metadata for input fields
    /// </summary>
    public enum InputMetaData
    {
        /// <summary>
        /// Input is email address.
        /// </summary>
        EmailAddress,
        /// <summary>
        /// Input is verification code.
        /// </summary>
        VerificationCode,
        /// <summary>
        /// Input is password.
        /// </summary>
        Password,
        /// <summary>
        /// Input is confirm password.
        /// </summary>
        ConfirmPassword,
        /// <summary>
        /// Input is  name.
        /// </summary>
        Name,
        /// <summary>
        /// Input is given name.
        /// </summary>
        GivenName,
        /// <summary>
        /// Input is middle name
        /// </summary>
        MiddleName,
        /// <summary>
        /// Input is family name
        /// </summary>
        FamilyName,
        /// <summary>
        /// Input is date of birth.
        /// </summary>
        DateOfBirth,
        /// <summary>
        /// Input is photo capture.
        /// </summary>
        PhotoCapture,
        /// <summary>
        /// Input is confirm declaration.
        /// </summary>
        ConfirmDeclaration,
        /// <summary>
        /// Input is enable biometric switch
        /// </summary>
        EnableBiometric,
        /// <summary>
        /// Input is accept terms and conditions checkbox
        /// </summary>
        AcceptTermsAndConditions,
        /// <summary>
        /// Input is passport document number
        /// </summary>
        PassportDocumentNumber,
        /// <summary>
        /// Input is Gender
        /// </summary>
        Gender,
        /// <summary>
        /// Input is Consent
        /// </summary>
        Consent,
        /// <summary>
        /// Input is the first line of the name on the Medicare card
        /// </summary>
        MedicareNameLine1,
        /// <summary>
        /// Input is the second line of the name on the Medicare card
        /// </summary>
        MedicareNameLine2,
        /// <summary>
        /// Input is the third line of the name on the Medicare card
        /// </summary>
        MedicareNameLine3,
        /// <summary>
        /// Input is the fourth line of the name on the Medicare card
        /// </summary>
        MedicareNameLine4,
        /// <summary>
        /// The medicare card number.
        /// </summary>
        MedicareCardNumber,
        /// <summary>
        /// The medicare card irn.
        /// </summary>
        MedicareCardIrn,
        /// <summary>
        /// The type of the medicare card.
        /// </summary>
        MedicareCardType,
        /// <summary>
        /// The medicare card expiry date.
        /// </summary>
        MedicareCardExpiryDate,
        /// <summary>
        /// The medicare consent declaration.
        /// </summary>
        MedicareConsentDeclaration,
        /// <summary>
        /// Input is Driver Licence.
        /// </summary>
        DriverLicenceDocumentNumber,
        /// <summary>
        /// Input is Driver Licence State of issue.
        /// </summary>
        DriverLicenceState
    }
}
