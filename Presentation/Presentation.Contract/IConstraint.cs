﻿using System;

namespace myGovID.Presentation.Contract
{
    /// <summary>
    /// Constraint interface
    /// </summary>
    public interface IConstraint<T> where T: struct, IConvertible
    {
        /// <summary>
        /// Gets the type of constraint
        /// </summary>
        /// <value>The type.</value>
        T Type { get; }
        /// <summary>
        /// Method that validates if the given value satifies the constraint.
        /// </summary>
        /// <returns>Constraint validation result</returns>
        /// <param name="value">Value which needs to be validated</param>
        ConstraintValidationResult IsSatisfied(object value);
    }
}
