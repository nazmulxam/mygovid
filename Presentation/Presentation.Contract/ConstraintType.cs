﻿using System;
namespace myGovID.Presentation.Contract
{
    /// <summary>
    /// Constraint type.
    /// </summary>
    public enum ConstraintType
    {
        Optional, Mandatory, MaxLength, MinLength, Equals,
        ImageType, ImageSizeInKB, Regex,
        DateFormat, DatePrevious, DateRange,
        MultipleChoice, AgeInYears, Group
    }
}
