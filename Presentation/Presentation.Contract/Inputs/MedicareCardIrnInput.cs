﻿using System.Collections.Generic;

namespace myGovID.Presentation.Contract.Inputs
{
    public class MedicareCardIrnInput : MultipleChoiceInput<string>
    {
        private static readonly List<string> options = new List<string>
        {
            "1", "2", "3", "4", "5", "6", "7", "8", "9"
        };

        public MedicareCardIrnInput(string label, string accessibilityId, InputMetaData metadata = InputMetaData.MedicareCardIrn) :
                    base(label, accessibilityId, options, true, metadata)
        {
        }

        public override void FromString(string value)
        {
            Value = value;
        }
    }
}
