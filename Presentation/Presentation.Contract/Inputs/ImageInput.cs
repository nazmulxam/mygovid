﻿using System;
using myGovID.Presentation.Contract.Constraints;
namespace myGovID.Presentation.Contract.Inputs
{
    public class ImageInput : FormInput<String>
    {
        public ImageInput(string label, string accessibilityId, ImageType imageType, float imageSizeInKB, InputMetaData inputMetaData = InputMetaData.PhotoCapture): 
                        base(label, accessibilityId, inputMetaData)
        {
            Constraints = new IConstraint<ConstraintType>[] {
                new ImageTypeConstraint(imageType),
                new ImageSizeInKBConstraint(imageSizeInKB) };
        }
        public override void FromString(string value)
        {
            Value = value;
        }
    }
}
