﻿using myGovID.Presentation.Contract.Constraints;

namespace myGovID.Presentation.Contract.Inputs
{
    public class EnterExistingPasswordInput : PasswordInput
    {
        public EnterExistingPasswordInput(string label, string accessibilityId, int minLength, int maxLength, InputMetaData inputMetaData = InputMetaData.Password) : base(label, accessibilityId, minLength, maxLength, inputMetaData)
        {
            Constraints = new IConstraint<ConstraintType>[] {
                new MandatoryConstraint(),
                new MinLengthConstraint(minLength, "Password must contain at least " + minLength + " characters"),
                new MaxLengthConstraint(maxLength, "Password must contain less than " + maxLength + " characters")
            };
        }
    }
}
