﻿using System.Text.RegularExpressions;
using myGovID.Presentation.Contract.Constraints;

namespace myGovID.Presentation.Contract.Inputs
{
    public class MedicareCardNumberInput : TextInput
    {
        private const int MedicareCardNumberLength = 10;
        private static readonly Regex MedicareCardNumberRegex = new Regex("^[0-9]{10}$");

        public MedicareCardNumberInput(string label, string accessibilityId, InputMetaData metadata = InputMetaData.MedicareCardNumber) :
                        base(label, accessibilityId, metadata)
        {
            Constraints = new IConstraint<ConstraintType>[] {
                    new MandatoryConstraint(),
                    new MinLengthConstraint(MedicareCardNumberLength),
                    new MaxLengthConstraint(MedicareCardNumberLength),
                    new RegexConstraint(MedicareCardNumberRegex)
                };
        }

        public override void FromString(string value)
        {
            Value = value;
        }
    }
}
