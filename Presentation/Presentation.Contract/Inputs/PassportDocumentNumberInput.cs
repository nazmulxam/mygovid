﻿using myGovID.Presentation.Contract.Constraints;
using System.Collections.Generic;
namespace myGovID.Presentation.Contract.Inputs
{
    public class PassportDocumentNumberInput : TextInput
    {
        protected IEnumerable<IConstraint<ConstraintType>> ConstraintList;
        private readonly string _passportNumberRegex;
        public PassportDocumentNumberInput(string label, string accessibilityId,
                         int minLength, int maxLength, InputMetaData inputMetaData = InputMetaData.Name)
                        : base(label, accessibilityId, inputMetaData)
        {
            _passportNumberRegex = "^[A-Za-z0-9]{" + minLength + "," + maxLength + "}$";
            ConstraintList = new List<IConstraint<ConstraintType>>
            {
                new MandatoryConstraint(),
                new MinLengthConstraint(minLength, "This field is invalid"),
                new MaxLengthConstraint(maxLength, "This field is invalid"),
                new RegexConstraint(new System.Text.RegularExpressions.Regex(_passportNumberRegex))
            };
            Constraints = ((List<IConstraint<ConstraintType>>)ConstraintList).ToArray();
        }
        public override void FromString(string value)
        {
            Value = value;
        }
    }
}
