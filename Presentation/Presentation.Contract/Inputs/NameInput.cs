﻿using myGovID.Presentation.Contract.Constraints;
using System.Collections.Generic;

namespace myGovID.Presentation.Contract.Inputs
{
    public class NameInput : TextInput
    {
        public NameInput(string label, string placeholder, string accessibilityId, bool mandatory,
                 int minLength, int maxLength, InputMetaData inputMetaData = InputMetaData.Name)
                : base(label, placeholder, accessibilityId, inputMetaData)
        {
            SetupConstraints(mandatory, minLength, maxLength);
        }

        public NameInput(string label, string accessibilityId, bool mandatory,
                         int minLength, int maxLength, InputMetaData inputMetaData = InputMetaData.Name)
                        : base(label, accessibilityId, inputMetaData)
        {
            SetupConstraints(mandatory, minLength, maxLength);
        }

        public override void FromString(string value)
        {
            Value = value;
        }

        private void SetupConstraints(bool mandatory, int minLength, int maxLength)
        {
            var constraints = new List<IConstraint<ConstraintType>>();
            if (mandatory)
            {
                constraints.Add(new MandatoryConstraint());
            }
            constraints.Add(new MinLengthConstraint(minLength, "This field is required"));
            constraints.Add(new MaxLengthConstraint(maxLength, "This field is invalid"));

            Constraints = constraints;
        }
    }
}
