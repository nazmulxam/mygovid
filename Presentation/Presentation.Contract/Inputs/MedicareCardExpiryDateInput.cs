using System;
using myGovID.Presentation.Contract.Constraints;

namespace myGovID.Presentation.Contract.Inputs
{
    public class MedicareCardExpiryDateInput : FormInput<string>
    {
        public const int CardExpiryYears = 20;

        private DateFormat _dateFormat;
        public DateFormat DateFormat
        {
            get => _dateFormat;
            set { Set(nameof(DateFormat), ref _dateFormat, value); }
        }

        public MedicareCardExpiryDateInput(string label, string accessibilityId, Func<string> cardTypeProvider, InputMetaData metadata = InputMetaData.MedicareCardExpiryDate)
        : base(label, accessibilityId, metadata)
        {

            var yellowCardDateConstraints = new ConditionalConstraintGroup(
                (value) => cardTypeProvider() == MedicareCardType.Yellow,
                new DateFormatConstraint(DateFormat.YellowMedicareCardExpiry),
                new DateRangeConstraint(DateFormat.YellowMedicareCardExpiry,
                                        DateTime.Now.Date,
                                        DateTime.Now.Date.AddYears(CardExpiryYears)));

            var blueCardDateConstraints = new ConditionalConstraintGroup(
                (value) => cardTypeProvider() == MedicareCardType.Blue,
                new DateFormatConstraint(DateFormat.BlueMedicareCardExpiry),
                new DateRangeConstraint(DateFormat.BlueMedicareCardExpiry,
                                        DateTime.Now.Date,
                                        DateTime.Now.Date.AddYears(CardExpiryYears)));

            var greenCardDateConstraints = new ConditionalConstraintGroup(
                (value) => cardTypeProvider() == MedicareCardType.Green,
                new DateFormatConstraint(DateFormat.GreenMedicareCardExpiry),
                new DateRangeConstraint(DateFormat.GreenMedicareCardExpiry,
                                        DateTime.Now.Date,
                                        DateTime.Now.Date.AddYears(CardExpiryYears)));

            Constraints = new IConstraint<ConstraintType>[] {
                new MandatoryConstraint(),
                yellowCardDateConstraints,
                blueCardDateConstraints,
                greenCardDateConstraints
            };
        }

        public override void FromString(string value)
        {
            Value = value;
        }
    }
}
