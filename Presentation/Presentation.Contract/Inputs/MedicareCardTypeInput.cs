﻿using System.Collections.Generic;

namespace myGovID.Presentation.Contract.Inputs
{
    public class MedicareCardTypeInput : MultipleChoiceInput<string>
    {
        public MedicareCardTypeInput(string label, string accessibilityId, InputMetaData metadata = InputMetaData.MedicareCardType) :
                    base(label, accessibilityId, MedicareCardType.All, true, metadata)
        {
        }

        public override void FromString(string value)
        {
            Value = value;
        }
    }
}
