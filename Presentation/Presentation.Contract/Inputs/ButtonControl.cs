using System;
using EnsureThat;
using GalaSoft.MvvmLight.Command;

namespace myGovID.Presentation.Contract.Inputs
{
    public class ButtonControl : BaseControl
    {
        public readonly RelayCommand OnTapped;

        public ButtonControl(string label, string accessibilityId, Action onTappedAction)
        {
            Ensure.String.IsNotNull(label, nameof(label));
            Ensure.String.IsNotNull(accessibilityId, nameof(accessibilityId));

            Label = label;
            AccessibilityIdentifier = accessibilityId;

            OnTapped = new RelayCommand(onTappedAction, () => Visible);
            PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName == nameof(Visible))
                {
                    OnTapped.RaiseCanExecuteChanged();
                }
            };
        }
    }
}
