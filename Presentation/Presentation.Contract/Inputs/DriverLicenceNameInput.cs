﻿using myGovID.Presentation.Contract.Constraints;
using System.Linq;
using System.Text.RegularExpressions;
namespace myGovID.Presentation.Contract.Inputs
{
    public class DriverLicenceNameInput : NameInput
    {
        private readonly string _pattern = "^(?! )[A-Za-z .,\\u2019\\u0027-]*(?<! )$";

        public DriverLicenceNameInput(string label, string accessibilityId, bool mandatory, int minLength, int maxLength, InputMetaData inputMetaData)
            : base(label, accessibilityId, mandatory, minLength, maxLength, inputMetaData)
        {
            var constraints = Constraints.ToList();
            if (mandatory)
            {
                constraints.Add(new MandatoryConstraint());
                constraints.Add(new MinLengthConstraint(minLength));
                constraints.Add(new MaxLengthConstraint(maxLength));
            }
            constraints.Add(new RegexConstraint(new Regex(_pattern)));
            Constraints = constraints;
        }
    }
}
