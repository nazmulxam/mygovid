﻿using myGovID.Presentation.Contract.Constraints;
using System.Text.RegularExpressions;
namespace myGovID.Presentation.Contract.Inputs
{
    public class EmailInput : TextInput
    {
        private readonly string _pattern = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        public EmailInput(string label, string accessibilityId, int maxLength, InputMetaData inputMetaData = InputMetaData.EmailAddress) : base(label, accessibilityId, inputMetaData)
        {
            Constraints = new IConstraint<ConstraintType>[] {
                new MandatoryConstraint(),
                new MaxLengthConstraint(maxLength, "This field is invalid"),
                new RegexConstraint(new Regex(_pattern))
            };
        }
        public override void FromString(string value)
        {
            Value = value;
        }
    }
}
