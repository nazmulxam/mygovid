﻿using myGovID.Presentation.Contract.Constraints;
using System.Linq;
using System.Text.RegularExpressions;
namespace myGovID.Presentation.Contract.Inputs
{
    public class PersonalDetailsNameInput: NameInput
    {
        private readonly string _pattern = "^(?! )[A-Za-z .,\\u2019\\u0027-]*(?<! )$";
        public PersonalDetailsNameInput(string label, string accessibilityId, bool mandatory,
                                        int minLength, int maxLength, InputMetaData inputMetaData = InputMetaData.Name): 
                                        base(label, accessibilityId, mandatory, minLength, maxLength, inputMetaData)
        {
            var newConstraints = Constraints.ToList();
            newConstraints.Add(new RegexConstraint(new Regex(_pattern)));
            Constraints = newConstraints;
        }
    }
}
