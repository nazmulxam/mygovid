﻿namespace myGovID.Presentation.Contract.Inputs
{
    public class BooleanInput : FormInput<bool>
    {
        public BooleanInput(string label, string accessibilityId, bool defaultValue, InputMetaData metadata) : base(label, accessibilityId, metadata)
        {
            Value = defaultValue;
        }
        public override void FromString(string value)
        {
            Value = value.Equals("1");
        }
    }
}
