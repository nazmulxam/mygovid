﻿using System;
using System.Collections.Generic;
using myGovID.Presentation.Contract.Constraints;

namespace myGovID.Presentation.Contract.Inputs
{
    public abstract class MultipleChoiceInput<T> : FormInput<T> where T : IComparable
    {
        public IEnumerable<T> Options { get; private set; }

        public MultipleChoiceInput(
            string label,
            string accessibilityId,
            IEnumerable<T> options,
            bool mandatory,
            InputMetaData metadata)
            : base(label, accessibilityId, metadata)
        {
            var constraints = new List<IConstraint<ConstraintType>>();
            if (mandatory)
            {
                constraints.Add(new MandatoryConstraint());
            }
            constraints.Add(new MultipleChoiceConstraint<T>(options));

            Constraints = constraints;
            Options = options;
        }
    }
}
