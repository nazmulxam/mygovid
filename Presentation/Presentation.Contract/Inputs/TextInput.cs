﻿using System;
namespace myGovID.Presentation.Contract.Inputs
{
    public abstract class TextInput : FormInput<string>
    {
        private bool _trimLeadingAndTrailingSpaces;
        public TextInput(string label, string placeholder, string accessibilityId, InputMetaData metadata, bool trimLeadingAndTrailingSpaces = true)
                                : base(label, placeholder, accessibilityId, metadata)
        {
            _trimLeadingAndTrailingSpaces = trimLeadingAndTrailingSpaces;
        }

        protected TextInput(string label, string accessibilityId, InputMetaData metadata, bool trimLeadingAndTrailingSpaces = true) : base(label, accessibilityId, metadata)
        {
            _trimLeadingAndTrailingSpaces = trimLeadingAndTrailingSpaces;
        }

        public override bool IsValid()
        {
            if (Value != null && _trimLeadingAndTrailingSpaces)
            {
                Value = Value.Trim();
            }
            return base.IsValid();
        }
    }
}
