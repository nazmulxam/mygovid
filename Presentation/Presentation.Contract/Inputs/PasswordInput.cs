﻿using myGovID.Presentation.Contract.Constraints;
using System.Text.RegularExpressions;
namespace myGovID.Presentation.Contract.Inputs
{
    public class PasswordInput: FormInput<string>
    {
        public static string SpecialCharSymbols => "\\d~!@#$%^&*\\(\\)\\-_=+\\[\\]\\{\\}\\\\|;:\\/\\?.><,";
        public static string MissingNumberRegex => "^(?=.*[" + SpecialCharSymbols + "]).*$";
        public static string MissingOneUppercaseRegex => "^(?=.*[A-Z]).*$";
        public static string MissingOneLowercaseRegex => "^(?=.*[a-z]).*$";
        private readonly string _passwordRegex;
        public PasswordInput(string label, string accessibilityId, int minLength, int maxLength, InputMetaData inputMetaData = InputMetaData.Password) 
            : base(label, accessibilityId, inputMetaData)
        {
            _passwordRegex = "^(?=.*[A-Z])(?=.*[a-z])(?=.*[" + SpecialCharSymbols + ")])[A-Za-z" + SpecialCharSymbols + "]{" + minLength + "," + maxLength + "}$";
            Constraints = new IConstraint<ConstraintType>[] {
                new MandatoryConstraint(),
                new MinLengthConstraint(minLength, "Password must contain at least " + minLength + " characters"),
                new MaxLengthConstraint(maxLength, "Password must contain less than " + maxLength + " characters"),
                new RegexConstraint(new Regex(MissingNumberRegex), "Password must contain either a number or special character"),
                new RegexConstraint(new Regex(MissingOneUppercaseRegex), "Password must contain at least one uppercase character"),
                new RegexConstraint(new Regex(MissingOneLowercaseRegex), "Password must contain at least one lowercase character"),
                new RegexConstraint(new Regex(_passwordRegex))
            };
        }
        public override void FromString(string value)
        {
            Value = value;
        }
    }
}
