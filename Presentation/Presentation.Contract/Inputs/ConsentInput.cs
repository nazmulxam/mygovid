﻿using System;
using myGovID.Presentation.Contract.Constraints;

namespace myGovID.Presentation.Contract.Inputs
{
    public class ConsentInput : BooleanInput
    {
        public ConsentInput(string label, string accessibilityId, bool defaultValue, InputMetaData metadata) : base(label, accessibilityId, defaultValue, metadata)
        {
            Constraints = new IConstraint<ConstraintType>[] {
                new BooleanValueConstraint(true, "consent not accepted")
            };
        }
    }
}
