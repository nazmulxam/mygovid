﻿using System;
namespace myGovID.Presentation.Contract.Inputs
{
    public enum Gender
    {
        Male, Female, Unspecified
    }
}
