﻿using System;
using myGovID.Presentation.Contract.Constraints;
namespace myGovID.Presentation.Contract.Inputs
{
    public class ConfirmInput: FormInput<string>
    {
        public ConfirmInput(string label, string accessibilityId, PasswordInput passwordInput, InputMetaData metaData = InputMetaData.ConfirmPassword) 
            : base(label, accessibilityId, metaData)
        {
            if(passwordInput == null) 
            {
                throw new ArgumentNullException(nameof(passwordInput));
            }
            Constraints = new IConstraint<ConstraintType>[] {
                new PasswordMatchConstraint(passwordInput)
            };
        }
        public override void FromString(string value)
        {
            Value = value;
        }
    }
}
