﻿using myGovID.Presentation.Contract.Constraints;
using System.Text.RegularExpressions;
namespace myGovID.Presentation.Contract.Inputs
{
    public class CodeInput : FormInput<string>
    {
        public CodeInput(string label, string accessibilityId, int length, InputMetaData metadata = InputMetaData.VerificationCode) : 
                            base(label, accessibilityId, metadata)
        {
            Constraints = new IConstraint<ConstraintType>[] {
                new MandatoryConstraint(),
                new MinLengthConstraint(length),
                new MaxLengthConstraint(length),
                new RegexConstraint(new Regex("^[0-9]{"+length+"}$"))
            };
        }
        public override void FromString(string value)
        {
            Value = value;
        }
    }
}
