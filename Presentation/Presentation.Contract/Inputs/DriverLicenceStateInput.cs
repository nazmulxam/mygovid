﻿using System;
using System.Collections.Generic;
using myGovID.Presentation.Contract.Constraints;

namespace myGovID.Presentation.Contract.Inputs
{
    public class DriverLicenceStateInput : MultipleChoiceInput<string>
    {
        private static readonly List<string> options = new List<string>
        {
            "ACT", "NSW", "VIC", "WA", "NT", "QLD", "TAS", "SA"
        };

        public DriverLicenceStateInput(string label, string accessibilityId, InputMetaData metadata = InputMetaData.DriverLicenceState)
        : base(label, accessibilityId, options, true, metadata)
        {
        }
        public override void FromString(string value)
        {
            Value = value;
        }
    }
}
