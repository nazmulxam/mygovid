﻿using System;
using System.Collections.Generic;

namespace myGovID.Presentation.Contract.Inputs
{
    public class GenderInput : MultipleChoiceInput<string>
    {
        private static readonly List<string> options = new List<string>
        {
            Gender.Male.ToString(), Gender.Female.ToString(), Gender.Unspecified.ToString() 
        };
        public GenderInput(string label, string accessibilityId, bool mandatory, InputMetaData metadata) : base(label, accessibilityId, options, mandatory, metadata)
        {
            
        }

        public override void FromString(string value)
        {
            Value = value;
        }
    }
}
