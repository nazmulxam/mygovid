using myGovID.Presentation.Contract.Constraints;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
namespace myGovID.Presentation.Contract.Inputs
{
    public class MedicareCardNameInput : NameInput
    {
        public static MedicareCardNameInput BuildLine1(string label, string placeholder, string accessibilityId)
        {
            return new MedicareCardNameInput(label, placeholder, accessibilityId, true, 1, 27, InputMetaData.MedicareNameLine1);
        }

        public static MedicareCardNameInput BuildLine2(string label, string placeholder, string accessibilityId)
        {
            return new MedicareCardNameInput(label, placeholder, accessibilityId, false, 0, 25, InputMetaData.MedicareNameLine2);
        }

        public static MedicareCardNameInput BuildLine3(string label, string placeholder, string accessibilityId)
        {
            return new MedicareCardNameInput(label, placeholder, accessibilityId, false, 0, 23, InputMetaData.MedicareNameLine3);
        }

        public static MedicareCardNameInput BuildLine4(string label, string placeholder, string accessibilityId)
        {
            return new MedicareCardNameInput(label, placeholder, accessibilityId, false, 0, 21, InputMetaData.MedicareNameLine4);
        }

        public override void FromString(string value)
        {
            Value = value;
        }

        private static readonly Regex MedicareCardNameRegex = new Regex("^(?! )[A-Za-z0-9 \u2019\u0027-]*(?<! )$");

        protected MedicareCardNameInput(
            string label,
            string placeholder,
            string accessibilityId,
            bool mandatory,
            int minLength,
            int maxLength,
            InputMetaData inputMetaData)
            : base(label, placeholder, accessibilityId, mandatory, minLength, maxLength, inputMetaData)
        {
            Constraints = Constraints.Concat(new List<IConstraint<ConstraintType>>
            {
                new RegexConstraint(MedicareCardNameRegex)
            });
        }
    }
}
