﻿using System;
using System.Globalization;
using myGovID.Presentation.Contract.Constraints;

namespace myGovID.Presentation.Contract.Inputs
{
    public class BirthDateInput : FormInput<string>
    {
        private readonly int _minAge;
        private readonly int _maxAge;
        public BirthDateInput(string label, string accessibilityId, InputMetaData metadata = InputMetaData.DateOfBirth,
                              int minAge = 13, int maxAge = 100) : base(label, accessibilityId, metadata)
        {
            _minAge = minAge;
            _maxAge = maxAge;
            Constraints = new IConstraint<ConstraintType>[] {
                new MandatoryConstraint(),
                new DateFormatConstraint(DateFormat.DateOfBirth), //checks the date format
                new AgeInYearsConstraint(DateFormat.DateOfBirth, minAge, maxAge) //check if date is within the range
            };
        }
        public override void FromString(string value)
        {
            Value = value;
        }
        public DateTime GetDateTime()
        {
            DateTime.TryParseExact(Value, DateFormat.DateOfBirth.GetFormat(),
                                   CultureInfo.InvariantCulture, DateTimeStyles.None,
                                   out DateTime dateTime);
            return dateTime;
        }
    }
}
