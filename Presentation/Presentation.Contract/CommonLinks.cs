﻿using System;
namespace myGovID.Presentation.Contract
{
    public static class CommonLinks
    {
    
        public readonly static string AppStoreAppLink = "//itunes.apple.com/au/app/australian-taxation-office/id664461825";
        // Help links
        public readonly static string PassportHelpUrl = "https://www.mygovid.gov.au/verifyhelp";
        public readonly static string MedicareHelpUrl = "https://www.mygovid.gov.au/verifyhelp";
        public readonly static string DriverLicenceHelpUrl = "https://www.mygovid.gov.au/verifyhelp";
        // About page links
        public readonly static string WhatIsMyGovIDLink = "https://www.mygovid.gov.au/";
        public readonly static string SupportedServicesLink = "https://www.mygovid.gov.au/supportedservices";
        public readonly static string FeedbackLink = "https://www.mygovid.gov.au/feedback";
        public readonly static string HelpLink = "https://www.mygovid.gov.au/help";
        public readonly static string TermsAndConditions = "https://www.mygovid.gov.au/terms";
        public readonly static string Privacy = "https://www.mygovid.gov.au/privacy";

    }
}
