﻿using System;
using System.Threading.Tasks;

namespace myGovID.Presentation.Contract
{
    public class TaskAction<S> : IAction<ActionType, S>
    {
        private readonly Func<Task<IStep>> _runAction;
        public ActionType Type { get; }
        public S SubType { get; }
        public string AccessibilityIdentifier { get; }
        public bool SkipValidation { get; }
        public TaskAction(ActionType type, Func<Task<IStep>> runAction, S subType, bool skipValidation = false)
        {
            Type = type;
            _runAction = runAction ?? throw new ArgumentNullException(nameof(runAction));
            SubType = subType;
            AccessibilityIdentifier = "On" + Type.ToString() + (SubType != null ? SubType.ToString() : "");
            SkipValidation = skipValidation;
        }
        public Task<IStep> Run()
        {
            return _runAction.Invoke();
        }
    }
}