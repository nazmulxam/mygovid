﻿using System.Threading.Tasks;

namespace myGovID.Presentation.Contract.States
{
    public interface IState
    {
        Task<IStep> BeginStateAsync(IStep currentStep = null, IViewModel viewModel = null);
        Task<IStep> FinaliseStateAsync(IViewModel viewModel = null);
        IStep GoBack();
        bool CanGoBack();
    }
}