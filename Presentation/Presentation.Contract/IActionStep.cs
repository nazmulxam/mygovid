﻿using System;
namespace myGovID.Presentation.Contract
{
    /// <summary>
    /// Action step.
    /// </summary>
    public interface IActionStep<S> : IStep where S : struct, IConvertible
    {
        IAction<ActionType, S>[] Actions { get; }
    }
}
