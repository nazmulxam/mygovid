﻿using System.Collections.Generic;

namespace myGovID.Presentation.Contract
{
    public interface IStep
    {
        /// <summary>
        /// Gets the view model of the step
        /// </summary>
        /// <value>The view model.</value>
        IViewModel ViewModel { get; }
        /// <summary>
        /// Gets the actions supported by step
        /// </summary>
        /// <value>The actions.</value>
        IEnumerable<IAction<ActionType, T>> GetActions<T>();
        /// <summary>
        /// Gets or sets the page level messages.
        /// </summary>
        /// <value>The messages.</value>
        IEnumerable<PageMessage> Messages { get; set; }
        /// <summary>
        /// Child step.
        /// </summary>
        /// <value>The child step.</value>
        IStep ChildStep { get; set; }
    }
}