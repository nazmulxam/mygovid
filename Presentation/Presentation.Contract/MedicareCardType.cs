﻿using System;
using System.Collections.Generic;

namespace myGovID.Presentation.Contract
{
    public static class MedicareCardType
    {
        public const string Green = "Green";
        public const string Blue = "Blue";
        public const string Yellow = "Yellow";

        public static readonly IEnumerable<string> All = new List<string>
        {
            Green, Blue, Yellow
        };
    }
}
