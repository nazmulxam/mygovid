﻿namespace myGovID.Presentation.Contract.Steps.Settings
{
    public class SettingsSectionLinkItem : SettingsSectionItem
    {
        public string Url { get; set; }

        public SettingsSectionLinkItem()
        {
            Action = ActionType.Browse;
        }
    }
}
