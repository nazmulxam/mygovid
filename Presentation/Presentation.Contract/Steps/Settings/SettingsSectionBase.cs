﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace myGovID.Presentation.Contract.Steps.Settings
{
    public class SettingsSectionBase
    {
        public bool Clickable { get; set; }

        public bool Enabled { get; set; }
    }
}
