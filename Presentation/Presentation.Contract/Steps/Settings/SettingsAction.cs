﻿namespace myGovID.Presentation.Contract.Steps.Dashboard
{
    public enum SettingsAction
    {
        ChangePassword,
        Notification,
        Biometrics,
        TermsOfUse,
        Privacy
    }
}