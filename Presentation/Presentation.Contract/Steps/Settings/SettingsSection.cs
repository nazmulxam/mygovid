﻿using System.Collections.Generic;

namespace myGovID.Presentation.Contract.Steps.Settings
{
    public class SettingsSection: SettingsSectionBase
    {
        public string Header { get; set; }

        public IList<SettingsSectionItem> ItemList { get; set; }
    }
}
