﻿namespace myGovID.Presentation.Contract.Steps.Settings
{
    public class SettingsSectionSwitchItem : SettingsSectionItem
    {
        public bool Selected { get; set; }

        public SettingsSectionSwitchItem()
        {
            Clickable = false;
        }
    }
}
