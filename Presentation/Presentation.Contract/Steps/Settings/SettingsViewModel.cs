﻿using System.Collections.Generic;
using myGovID.Presentation.Contract.Steps.Dashboard;

namespace myGovID.Presentation.Contract.Steps.Settings
{
    public class SettingsViewModel : IViewModel
    {
        public bool IsValid() => true;

        public readonly IEnumerable<SettingsSection> SettingsSectionList;

        public SettingsViewModel()
        {
            SettingsSectionList = new List<SettingsSection>()
                    {
                        new SettingsSection(){
                            ItemList = new List<SettingsSectionItem>(){
                                ChangePassword
                            }
                        },
                        new SettingsSection(){
                            ItemList = new List<SettingsSectionItem>(){
                                Notification,
                                Biometrics
                            }
                        },
                        new SettingsSection(){
                            ItemList = new List<SettingsSectionItem>(){
                                TermsOfUse,
                                Privacy
                            }
                        }
                    };
        }
        public readonly SettingsSectionItem ChangePassword = new SettingsSectionItem()
        {
            SettingsAction = SettingsAction.ChangePassword,
            Title = "Change Password",
            Enabled = false
        };

        public readonly SettingsSectionSwitchItem Notification = new SettingsSectionSwitchItem()
        {
            SettingsAction = SettingsAction.Notification,
            Title = "Allow notification",
        };

        public readonly SettingsSectionSwitchItem Biometrics = new SettingsSectionSwitchItem()
        {
            SettingsAction = SettingsAction.Biometrics,
            Title = "Enable Touch ID",
        };

        public readonly SettingsSectionLinkItem TermsOfUse = new SettingsSectionLinkItem()
        {
            SettingsAction = SettingsAction.TermsOfUse,
            Title = "Terms and conditions",
            Url = CommonLinks.TermsAndConditions
        };

        public readonly SettingsSectionLinkItem Privacy = new SettingsSectionLinkItem()
        {
            SettingsAction = SettingsAction.Privacy,
            Title = "Privacy",
            Url = CommonLinks.Privacy
        };
    }
}
