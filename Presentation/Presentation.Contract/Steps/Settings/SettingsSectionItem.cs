﻿using myGovID.Presentation.Contract.Steps.Dashboard;

namespace myGovID.Presentation.Contract.Steps.Settings
{
    public class SettingsSectionItem: SettingsSectionBase
    {
        public ActionType Action { get; set; }

        public SettingsAction SettingsAction { get; set; }

        public string Title { get; set; }

        public SettingsSectionItem()
        {
            Action = ActionType.Next;
            Clickable = true;
            Enabled = true;
        }
    }
}
