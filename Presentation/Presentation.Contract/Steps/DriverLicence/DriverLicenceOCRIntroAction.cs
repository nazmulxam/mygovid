﻿namespace myGovID.Presentation.Contract.Steps.DriverLicence
{
    public enum DriverLicenceOCRIntroAction
    {
        ManualEntry, OCRCapture
    }
}
