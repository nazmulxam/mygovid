﻿namespace myGovID.Presentation.Contract.Steps.DriverLicence
{
    public enum CaptureDriverLicenceDetailsAction
    {
        Submit, Rescan, Back, Help
    }
}