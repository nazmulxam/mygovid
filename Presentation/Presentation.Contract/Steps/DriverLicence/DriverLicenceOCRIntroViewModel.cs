﻿using System;
using System.Collections.Generic;

namespace myGovID.Presentation.Contract.Steps.DriverLicence
{
    public class DriverLicenceOCRIntroViewModel : IViewModel
    {
        public DriverLicenceOCRIntroModel[] Pages { get; }
        public IEnumerable<PageMessage> Messages { get; set; }
        public DriverLicenceOCRIntroViewModel(DriverLicenceOCRIntroModel[] pages)
        {
            if (pages == null || pages.Length == 0)
            {
                throw new ArgumentNullException(nameof(pages));
            }
            Pages = pages;
        }
        public bool IsValid() => true;
    }
}
