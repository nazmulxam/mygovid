﻿using System;
using myGovID.Presentation.Contract.Steps.HelpOverlay;

namespace myGovID.Presentation.Contract.Steps.DriverLicence
{
    public class DriverLicenceOCRIntroModel : IHelpOverlayModel
    {
        public string Image { get; }
        public string Title { get; }
        public string Description { get; }
        public DriverLicenceOCRIntroModel(string image, string title, string description)
        {
            if (string.IsNullOrEmpty(image))
            {
                throw new ArgumentNullException(nameof(image));
            }
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentNullException(nameof(title));
            }
            if (string.IsNullOrEmpty(description))
            {
                throw new ArgumentNullException(nameof(description));
            }
            Image = image;
            Title = title;
            Description = description;
        }
    }
}
