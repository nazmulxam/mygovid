﻿using myGovID.Presentation.Contract.Inputs;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace myGovID.Presentation.Contract.Steps.DriverLicence
{
    public class DriverLicenceViewModel : FormViewModel
    {
        public DriverLicenceDocumentNumberInput DriverLicenceDocumentNumber { get { return GetInput<DriverLicenceDocumentNumberInput>(InputMetaData.DriverLicenceDocumentNumber); } }
        public DriverLicenceNameInput GivenName { get { return GetInput<DriverLicenceNameInput>(InputMetaData.GivenName); } }
        public DriverLicenceNameInput MiddleName { get { return GetInput<DriverLicenceNameInput>(InputMetaData.MiddleName); } }
        public DriverLicenceNameInput FamilyName { get { return GetInput<DriverLicenceNameInput>(InputMetaData.FamilyName); } }
        public BirthDateInput DateOfBirth { get { return GetInput<BirthDateInput>(InputMetaData.DateOfBirth); } }
        public DriverLicenceStateInput State { get { return GetInput<DriverLicenceStateInput>(InputMetaData.DriverLicenceState); } }
        public BooleanInput ConsentProvided { get { return GetInput<BooleanInput>(InputMetaData.Consent); } }

        public bool IsUsingOCR { get; }
        public DriverLicenceViewModel(bool hasOCR) : base(new List<IInput> {
            new DriverLicenceDocumentNumberInput("Licence number", "licenceNumber", 1, 10, InputMetaData.DriverLicenceDocumentNumber),
            new DriverLicenceNameInput("Given names/s", "givenNames", true, 1, 20, InputMetaData.GivenName),
            new DriverLicenceNameInput("Middle name", "middleName", false, 0, 20, InputMetaData.MiddleName),
            new DriverLicenceNameInput("Family name", "familyName", true, 1, 40, InputMetaData.FamilyName),
            new BirthDateInput("Date of birth", "dateOfBirth", InputMetaData.DateOfBirth, 16,100),
            new DriverLicenceStateInput("State", "state", InputMetaData.DriverLicenceState),
            new ConsentInput("I consent to have my identity details verified with the document issuer", "consent", false, InputMetaData.Consent)
        })
        {
            IsUsingOCR = hasOCR;
        }
    }
}