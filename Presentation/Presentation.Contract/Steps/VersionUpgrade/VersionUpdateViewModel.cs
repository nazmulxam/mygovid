﻿using System;
using System.Collections.Generic;

namespace myGovID.Presentation.Contract.Steps.VersionUpgrade
{
    public class VersionUpgradeViewModel: IViewModel
    {
        public bool ForcedUpdate { get; }
        public string UpgradeLink { get; }
        public IEnumerable<PageMessage> Messages { get; set; }
        public VersionUpgradeViewModel(string upgradeLink, bool forcedUpdate = false) 
        {
            if(string.IsNullOrEmpty(upgradeLink)) 
            {
                throw new ArgumentNullException(nameof(upgradeLink));
            }
            UpgradeLink = upgradeLink;
            ForcedUpdate = forcedUpdate;
        }
        public bool IsValid() => true;
    }
}
