﻿using System;
namespace myGovID.Presentation.Contract.Steps.Browse
{
    public class BrowseViewModel : IViewModel
    {
        public string Url { get; }
        public BrowseViewModel(string url)
        {
            Url = url;
        }

        public bool IsValid()
        {
            return Uri.TryCreate(Url, UriKind.Absolute, out Uri uriResult);
        }
    }
}
