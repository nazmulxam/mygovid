﻿using System.Collections.Generic;
using System.Linq;
using myGovID.Presentation.Contract.Inputs;
using Newtonsoft.Json;

namespace myGovID.Presentation.Contract.Steps.Authenticate
{
    public class AuthenticateViewModel : FormViewModel
    {
        public bool UseBiometrics { get; }
        [JsonIgnore]
        public EnterExistingPasswordInput PasswordInput
        {
            get => ((EnterExistingPasswordInput)Inputs.First((input) => input.MetaData == InputMetaData.Password));
        }
        public AuthenticateViewModel(bool useBiometrics) : base(new List<IInput>() { new EnterExistingPasswordInput("Password", "passwordInput", 10, 60) } )
        {
            UseBiometrics = useBiometrics;
        }
        public string GetUserPassword()
        {
            return PasswordInput.Value;
        }
    }
}
