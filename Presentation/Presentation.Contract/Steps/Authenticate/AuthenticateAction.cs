﻿namespace myGovID.Presentation.Contract.Steps.Authenticate
{
    public enum AuthenticateAction
    {
        Biometric, Password, ForgottenPassword
    }
}