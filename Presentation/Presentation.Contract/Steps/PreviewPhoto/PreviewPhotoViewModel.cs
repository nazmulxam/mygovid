﻿using myGovID.Presentation.Contract.Inputs;
using System;
using System.Collections.Generic;
namespace myGovID.Presentation.Contract.Steps.PreviewPhoto
{
    public class PreviewPhotoViewModel : FormViewModel
    {
        public string ImageInBase64 { get; }
        public PreviewPhotoViewModel(string imageInBase64) : base(new List<IInput>{ 
            new BooleanInput("Declaration", "declarationInput", false, InputMetaData.ConfirmDeclaration) 
        })
        {
            if(string.IsNullOrEmpty(imageInBase64)) 
            {
                throw new ArgumentNullException(nameof(imageInBase64));
            }
            ImageInBase64 = imageInBase64;
        }
    }
}