﻿using myGovID.Presentation.Contract.Inputs;
using System.Collections.Generic;
using System.Linq;
namespace myGovID.Presentation.Contract.Steps.CaptureEmail
{
    public class CaptureEmailViewModel : FormViewModel
    {
        public int MaxLength { get; }
        public CaptureEmailViewModel(int maxLength) : base(new List<IInput>() { new EmailInput("Email address", "emailAddressInput", maxLength) })
        {
            MaxLength = maxLength;
        }
        public string GetEmailAddress() {
            return ((EmailInput) Inputs.First(arg => arg.MetaData == InputMetaData.EmailAddress)).Value;
        }
    }
}
