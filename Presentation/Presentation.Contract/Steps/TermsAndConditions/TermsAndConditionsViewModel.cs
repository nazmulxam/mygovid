﻿using myGovID.Presentation.Contract.Inputs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace myGovID.Presentation.Contract.Steps.TermsAndConditions
{
    public class TermsAndConditionsViewModel: FormViewModel
    {
        public string TermsAndConditions { get; }
        public bool IsHtml { get; }
        public string Link { get; }
        public string Version { get; }
        public TermsAndConditionsViewModel(string termsAndConditions, bool isHtml, string link, string version) : 
                base(new List<IInput>() { new BooleanInput("Accepted", "acceptedInput", false, InputMetaData.AcceptTermsAndConditions) })
        {
            if(string.IsNullOrEmpty(termsAndConditions)) 
            {
                throw new ArgumentNullException(nameof(termsAndConditions));
            }
            if (string.IsNullOrEmpty(link))
            {
                throw new ArgumentNullException(nameof(link));
            }
            if (string.IsNullOrEmpty(version)) 
            {
                throw new ArgumentNullException(nameof(version));
            }
            TermsAndConditions = termsAndConditions;
            IsHtml = isHtml;
            Link = link;
            Version = version;
        }
        public bool IsTermsAndConditionsAccepted()
        {
            return ((BooleanInput)Inputs.First()).Value;
        }
    }
}
