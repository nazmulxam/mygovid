﻿namespace myGovID.Presentation.Contract.Steps.MedicareOcrIntroStep
{
    public enum MedicareOcrIntroAction
    {
        ScanDetails, EnterManually, Back
    }
}
