﻿namespace myGovID.Presentation.Contract.Steps.MedicareOcrIntroStep
{
    public class MedicareOcrIntroViewModel : IViewModel
    {
        public bool IsValid() => true;
        public readonly string Title = "Scan your Medicare card";
        public readonly string Body = "Position the front of your card in the frame and we'll automatically scan the details.";
        public readonly string ScanButtonLabel = "Scan details";
        public readonly string ManualButtonLabel = "Enter manually";
    }
}
