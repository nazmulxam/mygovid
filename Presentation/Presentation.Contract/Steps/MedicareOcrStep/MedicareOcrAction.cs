﻿namespace myGovID.Presentation.Contract.Steps.MedicareOcrStep
{
    public enum MedicareOcrAction
    {
        PerformScan, EnterManually
    }
}
