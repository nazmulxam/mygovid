﻿namespace myGovID.Presentation.Contract.Steps.MedicareCardInput
{
    public enum MedicareCardInputAction
    {
        Submit, Rescan, Back, Help
    }
}
