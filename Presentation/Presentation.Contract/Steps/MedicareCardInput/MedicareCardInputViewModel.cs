using System.Collections.Generic;
using System.Linq;
using myGovID.Presentation.Contract.Inputs;
using GalaSoft.MvvmLight.Helpers;

using GalaSoft.MvvmLight;
using System.ComponentModel;
using System.Globalization;
using System;

namespace myGovID.Presentation.Contract.Steps.MedicareCardInput
{
    public class MedicareCardInputViewModel : FormViewModel
    {
        public readonly string Title = "Medicare card";
        public readonly string Body = "Enter your details.";
        public readonly string NextButtonLabel = "Confirm";
        private const string CardNameFieldPlaceholder = "Name as appears on Medicare card";
        public readonly MedicareCardNameInput NameLine1 = MedicareCardNameInput.BuildLine1("Name", CardNameFieldPlaceholder, "nameLine1");
        public readonly MedicareCardNameInput NameLine2 = MedicareCardNameInput.BuildLine2(string.Empty, CardNameFieldPlaceholder, "nameLine2");
        public readonly MedicareCardNameInput NameLine3 = MedicareCardNameInput.BuildLine3(string.Empty, CardNameFieldPlaceholder, "nameLine3");
        public readonly MedicareCardNameInput NameLine4 = MedicareCardNameInput.BuildLine4(string.Empty, CardNameFieldPlaceholder, "nameLine4");
        public readonly MedicareCardExpiryDateInput Expiry;
        public readonly MedicareCardTypeInput CardType = new MedicareCardTypeInput("Colour", "cardType");
        public readonly MedicareCardIrnInput Irn = new MedicareCardIrnInput("Individual reference number", "irn");
        public readonly MedicareCardNumberInput CardNumber = new MedicareCardNumberInput("Card number", "cardNumber");
        public readonly ConsentInput ConsentProvided = new ConsentInput("I consent to have my identity details verified with the document issuer.", "consent", false, InputMetaData.MedicareConsentDeclaration);

        public readonly ButtonControl AddNameButton;
        public readonly ButtonControl RemoveNameButton;

        public readonly bool DidUseOcr;
        private int VisibleNameInputsNumber => Inputs.Count((input) => input is MedicareCardNameInput && input.Visible);

        public MedicareCardInputViewModel(bool didUseOcr)
        {
            DidUseOcr = didUseOcr;

            AddNameButton = new ButtonControl("+ Add new name field", "addNameField", AddNameButtonTapped);
            RemoveNameButton = new ButtonControl("× Remove", "removeNameField", RemoveNameButtonTapped);

            // Expiry depends on card type
            Expiry = new MedicareCardExpiryDateInput("Card Expiry", "cardExpiry", () => CardType.Value);

            // Set inputs that need validation
            Inputs = new List<IInput> {
                CardNumber,
                NameLine1,
                NameLine2,
                NameLine3,
                NameLine4,
                Irn,
                CardType,
                Expiry,
                ConsentProvided
            };

            // Initial state
            NameLine2.Visible = false;
            NameLine3.Visible = false;
            NameLine4.Visible = false;
            RemoveNameButton.Visible = false;
            Expiry.Placeholder = DateFormat.GreenMedicareCardExpiry.GetFormat().ToLowerInvariant();

            // Bindings
            NameLine1.PropertyChanged += NameInputPropertyChangeListener;
            NameLine2.PropertyChanged += NameInputPropertyChangeListener;
            NameLine3.PropertyChanged += NameInputPropertyChangeListener;
            NameLine4.PropertyChanged += NameInputPropertyChangeListener;
            CardType.PropertyChanged += CardTypePropertyChangeListener;
            Expiry.PropertyChanged += ExpiryPropertyChangeListener;
        }

        private void AddNameButtonTapped()
        {
            var nextInput = Inputs.FirstOrDefault((input) => input is MedicareCardNameInput && !input.Visible);
            if (nextInput != null)
            {
                (nextInput as MedicareCardNameInput).Visible = true;
            }
        }

        private void RemoveNameButtonTapped()
        {
            var nextInput = Inputs.LastOrDefault((input) => input is MedicareCardNameInput && input.Visible);
            if (nextInput != null)
            {
                (nextInput as MedicareCardNameInput).Visible = false;
            }
        }

        private void UpdateButtonVisibility()
        {
            var visibleNameLines = VisibleNameInputsNumber;
            AddNameButton.Visible = visibleNameLines < 4;
            RemoveNameButton.Visible = visibleNameLines > 1;
        }

        private void NameInputPropertyChangeListener(object sender, PropertyChangedEventArgs e)
        {
            // Update number of visible inputs count
            if (e.PropertyName == nameof(IControl.Visible))
            {
                UpdateButtonVisibility();
            }
        }

        private void CardTypePropertyChangeListener(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(MedicareCardTypeInput.Value))
            {
                // Update expiry date format card type changes
                switch (CardType.Value)
                {
                    case MedicareCardType.Blue:
                        Expiry.DateFormat = DateFormat.BlueMedicareCardExpiry;
                        break;
                    case MedicareCardType.Yellow:
                        Expiry.DateFormat = DateFormat.YellowMedicareCardExpiry;
                        break;
                    default:
                        Expiry.DateFormat = DateFormat.GreenMedicareCardExpiry;
                        break;
                }

                // Disable expiry input and clear the value if there's no valid card type
                Expiry.Enabled = MedicareCardType.All.Contains(CardType.Value);

                // Clear value if we've swapped to a date format that's different
                if (!DateTime.TryParseExact(Expiry.Value,
                                            Expiry.DateFormat.GetFormat(),
                                            CultureInfo.InvariantCulture,
                                            DateTimeStyles.None,
                                            out DateTime date))
                {
                    Expiry.Value = string.Empty;
                }
            }
        }

        private void ExpiryPropertyChangeListener(object sender, PropertyChangedEventArgs e)
        {
            // Update placeholder when date format changes
            if (e.PropertyName == nameof(MedicareCardExpiryDateInput.DateFormat))
            {
                Expiry.Placeholder = Expiry.DateFormat.GetFormat().ToLowerInvariant();
            }
        }
    }
}