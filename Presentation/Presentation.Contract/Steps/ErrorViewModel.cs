﻿using System;
using System.Collections.Generic;

namespace myGovID.Presentation.Contract.Steps
{
    public class ErrorViewModel : IViewModel
    {
        public PresentationError Error { get; }

        public IEnumerable<PageMessage> Messages { get; set; }

        public ErrorViewModel(PresentationError exception)
        {
            Error = exception ?? throw new ArgumentNullException(nameof(exception));
        }
        public bool IsValid() => true;
    }
}
