﻿using System;
using System.Collections.Generic;

namespace myGovID.Presentation.Contract.Steps.DeadEnd
{
    public class DeadEndViewModel : IViewModel
    {

        public readonly string ErrorTitleAccessibilityIdentifier = "ErrorTitle";
        public readonly string ErrorDescriptionAccessibilityIdentifier = "ErrorDescription";
        public readonly string ErrorCodeAccessibilityIdentifier = "ErrorCode";


        public PresentationError Error { get; }

        public IEnumerable<PageMessage> Messages { get; set; }

        public DeadEndViewModel(PresentationError error)
        {
            Error = error ?? throw new ArgumentNullException(nameof(error));
        }
        public bool IsValid() => true;
    }
}