﻿using System;
using System.Collections.Generic;
namespace myGovID.Presentation.Contract.Steps.RecoverableDeadEnd
{
    public class RecoverableDeadEndViewModel : IViewModel
    {
        public PresentationError Exception { get; }

        public IEnumerable<PageMessage> Messages { get; set; }

        public RecoverableDeadEndViewModel(PresentationError exception)
        {
            Exception = exception ?? throw new ArgumentNullException(nameof(exception));
        }
        public bool IsValid() => true;
    }
}