﻿using myGovID.Presentation.Contract.Inputs;
using System.Collections.Generic;
namespace myGovID.Presentation.Contract.Steps.CapturePhoto
{
    public class CapturePhotoViewModel : FormViewModel
    {
        public ImageType ImageType { get; }
        public float ImageSizeInKB { get; }
        public CapturePhotoViewModel(ImageType imageType, float imageSizeInKB) 
            : base(new List<IInput> { new ImageInput("Image", "image", imageType, imageSizeInKB) })
        {
            ImageType = imageType;
            ImageSizeInKB = imageSizeInKB;
        }
    }
}
