﻿namespace myGovID.Presentation.Contract.Steps.HelpOverlay
{
    public interface IHelpOverlayModel
    {
        string Image { get; }
        string Title { get; }
        string Description { get; }
    }
}
