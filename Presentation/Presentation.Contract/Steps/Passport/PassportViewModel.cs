﻿using System.Collections.Generic;
using myGovID.Presentation.Contract.Inputs;

namespace myGovID.Presentation.Contract.Steps.Passport
{
    public class PassportViewModel : FormViewModel
    {

        public PassportNameInput GivenName { get { return GetInput<PassportNameInput>(InputMetaData.GivenName); } }
        public PassportNameInput FamilyName { get { return GetInput<PassportNameInput>(InputMetaData.FamilyName); } }
        public PassportDocumentNumberInput PassportNumber { get { return GetInput<PassportDocumentNumberInput>(InputMetaData.PassportDocumentNumber); } }
        public GenderInput Gender { get { return GetInput<GenderInput>(InputMetaData.Gender); } }
        public BirthDateInput DateOfBirth { get { return GetInput<BirthDateInput>(InputMetaData.DateOfBirth); } }
        public BooleanInput ConsentProvided { get { return GetInput<BooleanInput>(InputMetaData.Consent); } }

        public bool IsUsingOCR { get; }
        public PassportViewModel(bool hasOCR) : base(new List<IInput> {
            new PassportDocumentNumberInput("Document Number", "documentNumber", 8, 9, InputMetaData.PassportDocumentNumber),
            new PassportNameInput("Given name/s", "givenNamesInput",true, 1, 31, InputMetaData.GivenName),
            new PassportNameInput("Family name", "familyNameInput",true, 1, 31, InputMetaData.FamilyName),
            new BirthDateInput("Date of birth", "dateOfBirth", InputMetaData.DateOfBirth),
            new GenderInput("Gender", "gender",true, InputMetaData.Gender),
            new ConsentInput("I consent to have my identity details verified with the document issuer", "consent", false, InputMetaData.Consent)
        })
        {
            IsUsingOCR = hasOCR;
        }
    }
}
