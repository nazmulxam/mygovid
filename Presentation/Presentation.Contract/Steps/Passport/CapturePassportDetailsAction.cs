﻿namespace myGovID.Presentation.Contract.Steps.Passport
{
    public enum CapturePassportDetailsAction
    {
        Submit, Rescan, Back, Help
    }
}