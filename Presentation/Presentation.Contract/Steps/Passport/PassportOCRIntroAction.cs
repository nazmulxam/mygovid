﻿using System;
namespace myGovID.Presentation.Contract.Steps.Passport
{
    public enum PassportOCRIntroAction
    {
        ManualEntry, OCRCapture
    }
}
