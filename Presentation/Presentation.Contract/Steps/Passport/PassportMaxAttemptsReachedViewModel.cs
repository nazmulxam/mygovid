﻿using System;
using System.Collections.Generic;

namespace myGovID.Presentation.Contract.Steps.Passport
{
    public class PassportMaxAttemptsReachedViewModel : IViewModel
    {
        public PresentationError Exception { get; }

        public IEnumerable<PageMessage> Messages { get; set; }

        public PassportMaxAttemptsReachedViewModel(PresentationError exception)
        {
            Exception = exception ?? throw new ArgumentNullException(nameof(exception));
        }
        public bool IsValid() => true;
    }
}