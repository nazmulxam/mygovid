﻿using System;
namespace myGovID.Presentation.Contract.Steps.Passport
{
    public class PassportOCRIntroViewModel : IViewModel
    {
        public string PageHeader { get; }
        public string Title { get; }
        public string Description { get; }

        public PassportOCRIntroViewModel()
        {
            PageHeader = "Build your identity";
            Title = "Scan your passport";
            Description = "Position the photo page of your passport in the frame and your details will automatically scan.";
        }

        public bool IsValid()
        {
            return true;
        }
    }
}
