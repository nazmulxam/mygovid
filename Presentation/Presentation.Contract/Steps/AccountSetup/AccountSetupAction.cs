﻿namespace myGovID.Presentation.Contract.Steps.AccountSetup
{
    public enum AccountSetupAction
    {
        Register, Recover, Help
    }
}