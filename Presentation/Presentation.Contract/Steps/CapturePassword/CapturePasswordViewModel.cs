﻿using myGovID.Presentation.Contract.Inputs;
using System.Collections.Generic;
using System.Linq;
namespace myGovID.Presentation.Contract.Steps.CapturePassword
{
    public class CapturePasswordViewModel : FormViewModel
    {
        public PasswordInput PasswordInput { get; }
        public CapturePasswordViewModel() : base(new List<IInput> {
            new PasswordInput("Password", "passwordInput", 10, 60)
        })
        {
            PasswordInput = (PasswordInput)Inputs.First(input => input.MetaData == InputMetaData.Password);
            ((List<IInput>) Inputs).Add(new ConfirmInput("Confirm password", "confirmPasswordInput", PasswordInput));
        }
        public string GetPassword()
        {
            return PasswordInput.Value;
        }
    }
}
 