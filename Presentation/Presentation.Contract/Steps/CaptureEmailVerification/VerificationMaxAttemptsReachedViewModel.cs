﻿using System;
using System.Collections.Generic;

namespace myGovID.Presentation.Contract.Steps.CaptureEmailVerification
{
    public class VerificationMaxAttemptsReachedViewModel : IViewModel
    {
        public PresentationError Exception { get; }

        public IEnumerable<PageMessage> Messages { get; set; }

        public VerificationMaxAttemptsReachedViewModel(PresentationError exception)
        {
            Exception = exception ?? throw new ArgumentNullException(nameof(exception));
        }
        public bool IsValid() => true;
    }
}