﻿using myGovID.Presentation.Contract.Inputs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace myGovID.Presentation.Contract.Steps.CaptureEmailVerification
{
    public class CaptureEmailVerificationViewModel : FormViewModel
    {
        public string EmailAddress { get; }
        public int CodeLength { get; }
        public CaptureEmailVerificationViewModel(string emailAddress, int codeLength) 
            : base(new List<IInput> { new CodeInput("Verification code", "verificationCodeInput", codeLength) })
        {
            if(string.IsNullOrEmpty(emailAddress)) 
            {
                throw new ArgumentNullException(nameof(emailAddress));
            }
            EmailAddress = emailAddress;
            CodeLength = codeLength;
        }
        public string GetVerificationCode()
        {
            return ((CodeInput)Inputs.First()).Value;
        }
    }
}
