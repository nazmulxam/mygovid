﻿using myGovID.Presentation.Contract.Steps.Dashboard.Document;
using myGovID.Presentation.Contract.Steps.Dashboard.Alert;
using System.Collections.Generic;
using System;
using GalaSoft.MvvmLight;

namespace myGovID.Presentation.Contract.Steps.Dashboard
{
    public class DashboardViewModel : ViewModelBase, IViewModel
    {
        private string _firstName;
        private AuthStrengthType _authStrengthType;
        private string _welcomeMessage;
        private string _nextSteps;

        public IEnumerable<IDocument> Documents { get; }
        public IEnumerable<IAlert> Alerts { get; }
        public IEnumerable<PageMessage> Messages { get; set; }
        public string WelcomeMessage
        {
            get => _welcomeMessage;
            set
            {
                _welcomeMessage = value;
                string propertyName = GetPropertyName(System.Reflection.MethodBase.GetCurrentMethod());
                RaisePropertyChanged(propertyName);
            }
        }
        public string FirstName
        {
            get => _firstName;
            set
            {
                _firstName = value;
                WelcomeMessage = $"Hi {value}";
            }
        }
        public AuthStrengthType AuthStrengthType
        {
            get => _authStrengthType;
            set
            {
                _authStrengthType = value;
                TutorialLinkText = GetTutorialLink();
                NextSteps = GetNextStepsDescription();
                string propertyName = GetPropertyName(System.Reflection.MethodBase.GetCurrentMethod());
                RaisePropertyChanged(propertyName);
            }
        }
        public string NextSteps
        { 
            get => _nextSteps;
            set
            {
                _nextSteps = value;
                string propertyName = GetPropertyName(System.Reflection.MethodBase.GetCurrentMethod());
                RaisePropertyChanged(propertyName);
            }
        }
        public string TutorialLinkText { get; private set; }
        public string TutorialLink => "mygovid://showTutorial";
        public DashboardViewModel(string firstName, IEnumerable<IDocument> documents, IEnumerable<IAlert> alerts,  
                                  AuthStrengthType authStrengthType)
        {
            FirstName = firstName ?? throw new ArgumentException($"'{nameof(firstName)}' cannot be null");
            AuthStrengthType = authStrengthType;
            Alerts = alerts;
            Documents = documents;
            TutorialLinkText = GetTutorialLink();
            NextSteps = GetNextStepsDescription();
        }
        public bool IsValid() => true;
        private string GetNextStepsDescription()
        { 
            switch(_authStrengthType)
            {
                case AuthStrengthType.Intermediate:
                    return $"<strong>Your identity has been verified</strong><br/>You can now use your myGovID to login to online services. <a href=\"{TutorialLink}\">{TutorialLinkText}</a>";
                case AuthStrengthType.Strong:
                    return $"<strong>Your identity has been verified</strong><br/>You can now leave the app and return to what you were doing. <a href=\"{TutorialLink}\">{TutorialLinkText}</a> to learn more about using myGovID.";
                default:
                    return $"<strong>Build your identity</strong><br/>Verify your details so that you can use your myGovID to login to online services. <a href=\"{TutorialLink}\">{TutorialLinkText}</a>";
            }
        }
        private string GetTutorialLink()
        { 
            if(_authStrengthType == AuthStrengthType.Strong)
            {
                return "View the tutorial";
            }
            return "View tutorial";
        }
        private string GetPropertyName(System.Reflection.MethodBase methodBase)
        {
            return methodBase.Name.Substring(4);
        }
    }
}