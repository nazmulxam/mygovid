﻿using System;
namespace myGovID.Presentation.Contract.Steps.Dashboard
{
    public enum AuthStrengthType
    {
        Weak, 
        Intermediate, 
        Strong, 
        Unknown
    }
}
