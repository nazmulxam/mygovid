﻿namespace myGovID.Presentation.Contract.Steps.Dashboard
{
    public enum DashboardAction
    {
        Passport, DriverLicence, Medicare, About, Settings, Account, Tutorial, FVS
    }
}