﻿using System;

namespace myGovID.Presentation.Contract.Steps.Dashboard.Document
{
    public class PassportDocument: IDocument
    {
        private readonly bool _supportsDVS;
        public PassportDocument(bool supportsDVS)
        {
            _supportsDVS = supportsDVS;
        }
        public string Title => "Passport";
        public string Description => $"Verify your details{(_supportsDVS ? " and take photo" : "")}";
        public string Image => "DocumentAddIcon";
        public DashboardAction Action => DashboardAction.Passport;
    }
}
