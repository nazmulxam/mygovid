﻿using System;

namespace myGovID.Presentation.Contract.Steps.Dashboard.Document
{
    public class FacialVerification : IDocument
    {
        public string Title => "Match your face";
        public string Description => "Against your passport photo";
        public string Image => "DocumentCameraIcon";
        public DashboardAction Action => DashboardAction.FVS;
    }
}
