﻿using System;

namespace myGovID.Presentation.Contract.Steps.Dashboard.Document
{
    public class MedicareDocument : IDocument
    {
        public string Title => "Medicare";
        public string Description => "Verify your details";
        public string Image => "DocumentAddIcon";
        public DashboardAction Action => DashboardAction.Medicare;
    }
}
