﻿using System;
using System.Threading.Tasks;

namespace myGovID.Presentation.Contract.Steps.Dashboard.Document
{
    public interface IDocument
    {
        /// <summary>
        /// Title of the document
        /// </summary>
        /// <value>The title.</value>
        string Title { get; }
        /// <summary>
        /// Description of the document
        /// </summary>
        /// <value>The description.</value>
        string Description { get; }
        /// <summary>
        /// Image name to be used with the given document
        /// </summary>
        /// <value>The image.</value>
        string Image { get; }
        /// <summary>
        /// Callback action to be called when document is selected
        /// </summary>
        /// <value>Callback action</value>
        DashboardAction Action { get; }
    }
}
