﻿using System;
using System.Collections.Generic;
using System.Linq;
using myGovID.Business.Contract;
using myGovID.Business.Contract.Common.Application;

namespace myGovID.Presentation.Contract.Steps.Dashboard.Document
{
    public class DocumentBuilder
    {
        private static readonly IList<Type> OrderedDocumentTypes = new List<Type> {
            typeof(FacialVerification),
            typeof(PassportDocument),
            typeof(DriverLicenceDocument),
            typeof(MedicareDocument)
        };

        private readonly IEnumerable<Capability> _capabilities;

        public DocumentBuilder(IEnumerable<Capability> capabilities)
        {
            _capabilities = capabilities;
        }

        public IEnumerable<IDocument> FromStates(IEnumerable<BusinessState> states)
        {
            var result = new HashSet<IDocument>();
            foreach (BusinessState state in states)
            {
                var document = FromState(state);
                if (document != null)
                {
                    result.Add(document);
                }
            }
            return result.OrderBy((doc) => OrderedDocumentTypes.IndexOf(doc.GetType())).ToList();
        }

        public IDocument FromState(BusinessState state)
        {
            switch (state)
            {
                case BusinessState.FVS:
                    return new FacialVerification();
                case BusinessState.Passport:
                    var supportsDVS = _capabilities?.Contains(Capability.DVS_Passport) ?? false;
                    return new PassportDocument(supportsDVS);
                case BusinessState.DriverLicence:
                    return new DriverLicenceDocument();
                case BusinessState.Medicare:
                    return new MedicareDocument();
            }
            return null;
        }
    }
}
