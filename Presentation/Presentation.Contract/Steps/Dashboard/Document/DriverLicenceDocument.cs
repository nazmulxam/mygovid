﻿using System;

namespace myGovID.Presentation.Contract.Steps.Dashboard.Document
{
    public class DriverLicenceDocument : IDocument
    {
        public string Title => "Driver licence";
        public string Description => "Verify your details";
        public string Image => "DocumentAddIcon";
        public DashboardAction Action => DashboardAction.DriverLicence;
    }
}
