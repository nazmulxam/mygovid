﻿using System;
namespace myGovID.Presentation.Contract.Steps.Dashboard.Alert
{
    public enum AlertType
    {
        Information, Warning
    }
}
