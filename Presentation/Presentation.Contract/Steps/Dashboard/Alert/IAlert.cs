﻿using System;
namespace myGovID.Presentation.Contract.Steps.Dashboard.Alert
{
    public interface IAlert
    {
        /// <summary>
        /// Message that goes with the Alert
        /// </summary>
        /// <value>The message.</value>
        string Message { get; }
        /// <summary>
        /// Whether this alert is a system outage alert.
        /// </summary>
        /// <value>true/false</value>
        bool IsSystemOutage { get; }
        /// <summary>
        /// Type of the alert
        /// </summary>
        /// <value>AlertType.Information or AlertType.Warning</value>
        AlertType Type { get; }
    }
}
