﻿using System;
namespace myGovID.Presentation.Contract.Steps.Dashboard.Alert
{
    public class Alert: IAlert
    {
        public string Message { get; }
        public bool IsSystemOutage { get; }
        public AlertType Type { get; }

        public Alert(string message, AlertType alertType, bool isSystemOutage)
        {
            if(string.IsNullOrEmpty(message)) 
            {
                throw new ArgumentNullException(nameof(message));
            }
            Message = message;
            Type = alertType;
            IsSystemOutage = isSystemOutage;
        }
    }
}
