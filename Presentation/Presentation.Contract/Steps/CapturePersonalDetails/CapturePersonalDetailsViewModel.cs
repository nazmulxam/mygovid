﻿using myGovID.Presentation.Contract.Inputs;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Globalization;

namespace myGovID.Presentation.Contract.Steps.CapturePersonalDetails
{
    public class CapturePersonalDetailsViewModel : FormViewModel
    {
        public CapturePersonalDetailsViewModel() : base(new List<IInput> {
            new PersonalDetailsNameInput("Given name/s", "givenNamesInput", false, 0, 102, InputMetaData.GivenName),
            new PersonalDetailsNameInput("Family name", "familyNameInput", true, 1, 40, InputMetaData.FamilyName),
            new BirthDateInput("Date of birth", "dateOfBirth")
        })
        { }

        public string GetFirstName() 
        {
            return ((PersonalDetailsNameInput)Inputs.First(input => input.MetaData == InputMetaData.GivenName)).Value;
        }

        public string GetLastName()
        {
            return ((PersonalDetailsNameInput) Inputs.First(input => input.MetaData == InputMetaData.FamilyName)).Value;
        }

        public DateTime GetDateOfBirth()
        {
            string dateOfBirth = ((BirthDateInput) Inputs.First(input => input.MetaData == InputMetaData.DateOfBirth)).Value;
            DateTime.TryParseExact(dateOfBirth, DateFormat.DateOfBirth.GetFormat(), 
                                   CultureInfo.InvariantCulture, DateTimeStyles.None, 
                                   out DateTime dateTime);
            return dateTime;
        }
    }
}