﻿using System;
using System.Collections.Generic;

namespace myGovID.Presentation.Contract.Steps.OnBoarding
{
    public class OnBoardingViewModel : IViewModel
    {
        public OnBoardingModel[] Pages { get; }
        public IEnumerable<PageMessage> Messages { get; set; }
        public bool IsNextVisible(int currentPage) => currentPage == Pages.Length - 1;
        public OnBoardingViewModel(OnBoardingModel[] pages)
        {
            if(pages == null || pages.Length == 0) 
            {
                throw new ArgumentNullException(nameof(pages));
            }
            Pages = pages;
        }
        public bool IsValid() => true;
    }
}
