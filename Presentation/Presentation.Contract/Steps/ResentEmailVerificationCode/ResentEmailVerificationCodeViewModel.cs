﻿using System;
using System.Collections.Generic;

namespace myGovID.Presentation.Contract.Steps.ResentEmailVerificationCode
{
    public class ResentEmailVerificationCodeViewModel : IViewModel
    {
        public string EmailAddress { get; }
        public IEnumerable<PageMessage> Messages { get; set; }
        public ResentEmailVerificationCodeViewModel(string emailAddress) 
        {
            if(string.IsNullOrEmpty(emailAddress)) 
            {
                throw new ArgumentNullException(nameof(emailAddress));
            }
            EmailAddress = emailAddress;
        }
        public bool IsValid() => true;
    }
}
