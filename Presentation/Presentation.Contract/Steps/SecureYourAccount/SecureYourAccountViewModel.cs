﻿using System;
using myGovID.Presentation.Contract.Inputs;
using System.Collections.Generic;
using System.Linq;

namespace myGovID.Presentation.Contract.Steps.SecureYourAccount
{
    public class SecureYourAccountViewModel : FormViewModel
    {
        public string Image { get; }
        public string Description { get; }
        public string BiometricType { get; }
        public SecureYourAccountViewModel(string biometricType, string description, string image) : base(new List<IInput> { 
            new BooleanInput(String.Format("<b>Enable {0}</b>", biometricType), "useBiometric", false, InputMetaData.EnableBiometric) 
        })
        {
            if(string.IsNullOrEmpty(image)) 
            {
                throw new ArgumentNullException(nameof(image));
            }
            Description = description;
            Image = image;
            BiometricType = biometricType;
        }
        public bool UseBiometrics()
        {
            return ((BooleanInput)Inputs.First()).Value;
        }
    }
}