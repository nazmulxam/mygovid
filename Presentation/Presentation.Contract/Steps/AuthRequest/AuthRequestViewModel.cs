﻿using myGovID.Presentation.Contract.Inputs;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace myGovID.Presentation.Contract.Steps.AuthRequest
{
    public class AuthRequestViewModel : FormViewModel
    {
        public readonly string AlertTitle = "Authentication request";
        public readonly string AlertMessage = "A request has been received to login to an online service. Enter your reference code below.";
        public readonly string AcceptButtonTitle = "Accept";
        public readonly string RejectButtonTitle = "Decline";

        public int AuthRequestLength { get; }

        public CodeInput CodeEntry => ((CodeInput)Inputs.First((input) => input.MetaData == InputMetaData.VerificationCode));

        public AuthRequestViewModel(int authRequestLength)
            : base(new List<IInput> { new CodeInput("Authentication request", "authenticationRequestInput", authRequestLength) })
        {
            AuthRequestLength = authRequestLength;
        }
    }
}
