﻿using System;
using myGovID.Presentation.Contract.Steps.RecoverableDeadEnd;

namespace myGovID.Presentation.Contract.Steps.MaxAttempts
{
    public interface IMaxAttemptsReachedStep : IRecoverableDeadEndStep
    {
    }
}
