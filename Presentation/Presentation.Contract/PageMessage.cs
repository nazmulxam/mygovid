﻿namespace myGovID.Presentation.Contract
{
    /// <summary>
    /// Message returned from backend in case of any errors
    /// </summary>
    public class PageMessage
    {
        /// <summary>
        /// Gets or sets the message code. e.g. 'POI31091 (400)'
        /// </summary>
        /// <value>The code.</value>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the message type. e.g. 'Error'
        /// </summary>
        /// <value>The type.</value>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the title. e.g. 'An error has occurred'
        /// </summary>
        /// <value>The title.</value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the description. e.g. '<p>An unexpected error has occurred</p>'
        /// </summary>
        /// <value>The description.</value>
        public string Description { get; set; }
    }
}