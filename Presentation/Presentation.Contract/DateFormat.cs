﻿namespace myGovID.Presentation.Contract
{
    /// <summary>
    /// Supported date formats
    /// </summary>
    public enum DateFormat
    {
        DateOfBirth,
        YellowMedicareCardExpiry,
        BlueMedicareCardExpiry,
        GreenMedicareCardExpiry
    }
}
