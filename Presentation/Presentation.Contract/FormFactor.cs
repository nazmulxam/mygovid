﻿namespace myGovID.Presentation.Contract
{
    public enum FormFactor
    {
        Phone, Pad, Unknown
    }
}
