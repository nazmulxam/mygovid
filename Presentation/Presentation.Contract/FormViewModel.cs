﻿using System.Collections.Generic;
using System.Linq;
using EnsureThat;
using Newtonsoft.Json;
namespace myGovID.Presentation.Contract
{
    public class FormViewModel : IViewModel
    {
        [JsonIgnore]
        public IEnumerable<IInput> Inputs { get; protected set; }

        protected FormViewModel(): this(Enumerable.Empty<IInput>())
        {
        }

        public FormViewModel(IEnumerable<IInput> inputs)
        {
            Ensure.Any.IsNotNull(inputs, nameof(inputs));
            Inputs = inputs;
        }

        public bool IsValid()
        {
            return Inputs.All(input => input.IsValid());
        }
        protected T GetInput<T>(InputMetaData metaData)
        {
            return (T)Inputs.First(input => input.MetaData == metaData);
        }
    }
}
