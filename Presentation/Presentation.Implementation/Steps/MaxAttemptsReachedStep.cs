﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.MaxAttempts;
using myGovID.Presentation.Contract.Steps.RecoverableDeadEnd;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class MaxAttemptsReachedStep : BaseStep<BaseState, RecoverableDeadEndViewModel>, IMaxAttemptsReachedStep
    {
        public MaxAttemptsReachedStep(BaseState state,
                                                 PresentationError error,
                                                 IEnumerable<PageMessage> messages = null,
                                                 IStep childStep = null)
            : base(state, new RecoverableDeadEndViewModel(error), messages, childStep)
        {
            Actions = new List<IAction<ActionType, Enum>> {
                new TaskAction<Enum>(ActionType.Next, OnNext, default)
            };
        }
        protected Task<IStep> OnNext()
        {
            return State.FinaliseStateAsync();
        }
    }
}