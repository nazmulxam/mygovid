﻿using myGovID.Presentation.States;
using myGovID.Presentation.Contract.Steps.Browse;
using myGovID.Presentation.Contract;
using System.Collections.Generic;

namespace myGovID.Presentation.Steps
{
    public class BrowseStep : BaseStep<BaseState, BrowseViewModel>, IBrowseStep
    {
        public BrowseStep(BaseState state, BrowseViewModel stepViewModel) : base(state, stepViewModel, null, null)
        {
        }
    }
}
