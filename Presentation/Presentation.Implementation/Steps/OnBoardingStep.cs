﻿using System.Collections.Generic;
using myGovID.Presentation.Contract.Steps.OnBoarding;
using myGovID.Presentation.Contract;
using myGovID.Presentation.States;
using System;
using System.Threading.Tasks;

namespace myGovID.Presentation.Steps
{
    public class OnBoardingStep : BaseStep<InitialRegistrationState, OnBoardingViewModel>, IOnBoardingStep
    {
        public OnBoardingStep(InitialRegistrationState state, IStep childStep = null) : base(state, new OnBoardingViewModel(new OnBoardingModel[] {
            new OnBoardingModel("OnBoardingPage1",
                                "What is myGovID?",
                                "It's a secure, safe and easy way for you to prove who you are when logging in to government online services."),
            new OnBoardingModel("OnBoardingPage2",
                                "Before you start",
                                "Verify your identity using your passport, Medicare card and driver licence."),
            new OnBoardingModel("OnBoardingPage3",
                                "Login securely",
                                "Login to government services securely anywhere, anytime and on any device.")
        }), null, childStep)
        {
            Actions = new List<IAction<ActionType, Enum>> {
                new TaskAction<Enum>(ActionType.Next, OnNextClicked, default)
            };
        }
        protected Task<IStep> OnNextClicked()
        {
            return State.ShowTermsAndConditionsAsync();
        }
    }
}