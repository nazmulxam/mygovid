﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Logout;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class LogoutStep : BaseStep<LogoutState, LogoutViewModel>, ILogoutStep
    {
        public LogoutStep(LogoutState state, IStep childStep = null) : base(state, new LogoutViewModel(), null, childStep)
        {
            Actions = new List<IAction<ActionType, Enum>> {
                new TaskAction<Enum>(ActionType.Previous, OnDismiss, default),
                new TaskAction<Enum>(ActionType.Next, OnDismiss, default)
            };
        }
        protected Task<IStep> OnDismiss()
        {
            return State.FinaliseStateAsync();
        }
    }
}