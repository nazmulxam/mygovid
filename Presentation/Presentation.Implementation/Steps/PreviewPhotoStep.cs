﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.PreviewPhoto;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class PreviewPhotoStep : BaseStep<BaseState, PreviewPhotoViewModel>, IPreviewPhotoStep
    {
        public PreviewPhotoStep(BaseState state, string imageInBase64, IEnumerable<PageMessage> messages = null, IStep childStep = null) : base(state, new PreviewPhotoViewModel(imageInBase64), messages, childStep)
        {
            if (string.IsNullOrEmpty(imageInBase64))
            {
                throw new ArgumentNullException(nameof(imageInBase64));
            }
            Actions = new List<IAction<ActionType, Enum>> {
                new TaskAction<Enum>(ActionType.Next, OnNextClicked, default),
                new TaskAction<Enum>(ActionType.Cancel, OnCancelClicked, default),
                new TaskAction<Enum>(ActionType.Previous, OnPreviousClicked, default),
            };
        }
        protected Task<IStep> OnCancelClicked()
        {
            return Task.Factory.StartNew<IStep>(() =>
            {
                return new OnBoardingStep(null);
            });
        }
        protected Task<IStep> OnNextClicked()
        {
            throw new NotImplementedException();
        }
        protected Task<IStep> OnPreviousClicked()
        {
            return Task.Factory.StartNew<IStep>(() =>
            {
                return new CapturePhotoStep(null);
            });
        }
    }
}