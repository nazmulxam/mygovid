﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.CaptureEmailVerification;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class CaptureEmailVerificationStep: BaseStep<EmailVerificationState, CaptureEmailVerificationViewModel>, ICaptureEmailVerificationStep
    {
        private const int EmailVerificationLength = 6;
        public CaptureEmailVerificationStep(EmailVerificationState state, string emailAddress, IEnumerable<PageMessage> messages = null, IStep childStep = null) : base(state, new CaptureEmailVerificationViewModel(emailAddress, EmailVerificationLength), messages, childStep)
        {
            if (string.IsNullOrEmpty(emailAddress))
            {
                throw new ArgumentNullException(nameof(emailAddress));
            }
            Actions = new List<IAction<ActionType, Enum>> {
                new TaskAction<Enum>(ActionType.Previous, OnChangeEmail, default, true),
                new TaskAction<Enum>(ActionType.Next, OnSendEmailVerification, default),
                new TaskAction<Enum>(ActionType.Clone, OnResendEmailVerificationCode, default, true)
            };
        }
        protected Task<IStep> OnChangeEmail()
        {
            return State.ChangeEmailAddressAsync();
        }
        protected Task<IStep> OnResendEmailVerificationCode()
        {
            return State.ResendVerificationCodeAsync(StepViewModel);
        }
        protected Task<IStep> OnSendEmailVerification()
        {
            return State.SubmitVerificationCodeAsync(StepViewModel);
        }
    }
}