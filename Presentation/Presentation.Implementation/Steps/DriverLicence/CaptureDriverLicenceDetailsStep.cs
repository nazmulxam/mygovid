﻿using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.DriverLicence;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class CaptureDriverLicenceDetailsStep : BaseStep<DriverLicenceState, DriverLicenceViewModel, CaptureDriverLicenceDetailsAction>, ICaptureDriverLicenceDetailsStep
    {
        public CaptureDriverLicenceDetailsStep(DriverLicenceState state, DriverLicenceViewModel driverLicenceInputViewModel = null, IEnumerable<PageMessage> messages = null, IStep childStep = null)
        : base(state, new DriverLicenceViewModel(false), messages, childStep)
        {
            Messages = messages;
            Actions = new List<IAction<ActionType, CaptureDriverLicenceDetailsAction>> {
                new TaskAction<CaptureDriverLicenceDetailsAction>(ActionType.Next, OnSubmit, CaptureDriverLicenceDetailsAction.Submit),
                new TaskAction<CaptureDriverLicenceDetailsAction>(ActionType.Previous, OnBack, CaptureDriverLicenceDetailsAction.Back, true),
                new TaskAction<CaptureDriverLicenceDetailsAction>(ActionType.Previous, OnRescan, CaptureDriverLicenceDetailsAction.Rescan, true),
                new TaskAction<CaptureDriverLicenceDetailsAction>(ActionType.Browse, OnBrowseClicked, CaptureDriverLicenceDetailsAction.Help, true)
            };
        }
        protected Task<IStep> OnSubmit()
        {
            return State.SubmitDriverLicenceDetailsAsync(StepViewModel);
        }
        protected async Task<IStep> OnBack()
        {
            return State.GoBack();
        }
        protected async Task<IStep> OnRescan()
        {
            return State.GoBack();
        }
        protected Task<IStep> OnBrowseClicked()
        {
            return State.OnHelp();
        }
    }
}