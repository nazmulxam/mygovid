﻿using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.DriverLicence;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps.DriverLicence
{
    public class DriverLicenceOCRIntroStep : BaseStep<DriverLicenceState, DriverLicenceOCRIntroViewModel, DriverLicenceOCRIntroAction>, IDriverLicenceOCRIntroStep
    {
        public DriverLicenceOCRIntroStep(DriverLicenceState state, IEnumerable<PageMessage> messages = null, IStep childStep = null)
        : base(state, new DriverLicenceOCRIntroViewModel(new DriverLicenceOCRIntroModel[] {
            new DriverLicenceOCRIntroModel("OCR_illustration_driverlicence",
                                "Scan your driver licence",
                                "Position the front of your card in the frame and your details will automatically scan."),
            new DriverLicenceOCRIntroModel("OCR_illustration_driverlicence2",
                                "Before you start",
                                "For best results hold your licence in your hand and then scan.")
        }), null, childStep)
        { 
            Messages = messages;
            Actions = new List<IAction<ActionType, DriverLicenceOCRIntroAction>> {
                new TaskAction<DriverLicenceOCRIntroAction>(ActionType.Next, OnEnterManually, DriverLicenceOCRIntroAction.ManualEntry),
                new TaskAction<DriverLicenceOCRIntroAction>(ActionType.Next, OnStart, DriverLicenceOCRIntroAction.OCRCapture),
                new TaskAction<DriverLicenceOCRIntroAction>(ActionType.Previous, OnPreviousAsync, default, true)
            };
        }
        private Task<IStep> OnEnterManually()
        {
            return State.OnEnterManually();
        }
        private Task<IStep> OnStart()
        {
            return State.OnStartOCRCapture();
        }
        private async Task<IStep> OnPreviousAsync()
        {
            return State.GoBack();
        }
    }
}