﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.DriverLicence;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps.DriverLicence
{
    public class DriverLicenceOCRStep : BaseStep<DriverLicenceState, DriverLicenceViewModel>
    {
        public DriverLicenceOCRStep(DriverLicenceState state, DriverLicenceViewModel stepViewModel, IEnumerable<PageMessage> messages = null, IStep childStep = null) : base(state, stepViewModel, messages, childStep)
        {
            Actions = new List<IAction<ActionType, Enum>> {
                new TaskAction<Enum>(ActionType.Next, OnEnterManually, DriverLicenceOCRIntroAction.ManualEntry),
                 new TaskAction<Enum>(ActionType.Next, OnCapture, DriverLicenceOCRIntroAction.OCRCapture)
            };
        }

        private Task<IStep> OnEnterManually()
        {
            throw new NotImplementedException();
        }

        private Task<IStep> OnCapture()
        {
            throw new NotImplementedException();
        }
    }
}
