﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.MedicareCardInput;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class MedicareCardInputStep : BaseStep<MedicareState, MedicareCardInputViewModel, MedicareCardInputAction>, IMedicareCardInputStep
    {
        public MedicareCardInputStep(MedicareState state, bool didUseOcr, IEnumerable<PageMessage> messages = null, IStep childStep = null)
        : base(state, new MedicareCardInputViewModel(didUseOcr), messages, childStep)
        {
            Actions = new List<IAction<ActionType, MedicareCardInputAction>> {
                new TaskAction<MedicareCardInputAction>(ActionType.Next, OnSubmit, MedicareCardInputAction.Submit),
                new TaskAction<MedicareCardInputAction>(ActionType.Previous, OnRescan, MedicareCardInputAction.Rescan, true),
                new TaskAction<MedicareCardInputAction>(ActionType.Previous, OnBack, MedicareCardInputAction.Back, true),
                new TaskAction<MedicareCardInputAction>(ActionType.Browse, OnHelp, MedicareCardInputAction.Help, true)
            };
        }
        protected Task<IStep> OnSubmit()
        {
            return State.ProcessMedicareCardInput(StepViewModel);
        }

        protected Task<IStep> OnRescan()
        {
            return Task.FromResult(State.BeginOcr());
        }

        protected Task<IStep> OnBack()
        {
            return Task.FromResult(State.GoBack());
        }

        protected Task<IStep> OnHelp()
        {
            return Task.FromResult(State.LaunchHelp());
        }
    }
}
