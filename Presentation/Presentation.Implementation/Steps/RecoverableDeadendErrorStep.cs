﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.RecoverableDeadEnd;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class RecoverableDeadEndErrorStep : BaseStep<RecoverableDeadEndState, RecoverableDeadEndViewModel>, IRecoverableDeadEndStep
    {
        public RecoverableDeadEndErrorStep(RecoverableDeadEndState state,
                                           PresentationError error,
                                           IEnumerable<PageMessage> messages = null,
                                           IStep childStep = null)
            : base(state, new RecoverableDeadEndViewModel(error), messages, childStep)
        {
            Actions = new List<IAction<ActionType, Enum>> {
                new TaskAction<Enum>(ActionType.Next, OnNext, default)
            };
        }
        protected async Task<IStep> OnNext()
        {
            return await State.FinaliseStateAsync();
        }
    }
}