﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.AuthRequest;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class AuthRequestStep : BaseStep<AuthRequestState, AuthRequestViewModel>, IAuthRequestStep
    {
        private const int AuthRequestCodeLength = 4;

        public AuthRequestStep(AuthRequestState state,
                                IEnumerable<PageMessage> messages = null,
                                IStep childStep = null)
        : base(state, new AuthRequestViewModel(AuthRequestCodeLength), messages, childStep)
        {
            List<IAction<ActionType, Enum>> actions = new List<IAction<ActionType, Enum>>
            {
                new TaskAction<Enum>(ActionType.Next, OnAccept, default),
                new TaskAction<Enum>(ActionType.Previous, OnReject, default, true)
            };

            Actions = actions;
        }

        protected Task<IStep> OnAccept()
        {
            return State.FinaliseStateAsync();
        }

        protected Task<IStep> OnReject()
        {
            return State.DismissOptionalUpgradeAsync();
        }
    }
}