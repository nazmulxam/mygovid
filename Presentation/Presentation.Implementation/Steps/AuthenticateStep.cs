﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Authenticate;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class AuthenticateStep : BaseStep<AuthenticateState, AuthenticateViewModel, AuthenticateAction>, IAuthenticateStep
    {
        public AuthenticateStep(AuthenticateState state, AuthenticateViewModel stepViewModel, IEnumerable<PageMessage> messages, IStep childStep = null) : base(state, stepViewModel, messages, childStep)
        {
            Actions = new List<IAction<ActionType, AuthenticateAction>> {
                new TaskAction<AuthenticateAction>(ActionType.Next, OnBiometricAuthenticationAsync, AuthenticateAction.Biometric, true),
                new TaskAction<AuthenticateAction>(ActionType.Next, OnPasswordAuthenticationAsync, AuthenticateAction.Password),
                new TaskAction<AuthenticateAction>(ActionType.Next, OnForgottenPasswordAsync, AuthenticateAction.ForgottenPassword, true)
            };
        }
        protected Task<IStep> OnBiometricAuthenticationAsync()
        {
            return State.OnBiometricAuthenticationAsync();
        }
        protected Task<IStep> OnPasswordAuthenticationAsync()
        {
            return State.OnPasswordAuthenticationAsync(StepViewModel);
        }
        protected Task<IStep> OnForgottenPasswordAsync()
        {
            return State.OnForgottenPasswordAsync();
        }
    }
}
