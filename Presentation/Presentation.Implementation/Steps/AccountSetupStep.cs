﻿using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.AccountSetup;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class AccountSetupStep : BaseStep<AccountSetupState, AccountSetupViewModel, AccountSetupAction>, IAccountSetupStep
    {
        public AccountSetupStep(AccountSetupState state, IEnumerable<PageMessage> messages = null, IStep childStep = null) : base(state, new AccountSetupViewModel(), messages, childStep)
        {
            Messages = messages;
            Actions = new List<IAction<ActionType, AccountSetupAction>> {
                new TaskAction<AccountSetupAction>(ActionType.Next, OnRegisterClicked, AccountSetupAction.Register),
                new TaskAction<AccountSetupAction>(ActionType.Next, OnRecoverClicked, AccountSetupAction.Recover),
                new TaskAction<AccountSetupAction>(ActionType.Browse, OnBrowseClicked, AccountSetupAction.Help)
            };
        }
        protected Task<IStep> OnRegisterClicked()
        {
            return State.OnRegisterAsync();
        }
        protected Task<IStep> OnRecoverClicked()
        {
           return State.OnRecover();
        }
        protected Task<IStep> OnBrowseClicked() 
        {
            return State.OnHelp();
        }
    }
}
