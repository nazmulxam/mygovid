﻿using System;
using System.Collections.Generic;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.DeadEnd;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class DeadEndStep : BaseStep<DeadEndState, DeadEndViewModel>, IDeadEndStep
    {
        public DeadEndStep(DeadEndState state, PresentationError error, IStep childStep = null) : base(state, new DeadEndViewModel(error), null, childStep)
        {
            Actions = new List<IAction<ActionType, Enum>> { };
        }
    }
}
