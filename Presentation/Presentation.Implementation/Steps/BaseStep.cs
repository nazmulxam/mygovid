﻿using System;
using System.Collections.Generic;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.States;

namespace myGovID.Presentation.Steps
{
    public abstract class BaseStep<S, T> : BaseStep<S, T, Enum> where S : class, IState where T : class, IViewModel
    {
        protected BaseStep(S state, T stepViewModel, IEnumerable<PageMessage> messages, IStep childStep = null) : base(state, stepViewModel, messages, childStep)
        {
        }
    }
    public abstract class BaseStep<S, T, E> : IStep where S: class, IState where T : class, IViewModel
    {
        public T StepViewModel { get; }
        protected S State { get; private set; }
        public IViewModel ViewModel => StepViewModel;
        public IEnumerable<IAction<ActionType, E>> Actions { get; protected set; }
        public IStep ChildStep { get; set; }
        public IEnumerable<PageMessage> Messages { get; set; }
        protected BaseStep(S state, T stepViewModel, IEnumerable<PageMessage> messages, IStep childStep = null)
        {
            StepViewModel = stepViewModel ?? throw new ArgumentNullException(nameof(stepViewModel));
            State = state;
            ChildStep = childStep;
            Messages = messages;
        }
        public IEnumerable<IAction<ActionType, T1>> GetActions<T1>()
        {
            return (IEnumerable<IAction<ActionType, T1>>)Actions;
        }
    }
}