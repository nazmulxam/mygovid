﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.CaptureEmail;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class CaptureEmailStep : BaseStep<EmailState, CaptureEmailViewModel>, ICaptureEmailStep
    {
        private const int EmailMaxLength = 254;
        public CaptureEmailStep(EmailState state, IEnumerable<PageMessage> messages = null, IStep childStep = null) : base(state, new CaptureEmailViewModel(EmailMaxLength), messages, childStep)
        {
            Actions = new List<IAction<ActionType, Enum>> {
                new TaskAction<Enum>(ActionType.Next, OnNextClicked, default)
            };
        }
        protected Task<IStep> OnNextClicked()
        {
            return State.SetEmailAddressAsync(StepViewModel);
        }
    }
}