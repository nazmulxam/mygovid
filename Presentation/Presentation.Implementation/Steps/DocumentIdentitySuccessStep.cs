﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.States;
using myGovID.Presentation.Contract.Steps;
using myGovID.Presentation.Contract.Steps.DocumentIdentitySuccess;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps.DocumentIdentitySuccess
{
    public class DocumentIdentitySuccessStep : BaseStep<IState, EmptyViewModel>, IDocumentIdentitySuccessStep
    {
        public DocumentIdentitySuccessStep(BaseState state) : base(state, new EmptyViewModel(), null, null)
        {
            Actions = new List<IAction<ActionType, Enum>> {
                new TaskAction<Enum>(ActionType.Next, OnNext, default),
            };
        }

        public Task<IStep> OnNext()
        {
            return State.FinaliseStateAsync();
        }
    }
}
