﻿using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.CapturePhoto;
using myGovID.Presentation.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace myGovID.Presentation.Steps
{
    public class CapturePhotoStep : BaseStep<BaseState, CapturePhotoViewModel>, ICapturePhotoStep
    {
        public CapturePhotoStep(BaseState state, IEnumerable<PageMessage> messages = null, IStep childStep = null) : base(state, new CapturePhotoViewModel(ImageType.JPG, 1.0f), messages, childStep)
        {
            Actions = new List<IAction<ActionType, Enum>> {
                new TaskAction<Enum>(ActionType.Next, OnNextClicked, default),
                new TaskAction<Enum>(ActionType.Cancel, OnCancelClicked, default)
            };
        }
        protected Task<IStep> OnCancelClicked()
        {
            return Task.Factory.StartNew<IStep>(() =>
            {
                return new CaptureEmailStep(null);
            });
        }
        protected Task<IStep> OnNextClicked()
        {
            return Task.Factory.StartNew<IStep>(() =>
            {
                return new PreviewPhotoStep(State, ((FormInput<string>)StepViewModel.Inputs.First()).Value, null);
            });
        }
    }
}