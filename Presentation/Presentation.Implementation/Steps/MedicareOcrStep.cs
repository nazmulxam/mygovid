﻿using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.MedicareOcrStep;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class MedicareOcrStep : BaseStep<MedicareState, MedicareOcrViewModel, MedicareOcrAction>, IMedicareOcrStep
    {
        public MedicareOcrStep(MedicareState state, IEnumerable<PageMessage> messages = null, IStep childStep = null)
        : base(state, new MedicareOcrViewModel(), messages, childStep)
        {
            Actions = new List<IAction<ActionType, MedicareOcrAction>> {
                new TaskAction<MedicareOcrAction>(ActionType.Next, OnPerformScan, MedicareOcrAction.PerformScan),
                new TaskAction<MedicareOcrAction>(ActionType.Next, OnEnterManually, MedicareOcrAction.EnterManually)
            };
        }
        protected Task<IStep> OnPerformScan()
        {
            return Task.FromResult(State.ProcessMedicareOcr(StepViewModel));
        }

        protected Task<IStep> OnEnterManually()
        {
            return Task.FromResult(State.EnterManually());
        }
    }
}
