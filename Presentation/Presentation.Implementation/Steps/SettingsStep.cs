﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Dashboard;
using myGovID.Presentation.Contract.Steps.Settings;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class SettingsStep : BaseStep<SettingsState, SettingsViewModel, SettingsAction>, ISettingsStep
    {

        public SettingsStep(SettingsState state = null, IEnumerable<PageMessage> messages = null, IStep childStep = null)
            : base(state, new SettingsViewModel(), messages, childStep)
        {
            Actions = new List<IAction<ActionType, SettingsAction>> {
                new TaskAction<SettingsAction>(ActionType.Next, OnChangePassword, SettingsAction.ChangePassword),
                new TaskAction<SettingsAction>(ActionType.Next, OnBiometrics, SettingsAction.Biometrics),
                new TaskAction<SettingsAction>(ActionType.Next, OnNotification, SettingsAction.Notification),
                new TaskAction<SettingsAction>(ActionType.Browse, OnTermsOfUse, SettingsAction.TermsOfUse),
                new TaskAction<SettingsAction>(ActionType.Browse, OnPrivacy, SettingsAction.Privacy),
                new TaskAction<SettingsAction>(ActionType.Previous, OnDashboard, default, true),

            };
        }

        protected Task<IStep> OnDashboard()
        {
            return State.GoToDashboard();
        }

        protected Task<IStep> OnChangePassword()
        {
            return State.GoToChangePassword();
        }

        protected Task<IStep> OnBiometrics()
        {
            return State.SetBiometrics();
        }

        protected Task<IStep> OnNotification()
        {
            return State.SetNotification();
        }

        protected Task<IStep> OnTermsOfUse()
        {
            return State.LaunchUrl(StepViewModel.TermsOfUse.Url);
        }

        protected Task<IStep> OnPrivacy()
        {
            return State.LaunchUrl(StepViewModel.Privacy.Url);
        }
    }
}