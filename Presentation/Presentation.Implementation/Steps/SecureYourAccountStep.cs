﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.SecureYourAccount;
using myGovID.Presentation.States;
using System.Linq;

namespace myGovID.Presentation.Steps
{
    public class SecureYourAccountStep : BaseStep<PersonalDetailsState, SecureYourAccountViewModel>, ISecureYourAccountStep
    {
        public SecureYourAccountStep(PersonalDetailsState state, BiometricType type, IEnumerable<PageMessage> messages, IStep childStep = null) : base(state, new SecureYourAccountViewModel(type.Description().DisplayName, GetDescription(type), type.Description().Image), messages, childStep)
        {
            Actions = new List<IAction<ActionType, Enum>> {
                new TaskAction<Enum>(ActionType.Next, OnNext, default)
            };
        }
        protected Task<IStep> OnNext()
        {
            return State.UseBiometricsAsync(StepViewModel);
        }
        private static string GetDescription(BiometricType biometricType)
        {
            string _message = "You can enable <b>{0}</b> to login using your <b>{1}</b>.";
            string _additionalInformation = "By enabling <b>{0}</b>, you accept that having anyone else&rsquo;s fingerprint stored on this device will authorise them access to your myGovID.";
            BiometricTypeDescription description = biometricType.Description();
            switch(biometricType)
            {
                case BiometricType.Face: return string.Format(_message, description.DisplayName, description.BiometricName);
                case BiometricType.Touch: return string.Format("{0}<br><br>{1}",
                        string.Format(_message, description.DisplayName, description.BiometricName),
                        string.Format(_additionalInformation, description.DisplayName)
                    );
                case BiometricType.FingerPrint: return string.Format("{0}<br><br>{1}",
                        string.Format("You can use your {0} to login to myGovID.", description.DisplayName),
                        string.Format("By using your {0} to login, you accept that having anyone else&rsquo;s {0} stored on this device will authorise them access to your myGovID", description.DisplayName)
                    );
                default: return "";
            }
        }
    }
}