﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.VersionUpgrade;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class VersionUpgradeStep : BaseStep<VersionUpgradeState, VersionUpgradeViewModel>, IVersionUpgradeStep
    {
        public VersionUpgradeStep(VersionUpgradeState state, VersionUpgradeViewModel stepViewModel, IStep childStep = null) : base(state, stepViewModel, null, childStep)
        {
            List<IAction<ActionType, Enum>> actions = new List<IAction<ActionType, Enum>>
            {
                new TaskAction<Enum>(ActionType.Next, OnAccept, default)
            };
            //if this a optional upgrade; then allow user to cancel the 
            //upgrade process
            if(!stepViewModel.ForcedUpdate) {
                actions.Add(new TaskAction<Enum>(ActionType.Previous, OnReject, default));
            }
            Actions = actions;
        }
        protected Task<IStep> OnAccept()
        {
            return State.FinaliseStateAsync();
        }
        protected Task<IStep> OnReject()
        {
            return State.DismissOptionalUpgradeAsync();
        }
    }
}