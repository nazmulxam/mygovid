﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.TermsAndConditionsConfirm;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class TermsAndConditionsConfirmStep : BaseStep<InitialRegistrationState, TermsAndConditionsConfirmViewModel>, ITermsAndConditionsConfirmStep
    {
        public string Link { get; }
        public string Version { get; }
        public TermsAndConditionsConfirmStep(InitialRegistrationState state, string link, string version, IEnumerable<PageMessage> messages = null, IStep childStep = null) : base(state, new TermsAndConditionsConfirmViewModel(), messages, childStep)
        {
            if (string.IsNullOrEmpty(link))
            {
                throw new ArgumentNullException(nameof(link));
            }
            if (string.IsNullOrEmpty(version))
            {
                throw new ArgumentNullException(nameof(version));
            }
            Link = link;
            Version = version;
            Actions = new List<IAction<ActionType, Enum>> {
                new TaskAction<Enum>(ActionType.Previous, OnPreviousAsync, default),
                new TaskAction<Enum>(ActionType.Next, OnReject, default)
            };
        }
        protected async Task<IStep> OnPreviousAsync()
        {
            return State.GoBack();
        }
        protected Task<IStep> OnReject()
        {
           return State.OnTermsAndConditionsDeclinedAsync();
        }
    }
}
