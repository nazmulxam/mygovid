﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.CapturePassword;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class CapturePasswordStep : BaseStep<PersonalDetailsState, CapturePasswordViewModel>, ICapturePasswordStep
    {
        public CapturePasswordStep(PersonalDetailsState state, IEnumerable<PageMessage> messages = null, IStep childStep = null) : base(state, new CapturePasswordViewModel(), messages, childStep)
        {
            Actions = new List<IAction<ActionType, Enum>> {
                new TaskAction<Enum>(ActionType.Next, OnCreateCredential, default)
            };
        }
        protected Task<IStep> OnCreateCredential()
        {
            return State.SetPasswordAsync(StepViewModel);
        }
    }
}