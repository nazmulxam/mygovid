﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Contract.Steps.CapturePersonalDetails;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.CapturePersonalDetails;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class CapturePersonalDetailsStep : BaseStep<PersonalDetailsState, CapturePersonalDetailsViewModel>, ICapturePersonalDetailsStep
    {
        public CapturePersonalDetailsStep(PersonalDetailsState state, IEnumerable<PageMessage> messages = null, IStep childStep = null) : base(state, new CapturePersonalDetailsViewModel(), messages, childStep)
        {
            Actions = new List<IAction<ActionType, Enum>> {
                new TaskAction<Enum>(ActionType.Next, OnNext, default)
            };
        }
        protected Task<IStep> OnNext()
        {
            return State.SetPersonalDetailsAsync(StepViewModel);
        }
    }
}