﻿using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Passport;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps.Passport
{
    public class CapturePassportDetailsStep : BaseStep<PassportState, PassportViewModel, CapturePassportDetailsAction>, ICapturePassportDetailsStep
    {
        public CapturePassportDetailsStep(PassportState state, PassportViewModel passportViewModel = null, IEnumerable<PageMessage> messages = null, IStep childStep = null) : base(state, new PassportViewModel(false), messages, childStep)
        {
            Actions = new List<IAction<ActionType, CapturePassportDetailsAction>> {
                new TaskAction<CapturePassportDetailsAction>(ActionType.Next, OnSubmit, CapturePassportDetailsAction.Submit),
                new TaskAction<CapturePassportDetailsAction>(ActionType.Previous, OnBack, CapturePassportDetailsAction.Back, true),
                new TaskAction<CapturePassportDetailsAction>(ActionType.Previous, OnRescan, CapturePassportDetailsAction.Rescan, true),
                new TaskAction<CapturePassportDetailsAction>(ActionType.Browse, OnBrowseClicked, CapturePassportDetailsAction.Help, true)
            };
        }
        protected Task<IStep> OnSubmit()
        {
            return State.SubmitPassportDetailsAsync(StepViewModel);
        }
        protected async Task<IStep> OnBack()
        {
            return State.GoBack();
        }
        protected async Task<IStep> OnRescan()
        {
            return State.GoBack();
        }
        protected Task<IStep> OnBrowseClicked()
        {
            return State.OnHelp();
        }
    }
}