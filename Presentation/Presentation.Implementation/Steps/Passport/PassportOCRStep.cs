﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Passport;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps.Passport
{
    public class PassportOCRStep : BaseStep<PassportState, PassportViewModel>
    {
        public PassportOCRStep(PassportState state, PassportViewModel stepViewModel, IEnumerable<PageMessage> messages = null, IStep childStep = null) : base(state, stepViewModel, messages, childStep)
        {
            Actions = new List<IAction<ActionType, Enum>> {
                new TaskAction<Enum>(ActionType.Next, OnEnterManually, PassportOCRIntroAction.ManualEntry),
                 new TaskAction<Enum>(ActionType.Next, OnCapture, PassportOCRIntroAction.OCRCapture)
            };
        }

        private Task<IStep> OnEnterManually()
        {
            throw new NotImplementedException();
        }

        private Task<IStep> OnCapture()
        {
            throw new NotImplementedException();
        }
    }
}
