﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Passport;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps.Passport
{
    public class PassportOCRIntroStep : BaseStep<PassportState, PassportOCRIntroViewModel, PassportOCRIntroAction>, IPassportOCRIntroStep
    {
        public PassportOCRIntroStep(PassportState state, IEnumerable<PageMessage> messages = null, IStep childStep = null) : base(state, new PassportOCRIntroViewModel(), messages, childStep)
        {
            Actions = new List<IAction<ActionType, PassportOCRIntroAction>> {
                new TaskAction<PassportOCRIntroAction>(ActionType.Next, OnEnterManually, PassportOCRIntroAction.ManualEntry),
                new TaskAction<PassportOCRIntroAction>(ActionType.Next, OnStart, PassportOCRIntroAction.OCRCapture),
                new TaskAction<PassportOCRIntroAction>(ActionType.Previous, OnPreviousAsync, default, true),
            };
        }

        private Task<IStep> OnEnterManually()
        {
            return State.OnEnterManually();
        }

        private Task<IStep> OnStart()
        {
            return State.OnStartOCRCapture();
        }

        private async Task<IStep> OnPreviousAsync()
        {
            return State.GoBack();
        }
    }
}
