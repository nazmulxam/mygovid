﻿using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.MedicareOcrIntroStep;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class MedicareOcrIntroStep : BaseStep<MedicareState, MedicareOcrIntroViewModel, MedicareOcrIntroAction>, IMedicareOcrIntroStep
    {
        public MedicareOcrIntroStep(MedicareState state, IStep childStep = null) : base(state, new MedicareOcrIntroViewModel(), null, childStep)
        {
            Actions = new List<IAction<ActionType, MedicareOcrIntroAction>> {
                new TaskAction<MedicareOcrIntroAction>(ActionType.Next, OnScanDetails, MedicareOcrIntroAction.ScanDetails),
                new TaskAction<MedicareOcrIntroAction>(ActionType.Next, OnEnterManually, MedicareOcrIntroAction.EnterManually),
                new TaskAction<MedicareOcrIntroAction>(ActionType.Previous, OnBack, MedicareOcrIntroAction.Back, true)
            };
        }
        protected Task<IStep> OnScanDetails()
        {
            return Task.FromResult(State.BeginOcr());
        }

        protected Task<IStep> OnEnterManually()
        {
            return Task.FromResult(State.EnterManually());
        }

        protected Task<IStep> OnBack()
        {
            return Task.FromResult(State.GoBack());
        }
    }
}