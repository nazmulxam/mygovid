﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Dashboard;
using myGovID.Presentation.Contract.Steps.Dashboard.Alert;
using myGovID.Presentation.Contract.Steps.Dashboard.Document;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class DashboardStep: BaseStep<DashboardState, DashboardViewModel, DashboardAction>, IDashboardStep
    {
        public DashboardStep(DashboardState state, string firstName,
                             IEnumerable<IDocument> documents, IEnumerable<IAlert> alerts,
                             AuthStrengthType authStrengthType, IStep childStep = null) : 
        base(state, new DashboardViewModel(firstName, documents, alerts, authStrengthType), null, childStep)
        {
            var actions = new List<IAction<ActionType, DashboardAction>> {
                new TaskAction<DashboardAction>(ActionType.Next, OnAbout, DashboardAction.About),
                new TaskAction<DashboardAction>(ActionType.Next, OnSettings, DashboardAction.Settings),
                new TaskAction<DashboardAction>(ActionType.Next, OnShowTutorial, DashboardAction.Tutorial)
            };

            var documentMapping = new Dictionary<DashboardAction, Func<Task<IStep>>> {
                { DashboardAction.Passport, OnPassport },
                { DashboardAction.Medicare, OnMedicare },
                { DashboardAction.DriverLicence, OnDriverLicence },
                { DashboardAction.FVS, OnFacialVerification }
            };

            actions.AddRange(
                from document in StepViewModel.Documents
                select new TaskAction<DashboardAction>(ActionType.Next, documentMapping[document.Action], document.Action)
            );

            Actions = actions;
        }
        public void UpdateViewModel(string firstName, AuthStrengthType authStrength)
        {
            StepViewModel.FirstName = firstName;
            StepViewModel.AuthStrengthType = authStrength;
        }
        protected Task<IStep> OnAbout()
        {
            return State.OnAbout();
        }

        protected Task<IStep> OnAccountDetails()
        {
            return State.OnAccountDetails();
        }

        protected Task<IStep> OnSettings()
        {
            return State.OnSettings();
        }

        protected Task<IStep> OnShowTutorial()
        {
            return State.OnShowTutorial();
        }

        protected Task<IStep> OnPassport()
        {
            return State.OnPassport();
        }

        protected Task<IStep> OnMedicare()
        {
            return State.OnMedicare();
        }

        protected Task<IStep> OnDriverLicence()
        {
            return State.OnDriverLicenceAsync();
        }

        protected Task<IStep> OnFacialVerification()
        {
            return State.OnFacialVerification();
        }
    }
}