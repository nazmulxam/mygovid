﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.ResentEmailVerificationCode;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class ResentEmailVerificationCodeStep : BaseStep<EmailVerificationState, ResentEmailVerificationCodeViewModel>, IResentEmailVerificationCodeStep
    {
        public ResentEmailVerificationCodeStep(EmailVerificationState state, string emailAddress, IStep childStep = null) : base(state, new ResentEmailVerificationCodeViewModel(emailAddress), null, childStep)
        {
            if (string.IsNullOrEmpty(emailAddress))
            {
                throw new ArgumentNullException(nameof(emailAddress));
            }
            Actions = new List<IAction<ActionType, Enum>> {
                new TaskAction<Enum>(ActionType.Next, OnDismissAsync, default)
            };
        }
        protected async Task<IStep> OnDismissAsync()
        {
            return State.GoBack();
        }
    }
}