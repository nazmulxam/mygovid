﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.TermsAndConditions;
using myGovID.Presentation.States;

namespace myGovID.Presentation.Steps
{
    public class TermsAndConditionsStep : BaseStep<InitialRegistrationState, TermsAndConditionsViewModel>, ITermsAndConditionsStep
    {
        private static string _termsAndConditionsText = "By creating a myGovID, you agree to be bound by the <a href='{0}'><strong>terms and conditions</strong></a> of use." +
            "<br/><br/>You are also agreeing to how the Australian Government collects, uses, and discloses your personal information as set out in the <a href='{1}'><strong>privacy notice</strong></a>." +
            "<br/><br/>By proceeding, this means you have read and understood the terms and conditions of use and privacy notice.";
        public const string PrivacyLink = "https://www.mygovid.gov.au/privacy";
        public TermsAndConditionsStep(InitialRegistrationState state, string link, string version, IEnumerable<PageMessage> messages, IStep childStep = null) : 
        base(state, new TermsAndConditionsViewModel(string.Format(_termsAndConditionsText, link, PrivacyLink), true, link, version), messages, childStep)
        {
            Actions = new List<IAction<ActionType, Enum>> {
                new TaskAction<Enum>(ActionType.Next, OnAccept, default),
                new TaskAction<Enum>(ActionType.Cancel, OnReject, default, true)
            };
        }
        protected Task<IStep> OnAccept()
        {
            return State.AcceptTermsAndConditionsAsync(StepViewModel);
        }
        protected Task<IStep> OnReject()
        {
            return State.ShowTermsAndConditionsConfirmationAsync(StepViewModel);
        }
    }
}
