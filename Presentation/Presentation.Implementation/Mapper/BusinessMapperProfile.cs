﻿using AutoMapper;
using myGovID.Business.Contract.Model;
using myGovID.Presentation.Contract.Steps.CaptureEmail;
using myGovID.Presentation.Contract.Steps.CaptureEmailVerification;
using myGovID.Presentation.Contract.Steps.Passport;
using myGovID.Presentation.Contract.Steps.DriverLicence;
using myGovID.Presentation.Contract.Steps.MedicareCardInput;
using myGovID.Presentation.Contract.Steps.TermsAndConditions;

namespace myGovID.Presentation.Mapper
{
    public class BusinessMapperProfile: Profile 
    {
        public BusinessMapperProfile() {
            CaptureEmailModelMap();
            CaptureEmailVerificationModelMap();
            RegisterPOIModelMap();
            PassportDetailsModelMapper();
            DriverLicenceDetailsModelMapper();
            MedicareDetailsModelMapper();
        }
        private void CaptureEmailModelMap()
        {
            CreateMap<CaptureEmailViewModel, CaptureEmailModel>()
                .ConvertUsing((CaptureEmailViewModel arg1, CaptureEmailModel arg2) => arg2 = new CaptureEmailModel(arg1.GetEmailAddress()));
        }
        private void CaptureEmailVerificationModelMap()
        {
            CreateMap<CaptureEmailVerificationViewModel, CaptureEmailVerificationModel>()
                .ConvertUsing((CaptureEmailVerificationViewModel src, CaptureEmailVerificationModel arg2) => arg2 = new CaptureEmailVerificationModel(src.EmailAddress, src.GetVerificationCode()));
        }
        private void RegisterPOIModelMap()
        {
            CreateMap<TermsAndConditionsViewModel, StartPoiModel>()
                .ConvertUsing((TermsAndConditionsViewModel src, StartPoiModel arg2) => arg2 = new StartPoiModel(src.Version));
        }
        private void PassportDetailsModelMapper()
        {
            CreateMap<PassportViewModel, PassportModel>()
               .ConvertUsing((PassportViewModel src, PassportModel result) =>
               {
                   result = new PassportModel(src.GivenName.Value,
                           src.FamilyName.Value,
                           src.PassportNumber.Value,
                           src.Gender.Value,
                           src.DateOfBirth.GetDateTime(),
                           src.ConsentProvided.Value);
                   return result;
               });
        }
        private void DriverLicenceDetailsModelMapper()
        {
            CreateMap<DriverLicenceViewModel, DriverLicenceModel>()
               .ConvertUsing((DriverLicenceViewModel src, DriverLicenceModel result) =>
               {
                   result = new DriverLicenceModel(src.DriverLicenceDocumentNumber.Value,
                            src.GivenName.Value,
                            src.MiddleName.Value,
                            src.FamilyName.Value,
                            src.DateOfBirth.GetDateTime(),
                            src.State.Value,
                            src.ConsentProvided.Value);
                   return result;
               });
        }
        private void MedicareDetailsModelMapper()
        {
            CreateMap<MedicareCardInputViewModel, MedicareDetailsModel>()
                .ConvertUsing((MedicareCardInputViewModel src, MedicareDetailsModel result) =>
                {
                    result = new MedicareDetailsModel(
                        src.NameLine1.Value,
                        src.NameLine2.Value,
                        src.NameLine3.Value,
                        src.NameLine4.Value,
                        src.CardNumber.Value,
                        src.Expiry.Value,
                        src.CardType.Value,
                        int.Parse(src.Irn.Value),
                        src.ConsentProvided.Value
                    );

                    return result;
                });
        }
    }
}
