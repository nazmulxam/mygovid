﻿using System;
using System.Threading.Tasks;
using myGovID.Business.Contract.Component;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Authenticate;
using myGovID.Presentation.Steps;
using myGovID.Services.Dependency.Contract;
using myGovID.Business.Contract.Model;
using System.Linq;
using myGovID.Services.Log.Contract;
using myGovID.Presentation.Contract.Inputs;

namespace myGovID.Presentation.States
{
    public class AuthenticateState : BaseState
    {
        private readonly IDependencyService _dependencyService;
        private readonly ILocalCredentialComponent _localCredentialComponent;
        private readonly IPoiComponent _poiComponent;
        public AuthenticateState(IStepRepository stepRepository,
                                 IVersionComponent versionComponent,
                                 IDependencyService dependencyService,
                                 ILocalCredentialComponent localCredentialComponent,
                                 IPoiComponent poiComponent,
                                 ILogger logger) : base(stepRepository, versionComponent, logger)
        {
            _dependencyService = dependencyService;
            _localCredentialComponent = localCredentialComponent;
            _poiComponent = poiComponent;
        }

        protected override IStep CreateInitialStep(IViewModel viewModel)
        {
            return new AuthenticateStep(this, (AuthenticateViewModel)viewModel, null, null);
        }

        public async Task<IStep> OnBiometricAuthenticationAsync()
        {
            AuthenticateViewModel viewModel = new AuthenticateViewModel(true);
            viewModel.PasswordInput.Value = _localCredentialComponent.GetUserPassword();
            // Password should never be invalid in this case.
            return await Login(viewModel);
        }

        public async Task<IStep> OnPasswordAuthenticationAsync(AuthenticateViewModel viewModel)
        {
            return await Login(viewModel);
        }

        public async Task<IStep> OnForgottenPasswordAsync()
        {
            throw new NotImplementedException();
        }

        private async Task<IStep> Login(AuthenticateViewModel viewModel)
        {
            bool result = await _localCredentialComponent.LoginUsingPasswordAsync(viewModel.GetUserPassword());
            if (!result)
            {
                if (viewModel != null)
                {
                    EnterExistingPasswordInput passwordInput = (EnterExistingPasswordInput)viewModel.Inputs.First((input) => input.MetaData == InputMetaData.Password);
                    passwordInput.Error = "The password you provided was invalid.";
                }
                return null;
            }
            result = await _localCredentialComponent.LoginUsingKeystoreAsync();
            if (!result)
            {
                return null; // TODO: Throw a reset error?
            }
            return await GetPoiAsync(viewModel);
        }

        private async Task<IStep> GetPoiAsync(AuthenticateViewModel viewModel = null)
        {
            try
            {
                PoiResponseModel poiResponseModel = await _poiComponent.GetPoi();
                return poiResponseModel.Messages != null && poiResponseModel.Messages.Any()
                        ? await TransitionOnPoiResponseAsync(poiResponseModel)
                        : await FinaliseStateAsync();
            }
            catch (Exception e)
            {
                return await OnExceptionAsync(e);
            }
        }
    }
}