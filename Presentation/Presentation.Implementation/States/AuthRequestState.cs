﻿using System.Threading.Tasks;
using EnsureThat;
using myGovID.Business.Contract.Component;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Steps;
using myGovID.Services.Log.Contract;

namespace myGovID.Presentation.States
{
    public class AuthRequestState : BaseState
    {
        public AuthRequestState(IStepRepository stepRepository,
                                      IVersionComponent versionComponent,
                                      ILogger logger
                                     ) : base(stepRepository, versionComponent, logger)
        {
        }

        public override async Task<IStep> BeginStateAsync(IStep currentStep, IViewModel viewModel)
        {
            Ensure.Any.IsNotNull(currentStep, nameof(currentStep));

            StepHistory.Push(currentStep);
            IStep authRequestStep = CreateInitialStep(viewModel);
            return ShowStep(authRequestStep);
        }

        protected override IStep CreateInitialStep(IViewModel viewModel)
        {
            return new AuthRequestStep(this);
        }

        public async Task<IStep> DismissOptionalUpgradeAsync()
        {
            return GoBack();
        }
    }
}