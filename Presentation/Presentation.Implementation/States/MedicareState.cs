using System;
using System.Linq;
using System.Threading.Tasks;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Component;
using myGovID.Business.Contract.Model;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Browse;
using myGovID.Presentation.Contract.Steps.MedicareCardInput;
using myGovID.Presentation.Contract.Steps.MedicareOcrStep;
using myGovID.Presentation.Steps;
using myGovID.Presentation.Steps.DocumentIdentitySuccess;
using myGovID.Services.Log.Contract;

namespace myGovID.Presentation.States
{
    public class MedicareState : BaseState
    {
        private readonly IDevice _device;
        private readonly IDocumentComponent _documentComponent;
        private readonly IApplication _application;

        public MedicareState(IStepRepository stepRepository,
                             IVersionComponent versionComponent,
                             IDocumentComponent documentComponent,
                             IDevice device,
                             IApplication application,
                             ILogger logger) : base(stepRepository, versionComponent, logger)
        {
            _device = device;
            _documentComponent = documentComponent;
            _application = application;
        }
        protected override IStep CreateInitialStep(IViewModel viewModel)
        {
            // TODO: Check what the OCR capability is called
            //var hasMedicareOcrFeature = _application.Capabilities.Contains(Capability.OCR_Medicare);
            var hasMedicareOcrFeature = true;

            if (hasMedicareOcrFeature)
            {
                return new MedicareOcrIntroStep(this);
            }
            else
            {
                return new MedicareCardInputStep(this, false);
            }

        }
        public async Task<IStep> ProcessMedicareCardInput(MedicareCardInputViewModel viewModel)
        {
            try
            {
                var model = AutoMapper.Mapper.Map<MedicareDetailsModel>(viewModel);

                MedicareDetailsBusinessResponseModel poiResponseModel = await _documentComponent.CreateMedicareDetailsAsync(model);
                return poiResponseModel.Messages != null && poiResponseModel.Messages.Any()
                        ? await TransitionOnPoiResponseAsync(poiResponseModel)
                        : new DocumentIdentitySuccessStep(this);
            }
            catch (Exception e)
            {
                return await OnExceptionAsync(e);
            }
        }

        public IStep BeginOcr()
        {
            return ShowStep(new MedicareOcrStep(this));
        }

        public IStep EnterManually()
        {
            return ShowStep(new MedicareCardInputStep(this, false));
        }

        public IStep ProcessMedicareOcr(MedicareOcrViewModel viewModel)
        {
            // TODO: Populate the MedicareCardInputViewModel from the OCR results
            return ShowStep(new MedicareCardInputStep(this, true));
        }

        public IStep LaunchHelp()
        {
            return new BrowseStep(this, new BrowseViewModel(CommonLinks.MedicareHelpUrl));
        }
    }
}