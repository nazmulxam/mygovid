﻿using System.Threading.Tasks;
using myGovID.Business.Contract.Component;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Browse;
using myGovID.Presentation.Steps;
using myGovID.Services.Dependency.Contract;
using myGovID.Services.Log.Contract;

namespace myGovID.Presentation.States
{
    public class SettingsState : BaseState
    {
        private readonly IDependencyService _dependencyService;

        public SettingsState(IStepRepository stepRepository,
                             IDependencyService dependencyService,
                             ILogger logger,
                             IVersionComponent versionComponent)
            : base(stepRepository, versionComponent, logger)
        {
            _dependencyService = dependencyService;
        }

        public async Task<IStep> GoToDashboard()
        {
            return await FinaliseStateAsync();
        }

        public async Task<IStep> GoToChangePassword()
        {
            return null;
        }

        public async Task<IStep> SetBiometrics()
        {
            return null;
        }

        public async Task<IStep> SetNotification()
        {
            return null;
        }

        protected override IStep CreateInitialStep(IViewModel viewModel)
        {
            return new SettingsStep(this);
        }

        public Task<IStep> LaunchUrl(string url)
        {
            return Task.Factory.StartNew(() =>
            {
                return (IStep)new BrowseStep(this, new BrowseViewModel(url));
            });
        }
    }
}