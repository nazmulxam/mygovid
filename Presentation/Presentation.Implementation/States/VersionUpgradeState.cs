﻿using System;
using System.Threading.Tasks;
using myGovID.Business.Contract.Component;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.VersionUpgrade;
using myGovID.Presentation.Steps;
using myGovID.Services.Log.Contract;

namespace myGovID.Presentation.States
{
    public class VersionUpgradeState : BaseState
    {
        private readonly IVersionComponent _versionComponent;
        public VersionUpgradeState(IStepRepository stepRepository,
                                   IVersionComponent versionComponent,
                                   ILogger logger) : base(stepRepository, versionComponent, logger)
        {
            _versionComponent = versionComponent;
        }
        public override async Task<IStep> BeginStateAsync(IStep currentStep, IViewModel viewModel)
        {
            StepHistory.Push(currentStep);
            IStep upgradeStep = CreateInitialStep(viewModel);
            return ShowStep(upgradeStep);
        }
        protected override IStep CreateInitialStep(IViewModel viewModel)
        {
            return new VersionUpgradeStep(this, (VersionUpgradeViewModel)viewModel, null);
        }
        public override Task<IStep> FinaliseStateAsync(IViewModel viewModel = null)
        {
            //TODO: Navigate to browser for upgrade
            throw new NotImplementedException();
        }
        public async Task<IStep> DismissOptionalUpgradeAsync()
        {
            _versionComponent.DismissUpgrade();
            return GoBack();
        }
    }
}