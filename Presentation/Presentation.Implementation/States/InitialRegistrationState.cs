﻿using System;
using System.Threading.Tasks;
using myGovID.Business.Contract.Component;
using myGovID.Business.Contract.Model;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.States;
using myGovID.Presentation.Contract.Steps.TermsAndConditions;
using myGovID.Presentation.Steps;
using myGovID.Services.Dependency.Contract;
using myGovID.Services.Log.Contract;

namespace myGovID.Presentation.States
{
    public class InitialRegistrationState : BaseState
    {
        private readonly ITermsAndConditionsComponent _termsAndConditionsComponent;
        private readonly IPoiComponent _poIComponent;
        private readonly IDependencyService _dependencyService;

        public InitialRegistrationState(IStepRepository stepRepository,
                                        IVersionComponent versionComponent,
                                        ITermsAndConditionsComponent termsAndConditionsComponent,
                                        IPoiComponent poIComponent,
                                        IDependencyService dependencyService,
                                        ILogger logger
                                       ) : base(stepRepository, versionComponent, logger)
        {
            _termsAndConditionsComponent = termsAndConditionsComponent;
            _poIComponent = poIComponent;
            _dependencyService = dependencyService;
        }
        protected override IStep CreateInitialStep(IViewModel viewModel)
        {
            return new OnBoardingStep(this, null);
        }
        public async Task<IStep> ShowTermsAndConditionsAsync()
        {
            try
            {
                TermsAndConditionsResponseModel response = await _termsAndConditionsComponent.GetTermsAndConditionsAsync();
                return ShowStep(new TermsAndConditionsStep(this, response.Url, response.Version, null), true);
            }
            catch (Exception e)
            {
                return await OnExceptionAsync(e);
            }
        }
        public async Task<IStep> AcceptTermsAndConditionsAsync(TermsAndConditionsViewModel viewModel)
        {
            try
            {
                PoiResponseModel response = await _poIComponent.StartPoiAsync(AutoMapper.Mapper.Map<StartPoiModel>(viewModel));
                return await FinaliseStateAsync(viewModel);
            }
            catch (Exception e)
            {
                return await OnExceptionAsync(e);
            }
        }
        public async Task<IStep> ShowTermsAndConditionsConfirmationAsync(TermsAndConditionsViewModel viewModel)
        {
            return ShowStep(new TermsAndConditionsConfirmStep(this, viewModel.Link, viewModel.Version), false);
        }
        public async Task<IStep> OnTermsAndConditionsDeclinedAsync()
        {
            IState state = (IState)_dependencyService.Resolve(typeof(AccountSetupState));
            try
            {
                return await state.BeginStateAsync();
            }
            catch (Exception e)
            {
                return await OnExceptionAsync(e);
            }
        }
    }
}