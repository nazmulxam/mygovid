﻿using System.Threading.Tasks;
using myGovID.Business.Contract.Component;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.RecoverableDeadEnd;
using myGovID.Presentation.Steps;
using myGovID.Services.Log.Contract;

namespace myGovID.Presentation.States
{
    public class RecoverableDeadEndState : BaseState
    {
        private IStepRepository _stepRepository;
        private ILocalCredentialComponent _localCredentialComponent;

        public RecoverableDeadEndState(IStepRepository stepRepository,
                                       IVersionComponent versionComponent,
                                       ILocalCredentialComponent localCredentialComponent,
                                       ILogger logger) : base(stepRepository, versionComponent, logger)
        {
            _stepRepository = stepRepository;
            _localCredentialComponent = localCredentialComponent;
        }

        public override Task<IStep> BeginStateAsync(IStep currentStep, IViewModel viewModel)
        {
            return Task.FromResult(CreateInitialStep(viewModel));
        }

        protected override IStep CreateInitialStep(IViewModel viewModel)
        {
            EnsureThat.EnsureArg.IsOfType(viewModel, typeof(RecoverableDeadEndViewModel), nameof(viewModel));

            return new RecoverableDeadEndErrorStep(this, ((RecoverableDeadEndViewModel)viewModel).Exception);
        }

        public override async Task<IStep> FinaliseStateAsync(IViewModel viewModel = null)
        {
            // Default behaviour if user is not registerd drop back to start of app
            // TODO: With push notifications figure out when it's appropriate to clear
            // the deregister and clear tokens.
            if (!_localCredentialComponent.IsRegistered())
            {
                return await _stepRepository.GetInitialStepAsync();
            }

            // Follow existing app links
            return await _stepRepository.GetNextStepAsync(null, null);
        }
    }
}
