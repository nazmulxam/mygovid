﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Component;
using myGovID.Business.Contract.Model;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Passport;
using myGovID.Presentation.Steps.DocumentIdentitySuccess;
using myGovID.Presentation.Steps.Passport;
using myGovID.Services.Log.Contract;

namespace myGovID.Presentation.States
{
    public class PassportState : BaseState
    {
        private readonly IStepRepository _stepRepository;
        private readonly IDevice _device;
        private readonly IDocumentComponent _documentComponent;
        private readonly IProcessInformation _processInformation;
        public PassportState(IStepRepository stepRepository,
                                IVersionComponent versionComponent,
                                IDocumentComponent documentComponent,
                                IProcessInformation processInformation,
                                IDevice device,
                                ILogger logger) : base(stepRepository, versionComponent, logger)
        {
            _stepRepository = stepRepository;
            _device = device;
            _documentComponent = documentComponent;
            _processInformation = processInformation;
        }

        public async Task<IStep> SubmitPassportDetailsAsync(PassportViewModel stepViewModel)
        {
            try
            {
                var model = new PassportModel(stepViewModel.GivenName.Value,
                                          stepViewModel.FamilyName.Value,
                                          stepViewModel.PassportNumber.Value,
                                          stepViewModel.Gender.Value,
                                          stepViewModel.DateOfBirth.GetDateTime(),
                                          stepViewModel.ConsentProvided.Value);


                PassportDocumentBusinessResponseModel poiResponseModel = await _documentComponent.CreatePassportDetailsAsync(model);
                return poiResponseModel.Messages != null && poiResponseModel.Messages.Any()
                ? await TransitionOnPoiResponseAsync(poiResponseModel)
                : new DocumentIdentitySuccessStep(this);
            }
            catch (Exception e)
            {
                return await OnExceptionAsync(e);
            }
        }

        public async Task<IStep> OnEnterManually()
        {
            return ShowStep(new CapturePassportDetailsStep(this));
        }

        public async Task<IStep> OnStartOCRCapture()
        {
            return ShowStep(new PassportOCRStep(this, new PassportViewModel(true)));
        }

        public async Task<IStep> OnOCRCaptured(PassportViewModel passportViewModel)
        {
            return ShowStep(new CapturePassportDetailsStep(this, passportViewModel));
        }

        protected override IStep CreateInitialStep(IViewModel viewModel)
        {
            if (HasOCR())
            {
                return new PassportOCRIntroStep(this);
            }
            else
            {
                return new CapturePassportDetailsStep(this);
            }
        }

        private bool HasOCR()
        {
            return _processInformation.Capabilities.Contains(Capability.OCR_Passport);
        }
    }
}
