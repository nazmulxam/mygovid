﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Component;
using myGovID.Business.Contract.Model;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.DriverLicence;
using myGovID.Presentation.Steps;
using myGovID.Presentation.Steps.DocumentIdentitySuccess;
using myGovID.Presentation.Steps.DriverLicence;
using myGovID.Services.Log.Contract;

namespace myGovID.Presentation.States
{
    public class DriverLicenceState : BaseState
    {
        private readonly IDevice _device;
        private readonly IDocumentComponent _documentComponent;
        private readonly IProcessInformation _processInformation;
        private readonly IStepRepository _stepRepository;

        public DriverLicenceState(IStepRepository stepRepository,
                                    IVersionComponent versionComponent,
                                    IDocumentComponent documentComponent,
                                    IProcessInformation processInformation,
                                    IDevice device,
                                    ILogger logger) : base(stepRepository, versionComponent, logger)
        {
            _stepRepository = stepRepository;
            _device = device;
            _documentComponent = documentComponent;
            _processInformation = processInformation;
        }
        public async Task<IStep> SubmitDriverLicenceDetailsAsync(DriverLicenceViewModel stepViewModel)
        {
            try
            {
                var model = new DriverLicenceModel(stepViewModel.DriverLicenceDocumentNumber.Value,
                                                    stepViewModel.GivenName.Value,
                                                    stepViewModel.MiddleName.Value,
                                                    stepViewModel.FamilyName.Value,
                                                    stepViewModel.DateOfBirth.GetDateTime(),
                                                    stepViewModel.State.Value,
                                                    stepViewModel.ConsentProvided.Value);

                DriverLicenceDocumentBusinessResponseModel poiResponseModel = await _documentComponent.CreateDriverLicenceDetailsAsync(model);
                return poiResponseModel.Messages != null && poiResponseModel.Messages.Any()
                        ? await TransitionOnPoiResponseAsync(poiResponseModel)
                        : new DocumentIdentitySuccessStep(this); ;
            }
            catch (Exception e)
            {
                return await OnExceptionAsync(e);
            }
        }
        public async Task<IStep> OnEnterManually()
        {
            return ShowStep(new CaptureDriverLicenceDetailsStep(this));
        }
        public async Task<IStep> OnStartOCRCapture()
        {
            return ShowStep(new DriverLicenceOCRStep(this, new DriverLicenceViewModel(true)));
        }
        public async Task<IStep> OnOCRCaptured(DriverLicenceViewModel driverLicenceViewModel)
        {
            return ShowStep(new CaptureDriverLicenceDetailsStep(this, driverLicenceViewModel));
        }
        protected override IStep CreateInitialStep(IViewModel viewModel)
        {
            if (HasOCR())
            {
                return new DriverLicenceOCRIntroStep(this);
            }
            else
            {
                return new CaptureDriverLicenceDetailsStep(this);
            }
        }
        private bool HasOCR()
        {
            return _processInformation.Capabilities.Contains(Capability.OCR_Driver_Licence);
        }
    }
}