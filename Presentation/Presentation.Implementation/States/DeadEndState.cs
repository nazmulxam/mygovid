﻿using System;
using System.Threading.Tasks;
using myGovID.Business.Contract.Component;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.DeadEnd;
using myGovID.Presentation.Steps;
using myGovID.Services.Log.Contract;

namespace myGovID.Presentation.States
{
    public class DeadEndState : BaseState
    {
        public DeadEndState(IStepRepository stepRepository,
                            IVersionComponent versionComponent,
                            ILogger logger) : base(stepRepository, versionComponent, logger)
        {
        }
        public override Task<IStep> BeginStateAsync(IStep currentStep, IViewModel viewModel)
        {
            return Task.Run(() => CreateInitialStep(viewModel));
        }
        protected override IStep CreateInitialStep(IViewModel viewModel)
        {
            return new DeadEndStep(this, ((DeadEndViewModel)viewModel).Error);
        }
        public override Task<IStep> FinaliseStateAsync(IViewModel viewModel = null)
        {
            throw new NotSupportedException();
        }
    }
}