﻿using System.Collections.Generic;
using System.Linq;
using myGovID.Business.Contract.Component;
using myGovID.Business.Contract.Model;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.States;
using System.Threading.Tasks;
using System;
using AutoMapper;
using myGovID.Core.Exceptions;
using myGovID.Business.Contract;
using myGovID.Services.Log.Contract;
using myGovID.Presentation.Steps;
using myGovID.Presentation.Contract.Steps.Browse;

namespace myGovID.Presentation.States
{
    public abstract class BaseState : IState
    {
        private readonly IStepRepository _stepRepository;
        protected Stack<IStep> StepHistory { get; }
        private readonly IVersionComponent _versionComponent;
        protected ILogger Logger { get; }

        protected BaseState(IStepRepository stepRepository, IVersionComponent versionComponent, ILogger logger)
        {
            StepHistory = new Stack<IStep>();
            Logger = logger;
            _versionComponent = versionComponent;
            _stepRepository = stepRepository;
        }
        public virtual async Task<IStep> BeginStateAsync(IStep currentStep = null, IViewModel viewModel = null)
        {
            if (currentStep != null)
            {
                StepHistory.Push(currentStep);
            }
            //TODO: check if user has access to this step, if not show forbidden step
            //TODO: fetch POI information and update links
            IStep initalStep = CreateInitialStep(viewModel);
            IStep upgradeStep = await _stepRepository.GetVersionUpgradeStepAsync(initalStep);
            initalStep.ChildStep = upgradeStep;
            return ShowStep(initalStep);
        }

        public virtual Task<IStep> FinaliseStateAsync(IViewModel viewModel = null)
        {
            return _stepRepository.GetNextStepAsync(null, viewModel);
        }

        protected abstract IStep CreateInitialStep(IViewModel viewModel);

        public IStep GoBack()
        {
            if (CanGoBack())
            {
                IStep topElement = StepHistory.Pop();
                IStep peek = StepHistory.Peek();
                if (peek.ChildStep == topElement)
                {
                    peek.ChildStep = null;
                }
                return peek;
            }
            throw new NotSupportedException("Cant go back.");
        }

        public bool CanGoBack()
        {
            return StepHistory.Count() > 1;
        }

        protected IStep ShowStep(IStep step, bool pop = false)
        {
            if (pop && StepHistory.Any())
            {
                StepHistory.Pop();
            }
            StepHistory.Push(step);
            return step;
        }

        protected async Task<IStep> TransitionOnPoiResponseAsync(PoiResponseModel responseModel)
        {
            IStep step = StepHistory.Peek();
            IEnumerable<PageMessage> messages = AutoMapper.Mapper.Map<IEnumerable<PageMessage>>(responseModel.Messages);
            IStep evaluatedStep = await _stepRepository.GetNextStepAsync(null, step.ViewModel);
            if (step.GetType() == evaluatedStep.GetType())
            {
                step.Messages = messages;
                return step;
            }
            IStep stepBeforeTransition = GetStepBeforeTransition(responseModel);
            if (stepBeforeTransition != null)
            {
                return stepBeforeTransition;
            }
            evaluatedStep.Messages = messages;
            return evaluatedStep;
        }

        protected virtual IStep GetStepBeforeTransition(PoiResponseModel responseModel)
        {
            return null;
        }

        protected async Task<IStep> OnExceptionAsync(Exception exception)
        {
            switch (exception)
            {
                case RecoverableDeadEndError recoverableError:
                    return await _stepRepository.GetRecoverableDeadEndStepAsync(
                        new PresentationError(recoverableError.Code, recoverableError.Title, recoverableError.Description));

                case TaskFailedException taskFailedException:
                    IStep step = StepHistory.Peek();
                    if (_stepRepository.IsValidState(this))
                    {
                        IEnumerable<PageMessage> messages = AutoMapper.Mapper.Map<IEnumerable<PageMessage>>(taskFailedException.Messages);
                        step.Messages = messages;
                        return step;
                    }

                    return await _stepRepository.GetNextStepAsync();

                default:
                    var presentationError = GetDeadEndPresentationError(exception);
                    return _stepRepository.GetDeadEndStep(presentationError);
            }
        }

        public PresentationError GetDeadEndPresentationError(Exception exception)
        {
            if (exception is AggregateException aggregateException)
            {
                Exception innerException = aggregateException.GetBaseException();
                return GetDeadEndPresentationError(innerException);
            }
            if (exception is AutoMapperMappingException autoMapperMappingException)
            {
                Exception innerException = autoMapperMappingException.GetBaseException();
                return GetDeadEndPresentationError(innerException);
            }

            Logger.Log(level: LogLevel.Error,
               message: $"{GetType().Name} remapping exception:",
               exception: exception);

            var errorCode = "UNK000001";
            if (exception is CodedException codedException)
            {
                errorCode = codedException.Code;
            }

            return new PresentationError(
                errorCode,
                "System error",
                "<p>An error has occurred while processing your request.<br><br>Close the app and try again.</p>");
        }

        public Task<IStep> OnHelp()
        {
            return Task.Factory.StartNew(() => { return (IStep)new BrowseStep(this, new BrowseViewModel(CommonLinks.HelpLink)); });
        }
    }
}