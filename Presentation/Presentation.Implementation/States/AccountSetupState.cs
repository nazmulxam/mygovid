﻿using System;
using System.Threading.Tasks;
using myGovID.Business.Contract.Component;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.States;
using myGovID.Presentation.Steps;
using myGovID.Services.Dependency.Contract;
using myGovID.Services.Log.Contract;

namespace myGovID.Presentation.States
{
    public class AccountSetupState : BaseState
    {
        private readonly IDependencyService _dependencyService;
        public AccountSetupState(IStepRepository stepRepository,
                                 IVersionComponent versionComponent,
                                 IDependencyService dependencyService,
                                 ILogger logger) : base(stepRepository, versionComponent, logger)
        {
            _dependencyService = dependencyService;
        }
        protected override IStep CreateInitialStep(IViewModel viewModel)
        {
            return new AccountSetupStep(this);
        }
        public async Task<IStep> OnRegisterAsync()
        {
            IState state = (IState)_dependencyService.Resolve(typeof(InitialRegistrationState));
            try
            {
                return await state.BeginStateAsync();
            }
            catch (Exception e)
            {
                return await OnExceptionAsync(e);
            }
        }
        public Task<IStep> OnRecover()
        {
            throw new NotImplementedException();
        }
    }
}