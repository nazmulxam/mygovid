using System.Threading.Tasks;
using myGovID.Business.Contract.Component;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.Dashboard;
using myGovID.Presentation.Contract.Steps.Dashboard.Alert;
using myGovID.Presentation.Contract.Steps.Dashboard.Document;
using myGovID.Presentation.Steps;
using myGovID.Services.Dependency.Contract;
using System;
using System.Collections.Generic;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Common.Process;
using myGovID.Business.Contract.Common.Rules;
using System.Linq;
using myGovID.Services.Log.Contract;
using myGovID.Presentation.Contract.States;
using myGovID.Services.Event.Contract;
using myGovID.Business.Contract.Common.Session;

namespace myGovID.Presentation.States
{
    public class DashboardState : BaseState
    {
        private readonly IDependencyService _dependencyService;
        private readonly IApplication _application;
        private readonly IRulesEngine _rulesEngine;
        private readonly IProcessInformation _processInformation;
        private readonly DocumentBuilder _documentBuilder;
        private readonly IEventService _eventService;

        public DashboardState(IStepRepository stepRepository,
                              IVersionComponent versionComponent,
                              IApplication application,
                              IRulesEngine rulesEngine,
                              IProcessInformation processInformation,
                              IDependencyService dependencyService,
                              ILogger logger,
                              IEventService eventService) : base(stepRepository, versionComponent, logger)
        {
            _dependencyService = dependencyService;
            _application = application;
            _rulesEngine = rulesEngine;
            _processInformation = processInformation;
            _documentBuilder = new DocumentBuilder(_application.Capabilities);
            _eventService = eventService;
        }
        public override Task<IStep> BeginStateAsync(IStep currentStep = null, IViewModel viewModel = null)
        {
            _eventService.Subscribe<IAuthenticationContext>(this, EventKey.IdentityUpdated, (authContext) =>
            {
                foreach (IStep step in StepHistory)
                {
                    if (step is DashboardStep)
                    {
                        ((DashboardStep)step).UpdateViewModel(GetFirstName(authContext), GetAuthStrength());
                    }
                    // TODO: Also update AboutStepViewModel.
                }
            });
            return base.BeginStateAsync(currentStep, viewModel);
        }
        public override Task<IStep> FinaliseStateAsync(IViewModel viewModel = null)
        {
            _eventService.UnSubscribe<IAuthenticationContext>(this, EventKey.IdentityUpdated);
            return base.FinaliseStateAsync(viewModel);
        }
        public Task<IStep> OnAbout()
        {
            throw new NotImplementedException();
        }

        public Task<IStep> OnAccountDetails()
        {
            throw new NotImplementedException();
        }

        public async Task<IStep> OnSettings()
        {
            IState state = (IState)_dependencyService.Resolve(typeof(SettingsState));
            return await state.BeginStateAsync();
        }

        public Task<IStep> OnShowTutorial()
        {
            throw new NotImplementedException();
        }

        public async Task<IStep> OnPassport()
        {
            IState state = (IState)_dependencyService.Resolve(typeof(PassportState));
            try
            {
                return await state.BeginStateAsync(StepHistory.FirstOrDefault());
            }
            catch (Exception e)
            {
                return await OnExceptionAsync(e);
            }
        }

        public Task<IStep> OnMedicare()
        {
            var medicareState = _dependencyService.Resolve<MedicareState>();
            return medicareState.BeginStateAsync(StepHistory.Peek());
        }

        public async Task<IStep> OnDriverLicenceAsync()
        {
            IState state = (IState)_dependencyService.Resolve(typeof(DriverLicenceState));
            try
            {
                return await state.BeginStateAsync(StepHistory.FirstOrDefault());
            }
            catch (Exception e)
            {
                return await OnExceptionAsync(e);
            }
        }

        public Task<IStep> OnFacialVerification()
        {
            throw new NotImplementedException();
        }

        protected override IStep CreateInitialStep(IViewModel viewModel)
        {
            return new DashboardStep(this,
                                     GetFirstName(_application.CurrentSession.AuthenticatedContext) ?? "",
                                     GetDocuments(),
                                     Array.Empty<IAlert>(),
                                     GetAuthStrength());
        }

        protected IEnumerable<IDocument> GetDocuments()
        {
            var states = _rulesEngine.AllStates(_processInformation);
            return _documentBuilder.FromStates(states);
        }

        protected string GetFirstName(IAuthenticationContext authContext)
        {
            if (authContext.AuthenticatedState == AuthenticationState.AuthenticationExpired || authContext.AuthenticatedState == AuthenticationState.Unauthenticated)
            {
                return null;
            }
            var identity = authContext.Current.MyGovIdentity;
            if (identity == null)
            {
                return null;
            }
            string firstName = identity.FirstName;
            if (string.IsNullOrWhiteSpace(firstName))
            {
                firstName = identity.LastName;
            }
            return firstName;
        }

        protected AuthStrengthType GetAuthStrength()
        {
            switch (_application.CurrentSession.SessionProcess.ProcessIPLevel)
            {
                case IdentityProofingLevel.IP4:
                case IdentityProofingLevel.IP3:
                    return AuthStrengthType.Strong;

                case IdentityProofingLevel.IP2:
                    return AuthStrengthType.Intermediate;

                case IdentityProofingLevel.IP1:
                    return AuthStrengthType.Weak;

                default:
                    return AuthStrengthType.Unknown;
            };
        }
    }
}