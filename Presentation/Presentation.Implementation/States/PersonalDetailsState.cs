﻿using System;
using System.Linq;
using System.Threading.Tasks;
using myGovID.Business.Contract.Common.Application;
using myGovID.Business.Contract.Component;
using myGovID.Business.Contract.Model;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.CaptureEmailVerification;
using myGovID.Presentation.Contract.Steps.CapturePassword;
using myGovID.Presentation.Contract.Steps.CapturePersonalDetails;
using myGovID.Presentation.Contract.Steps.SecureYourAccount;
using myGovID.Presentation.Steps;
using myGovID.Presentation.Steps.DocumentIdentitySuccess;
using myGovID.Services.Log.Contract;
using myGovID.Services.Storage.Contract;

namespace myGovID.Presentation.States
{
    public class PersonalDetailsState : BaseState
    {
        private readonly IDevice _device;
        private bool _useBiometrics;
        private readonly ILocalCredentialComponent _localCredentialComponent;
        private readonly IDocumentComponent _documentComponent;
        private readonly IStorage _memoryStorage;
        private string _emailAddress;
        public PersonalDetailsState(IStepRepository stepRepository,
                                    IVersionComponent versionComponent,
                                    ILocalCredentialComponent localCredentialComponent,
                                    IDocumentComponent documentComponent,
                                    IDevice device,
                                    ILogger logger,
                                    IStorage memoryStorage) : base(stepRepository, versionComponent, logger)
        {
            _device = device;
            _localCredentialComponent = localCredentialComponent;
            _documentComponent = documentComponent;
            _memoryStorage = memoryStorage;
        }

        protected override IStep CreateInitialStep(IViewModel viewModel)
        {
            if (!_localCredentialComponent.IsRegistered())
            {
                _emailAddress = (viewModel as CaptureEmailVerificationViewModel)?.EmailAddress;
                BiometricType biometricType = _device.Capabilities.Contains(DeviceCapability.Fingerprint)
                                                     ? BiometricType.FingerPrint
                                                     : _device.Capabilities.Contains(DeviceCapability.Touch)
                                                     ? BiometricType.Touch
                                                     : _device.Capabilities.Contains(DeviceCapability.FacialVerification)
                                                     ? BiometricType.Face
                                                     : BiometricType.None;
                return biometricType == BiometricType.None ? (IStep)new CapturePasswordStep(this) : new SecureYourAccountStep(this, biometricType, null, null);
            }
            return new CapturePersonalDetailsStep(this);
        }
        public async Task<IStep> UseBiometricsAsync(SecureYourAccountViewModel secureYourAccountViewModel)
        {
            _useBiometrics = secureYourAccountViewModel.UseBiometrics();
            return ShowStep(new CapturePasswordStep(this), true);
        }
        public async Task<IStep> SetPasswordAsync(CapturePasswordViewModel capturePasswordViewModel)
        {
            try
            {
                string password = capturePasswordViewModel.GetPassword();
                _localCredentialComponent.RegisterUser(_emailAddress, password, _useBiometrics);
                return ShowStep(new CapturePersonalDetailsStep(this), true);
            }
            catch (Exception e)
            {
                return await OnExceptionAsync(e);
            }
        }
        public async Task<IStep> SetPersonalDetailsAsync(CapturePersonalDetailsViewModel model)
        {
            try
            {
                CapturePersonalDetailsModel personalDetailsModel = new CapturePersonalDetailsModel(
                            model.GetFirstName(), model.GetLastName(), model.GetDateOfBirth());
                PersonalDetailsBusinessResponseModel poiResponseModel = await _documentComponent.CreatePersonalDetailsAsync(personalDetailsModel);


                return poiResponseModel.Messages != null && poiResponseModel.Messages.Any()
                ? await TransitionOnPoiResponseAsync(poiResponseModel)
                : UpdateIdentityAndReturnSuccessStep(poiResponseModel);
            }
            catch (Exception e)
            {
                return await OnExceptionAsync(e);
            }
        }
        public async override Task<IStep> FinaliseStateAsync(IViewModel model)
        {
            bool loginResponse = await _localCredentialComponent.LoginUsingPasswordAsync(_localCredentialComponent.GetUserPassword());
            return await base.FinaliseStateAsync(model);
        }
        private IStep UpdateIdentityAndReturnSuccessStep(PersonalDetailsBusinessResponseModel poiResponseModel)
        {
            _localCredentialComponent.UpdateIdentity(poiResponseModel.FirstName,
                                                          poiResponseModel.LastName,
                                                          poiResponseModel.BirthDate.ToShortDateString());
            return new DocumentIdentitySuccessStep(this);
        }
    }
}