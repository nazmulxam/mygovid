﻿using System.Threading.Tasks;
using myGovID.Business.Contract.Component;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Steps;
using myGovID.Services.Log.Contract;

namespace myGovID.Presentation.States
{
    public class LogoutState : BaseState
    {
        private readonly IStepRepository _stepRepository;

        public LogoutState(IStepRepository stepRepository, IVersionComponent versionComponent, ILogger logger)
            : base(stepRepository, versionComponent, logger)
        {
            _stepRepository = stepRepository;
        }

        public override Task<IStep> BeginStateAsync(IStep currentStep, IViewModel viewModel)
        {
            return Task.Run(() => CreateInitialStep(viewModel));
        }

        protected override IStep CreateInitialStep(IViewModel viewModel)
        {
            return new LogoutStep(this);
        }

        public override Task<IStep> FinaliseStateAsync(IViewModel viewModel = null)
        {
            return _stepRepository.GetInitialStepAsync();
        }
    }
}
