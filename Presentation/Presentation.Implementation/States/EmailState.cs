﻿using myGovID.Business.Contract.Component;
using myGovID.Business.Contract.Model;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.CaptureEmail;
using myGovID.Presentation.Steps;
using myGovID.Services.Log.Contract;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace myGovID.Presentation.States
{
    public class EmailState : BaseState
    {
        private readonly IEmailComponent _emailComponent;

        public EmailState(IStepRepository stepRepository,
                          IVersionComponent versionComponent,
                          IEmailComponent emailComponent,
                          ILogger logger) : base(stepRepository, versionComponent, logger)
        {
            _emailComponent = emailComponent;
        }

        protected override IStep CreateInitialStep(IViewModel viewModel)
        {
            return new CaptureEmailStep(this);
        }

        public async Task<IStep> SetEmailAddressAsync(CaptureEmailViewModel viewModel)
        {
            try
            {
                PoiResponseModel poiResponseModel = await _emailComponent.SubmitEmailAsync(AutoMapper.Mapper.Map<CaptureEmailModel>(viewModel));
                return poiResponseModel.Messages != null && poiResponseModel.Messages.Any()
                        ? await TransitionOnPoiResponseAsync(poiResponseModel)
                        : await FinaliseStateAsync(viewModel);
            }
            catch (Exception e)
            {
                return await OnExceptionAsync(e);
            }
        }
    }
}
