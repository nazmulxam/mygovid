﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using myGovID.Business.Contract.Component;
using myGovID.Business.Contract.Model;
using myGovID.Presentation.Contract;
using myGovID.Presentation.Contract.Steps.CaptureEmail;
using myGovID.Presentation.Contract.Steps.CaptureEmailVerification;
using myGovID.Presentation.Steps;
using myGovID.Services.Log.Contract;

namespace myGovID.Presentation.States
{
    public class EmailVerificationState : BaseState
    {
        private readonly IEmailVerificationComponent _emailVerificationComponent;
        private readonly ILocalCredentialComponent _localCredentialComponent;
        public EmailVerificationState(IStepRepository stepRepository,
                                      IVersionComponent versionComponent,
                                      IEmailVerificationComponent emailVerificationComponent,
                                      ILocalCredentialComponent localCredentialComponent,
                                      ILogger logger
                                     ) : base(stepRepository, versionComponent, logger)
        {
            _emailVerificationComponent = emailVerificationComponent;
            _localCredentialComponent = localCredentialComponent;
        }
        protected override IStep CreateInitialStep(IViewModel viewModel)
        {
            string emailAddress = null;
            if (viewModel is CaptureEmailViewModel captureEmailViewModel)
            {
                emailAddress = captureEmailViewModel.GetEmailAddress();
            }
            else if (viewModel is CaptureEmailVerificationViewModel captureEmailVerificationViewModel)
            {
                emailAddress = captureEmailVerificationViewModel.EmailAddress;
            }
            return new CaptureEmailVerificationStep(this, emailAddress);
        }
        public async Task<IStep> SubmitVerificationCodeAsync(CaptureEmailVerificationViewModel viewModel)
        {
            try
            {
                EmailVerificationResponseModel result = await _emailVerificationComponent.SubmitEmailVerificationAsync(AutoMapper.Mapper.Map<CaptureEmailVerificationModel>(viewModel));
                if (result.Messages != null && result.Messages.Any())
                {
                    return await TransitionOnPoiResponseAsync(result);
                }
                bool response = await _localCredentialComponent.RegisterCredentialAsync(result.PoiAssuranceToken);
                //TODO: What do we do with a false result?!
                return await FinaliseStateAsync(viewModel);
            }
            catch (Exception e)
            {
                return await OnExceptionAsync(e);
            }
        }
        public async Task<IStep> ResendVerificationCodeAsync(CaptureEmailVerificationViewModel viewModel)
        {
            try
            {
                PoiResponseModel poiResponse = await _emailVerificationComponent.ResendEmailVerificationCodeAsync();
                return ShowStep(new ResentEmailVerificationCodeStep(this, viewModel.EmailAddress), false);
            }
            catch (Exception e)
            {
                return await OnExceptionAsync(e);
            }
        }
        public async Task<IStep> ChangeEmailAddressAsync()
        {
            try
            {
                PoiResponseModel poiResponse = await _emailVerificationComponent.ChangeEmailAsync();
                return await FinaliseStateAsync();
            }
            catch (Exception e)
            {
                return await OnExceptionAsync(e);
            }
        }
        protected override IStep GetStepBeforeTransition(PoiResponseModel responseModel)
        {
            if (responseModel.Messages == null)
            {
                return null;
            }
            IEnumerable<PageMessage> messages = AutoMapper.Mapper.Map<IEnumerable<PageMessage>>(responseModel.Messages);
            PageMessage firstErrorMessage = messages.FirstOrDefault((message) => message.Type.ToLower().Equals("error"));
            return firstErrorMessage == null
                ? null
                : new MaxAttemptsReachedStep(this,
                                                        new PresentationError(firstErrorMessage.Code, firstErrorMessage.Title, firstErrorMessage.Description),
                                                        messages, null);
        }
    }
}