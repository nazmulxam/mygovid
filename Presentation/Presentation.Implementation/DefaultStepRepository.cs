﻿using myGovID.Presentation.Contract;
using myGovID.Presentation.Steps;
using myGovID.Business.Contract.Component;
using myGovID.Business.Contract;
using myGovID.Business.Contract.Common.Rules;
using myGovID.Presentation.States;
using myGovID.Presentation.Contract.States;
using System;
using myGovID.Services.Dependency.Contract;
using myGovID.Business.Contract.Common.Process;
using System.Threading.Tasks;
using myGovID.Services.Event.Contract;
using myGovID.Business.Contract.Model;
using myGovID.Presentation.Contract.Steps.VersionUpgrade;
using System.Linq;
using myGovID.Presentation.Contract.Steps.Authenticate;
using myGovID.Services.Log.Contract;
using System.Collections.Generic;
using myGovID.Presentation.Contract.Steps.RecoverableDeadEnd;

namespace myGovID.Presentation
{
    /// <summary>
    /// Default step repository.
    /// </summary>
    public class DefaultStepRepository : IStepRepository
    {
        private readonly IVersionComponent _versionComponent;
        private readonly IRulesEngine _rulesEngine;
        private readonly IDependencyService _dependencyService;
        private readonly IProcessInformation _processInformation;
        private readonly ILogger _logger;
        private readonly ILocalCredentialComponent _localCredential;
        private readonly IEventService _eventService;
        private readonly Dictionary<BusinessState, Type> _businessStateAndTypeMapping;
        public DefaultStepRepository(IEventService eventService, ILogger logger, IProcessInformation processInformation, IDependencyService dependencyService, IVersionComponent versionComponent, ILocalCredentialComponent localCredential, IRulesEngine rulesEngine)
        {
            _logger = logger;
            _processInformation = processInformation;
            _dependencyService = dependencyService;
            _versionComponent = versionComponent;
            _rulesEngine = rulesEngine;
            _localCredential = localCredential;
            _eventService = eventService;
            _eventService.Subscribe<object>(this, EventKey.Logout, Logout);
            _eventService.Subscribe<object>(this, EventKey.SessionExpired, Logout);
            _businessStateAndTypeMapping = new Dictionary<BusinessState, Type>
            {
                { BusinessState.AccountSetup, typeof(AccountSetupState) },
                { BusinessState.VersionUpgrade, typeof(VersionUpgradeState) },
                { BusinessState.Authenticate, typeof(AuthenticateState) },
                { BusinessState.Registration, typeof(InitialRegistrationState) },
                { BusinessState.Email, typeof(EmailState) },
                { BusinessState.EmailVerification, typeof(EmailVerificationState) },
                { BusinessState.PersonalDetails, typeof(PersonalDetailsState) },
                { BusinessState.Logout, typeof(LogoutState) },
                { BusinessState.Dashboard, typeof(DashboardState) },
                { BusinessState.Passport, typeof(PassportState) },
                { BusinessState.Medicare, typeof(MedicareState) },
                { BusinessState.DriverLicence, typeof(DriverLicenceState) }

            };
        }
        private void Logout(object obj)
        {
            Action showLogoutAction = async () =>
            {
                IStep step = await GetNextStepAsync();
                _eventService.Publish(EventKey.Step, step);
            };
            showLogoutAction.Invoke();
        }
        public async Task<IStep> GetInitialStepAsync()
        {
            if (_localCredential.IsRegistered())
            {
                IState state = (IState)_dependencyService.Resolve(GetTypeByBusinessState(BusinessState.Authenticate));
                return await state.BeginStateAsync(null, new AuthenticateViewModel(_localCredential.ShowBiometricChallenge()));
            }
            else
            {
                IState state = (IState)_dependencyService.Resolve(GetTypeByBusinessState(BusinessState.AccountSetup));
                return await state.BeginStateAsync();
            }
        }
        public IStep GetDeadEndStep(PresentationError error)
        {
            DeadEndState state = _dependencyService.Resolve<DeadEndState>();
            return new DeadEndStep(state, error, null);
        }
        public async Task<IStep> GetRecoverableDeadEndStepAsync(PresentationError error)
        {
            var state = _dependencyService.Resolve<RecoverableDeadEndState>();
            return await state.BeginStateAsync(null, new RecoverableDeadEndViewModel(error));
        }
        public async Task<IStep> GetNextStepAsync(IStep currentStep = null, IViewModel viewModel = null)
        {
            BusinessState stateResponse = _rulesEngine.FindNextState(_processInformation);
            _logger.Debug(message: "BusinessState: " + stateResponse.ToString());
            Type stateType = GetTypeByBusinessState(stateResponse);
            IState state = (IState)_dependencyService.Resolve(stateType);
            return await state.BeginStateAsync(currentStep, viewModel);
        }
        private Type GetTypeByBusinessState(BusinessState businessState)
        {
            if (_businessStateAndTypeMapping.TryGetValue(businessState, out Type value))
            {
                return value;
            }
            throw new NotImplementedException();
        }
        private BusinessState GetBusinessStateByType(Type type)
        {
            BusinessState state = _businessStateAndTypeMapping.FirstOrDefault(x => x.Value == type).Key;
            return state;
        }
        public async Task<IStep> GetVersionUpgradeStepAsync(IStep parentStep = null)
        {
            if (_processInformation.UpgradeActioned)
            {
                return null;
            }
            VersionResponseModel response = await _versionComponent.GetConfigurationAsync();
            ResourceLink updateLink = response.Links.FirstOrDefault((link) => link.Target == TargetType.Update);
            if (updateLink == null)
            {
                return null;
            }
            VersionUpgradeViewModel upgradeViewModel = new VersionUpgradeViewModel(updateLink.Url, !response.Capabilities.Any());
            VersionUpgradeState state = _dependencyService.Resolve<VersionUpgradeState>();
            return await state.BeginStateAsync(parentStep, upgradeViewModel);
        }
        public bool IsValidState(IState state)
        {
            IEnumerable<BusinessState> possibleStates = _rulesEngine.AllStates(_processInformation);
            BusinessState businessState = GetBusinessStateByType(state.GetType());
            return possibleStates.Contains(businessState);
        }
    }
}
