﻿namespace myGovID.Presentation
{
    public enum BiometricType
    {
        Touch, Face, FingerPrint, None
    }
    public static class BiometricTypeExtensions
    {
        public static BiometricTypeDescription Description(this BiometricType type)
        {
            switch (type)
            {
                case BiometricType.Touch: return new BiometricTypeDescription("Touch ID", "fingerprint", "TouchID");
                case BiometricType.Face: return new BiometricTypeDescription("Face ID", "face print", "FaceID");
                case BiometricType.FingerPrint: return new BiometricTypeDescription("fingerprint", "fingerprint", "Fingerprint");
                default: return new BiometricTypeDescription("", "", "");
            }
        }
    }
    public class BiometricTypeDescription
    {
        public readonly string DisplayName;
        public readonly string BiometricName;
        public readonly string Image;
        public BiometricTypeDescription(string displayName, string biometricName, string image)
        {
            DisplayName = displayName;
            BiometricName = biometricName;
            Image = image;
        }
    }
}
